Messenger = function (conf) {
    var chat = {
        activeThreadId: null,
        userId: conf.userId,
        init: function () {
            this.cacheDOM();
            this.activeThreadId = conf.activeThreadId;
            if (conf.temporaryThread) {
                this.renderTemporaryThread(conf.temporaryThread);
            }
            this.bindEvents();
            this.fetchThreads();
            this.initEcho();

        },
        cacheDOM: function () {
            this.$frame = $('#frame');
            this.$chatHistory = this.$frame.find('div.messages');
            this.$chatHeader = this.$frame.find('.contact-profile');
            this.$chatHistoryList = this.$chatHistory.find('ul');
            this.$button = this.$frame.find('.submit');
            this.$textarea = this.$frame.find('.message-input input');
            this.$threadList = this.$frame.find('#contacts ul');
            this.$searchText = this.$frame.find('.search');
        },
        bindEvents: function () {
            this.$button.on('click', this.addMessage.bind(this));
            this.$textarea.on('keyup', this.addMessageEnter.bind(this));
            this.$searchText.on('keyup', this.searchThread.bind(this));
            this.$threadList.on('click', 'li.contact', this, this.selectThread);
        },
        initEcho: function () {
            Echo.private('user.messages.' + conf.userId)
                .listen('MessageSent', (e) => {
                    this.renderReceivedMessage(e);
                });
        },
        enableMessageInputs: function () {
            this.$textarea.prop('disabled', false);
            this.$button.prop('disabled', false);
        },
        renderThread: function (thread, user) {
            // Nếu thread do user chat với cơ sở, hiển thị subject là tên cơ sở
            if (user.id == conf.userId) {
                thread.subject = thread.organization.name;
                thread.avatar = conf.rootUrl + (thread.organization.logo_image == null ? 'img/no-avatar.jpg' : "image/50/50/" + thread.organization.logo_image);
            }
            // Ngược lại, cơ sở chat với user, hiển thị tên user
            else {
                thread.subject = user.name;
                thread.avatar = conf.rootUrl + (thread.user.user_profile == null || thread.user.user_profile.avatar == null ? 'img/no-avatar.jpg' : 'image/50/50/' + thread.user.user_profile.avatar);
            }
            if (thread.id == this.activeThreadId) {
                thread.class = 'active';
                this.renderProfile(thread.subject, thread.avatar);
            }

            thread.subject = sdcApp.htmlEntities(thread.subject);
            thread.avatar = sdcApp.htmlEntities(thread.avatar);
            user.name = sdcApp.htmlEntities(user.name);
            return `<li class="contact ${thread.class}" data-thread-id="${thread.id}" data-thread-user-org="${user.id}:${thread.organization.id}" data-unread-count="${thread.unreadMessagesCount ? thread.unreadMessagesCount : 0}">
                        <div class="wrap">
                            <!--span class="contact-status online"></span --> <!-- Nguyen Quan comment -->
                            <img src="${thread.avatar}" alt="${thread.subject}"
                                 title="Tạo bởi: ${user.name}"/>
                            <div class="meta">
                                <p class="name">
                                    <div class="subject">${thread.subject}</div>
                                    <span class="unread-count badge">${thread.unreadMessagesCount ? thread.unreadMessagesCount : ''}</span>
                                </p>
                                <p class="preview">
                                    ${thread.latestMsg ?
                                    `<span>
                                    ${thread.latestMsg.user_id == conf.userId ? 'Bạn' : sdcApp.htmlEntities(thread.latestMsg.user.name)}:
                                    </span> ${sdcApp.htmlEntities(thread.latestMsg.body)}` :
                                    ''}
                                </p>
                            </div>
                        </div>
                    </li>`;
        },
        formatTimestamp: function (d) {
            return window.moment(d, 'YYYY-MM-DD HH:mm:ss', 'vi', true).calendar();//fromNow();//format('HH:mm DD/MM/YYYY');
        },
        renderMessage: function (message, creator) {
            var cssClass = this.userId == creator.id ? 'sent' : 'replies';
            var avatar = this.userId == creator.id ? $("#profile-img").attr("src")
                : conf.rootUrl + (creator.user_profile == null || creator.user_profile.avatar == null ? 'img/no-avatar.jpg' : 'image/50/50/' + creator.user_profile.avatar);
            return `<li class="${cssClass}">
                        <img src="${sdcApp.htmlEntities(avatar)}"
                             title="${sdcApp.htmlEntities(creator.name)}"
                             alt="${sdcApp.htmlEntities(creator.name)}" class="img-circle">
                        <p>${sdcApp.htmlEntities(message.body)}</p>
                        <div class="timestamp">${this.formatTimestamp(message.created_at)}</div>
                    </li>`;
        },
        renderProfile: function (subject, avatar) {
            this.$chatHeader.find('.name').html(sdcApp.htmlEntities(subject));
            this.$chatHeader.find('img').attr('src', sdcApp.htmlEntities(avatar));
        },
        renderThreads: function (threads) {
            for (var index = 0; index < threads.length; ++index) {
                var thread = threads[index];
                this.$threadList.append(this.renderThread(thread, thread.user));
            }
        },
        renderTemporaryThread: function (thread) {
            this.$threadList.append(this.renderThread(thread, thread.user));
            this.enableMessageInputs();
        },
        renderInitMessages: function (messages) {
            this.$chatHistoryList.empty();
            for (var index = 0; index < messages.length; ++index) {
                var message = messages[index];
                this.$chatHistoryList.append(this.renderMessage(message, message.user));
            }
            this.scrollToBottom();
        },
        fetchThreads: function () {
            var that = this;
            $.ajax({
                "url": conf.baseUrl + '/threads',
                "method": 'GET',
                "data": {active_id: that.activeThreadId}
            }).done(function (data) {
                if (data.activeThread) {
                    that.activeThreadId = data.activeThread.id;
                    that.enableMessageInputs();
                }
                that.renderThreads(data.threads);
                if (data.messages) {
                    that.renderInitMessages(data.messages)
                }
            }).fail(function (xhr) {
                alert('Rất tiếc, đã có lỗi tải tin nhắn :(');
            });
        },
        fetchMessagesOfThread: function (threadId) {
            var that = this;
            $.ajax({
                "url": conf.threadUrl + '/' + threadId,
                "method": 'GET',
            }).done(function (data) {
                that.renderInitMessages(data);
            }).fail(function (xhr) {
                alert('Rất tiếc, đã có lỗi tải tin nhắn :(');
            });
        },
        renderReceivedMessage: function (data) {
            var message = data.message.body;
            var threadId = data.message.thread_id;
            var $thread = $('li.contact[data-thread-id="' + threadId + '"]');
            // nếu chưa tồn tại thread và có dữ liệu của thread mới: khởi tạo item thread trong list thread
            if (!$thread.length && data.thread) {
                var thread = data.thread;
                // Tìm xem có thread tạm hay không
                var $tmpThread = $('li.contact[data-thread-user-org="' + data.user.id + ':' + thread.organization.id + '"]');
                if ($tmpThread.length) {
                    $thread = $tmpThread;
                    $thread.attr('data-thread-id', thread.id);
                    if ($tmpThread.hasClass('active')) {
                        this.activeThreadId = thread.id;
                    }
                }
                else {
                    // Nếu đây là thread đầu tiên, cho nó active
                    if (!$('li.contact').length) {
                        thread.class = 'active';
                        this.activeThreadId = thread.id;
                        this.enableMessageInputs();
                    }
                    $thread = $(this.renderThread(thread, data.user));
                    this.$threadList.prepend($thread);
                }
            }
            if ($thread) {
                if (threadId == this.activeThreadId) {
                    this.$chatHistoryList.append(this.renderMessage(data.message, data.user));
                    this.scrollToBottom();
                }
                else {
                    $thread.addClass('unread');
                    var unreadCount = 1 + parseInt($thread.attr('data-unread-count'), 10);
                    $thread.attr('data-unread-count', unreadCount)
                    $thread.find('.unread-count').html(unreadCount);
                }
                $thread.find('.preview').html('<span>' + (data.user.id == this.userId ? 'Bạn' : sdcApp.htmlEntities(data.user.name)) +
                    ': </span>' + sdcApp.htmlEntities(message));
            }
        },
        renderAddedMessage: function (data) {
            var $thread = $('li.contact.active');
            // Nếu thread đang active là thread tạm
            if (this.activeThreadId == 0 && data.isNewThread) {
                this.activeThreadId = data.thread.id;
                $thread.attr('data-thread-id', this.activeThreadId);
            }
            this.$chatHistoryList.append(this.renderMessage(data, data.user));
            $thread.find('.preview').html('<span>Bạn: </span>' + sdcApp.htmlEntities(data.body));
            this.scrollToBottom();
        },
        addMessage: function () {
            var that = this;
            var message = this.$textarea.val();
            if ($.trim(message) == '') {
                return false;
            }
            var newMessageData = {'message': message};
            // Trường hợp message đầu tiên của thread tạm giữa người dùng và org, cần truyền thêm org_id
            if (that.activeThreadId == 0) {
                newMessageData.org_id = conf.temporaryThread.organization_id;
            }
            $.ajax({
                "url": conf.baseUrl + '/threads/' + that.activeThreadId + '/messages',
                "method": 'POST',
                "data": newMessageData
            }).done(function (data) {
                that.renderAddedMessage(data);
                that.$textarea.val('');
            }).fail(function (xhr) {
                alert('Rất tiếc, đã có lỗi gửi tin nhắn :(');
            });
        },
        addMessageEnter: function (event) {
            // enter was pressed
            if (event.keyCode === 13) {
                this.addMessage();
            }
        },
        scrollToBottom: function () {
            // $(".messages").animate({scrollTop: this.$chatHistory[0].scrollHeight}, "fast");
            this.$chatHistory.scrollTop(this.$chatHistory[0].scrollHeight);
        },
        selectThread: function (e) {
            e.preventDefault();
            var that = e.data;
            $thread = $(this);
            if ($thread.hasClass('active')) {
                return;
            }
            that.activeThreadId = $thread.attr('data-thread-id');
            that.$threadList.find('li.active').removeClass('active');
            $thread.addClass('active');
            that.$activeThread = $thread;
            if (that.activeThreadId != 0) {
                $.ajax({
                    "url": conf.baseUrl + '/threads/' + that.activeThreadId + '/messages',
                    "method": 'GET',
                }).done(function (data) {
                    that.renderProfile($thread.find(".subject").html(), $thread.find("img").attr("src"));
                    that.renderInitMessages(data);
                    $thread.removeClass('unread');
                    $thread.attr('data-unread-count', 0);
                    $thread.find('.unread-count').html('');
                }).fail(function (xhr) {
                    alert('Rất tiếc, đã có lỗi tải tin nhắn :(');
                });
            }
            else {
                that.$chatHistoryList.empty();
            }
        },
        searchThread: function () {
            var filter, lis, subject;
            filter = this.$searchText.val().toUpperCase();
            lis = this.$threadList.children();
            lis.each(function() {
                var $this = $(this);
                subject = $this.find(".subject").html();
                if (subject.toUpperCase().indexOf(filter) > -1) {
                    $this.show();
                } else {
                    $this.hide();
                }
            });
        }
    };

    chat.init();
}
