if(window.$.fn.dataTable) {
    $.extend($.fn.dataTable.defaults, {
        "searching": false,
        "lengthChange": false,
        "processing": false,
        "serverSide": true,
        "orderCellsTop": true,
        "pageLength": 10,
        "sorting": [],
        "language": {
            "sEmptyTable": "Không có dữ liệu",
            "sProcessing": '<i class="fa fa-spin fa-spinner"></i>&nbsp; Đang xử lý...',
            "sLengthMenu": "Xem _MENU_ mục",
            "sZeroRecords": "Không tìm thấy kết quả nào phù hợp",
            "sInfo": "Đang xem _START_ đến _END_ trong tổng số _TOTAL_ mục",
            "sInfoEmpty": "Đang xem 0 đến 0 trong tổng số 0 mục",
            "sInfoFiltered": "(được lọc từ _MAX_ mục)",
            "sInfoPostFix": "",
            "sSearch": "Tìm:",
            "oPaginate": {
                "sFirst": "Đầu",
                "sPrevious": "Trước",
                "sNext": "Tiếp",
                "sLast": "Cuối"
            },
            "oAria": {
                "sSortAscending": ": kích hoạt để xếp cột này tăng dần",
                "sSortDescending": ": kích hoạt để xếp cột này giảm dần"
            },
        }
    });
}
var sdcApp = (function () {
    var loadingBlacklist = ['/open', '/get-product-cart'];
    var blockUITemplate =  '<div id="blockUI" class="loading-message loading-message-boxed">' +
        '<i class="fa fa-spin fa-spinner"></i><span>&nbsp;&nbsp;ĐANG TẢI...</span></div>';
    return {
        commonAjaxTblActionCell: '<div class="input-group"><div class="input-group-btn"><a href="#" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="glyphicon glyphicon-pencil"></i></a><a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a></div></div>',
        commonAjaxTblEdit: '<a href="#" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="glyphicon glyphicon-pencil"></i></a>',
        commonAjaxTblDel: '<a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>',
        showConfirmDelete: function (fn, itemName, title) {
            bootbox.confirm({
                title: title ? title : 'Xác nhận xóa',
                message: '<i class="fa fa-warning" style="color:red; font-size: 24px"></i> ' + ('Bạn có chắc chắn muốn xóa ' + (itemName !== undefined ? itemName : 'mục này') + '?'),
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Huỷ'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Xóa'
                    }
                },
                callback: function (result) {
                    if (result && fn) {
                        fn();
                    }
                }
            });
        },
        renderCommonTblActionCell: function (id, baseUrl) {
            return '<a href="' + baseUrl + '/' + id + '/edit" class="btn btn-link btn-xs"><i class="glyphicon glyphicon-edit"></i></a>' +
                '<a href="' + baseUrl + '/' + id + '" class="btn btn-link btn-xs btn-del"><i class="glyphicon glyphicon-trash"></i></a>';
        },
        bindTblDelEvent: function (tableId, oDataTable, baseUrl, nameColIndex) {
            $(tableId + ' tbody').on('click', '.btn-del', function (e) {
                e.preventDefault();
                var btn = $(this);
                var itemName;
                var id = oDataTable.row(btn.closest('tr')).id();
                if (nameColIndex !== undefined) {
                    itemName = btn.closest('tr').children().eq(nameColIndex).html();
                    itemName = '<strong>' + itemName + '</strong>';
                }
                sdcApp.showConfirmDelete(function () {
                    sdcApp.showLoading(true);
                    $.ajax({
                        url: baseUrl + '/' + id,
                        method: 'DELETE',
                    }).done(function (result) {
                        sdcApp.showLoading(false);
                        sdcApp.showToast(result);

                        oDataTable.ajax.reload(null, false);
                    }).fail(function () {
                        sdcApp.showLoading(false);
                        toastr.error('Lỗi hệ thống. Vui lòng thực hiện lại!');
                    });
                }, itemName);
            });
        },
        bindTblEditEvent: function (tableId, oDataTable, callback) {
            $(tableId + ' tbody').on('click', '.btn-edit', function (e) {
                e.preventDefault();
                var id = oDataTable.row($(this).closest('tr')).id();
                callback(id);
            });
        },
        buildEditUrl: function (baseUrl, id) {
            return baseUrl + '/' + id + '/edit';
        },

        //refer https://github.com/RyanSMurphy/Laravel-Bootstrap-Modal-Form/blob/master/src/laravel-bootstrap-modal-form.js
        resetFormErrors: function ($form) {
            $form.find('.form-group').removeClass('has-error');
            $form.find('.form-group').find('.error-block').remove();
        },
        renderFormErrors: function (errors, $form) {
            $.each(errors, function (field, message) {
                //console.error(field + ': ' + message);
                //handle arrays
                /*if(field.indexOf('.') != -1) {
                 field = field.replace('.', '[');
                 //handle multi dimensional array
                 for (i = 1; i <= (field.match(/./g) || []).length; i++) {
                 field = field.replace('.', '][');
                 }
                 field = field + "]";
                 }*/
                var $error = $('<p class="help-block error-block">' + message[0] + '</p>');
                var element = $form.find('[name=' + field + ']');
                var inputGroup = element.parent('.input-group');
                element.closest('.form-group').addClass('has-error');
                // nếu thuộc formgroup, insert msg sau thẻ div .input-group
                if (inputGroup.length) {
                    $error.insertAfter(inputGroup);
                }
                // nếu được cấu hình dùng select2, insert msg sau điều khiển select2
                else if (element.hasClass('select2-hidden-accessible')) {
                    $error.insertAfter(element.next());
                }
                // insert msg ngay sau element
                else {
                    $error.insertAfter(element);
                }
            });
        },
        getFormDataAndType: function ($form) {
            var fileElements = $form.find('[type=file]'),
                contentType = false,
                data = null;
            // Check for file inputs. If found, prepare submission via FormData object.
            if (fileElements.length) {
                var inputs = $form.serializeArray();
                data = new FormData();

                // Append input to FormData object.
                $.each(inputs, function (index, input) {
                    data.append(input.name, input.value);
                });

                // Append files to FormData object.
                $.each(fileElements, function (index, input) {
                    for (var i = 0; i < input.files.length; i++) {
                        data.append(input.name, input.files[i]);
                    }
                });
            }
            // If no file input found, do not use FormData object (better browser compatibility).
            else {
                data = $form.serialize();
                contentType = 'application/x-www-form-urlencoded; charset=UTF-8';
            }

            return {
                data: data,
                contentType: contentType
            };
        },
        showLoading: function (isShow) {
            return; // TODO rà soát để xem có thể bỏ hẳn hàm này
            if (isShow)
                $('.loading_show').css('display', 'block');
            else
                $('.loading_show').css('display', 'none');
        },
        // Fix toolbox của CKEditor không nhận focus
        fixCKEditorModal: function () {
            $(document).on('focusin', function (e) {
                if ($(e.target).closest(".cke_dialog").length) {
                    e.stopImmediatePropagation();
                }
            });
        },
        // showModal: function (title = 'Thông báo', msg = 'Xuất hiện lỗi', size = 'small') {
        //     var dialog = bootbox.dialog({
        //         size: size,
        //         title: title,
        //         message: msg,
        //         buttons: {
        //             cancel: {
        //                 label: "Thoát"
        //             }
        //         }
        //     });
        //     dialog.modal('show');
        // },
        showToast: function (result) {
            if (result.status !== undefined && result.msg !== undefined) {
                switch (result.status) {
                    case 'success':
                        toastr.success(result.msg);
                        break;
                    case 'warning':
                        toastr.warning(result.msg);
                        break;
                    case 'error':
                        toastr.error(result.msg);
                        break;
                }
            }

        },
        /**
         * Format cột date
         * @param toFormat theo định dạng của thư viện Moment. Mặc định (không truyền tham số) DD/MM/YYYY.
         * Có thể truyền giá trị định dạng tắt: dst (date-short-time định dạng HH:mm DD/MM/YYYY), dft (date-full-time định dạng HH:mm:ss DD/MM/YYYY)
         * @returns {function(data, type, row): *}
         */
        renderDataTableDate: function (toFormat) {
            if ( arguments.length === 0 ) {
                toFormat = 'DD/MM/YYYY';
            }
            if (toFormat === 'dst') {
                toFormat = 'HH:mm DD/MM/YYYY';
            }
            if (toFormat === 'dft') {
                toFormat = 'HH:mm:ss DD/MM/YYYY';
            }
            return function ( d, type, row ) {
                var m = window.moment( d, 'YYYY-MM-DD HH:mm:ss', 'vi', true );

                // Order and type get a number value from Moment, everything else
                // sees the rendered value
                return m.format( type === 'sort' || type === 'type' ? 'x' : toFormat );
            };
        },
        renderDataTableNumber: function (precision, postfix, prefix, thousands, decimal) {
            thousands = thousands === undefined ? '.' : thousands;
            decimal = decimal === undefined ? ',' : decimal;
            return $.fn.dataTable.render.number(thousands, decimal, precision, prefix, postfix);
        },
        isNotInLoadingBlacklist: function(url){
            for (var i = 0; i < loadingBlacklist.length; i++){
                if (url.search(new RegExp(loadingBlacklist[i])) >= 0) {
                    return false;
                }
            }
            return true;
        },
        blockUI: function () {
            if ($('#blockUI').length === 0) {
                $.blockUI({
                    fadeIn: 0,
                    baseZ: 99999,
                    css: {
                        border: '0',
                        padding: '0',
                        backgroundColor: 'none'
                    },
                    overlayCSS:  {
                        backgroundColor: '#000',
                        opacity: 0.08
                    },
                    message: blockUITemplate
                });
            }
        },
        unblockUI: function () {
            $.unblockUI({fadeOut: 200});
        },
        htmlEntities(str) {
            return String(str)
                .replace(/&/g, '&amp;')
                .replace(/"/g, '&quot;')
                .replace(/'/g, '&#39;')
                .replace(/</g, '&lt;')
                .replace(/>/g, '&gt;');
        }
    };
})();

// document is ready
$(function () {
    // Thiết lập jquery ajax
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    $(document).ajaxSend(function(event, jqXHR, ajaxOptions) {
        if (sdcApp.isNotInLoadingBlacklist(ajaxOptions.url)) {
            sdcApp.blockUI();
        }
    }).ajaxStop(function(event, request) {
        sdcApp.unblockUI();
    }).ajaxError(function(event, jqXHR, ajaxSettings, thrownError) {
        // if(jqXHR.status === 403){
        //     window.location.reload();
        // }
        //else{
        //    $.unblockUI();
        //jAlert("Đã có lỗi trong quá trình kết nối tới máy chủ :(", 'Cảnh báo');
        //}
    });

    /*Cau hinh upload ảnh cho CKEditor*/
    if (window.CKEDITOR != null) {
        var token = $('meta[name="csrf-token"]').attr('content');
        //Path upload image
        CKEDITOR.config.filebrowserImageUploadUrl = '/upload/image-cke?opener=ckeditor&type=images&_token=' + token;
        //Path upload file cho liên kết
        CKEDITOR.config.filebrowserUploadUrl = '/upload/file-cke?opener=ckeditor&type=file&_token=' + token;
    }

    // Thiết lập toast notification
    if (window.toastr) {
        toastr.options = {
            "closeButton": true,
            "positionClass": "toast-bottom-right"
        }
    }
    // Thiết lập bootstrap datepicker
});
