/**
 * Hỗ trợ tạo chức năng CRUD đơn giản
 * @param baseUrl Url của index
 * @param tableId Id của table, ví dụ #table (bao gồm # nếu là id, . nếu là class...)
 * @param tableConf Cấu hình datatable
 * @param searchFormId Có thể null, Id của form search, ví dụ #search-form (bao gồm # nếu là id, . nếu là class...)
 * @param searchFieldNames Có thể null, mảng các trường trên form search, ví dụ ['keywork', 'category_id']
 * @param btnAddId Id button add
 * @param titleCreate Tiêu đề form tạo mới
 * @param titleUpdate Tiêu đề form cập nhật
 * @param modalSize Có thể truyền vào hoặc không, Kích thước của modal: medium, small, large
 */
CommonCRUD = function (baseUrl, tableId, tableConf, searchFormId, searchFieldNames, btnAddId, titleCreate, titleUpdate, modalSize) {
    var conf = {
        baseUrl: baseUrl,
        tableId: tableId,
        tableConf: tableConf,
        searchFormId: searchFormId,
        searchFieldNames: searchFieldNames,
        btnAddId: btnAddId,
        titleCreate: titleCreate,
        titleUpdate: titleUpdate,
        modalSize: modalSize,
        nameColIndex: 0,
        //beforeSerialize: function($form){}
        //requestSaveDoneHandler: function(data){console.log(data)},
        //requestSaveFailHandler: function(xhr){console.log(xhr)},
        //requestDeleteDoneHandler: function(data){console.log(data)},
        //requestDeleteFailHandler: function(xhr){console.log(xhr)},
        //formShowedHandler: function($form){console.log($form)},
        //storeHandler: function(){console.log('storeHandler'); return false},
        //updateHandler: function(){console.log('updateHandler'); return false},
        // buttons: {
        //     cancel: {
        //         label: "Hủy"
        //     },
        //     ok: {
        //         label: "Lưu",
        //         callback: function () {
        //             okHandler(editItemId);
        //             return false;
        //         }
        //     }
        // },
        // bindRowActionHandlers: function () {
        //     sdcApp.bindTblDelEvent(conf.tableId, oTable, conf.baseUrl, conf.nameColIndex);
        //     sdcApp.bindTblEditEvent(conf.tableId, oTable, function (id) {
        //         var editUrl = conf.baseUrl + '/' + id + '/edit';
        //         editItemId = id;
        //         okHandler = update;
        //         showForm(conf.titleUpdate, editUrl);
        //     });
        // }
        // bindDefaultEditAction: true,
        // bindDefaultDeleteAction: true,
    };
    return CommonCRUD2(conf);
};

/**
 * Thiết lập javascirpt, ajax cho chức năng CRUD dựa trên tham số cấu hình đưa vào
 * @param conf  Object cấu hình. Trong đó:
 * baseUrl:     (String)    Url của Index
 tableId:       (Sring)     (Bắt buộc)    Id của table, ví dụ #table (bao gồm # nếu là id, . nếu là class...)
 tableConf:     (Object)    (Bắt buộc)    Cấu hình datatable. Cột action nếu không dùng render mặc định truyền bindRowActions để gán sự kiên nếu cần
 searchFormId:  (String)    Id của form search, ví dụ #search-form (bao gồm # nếu là id, . nếu là class...); Có thể null,
 searchFieldNames:  (Array) Mảng các trường trên form search, ví dụ ['keywork', 'category_id'], Có thể null,
 btnAddId:      (String)    Id của button thêm mới, có thể null
 titleCreate:   (String)    Tiêu đề form thêm mới,
 titleUpdate:   (String)    Tiêu đề form cập nhật,
 modalSize:     (String)    Kích thước form modal, nhận các giá trị medium|large|small,
 nameColIndex:  (int)       Thứ tự cột dùng làm tên của item khi xác nhận xóa,
 createRequestData          (function(){... return data}) Hàm tạo dữ liệu (theo chuẩn định dạng của jquery ajax data) cần gửi về máy chủ trong request create
 beforeSerialize            (function($form)) Hàm xử lý sự kiện trước khi form được serialize
 requestSaveDoneHandler:    (function(data)) Hàm xử lý sự kiện khi request ajax lưu done
 requestSaveFailHandler:    (function(xhr))  Hàm xử lý sự kiện khi request ajax lưu fail,
 requestDeleteDoneHandler:  (function(data)) Hàm xử lý sự kiện khi request ajax xóa done
 requestDeleteFailHandler:  (function(xhr)) Hàm xử lý sự kiện khi request ajax xóa fail
 formShowedHandler:         (function($form)) Hàm xử lý sự kiện khi form đã hiển thị
 storeHandler:              (function())    Hàm xử lý cho sự kiện store (bấm OK khi thêm mới), viết hàm xử lý và truyền vào khi cần tùy biến việc xử lý sự kiện này
 updateHandler:             (function(itemId))    Hàm xử lý cho sự kiện update (bấm OK khi cập nhật), viết hàm xử lý và truyền vào khi cần tùy biến việc xử lý sự kiện này
 buttons:                   {Object}    Các button của modal, xem tài liệu của bootboxjs để biết thêm chi tiết.
 bindRowActionHandlers:     (function(tableId, oTable, baseUrl, nameColIndex)) Hàm xử lý bind sự kiện cho các button trên table
 bindDefaultEditAction: (bool) False: không Gọi hàm bind sự kiện edit mặc định. Mặc định true
 bindDefaultDeleteAction: (bool) False: không Gọi hàm bind sự kiện delete mặc định. Mặc định true
 * @returns {{oTable: *, form: *, dialog: *, editItemId: editItemId, reloadTable: reloadTable, reloadTablePage: reloadTablePage}}
 * @constructor
 */
CommonCRUD2 = function (conf) {
    conf = conf || {};
    var oTable;
    var dialog;
    var form;
    var okHandler;
    var editItemId = 0;

    var store = function () {
        sdcApp.showLoading(true);
        if (conf.beforeSerialize) {
            conf.beforeSerialize(form);
        }
        var formData = sdcApp.getFormDataAndType(form);
        $.ajax({
            "url": conf.baseUrl,
            "method": 'POST',
            "data": formData.data,
            "contentType": formData.contentType,
            "processData": formData.contentType !== false
        }).done(function (data) {
            dialog.modal('hide');
            reloadTable();
            if (conf.requestSaveDoneHandler) {
                conf.requestSaveDoneHandler(data);
            }
        }).fail(function (xhr) {
            if (xhr.status === 422) {
                sdcApp.resetFormErrors(form);
                var errors = xhr.responseJSON.errors;
                sdcApp.renderFormErrors(errors, form);
            }
            if (conf.requestSaveFailHandler) {
                conf.requestSaveFailHandler(xhr);
            }
        });
    };

    var update = function (id) {
        sdcApp.showLoading(true);
        if (conf.beforeSerialize) {
            conf.beforeSerialize(form);
        }
        var formData = sdcApp.getFormDataAndType(form);
        $.ajax({
            "url": conf.baseUrl + '/' + id,
            "method": 'POST',
            "data": formData.data,
            "contentType": formData.contentType,
            "processData": formData.contentType !== false
        }).done(function (data) {
            dialog.modal('hide');
            reloadTablePage();
            if (conf.requestSaveDoneHandler) {
                conf.requestSaveDoneHandler(data);
            }

        }).fail(function (xhr) {
            if (xhr.status === 422) {
                sdcApp.resetFormErrors(form);
                var errors = xhr.responseJSON.errors;
                sdcApp.renderFormErrors(errors, form);
            }
            if (conf.requestSaveFailHandler) {
                conf.requestSaveFailHandler(xhr);
            }

        });
    };

    var saveDoneHandler = function (result) {
        sdcApp.showToast(result);
        sdcApp.showLoading(false);
    };

    var saveFailHandler = function (result) {
        sdcApp.showLoading(false);
        if (result.status == 422)
            toastr.error('Vui lòng nhập đủ thông tin yêu cầu!');
        else
            toastr.error('Lỗi hệ thống. Vui lòng thực hiện lại!');
    };

    var defaultConf = {
        //baseUrl: '',
        //tableId: '',
        //tableConf: {},
        //searchFormId: '',
        //searchFieldNames: '',
        //btnAddId: '',
        titleCreate: 'Thêm mới',
        titleUpdate: 'Cập nhật',
        modalSize: 'medium',
        nameColIndex: 0,
        requestSaveDoneHandler: saveDoneHandler,
        requestSaveFailHandler: saveFailHandler,
        //requestDeleteFailHandler: function(){},
        //formShowedHandler: function(form){},
        storeHandler: store,
        updateHandler: update,
        buttons: {
            cancel: {
                label: "Hủy"
            },
            ok: {
                label: "Lưu",
                callback: function () {
                    okHandler(editItemId);
                    return false;
                }
            }
        },
        bindDefaultEditAction: true,
        bindDefaultDeleteAction: true
    };
    // Merge config
    for (var key in defaultConf) {
        if (conf[key] === undefined) {
            conf[key] = defaultConf[key];
        }
    }
    //conf = defaultConf;

    // var bindDefaultRowActionHandlers = function (tableId, oTable, baseUrl, nameColIndex) {
    //     sdcApp.bindTblDelEvent(tableId, oTable, baseUrl, nameColIndex);
    //     sdcApp.bindTblEditEvent(tableId, oTable, function (id) {
    //         var editUrl = conf.baseUrl + '/' + id + '/edit';
    //         editItemId = id;
    //         okHandler = conf.updateHandler;
    //         showForm(conf.titleUpdate, editUrl);
    //     });
    // };

    var showForm = function (title, url) {
        $.ajax({
            url: url,
            data: editItemId === 0 && conf.createRequestData ? conf.createRequestData() : null
        }).done(function (data) {
            dialog = bootbox.dialog({
                size: conf.modalSize,
                title: title,
                message: data,
                buttons: conf.buttons
            });
            form = dialog.find('form').first();
            if (conf.formShowedHandler) {
                dialog.on("shown.bs.modal", function () {
                    conf.formShowedHandler(form);
                });
            }
        });
    };

    var reloadTable = function () {
        oTable.draw();
    };
    var reloadTablePage = function () {
        oTable.ajax.reload(null, false);
    };

    // Nếu có form tìm kiếm, gắn sự kiện summit và lấy mảng item các field
    if (conf.searchFormId) {
        var searchForm = $(conf.searchFormId);
        // Có thể có khoặc không có (nút tìm kiếm chỉ có tác dụng refesh) field cần tìm
        // Nếu có, lấy mảng jQuery element
        if (conf.searchFieldNames) {
            var searchElements = [];
            for (var i = 0; i < conf.searchFieldNames.length; i++) {
                searchElements[i] = searchForm.find('[name="' + conf.searchFieldNames[i] + '"]');
            }
        }
        searchForm.on('submit', function (e) {
            e.preventDefault();
            reloadTable();
        });
    }
    conf.tableConf.ajax = {
        "url": conf.baseUrl + '/data',
        "data": function (d) {
            if (conf.searchFieldNames) {
                for (var i = 0; i < conf.searchFieldNames.length; i++) {
                    d[conf.searchFieldNames[i]] = searchElements[i].val();
                }
            }
        }
    };

    oTable = $(conf.tableId).DataTable(conf.tableConf);
    if (conf.bindDefaultDeleteAction) {
        sdcApp.bindTblDelEvent(conf.tableId, oTable, conf.baseUrl, conf.nameColIndex);
    }
    if (conf.bindDefaultEditAction) {
        sdcApp.bindTblEditEvent(conf.tableId, oTable, function (id) {
            var editUrl = conf.baseUrl + '/' + id + '/edit';
            editItemId = id;
            okHandler = conf.updateHandler;
            showForm(conf.titleUpdate, editUrl);
        });
    }

    if (conf.btnAddId) {
        $(conf.btnAddId).click(function (e) {
            e.preventDefault();
            editItemId = 0;
            okHandler = conf.storeHandler;
            showForm(conf.titleCreate, conf.baseUrl + '/create')
        });
    }

    return {
        oTable: oTable,
        form: form,
        dialog: dialog,
        editItemId: editItemId,
        reloadTable: reloadTable,
        reloadTablePage: reloadTablePage
    };
};
