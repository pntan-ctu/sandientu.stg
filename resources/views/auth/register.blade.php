@extends('layouts.web')

@section('title')
Đăng ký thành viên
@endsection

@section('content')

<div class="wrap-page">
    <div class="register-bg">
        <!-- <div class="block-title-h1">
            <div class="container"><h1>Đăng ký</h1></div>
        </div> -->
        <div class="container">
            <div class="col-md-offset-2 col-md-8">
                <div class="login register">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="left-form-register">
                                <div class="dk-tit">Đăng ký</div>
                                <p>Đăng ký để theo dõi đơn hàng, lưu
                                    danh sách sản phẩm yêu thích, nhận
                                    nhiều ưu đãi hấp dẫn.
                                </p>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="right-form-register">
                                <div class="login-title row">
                                    <h3>Đăng ký thành viên</h3>
                                    <div class="login-other"><span>Bạn đã là thành viên? <a class="btn-register" href="{{ route('login') }}">Đăng nhập</a> tại đây</span></div>
                                </div>
                                <form id="form_register" class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
                                    @csrf
                                    <div class="mod-regis-cs">

                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Họ tên:</label>
                                            <div class="col-sm-9">
                                                <input id="name" type="text" class="input-lg-1 form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}" required autofocus>

                                                @if ($errors->has('name'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <!-- <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Điện thoại:</label>
                                            <div class="col-sm-9">
                                                <input id="tel" type="phone" class="input-lg-1 form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" value="{{ old('tel') }}" required>

                                                @if ($errors->has('tel'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('tel') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div> -->

                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Email:</label>
                                            <div class="col-sm-9">
                                                <input id="email" type="email" class="input-lg-1 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                                @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Mật khẩu:</label>
                                            <div class="col-sm-9">
                                                <input id="password" type="password" class="input-lg-1 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                @if ($errors->has('password'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('password') }}</strong>
                                                </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Nhập lại mật khẩu:</label>
                                            <div class="col-sm-9">
                                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label for="" class="col-sm-3 control-label">Xác thực:</label>
                                            <div class="col-sm-9">
                                                <div class="g-recaptcha" data-sitekey="{{config('app.google_nocaptcha_sitekey')}}"></div>

                                                @if ($errors->has('g-recaptcha-response'))
                                                <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                                </span>
                                                @endif

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-9">
                                                <div class="checkbox"><label> <input id="checkbox" type="checkbox" name="checkbox" {{ old('checkbox') ? 'checked' : '' }} required> Tôi đồng ý với <a href="{{ url('about/dieu-khoan-tham-gia')}}"> điều khoản sử dụng và chính sách của phần mềm</a>. </label></div>

                                                {{--@if ($errors->has('checkbox'))
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $errors->first('checkbox') }}</strong>
                                                </span>
                                                @endif--}}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-sm-offset-3 col-sm-6">
                                                <button type="submit" class="btn-lg next-btn-primary">ĐĂNG KÝ</button>
                                            </div>
                                        </div>
                                        <div class="mod-login-third">
                                            <div class="mod-third-party-login">
                                                <div class="mod-third-party-login-line">
                                                    <span>Hoặc kết nối với tài khoản mạng xã hội</span>
                                                </div>
                                                <div class="mod-third-party-login-bd row">
                                                    <div class="col-sm-6"><a style="color: #fff;" href="redirect/facebook" class="mod-button mod-button mod-third-party-login-btn mod-third-party-login-fb">Facebook</a></div>
                                                    <div class="col-sm-6"><a style="color: #fff;" href="redirect/google" class="mod-button mod-button mod-third-party-login-btn mod-third-party-login-google">Google</a></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection