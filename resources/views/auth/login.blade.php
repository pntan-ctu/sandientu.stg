@extends('layouts.web')

@section('title')
Đăng nhập
@endsection

@section('content')
<div class="wrap-page">
    <div class="register-bg">
        <!-- <div class="block-title-h1">
            <div class="container"><h1>Đăng nhập</h1></div>
        </div> -->
        <div class="container">
            <div class="row">
                <div class="col-md-offset-2 col-md-8">
                    <div class="login register">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="left-form-login">
                                    <div class="dn-tit">Đăng nhập</div>
                                    <p>Đăng nhập để theo dõi đơn hàng, lưu 
                                    danh sách sản phẩm yêu thích, nhận
                                    nhiều ưu đãi hấp dẫn.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-8">   
                                <div class="right-form-login">
                                    <div class="login-title row">
                                        <div class=""><h3>Đăng nhập</h3></div>
                                        <div class="login-other"><span>Bạn chưa là thành viên? <a class="btn-register" href="{{ route('register') }}">Đăng ký</a> tại đây</span></div>
                                    </div>
                                    <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
                                        @csrf
                                        <div class="mod-regis-cs">
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Địa chỉ email:</label>
                                                <div class="col-sm-9">

                                                    <input id="email" type="email" class="input-lg-1 form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required autofocus>

                                                    @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif

                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label for="" class="col-sm-3 control-label">Mật khẩu:</label>
                                                <div class="col-sm-9">
                                                    <input id="password" type="password" class="input-lg-1 form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                                                    @if ($errors->has('password'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('password') }}</strong>
                                                    </span>
                                                    @endif
                                                </div>
                                            </div>
                                            
                                            <div class="forgot-password">
                                                <a class="btn user-link" href="{{ route('password.request') }}">
                                                    {{ __('Quên mật khẩu?') }}
                                                </a>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-sm-offset-3 col-sm-6">
                                                    <button type="submit" class="btn-lg next-btn-primary">ĐĂNG NHẬP</button>
                                                </div>
                                            </div>
                                            <div class="mod-login-third">
                                            <div class="mod-third-party-login">
                                                <div class="mod-third-party-login-line">
                                                    <span>Hoặc kết nối với tài khoản mạng xã hội</span>
                                                </div>
                                                <div class="mod-third-party-login-bd row">
                                                    <div class="col-sm-6"><a style="color: #fff;" href="redirect/facebook" class="mod-button mod-button mod-third-party-login-btn mod-third-party-login-fb">Facebook</a></div>
                                                    <div class="col-sm-6"><a style="color: #fff;" href="redirect/google" class="mod-button mod-button mod-third-party-login-btn mod-third-party-login-google">Google</a></div>
                                                </div>
                                            </div>
                                        </div>
                                        </div>
                                    </form>  
                                </div>
                            </div>
                        </div>        
                    </div>    
                </div>	 
            </div>    
        </div>
    </div>
</div>
@endsection
