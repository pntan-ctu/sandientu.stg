@extends('layouts.web')

@section('title')
Xác thực
@endsection

@section('content')
    <style>
        .card-body { padding: 10px; box-sizing: border-box;}
        .card-body a{ text-decoration: underline; color: cornflowerblue;}
    </style>
    <div class="wrap-page">
        <div class="register-bg">
            <div class="container">
                <div class="col-md-offset-1 col-md-10">
                    <div class="login register">
                        <div class="login-title row col-md-12">
                            <div class="col-md-6"><h3>{{ __('Verify Your Email Address') }}</h3></div>
                        </div>
                        <div class="col-md-offset-0 card-body">
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                </div>
                            @endif

                            {{ __('Before proceeding, please check your email for a verification link.') }}
                            {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                        </div>
                        <br>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
