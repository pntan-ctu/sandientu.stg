@extends('layouts.web')

@section('title')
Đặt lại mật khẩu
@endsection

@section('content')
    <style>
        .text-center{ text-align: center !important;}
        .card-body{ padding: 10px}
    </style>
    <div class="wrap-page">
        <div class="register-bg">
            <!--       <div class="block-title-h1">
                        <div class="container"><h1>Đăng nhập</h1></div>
                    </div>-->
            <div class="container">
                <div class="row">
                    <div class="col-md-offset-2 col-md-8">
                        <div class="login register reset">
                            <div class="login-title row">
                                <div class="col-md-6"><h3>{{ __('Reset Password') }}</h3></div>
                                <div class="col-md-6 login-other"><span>Quay lại <a class="btn-register" href="{{ route('login') }}">Đăng nhập</a></span></div>
                            </div>
                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form method="POST" action="{{ route('password.email') }}" class="form-horizontal mod-regis-cs">
                                    @csrf
                                    <div class="form-group row">
                                        <label for="" class="text-center col-sm-3 control-label">{{ __('E-Mail Address') }}</label>
                                        <div class="col-sm-9">
                                            <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>

                                            @if ($errors->has('email'))
                                                <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('email') }}</strong>
                                        </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-sm-offset-3 col-sm-6">
                                            <button type="submit" class="btn-lg next-btn-primary">
                                                {{ __('Send Password Reset Link') }}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
        </div>
    </div>
@endsection
