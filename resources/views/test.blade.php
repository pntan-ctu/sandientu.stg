<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
 
	<title>Laravel Multi Uploading Example</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" 		integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
 
	<!-- Optional theme -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" 		integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">
 
	<!-- Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">

    <script src="{!! asset('js/jquery-2.2.0.min.js') !!}"></script>

	<!-- Styles -->
	<style>
		.container {
			margin-top:2%;
		}
	</style>
</head>
<body>
	<button id="btnOK">OK</button>
    <script>
		$("#btnOK").click(function(){
            var objInfo = {'introduction_info': [
                {
                    "key": "chung-nhan-chat-luong",
                    "content": "<p>Thông tin chứng nhận chất lượng</p>"
                },
                {
                    "key": "gioi-thieu",
                    "content": "<p align=\"justify\"><strong>Nước mắm Thanh Hương thượng hạng</strong>&nbsp;nổi tiếng cả nước bởi hương vị đặc trưng m&agrave; kh&ocirc;ng loại nước mắm n&agrave;o c&oacute; được. Thường những ai đi du lịch, hay gh&eacute; qua xứ Thanh, đều mua v&agrave;i l&iacute;t về l&agrave;m qu&agrave;. Ch&iacute;nh v&igrave; thế, chất lượng cũng như thương hiệu nước mắm Thanh Hương đang được người ti&ecirc;u d&ugrave;ng đ&aacute;nh gi&aacute; cao.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p align=\"justify\">Để c&oacute; được loại nước mắm thơm ngon, quy tr&igrave;nh chế biến cũng như chọn nguy&ecirc;n liệu rất chặt chẽ. Trước ti&ecirc;n, phải chọn loại c&aacute; nhiều đạm (chủ yếu l&agrave; c&aacute; cơm, nục v&agrave; tr&iacute;ch). Tiếp đ&oacute;, nguy&ecirc;n liệu phải được phơi, trộn muối cẩn thận, sau đ&oacute; chuyển về chượp ch&iacute;n đ&uacute;ng thời gian. Thời gian ng&acirc;m ủ, chượp rất quan trọng, nếu qu&aacute; ch&iacute;n sẽ sẫm m&agrave;u, dẫn đến nước đen, kh&ecirc; kh&eacute;t; nếu d&ugrave;ng sớm th&igrave; đạm &iacute;t, c&oacute; m&ugrave;i tanh v&agrave; hương k&eacute;m, ch&iacute;nh v&igrave; thế phải duy tr&igrave; đ&uacute;ng thời gian, nhiệt v&agrave; phải ướp c&aacute; theo tỷ lệ 4 c&aacute;/1 muối (tức 4kg c&aacute;/kg muối) v&agrave; 1kg c&aacute; chỉ lấy tối đa 0,7 l&iacute;t nước mắm.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p align=\"justify\">Điều đặc biệt v&agrave; b&iacute; quyết th&agrave;nh c&ocirc;ng l&agrave; nước mắm Thanh Hương kh&ocirc;ng sử dụng chất điều vị m&agrave; tận dụng sự tinh khiết của nguy&ecirc;n liệu c&aacute;. &ocirc;ng Nguyễn B&aacute; Thang, Gi&aacute;m đốc C&ocirc;ng ty khẳng định: &ldquo;Nước mắm Thanh Hương kh&ocirc;ng bao giờ sử dụng chất điều vị v&igrave; sẽ ảnh hưởng đến m&ugrave;i vị của sản phẩm. Ch&iacute;nh điều n&agrave;y đ&atilde; gi&uacute;p C&ocirc;ng ty lu&ocirc;n giữ vững thương hiệu&rdquo;. &Ocirc;ng Thang cho biết th&ecirc;m: &ldquo;Nước mắm Thanh Hương được kết hợp giữa c&ocirc;ng nghệ truyền thống v&agrave; hiện đại. C&aacute; được mua từ c&aacute;c v&ugrave;ng biển như Phan Thiết (B&igrave;nh Thuận), Nha Trang (Kh&aacute;nh H&ograve;a), đặc biệt l&agrave; Ninh Thuận, nơi c&oacute; c&aacute; ngon, nhiều đạm, tuy mua với gi&aacute; cao gần 3 lần nhưng v&igrave; chất lượng sản phẩm, ch&uacute;ng t&ocirc;i vẫn nhập. Một năm C&ocirc;ng ty sử dụng 5.000-6.000 tấn c&aacute; nguy&ecirc;n liệu&rdquo;.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p align=\"justify\">Hiện nay, c&ocirc;ng ty đ&atilde; c&oacute; hơn 400 đại l&yacute; ti&ecirc;u thụ sản phẩm tại Thanh H&oacute;a v&agrave; c&aacute;c tỉnh, th&agrave;nh phố ph&iacute;a Bắc như: H&agrave; Nội, Vĩnh Ph&uacute;c, H&ograve;a B&igrave;nh, Hải Dương&hellip; Để giữ vững thương hiệu v&agrave; đứng vững tr&ecirc;n thị trường, c&ocirc;ng ty đ&atilde; đầu tư h&agrave;ng chục tỷ đồng x&acirc;y dựng cơ sở hạ tầng v&agrave; d&acirc;y chuyền sản xuất, n&acirc;ng cao chất lượng sản phẩm. Năm 2016, c&ocirc;ng ty đ&atilde; xuất b&aacute;n ra thị trường tr&ecirc;n 6,2 triệu l&iacute;t nước mắm, doanh thu đạt hơn 85 tỷ đồng, giải quyết việc l&agrave;m cho tr&ecirc;n 60 c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n với mức thu nhập ổn định 6,4 triệu đồng/người/th&aacute;ng, nộp ng&acirc;n s&aacute;ch Nh&agrave; nước hơn 12 tỷ đồng.</p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p align=\"justify\">Trải qua nhiều thăng trầm, hiện nay C&ocirc;ng ty Thanh Hương c&oacute; địa chỉ mới tại Cầu Gh&eacute;p (huyện Tĩnh Gia) v&agrave; l&agrave; đơn vị đầu ti&ecirc;n của tỉnh Thanh Ho&aacute; được chuyển đổi từ doanh nghiệp Nh&agrave; nước sang cổ phần ho&aacute;. H&agrave;ng năm, C&ocirc;ng ty đều nhận được cờ thi đua, bằng khen của tỉnh, c&aacute;c bộ ng&agrave;nh v&agrave; Ch&iacute;nh phủ. Cụ thể, năm 2002, 2007 C&ocirc;ng ty đ&atilde; được đ&oacute;n nhận Hu&acirc;n chương Lao động hạng Ba v&agrave; hạng Hai; năm 2003 nhận C&uacute;p v&agrave;ng giải thưởng chất lượng Việt của Bộ Khoa học v&agrave; C&ocirc;ng nghệ. Năm 2007, thương hiệu nước mắm Thanh Hương ch&iacute;nh thức được Cục Sở hữu tr&iacute; tuệ cấp đăng k&yacute; độc quyền . Những giải thưởng n&agrave;y sẽ l&agrave; nguồn động vi&ecirc;n để c&aacute;n bộ, c&ocirc;ng nh&acirc;n vi&ecirc;n C&ocirc;ng ty Nước mắm Thanh Hương ng&agrave;y c&agrave;ng giữ vững uy t&iacute;n, sản xuất loại nước chấm thơm ngon, m&atilde;i m&atilde;i gần gũi trong bữa ăn h&agrave;ng ng&agrave;y của người ti&ecirc;u d&ugrave;ng Việt.</p>"
                },
                {
                    "key": "san-pham",
                    "content": "<p>Thông tin chứng nhận sản phẩm </p>"
                }
            ]};
            console.log(objInfo.introduction_info);

            var api_key = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6ImFhMGM2NzdiYWVhNDM5ZGMzNDczYjI1NjIxNWE3Mzg4NjMwNWU1NmE3MThmNDIwMDBhNTJmMTdhZjEyZDAxMzI2YTZmNjViMTRhNzNkMTYyIn0.eyJhdWQiOiI5IiwianRpIjoiYWEwYzY3N2JhZWE0MzlkYzM0NzNiMjU2MjE1YTczODg2MzA1ZTU2YTcxOGY0MjAwMGE1MmYxN2FmMTJkMDEzMjZhNmY2NWIxNGE3M2QxNjIiLCJpYXQiOjE1NDg3MjQ3NzEsIm5iZiI6MTU0ODcyNDc3MSwiZXhwIjoxNTgwMjYwNzcxLCJzdWIiOiI2MiIsInNjb3BlcyI6W119.BfbC2BdJEMxBdXuvsrxWN6sl8zN7uR_wU0b7wghEL3e2ghkLl9V60Lyt93Q82iMeow3DwzjNq9pg3WMT5ZV2d9gaGUSbXpuV5wXt4cPL1JbhG_gVGaugKtzKwbO9thrUq_ADKMGJ4veicBiRRAc-A4Zr-4GOskTHuA8I7LdreP898NllBRkhPStEaZslpxoRA4KpNhmj2jOn8c__SPiU_S3NUxrcbrNeiYwwW4nfm2VUXiK4iTT_VJdvNCFusEUigfKAIoDd9Ui_-5olzKRLQ-V1sMQG0NwkLQihIqudLYAo9G8Z_A00oOTfxwax01Mreke74XqCx3gbOQTqg7moNK2vlbYjpwKgfpcD9wFRcJ2pIPZBJ_DMEhNJJFO4z7XRj6czbjQQskcMVl-UG7HjCEfryXtcxZP7rJpx5jTmaDRSbWnwg76PopgR2tadsuIg4ejOLo8xTdASMqddbl6ushIPhsPjPK-OPXH7hhm_D2F7Wdw1A6kIhVAAXcwh5OUVtTqzVIavjemLLyO-cusMNU6MDEKrfPFiBS7VVRl3BrwIjvwrAyLLlnGrECpvz-DpMsW54XnguGxtM9sQc4iNoOJFEcSjHAG9q8RlcEaV9rlnWeUz1wpnafSSumkv2A6ex3r-jGzthyrqm08wxm_blN6dyoct1kT1FH_uA_RDgVw';

            fetch('http://attp.vn/api/organization/58/saveIntroductionInfo', {
                method: "POST", // *GET, POST, PUT, DELETE, etc.
                mode: "cors", // no-cors, cors, *same-origin
                cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
                credentials: "same-origin", // include, *same-origin, omit
                headers: {
                    'Accept': 'application/json',
                    "Content-Type": "application/json",
                    'Authorization': 'Bearer ' + api_key
                },
                redirect: "follow", // manual, *follow, error
                referrer: "no-referrer", // no-referrer, *client
                body: JSON.stringify(objInfo), // body data type must match "Content-Type" header
            }).then(response => response.json());
		});

    </script>
    <!-- end -->

</body>
</html>