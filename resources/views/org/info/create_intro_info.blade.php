@extends('layouts.org')

@section('title')
    Thông tin giới thiệu
@endsection

@push('styles')
    <style>
        .nav-tabs-custom{
            box-shadow: none!important;
        }
    </style>
@endpush

@section('content')
    <div class="wrap-page">
        <div class="box" id="box-area">
            @if(isset($orgInfoConfig[0]->id))
                {!! Former::vertical_open_for_files(route('introInfo.update',[$organizationId,$orgInfoConfig[0]->id]),'put') !!}
            @else
                {!! Former::vertical_open_for_files(route('introInfo.store',$organizationId)) !!}
            @endif
            {{-- {!! Former::hidden('organization_id')->value(isset($organization_id)?$organization_id:$product->id)!!}--}}
            <div class="nav-tabs-custom">
                <ul class="nav nav-tabs">
                    @foreach($orgInfoConfig as $key=> $item)
                        <li class="{{ $key==0? 'active':'' }}">
                            <a href='{{"#tab_".($key+1)}}' data-toggle="tab"
                               aria-expanded='{{ $key==0? "true":"false" }}'>{{$item->name}}</a>
                        </li>
                    @endforeach
                </ul>
                <div class="tab-content">

                    @foreach($orgInfoConfig as $key=> $item)
                        <div class="{{ $key==0? 'tab-pane active':'tab-pane' }}" id='{{"tab_".($key+1)}}'>
                            {!! Former::textarea('content[]')->id('txtEditor_' . $key)->label('Nội dung ')->addClass('editor')->value(isset($item->content)?$item->content:"") !!}
                            {!! Former::hidden('key[]')->value($item->key)!!}
                        </div>
                        <div class="clearfix"></div>
                    @endforeach
                    
                    <div class="form-group" style="padding-bottom: 15px;">
                        <div class="right-button" type="submit">
                            <button class="btn btn-success" id="save_other_info">
                                <i style="margin-right: 10px;" class="fa  fa-save"></i>Cập nhật thông tin
                            </button>
                        </div>
                    </div>

                </div>
            </div>
            {!!Former::close() !!}
        </div>
    </div>
@endsection

@push('scripts')
    <script src="js/org/info_org.js"></script>
    <script>
        $(function () {
            var infoOrg = InfoOrg();
            infoOrg.init_create_intro();
        })

    </script>
    <script src="bower_components/ckeditor/ckeditor.js"></script>
    <script src="bower_components/ckeditor/plugins/uploadfile/plugin.js"></script>
@endpush