@extends('layouts.org')

@section('title')
    Thông tin chung
@endsection

@push('styles')
    <style>
        td {
            vertical-align: middle !important;
            text-align: center !important;
        }
        .nav-tabs-custom{
            box-shadow: none!important;
        }
        .image_avatar{
            text-align: center;
        }
        .title-step {
            font-weight: bold;
            font-size: 20px;
            color: #d91c5f;
        }

        .fa-icon {
            width: 14px;
        }

        .right-container {
            flex: 1;
            padding-left: 10px;
        }

        .left-container {
            text-align: center;
        }

        .btn-file {
            margin-top: 5px;
            width: 100%;
        }

        .image_avatar>img {
            border: solid 1px gainsboro;
            padding: 4px;
        }
    </style>
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
@endpush

@section('content')
    <div class="box" id="box-product">
        <div id="primary_info_container">
            <!-- /.box-header -->
            <div class="box-body">
                <?php Former::populate($organization) ?>
                {!! Former::horizontal_open_for_files(route('generalInfo.update',[$organization->id]),'put')->novalidate() !!}
                <div class="form-container row">
                    {{--upload anh--}}
                    <div class="col-md-3 col-sm-3 col-xs-12  {{$errors->has('logo_image')?'has-error':''}} ">
                        <div class="image_avatar">
                            <img width="200" height="200" src="{{$organization->getLogo(200, 200)}}">
                        </div>
                        <div class="btn bg-maroon btn-file">
                            <i class="fa fa-picture-o"></i> Ảnh đại diện công ty
                            <input type="file" class="form-control" id="image_upload_input" name="logo_image"/>
                        </div>
                        <span class="help-block">{{$errors->first('logo_image')}}</span>
                    </div>

                    <div class="col-sm-9 col-md-9 col-xs-12">
                        {!! Former::setOption('TwitterBootstrap3.labelWidths', ['large' => 3, 'small' => 4]) !!}
                        @if ($organization->status > 0)
                        {!! Former::text('name','Tên cơ sở sản xuất ')->readonly() !!}
                        @else
                        <div class="form-group required {{$errors->has('manage_organization_id')?'has-error':''}} ">
                            <label for="manage_organization_id" class="control-label col-lg-3 col-sm-4">Cơ quan
                                tiếp nhận <sup>*</sup></label>
                            <div class="col-lg-9 col-sm-8 tree-search">
                                @include("components/select_search",['tree'=>$orgTypeTree,"level"=>0,"path"=>null,"root"=>null,
                                'parent_id'=>$organization->manage_organization_id,'name'=>'manage_organization_id',
                                'placeholder'=>'--- Hãy chọn cơ quan tiếp nhận yêu cầu ---'])
                            </div>
                        </div>

                        {!! Former::text('name','Tên cơ sở sản xuất ')->required() !!}
                        @endif
                        <div class="form-group required {{$errors->has('region_id')?'has-error':''}} ">
                            <label for="region_id" class="control-label col-lg-3 col-sm-4">Tỉnh, huyện, xã
                                <sup>*</sup> </label>
                            <div class="col-lg-9 col-sm-8 tree-search">
                                @include("components/select_search",['tree'=>$regions,"level"=>0,"path"=>null,"root"=>null,
                                'parent_id'=>(isset($organization)?$organization->region_id:null),'name'=>'region_id',
                                'placeholder'=>'Hãy chọn địa phương quản lý'])
                            </div>
                        </div>
                        {!! Former::text('address','SN/thôn/xóm ')->prepend('<i class="fa fa-home fa-icon"></i>')->required()  !!}
                        {!! Former::text('tel','Điện thoại ')->addClass('tel')->prepend('<i class="fa fa-phone fa-icon"></i>')->required() !!}
                        @if ($organization->status > 0)
                        {!! Former::radios('Ngành quản lý')->radios($departments)->inline()->disabled() !!}
                        {!! Former::select("founding_type","Loại giấy tờ ")->addOption('--- Chọn loại giấy tờ ---', '')->fromQuery($founding_type,'name','id')->disabled() !!}
                        {!! Former::text('founding_number','Giấy tờ số ')->addClass('founding_number')->readonly() !!}
                        {!! Former::text('founding_by_gov','Cơ quan cấp phép ')->addClass('founding_by_gov')->readonly() !!}
                        {!! Former::text('founding_date','Ngày cấp')->addClass('founding_date form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->readonly()
                         ->forceValue(($organization && $organization->founding_date) ? $organization->founding_date->toShortDateString() : null)!!}
                        @else                        
                        {!! Former::radios('Ngành quản lý')->radios($departments)->inline()->required() !!}
                        {!! Former::select("founding_type","Loại giấy tờ ")->addOption('--- Chọn loại giấy tờ ---', '')->fromQuery($founding_type,'name','id')->required() !!}
                        {!! Former::text('founding_number','Giấy tờ số ')->addClass('founding_number')->required() !!}
                        {!! Former::text('founding_by_gov','Cơ quan cấp phép ')->addClass('founding_by_gov')->required() !!}
                        {!! Former::text('founding_date','Ngày cấp')->addClass('founding_date form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->required()
                         ->forceValue(($organization && $organization->founding_date) ? $organization->founding_date->toShortDateString() : null)!!}
                        @endif
                        {!! Former::text('director','Người đại diện ')->addClass('director')->prepend('<i class="fa fa-user fa-icon"></i>')->required() !!}
                        {!! Former::select("organization_level_id","Quy mô ")->addClass('organization_level_id')->addOption('--- Chọn quy mô ---', '')->fromQuery($organizationLevel,'name','id')->required() !!}
                        {!! Former::text('email','Email ')->addClass('email')->prepend('<i class="fa fa-envelope-o fa-icon"></i>') !!}
                        {!! Former::text('website','Website ')->addClass('website')->prepend('<i class="fa fa-globe fa-icon"></i>') !!}
                        {!! Former::text('fanpage','FanPage ')->addClass('fanpage')->prepend('<i class="fa fa-facebook fa-icon"></i>') !!}
                        {!! Former::text('map','Bản đồ ')->addClass('map')->prepend('<i class="fa  fa-map-marker fa-icon"></i>')->placeholder("19.808241, 105.777592") !!}
                        {!! Former::select('status','Trạng thái')->fromQuery($status,'name','id') !!}
                        <div class="right-button">
                            <button type="submit" class="btn Normal btn-success">
                                <i class="fa  fa-save"></i>
                                Lưu thông tin
                            </button>
                        </div>
                    </div>
                </div>
                {!! Former::close() !!}
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script src="js/manage/organization.js"></script>
    <script>

        $(function () {
            var organization = Organization('{{ route('organization.create',$organization->id) }}');
            organization.init_create("{{$type}}");
        })
    </script>

@endpush