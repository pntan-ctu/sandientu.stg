@extends('layouts.org')

@section('title')
@if(@isset($payType))
    Cập nhật hình thức Thanh toán
@elseif(@isset($shipType))
    Cập nhật hình thức Vận chuyển
@endif
@endsection
@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        label[for="content"]{margin-top: 15px}
        label[for="is_default"]{display: none}
        .checkbox label[for="is_default"]{display: block}
    </style>
@endpush
@section('content')
@if(@isset($payType))
 <div class="row">
    <div class="col-xs-12">
        <div class="box" id="album-box">
            <div class="box-body">
                    <form action="business/{{$organizationId}}/pay-type/{{$organizationPayType->id}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                        <input type="hidden" name="_method" value="PUT" />
                        <input type="hidden" name="id" value="{{$organizationPayType->id}}" />
                    <div class="col-md-offset-2 col-md-9">
                        <label>Phương thức thanh toán</label>
                        <select class="form-control" name="pay_type_id">
                            @foreach($payType as $item)
                            <option 
                                @if($organizationPayType->pay_type_id == $item->id)
                                {{"selected"}}
                                @endif
                                value="{{$item["id"]}}">{{$item["name"]}}</option>
                            @endforeach
                        </select>
                        @if(count($errors)>0)
                            <div class="alert alert-danger">
                                @foreach($errors->all() as $err)
                                    {{$err}}<br>
                                @endforeach
                            </div>
                        @endif
                        {!! Former::textarea('content','Nội dung thanh toán')->rows(5)->value($organizationPayType->content) !!}
                        @if($organizationPayType->is_default == 1)
                            {!! Former::checkbox('is_default')->text('Thiết lập mặc định')->check(); !!}
                        @else
                            {!! Former::checkbox('is_default')->text('Thiết lập mặc định'); !!}
                        @endif

                            <div style="padding-top: 10px;" class="text-center">
                                <button type="submit" class="btn btn-success">
                                    <i class="fa fa-save"></i>
                                    Cập nhật
                                </button>
                                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>
                                    <a style="color: #ffff" href="business/{{$organizationId}}/pay-ship"> Trở lại </a>
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @elseif(@isset($shipType))
     <div class="row">
        <div class="col-xs-12">
            <div class="box" id="album-box">
                <div class="box-body">
                        <form method="POST" action="business/{{$organizationId}}/ship-type/{{$organizationShipType->id}}" >
                            <input type="hidden" name="_token" value="{{csrf_token()}}" />
                            <input type="hidden" name="_method" value="PUT" />
                            <input type="hidden" name="id" value="{{$organizationShipType->id}}" />
                        <div class="col-md-offset-2 col-md-9">
                            <label>Hình thức vận chuyển</label>
                            <select class="form-control" name="ship_type_id">
                                @foreach($shipType as $item)
                                <option
                                    @if($organizationShipType->ship_type_id == $item->id)
                                    {{"selected"}}
                                    @endif
                                    value="{{$item["id"]}}">{{$item["name"]}}</option>
                                @endforeach
                            </select>
                            @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $err)
                                        {{$err}}<br>
                                    @endforeach
                                </div>
                            @endif
                            {!! Former::textarea('content','Nội dung vận chuyển')->rows(5)->value($organizationShipType->content) !!}
                            @if($organizationShipType->is_default == 1)
                                {!! Former::checkbox('is_default')->text('Thiết lập mặc định')->check(); !!}
                            @else
                                {!! Former::checkbox('is_default')->text('Thiết lập mặc định'); !!}
                            @endif
                        <div style="padding-top: 10px;" class="text-center">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Cập nhật
                            </button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>
                                <a style="color: #ffff" href="business/{{$organizationId}}/pay-ship"> Trở lại </a>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
@push('scripts')
@endpush
