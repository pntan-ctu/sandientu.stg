@extends('layouts.org')

@section('title')
@if(@isset($payType))
    Thêm mới hình thức Thanh toán
@elseif(@isset($shipType))
    Thêm mới hình thức Vận chuyển
@endif
@endsection
@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        label[for="content"]{margin-top: 15px}
        label[for="is_default"]{display: none}
        .checkbox label[for="is_default"]{display: block}
    </style>
@endpush
@section('content')
@if(@isset($payType))
 <div class="row">
    <div class="col-xs-12">
        <div class="box" id="album-box">
            <div class="box-body">
                
                    <form action="{{route('pay-type.store',['organizationId' => $organizationId])}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div class="col-md-offset-2 col-md-9">
                        <label>Hình thức thanh toán</label>
                        <select class="form-control" name="pay_type_id">
                            @foreach($payType as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $err)
                                        {{$err}}<br>
                                    @endforeach
                                </div>
                        @endif
                        {!! Former::textarea('content','Nội dung thanh toán')->rows(5) !!}
                        {!! Former::checkbox('is_default')->text('Thiết lập mặc định'); !!}
                        <div style="padding-top: 10px;" class="text-center">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Thêm
                            </button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>
                                <a style="color: #ffff" href="business/{{$organizationId}}/pay-ship"> Trở lại </a>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@elseif(@isset($shipType))
<div class="row">
    <div class="col-xs-12">
        <div class="box" id="album-box">
            <div class="box-body">
                    <form action="{{route('ship-type.store',['organizationId' => $organizationId])}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}" />
                    <div class="col-md-offset-2 col-md-9">
                        <label>Hình thức vận chuyển</label>
                        <select class="form-control" name="ship_type_id">
                            @foreach($shipType as $item)
                            <option value="{{$item->id}}">{{$item->name}}</option>
                            @endforeach
                        </select>
                        @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $err)
                                        {{$err}}<br>
                                    @endforeach
                                </div>
                        @endif
                        {!! Former::textarea('content','Nội dung vận chuyển')->rows(5) !!}
                        {!! Former::checkbox('is_default')->text('Thiết lập mặc định'); !!}
                        <div style="padding-top: 10px;" class="text-center">
                            <button type="submit" class="btn btn-success">
                                <i class="fa fa-save"></i>
                                Thêm
                            </button>
                            <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>
                                <a style="color: #ffff" href="business/{{$organizationId}}/pay-ship"> Trở lại </a>
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif
@endsection
@push('scripts')
<script>
    $('select[name="ship_type_id"]').change(function(){
           $('#ship_type').val(this.value); 
        });
</script>
@endpush
