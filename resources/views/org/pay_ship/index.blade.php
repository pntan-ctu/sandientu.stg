@extends('layouts.org')

@section('title')
    Hình thức Thanh toán
@endsection
@push('styles')
<link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
@endpush
@section('content')
 <div class="row">
    <div class="col-xs-12">
        <div class="box" id="album-box">
            <div class="box-header">
                <div class="right-button">
                    <a type="button" class="btn Normal btn-success" href="{{route('pay-type.create',['organizationId' => $organizationId])}}">
                        <i class="fa fa-plus"></i>
                        Thêm mới
                    </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                    <colgroup>
                        <col width="30%">
                        <col width="40%">
                        <col width="20%">
                        <col width="20%">
                    </colgroup>
                    <thead>
                        <tr role="row" class="heading">
                            <th>Hình thức thanh toán</th>
                            <th>Nội dung thanh toán</th>
                            <th style="text-align: center">Thiết lập mặc định</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($pay as $item)
                        <tr id="2" role="row" class="odd">
                            <td >{{$item->pay_types_name}}</td>
                            <td >{{$item->content}}</td>
                            <td align="center">{{$item->is_default == 1 ? "x" : ""}}</td>
                            <td class="text-center">
                                <form action="{{route('pay-type.destroy',['organizationId' => $organizationId,'id'=>$item->id])}}" method="POST">
                                <a href="{{route('pay-type.edit',['organizationId' => $organizationId, 'id' => $item->id])}}" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </a>
                                    <input type="hidden" name="_method" value="DELETE" />
                                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                <button Onclick="return ConfirmDelete();" type="submit" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
<section class="modul-name">
    Hình thức Vận chuyển
</section>
<div class="row">
    <div class="col-xs-12">
        <div class="box" id="album-box">
            <div class="box-header">
                <div class="right-button">
                    <a type="button" class="btn Normal btn-success" href="{{route('ship-type.create',['organizationId' => $organizationId])}}">
                        <i class="fa fa-plus"></i>
                        Thêm mới
                    </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                    <colgroup>
                        <col width="30%">
                        <col width="30%">
                        <col width="20%">
                        <col width="20%">
                    </colgroup>
                    <thead>
                        <tr role="row" class="heading">
                            <th>Hình thức vận chuyển</th>
                            <th>Nội dung vận chuyển</th>
                            <th style="text-align: center">Thiết lập mặc định</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($ship as $item)
                        <tr id="2" role="row" class="odd">
                            <td >{{$item->ship_types_name}}</td>
                            <td >{{$item->content}}</td>
                            <td align="center">{{$item->is_default == 1 ? "x" : ""}}</td>
                            <td class="text-center">
                                <form action="{{route('ship-type.destroy',['organizationId' => $organizationId,'id'=>$item->id])}}" method="POST">
                                <a href="business/{{$organizationId}}/ship-type/{{$item->id}}/edit" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa">
                                    <i class="glyphicon glyphicon-pencil"></i>
                                </a>
                                    <input type="hidden" name="_method" value="DELETE" />
                                    <input type="hidden" name="_token" value="{{csrf_token()}}" />
                                <button Onclick="return ConfirmDelete();" type="submit" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></button>
                                </form>
                            </td>
                        </tr>
                        @endforeach
                </table>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
@endsection
@push('scripts')
<script>
    function ConfirmDelete()
    {
      var x = confirm("Bạn có chắc chắn muốn xóa?");
      if (x)
          return true;
      else
        return false;
    }
</script>
@endpush
