<style>
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
</style>
<?php Former::populate($cert); ?>
@if($cert)
    {!! Former::open(route('certificates.update', ['organizationId' => $organizationId, 'id' => $cert->id]), 'put') !!}
@else
    {!! Former::open(route('certificates.store', ['organizationId' => $organizationId])) !!}
@endif
{!! Former::text('title', 'Tên vùng') !!}
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Loại chứng nhận, xác nhận : </label>
    <div class="col-lg-10 col-sm-8">
        <select class="form-control select2 select2-hidden-accessible required" name="certificate_category_id" aria-hidden="true">
            <option value="0">----Chọn loại chứng nhận, xác nhận-----</option>
            @foreach ($tree as $key => $value)
                <option value="{{ $value->id }}" @if(isset($cert) && $value->id == $cert->certificate_category_id) selected="selected" @endif>{{ $value->name }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Giới thiệu : </label>
    <div class="col-lg-10 col-sm-8">
        <textarea class="ckeditor" name="description" id="my-editor">@if(isset($cert->description)) {{ html_entity_decode($cert->description) }} @endif</textarea>
    </div>
</div>
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Đăng ảnh: </label>
    <div class="col-lg-10 col-sm-8">
        <img id="logo-img" title="Chọn ảnh đại diện" onclick="document.getElementById('add-new-logo').click();"
             @if(isset($cert->image) && $cert->image)
             src="{{ url('image/750/750/'.$cert->image) }}"
             @else
             src="{{ asset('storage/image/no-image.png') }}"
                @endif
        />
        <input type="file" style="display: none" id="add-new-logo" name="image" accept="image/*" onchange="addNewLogo(this)"/>
    </div>
</div>
@if($cert)
    {!! Former::hidden('organization_id', $cert->organization_id) !!}
@else
    {!! Former::hidden('organization_id', $organizationId) !!}
@endif
{!! Former::close() !!}
<script type="text/javascript">
    var addNewLogo = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Hiển thị ảnh vừa mới upload lên
                $('#logo-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            //submit form để upload ảnh
        }
    }
</script>