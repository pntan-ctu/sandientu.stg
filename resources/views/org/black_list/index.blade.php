@extends('layouts.org')

@section('title')
    Người dùng bị ngăn chặn
@endsection

@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
    
    <style>
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #keyword{
            min-width: 200px;
        }
    </style>
@endpush

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left col-xs-12') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="30%">
                    <col width="60%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Tên tài khoản</th>
                    <th>Lý do chặn</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/org/black_list.js"></script>
    <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    $(function () {
        sdcApp.fixCKEditorModal();
        BlackList('{{ route('black-list.index', ['organizationId' => $organizationId]) }}');
        
    })
</script>
@endpush
