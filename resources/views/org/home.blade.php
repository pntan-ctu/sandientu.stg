@extends('layouts.org')

@section('title')
    Tổng quan về cơ sở
@endsection

@push('styles')
    <style>
        .nav-tabs-custom {
            width: 100%;
        }
      .small-boxes {
            position: relative;
            min-height: 1px;
            padding-right: 5px;
            padding-left: 5px;
        }
      .small-row {
            padding-right: 10px;
            padding-left: 10px;
        }
    </style>
@endpush

@section('content')

    <div class="box" id="box-area">
      <div class="row small-row">
          <!-- Custom tabs (Charts with tabs)-->
          <div class="col-lg-7 small-boxes">
              <div class="nav-tabs-custom" style="float: right;width:100%;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-bar-chart"></i> Xử lý đơn đặt hàng</li>
                </ul>
                <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="sales-chart" style="position: relative; height: 300px;"></div>
                </div>
              </div>
          </div>    
          <div class="col-lg-5 small-boxes">
              <div class="nav-tabs-custom" style="float: right;width:100%;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-globe"></i> Tình hình hoạt động của cơ sở</li>
                </ul>
                <div class="tab-content no-padding" style="padding-left: 50px !important;">
                  <!-- Morris chart - Sales -->
                  <p style="font-weight:bold; padding-left: 10px;">- Tổng số lượt xem cơ sở : {{$organization->visits()->count()}} </p>
                  <p style="padding-left: 25px;">+ Số lượt xem theo ngày: {{$organization->visits()->period('day')->count()}}</p>
                  <p style="padding-left: 25px;">+ Số lượt xem theo tuần: {{$organization->visits()->period('week')->count()}}</p>
                  <p style="padding-left: 25px;">+ Số lượt xem theo tháng: {{$organization->visits()->period('month')->count()}}</p>
                  <p style="padding-left: 25px;">+ Số lượt xem theo năm: {{$organization->visits()->period('year')->count()}}</p>
                  <p style="font-weight:bold;padding-left: 10px;">- Điểm đánh giá trung bình: 
                      <?php
                      echo(round($organization->avg_rate, 1));
                      ?>/5
                  </p>
                  <p style="font-weight:bold;padding-left: 10px;">- Tổng số lượt đánh giá cơ sở: {{$organization->count_rate}} </p>
                  <p style="font-weight:bold;padding-left: 10px;">- Tổng số đơn hàng đã bán: {{$organization->count_order}}</p>
                </div>
              </div>
          </div>
          <div class="clear"></div>
      </div>
      </div>
@endsection

@push('scripts')
<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script>
      google.load('visualization', '1.0', {'packages':['corechart']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {

        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', 'Topping');
        data1.addColumn('number', 'Slices');
        data1.addRows([
          ['Chưa xử lý', {!!$order1!!}],
          ['Đã xử lý', {!!$order2!!}],
          ['Đã hủy', {!!$order3!!}],
          ['Đã xác nhận', {!!$order4!!}]
        ]);
        var options1 = {'title':'Tỉ lệ xử lý đơn hàng', pieHole: 0.3, is3D:true};
        var chart1 = new google.visualization.PieChart(document.getElementById('sales-chart'));
        chart1.draw(data1, options1);
      }
</script>

@endpush