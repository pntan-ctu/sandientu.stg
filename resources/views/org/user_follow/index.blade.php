@extends('layouts.org')
@push('styles')
    <link href="css/favorite.css" rel="stylesheet">
    <style>
        .main-header{width: calc(100% - 104px)}
        .btnSendMsg{float: right; margin-top: -40px; margin-right: 5px}
        #msgSubject, #msgContent{width: 100%}
        content>div:first-child{border-bottom: 1px solid #eee;width: 104px;float: right;margin-top: -1px}
    </style>
@endpush
@section('title')
    Người theo dõi cơ sở
@endsection

@section('content')
    <div>
        @if(isUserBusiness())
            <button class="btn btn-success btnSendMsg">Gửi tin nhắn</button>
        @endif
    </div>
<div class="form-ads-container">
    <div class="col-main-chil">
        <div class="wrap-product">
            <div class="row">
                <div class="posts">
                    @if(isset($follows))
                    @foreach($follows as $value)
                    <div class="col-sm-4 col-md-2 col-xs-6 ">
                        <div class="user_follow_margin">
                            <div class="thumb-product-like-user item2">
                                <div class="img">
                                    @if(@isset($value->user->userProfile->avatar))
                                    <img style="width:115px; height:115px;" src="{{url("image/115/115/".$value->user->userProfile->avatar)}}" alt="" >
                                    @else
                                    <img style="width:115px; height:115px;"  src="{{url("css/images/no-avatar.jpg")}}" alt="" >
                                    @endif
                                </div>
                                <div class="wrap-info">
                                    <h3>{{ $value->user->name }}</h3>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                    @endif
                </div>
            </div>
            <div class="paginate">
                {{$follows->links()}}
            </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="modal-send-message" style="display: none; padding-right: 17px;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title" style="font-weight: bold">Gửi tin nhắn</h4>
            </div>
            <div class="modal-body row" style="padding: 0px 30px;">
                <div style="margin: 10px">Gửi tin nhắn tới tất cả những người theo dõi cơ sở</div>
                <table class="table" style="margin-bottom: 0" width="100%">
                    <!--tr>
                        <td width="80px">Tiêu đề</td>
                        <td>
                            <input id="msgSubject" type="text" maxlength="511" />
                        </td>
                    </tr-->
                    <tr>
                        <td width="80px">Nội dung</td>
                        <td>
                            <textarea id="msgContent" rows="5"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnSend">
                    <i class="fa fa-sign-in"></i> Gửi
                </button>
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <i class="fa fa-times"></i> Đóng
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
        $(".btnSendMsg").click(function(){
            //$("#msgSubject").val("");
            $("#msgContent").val("");
            $("#modal-send-message").modal("show");
        });

        $("#btnSend").click(function() {
            /*var msg_subject = $("#msgSubject").val().trim();
            if(msg_subject.length == 0){
                toastr.error("Bạn chưa nhập tiêu đề");
                return;
            }*/

            var msg_content = $("#msgContent").val().trim();
            if(msg_content.length == 0){
                toastr.error("Bạn chưa nhập nội dung");
                return;
            }

            /*if(msg_content.length > 1023){
                toastr.error("Nội dung không được vượt quá 1023 ký tự");
                return;
            }*/

            $.ajax({
                url: '{{url('business/'.$OrgId.'/sendMessageToFollowers')}}',
                type: 'post',
                data: {
                    //msg_subject: msg_subject,
                    msg_content: msg_content
                },
                success: function (result) {
                    $("#modal-send-message").modal("hide");
                    toastr.success("Gửi tin nhắn thành công.");
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        });
    </script>
@endpush