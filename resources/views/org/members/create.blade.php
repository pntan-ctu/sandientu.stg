<style>
    .checkbox{
        float: left !important;
        margin-right: 30px !important;
    }
    #active{margin-left: 0px !important}
    sup {
    color: red;
    }
</style>
<?php
Former::populate($member);
$rules = ['tel' => 'max:11'];
$roles = isset($roles) ? $roles : array();
?>
@if($member)
    {!! Former::open(route('members.update', ['organizationId' => $organizationId, 'id' => $member->id]), 'put')->rules($rules) !!}
    {!! Former::text('name', 'Họ tên')->required() !!}
    {!! Former::text('email')->readonly()->required() !!}
@else
    {!! Former::open(route('members.store', ['organizationId' => $organizationId]))->rules($rules) !!}
    {!! Former::text('name', 'Họ tên')->required() !!}
    {!! Former::text('email')->required() !!}
@endif
{!! Former::text('tel', 'Di động') !!}
@php
if(count($roles)) {
    $sDraw = "echo Former::checkboxes('role[]', 'Phân quyền')->required()->checkboxes([";
    foreach ($roles as $r){
        $sDraw .= "'". $r['title'] . "'" . "=> ['name' => 'role[" . $r['name'] . "]', 'value' => '" . $r['name'] . "'],";
    }
    $sDraw .= "])";
    foreach ($roles as $r){
        $sDraw .= "->check('role[" . $r['role_id'] . "]')";
    }
    $sDraw .= ";";
    eval($sDraw);
}
@endphp

{!! Former::checkbox('active', 'Kích hoạt') !!}
{!! Former::close() !!}

<!--<script type="text/javascript">
    $("button[class='btn btn-primary']").click(function(){
        if($("div[class='checkbox'] input:checked").length == 0)
        {
            toastr.error("Bạn chưa chọn phân quyền cho tài khoản");
            return false;
        }
    });
</script>-->