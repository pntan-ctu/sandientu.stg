@extends('layouts.org')

@section('title')
    Thành viên quản trị
@endsection

@push('styles')
    <style>
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #keyword{
            min-width: 200px;
        }
    </style>
@endpush

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left col-xs-12') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Nhập từ khoá cần tìm') !!}
            {!! Former::select('active_search', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chưa kích hoạt', '1' => 'Kích hoạt']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            <a id="btn-add" type="button" class="btn btn-success right-button">
                <i class="fa fa-plus"></i> Thêm mới
            </a>
            {!! Former::close() !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="20%">
                    <col width="15%">
                    <col width="15%">
                    <col width="15%">
                    <col width="10%">
                    <col width="15%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Họ tên</th>
                    <th>Email</th>
                    <th>Mật khẩu khởi tạo</th>
                    <th>Ngày tạo</th>
                    <th>Kích hoạt</th>
                    <th>Reset mật khẩu</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
@endpush
@push('scripts')
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script src="js/sdc-crud.js"></script>
    <script src="js/org/member.js"></script>
    <script>
        Member('{{ route('members.index', ['organizationId' => $organizationId]) }}');

        function resetPassword(iUserId, sName) {
            bootbox.confirm(
                "Bạn chắc chắn muốn reset mật khẩu cho <b>" + sName + "</b>?",
                function(result) {
                    if (result) {
                        $.ajax({
                          url: '{{"business/" . $organizationId . "/members/"}}' + iUserId + '/reset-password'
                        }).done(function(rs) {          
                            toastr.success("Reset mật khẩu thành công !");
                            $('#datatable').DataTable().ajax.reload(null, false);
                        });
                    }
                }
            );
        }
    </script>
@endpush