<div class="box" id="box-area">
    <div class="box-header">
        <div class="right-button">
            <button id="btn-add" type="button" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Thêm mới
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
            <colgroup>
                <col width="40%">
                <col width="40%">
                <col width="10%">
                <col width="10%">
            </colgroup>
            <thead>
            <tr role="row" class="heading">
                <th>Tiêu đề</th>
                <th>Miêu tả</th>
                <th>Ảnh</th>
                <th></th>
            </tr>
            </thead>
        </table>
    </div>
    <div class="right-button" style="margin-top: 15px;">
    <a style="color: #ffff" href="{{route('product.index', ['organizationId'=>$organizationId])}}">
        <button type="button" class="btn btn-danger" data-dismiss="modal">
            <i class="fa fa-share-square"></i>
            Quay lại danh sách
        </button>
    </a>
    </div>
    <!-- /.box-body -->
</div>