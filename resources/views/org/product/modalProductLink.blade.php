
<div class="modal fade in" id="modal-product-links" style="display: none; padding-right: 17px;">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Thêm sản phẩm gốc</h4>
            </div>
            <div class="box-header" style="padding: 5px 15px;">
                <form method="POST" id="search-form" class="form-inline pull-left  col-lg-12" role="form">
                    <div class="form-group col-lg-5" style="padding:0px;">
                        <input type="text" class="form-control" style="width:100%;" name="keyword"
                               placeholder="Nhập từ khoá cần tìm">
                    </div>

                    <div class="form-group col-lg-5">
                        @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>'Loại sản phẩm','parent_id'=>null,'name'=>'product_category_id_modal'])
                    </div>

                    <button type="submit" class="btn btn-primary col-lg-2">Tìm kiếm</button>
                </form>
            </div>
            <div class="modal-body row" style="padding: 0px 30px;">
                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover" id="datatable-product-link"
                           width="100%">
                        <colgroup>
                            <col width="10%">
                            <col width="40%">
                            <col width="40%">
                            <col width="10%">
                        </colgroup>
                        <thead>
                        <tr role="row" class="heading">
                            <th>Mã SP</th>
                            <th>Tên sản phẩm</th>
                            <th>Cơ sở sản xuất</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <div class="col-lg-12" id="box-add">

                </div>

                <textarea id="more-detail" rows="3" style="margin: 10px 0;" class="col-lg-12"
                          placeholder="Sản phẩm ngoài hệ thống hoặc thông tin thêm..."></textarea>

                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCloseGroupNews">Đóng
                </button>
                <button type="submit" class="btn btn-primary" id="btnSaveProductLinks">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>