<div class="box-body">
    @if(isset($productConfigs[0]->product_id))
        {!! Former::vertical_open_for_files()->id('more-info') !!}
    @else
        {!! Former::vertical_open_for_files()->id('more-info') !!}
    @endif
    {!! Former::hidden('product_id')->value(isset($product_id)?$product_id:$product->id)!!}
    <div class="nav-tabs-custom">
        <ul class="nav nav-tabs">
            @foreach($productConfigs as $key=> $item)
                <li class="{{ $key==0? 'active':'' }}">
                    <a href='{{"#tab_".($key+1)}}' data-toggle="tab"
                       aria-expanded='{{ $key==0? "true":"false" }}'>{{$item->name}}</a>
                </li>
            @endforeach
        </ul>
        <div class="tab-content">

            @foreach($productConfigs as $key=> $item)
                <div class="{{ $key==0? 'tab-pane active':'tab-pane' }}" id='{{"tab_".($key+1)}}'>
                    {!! Former::textarea('content[]')->id('txtEditor_' . $key)->label('Nội dung ')->addClass('editor')->value(isset($item->content)?$item->content:"") !!}
                    {!! Former::hidden('key[]')->value($item->key)!!}
                </div>
            @endforeach
            <div class="form-group">
                <div style="height: 25px;">
                    <div class="right-button">
                        <a style="color: #ffff" href="{{route('product.index', ['organizationId'=>$organizationId])}}">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                <i class="fa fa-share-square"></i>
                                Quay lại danh sách
                            </button>
                        </a>
                        <a class="btn btn-success" id="save_other_info">
                            <i style="margin-right: 10px;" class="fa fa-save"></i>Lưu thông tin
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>
    {!!Former::close() !!}
</div>
