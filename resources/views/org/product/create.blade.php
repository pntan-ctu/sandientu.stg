@extends('layouts.org')
@push('styles')

<!-- Latest compiled and minified JS -->
<link href="css/profiles.css" rel="stylesheet">
    <style>
        td {
            vertical-align: middle !important;
            /*text-align: center !important;*/
        }

        .grid_image div {
            padding: 2px;
        }
        .grid_image img {
            height: 50px;
        }

        .icon-close {
            position: absolute;
            top: 0px;
            right: 1px;
            color: #040404;
            cursor: pointer;
            opacity: 0.4;
            border-radius: 50%;
            background-color: #FFF;
            padding: 2px;
        }

        .image_avatar {
            border: solid 1px #eee;
            padding: 4px;
            width: 100%;
        }

        .image_avatar > img {
            width: 100%;
            height: 100%;
        }

        .grid_image > div:hover .icon-close {
            opacity: 1;
        }

        .add-image {
            /*margin-top: 5px;*/
        }

        .add-image > div {
            margin-top: 5px;
            width: 100%;
        }

        .right-container {
            flex: 1;
            padding-left: 10px;
        }

        .left-container {
            text-align: center;
        }

        .form-container {
            width: 100%;
        }

        .btn-file {
            margin-top: 5px;
            width: 100%;
        }
        .note {
            font-size: 12px;
            color: #3c8dbc;
            margin-top: 5px;
        }

        .title-step {
            font-weight: bold;
            font-size: 20px;
            color: #5aa32a;
        }

        #more_info_container {
            display: block;
        }

        .active > a {
            font-weight: bold;
            color: #5aa32a !important;
        }

        #box-add {
            padding: 0px;
        }

        .product-add {
            text-align: left;
            float: left;
            background: #3c8dbc;
            margin-right: 10px;
            border-radius: 10px;
            padding: 5px 0px;
            margin-bottom: 5px;
        }

        .product-add i {
            position: absolute;
            top: -11px;
            right: -8px;
            background: red;
            opacity: 1;
            color: #ffffff;
            width: 16px;
            font-size: 12px;
            text-align: center;
        }

        .product-add > span {
            position: relative;
            color: #ffffff;
        }

        #product_links {
            font-size: 35px;
            padding: 0px;
            margin-top: -3px;
            float: left;
        }
        .disabledbutton {
            pointer-events: none;
            opacity: 0.4;
        }
        .ajax-load-qa {
	background: url("{{ asset('img/ajax-loader.gif') }}") no-repeat center center rgba(255,255,255,1);
	z-index: 1000;
	left: 0;
	top: 0;
	width: 100%;
	height: 100%;
	display: none;
}

    </style>

@endpush
@section('title')
    Quản lý sản phẩm
@endsection

@section('decription')
    Quản lý sản phẩm
@endsection

@section('content')

<div class="row">
    <div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" style="padding-left:20px; padding-right:1px; padding-top:6px; font-size:18px;">
        <span class="label label-primary lv" >Chọn lĩnh vực đăng tin </span><div id="ajax_loader" class="ajax-load-qa"> </div>
    </div>
    <div class="col-xs-10 col-sm-10 col-md-10 col-lg-10">
        <div  style="margin-left:10px; margin-right:10px;" role="tabpanel">
            <!-- Nav tabs -->
            <ul class="nav nav-tabs" role="tablist">
            <?php
                $ctl = DB::table('product_categories')->select('name','id')->where('parent_id','=',null)->get();
            ?>
            @foreach($ctl as $value)
                <li id="{{ $value->id  }}"  role="presentation" class="">
                    <a style="font-size:13px; text-transform: uppercase; width:150px; text-align:center;" id="{{ $value->id }}" href="#home" aria-controls="home" role="tab" data-toggle="tab">{{ $value->name }}</a>
                </li>
                <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
                
            @endforeach
            </ul>
        </div>
    </div>
</div>
<script>

function ref(){
        $(document).ready(function () {
            $('{{isset($name)?"#".$name:'.js-example-basic-single'}}').select2({
                width: '100%',
                placeholder: "{{isset($placeholder)?$placeholder :''}}",
                templateResult: function (data) {
                    // We only really care if there is an element to pull classes from
                    if (!data.element) {
                        return data.text;
                    }

                    var $element = $(data.element);
                    var level = $element.attr('level');
                    var pad = level * 20;
                    var icon_class = "icon_class";
                    if (level == 0)
                        icon_class = "icon_class_0";
                    if (level == 1)
                        icon_class = "icon_class_1";
                    if (level == 2)
                        icon_class = "icon_class_2";
                    if (level == 3)
                        icon_class = "icon_class_3";

                    var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right ' + icon_class + ' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                    var $wrapper = $(wraptext);
                    return $wrapper;
                },
                templateSelection: function (state) {
                    if (!state.id) {
                        return state.text;
                    }
                    return $(state.element).attr('path');
                }
            });
        });
}

function getCatalogy(id){
            $.ajax({
               type:'POST',
               url: window.location.href+'/getcatalog/'+id,
               data:'_token = <?php echo csrf_token() ?>',
               success:function(data) {
                   if(data != "")
                  $("#dm").html(data);
                  ref();
               }
            });
         }
</script>
<script>
                    $( document ).ready(function() {
                        $(".ns").hide();
                        $(".dvdl").hide();
                        $(".dvvl").hide();
                        $(".dadt").hide();
                        $("#40").addClass("active");
                        $("#id_parent").val('40');
                    });

                    $("#53").click(function(){
                        getCatalogy(53);
                        $(".lv").hide();
                        $('#ajax_loader').css( 'display', 'block' );
                        $("#box-product").hide();
                            setTimeout(function(){
                                $('#ajax_loader').css( 'display', 'none' );
                                $("#box-product").show();
                                $(".bds").hide();
                                $(".dadt").hide();
                                $(".ns").hide();
                                $(".dvdl").show();
                                $(".all").show();
                                $(".dvvl").hide();
                                $("#id_parent").val('53');
                                $(".lv").show();
                        }, 500);
                        });
                       

                    $("#40").click(function(){
                        getCatalogy(40);
                        $(".lv").hide();
                        $('#ajax_loader').css( 'display', 'block' );
                        $("#box-product").hide();
                            setTimeout(function(){
                                $('#ajax_loader').css( 'display', 'none' );
                                $("#box-product").show();
                                $(".bds").show();
                                $(".dadt").hide();
                                $(".all").show();
                                $(".ns").hide();
                                $(".dvdl").hide();
                                $(".dvvl").hide();
                                $("#id_parent").val('40');
                                $(".lv").show();
                        }, 500);
                    });
                    $("#50").click(function(){
                        getCatalogy(50);
                        $(".lv").hide();
                        $('#ajax_loader').css( 'display', 'block' );
                        $("#box-product").hide();
                            setTimeout(function(){
                                $('#ajax_loader').css( 'display', 'none' );
                                $("#box-product").show();
                                $(".bds").hide();
                                $(".dadt").hide();
                                $(".ns").show();
                                $(".all").show();
                                $(".dvdl").hide();
                                $(".dvvl").hide();
                                $("#id_parent").val('50');
                                $(".lv").show();
                        }, 500);                       
                    });
                    $("#55").click(function(){
                        getCatalogy(55);
                        $(".lv").hide();
                        $('#ajax_loader').css( 'display', 'block' );
                        $("#box-product").hide();
                            setTimeout(function(){
                                $('#ajax_loader').css( 'display', 'none' );
                                $("#box-product").show();
                                $(".all").hide();
                                $(".bds").hide();
                                $(".dadt").hide();
                                $(".ns").hide();
                                $(".dvdl").hide();
                                $(".dvvl").show();
                                $("#id_parent").val('55');
                                $(".lv").show();
                        }, 500);
                    });
                    $("#56").click(function(){
                        getCatalogy(56);
                        $(".lv").hide();
                        $('#ajax_loader').css( 'display', 'block' );
                        $("#box-product").hide();
                            setTimeout(function(){
                                $('#ajax_loader').css( 'display', 'none' );
                                $("#box-product").show();
                                $(".all").hide();
                                $(".bds").hide();
                                $(".ns").hide();
                                $(".dvdl").hide();
                                $(".dvvl").hide();
                                $(".dadt").show();
                                $("#id_parent").val('56');
                                $(".lv").show();
                        }, 500);
                    });
                </script>



@if(isset($product))
                <?php 
                    $parent_id_first  = DB::table('product_categories')->select('parent_id')->where('id','=',$product->product_category_id) ->get();
                    if($parent_id_first[0]->parent_id == null)  $parent_id = $product->product_category_id; else $parent_id = $parent_id_first[0]->parent_id;
                   if($parent_id == 40){
                       ?>
                            <script>
                                $(document).ready(function(){
                                    $(".bds").show();
                                    $(".ns").hide();
                                    $(".dvdl").hide();
                                    $(".dvvl").hide();
                                    $("#50").removeClass("active");
                                    $("#50").addClass("disabledbutton");
                                    $("#53").addClass("disabledbutton");
                                    $("#55").addClass("disabledbutton");
                                    $("#56").addClass("disabledbutton");
                                    $("#40").addClass("active");
                                    $("#53").removeClass("active");
                                    $("#55").removeClass("active");
                                    $("#id_parent").val('40');
                                });
                            </script>
                       <?php
                   }
                   else
                        if($parent_id  == 50){
                            ?>
                                <script>
                                $(document).ready(function(){
                                    $(".bds").hide();
                                    $(".dvdl").hide();
                                    $(".dvvl").hide();
                                    $(".ns").show();
                                    $("#50").addClass("active");
                                    $("#40").removeClass("active");
                                    $("#53").removeClass("active");
                                    $("#55").removeClass("active");
                                    $("#40").addClass("disabledbutton");
                                    $("#53").addClass("disabledbutton");
                                    $("#55").addClass("disabledbutton");
                                    $("#56").addClass("disabledbutton");
                                    $("#id_parent").val('50');
                                });
                            </script>        
                            <?php 
                        }
                        else 
                            if($parent_id  == 53){
                                ?>
                                <script>
                                $(document).ready(function(){
                                    $(".bds").hide();
                                    $(".dvdl").show();
                                    $(".dvvl").hide();
                                    $(".ns").hide();
                                    $("#50").removeClass("active");
                                    $("#40").removeClass("active");
                                    $("#53").addClass("active");
                                    $("#55").removeClass("active");
                                    $("#40").addClass("disabledbutton");
                                    $("#50").addClass("disabledbutton");
                                    $("#55").addClass("disabledbutton");
                                    $("#56").addClass("disabledbutton");
                                    $("#id_parent").val('53');
                                });
                            </script>        
                            <?php 
                            }
                            else 
                                if($parent_id  == 55){
                                    ?>
                                <script>
                                $(document).ready(function(){
                                    $(".bds").hide();
                                    $(".dvdl").hide();
                                    $(".dvvl").show();
                                    $(".ns").hide();
                                    $(".all").hide();
                                    $("#50").removeClass("active");
                                    $("#40").removeClass("active");
                                    $("#53").removeClass("active");
                                    $("#55").addClass("active");
                                    $("#50").addClass("disabledbutton");
                                    $("#40").addClass("disabledbutton");
                                    $("#53").addClass("disabledbutton");
                                    $("#56").addClass("disabledbutton");
                                    $("#id_parent").val('55');
                                });
                            </script>        
                            <?php 
                                }
                                else 
                                    if($parent_id == 56){
                                        ?>
                                        <script>
                                                $(document).ready(function(){
                                                    $(".bds").hide();
                                                    $(".dvdl").hide();
                                                    $(".dvvl").hide();
                                                    $(".ns").hide();
                                                    $(".all").hide();
                                                    $(".dadt").show();
                                                    $("#50").removeClass("active");
                                                    $("#40").removeClass("active");
                                                    $("#53").removeClass("active");
                                                    $("#55").removeClass("active");
                                                    $("#50").addClass("disabledbutton");
                                                    $("#40").addClass("disabledbutton");
                                                    $("#53").addClass("disabledbutton");
                                                    $("#55").addClass("disabledbutton");
                                                    $("#id_parent").val('56');
                                                });
                                        </script>        
                                        <?php
                                    }
                 ?>
               
@endif

    <div class="box" id="box-product">
        <div class="box-header">
            <div class="form-group">
                <div class="col-xs-12 right-button">
                    @include('components/step_progress',['step'=>1,'product_id'=>isset($product)?$product->id:null])
                </div>
            </div>
        </div>
        <div class="form-info step_1" id="primary_info_container">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="form-container row">
                    {{--upload anh--}}
                    <div class="left-container col-sm-3 col-xs-12">
                        <form id="upload_form" method="post" enctype="multipart/form-data">
                            <div class="image_avatar" ondrop='drop(event)' ondragover='allowDrop(event)'>
                                <img
                                        src={{ isset($product->avatar_image)?url('image/200/200/'.$product->avatar_image):url('css/images/no-image.png') }}
                                        {{ isset($product->avatar_image)? "path=".$product->avatar_image:null }}
                                >
                            </div>

                            <div class=" col-lg-12 add-image rm-padding">
                                <div id="img-table" class="grid_image">
                                    @if(isset($productImage))
                                        @foreach($productImage as $key=> $item)
                                            <?php $index = $key + 1; ?>
                                            <div class='col-xs-4 '>
                                                <i class='fa fa-close icon-close'></i>
                                                <img path="{{$item->image}}" id="{{'drag'.$index}}"
                                                     draggable='true' ondragstart='drag(event)' width='100%' height='50'
                                                     src="{{'image/0/0/'.$item->image}}"/>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <div class="btn bg-maroon btn-file">
                                    <i class="fa fa-picture-o"></i> Thêm ảnh sản phẩm
                                    <input type="file" class="form-control" id="image_upload_input" name="files[]"
                                           multiple/>
                                </div>
                            </div>
                            <div class="col-lg-12 add-image rm-padding">
                                <i class="note">Bạn có thể thay đổi ảnh đại diện bằng cách kéo ảnh lên khung ảnh đại
                                    diện</i>
                            </div>
                        </form>
                    </div>

                    {{--Form thong tin--}}
                    <div class="right-container col-sm-9 col-xs-12">
                        <?php Former::populate($product) ?>
                        @if($product)
                            {!! Former::open()->addClass('form-input') !!}
                        @else
                            {!! Former::open()->method('post')->addClass('form-input') !!}
                        @endif
                         {!!  Former::setOption('TwitterBootstrap3.labelWidths', ['large' => 4, 'small' => 4]) !!}
                        {!! Former::text('name','Tiêu đề tin')->placeholder('Nhập tiêu đề tin đăng')->required() !!}
                        <div class="form-group required {{$errors->has('product_category_id')?'has-error':''}} ">
                            <label for="name" class="control-label col-lg-4 col-sm-4">Danh mục sản phẩm <sup>*</sup></label>
                            <div id="dm" class="col-lg-8 col-sm-8">
                                @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>"Chọn danh mục sản phẩm",'parent_id'=>($product?$product->product_category_id:null),'name'=>'product_category_id'])
                            </div>
                        </div>
                        {!! Former::hidden('id')->id("id")->addClass('id-primary') !!}
                        <div class="form-group bds">
                            {!! Former::text('it5_vitri','Vị trí')->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->placeholder('Nhập vị trí hiện tại của bất động sản')->id('vitriBDS') !!}
                        </div>
                        <!-- <div class="form-group bds required {{$errors->has('product_category_id')?'has-error':''}} ">
                            <label for="name" class="control-label col-lg-4 col-sm-4">Chọn khu vực BĐS <sup>*</sup></label>
                            <style type="text/css">
                                .it5{
                                    width:49%;
                                    height: 35px;
                                    float: left;
                                    margin-left: 1%;
                                    background: transparent;
                                    }
                            </style>
                            <div class="col-lg-8 col-sm-8">
                                <select id="sl_huyen" name="sl_huyen" class="it5">
                                    <option value="">--Chọn Huyện/TP/TX--</option>
                                    <option value="819">Cái Bè</option>
                                    <option value="820">Cai Lậy</option>
                                    <option value="821">Châu Thành</option>
                                    <option value="822">Chợ Gạo</option>
                                    <option value="824">Gò Công Đông</option>
                                    <option value="823">Gò Công Tây</option>
                                    <option value="815">Mỹ Tho</option>
                                    <option value="825">Tân Phú Đông</option>
                                    <option value="818">Tân Phước</option>
                                    <option value="817">TX Cai Lậy</option>
                                    <option value="816">TX Gò Công</option>
                                </select>
                                <select id="sl_xa" name="sl_xa" class="it5">
                                    <option value="">--Chọn Xã/Phường--</option>
                                </select>
                            </div>
                        </div> -->
                       <!--  <div  class="form-group bds">
                            {!! Former::text('it5_soto','Số tờ')->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->placeholder('Nhập số tờ bản đồ')->id('it5_soto') !!}
                        </div>
                         <div class="form-group bds">
                            {!! Former::text('it5_sothua','Số thửa')->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->placeholder('Nhập số thửa')->id('it5_sothua') !!}
                        </div>
                         <div class="form-group bds">
                        @include('org.product.maps_polygon')
                        </div> -->
                        <div class="form-group bds">
                            {!! Former::text('it5_sdtlienhe','Số điện thoại')->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->placeholder('Nhập số điện thoại liên hệ')->id('sdtlienhe') !!}
                        </div>
                        <div  class="form-group bds">
                            {!! Former::text('it5_dientichsudung','Diện tích sử dụng ')->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->placeholder('Vui lòng nhập số')->id('dientichsudung')->append('m²')  !!}
                        </div>
                        <div class="form-group bds">
                            {!! Former::select('it5_giaytophaply','Giấy tờ pháp lý')->options(['Có sổ đỏ/Sổ'])->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->placeholder('Nhập giấy tờ pháp lý đã dó ( Sổ đỏ / sổ hồng)')->id('giaytophaply') !!}
                        </div>
                        <div class="form-group bds">
                            {!! Former::text('it5_ngangdai','Độ dài cụ thể Dài x Rộng')->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->placeholder('Nhập chiều dài chiều rộng cụ thể nếu có')->id('dodaicuthe') !!}
                        </div>
                        <div class="form-group all">
                            {!! Former::text('price','Giá ')->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->placeholder('Nhập giá')->id('price')->append('VNĐ')  !!}
                        </div>
                        <div class="form-group all">    
                            {!! Former::text('price_sale','Giá khuyến mãi ')->addGroupClass('rm-padding rm-margin sm-margin-top')->addClass('number_input')->placeholder('Nhập giá khuyến mãi (Nếu có) ')->append('VNĐ')  !!}
                        </div>
                        <input type="text" name="unit" id="unit" value="unit" style="display: none;">
                        <div class="form-group ns">
                            {!! Former::text('unit','Đơn vị tính ')->addGroupClass('rm-padding rm-margin')->value(0 ) !!}
                        </div>
                        <div class="form-group ns">
                            {!! Former::text('weight_per_unit','Khối lượng/một đơn vị tính')->addGroupClass(' rm-padding rm-margin')->addClass('number_input')->id('price')->append('Gram')  !!}
                        </div>
                        <div class="form-group ns">
                            {!! Former::text('barcode','Mã vạch ')->addGroupClass('rm-padding rm-margin sm-margin-top') !!}
                        </div>
                        <div class="form-group ns">
                            {!! Former::select('status','Trạng thái ')->addClass('status_selection')->addGroupClass('rm-padding rm-margin')->fromQuery($listStatus)->select(isset($product)?$product->status:1) !!}
                        </div>
                        <div class="form-group ns">    
                            {!! Former::text('duration','Mùa vụ ')->addClass('duration_input')->addGroupClass('rm-padding rm-margin sm-margin-top')->setAttribute('enabled',isset($product)&&$product->status==2?null:'disabled') !!}
                        </div>
                         <div class="form-group ns">
                            {!! Former::text('capacity','Sản lượng ')->addGroupClass('rm-padding rm-margin') !!}
                        </div>
                        <div class="form-group ns">    
                            {!! Former::text('pack_style','Quy cách đóng gói ')->addGroupClass('rm-padding rm-margin sm-margin-top') !!}
                        </div>
                        <div class="form-group ns">
                            {!! Former::text('preservation_method','Cách bảo quản ')->addGroupClass('rm-padding rm-margin') !!}
                        </div>
                        <div class="form-group ns">    
                            {!! Former::radios('has_check[]')->radios(['Không' => 'has_check','Có' =>'has_check'])->inline()->check(1)->addGroupClass('rm-padding rm-margin sm-margin-top')->label('Truy xuất nguồn gốc ')!!}
                        </div>
                        <div class="dvdl form-group">
                            {!! Former::select('it5_loaitour','Loại Tour ')->addClass('status_selection')->addGroupClass('rm-padding rm-margin')->fromQuery($loaitour)->select(isset($product)?$product->it5_loaitour:1) !!}
                        </div> 
                        <div class=" dvdl form-group">
                            {!! Former::text('it5_thoigiankhoihanh','Thời gian khởi hành ')->type('datetime-local')->addGroupClass('rm-padding rm-margin')->value('isset($product)?$product->it5_thoigiankhoihanh') !!}
                        </div>          
                        <div class=" dvdl form-group">
                            {!! Former::text('it5_noikhoihanh','Nơi khởi hành ')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập nơi khởi hành') !!}
                        </div>
                        <div class=" dvdl form-group">
                            {!! Former::text('it5_noiden','Nơi đến')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập nơi sẽ đến') !!}
                        </div>
                        <div class=" dvdl form-group">
                            {!! Former::text('it5_thoigiandidukien','Thời gian đi dự kiến')->addGroupClass('rm-padding rm-margin')->placeholder('Dự kiến thời gian đi')->append('Giờ') !!}
                        </div>
                        <div class=" dvdl form-group">
                            {!! Former::text('it5_phuongtiendi','Phương tiện đi')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập phương tiện đi') !!}
                        </div>
                        <div class=" dvvl form-group">
                            {!! Former::number('it5_mucluong','Mức lương')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập mức lương')->require() !!}
                        </div>
                        <div class=" dvvl form-group">
                        {!! Former::select('it5_hinhthuctraluong','Hình thức trả lương ')->addClass('status_selection')->addGroupClass('rm-padding rm-margin')->fromQuery($listhinhthucluong)->select(isset($product)?$product->it5_hinhthuctraluong:0) !!}
                        </div>
                        <div class=" dvvl form-group">
                        {!! Former::select('it5_yeucaugioitinh','Yêu cầu giới tính ')->addClass('status_selection')->addGroupClass('rm-padding rm-margin')->fromQuery($yeucaugioitinh)->select(isset($product)?$product->it5_yeucaugioitinh:0) !!}
                        </div>
                        <div class=" dvvl form-group">
                        {!! Former::number('it5_soluongtuyendung','Số lượng tuyển dụng ')->addGroupClass('rm-padding rm-margin')!!}
                        </div>
                        <div class=" dvvl form-group">
                        {!! Former::select('it5_trinhdotoithieu','Yêu cầu trình độ tối thiểu ')->addClass('status_selection')->addGroupClass('rm-padding rm-margin')->fromQuery($yeucautrinhdo)->select(isset($product)?$product->it5_yeucautrinhdo:0) !!}
                        </div>
                        <div class=" dvvl form-group">
                        {!! Former::number('it5_tuoitoida','Yêu cầu tuổi tối đa ')->addGroupClass('rm-padding rm-margin')!!}
                        </div>
                        <div class=" dvvl form-group">
                        {!! Former::number('it5_tuoitoithieu','Yêu cầu tuổi thiểu ')->addGroupClass('rm-padding rm-margin')!!}
                        </div>
                        <div class=" dvvl form-group">
                        {!! Former::select('it5_loaicongviec','Loại công việc')->addClass('status_selection')->addGroupClass('rm-padding rm-margin')->fromQuery($loaicongviec)->select(isset($product)?$product->it5_loaicongviec:0) !!}
                        </div>
                        <div class=" dadt form-group">
                            {!! Former::text('it5_diachiduan','Địa chỉ dự án cụ thể')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập địa chỉ dự án') !!}
                        </div>
                        <div class=" dadt form-group">
                            {!! Former::text('it5_quymoduan','Quy mô dự án')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập quy mô dự án') !!}
                        </div>
                        <div class=" dadt form-group">
                            {!! Former::number('it5_vondautuduan','Vốn đầu tư')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập vốn đầu tư')->append('VNĐ') !!}
                        </div>
                        <div class=" dadt form-group">
                            {!! Former::number('it5_soluongdautukeugoi','Số lượng kêu gọi đầu tư')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập số lượng kêu gọi đầu tư')->append('Người') !!}
                        </div>
                        <div class=" dadt form-group">
                            {!! Former::number('it5_vondaututoithieu','Vốn đầu tư tối thiểu')->addGroupClass('rm-padding rm-margin')->placeholder('Nhập vốn đầu tư tối thiểu')->append('VNĐ') !!}
                        </div>
                        {!! Former::hidden('id_parent')->id('id_parent')->addClass('id-primary') !!}
                        <div class="form-group ns" id="imgtemp">
                            <label class="control-label col-lg-4 col-sm-4">Ảnh tem truy xuất nguồn gốc </label>
                            <div class="col-lg-8 col-sm-8">
                                <div class="upload-img-container">
                                    <div class="upload-img-border">
                                        <div class="upload-img-wrap">
                                            @if(isset($product->check_temp_image))
                                            <img class="media-object" src="{{url("/image/128/128/".$product->check_temp_image)}}"  >
                                            
                                            @endif
                                        </div>
                                        @if( isset($product->check_temp_image) )
                                        <a href="javascript:void(0)" onclick="removeImage()" class="upload-img-clear-btn">Remove</a>
                                        @endif
                                    </div>
                                    <div class="upload-img-wrap-desc">
                                        <div class="btn btn-default btn-file">
                                            <i class="fa fa-picture-o"></i> Chọn ảnh tem truy xuất nguồn gốc
                                            <input type="file" id="temp_image" name="temp_image" onChange='uploadImage($(this)[0].files);'>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class=" bds control-label col-lg-4 col-sm-4">Thêm dự án gốc </label>
                            <div class="bds col-lg-8 col-sm-8">
                                <a class="btn fa fa-plus-square" id="product_links"></a>
                                <div id="box-add-create">
                                    @if(isset($productLink))
                                        @foreach($productLink as $item)
                                            <div class='product-add'
                                                 id='{{isset($item->link_product_id)?$item->link_product_id:0}}'><span
                                                        class='col-lg-12'>{{isset($item->link_product_id)?$item->name.' - '.$item->cssx:$item->descript}}
                                                    <i class='fa fa-close icon-close'></i></span></div>
                                        @endforeach
                                    @endif
                                </div>
                                {{--<div class="btn  btn-default btn-file col-lg-3" id="product_links">--}}
                                {{--<i class="fa  fa-plus-square" style="margin-right: 5px;"></i>--}}
                                {{--<input type="file" class="form-control" id="product_links" name="product_links[]"--}}
                                {{--multiple/>--}}
                                {{--</div>--}}
                            </div>
                            <input id="paths" type="hidden" name="paths[]" value=""/>
                            <input id="path_avatar" type="hidden" name="path_avatar" value=""/>
                            <input id="link_product_id" type="hidden" name="link_product_ids[]" value=""/>
                            <input id="descript" type="hidden" name="descript" value=""/>
                            <input id="hascheck" type="hidden" name="hascheck" value="{{isset($product->has_check)?$product->has_check:'0'}}"/>
                            <input id="check_temp_image" type="hidden" name="check_temp_image" value="{{isset($product->check_temp_image)?$product->check_temp_image:''}}" />
                            <div class="form-group" style="margin-bottom: 0">
                                <div class="col-lg-12" style="margin-top:10px;">
                                    <div class="right-button">
                                        <a style="color: #ffff" href="{{route('product.index', ['organizationId'=>$organizationId])}}">
                                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                                <i class="fa fa-share-square"></i>
                                                Quay lại danh sách
                                            </button>
                                        </a>
                                        <button class="btn color_fff btn-success" id="save_primary_info">
                                            <i class="fa fa-save"></i>
                                            Lưu sản phẩm
                                        </button>
                                    </div>
                                </div>
                            </div>
                            {!! Former::close() !!}
                        </div>

                    </div>

                </div>
            </div>
        </div>
        {{--Step2: Thông tin mở rộng--}}
        <div class="form-info step_2" id="more_info_container" style="display: none">
            {{--@if(isset($product))--}}
            {{--@include('org/product/moreInfo',['organizationId'=>$organizationId])--}}
            {{--@endif--}}
        </div>

        {{--Step3: Giấy tờ pháp lý--}}
        <div class="form-info step_3" id="certificate_info_container" style="display: none">
            {{--@if(isset($product))--}}
            {{--@include('org/product/certificateInfo',['organizationId'=>$organizationId])--}}
            {{--@endif--}}
        </div>

        {{--Modal product links--}}
        @include('org/product/modalProductLink') 


        @endsection
        @push('scripts')
            <script src="js/sdc-crud.js"></script>
            <script src="js/org/product.js"></script>
           
            <script>
                $(function () {
                    var product = Product('{{ route('product.create',$organizationId) }}', "{{isset($product)?$product->id:''}}");
                    product.init_create(product, "{{$organizationId}}");
                    var checkImgTemp = $('#hascheck').val();
                    if (checkImgTemp == 0) {
                        $('#imgtemp').css("display", "none");
                    }
                    else if (checkImgTemp == 1) {
                        $('#imgtemp').css("display", "block");
                    }
                    $('input[type=radio][name=has_check]').change(function() {
                        if (this.value == 0) {
                            $('#imgtemp').css("display", "none");
                            // $('#check_temp_image').val('');
                        }
                        else if (this.value == 1) {
                            $('#imgtemp').css("display", "block");
                        }
                    });
                })

                $(function () {
                    var productLink = Product('{{ route('product-links.index',[$organizationId]) }}', '');
                    productLink.init_product_link();
                })

                var json = {
    "status": "OK",
    "DanhMuc": {
        "QuanHuyen": [
            {
                "OBJECTID": "2",
                "tenHuyen": "Cái Bè",
                "MaTinh": "TG",
                "Loai": "Huyen",
                "huyenId": "819"
            },
            {
                "OBJECTID": "10",
                "tenHuyen": "Cai Lậy",
                "MaTinh": "TG",
                "Loai": "Huyen",
                "huyenId": "820"
            },
            {
                "OBJECTID": "4",
                "tenHuyen": "Châu Thành",
                "MaTinh": "TG",
                "Loai": "Huyen",
                "huyenId": "821"
            },
            {
                "OBJECTID": "5",
                "tenHuyen": "Chợ Gạo",
                "MaTinh": "TG",
                "Loai": "Huyen",
                "huyenId": "822"
            },
            {
                "OBJECTID": "11",
                "tenHuyen": "Gò Công Đông",
                "MaTinh": "TG",
                "Loai": "Huyen",
                "huyenId": "824"
            },
            {
                "OBJECTID": "7",
                "tenHuyen": "Gò Công Tây",
                "MaTinh": "TG",
                "Loai": "Huyen",
                "huyenId": "823"
            },
            {
                "OBJECTID": "6",
                "tenHuyen": "Mỹ Tho",
                "MaTinh": "TG",
                "Loai": "TP",
                "huyenId": "815"
            },
            {
                "OBJECTID": "8",
                "tenHuyen": "Tân Phú Đông",
                "MaTinh": "TG",
                "Loai": "Huyen",
                "huyenId": "825"
            },
            {
                "OBJECTID": "3",
                "tenHuyen": "Tân Phước",
                "MaTinh": "TG",
                "Loai": "Huyen",
                "huyenId": "818"
            },
            {
                "OBJECTID": "1",
                "tenHuyen": "TX Cai Lậy",
                "MaTinh": "TG",
                "Loai": "TX",
                "huyenId": "817"
            },
            {
                "OBJECTID": "9",
                "tenHuyen": "TX Gò Công",
                "MaTinh": "TG",
                "Loai": "TX",
                "huyenId": "816"
            }
        ],
        "PhuongXa": [
            {
                "OBJECTID": "40",
                "tenXa": "P.1",
                "maXa": "28261",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "50",
                "tenXa": "P.1",
                "maXa": "28303",
                "Loai": "Phuong",
                "huyenId": "816"
            },
            {
                "OBJECTID": "108",
                "tenXa": "P.1",
                "maXa": "28435",
                "Loai": "Phuong",
                "huyenId": "817"
            },
            {
                "OBJECTID": "113",
                "tenXa": "P.2",
                "maXa": "28436",
                "Loai": "Phuong",
                "huyenId": "817"
            },
            {
                "OBJECTID": "54",
                "tenXa": "P.2",
                "maXa": "28297",
                "Loai": "Phuong",
                "huyenId": "816"
            },
            {
                "OBJECTID": "32",
                "tenXa": "P.2",
                "maXa": "28264",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "61",
                "tenXa": "P.3",
                "maXa": "28294",
                "Loai": "Phuong",
                "huyenId": "816"
            },
            {
                "OBJECTID": "47",
                "tenXa": "P.3",
                "maXa": "28258",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "119",
                "tenXa": "P.3",
                "maXa": "28437",
                "Loai": "Phuong",
                "huyenId": "817"
            },
            {
                "OBJECTID": "102",
                "tenXa": "P.4",
                "maXa": "28439",
                "Loai": "Phuong",
                "huyenId": "817"
            },
            {
                "OBJECTID": "46",
                "tenXa": "P.4",
                "maXa": "28252",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "60",
                "tenXa": "P.4",
                "maXa": "28300",
                "Loai": "Phuong",
                "huyenId": "816"
            },
            {
                "OBJECTID": "59",
                "tenXa": "P.5",
                "maXa": "28249",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "39",
                "tenXa": "P.5",
                "maXa": "28306",
                "Loai": "Phuong",
                "huyenId": "816"
            },
            {
                "OBJECTID": "95",
                "tenXa": "P.5",
                "maXa": "28440",
                "Loai": "Phuong",
                "huyenId": "817"
            },
            {
                "OBJECTID": "42",
                "tenXa": "P.6",
                "maXa": "28270",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "49",
                "tenXa": "P.7",
                "maXa": "28255",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "44",
                "tenXa": "P.8",
                "maXa": "28267",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "53",
                "tenXa": "P.9",
                "maXa": "28273",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "77",
                "tenXa": "P.10",
                "maXa": "28276",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "90",
                "tenXa": "An Cư",
                "maXa": "28390",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "9",
                "tenXa": "An Hữu",
                "maXa": "28429",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "30",
                "tenXa": "An Thái Đông",
                "maXa": "28414",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "57",
                "tenXa": "An Thái Trung",
                "maXa": "28426",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "38",
                "tenXa": "An Thạnh Thủy",
                "maXa": "28639",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "55",
                "tenXa": "Bàn Long",
                "maXa": "28573",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "64",
                "tenXa": "Bình Ân",
                "maXa": "28735",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "149",
                "tenXa": "Bình Đông",
                "maXa": "28708",
                "Loai": "Xa",
                "huyenId": "816"
            },
            {
                "OBJECTID": "45",
                "tenXa": "Bình Đức",
                "maXa": "28579",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "41",
                "tenXa": "Bình Nghị",
                "maXa": "28741",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "80",
                "tenXa": "Bình Nhì",
                "maXa": "28666",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "19",
                "tenXa": "Bình Ninh",
                "maXa": "28648",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "68",
                "tenXa": "Bình Phan",
                "maXa": "28633",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "110",
                "tenXa": "Bình Phú",
                "maXa": "28657",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "127",
                "tenXa": "Bình Phú",
                "maXa": "28471",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "105",
                "tenXa": "Bình Phục Nhứt",
                "maXa": "28621",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "22",
                "tenXa": "Bình Tân",
                "maXa": "28681",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "76",
                "tenXa": "Bình Trưng",
                "maXa": "28564",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "128",
                "tenXa": "Bình Xuân",
                "maXa": "28717",
                "Loai": "Xa",
                "huyenId": "816"
            },
            {
                "OBJECTID": "31",
                "tenXa": "Cái Bè",
                "maXa": "28360",
                "Loai": "TT",
                "huyenId": "819"
            },
            {
                "OBJECTID": "75",
                "tenXa": "Cẩm Sơn",
                "maXa": "28489",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "37",
                "tenXa": "Chợ Gạo",
                "maXa": "28594",
                "Loai": "TT",
                "huyenId": "822"
            },
            {
                "OBJECTID": "106",
                "tenXa": "Đăng Hưng Phước",
                "maXa": "28624",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "98",
                "tenXa": "Đạo Thạnh",
                "maXa": "28282",
                "Loai": "Xa",
                "huyenId": "815"
            },
            {
                "OBJECTID": "133",
                "tenXa": "Điềm Hy",
                "maXa": "28540",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "89",
                "tenXa": "Đông Hòa",
                "maXa": "28549",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "24",
                "tenXa": "Đông Hòa Hiệp",
                "maXa": "28411",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "118",
                "tenXa": "Đồng Sơn",
                "maXa": "28654",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "104",
                "tenXa": "Đồng Thạnh",
                "maXa": "28660",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "97",
                "tenXa": "Dưỡng Điềm",
                "maXa": "28546",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "141",
                "tenXa": "Gia Thuận",
                "maXa": "28714",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "158",
                "tenXa": "Hậu Mỹ Bắc A",
                "maXa": "28363",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "165",
                "tenXa": "Hậu Mỹ Bắc B",
                "maXa": "28366",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "121",
                "tenXa": "Hậu Mỹ Phú",
                "maXa": "28375",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "146",
                "tenXa": "Hậu Mỹ Trinh",
                "maXa": "28372",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "96",
                "tenXa": "Hậu Thành",
                "maXa": "28393",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "62",
                "tenXa": "Hiệp Đức",
                "maXa": "28501",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "27",
                "tenXa": "Hòa Định",
                "maXa": "28645",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "8",
                "tenXa": "Hòa Hưng",
                "maXa": "28432",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "100",
                "tenXa": "Hòa Khánh",
                "maXa": "28399",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "142",
                "tenXa": "Hòa Tịnh",
                "maXa": "28600",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "33",
                "tenXa": "Hội Xuân",
                "maXa": "28507",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "168",
                "tenXa": "Hưng Thạnh",
                "maXa": "28342",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "83",
                "tenXa": "Hữu Đạo",
                "maXa": "28555",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "123",
                "tenXa": "Kiểng Phước",
                "maXa": "28726",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "28",
                "tenXa": "Kim Sơn",
                "maXa": "28585",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "107",
                "tenXa": "Long An",
                "maXa": "28558",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "14",
                "tenXa": "Long Bình",
                "maXa": "28687",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "71",
                "tenXa": "Long Bình Điền",
                "maXa": "28636",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "74",
                "tenXa": "Long Chánh",
                "maXa": "28315",
                "Loai": "Xa",
                "huyenId": "816"
            },
            {
                "OBJECTID": "122",
                "tenXa": "Long Định",
                "maXa": "28552",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "34",
                "tenXa": "Long Hòa",
                "maXa": "28318",
                "Loai": "Xa",
                "huyenId": "816"
            },
            {
                "OBJECTID": "82",
                "tenXa": "Long Hưng",
                "maXa": "28309",
                "Loai": "Xa",
                "huyenId": "816"
            },
            {
                "OBJECTID": "88",
                "tenXa": "Long Hưng",
                "maXa": "28561",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "86",
                "tenXa": "Long Khánh",
                "maXa": "28486",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "67",
                "tenXa": "Long Thuận",
                "maXa": "28312",
                "Loai": "Xa",
                "huyenId": "816"
            },
            {
                "OBJECTID": "58",
                "tenXa": "Long Tiên",
                "maXa": "28498",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "36",
                "tenXa": "Long Trung",
                "maXa": "28504",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "16",
                "tenXa": "Long Vĩnh",
                "maXa": "28678",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "109",
                "tenXa": "Lương Hòa Lạc",
                "maXa": "28612",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "87",
                "tenXa": "Mỹ Đức Đông",
                "maXa": "28405",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "72",
                "tenXa": "Mỹ Đức Tây",
                "maXa": "28408",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "147",
                "tenXa": "Mỹ Hạnh Đông",
                "maXa": "28450",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "140",
                "tenXa": "Mỹ Hạnh Trung",
                "maXa": "28453",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "135",
                "tenXa": "Mỹ Hội",
                "maXa": "28387",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "73",
                "tenXa": "Mỹ Lợi A",
                "maXa": "28396",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "111",
                "tenXa": "Mỹ Lợi B",
                "maXa": "28381",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "66",
                "tenXa": "Mỹ Long",
                "maXa": "28495",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "17",
                "tenXa": "Mỹ Lương",
                "maXa": "28420",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "93",
                "tenXa": "Mỹ Phong",
                "maXa": "28288",
                "Loai": "Xa",
                "huyenId": "815"
            },
            {
                "OBJECTID": "166",
                "tenXa": "Mỹ Phước",
                "maXa": "28351",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "154",
                "tenXa": "Mỹ Phước Tây",
                "maXa": "28447",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "115",
                "tenXa": "Mỹ Tân",
                "maXa": "28378",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "163",
                "tenXa": "Mỹ Thành Bắc",
                "maXa": "28441",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "145",
                "tenXa": "Mỹ Thành Nam",
                "maXa": "28456",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "139",
                "tenXa": "Mỹ Tịnh An",
                "maXa": "28603",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "150",
                "tenXa": "Mỹ Trung",
                "maXa": "28369",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "7",
                "tenXa": "Ngũ Hiệp",
                "maXa": "28516",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "132",
                "tenXa": "Nhị Bình",
                "maXa": "28543",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "117",
                "tenXa": "Nhị Mỹ",
                "maXa": "28474",
                "Loai": "Phuong",
                "huyenId": "817"
            },
            {
                "OBJECTID": "94",
                "tenXa": "Nhị Quý",
                "maXa": "28477",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "29",
                "tenXa": "P.Tân Long",
                "maXa": "28279",
                "Loai": "Phuong",
                "huyenId": "815"
            },
            {
                "OBJECTID": "101",
                "tenXa": "Phú An",
                "maXa": "28492",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "161",
                "tenXa": "Phú Cường",
                "maXa": "28444",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "3",
                "tenXa": "Phú Đông",
                "maXa": "28750",
                "Loai": "Xa",
                "huyenId": "825"
            },
            {
                "OBJECTID": "129",
                "tenXa": "Phú Kiết",
                "maXa": "28609",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "169",
                "tenXa": "Phú Mỹ",
                "maXa": "28336",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "137",
                "tenXa": "Phú Nhuận",
                "maXa": "28465",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "23",
                "tenXa": "Phú Phong",
                "maXa": "28588",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "79",
                "tenXa": "Phú Quý",
                "maXa": "28483",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "1",
                "tenXa": "Phú Tân",
                "maXa": "28753",
                "Loai": "Xa",
                "huyenId": "825"
            },
            {
                "OBJECTID": "5",
                "tenXa": "Phú Thạnh",
                "maXa": "28696",
                "Loai": "Xa",
                "huyenId": "825"
            },
            {
                "OBJECTID": "152",
                "tenXa": "Phước Lập",
                "maXa": "28357",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "92",
                "tenXa": "Phước Thạnh",
                "maXa": "28567",
                "Loai": "Xa",
                "huyenId": "815"
            },
            {
                "OBJECTID": "13",
                "tenXa": "Phước Trung",
                "maXa": "28744",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "112",
                "tenXa": "Quơn Long",
                "maXa": "28618",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "78",
                "tenXa": "Song Bình",
                "maXa": "28630",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "43",
                "tenXa": "Song Thuận",
                "maXa": "28582",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "21",
                "tenXa": "Tam Bình",
                "maXa": "28513",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "124",
                "tenXa": "Tam Hiệp",
                "maXa": "28537",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "138",
                "tenXa": "Tân Bình",
                "maXa": "28462",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "143",
                "tenXa": "Tân Bình Thạnh",
                "maXa": "28606",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "65",
                "tenXa": "Tân Điền",
                "maXa": "28738",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "91",
                "tenXa": "Tân Đông",
                "maXa": "28732",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "130",
                "tenXa": "Tân Hiệp",
                "maXa": "28519",
                "Loai": "TT",
                "huyenId": "821"
            },
            {
                "OBJECTID": "15",
                "tenXa": "Tân Hòa",
                "maXa": "28702",
                "Loai": "TT",
                "huyenId": "824"
            },
            {
                "OBJECTID": "173",
                "tenXa": "Tân Hòa Đông",
                "maXa": "28324",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "160",
                "tenXa": "Tân Hòa Tây",
                "maXa": "28348",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "167",
                "tenXa": "Tân Hòa Thành",
                "maXa": "28339",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "126",
                "tenXa": "Tân Hội",
                "maXa": "28468",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "162",
                "tenXa": "Tân Hội Đông",
                "maXa": "28522",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "52",
                "tenXa": "Tân Hưng",
                "maXa": "28417",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "156",
                "tenXa": "Tân Hương",
                "maXa": "28525",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "153",
                "tenXa": "Tân Lập",
                "maXa": "28345",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "159",
                "tenXa": "Tân Lập 2",
                "maXa": "28354",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "157",
                "tenXa": "Tân Lý Đông",
                "maXa": "28528",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "144",
                "tenXa": "Tân Lý Tây",
                "maXa": "28531",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "69",
                "tenXa": "Tân Mỹ Chánh",
                "maXa": "28291",
                "Loai": "Xa",
                "huyenId": "815"
            },
            {
                "OBJECTID": "10",
                "tenXa": "Tân Phong",
                "maXa": "28510",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "4",
                "tenXa": "Tân Phú",
                "maXa": "28693",
                "Loai": "Xa",
                "huyenId": "825"
            },
            {
                "OBJECTID": "134",
                "tenXa": "Tân Phú",
                "maXa": "28459",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "155",
                "tenXa": "Tân Phước",
                "maXa": "28711",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "116",
                "tenXa": "Tân Tây",
                "maXa": "28723",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "11",
                "tenXa": "Tân Thanh",
                "maXa": "28423",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "12",
                "tenXa": "Tân Thành",
                "maXa": "28747",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "2",
                "tenXa": "Tân Thạnh",
                "maXa": "28699",
                "Loai": "Xa",
                "huyenId": "825"
            },
            {
                "OBJECTID": "6",
                "tenXa": "Tân Thới",
                "maXa": "28690",
                "Loai": "Xa",
                "huyenId": "825"
            },
            {
                "OBJECTID": "99",
                "tenXa": "Tân Thuận Bình",
                "maXa": "28627",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "125",
                "tenXa": "Tân Trung",
                "maXa": "28729",
                "Loai": "Xa",
                "huyenId": "816"
            },
            {
                "OBJECTID": "18",
                "tenXa": "Tăng Hòa",
                "maXa": "28705",
                "Loai": "Xa",
                "huyenId": "824"
            },
            {
                "OBJECTID": "131",
                "tenXa": "Thân Cửu Nghĩa",
                "maXa": "28534",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "114",
                "tenXa": "Thanh Bình",
                "maXa": "28615",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "81",
                "tenXa": "Thành Công",
                "maXa": "28663",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "103",
                "tenXa": "Thanh Hòa",
                "maXa": "28480",
                "Loai": "Xa",
                "huyenId": "817"
            },
            {
                "OBJECTID": "170",
                "tenXa": "Thạnh Hòa",
                "maXa": "28333",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "164",
                "tenXa": "Thạnh Lộc",
                "maXa": "28438",
                "Loai": "Xa",
                "huyenId": "820"
            },
            {
                "OBJECTID": "172",
                "tenXa": "Thạnh Mỹ",
                "maXa": "28330",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "51",
                "tenXa": "Thạnh Nhựt",
                "maXa": "28675",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "84",
                "tenXa": "Thạnh Phú",
                "maXa": "28570",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "171",
                "tenXa": "Thạnh Tân",
                "maXa": "28327",
                "Loai": "Xa",
                "huyenId": "818"
            },
            {
                "OBJECTID": "63",
                "tenXa": "Thạnh Trị",
                "maXa": "28672",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "85",
                "tenXa": "Thiện Trí",
                "maXa": "28402",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "136",
                "tenXa": "Thiện Trung",
                "maXa": "28384",
                "Loai": "Xa",
                "huyenId": "819"
            },
            {
                "OBJECTID": "25",
                "tenXa": "Thới Sơn",
                "maXa": "28591",
                "Loai": "Xa",
                "huyenId": "815"
            },
            {
                "OBJECTID": "70",
                "tenXa": "Trung An",
                "maXa": "28285",
                "Loai": "Xa",
                "huyenId": "815"
            },
            {
                "OBJECTID": "151",
                "tenXa": "Trung Hòa",
                "maXa": "28597",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "148",
                "tenXa": "TT Mỹ Phước",
                "maXa": "28321",
                "Loai": "TT",
                "huyenId": "818"
            },
            {
                "OBJECTID": "120",
                "tenXa": "Vàm Láng",
                "maXa": "28720",
                "Loai": "TT",
                "huyenId": "824"
            },
            {
                "OBJECTID": "35",
                "tenXa": "Vĩnh Bình",
                "maXa": "28651",
                "Loai": "TT",
                "huyenId": "823"
            },
            {
                "OBJECTID": "20",
                "tenXa": "Vĩnh Hựu",
                "maXa": "28684",
                "Loai": "Xa",
                "huyenId": "823"
            },
            {
                "OBJECTID": "48",
                "tenXa": "Vĩnh Kim",
                "maXa": "28576",
                "Loai": "Xa",
                "huyenId": "821"
            },
            {
                "OBJECTID": "26",
                "tenXa": "Xuân Đông",
                "maXa": "28642",
                "Loai": "Xa",
                "huyenId": "822"
            },
            {
                "OBJECTID": "56",
                "tenXa": "Yên Luông",
                "maXa": "28669",
                "Loai": "Xa",
                "huyenId": "823"
            }
        ]
    }
}
                $("#sl_huyen").change(function(){
                        
                    var huyen_id = $("#sl_huyen").val();
                    var phuongxa = json.DanhMuc.PhuongXa;
                    function myFunction(item) {

                        if(item.huyenId == huyen_id)

                          $("#sl_xa").append("<option value='"+item.maXa+"'>"+item.tenXa+"</option>");
                        }
                    $("#sl_xa").html("");
                    phuongxa.forEach(myFunction);


                });

            </script>
            <script src="bower_components/ckeditor/ckeditor.js"></script>
            <script src="bower_components/ckeditor/plugins/uploadfile/plugin.js"></script>
            <script src="js/org/cert.js"></script>
    @endpush
