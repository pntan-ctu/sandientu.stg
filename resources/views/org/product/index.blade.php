@extends('layouts.org')
@section('title')
    Danh sách tin đăng
@endsection

@push('styles')
    <style>
        td {
            vertical-align: middle !important;
        }
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #keyword{
            min-width: 200px;
        }
    </style>
@endpush

@section('content')
    <div class="box" id="box-product-category">
        <div class="box-header">
            <form method="POST" id="search-form" class="form-inline pull-left col-xs-12" role="form">
                <div class="form-group">
                    <input type="text" class="form-control"  id="keyword" name="keyword" placeholder="Nhập từ khoá cần tìm">
                </div>
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>

                <div class="right-button">
                    <a type="button" class="btn btn-success" href="{{ route('product.create',[$organizationId]) }}">
                        <i class="fa fa-plus"></i> Thêm tin mới
                    </a>
                </div>
            </form>

        </div>
        <!-- /.box-header -->
        <style type="text/css">
            tr th{
                text-align: center;
            }
             tr td{
                text-align: center;
            }
        </style>
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                <thead>
                <tr role="row" class="heading">
                    <th class="all">Mã bất động sản</th>
                    <th class="all">Tên tin đăng</th>
                    <th class="min-mobile-l">Giá (đồng)</th>
                    <th class="min-tablet-p">Diện tích (m²)</th>
                    <th class="min-tablet-p">Trạng thái</th>
                    {{--<th>Mùa vụ</th>--}}
                    <th class="min-tablet-p">Ảnh</th>
                    <th class="min-tablet-p"></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection

@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/org/product.js"></script>
    <script>
        $(function () {
            var product = Product('{{ route('product.index',[$organizationId]) }}', '');
            product.init_index();
        })
    </script>
@endpush