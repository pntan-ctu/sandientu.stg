@extends('layouts.org')

@section('title')
    Quản lý chi nhánh, đơn vị trực thuộc
@endsection

@push('styles')
    <style>
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #keyword{
            min-width: 200px;
        }
    </style>
@endpush

@section('content')
    <div class="box" id="box-area">
        <div class="box-header">
            <form method="POST" id="search-form" class="form-inline pull-left col-xs-12" role="form">
                <div class="form-group">
                    <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
                </div>
                {{--<input type="hidden" class="form-control" id="organization_id" name="organization_id" value="{{ $organization->id }}" placeholder="Nhập từ khoá cần tìm">--}}
                <button type="submit" class="btn btn-primary">Tìm kiếm</button>

                <div class="right-button">
                    <button id="btn-add" type="button" class="btn btn-success">
                        <i class="fa fa-plus"></i> Thêm mới
                    </button>
                </div>
            </form>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="15%">
                    <col width="25%">
                    <col width="10%">
                    <col width="20%">
                    <col width="20%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Tên chi nhánh</th>
                    <th>Địa chỉ</th>
                    <th>Điện thoại</th>
                    <th>Email</th>
                    <th>Ngày tạo</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
<script src="js/sdc-crud.js"></script>
<script src="js/org/branch.js"></script>
<!-- <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script> -->
<script>
    $(function () {
        sdcApp.fixCKEditorModal();
        branch('{{ route('branch.index', ['organizationId' => $organizationId]) }}');
        
    })
</script>
@endpush
