<style type="text/css">
    sup {
        color: #EA3838 !important;
    }
</style>
<?php Former::populate($partner); ?>
@if($partner)
    {!! Former::open(route('partner.update', ['organizationId' => $organizationId,'type'=>$type ,'id' => $partner->id]), 'put') !!}
@else
    {!! Former::open(route('partner.store', ['organizationId' => $organizationId,'type'=>$type])) !!}
@endif
{!! Former::text('name', 'Tên :')->required() !!}
{!! Former::text('address', 'Địa chỉ :')->required() !!}
{!! Former::number('tel', 'Số điện thoại :') !!}
{!! Former::email('email', 'Email :') !!}
{!! Former::text('website', 'Website :') !!}
{!! Former::close() !!}