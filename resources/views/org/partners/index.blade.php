@extends('layouts.org')
@push('styles')
    <style>
        .icon-close {
            position: absolute;
            top: 0px;
            right: 1px;
            color: #040404;
            cursor: pointer;
            opacity: 0.4;
            border-radius: 50%;
            background-color: #FFF;
            padding: 2px;
        }

        #box-add {
            padding: 0px;
        }

        .org-add {
            text-align: left;
            float: left;
            background: #3c8dbc;
            margin-right: 10px;
            border-radius: 10px;
            padding: 5px 0px;
            margin-bottom: 5px;
        }

        .org-add i {
            position: absolute;
            top: -11px;
            right: -8px;
            background: red;
            opacity: 1;
            color: #ffffff;
            width: 16px;
            font-size: 12px;
            text-align: center;
        }

        .org-add > span {
            position: relative;
            color: #ffffff;
        }

        #product_links {
            font-size: 35px;
            padding: 0px;
            margin-top: -3px;
            float: left;
        }

        #search-form {
            padding: 0px;
            text-align: left;
        }

        #keyword{
            min-width: 200px;
        }
    </style>
@endpush
@section('title')
    {{ $type=='cung-cap'?"Cơ sở cung cấp sản phẩm, nguyên vật liệu":"Cơ sở phân phối sản phẩm" }}
@endsection

@section('content')
    <div class="wrap-page">
        <div class="box" id="box-area">
            <div class="box-header">
                <form method="POST" id="search-form" class="form-inline pull-left col-xs-12" role="form">
                    <div class="form-group">
                        <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
                    </div>
                    <button type="submit" class="btn btn-primary">Tìm kiếm</button>

                    <div class="right-button">
                        <button id="btn-add" type="button" class="btn btn-success">
                            <i class="fa fa-plus"></i>
                            Thêm mới
                        </button>
                        <a class="btn btn btn-info" id="org_links" style="margin-left: 2px">
                            <i class="fa fa-plus"></i> Liên kết CS
                        </a>
                    </div>
                </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                    <colgroup>
                        <col width="25%">
                        <col>
                        <col width="15%">
                        <col width="10%">
                    </colgroup>
                    <thead>
                    <tr role="row" class="heading">
                        <th>Tên nhà cung cấp</th>
                        <th>Địa chỉ</th>
                        <th>Điện thoại</th>
                        <th></th>
                    </tr>
                    </thead>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
@include('org/partners/org_link')
@endsection

@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/org/partner.js"></script>
    <script>
        $(function () {
            sdcApp.fixCKEditorModal();
            var p = new partner('{{ route('partner.index', ['organizationId' => $organizationId,'type'=>$type]) }}','{{$type}}');
            p.init_index();
            var org = new partner('{{ route('partner-link.index', ['organizationId' => $organizationId,'type'=>$type]) }}','{{$type}}');
            org.init_org_link();
            var org_link = new partner('{{ route('partner-link.index', ['organizationId' => $organizationId,'type'=>$type]) }}','{{$type}}');
            org_link.init_create("{{$type}}","{{$organizationId}}");
        });
    </script>
@endpush