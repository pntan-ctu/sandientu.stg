<style>
    .modal-backdrop { position: relative !important;}
</style>
<div class="modal fade in" id="modal-org-links" style="display: none; padding-right: 17px;">
    <div class="modal-dialog" style="width: 800px">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Thêm cơ sở đã tồn tại</h4>
            </div>
            <div class="box-header" style="padding: 5px 15px;">
                <form method="POST" id="search-form-link" class="form-inline pull-left  col-lg-12" role="form">
                    <div class="form-group col-lg-5" style="padding:0px;">
                        <input type="text" class="form-control" style="width:100%;" name="keyword"
                               placeholder="Nhập từ khoá cần tìm">
                    </div>
                    
                    <button style="margin-left: 10px;" type="submit" class="btn btn-primary col-lg-2">Tìm kiếm</button>
                </form>
            </div>
            <div class="modal-body row" style="padding: 0px 30px;">
                <div class="box-body">
                    <table class="table table-bordered table-striped table-hover" id="datatable-org-link"
                           width="100%">
                        <colgroup>
                            <col width="40%">
                            <col width="30%">
                            <col width="20%">
                            <col width="10%">
                        </colgroup>
                        <thead>
                        <tr role="row" class="heading">
                            <th>Tên nhà cung cấp</th>
                            <th>Địa chỉ</th>
                            <th>Số điện thoại</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>

            </div>
            <div class="modal-footer">
                <div class="col-lg-12" id="box-add">

                </div>
                <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCloseGroupNews">Đóng
                </button>
                <button type="submit" class="btn btn-primary" id="btnSaveOrgLinks">Lưu</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>