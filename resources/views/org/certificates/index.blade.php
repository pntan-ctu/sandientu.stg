@extends('layouts.org')

@section('title')
    Quản lý chứng nhận, xác nhận
@endsection

@section('content')
    <div class="box" id="box-area">
        <div class="box-header">
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="40%">
                    <col width="40%">
                    <col width="10%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Tên chứng nhận, xác nhận</th>
                    <th>Miêu tả</th>
                    <th>Ảnh</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/org/cert.js"></script>
    <script>
        $(function () {
            Cert('{{ route('certificates-org.index', ['organizationId' => $organizationId]) }}');
        })
    </script>
@endpush