<style>
    .right-container {
        flex: 1;
        padding-left: 10px;
    }

    .left-container {
        width: 200px;
    }

    .form-container {
        display: flex;
        width: 100%;
    }

    .btn-file {
        margin-top: 5px;
        width: 200px;
    }

    .view_image_chung_chi {
        border: solid 1px gainsboro;
        padding: 4px;
        width: 200px;
        height: 200px;
    }

    .view_image_chung_chi > img {
        width: 100%;
        height: 100%;
    }
</style>
<?php Former::populate($cert); ?>
@if($cert)
    {!! Former::open(route('certificates.update', ['organizationId' => $organizationId,'type_id'=>$type_id, 'id' => $cert->id]), 'put') !!}
@else
    {!! Former::open(route('certificates.store', ['organizationId' => $organizationId,'type_id'=>$type_id])) !!}
@endif
<div class="form-container">
    {{--upload anh--}}
    <div class="left-container  {{$errors->has('image')?'has-error':''}} ">
        <div class="view_image_chung_chi">
            <img src={{ isset($cert)?url('image/200/200/'.$cert->image):url('css/images/no-image.png') }} >
        </div>
        <div class="btn bg-maroon btn-file">
            <i class="fa fa-picture-o"></i> Ảnh chứng nhận, xác nhận
            <input type="file" class="form-control" id="image_chung_chi" name="image"/>
        </div>
        <span class="help-block">{{$errors->first('image')}}</span>
    </div>

    <div class="right-container">
        {!! Former::setOption('TwitterBootstrap3.labelWidths', ['large' => 3, 'small' => 4]) !!}
        {!! Former::text('title','Tên chứng nhận, xác nhận ') !!}
        {!! Former::select("certificate_category_id","Loại chứng nhận, xác nhận ")->addClass('select-search certificate_category_id')->addOption('', '')->fromQuery($certificateCategory,'name','id')!!}
        {!! Former::textarea('description','Miêu tả ')->rows(6) !!}
        {!!Former::close() !!}
    </div>
</div>
{!! Former::close() !!}
<script type="text/javascript">
    $('.certificate_category_id').select2({
        width: '100%',
        placeholder: "Loại chứng nhận, xác nhận"
    });
    $("#image_chung_chi").on("change", function () {
        var files = $(this)[0].files;
        $('.view_image_chung_chi>img').attr('src', URL.createObjectURL(files[0]));
    });
</script>
