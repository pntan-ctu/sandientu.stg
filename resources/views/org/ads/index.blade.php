@extends('layouts.org')

@section('title')
    Đăng tin cung cầu
@endsection

@push('styles')
    <style>
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #keyword{
            min-width: 200px;
        }
    </style>
@endpush

@section('content')
    <div class="buy-center">
        <div class="row">
            <div class="col-md-12">
                <div class="buy-center-linked-list ">
                    <div class="box" id="box-area">
                        <div class="box-header">
                            {!! Former::openInline()->id('search-form')->addClass('pull-left col-xs-12') !!}
                            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
                            {!! Former::select('status', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chờ duyệt', '1' => 'Duyệt đăng', '2' => 'Huỷ duyệt']) !!}
                            {!! Former::primary_submit('Tìm kiếm')->id('search') !!}
                            <a type="button" class="btn btn-success right-button" href="{{ route('advertising-org.create',['organizationId' => $organizationId]) }}">
                                <i class="fa fa-plus"></i> Đăng tin mới
                            </a>
                            {!! Former::close() !!}
                        </div>
                        <!-- /.box-header -->
                        <div class="box-body">
                            @include('web/ads/table_view')
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/web/ads.js"></script>
    <script>

        $(function () {
            Ads('{{ route('advertising-org.index',['organizationId' => $organizationId]) }}');
            {{--$('.button-dangtin').on('click', function (e) {--}}
            {{--location.href = "{{route('advertising.create')}}";--}}
            {{--});--}}
        });
    </script>
@endpush

