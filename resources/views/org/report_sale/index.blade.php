@extends('layouts.org')

@section('title')
    Thống kê doanh số theo sản phẩm
@endsection
@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #search-form {
            padding: 0px;
            text-align: left;
        }
    </style>
@endpush
@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left col-xs-12') !!}
            <span>Từ ngày: </span>
            {!! Former::text('from_date', 'Từ')->addClass('form-control datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>') !!}
            <span> đến ngày: </span>
            {!! Former::text('to_date', 'Đến')->addClass('form-control datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>') !!}
            <button type="submit" class="btn-primary btn"><i class="fa fa-dribbble"></i> Thống kê</button>
            <button id="btnExport" class="btn-primary btn"><i class="fa fa-file-excel-o"></i> Xuất Excel</button>
            {!! Former::close() !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="8%">
                    <col width="47%">
                    <col width="20%">
                    <col width="25%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>STT</th>
                    <th>Sản phẩm</th>
                    <th>Số lượng</th>
                    <th>Tổng tiền</th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/org/report_sale.js"></script>
    <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
<script>
    $(function () {
        var organizationId = {{ $organizationId  }};
        sdcApp.fixCKEditorModal();
        var rp = ReportSale('{{ route('report-sale.index', ['organizationId' => $organizationId]) }}', organizationId);
        rp.init_index();
        
    })
</script>
@endpush