@extends('layouts.org')

@section('title')
    Lịch sử mua hàng
@endsection
@section('content')
    <style>
        .box {
            position: relative;
            border-radius: 0px;
            background: #ffffff;
            border-top: none;
            margin-bottom: 0px;
            width: 100%;
            box-shadow: none;
        }
        .or_status{ font-size: 16px; font-style: italic; color: red}
        .table>tbody>tr>td, .table>tfoot>tr>td {
            vertical-align: middle;
        }

        .scrollbar {
            margin-left: 22px;
            float: left;
            height: 300px;
            width: 65px;
            background: #F5F5F5;
            overflow-y: scroll;
            margin-bottom: 25px;
        }

        #wrapper {
            text-align: center;
            margin: auto;
        }

        #style-1::-webkit-scrollbar,
        #style-2::-webkit-scrollbar {
            width: 7px;
            background-color: #F5F5F5;
        }

        #style-4::-webkit-scrollbar,
        #style-5::-webkit-scrollbar,
        #style-6::-webkit-scrollbar,
        #style-7::-webkit-scrollbar,
        #style-8::-webkit-scrollbar,
        #style-9::-webkit-scrollbar,
        #style-10::-webkit-scrollbar,
        #style-11::-webkit-scrollbar {
            width: 10px;
            background-color: #F2F2F2;
        }

        /**  STYLE 1 */
        #style-1::-webkit-scrollbar-thumb {
            background-color: #ddd;
        }

        #style-1::-webkit-scrollbar-track {
            background-color: #F2F2F2;
        }

        /**  STYLE 2 */
        #style-2::-webkit-scrollbar-thumb {
            border-radius: 10px;
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,.3);
            background-color: #D62929;
        }

        #style-2::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            border-radius: 10px;
            background-color: #F5F5F5;
        }

        /**  STYLE 3 */
        #style-3::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        #style-3::-webkit-scrollbar {
            width: 6px;
            background-color: #F5F5F5;
        }

        #style-3::-webkit-scrollbar-thumb {
            background-color: #000000;
        }

        /**  STYLE 4 */
        #style-4::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        #style-4::-webkit-scrollbar-thumb {
            background-color: #000000;
            border: 2px solid #555555;
        }

        /**  STYLE 5 */
        #style-5::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        #style-5::-webkit-scrollbar-thumb {
            background-color: #0ae;
            background-image: -webkit-gradient(linear, 0 0, 0 100%,
            color-stop(.5, rgba(255, 255, 255, .2)),
            color-stop(.5, transparent), to(transparent));
        }

        /**  STYLE 6 */
        #style-6::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        #style-6::-webkit-scrollbar-thumb {
            background-color: #F90;
            background-image: -webkit-linear-gradient(45deg,rgba(255, 255, 255, .2) 25%,
            transparent 25%,
            transparent 50%,
            rgba(255, 255, 255, .2) 50%,
            rgba(255, 255, 255, .2) 75%,
            transparent 75%,
            transparent)
        }

        /** STYLE 7 */
        #style-7::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
            border-radius: 10px;
        }

        #style-7::-webkit-scrollbar-thumb {
            border-radius: 10px;
            background-image: -webkit-gradient(linear,
            left bottom,
            left top,
            color-stop(0.44, rgb(122,153,217)),
            color-stop(0.72, rgb(73,125,189)),
            color-stop(0.86, rgb(28,58,148)));
        }

        /**  STYLE 8 */
        #style-8::-webkit-scrollbar-track {
            border: 1px solid black;
            background-color: #F5F5F5;
        }

        #style-8::-webkit-scrollbar-thumb {
            background-color: #000000;
        }

        /**  STYLE 9 */
        #style-9::-webkit-scrollbar-track {
            -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
            background-color: #F5F5F5;
        }

        #style-9::-webkit-scrollbar-thumb {
            background-color: #F90;
            background-image: -webkit-linear-gradient(90deg, rgba(255, 255, 255, .2) 25%,
            transparent 25%,
            transparent 50%,
            rgba(255, 255, 255, .2) 50%,
            rgba(255, 255, 255, .2) 75%,
            transparent 75%,
            transparent)
        }

        /**  STYLE 10 */
        #style-10::-webkit-scrollbar-thumb {
            border-radius: 10px;
            background: linear-gradient(left, #96A6BF, #63738C);
            box-shadow: inset 0 0 1px 1px #5C6670;
        }

        #style-10::-webkit-scrollbar-track {
            border-radius: 10px;
            background: #eee;
            box-shadow: 0 0 1px 1px #bbb, inset 0 0 7px rgba(0,0,0,0.3)
        }

        #style-10::-webkit-scrollbar-thumb:hover {
            background: linear-gradient(left, #8391A6, #536175);
        }

        /**  STYLE 11 */
        #style-11::-webkit-scrollbar-track {
            border-radius: 10px;
            background: rgba(0,0,0,0.1);
            border: 1px solid #ccc;
        }

        #style-11::-webkit-scrollbar-thumb {
            border-radius: 10px;
            background: linear-gradient(left, #fff, #e4e4e4);
            border: 1px solid #aaa;
        }

        #style-11::-webkit-scrollbar-thumb:hover {
            background: #fff;
        }

        #style-11::-webkit-scrollbar-thumb:active {
            background: linear-gradient(left, #22ADD4, #1E98BA);
        }

        .w0 { width: auto !important;}
        .font-12 { font-size: 12px !important;}
        .none { background: none !important; padding: 0px 0px 0px 5px !important;}
        .box_comment { width: 100%; float: left; box-sizing: border-box; background-color: white; border-radius: 5px; padding: 5px;}
        .box_comment .input_cm{ height: 30px; border-radius: 5px}
        .box_comment .btn_send{ margin-top: 5px}
        .color_fff{ color: #fff;}
        .btn.focus, .btn:focus, .btn:hover{ color: #fff;}
        .bg-danger { background-color: #dc3545 !important;}

        @media screen and (max-width: 600px) {
            table#cart tbody td .form-control {
                width:20%;
                display: inline !important;
            }

            .actions .btn {
                width:36%;
                margin:1.5em 0;
            }

            .actions .btn-info {
                float:left;
            }

            .actions .btn-danger {
                float:right;
            }

            table#cart thead {
                display: none;
            }

            table#cart tbody td {
                display: block;
                padding: .6rem;
                min-width:320px;
            }

            table#cart tbody tr td:first-child {
                background: #333;
                color: #fff;
            }

            table#cart tbody td:before {
                content: attr(data-th);
                font-weight: bold;
                display: inline-block;
                width: 8rem;
            }

            table#cart tfoot td {
                display:block;
            }
            table#cart tfoot td .btn {
                display:block;
            }
        }
    </style>

    <div class="user-container">
        <div class="box-info">
            <div class="order_box">
                @switch($data->status)
                    @case(1)
                    <p class="text-primary text-uppercase"><i class="fa fa-clock-o fa-icon"></i> <b>Chưa xử lý</b></p>
                    @break
                    @case(2)
                    <p class="text-success text-uppercase"><i class="fa fa-check fa-icon"></i> <b>Đã xử lý</b></p>
                    @break
                    @case(3)
                    <p class="text-danger text-uppercase"><i class="fa fa-close fa-icon"></i> <b>Đã hủy</b></p>
                    @break
                    @case(4)
                    <p class="text-info text-uppercase"><i class="fa fa-check-square fa-icon"></i> <b>Đã nhận hàng</b></p>
                    @break
                    @default
                    <p class="text-warning text-uppercase"><i class="fa fa-cart-plus fa-icon"></i> <b>Nháp</b></p>
                @endswitch
                <p><i class="fa fa-barcode fa-icon"></i> Mã hoá đơn : <b>{{ $data->code }}</b></p>
                <p><i class="fa fa-calendar fa-icon"></i> Ngày đặt hàng : <b>{{ $data->order_date->format('d-m-Y') }}</b></p>
                <p><i class="fa fa-home fa-icon"></i> Nhà cung cấp : <b>{{ $data->org->name }}</b></p>
                <p><i class="fa fa-phone fa-icon"></i> Điện thoại : <b>{{ $data->org->tel }}</b></p>
                <p><i class="fa  fa-map-marker fa-icon"></i> Địa chỉ : <b>{{ $data->org->address }}</b></p>
            </div>
        </div>
        <table id="cart" class="table table-hover table-condensed">
            <thead>
            <tr>
                <th style="width:5%">STT</th>
                <th style="width:45%">Tên sản phẩm</th>
                <th style="width:15%" class="text-right">Giá</th>
                <th style="width:13%" class="text-right">Số lượng</th>
                <th style="width:22%" class="text-right">Thành tiền</th>
            </tr>
            </thead>
            <tbody>
            @foreach($data->data as $key =>$item)
                <tr>
                    <td data-th="STT: ">{{ $key+1 }}</td>
                    @php
                        $curProduct = \App\Models\Product::find($item->product_id);
                    @endphp
                    <td data-th="Sản phẩm: "><a href="{{ asset('san-pham/'.$curProduct->path) }}" target="_blank">{{ $item->product_name }}</a></td>
                    <td data-th="Giá: "  class="text-right">{{number_format($item->price,0,",",".") }} đ</td>
                    <td data-th="Số lượng: "  class="text-right">{{ $item->amount }}</td>
                    <td data-th="Thành tiền: " class="text-right">{{number_format($item->money,0,",",".") }} đ</td>
                </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="4" class="text-right"><strong>Tổng tiền</strong>
                </td>
                <td colspan="1" class="text-right"><strong>{{number_format($data->total_money,0,",",".") }} đ</strong>
                </td>
            </tr>
            <tr>
                <td colspan="4">
                    <a href="{{ route('buys.index', ['organizationId' => $organizationId]) }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Quay lại danh sách</a>
                </td>
                <td>
                    @if($data->status == 1 && $payment_status != "00")
                        <button type="button" class="btn color_fff bg-danger btn-block" status="{{ $data->status }}" order_id="{{ $data->id }}" id="delete">Xoá đơn</button>
                    @elseif($data->status == 2)
                        <button type="button" class="btn color_fff btn-success btn-block" status="{{ $data->status }}" order_id="{{ $data->id }}" id="action">Đã nhận hàng</button>
                    @elseif($data->status == 3)
                        <button type="button" disabled="disabled" class="btn color_fff btn-success btn-block" status="{{ $data->status }}" order_id="{{ $data->id }}" id="action">Đã hủy</button>
                    @elseif($data->status == 4)
                        <button type="button" disabled="disabled" class="btn color_fff btn-success btn-block" status="{{ $data->status }}" order_id="{{ $data->id }}" id="action">Đã nhận hàng</button>
                    @endif

                </td>
            </tr>
            </tfoot>
        </table>
    </div>
    <div class="user-container">
        <h3 class="bb-double"><i class="fa fa-comments-o fa-icon w0"></i> Hỏi đáp</h3>
        <div class="box_chat force-overflow" id="style-1">
            @foreach($data->messages as $mess)
                <div class="chat_item">
                    @if($mess->sendable_type == \App\Models\User::class)

                    @endif
                    <p class="">{{ $mess->content }}</p>
                    <div class="info-chat">
                        <span>{{ $mess->sendable->name }}</span> | <span><i class="fa fa-clock-o fa-icon"></i> {{ $mess->created_at->format('H:i d-m-Y') }}</span>
                    </div>

                </div>
            @endforeach
        </div>
        <div class="box_comment">
            <textarea class="txtComment form-control" id="comment" value="Ý kiến của bạn" placeholder="Câu hỏi của bạn"></textarea>
            <input type="button" class="btn color_fff btn-success btn_send" value="Gửi" order_id="{{ $data->id }}" onclick="sendMess();">
        </div>
        <div class="clearfix"></div>
    </div>
@endsection
@push('scripts')
<script src="js/sdc-crud.js"></script>
<!-- <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script> -->
<script>
    function sendMess() {
        var text = jQuery("#comment").val();
        if (text) {
            var order_id = "{{ $data->id }}";
            var url = "{{ route('buys.addComment', ['organizationId' => $organizationId]) }}";
            var data = {order_id:order_id,text:text};
            jQuery.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: 'JSON',
                success: function( msg ) {
                    if(msg){
                        location.reload();
                    }
                }
            });
        }else {
            return;
        }
    }
    jQuery("#action").click(function () {
        var status = jQuery(this).attr("status");
        var order_id = jQuery(this).attr("order_id");
        var url = "{{ route('buys.actOrder', ['organizationId' => $organizationId]) }}";
        var data = {id:order_id,status:status};
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'JSON',
            success: function( msg ) {
                if(msg.id){
                    location.reload();
                }
            }
        });

    })
    jQuery("#delete").click(function () {
        var status = jQuery(this).attr("status");
        var order_id = jQuery(this).attr("order_id");
        var url = "{{ route('buys.huyOrder', ['organizationId' => $organizationId]) }}";
        var data = {id:order_id,status:status};
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'JSON',
            success: function( msg ) {
                toastr.success(msg.msg);
                if(msg.check == 1){
                    location.href = "{{ route('buys.index', ['organizationId' => $organizationId]) }}";
                }else {
                    location.reload();
                }
            }
        });

    })
    
    $('#comment').keypress(function (e) {
        var key = e.which;
        if(key == 13)  // the enter key code
        {
            sendMess();
        }
    });
</script>
@endpush