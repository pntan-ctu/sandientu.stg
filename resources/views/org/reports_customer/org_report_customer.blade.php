@extends('layouts.org')

@section('title')
    Thống kê doanh số theo khách hàng
@endsection
@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
    <style>
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #search-form {
            padding: 0px;
            text-align: left;
        }
    </style>
@endpush

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            <span>Từ ngày: </span>
            {!! Former::text('from_date', 'Từ')->addClass('form-control datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>') !!}
            <span> đến ngày: </span>
            {!! Former::text('to_date', 'Đến')->addClass('form-control datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>') !!}
            <button type="submit" id="btnStatistic" class="btn-primary btn"><i class="fa fa-dribbble"></i> Thống kê</button>
            <button id="btnExport" class="btn-primary btn"><i class="fa fa-file-excel-o"></i> Xuất Excel</button>
            {!! Former::close() !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="5%">
                    <col width="30%">
                    <col width="18%">
                    <col width="14%">
                    <col width="17%">
                    <col width="15%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>STT</th>
                    <th>Khách hàng</th>
                    <th>Địa chỉ</th>
                    <th>Số điện thoại</th>
                    <th>Số lần mua hàng</th>
                    <th>Tổng tiền</th>
                </tr>
                </thead>
            </table>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/org/report_customer.js"></script>
    <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script>
        $(function () {
            var OrgId = {{ $OrgId  }};
            sdcApp.fixCKEditorModal();
            var rp = ReportCustomer('{{ route('report-customer.index', ['organizationId' => $OrgId]) }}', OrgId);
            rp.init_index();
        })
    </script>
@endpush