<div class="content pdp-mod-comments">
    <div class="mod-title">
        <strong>Ý kiến bạn đọc ({{$NewsComments->count() }})</strong>
    </div>
    <div class="list_comment">
        @foreach ($NewsComments as $key => $value)
        <div class="comment_item @if($key%2==1) hight_light @endif">
            <div class="full_content">{{ $value->comment }}</div>
            <div class="user_status">
                <a class="avata_coment" href="#">
                    <img class="img_avatar" src="{{url('css/images/img_60x60.gif')}}"></a>
                <span class="left txt_666 txt_14">
                    <a class="nickname txt_666" href="#" title="" target="_blank"><b>{{$value->name}}</b></a>  {{$value->created_at->format('d-m-Y h:m:s')}}</span>
<!--                <p class="txt_666 right block_like_web">
                    <a id="27403563" class="txt_666 txt_11 link_thich " href="javascript:;" rel="">
                        <i class="ic ic-like"></i>&nbsp;&nbsp;<span class="txt_666 txt_11 total_like" href="javascript:;">300</span>&nbsp;&nbsp;Thích</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a id="27403563" class="txt_blue txt_11 link_reply" href="javascript:;" rel="27403563" parent="27403563"><b>Trả lời</b></a>&nbsp;&nbsp;|&nbsp;&nbsp;<a href="javascript:;" rel="27403563" class="share_cmt_fb txt_blue txt_11">Chia sẻ</a>
                </p>-->
            </div>
        </div>
        <div class="clearfix"></div>

        @endforeach
    </div>	
    <div class="input_comment">
        <form id="comment_post_form" enctype="multipart/form-data">

            <div class="caption-txt">
                Nhập thông tin của bạn:
            </div>
            <div class='form-group'>
                <input type="text" class="txt-login" name="email" id="email" value="" placeholder="Email">
                <input type="fullname" class="txt-login" name="fullname" id="fullname" value="" placeholder="Họ tên">
            </div>
            <textarea id="txtComment" name="txtComment" value="" placeholder="Ý kiến của bạn" class="block_input" rows="" cols=""></textarea>

            <div class="block_relative">
                <div class="right block_btn_send">
                    <button type="button" onclick="upload()" class="btn_send_comment btn_sdc" id="comment_post_button">Gửi</button>
                </div>
            </div>
        </form>
    </div>

</div>
<script type="text/javascript">
    function validate(){
        if($('#email').val().trim()=== ''){
            alert("Bạn chưa nhập Email");
            $('#email').focus();
            return false;
        }
         if($('#fullname').val().trim() === ''){
            alert("Bạn chưa nhập họ tên");
            $('#fullname').focus();
            return false;
        }
         if($('#txtComment').val().trim() === ''){
            alert("Bạn chưa nhập ý kiến");
            $('#txtComment').focus();
            return false;
        }
        return true;
    }
    function upload() {
    var form_data = new FormData($("#comment_post_form")[0]);
    form_data.append('email', $('#email').val());
    form_data.append('fullname', $('#fullname').val());
    form_data.append('txtComment', $('#txtComment').val());
    form_data.append('article_id', {{$NewsArticle->id }});
  //  console.log($('#txtComment').val());
    //var formData = sdcApp.getFormDataAndType($("#comment_post_form"));
    if(!validate())
        return;
    $.ajax({
    url: "{{url('news-comments')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            // dataType: 'json',
            processData: false,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
    }).done(function (data) {
        alert("Gửi bình luận thành công!");
        $('#email').val('');
        $('#fullname').val('');
        $('#txtComment').val('');
    });
    }
</script>