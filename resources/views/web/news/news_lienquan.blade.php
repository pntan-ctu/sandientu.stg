<div class="box_category">
    <div class="box_tinlienquan">
        <div class="title_box"> Tin liên quan </div>
        <div class="list_news_quantam row">
            @foreach ($NewsRelate as $key => $value)
            <div class="list-news col-md-1-5 col-sm-4 col-xs-6">
                <a class="thumb_art" href="{{url('tin-tuc/'.$value->path)}}">
                    <img class="" src="{{url("/image/115/69/".$value->img_path)}}" style="width: 115px; height: 69px;">
                </a>
                <div class="media-body">
                    <div class="title_news"> <a href="{{url('tin-tuc/'.$value->path)}}">{{$value->title}}</a></div>
                    <div class="info-help">
                        <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{$value->created_at->format('d-m-Y')}}</span>
                    </div>
                </div>
            </div>
            @endforeach


        </div> 
        
    </div> 
</div> 