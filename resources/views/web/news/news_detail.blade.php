@extends('layouts.web')
@section('title')
{{$NewsArticle->title}}
@endsection
@section('description', $NewsArticle->summary)
@section('og:title', $NewsArticle->title)
@section('og:image', url("/image/300/300/".$NewsArticle->img_path))
@section('content')


<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1>Tin tức</h1></div>
    </div>
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('danh-muc-tin-tuc')}}" title="Tin tức">Tin tức</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        
        <div class="box-news">
            <div class="row">
                <div class="col-md-8">
                    <h2 class="h2-tit-news">{{$NewsArticle->title}}</h2>
                    <div class="info-help">
                        <span><i class="fa fa-file-text-o" aria-hidden="true"></i><a href="#">{{$NewsGroup->name}}</a></span> 
                        <span style="margin-left: 10px;margin-right: 10px">|</span>
                        <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{$NewsArticle->created_at->format('d-m-Y')}}</span>
                        <span style="margin-left: 10px;margin-right: 10px">|</span>
                        <span><i class="fa fa-television" aria-hidden="true"></i> {{$NewsArticle->visits()->count()}} lượt xem</span>
                    </div>
                    <div class="detail-news-p">
                        {!! $NewsArticle->content !!}
                    </div>
                    <!--<div class="source">Theo Dân trí</div>-->
                @include('web.news.news_lienquan')
                @include('web.news.news_ykien')
                </div>
                <div class="col-md-4">
                    <div class="box-news-hot">
                        <h3 class="h3-help">Bài viết mới nhất</h3>
                        <div class="grid-news">
                            
                             @foreach ($newArticles as $key => $value)
                            <div class="media">
                                <a class="pull-left" href="{{url('tin-tuc/'.$value->path)}}">
                                    <img class="media-object" src="{{url("/image/0/0/".$value->img_path)}}" style="width: 64px; height: 64px;">
                                </a>
                                <div class="media-body">
                                    <h4 class="media-news media-heading">{{$value->title}}</h4>
                                    <div class="info-help">
                                        <span><i class="fa fa-file-text-o" aria-hidden="true"></i>
                                            <a href="{{url('tin-tuc/'.$value->path)}}">{{$value->NewsGroup->name}}</a></span> 
                                        <span style="margin-left: 10px;margin-right: 10px">|</span>
                                        <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{$value->created_at->format('d-m-Y')}}</span>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>  
            
        </div>

    </div> 
</div>  



@endsection