@extends('layouts.web')
@section('title')
Tin tức
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:title', 'Danh mục tin tức nông sản, thực phẩm an toàn')
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')

<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1>Tin tức</h1></div>
    </div>
     <div class="wrap-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                    <li><a href="{{url('danh-muc-tin-tuc')}}" title="Tin tức">Tin tức</a></li>
                </ul>
            </div>
        </div>
    <div class="container">
       
        <div class="box-news">
            <div class="block-content">
                <div class="row is-flex">
                    @foreach ($NewsArticle as $key => $value)

                    <div class="col-md-4 col-sm-6 col-xs-12">
                        <div class="item news-content">
                            <a href="{{url('tin-tuc/'.$value->path)}}" class="img" style="background-image: url({{url("/image/0/0/".$value->img_path)}});">
                                <img src="{{url("/image/0/0/".$value->img_path)}}" alt="">
                            </a>
                            <h3><a href="{{url('tin-tuc/'.$value->path)}}">{{$value->title}}</a></h3>
                            <p>{{$value->summary}}</p>
                        </div>
                    </div>
                    @endforeach


                </div>
                {!! $NewsArticle->render() !!}
            </div>    
        </div>

    </div> 
</div> 
@endsection