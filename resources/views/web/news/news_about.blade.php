@extends('layouts.web')
@section('title')
{{$newsArticle->title}}
@endsection
@section('content')

<div class="wrap-page">
    <!-- <div class="block-title-h1">
        <div class="container"><h1>Tin tức</h1></div>
    </div> -->
    <div class="container">
        
        <div class="box-news">
            <div class="row">
                <div class="col-md-12">
                    <div class="article-title">{{$newsArticle->title}}</div>
                    <!-- <div class="info-help">
                        <span><i class="fa fa-television" aria-hidden="true"></i> {{$newsArticle->visits()->count()}} lượt xem</span>
                    </div> -->
                    <div class="article-detail">
                        {!! $newsArticle->content !!}
                    </div>
                </div>
            </div>  
            
        </div>

    </div> 
</div>  



@endsection