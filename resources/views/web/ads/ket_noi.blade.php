@extends('layouts.web')
@section('title')
Kết nối cung cầu
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')
<div class="buy-center" >
<!--    <div class="help-center-top">
        <div class="help-center-content">
            <h3 class="help_title">Kết nối cung cầu </h3>
            <button type="button" onclick="createAds();"  class="btn-lg btn-help">Đăng tin cung cầu</button>
        </div>			
    </div>-->
    <div class="wrap-page">
        <div class="wrap-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                       <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                        <li><a href="{{url('/ket-noi')}}" title="Sản phẩm">Kết nối cung cầu </a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="buy-center" >
                <div class="box-mark">
                    <div class="row wrap-box-mark">
                        <div class="col-md-4 col-xs-4">
                            <div class="col-mark">
                                <a href="{{route('ket-noi',['typeAds' => 'can-mua'])}}">
                                    <img src="{{url('css/images/canmua.png')}}">
                                    <div class="headingmark">Cần mua</div>
                                </a>		
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <div class="col-mark">
                                <a href="{{route('ket-noi',['typeAds' => 'can-ban'])}}">
                                    <img src="{{url('css/images/canban.png')}}">
                                    <div class="headingmark">Cần bán</div>
                                </a>		
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <div class="col-mark">
                                <a href="{{route('ket-noi',['typeAds' => 'tim-doi-tac'])}}">
                                    <img src="{{url('css/images/timdoitac.png')}}">
                                    <div class="headingmark">Tìm đối tác</div>
                                </a>		
                            </div>
                        </div>
                    </div>
                </div>	

                
                <div class="box-mark">
                    <div class="row wrap-box-mark">
                        <div class="col-md-4 col-xs-4">
                            <div class="col-mark">
                                <a href="{{route('ket-noi',['typeAds' => 'thong-tin-viec-lam'])}}">
                                    <img src="{{url('css/images/vieclaml.png')}}">
                                    <div class="headingmark">Việc làm</div>
                                </a>		
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <div class="col-mark">
                                <a href="{{route('ket-noi',['typeAds' => 'dich-vu-du-lich'])}}">
                                    <img src="{{url('css/images/dulichl.png')}}">
                                    <div class="headingmark">Du lịch</div>
                                </a>		
                            </div>
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <div class="col-mark">
                                <a href="{{route('ket-noi',['typeAds' => 'du-an-dau-tu'])}}">
                                    <img src="{{url('css/images/dautul.png')}}">
                                    <div class="headingmark">Đầu tư</div>
                                </a>		
                            </div>
                        </div>
                    </div>
                </div>	

                <div class="search-kncc">
                    <div class=""></div>
                    <div class="form-inline" >
                        
                        <div class="clearfix"></div>
                        <div class="col-md-3 col-100">
                            <div class="form-group" style="width: 100%">
                                <div class="input-group" style="width: 100%">
                                    {{-- @include('components.tree_product_category',array('tree' =>$tree,'level'=>0,'parent_id'=>null,'name'=>'product_category_id'))--}}
                                    @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>'Tất cả','parent_id'=>isset($typeProductId)?$typeProductId:null,
                                    'name'=>'product_category_id','placeholder'=>'Chọn danh mục'])
                                </div>
                            </div>
                        </div>

                        <div class="col-md-4 col-100">
                            <div class="form-group" style="width: 100%">
                                <div class="input-group" style="width: 100%">
                                    @include("components/select_search",['tree'=>$regions,"level"=>0,"path"=>null,"root"=>"Tất cả",
                                    'parent_id'=>isset($regionsId)?$regionsId:null,'name'=>'region_id',
                                    'placeholder'=>'Chọn Tỉnh/Huyện/Xã'])
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-100">
                            <div class="form-group" style="width: 100%">
                                <div class="input-group" style="width: 100%">
                                    <input style="width: 100%" value="{{isset($keyWord)? $keyWord:''}}" type="text" class="form-control" id="keyword" placeholder="Bạn cần tìm gì">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 col-100">
                            <div class="form-group" style="width: 100%">
                                <div class="input-group" style="width: 100%">
                                    <button onclick="setLocationAds()" class="btn-grid-search" id="btn-search-ads">Tìm kiếm</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-8">
                        <div class="row heading-page- ">
                            <div class="col-md-6  col-xs-12  block-h1 results-typeAds">
                               
                            </div>
                            
                            <div class="col-md-6  col-xs-12  block-h1">
                                <button class="button-dangtin" onclick="createAds();"><i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i>Đăng tin cung cầu</button>
                            </div>
                        </div>
                        <div class="buy-center-linked-list ">
                            @if(isset($ads))
                            <ul class="buy-center-linked-list-columns">
                                @foreach($ads as  $value) 
                                <li class="buy-li">
                                    <div class="col-md-9 col-sm-9 buy-left">
                                        <div class="news-buy">
                                            <div class="icon-buy-li">
                                                <a href="{{url('ket-noi'.'/'.$value->path)}}">
                                                  @if($value->advertisingImages->count()>0)
                                                        <img src="{{url("/image/300/300/".$value->advertisingImages->first()->image)}}">
                                                    @else
                                                        @if( strpos($value->advertisingable_type, 'User') )
                                                            <img @if(isset($value->createdByUser->UserProfile ) && isset($value->createdByUser->UserProfile->avatar)) src="{{url("/image/300/300/".$value->createdByUser->UserProfile->avatar)}}" @else src="{{url('css/images/no-image.png')}}" @endif >
                                                        @else
                                                            <img @if(isset($value->advertisingable->logo_image)) src="{{url("/image/300/300/".$value->advertisingable->logo_image)}}" @else src="{{url('css/images/no-image.png')}}" @endif>
                                                        @endif
                                                    @endif
                                                </a>
                                            </div>
                                        <div class="wrap-buy-li">
                                            <h4><a class="help-center-link" href="{{url('ket-noi'.'/'.$value->path)}}">{{$value->title}}</a></h4>
                                            <div class="info-buy">
                                                <span class="userport">
                                                    <span><i class="fa fa-user" aria-hidden="true"></i><a href="#">{{isset($value->advertisingable)? $value->advertisingable->name:""}}</a></span> 
                                                </span>
                                                <span style="margin-left: 5px;margin-right: 5px">|</span>
                                                <span class="port-time">		
                                                    <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $value->created_at->toShortDateString()}}</span>
                                                </span>
                                                <span style="margin-left: 5px;margin-right: 5px">|</span>
                                                <span class="port-time">		
                                                    <span><i class="fa fa fa-bars" aria-hidden="true"></i> {{ $value->advertisingCategory->name}}</span>
                                                </span>
                                                <span style="margin-left: 5px;margin-right: 5px">|</span>
                                                <span class="port-time">		
                                                    <span><i class="fa fa fa-television" aria-hidden="true"></i> {{ $value->visits()->count()}} lượt xem</span>
                                                </span>
                                            </div>
                                            <div class="post-desc">
                                                {{$value->content}}
                                            </div>
                                        </div>
                                    </div>

                                    </div>	
                                    <div class="col-md-3 col-sm-3 text-l">
                                        <div class="post-area">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            {{isset($value->region)? $value->region->full_name:''}}
                                        </div>
                                    </div>	
                                    <div class="clearfix"></div>
                                </li>	
                                @endforeach

                            </ul>	
                            {!! $ads->render() !!}
                            @endif
                        </div>
                    </div>	
                    <div class="col-md-4">	
                        @include('web.ads.left_content')
                    </div>
                </div> 
            </div>
        </div>	
    </div>
    @endsection
    @push('scripts')
    <script>
        var typeAds = "";
        function createAds() {
            if (!checkLogin()) {
                return false;
            }
            @if(getRoleAccess() == \App\RoleAccess::BUSINESS)
                location.href = "{{ route('advertising-org.create', ['organizationId' => getOrgModel()->id]) }}";
            @else
                location.href = "{{url('user/advertising/create')}}";
            @endif
        }

        function setLocationAds() {
            var typeAds = urlParam('typeAds') ? urlParam('typeAds') : '';
            var keyword = $('#keyword').val();
            var region_id = $('select[name=region_id]').val();
            var typeProduct = $('select[name=product_category_id]').val();
            window.location.href = window.location.href.replace(/[\?#].*|$/, "?typeAds=" + typeAds + "&keyword=" + keyword + "&typeProduct=" + typeProduct + "&region=" + region_id);
        }
        $(document).ready(function () {

            typeAds = urlParam('typeAds');
            if (typeAds !== null && typeAds !== 0) {
                if (typeAds === 'can-mua')
                    stypeAds = "Cần mua";
                else if (typeAds === 'can-ban')
                    stypeAds = "Cần bán";
                else if (typeAds === 'tim-doi-tac')
                    stypeAds = "Tìm đối tác";
                else if(typeAds === 'thong-tin-viec-lam')
                    stypeAds = 'Việc làm';
                else if(typeAds === 'dich-vu-du-lich')
                    stypeAds = 'Địa điểm du lịch';
                else if(typeAds === 'du-an-dau-tu')
                    stypeAds = 'Đầu tư dự án';
                var sDiv = "<h1> " + stypeAds + " :</h1>";
                sDiv += '<h4 name="results-count"> {{$ads->count()}} kết quả</h4>';
                $('.results-typeAds').html(sDiv);
            } else {
                $('.results-typeAds').html("<h1> Tin đăng mới nhất </h1>");
            }


            //$('select[name=product_category_id] option[parent_id='+typeProduct+']').attr('selected','selected');  
            //$('select[name=product_category_id]').text() = typeProduct;
        });
    </script>
    @endpush