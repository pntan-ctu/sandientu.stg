@extends('layouts.user')
@section('title')
    Đăng tin cung cầu
@endsection

@push('styles')
    <style>
        .icon {
            margin-right: 10px;
            z-index: 9999999;
        }

        .tree-search > span {
            width: 100% !important;
        }

        .icon_class_0 {
            color: #3300ff;
            font-weight: bold;
        }

        .icon_class_1 {
            color: #969696;
            font-weight: bold;
        }

        .icon_class_2 {
            color: #249878;
            font-weight: bold;
        }

        .icon_class_3 {
            color: #bcbf07;
            font-weight: bold;
        }
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #keyword{
            min-width: 200px;
        }
    </style>
@endpush
@section('content')

    <div class="user-container">
        <!-- <div class="head-buy">
            <h3 class="h3-buy">Lịch sử đăng tin cung cầu</h3>
            <button class="button-dangtin pull-right">
                <i class="fa fa-pencil" aria-hidden="true" style="margin-right: 10px;"></i>Đăng tin
            </button>
        </div> -->
        <div class="box" id="box-area">
            <div class="box-header">
                {!! Former::openInline()->id('search-form')->addClass('pull-left col-xs-12') !!}
                {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
                {!! Former::select('status', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chờ duyệt', '1' => 'Duyệt đăng', '2' => 'Huỷ duyệt']) !!}
                {!! Former::primary_submit('Tìm kiếm')->id('search') !!}
                <a class="btn btn-success right-button" href="{{route('advertising.create')}}">
                    <i class="fa fa-plus" aria-hidden="true" style="margin-right: 10px;"></i>Đăng tin mới
                </a>
                {!! Former::close() !!}
            </div>
            <div class="clearfix"></div>
            <!-- /.box-header -->
            <div class="box-body">
                @include('web/ads/table_view')
            </div>
            <!-- /.box-body -->
        </div>
    </div>
@endsection

@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/web/ads.js"></script>
    <script>
        $(function () {
            Ads('{{ route('advertising.index') }}');
            // $('.btn-dangtin').on('click', function (e) {
            //     location.href = "{{route('advertising.create')}}";
            // });
        });
    </script>
@endpush

