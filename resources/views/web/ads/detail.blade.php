@extends('layouts.web')
@section('title')
{{$ads->title}}
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')
<link href="{{ asset('css/jgallery.min.css?v=1.6.0') }}" rel="stylesheet">
<div class="buy-center" >
   
<!--    <div class="help-center-top">
        <div class="help-center-content">
            <h3 class="help_title">Kết nối cung cầu </h3>

            <button type="button" onclick="createAds();"  class="btn-lg btn-help">Đăng tin cung cầu</button>
        </div>			
    </div>-->
</div>
<div class="wrap-page detail-ads-wrap">
     <div class="wrap-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                    <li><a href="{{url('/ket-noi')}}" title="Sản phẩm">Kết nối cung cầu
                    </a></li>
                </ul>
            </div>
        </div>
    <div class="container">
        <div class="buy-center" >

            <div class="row">
                <div class="col-md-8">
                    <div class="buy-center-linked-list buy-center-linked-list-detail">
                        
                        <div class="row">
                            <div class="col-md-3 col-sm-4 col-xs-4 col-100">
                                <div class="messageUserBlock">
                                    <div class="avatarHolder"><img src="{{$avatar_url}}"></div>
                                    <div class="userText">
                                        <div class="userTitle"><a href="#" class="username" dir="auto" itemprop="name">{{$advertisingable_name}}</a></div>
                                        <div class="datejoin">Tham gia: @if($ads->advertisingable != null)  {{isset($ads->advertisingable->created_at)? $ads->advertisingable->created_at->toShortDateString():''}} @endif</div>
                                        <div class="area1">
                                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                                            {{$region_full_name}}
                                        </div>
                                        @if(isset($ads->advertisingable->tel))
                                        <!--<div class="sdt-buy"><a href="tel:{{$ads->advertisingable->tel}}"><i class="fa fa-phone" aria-hidden="true"></i><span style="font-size: 18px;">{{$ads->advertisingable->tel}}</span></a></div>-->
                                        <div class="sdt-buy"><a href="tel:{{$ads->tel}}"><img src="{{url('css/images/click-to-call-me.png')}}" alt="Gọi cho người đăng tin"/></a></div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-9 col-sm-8 col-xs-8 col-100 border-ads">
                                <div class="block-h1">
                                    <h1 >{{$ads->title}}</h1>
                                </div>
                                <div class="content-muaban">
                                    <div class="ngaydang">
                                        <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{$ads->created_at->toShortDateString()}}</span>
                                        <span style="margin-left: 10px;margin-right: 10px">|</span>
                                        <span><i class="fa fa-television" aria-hidden="true"></i> {{$ads->visits()->count()}} lượt xem</span>
                                    </div>
                                    <div class="news-buy-main">
                                        <div class="loaisp" style="margin-bottom: 8px;"><strong>Loại danh mục:</strong><span class="span-label">{{$ads->productCategory->name}}</span></div>
                                        <div><strong>Khu vực:</strong>   <span class="area"><i class="fa fa-map-marker" aria-hidden="true"></i>{{$ads->region->full_name}}</span></div>
                                        <div><strong>Địa chỉ liên hệ:</strong>   <span class="area"><i class="fa fa-map-marker" aria-hidden="true"></i>{{$ads->address}}</span></div>
                                        <div><strong>Điện thoại liên hệ:</strong> <span class="area"><i class="fa fa-phone" aria-hidden="true" style="width:30px; text-align: center;" ></i>{{$ads->tel}}</span></div>
                                        @if(isset($ads->to_date))
                                        @if($ads->advertisingCategory->name === 'Du lịch') 
                                        <div>
                                            <strong>Cước phí tham quan:</strong> <span class="area">
                                                <i class="fa fa-money" aria-hidden="true" style="width:30px; text-align: center;" >
                                                </i><?php echo number_format($ads->it5_cuocphithamquan); ?>đ /1 người / lượt</span>
                                        </div>
                                        @endif
                                        @if($ads->advertisingCategory->name === 'Việc làm') 
                                        <div>
                                            <strong>Mức Lương:</strong> <span class="area">
                                                <i class="fa fa-money" aria-hidden="true" style="width:30px; text-align: center;" >
                                                </i><?php echo number_format($ads->it5_mucluong); ?>đ /tháng</span>
                                        </div>
                                        @endif
                                         @if($ads->advertisingCategory->name === 'Tìm kiếm đầu tư') 
                                        <div>
                                            <strong>Quy mô dự án:</strong> <span class="area">
                                                <i class="fa fa-money" aria-hidden="true" style="width:30px; text-align: center;" >
                                                </i><?php echo number_format($ads->it5_quymoduan); ?> đ</span>
                                        </div>
                                        @endif
                                        <div>Ngày hết hạn: <span style="color: #ff5722;"> {{isset($ads->to_date)? $ads->to_date->toShortDateString():''}} </span></div>
                                        @endif
                                    </div>
                                    <div class="chitiet-buy">
                                        <p>{{$ads->content}}</p>
                                    </div>
                                    <div id="gallery" style="width:100%;">
                                        @if($ads->advertisingImages->count()>0)
                                        @foreach($ads->advertisingImages as  $image) 
                                        <img src="{{url("/image/550/550/".$image->image)}}" />
                                        @endforeach
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>	
                    </div>
                </div>	
                <div class="col-md-4">	
                    @include('web.ads.left_content')
                </div>
            </div> 
            <div class="block-h1" style="border-bottom: 1px solid #eee;width: 100%;">
                    <h1>Tin kết nối cung cầu</h1>
                </div> 
            <div class="buy-center-linked-list-columns-index">
                <div class="row is-flex">
                    @if(isset($ads_most))
                    @foreach($ads_most as  $value) 

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="buy-li1">
                            <div class="news-buy">
                                <div class="icon-buy-li">
                                    <a href="{{url('ket-noi'.'/'.$value->path)}}">
                                        <img @if($value->advertisingImages->count()>0) src="{{url("/image/80/80/".$value->advertisingImages->first()->image)}}" @else src="{{url('css/images/no-image.png')}}" @endif>
                                    </a>
                                </div>

                                <div class="wrap-buy-li">
                                    <h4><a class="post-link1" href="{{url('ket-noi'.'/'.$value->path)}}">{{$value->title}}</a></h4>
                                    <div class="info-buy">
                                        <span class="userport">
                                            <span><i class="fa fa-user" aria-hidden="true"></i>
                                                @if(isset($value->advertisingable))
                                                <a href="
                                                    {{
                                                        $value->advertisingable_type == 'App\Models\User' ?
                                                            $value->advertisingable->getUrlActivity() :
                                                            ($value->advertisingable_type == 'App\Models\Organization' ?
                                                                url('shop/'.$value->advertisingable->path) : "#")
                                                    
                                                    }}
                                                    ">
                                                    {{$value->advertisingable->name}}
                                                    @endif
                                                </a>
                                            </span>
                                        </span>
                                        <span style="margin-left: 1px;margin-right: 1px">|</span>
                                        <span class="port-time">		
                                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $value->created_at->toShortDateString()}}</span>
                                        </span>
                                        <span style="margin-left: 1px;margin-right: 1px">|</span>
                                        <span class="port-time">		
                                            <span><i class="fa fa fa-bars" aria-hidden="true"></i> {{ $value->advertisingCategory->name}}</span>
                                        </span>
                                    </div>
                                    <div class="post-desc">
                                        {{$value->content}}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>	
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>	
    @endsection
    @push('scripts')
    <script type="text/javascript" src="{{asset('js/jgallery/jgallery.min.js?v=1.6.0')}}"></script>
    <script type="text/javascript" src="{{asset('js/jgallery/touchswipe.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('js/jgallery/tinycolor-0.9.16.min.js')}}"></script>
    <script>
                function createAds() {
                    if (!checkLogin()) {
                        return false;
                    }
                    location.href = "{{url('user/advertising/create')}}";
                }
                $(function () {
                    $('#gallery').jGallery(
                        {
                            mode: 'slider',
                            browserHistory: false,
                            height: '500px',
                            width: '500px',
                        }
                    );
                });
    </script>
    @endpush