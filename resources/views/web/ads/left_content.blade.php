<div class="ncc-center-topic">
    <div class="block-h1">
        <h1>Nhà cung cấp tiêu biểu</h1>
    </div>


    @foreach($organization as  $value) 
    <div class="wrap-ncc-r match-my-cols is-flex1">
        <div class="ncc-rightlist media">
            <a class= "pull-left" href="{{url('shop/'.$value->path)}}" >
                <img class="img-ncc-right-list" title="" alt="" src="{{isset($value->logo_image)? url("/image/135/90/".$value->logo_image): url('css/images/no-image.png')}}">
            </a>
            <div class="media-body">
                <h4 class="media-heading"><a href="{{url('shop/'.$value->path)}}">{{$value->name}}</a></h4>
                 <p style="margin-bottom: 0"><b>Địa chỉ: </b>{{ $value->address }}</p>
                <!--<p>Sản phẩm: @foreach ($value->products as $key => $product) <a href="{{url('san-pham/'.$product->path)}}">{{ $product->name.";" }} </a> @endforeach</p>-->
            </div>
        </div>

    </div>  
    @endforeach    
</div>

