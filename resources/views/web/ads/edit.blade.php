<style type="text/css">
    .select2-container--default .select2-selection--single .select2-selection__rendered {
        padding: 0px
    }

    .tree-search > span {
        width: 100% !important
    }

    td {
        vertical-align: middle !important;
        text-align: center !important;
    }

    .grid_image div {
        padding: 2px;
    }

    .icon-close {
        position: absolute;
        top: 0px;
        right: 1px;
        color: #040404;
        cursor: pointer;
        opacity: 0.4;
        border-radius: 50%;
        background-color: #FFF;
        padding: 2px;
    }

    .image_avatar > img {
        width: 100%;
        height: 100%;
    }

    .grid_image > div:hover .icon-close {
        opacity: 1;
    }

    .add-image {
        margin-top: 20px;
    }

    #img-table > div {
        width: 100px;
    }

    #img-table > div img {
        width: 100px;
        height: 80px;
    }

    .radio-inline {
        margin-right: 20px;
    }

    .hidden-link-product {
        display: none;
    }
</style>
<?php Former::populate($ads) ?>
{!! Former::open(route('advertising.update', $ads->id), 'put' ) !!}
{!! Former::radios('category_id', 'Loại thông tin ')->radios($adsCate)->addClass('category_id')->inline()->required() !!}
@if(!$isMember)
    {!! Former::select('link_product_id','Sản phẩm liên kết ')->onGroupAddClass(($ads->category_id!=2)?'link_product hidden-link-product':'link_product')->addOption('--- Sản phẩm liên kết ---', 0)->fromQuery($productLink,'name','id') !!}
@endif
{!! Former::text('title','Tiêu đề ')->addClass('title')->required() !!}
{!! Former::textarea('content','Nội dung ')->rows(4)->required()  !!}

<div class="form-group required {{$errors->has('product_category_id')?'has-error':''}} ">
    <label for="product_category_id" class="control-label col-lg-2 col-sm-4">Loại sản phẩm
        <sup>*</sup></label>
    <div class="col-lg-10 col-sm-8 ">
        @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>null,'parent_id'=>(isset($ads)?$ads->product_category_id:null),
        'name'=>'product_category_id'])
    </div>
</div>

<div class="form-group required {{$errors->has('region_id')?'has-error':''}} ">
    <label for="region_id"
           class="control-label col-lg-2 col-sm-4 region-label"> @if($ads->category_id==1){{'Khu vực giao hàng'}}
        @elseif($ads->category_id==2){{'Khu vực bán hàng'}} @elseif($ads->category_id==3){{'Khu vực'}} 
        @elseif($ads->category_id==4){{'Khu vực tuyển dụng'}} @elseif($ads->category_id==5){{'Địa điểm du lịch'}} @else($ads->category_id==6){{'Địa chỉ tìm đầu tư'}} @endif<sup>*</sup></label>
    <div class="col-lg-10 col-sm-8 tree-search">
        @include("components/select_search",['tree'=>$regions,"level"=>0,"path"=>null,"root"=>null,
        'parent_id'=>(isset($ads)?$ads->region_id:null),'name'=>'region_id',
        'placeholder'=>'Chọn Tỉnh/Huyện/Xã'])
    </div>
</div>

{!! Former::text('to_date','Thời hạn đăng tin ')->addClass('toDate form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->required()->forceValue(($ads && $ads->to_date) ? $ads->to_date->toShortDateString() : null)!!}
@if($ads->category_id==5)
{!! Former::text("cuocphithamquan","Cước phí tham quan")->required()->forceValue($ads->it5_cuocphithamquan) !!}
@endif
@if($ads->category_id==4)
{!! Former::text("mucluong","Mức lương")->required()->forceValue($ads->it5_mucluong) !!}
@endif

{!! Former::text('address','Địa chỉ liên hệ ')->required() !!}
{!! Former::text('tel','Điện thoại ')->required() !!}
<input id="paths" type="hidden" name="paths[]" value=""/>
{!! Former::close() !!}

<form id="upload_form" method="post" enctype="multipart/form-data">
    <div class=" col-lg-12 add-image rm-padding">
        <div class="btn bg-maroon btn-file col-lg-2 col-sm-4 ">
            <i class="fa fa-picture-o"></i> Ảnh sản phẩm
            <input type="file" class="form-control" id="image_upload_input" name="files[]"
                   multiple/>
        </div>

        <div id="img-table" class="grid_image col-lg-10 col-sm-8 ">
            @if(isset($productImage))
                @foreach($productImage as $key=> $item)
                    <?php $index = $key + 1; ?>
                    <div class='col-xs-4'>
                        <i class='fa fa-close icon-close'></i>
                        <img path="{{$item->image}}" id="{{'drag'.$index}}"
                             draggable='true' ondragstart='drag(event)' width='100px' height='80px'
                             src="{{'image/0/0/'.$item->image}}"/>
                    </div>
                @endforeach
            @endif
        </div>

    </div>
</form>

<script type="text/javascript">
    
    $('.toDate').datepicker({
        language: 'vi',
        autoclose: true
    });

    $("#image_upload_input").on("change", function (event) {
        event.preventDefault();
        upload();
    });

    function upload() {
        sdcApp.showLoading(true);
        var formData = sdcApp.getFormDataAndType($("#upload_form"));
        $.ajax({
            url: 'upload/images',
            method: 'POST',
            data: formData.data,
            contentType: formData.contentType,
            dataType: 'json',
            processData: false
        }).done(function (rs) {
            if (rs.success) {
                addToTable(rs.data);
            }
            sdcApp.showLoading(false);
        }).fail(function () {
            sdcApp.showLoading(false);
        });
    }

    function addToTable(data) {
        for (var i = 0; i < data.length; i++) {
            $(".grid_image").append("<div class='col-xs-4 '><i class='fa fa-close icon-close'></i>" +
                "<img path='" + data[i].path + "' style='width: 100%;height: 80px;' src='" + 'image/100/80/' + data[i].path + "'></div>");
        }
        $(".grid_image i").click(function (e) {
            e.preventDefault();
            $(this).parent().remove();
        });
    }

    $(".grid_image i").click(function (e) {
        e.preventDefault();
        $(this).parent().remove();
    });

    $("#tel").inputmask({mask: "0|+9{9}", greedy: false});
    
</script>

