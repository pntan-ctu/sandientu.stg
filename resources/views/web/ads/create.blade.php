@extends('layouts.'.$layout)
@push('styles')
<script src="https://cdn.ckeditor.com/4.13.0/standard/ckeditor.js"></script>
    <style>
        td {
            vertical-align: middle !important;
            text-align: center !important;
        }

        .grid_image div {
            padding: 2px;
        }

        .icon-close {
            position: absolute;
            top: 0px;
            right: 1px;
            color: #040404;
            cursor: pointer;
            opacity: 0.4;
            border-radius: 50%;
            background-color: #FFF;
            padding: 2px;
        }

        .image_avatar {
            border: solid 1px gainsboro;
            padding: 4px;
            width: 200px;
            height: 200px;
        }

        .image_avatar > img {
            width: 100%;
            height: 100%;
        }

        .grid_image > div:hover .icon-close {
            opacity: 1;
        }

        .add-image {
            /*margin-top: 5px;*/
        }

        #img-table > div {
            width: 100px;
        }

        .radio-inline {
            margin-right: 20px;
        }

        .hidden-link-product {
            display: none;
        }
    </style>
@endpush
@section('title')
    Đăng tin cung cầu
@endsection
@section('content')
    <div class="form-ads-container">
        <div class="toolbarUser">
            <div class="user-info-right ">
                <?php Former::populate($ads) ?>
                {!! Former::open() !!}
                {!! Former::hidden('id')->id("id") !!}
                {!! Former::setOption('TwitterBootstrap3.labelWidths', ['large' => 3, 'small' => 4]) !!}
                {!! Former::radios('category_id', 'Loại thông tin ')->radios($adsCate)->addClass('category_id')->inline()->check(1)->required() !!}
                @if(!$isMember)
                    {!! Former::select('link_product_id','Sản phẩm liên kết ')->onGroupAddClass('link_product hidden-link-product')->addOption('--- Sản phẩm liên kết ---', 0)->fromQuery($productLink,'name','id') !!}
                @endif
                {!! Former::text('title','Tiêu đề ')->addClass('title')->required() !!}
                {!! Former::textarea('content','Nội dung ')->rows(15)->required()  !!}
                <script>
                        CKEDITOR.replace( 'content' );
                </script>

                <div id="loaisanpham" class="form-group required {{$errors->has('product_category_id')?'has-error':''}} ">
                    <label for="product_category_id" class="control-label col-lg-3 col-sm-4">Loại sản phẩm
                        <sup>*</sup></label>
                    <div class="col-lg-9 col-sm-8 ">
                        @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>null,'parent_id'=>null,
                        'name'=>'product_category_id'])
                    </div>
                </div>

                <div class="form-group required {{$errors->has('region_id')?'has-error':''}} ">
                    <label for="region_id" class="control-label col-lg-3 col-sm-4 region-label">Khu vực giao
                        hàng <sup>*</sup></label>
                    <div class="col-lg-9 col-sm-8 tree-search">
                        @include("components/select_search",['tree'=>$regions,"level"=>0,"path"=>null,"root"=>null,
                        'parent_id'=>null,'name'=>'region_id',
                        'placeholder'=>'Chọn Tỉnh/Huyện/Xã'])
                    </div>
                </div>

                {!! Former::text('to_date','Thời hạn đăng tin ')->addClass('toDate form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->required()->forceValue(($ads && $ads->to_date) ? $ads->to_date->toShortDateString() : null)!!}

                    <div style="display: none;" id="cuocphi" class="form-group required {{$errors->has('product_category_id')?'has-error':''}} ">
                    <label for="product_category_id" class="control-label col-lg-3 col-sm-4">Cước phí tham quan
                        <sup>*</sup></label>
                    <div id="ipcuocphi" class="col-lg-9 col-sm-8 ">
                       
                    </div>
                </div>

                 <div style="display: none;" id="mucluong" class="form-group required {{$errors->has('product_category_id')?'has-error':''}} ">
                    <label for="product_category_id" class="control-label col-lg-3 col-sm-4">Mức lương
                        <sup>*</sup></label>
                    <div id="ipmucluong" class="col-lg-9 col-sm-8 ">
                       
                    </div>
                </div>

                <div style="display: none;" id="quymo" class="form-group required {{$errors->has('product_category_id')?'has-error':''}} ">
                    <label for="product_category_id" class="control-label col-lg-3 col-sm-4">Quy mô dự án
                        <sup>*</sup></label>
                    <div id="ipquymo" class="col-lg-9 col-sm-8 ">
                       
                    </div>
                </div>
                {!! Former::text('address','Địa chỉ liên hệ ')->required() !!}
                {!! Former::text('tel','Điện thoại ')->required() !!}
                <input id="paths" type="hidden" name="paths[]" value=""/>
                {!! Former::close() !!}

                <div class="row">
                    <form id="upload_form" method="post" enctype="multipart/form-data">
                        <div class="add-image rm-padding row">
                            <div class="col-lg-3 col-md-3 col-sm-4 col-xs-4" style="text-align: right;">
                                <div class="btn bg-maroon btn-file">
                                    <i class="fa fa-picture-o"></i> <span id="anh">Ảnh sản phẩm</span>
                                    <input type="file" class="form-control" id="image_upload_input" name="files[]" multiple/>
                                </div>    
                            </div>

                            <div id="img-table" class="grid_image col-lg-9 col-md-8  col-sm-4 col-xs-8 ">

                            </div>

                        </div>
                    </form>
                </div>
                <div class="form-group pull-right" style="margin-bottom: 0">
                    <!--<div class="col-md-6 offset-md-4">-->
                    <button type="button" class="btn btn-primary btn-creat-ads">
                        {{ __('Lưu thông tin') }}
                    </button>
                    <!--</div>-->
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
    <script>
        $('#link_product_id').select2({
            width: '100%',
            placeholder: "--- Chọn sản phẩm đã có trong cơ sở ---"
        }).on('select2:select', function (e) {
            $('.grid_image').empty();
            var data = e.params.data;
            if ($('#id').val() == "") {
                if (data.id == 0) {
                    $(".grid_image").empty();
                    $('.title').val('');
                    return;
                }
                $('.title').val(data.text);
                sdcApp.showLoading(true);
                $.ajax({
                    url: "{{url('user/advertising/product_link')}}",
                    method: 'GET',
                    data: {
                        product_id: data.id
                    },
                    dataType: 'json',
                    // processData: false
                }).done(function (result) {
                    sdcApp.showLoading(false);
                    $("#product_category_id").val(result.product[0].product_category_id).trigger("change");
                    addImageToGrid(result.image);
                }).fail(function (xhr) {
                    sdcApp.showLoading(false);
                    if (xhr.status === 422) {
                        sdcApp.showLoading(false);
                    }
                });
            }
        });

        $('.toDate').datepicker({
            language: 'vi',
            autoclose: true
        });

        $("#image_upload_input").on("change", function (event) {
            event.preventDefault();
            upload();
        });

        function upload() {
            sdcApp.showLoading(true);
            var formData = sdcApp.getFormDataAndType($("#upload_form"));
            $.ajax({
                url: 'upload/images',
                method: 'POST',
                data: formData.data,
                contentType: formData.contentType,
                dataType: 'json',
                processData: false
            }).done(function (rs) {
                if (rs.success) {
                    addToTable(rs.data);
                }
                sdcApp.showLoading(false);
            }).fail(function () {
                sdcApp.showLoading(false);
            });
        }

        function addImageToGrid(data) {
            for (var i = 0; i < data.length; i++) {
                $(".grid_image").append("<div class='col-xs-4 '><i class='fa fa-close icon-close'></i>" +
                    "<img path='" + data[i].image + "' style='width: 100%;height: 80px;' src='" + 'image/100/80/' + data[i].image + "'></div>");
            }
            $(".grid_image i").click(function (e) {
                e.preventDefault();
                $(this).parent().remove();
            });
        }

        function addToTable(data) {
            for (var i = 0; i < data.length; i++) {
                $(".grid_image").append("<div class='col-xs-4 '><i class='fa fa-close icon-close'></i>" +
                    "<img path='" + data[i].path + "' style='width: 100%;height: 80px;' src='" + 'image/100/80/' + data[i].path + "'></div>");
            }
            $(".grid_image i").click(function (e) {
                e.preventDefault();
                $(this).parent().remove();
            });
        }

        $(function () {
            $('.btn-creat-ads').on('click', function (e) {
                sdcApp.showLoading(true);
                var paths = [];
                var path_avatar = $(".image_avatar>img").attr('path');
                $(".grid_image").children('div').each(function () {
                    paths.push($(this).children('img').attr('path'));
                });
                $("#paths").val(paths);
                $("#path_avatar").val(path_avatar);

                var form = $(".form-horizontal");
                var dataForm = form.serialize();
                // var form_data = sdcApp.getFormDataAndType($(".form-input"));
                $.ajax({
                    url: "{{isset($organizationId)?url('business/'.$organizationId.'/advertising-org'):url('user/advertising')}}",
                    method: 'POST',
                    data: dataForm,
                    dataType: 'json',
                    processData: false
                }).done(function (msg) {
                    sdcApp.showLoading(false);
                    toastr[msg.status](msg.msg);
                    window.location.href = '{{isset($organizationId)?route("advertising-org.index",['organizationId'=>$organizationId]):route("advertising.index")}}'
                    // $('.form-input').trigger("reset");
                }).fail(function (xhr) {
                    sdcApp.showLoading(false);
                    if (xhr.status === 422) {
                        sdcApp.resetFormErrors(form);
                        var errors = xhr.responseJSON.errors;
                        sdcApp.renderFormErrors(errors, form);
                        sdcApp.showLoading(false);
                        sdcApp.showToast({status: 'error', msg: 'Vui lòng nhập đủ thông tin!'});
                    }
                });
            });

            $(".category_id").on('change', function () {
                if ($(this).val() == '1') {
                    $('.region-label').html('Khu vực giao hàng <sup>*</sup>');
                    $("#loaisanpham").show();
                    $("#cuocphi").hide();
                     $("#quymo").hide();
                    $("#mucluong").hide();
                    if ($(".link_product").length > 0) {
                        $(".link_product").addClass('hidden-link-product');
                        $(".grid_image").empty();
                        $('.title').val('');
                    }
                }
                else if ($(this).val() == '2') {
                    $('.region-label').html('Khu vực bán hàng <sup>*</sup>');
                    $("#loaisanpham").show();
                     $("#quymo").hide();
                    $("#cuocphi").hide();
                    $("#mucluong").hide();
                    if ($(".link_product").length > 0) {
                        $(".link_product").removeClass('hidden-link-product');
                        $('#link_product_id').val(0).trigger('change');
                    }
                }
                else if ($(this).val() == 3){
                    $('.region-label').html('Khu vực <sup>*</sup>');
                    $("#loaisanpham").show();
                    $("#cuocphi").hide();
                     $("#quymo").hide();
                    $("#mucluong").hide();
                    if ($(".link_product").length > 0) {
                        $(".link_product").addClass('hidden-link-product');
                        $(".grid_image").empty();
                        $('.title').val('');
                    }
                }
                else if($(this).val() == 4){
                    $('.region-label').html('Khu vực tuyển dụng <sup>*</sup>');
                    // $("#loaisanpham").show();
                    $("#cuocphi").hide();
                     $("#quymo").hide();
                    $("#mucluong").show();
                    $("#ipmucluong").html('{!! Former::text("mucluong","")->required() !!}')
                    $("#anh").html('Ảnh minh hoạ');
                    if ($(".link_product").length > 0) {
                        $(".link_product").addClass('hidden-link-product');
                        $(".grid_image").empty();
                        $('.title').val('');
                    }
                }
                else if($(this).val() == 5){
                    $('.region-label').html('Địa chỉ điểm du lịch <sup>*</sup>');
                    // $("#loaisanpham").hide();
                    $("#ipcuocphi").html('{!! Former::text("cuocphithamquan","")->required() !!}')
                    $("#cuocphi").show();
                    $("#mucluong").hide();
                    $("#quymo").hide();
                    $("#anh").html('Hình ảnh điểm du lịch');
                    $("#cuocphithamquan").show();
                    if ($(".link_product").length > 0) {
                        $(".link_product").addClass('hidden-link-product');
                        $(".grid_image").empty();
                        $('.title').val('');
                    }
                }
                else if($(this).val() == 6){
                    $('.region-label').html('Địa chỉ tìm đầu tư <sup>*</sup>')
                    $("#cuocphi").hide();
                    $("#mucluong").hide();
                    $("#ipquymo").html('{!! Former::text("quymoduan","")->required() !!}')
                    $("#quymo").show();
                    $("#anh").html('Hình ảnh dự án');
                    if ($(".link_product").length > 0) {
                        $(".link_product").addClass('hidden-link-product');
                        $(".grid_image").empty();
                        $('.title').val('');
                    }
                }

            });

            $("#tel").inputmask({mask: "0|+9{9}", greedy: false});

        });
        $("cuocphithamquan").hide();
    </script>
@endpush