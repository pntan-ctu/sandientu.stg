<div class="modal fade" id="modal-question" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Gửi câu hỏi </h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive">
                    <div class="box-body">
                        <div class="form-area">  
                            <form role="form" id="frm_Question">
                                <div class="form-group">
                                    <label for="name" class="control-label">Họ tên</label>
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Họ tên" required>
                                </div>
                                <div class="form-group">
                                    <label for="email" class="control-label">Email</label>
                                    <input type="text" class="form-control" id="email" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label for="mobile" class="control-label">Điện thoại</label>
                                    <input type="text" class="form-control" id="mobile" name="mobile" placeholder="Điện thoại">
                                </div>
                                <div class="form-group">
                                    <label for="address" class="control-label">Địa chỉ</label>
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Địa chỉ">
                                </div>
                                <div class="form-group">
                                    <label for="title" class="control-label">Câu hỏi</label>
                                    <input type="text" class="form-control" id="title" name="title" placeholder="Câu hỏi" required>
                                </div>
<!--                                <div class="form-group">
                                    <label for="content" class="control-label">Nội dung chi tiết</label>
                                    <textarea class="form-control" type="textarea" name="content" id="content" placeholder="Nội dung chi tiết" rows="3" required></textarea>
                                </div>-->
                                <div class="form-group">
                                    <label for="" class="col-sm-3 control-label">Xác thực:</label>
                                    <div class="col-sm-9">
                                        <div class="g-recaptcha" data-sitekey="{{config('app.google_nocaptcha_sitekey')}}"></div>
<!--                                        @if ($errors->has('g-recaptcha-response'))
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif-->
                                    </div>
                                   
                                </div>
                                 <span class="invalid-feedback" role="alert"></span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Hủy</button>
                <button id="btn-Question" type="button" class="btn btn-primary" onClick='saveQuestion()'><i class="fa fa-check"></i>Gửi</button>
            </div>
        </div>
    </div>
</div>