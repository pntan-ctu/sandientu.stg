
<div class="help-center-topic">
    <h3 class="help-center-topic-tit">Câu hỏi thường gặp</h3>
    <ul class="help-center-topic-list">
        @if(isset($commonHelps))
        @foreach($commonHelps as $key =>  $value)
            <li class="help-center-topic-list-item">
            <a class="help-center_link" href="{{url('tro-giup/'.$value->path)}}">
                <span class="numberLeft">{{$key+1}}</span>
                <!--<span class="help_icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt="Tài khoản của tôi"></span>-->
                <span class="help-link-text">{{$value->question}}</span>
            </a>
        </li>
        @endforeach
        @endif
<!--        <li class="help-center-topic-list-item">
            <a class="help-center_link" href="#">
                <span class="help_icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt="Tài khoản của tôi"></span>
                <span class="help-link-text">Hướng dẫn mua hàng</span>
            </a>
        </li>
        <li class="help-center-topic-list-item">
            <a class="help-center_link" href="#">
                <span class="help_icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt=""></span>
                <span class="help-link-text">Sản phẩm bán trên ATTPTHA</span>
            </a>
        </li>
        <li class="help-center-topic-list-item">
            <a class="help-center_link" href="#">
                <span class="help_icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt=""></span>
                <span class="help-link-text">Thanh toán</span>
            </a>
        </li>
        <li class="help-center-topic-list-item">
            <a class="help-center_link" href="#">
                <span class="help_icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt=""></span>
                <span class="help-link-text">Vận chuyển hàng hóa</span>
            </a>
        </li>
        <li class="help-center-topic-list-item">
            <a class="help-center_link" href="#">
                <span class="help_icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt=""></span>
                <span class="help-link-text">Đổi trả hàng</span>
            </a>
        </li>
        <li class="help-center-topic-list-item">
            <a class="help-center_link" href="#">
                <span class="help_icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt=""></span>
                <span class="help-link-text">Hoàn tiền</span>
            </a>
        </li>
        <li class="help-center-topic-list-item">
            <a class="help-center_link" href="#">
                <span class="help_icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt=""></span>
                <span class="help-link-text">Chủ đề khác</span>
            </a>
        </li>-->
    </ul>
</div>

