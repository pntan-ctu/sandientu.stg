@extends('layouts.web')
@section('title')
Trợ giúp
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')
<div>
    <div class="container">
        <div class="wrap-breadcrumb">
            <div class="content-center">
<!--                <ul class="breadcrumb">
                    <li><a href="#" title="Trang chủ">Trang chủ</a></li>
                    <li><a href="#" title="Sản phẩm">Trợ Giúp</a></li>
                </ul>-->
            </div>
        </div>
        <div class="help-center _page" >
            <div class="help-center-top">
                <div class="help-center-content">
                    <h3 class="help_title">Chúng tôi giúp gì được cho bạn? </h3>
                    <p>Cơ quan chức năng sẽ trả lời câu hỏi của bạn</p>
                    <button type="button" class="btn-lg btn-help" onclick="showModalQuestion()">Gửi câu hỏi</button>
                </div>			
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="help-center-linked-list ">
                        <h3 class="h3-help">Danh sách Câu hỏi</h3>
                        <ul class="help-center-linked-list-columns">
                            @if(isset($questions))
                            <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                                @foreach($questions as $key =>  $value) 
                                <div class="card">
                                    <div class="help-li">
                                        <div class="wrap-help-li">
                                            <!--<a role="button" class="item-question collapsed" data-toggle="collapse" href="#collapse{{$key}}" aria-expanded="false" aria-controls="collapse1a">-->
                                            <a role="button" class="item-question" href="{{url('tro-giup/'.$value->path)}}">
                                                <!--<span class="numberLeft">{{$key+1}}</span>-->
                                                <span class="help-icon-wrapper"><img class="" src="{{url('css/images/hoi.png')}}" alt="Tài khoản của tôi"></span>
                                                <span class="help-center-link">{{$value->question}} </span> 
                                                <div class="info-help">
                                                    <span><i class="fa fa-user" aria-hidden="true"></i>{{$value->name}}</span> 
                                                    <span style="margin-left: 10px;margin-right: 10px">|</span>
                                                    <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{$value->created_at->toShortDateString()}}</span>
                                                </div>
                                            </a>
                                        </div>

                                    </div>
                                </div>

                                @endforeach
                            </div>
                             {!! $questions->render() !!}
                            @endif

                        </ul>	
                    </div>
                </div>	
                <div class="col-md-4">	
                    @include('web.trogiup.common_right')
                </div>
            </div> 
        </div>
    </div>	
    @include('web.trogiup.modal_question')
    @endsection
    @push('scripts')
    <!--<script src="{{ asset('js/sdc.js') }}"></script>-->
    <script>

        function showModalQuestion() {
            $('#modal-question').modal();
        }

        function validateQuestion() {
            if ($('#name').val().trim() === '') {
                alert("Bạn chưa nhập họ tên");
                $('#name').focus();
                return false;
            }
            if ($('#title').val().trim() === '') {
                alert("Bạn chưa nhập tiêu đề");
                $('#title').focus();
                return false;
            }
//            if ($('#content').val().trim() === '') {
//                alert("Bạn chưa nhập nội dung");
//                $('#content').focus();
//                return false;
//            }
            return true;
        }

        function saveQuestion() {
            var form_data = new FormData();
            if (!validateQuestion())
                return false;
            form_data.append('name', $('#name').val());
            form_data.append('email', $('#email').val());
            form_data.append('mobile', $('#mobile').val());
            form_data.append('title', $('#title').val());
//            form_data.append('content', $('#content').val());
            form_data.append('g-recaptcha-response', $('#g-recaptcha-response').val());
            $.ajax({
                url: "{{url('add-question')}}",
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
            }).done(function (msg) {
                if (msg.status === 'success') {
                    toastr.success(msg.msg);
                    $('#frm_Question').trigger("reset");
                    $('.invalid-feedback').html('');
                    grecaptcha.reset();
                    $('#modal-question').modal('hide');
                    
                }
            }).fail(function (xhr) {
                $('.invalid-feedback').html('');
                var form = $('frm_Question');
                if (xhr.status === 422) {
                    resetFormErrors(form);
                    var errors = xhr.responseJSON.errors;
                    renderFormErrors(errors, form);
                }
            });
        }
        var resetFormErrors = function ($form) {
            $form.find('.form-group').removeClass('has-error');
            $form.find('.form-group').find('.error-block').remove();
        }
        var renderFormErrors = function (errors, $form) {
            $.each(errors, function (field, message) {
                var $error = $('<p class="help-block error-block">' + message[0] + '</p>');
                var element = $form.find('[name=' + field + ']');
                var inputGroup = element.parent('.input-group');
                element.closest('.form-group').addClass('has-error');
                // nếu thuộc formgroup, insert msg sau thẻ div .input-group
                if (inputGroup.length) {
                    $error.insertAfter(inputGroup);
                }
                // nếu được cấu hình dùng select2, insert msg sau điều khiển select2
                else if (element.hasClass('select2-hidden-accessible')) {
                    $error.insertAfter(element.next());
                }
                // insert msg ngay sau element
                else {
                    $('.invalid-feedback').append('<p class="help-block error-block">' + message[0] + '</p>');
                }
            });
        }
//        $(document).on('ready', function () {
//
//        });
    </script>
    @endpush
