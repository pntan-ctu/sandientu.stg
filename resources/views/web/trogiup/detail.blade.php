@extends('layouts.web')
@section('title')
Trợ giúp
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')
<div class="block-videos">
    <div class="wrap-page">
         <div class="help-center-top">
                <div class="help-center-content">
                    <h3 class="help_title">Chúng tôi giúp gì được cho bạn? </h3>
                    <p>Cơ quan chức năng sẽ trả lời câu hỏi của bạn</p>
                    <button type="button" class="btn-lg btn-help" onclick="showModalQuestion()">Gửi câu hỏi</button>
                </div>			
            </div>
        <div class="container">
            <div class="col-md-8">
                <div class="block-videos-content">
                    @if(isset($questions))
                  
                    <h2 class="title-detail">{{$questions->question}}</h2>
                    <p class="time-news">
                        <span><i class="fa fa-clock-o"></i> {{$questions->created_at->toShortDateString()}}</span>
                        <span>|</span>
                    </p>
                    
                    <div class="content-answer-body">
                        {{$questions->answers}}
                    </div>
                    
                    @endif
                   
                </div>
            </div>
            <div class="col-md-4">
               @include('web.trogiup.common_right')
            </div>
        </div>	
    </div>
</div>
@endsection
