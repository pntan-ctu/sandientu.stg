@extends('layouts.web')

@section('title')
Thông tin phản hồi
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@push('styles')

<link href="{{ asset('css/contact.css') }}" rel="stylesheet">
<style>
    .select2-container .select2-selection--single {
    height: 34px;
    }  
    .select2-container--default .select2-selection--single .select2-selection__rendered {
    line-height: 32px;
}
</style>
@endpush
@section('content')
<div class="blocks-contact">
    <div class="wrap-page">
        <div class="wrap-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                    <li><a href="{{url('lien-he')}}" title="Văn bản">Liên hệ, phản hồi</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="content-center" >
                <div class="row">
                    <div class="col-md-8">
                        <div class="blocks-contact-title">
                            <h3>Thông tin phản hồi</h3>
                        </div>
                        <div class="blocks-contact-title-conment">
                            <p style="font-style: italic;">Cảm ơn Quý khách hàng đã tin tưởng sử dụng phần mềm Kết nối cung cầu sản phẩm nông sản, thực phẩm an toàn. Nếu bạn có các vấn đề cần phản ánh với các cơ quan quản lý an toàn thực phẩm, mời bạn gửi phản hồi về cho chúng tôi theo mẫu sau:</p>
                        </div>
                        <div class="group">
                           <form role="form" method="Post" action="lien-he"  enctype="multipart/form-data" class="row formcontact">
                               <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                @if(count($errors)>0)
                                <div class="alert alert-danger">
                                    @foreach($errors->all() as $err)
                                        {{$err}}<br>
                                    @endforeach
                                </div>
                                @endif
                                @if(session('thongbao'))
                                <div class="alert alert-success">
                                    {{session('thongbao')}}
                                </div>
                                @endif
                                <div class="col-xs-6 " style="margin-bottom:20px;">
                                    <input type="text" class="form-control" id="name" name="name" placeholder="Họ tên *" required>
                                </div>
                                <div class="col-xs-6" style="margin-bottom:20px;">
                                    <input type="number" class="form-control" id="mobile" name="phone" placeholder="Điện thoại" required>
                                </div>
                                <div class="col-xs-6" style="margin-bottom:20px;">
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Email" required>
                                </div>
                                <div class="col-xs-6" style="margin-bottom:10px;">
                                    
                                    @include("components/select_search",['tree'=>$orgTypeTree,"level"=>0,"path"=>null,"root"=>null,
                                        'parent_id'=>null,'name'=>'org_id','placeholder'=>'Chọn cơ quan tiếp nhận phản hồi *'])
                                    <input type="hidden" id="org" name="org" >
                                </div>
                                <div class="col-xs-12" style="margin-bottom:20px;">
                                    <input type="text" class="form-control" id="subject" name="title" placeholder="Tiêu đề *" required>
                                </div>
                                <div class="col-xs-12" style="margin-bottom:20px;">
                                        <textarea class="form-control" name="content" type="textarea" id="message" placeholder="Nội dung chi tiết *" required rows="7"></textarea>
                                </div>
                                <div class="col-xs-12" style="margin-bottom:20px;">
                                        <label class="control-label col-lg-2 col-sm-4">Tệp đính kèm: </label>
                                        <input style="border:none;width: auto;" type="file" id="files" name="files[]" multiple/>
                                        &nbsp; <span class="label label-info" id="upload-file-info"></span>
                                </div>
                                <div class="col-xs-12" style="margin-bottom:20px;">
                                    <label for="" class="control-label col-lg-2 col-sm-4">Xác thực:</label>
                                    <div class="btn btn-default btn-file">
                                        <div class="g-recaptcha" data-sitekey="{{config('app.google_nocaptcha_sitekey')}}"></div>
                                        @if ($errors->has('g-recaptcha-response'))
                                            <span class="invalid-feedback" role="alert">
                                            <strong>{{ $errors->first('g-recaptcha-response') }}</strong>
                                        </span>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-xs-12" style="margin-bottom:10px; ">
                                    <p style="font-style: italic;">Các ô có dấu <span>*</span> cần điền đầy đủ thông tin. Bạn có thể chọn nhiều file đính kèm để gửi. <br>
                                      Chúng tôi cam kết mọi thông tin phản hồi sẽ được bảo mật và chỉ sử dụng thông tin vào mục đích bảo vệ người tiêu dùng.</p>
                                </div>
                                <div class="col-xs-12" style="margin-bottom:10px;text-align: center;">
                                    <button  type="submit" id="submit" name="submit" class="btn btn-success">Gửi phản hồi</button>
                                    <button  type="reset" name="submit" class="btn btn-default1" id="searchReset">Làm mới</button>
                                </div>
                              </form> 
                        </div>
                    </div>
                    @include('web.contact.org_right')
                </div>
            </div>	
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $('select[name="org_id"]').change(function() {
        $('#org').val(this.value); 
    });
</script>
<script src='https://www.google.com/recaptcha/api.js'></script>
@endpush

