<div class="col-md-4">
    <div class="blocks-contact-title">
        <h3>Thông tin liên hệ</h3>
    </div>

    <form class="form-inline searchlh" action="{{url('lien-he')}}" method="get">
        <input style="width: 80%" placeholder="Tìm kiếm cơ quan quản lý" id="keyword" type="text" name="keyword" >
        <button class="iconSearchHeader" style="width: 18%;"><i class="fa fa-search"></i></button>
    </form>
    <div class="grid-news">
        @if(isset($org))
        @foreach ($org as $key => $value)
        <div class="media">
            <div class="media-body">
                <h4 class="media-news media-heading">{{$value->name}}</h4>
                <div class="info-help" style="font-size: 13px;color: #666">
                    <div><i class="fa fa-map-marker" aria-hidden="true"></i>
                       {{$value->address}}
                   </div>
                   <div>
                        <span><i class="fa fa-phone" aria-hidden="true"></i>{{$value->tel}} </span>
                        <span> | <i class="fa fa-envelope" aria-hidden="true"></i>{{$value->email}}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        @endforeach
        @endif
        <div style="text-align:center;padding-top: 10px; margin-bottom: 20px;">
        {{$org->links()}}
        </div>
    </div>  
</div>
