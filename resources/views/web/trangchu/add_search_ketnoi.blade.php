@foreach( $data["data"] as $item )
<div class="col-md-6 col-sm-6 col-xs-12">
    <div class="buy-li1">
        <div class="news-buy">
            <div class="icon-buy-li">
                <a href="#">
                    <img src="{{ isset($item->image)? url('image/80/80/'.$item->image): url('css/images/no-image.png') }}" />
                </a>
            </div>
            <div class="wrap-buy-li">
                <h4><a class="post-link1" href="{{url('ket-noi'.'/'.$item->path)}}">{{ $item->title }}</a></h4>
                <div class="info-buy">
                    <span class="userport">
                        <span><i class="fa fa-user" aria-hidden="true"></i><a href="#">{{ $item->name }}</a></span>
                    </span>
                    <span style="margin-left: 1px;margin-right: 1px">|</span>
                    <span class="port-time">
                        <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{ Carbon\Carbon::createFromTimeString($item->created_at)->toShortDateString() }}</span>
                    </span>
                    <span style="margin-left: 1px;margin-right: 1px">|</span>
                    <span class="port-time">
                        <span><i class="fa fa fa-bars" aria-hidden="true"></i>
                            @if($item->category_id == 1)
                            Cần mua
                            @elseif($item->category_id == 2)
                            Cần bán
                            @else
                            Tìm đối tác
                            @endif
                        </span>
                    </span>
                </div>
                <div class="post-desc">{{ $item->content }}</div>
            </div>
        </div>
    </div>
</div>
@endforeach