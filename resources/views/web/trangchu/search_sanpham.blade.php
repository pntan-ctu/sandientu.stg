@extends('layouts.web')
@section('title')
    Kết quả tìm kiếm
@endsection
@section('content')
    <style>
        .thumb-product.item .img img { width: 100%; height: 100%;}
    </style>
    <div class="contentMain">
        <div class="wrap-page">
            <div class="wrap-breadcrumb">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#" title="Trang chủ">Trang chủ</a></li>
                        <li><a href="/product" title="Sản phẩm">Tìm kiếm</a></li>
                        <li><a href="#" title="Thịt Bò Gác Bếp">{{ $text }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="container">
                <p class="cc-senoti">Tìm thấy <strong>{{ $data["count"] }}</strong> kết quả với từ khóa <strong>"{{ $text }}"</strong></p>
                <div class="menusearch cc-searchtab">
                    <ul class="sort-list nav nav-cc">
                        <li data-order="newest">
                            <a href="{{url('search?text='.$text.'&type=ket-noi&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Kết nối cung cầu</a>
                        </li>
                        <li class="active" class="" data-order="top_seller">
                            <a href="{{url('search?text='.$text.'&type=san-pham&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Sản phẩm</a>
                        </li>
                        <li class="" data-order="discount_percent,desc">
                            <a href="{{url('search?text='.$text.'&type=co-so&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Cơ sở SX - KD</a>
                        </li>
                        <li class="" data-order="discount_percent,desc">
                            <a href="{{url('search?text='.$text.'&type=tin-tuc&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Tin tức</a>
                        </li>
                        <li class="" data-order="discount_percent,desc">
                            <a href="{{url('search?text='.$text.'&type=dia-diem&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Địa điểm</a>
                        </li>
                    </ul>
                </div>
                <div class="row cc-serow">
                    <div class="col-md-9 cc-secol1">
                        <div class="product-search">
                            <div class="heading-box-">
                                <div class="block-h3">
                                    <a href="{{url('search?text='.$text.'&type=ket-noi&choose=lien-quan')}}">
                                        <h3>Sản phẩm: </h3>
                                        <h4 name="results-count"> {{ $data["sanpham"]["total_row"] }} kết quả</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="btn-group sort-box cc-searchtab">
                                <ul class="sort-list nav nav-cc" style="text-align: right">
                                    <li>Sắp xếp theo: </li>
                                    <li @if($data["sanpham"]["sort_type"] == 0) class="active" @endif data-order="newest">
                                        <a href="{{url('search?text='.$text.'&type=san-pham&choose=lien-quan')}}">ĐỘ LIÊN QUAN</a>
                                    </li>
                                    <li @if($data["sanpham"]["sort_type"] == 1) class="active" @endif data-order="discount_percent,desc">
                                        <a href="{{url('search?text='.$text.'&type=san-pham&choose=thap-cao')}}">GIÁ THẤP ĐẾN CAO</a>
                                    </li>
                                    <li @if($data["sanpham"]["sort_type"] == 2) class="active" @endif data-order="top_seller">
                                        <a href="{{url('search?text='.$text.'&type=san-pham&choose=cao-thap')}}">GIÁ CAO ĐẾN THẤP</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="row is-flex category-products-cc" id="list-content">
                                @foreach( $data["sanpham"]["data"] as $sp )
                                    <div class="col-md-3 col-sm-3 col-xs-6">
                                        <div class="thumb-product item">
                                            <div class="img">
                                                <a href="{{url('san-pham'.'/'.$sp->path)}}" title=""><img src="{{ isset($sp->avatar_image)?url('image/300/300/'.$sp->avatar_image):url('css/images/no-image.png') }}" alt=""></a>
                                            </div>
                                            <div class="wrap-info">
                                                <h3><a href="{{url('san-pham'.'/'.$sp->path)}}">{{ $sp->name }}</a></h3>
                                                @if(isset($sp->price) && $sp->price>0)
                                                    @if(isset($sp->price_sale) && $sp->price_sale>0)
                                                    <div class="price-product">
                                                        <span class="discount">{{isset($sp->price_sale) ? number_format($sp->price_sale,0,",",".").' ₫' : ''}}</span>
                                                        <span class="true-price">{{isset($sp->price) ? number_format($sp->price,0,",",".").' ₫' : ''}}</span>
                                                    </div>
                                                    @else
                                                    <p class="discount">{{isset($sp->price) ? number_format($sp->price,0,",",".").' ₫' : ''}}</p>
                                                    @endif
                                                    @else
                                                    <p class="discount">Giá: Liên hệ</p>
                                                @endif
                                            </div>
                                            <div class="container-star" style="width: 100%; padding-left:10px">
                                                @if(isset($sp->price) && $sp->price>0)
                                                <span class="shopping-cart-index"><a href="javascript:void(0)" onclick="addItemToCart({{$sp->id}},{{$sp->organization_id}})"> <i class="fa fa-shopping-cart"></i></a> </span>
                                                @else
                                                <button type="button" class="send-request" data-id="{{$sp->id}}">Gửi yêu cầu</button>
                                                @endif
                                            </div>
                                            <div class="shop-info-index">
                                                <div class="">
                                                    <img class="image-avatar-index" src="{{isset($sp->org_logo)? url("/image/20/20/".$sp->org_logo): url("css/images/no-image.png")}}">
                                                </div>

                                                <span class="shop-info-index-text">
                                                    <a href='{{url('shop/'.$sp->org_path)}}'>{{$sp->org_name}}</a>
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if( $data["sanpham"]["number_of_row_next"] > 0 )
                                <p class="fs-sevmore">
                                    <a data-keyword="i" id="load_page" data-type="ket-noi" href="javascript:;" data-page="2" onclick="load();">Xem thêm <strong>"{{ $data["sanpham"]["number_of_row_next"] }}"</strong> sản phẩm</a>
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3 cc-secol2">
                        <div class="news-center-topic">
                            <div class="heading-box-">
                                <div class="block-h3">
                                    <h3>Cơ sở SX - KD </h3>
                                </div>
                            </div>
                            <div class="news-r-tk row match-my-cols is-flex1">
                                @foreach( $data["coso"]["data"] as $cs )
                                    <div class="ncc-rightlist col-md-12 col-sm-6">
                                        <a class="pull-left" href="{{url('shop'.'/'.$cs->path)}}">
                                            <img class="img-ncc-right-list" title="" alt="" src="{{ isset($cs->logo_image)?url('image/300/300/'.$cs->logo_image):url('images/no-image.png') }}">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="{{url('shop'.'/'.$cs->path)}}">{{ $cs->name }}</a></h4>
                                            <p class="info-help" style="font-size: 13px;color: #666">
                                                <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                {{$cs->address}}</p>
                                            <p class="info-help" style="font-size: 13px;color: #666"> <i class="fa fa-phone" aria-hidden="true"></i>{{$cs->tel}}</p>
                                            
                                        </div>
                                    </div>
                                @endforeach
                                <div style="width: 100%; text-align: right">
                                    <a href="{{url('search?text='.$text.'&type=co-so&choose=lien-quan')}}">
                                        <u>Xem tất cả {{ $data["coso"]["total_row"] }} cơ sở</u>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="ncc-right-topic">
                            <div class="heading-box-">
                                <div class="block-h3">
                                    <h3>tin tức</h3>
                                </div>
                            </div>
                            <div class="wrap-ncc-r row match-my-cols is-flex1">
                                @foreach( $data["tintuc"]["data"] as $sp )
                                    <div href="{{url('tin-tuc'.'/'.$sp->path)}}" class="ncc-rightlist col-md-12 col-sm-6">
                                        <a class="pull-left">
                                            <img class="img-ncc-right-list" title="" alt="" src="{{ isset($sp->img_path)?url('image/300/300/'.$sp->img_path):url('images/no-image.png') }}">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="{{url('tin-tuc'.'/'.$sp->path)}}">{{ $sp->title }}</a></h4>
                                        </div>
                                    </div>
                                @endforeach
                                <div style="width: 100%; text-align: right">
                                    <a href="{{url('search?text='.$text.'&type=tin-tuc&choose=lien-quan')}}">
                                        <u>Xem tất cả {{ $data["tintuc"]["total_row"] }} tin tức</u>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    var page = 1;
    var type = "{{ $type }}";
    var text = "{{ $text }}";
    var choose = "{{ $choose }}";
    var count = parseInt("{{ $data["ketnoi"]["number_of_row_next"] }}");
    function load() {
        var url = "{{ route('load-page-search') }}";
        var data = {type:type,page:page,text:text,choose:choose};
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'html',
            success: function( data ){
                if(data){
                    $(data).hide().appendTo("#list-content").fadeIn("slow");
                    if((count - 12) < 12) {
                        jQuery("#load_page").hide();
                    }else {
                        page = page + 1;
                        count = count - 12;
                        jQuery("#load_page").html('Xem thêm <strong>"'+ count +'"</strong> sản phẩm');
                    }
                }else {
                    return;
                }
            }
        });
    }
</script>
@endpush