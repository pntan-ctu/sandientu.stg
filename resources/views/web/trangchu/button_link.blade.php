<div class="mod-card-kncc row">
    <div class="col-sm-offset-3 col-md-offset-0 col-md-4 col-sm-3 col-xs-4">
        <div class="mod-card-item ">
            <a class="card-channels-link canmua-card" >
                <div class="card-channels-symbol">
                    <img src="{{url('css/images/icon1_03.png') }}" alt="Cần mua">
                </div>
                <div class="card-channels-name  none-mobile">
                    Cần mua
                </div>
            </a>
        </div>  
    </div>
    <div class="col-sm-3 col-xs-4 col-md-4">
        <div class="mod-card-item ">
            <a class="card-channels-link canban-card"> 
                <div class="card-channels-symbol">
                    <img src="{{url('css/images/forsale_07.png') }}" alt="Cần bán">
                </div>
                <div class="card-channels-name none-mobile">
                    Cần bán
                </div>
            </a>
        </div>  
    </div>

    <div class="col-sm-3 col-xs-4 col-md-4">
        <div class="mod-card-item ">
            <a class="card-channels-link timdoitac-card"> 
                <div class="card-channels-symbol">
                    <img src="{{url('css/images/icon1_05.png') }}"alt="Tìm đối tác">
                </div>
                <div class="card-channels-name  none-mobile">
                    Tìm đối tác
                </div>
            </a>
        </div>  
    </div>
</div>



