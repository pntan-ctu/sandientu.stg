<!-- Carousel
  ================================================== -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.0/jquery.min.js"></script>
<div class="full-home-banners row">
    <div class="full-home-banners__main-banner col-md-8 col-sm-12 col-xs-12">

      <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                @foreach ($slides as $key => $value)
                <li data-target="#myCarousel" data-slide-to="{{$key}}" @if($key ==0) class="active" @endif></li>
                @endforeach
            </ol>
            <div class="carousel-inner">
                @foreach ($slides as $key => $value)
                <div  @if($key ==0) class="item active" @else class="item" @endif>
                       <a href="{{ $value->link }}" target="_blank"><img class="img-responsive" src="{{url("/image/0/255/".$value->image_path)}} " alt="{{$value->title}}"></a>
                </div>
                @endforeach
            </div>
            <a class="left carousel-control" href="#myCarousel" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
    
  </div>
    </div>
    <div class="col-md-4 hidden-sm hidden-xs banner-right">
        <div class="banner-right">
            @if(isset($advs))
                @foreach($advs as $value)
                    <div class="img-right-item">
                        <a target="_blank" href="{{$value->url}}"><img src="{{isset($value->img_path)? url("/image/380/125/".$value->img_path):url("css/images/no-image.png")}}" alt="{{$value->title}}" /></a>
                    </div>
                @endforeach
            @endif
            
<!--            <div class="img-right-item">
                <a href="#">
                    <img src="{{url("css/images/banner-right-1.jpg")}} ">  
                </a>
            </div>-->
        </div>
    </div>
</div>
<!-- /.carousel -->
<style type="text/css">
  .swipe{position:relative;overflow:hidden;visibility:hidden;margin-top:6em;}
.swipe-wrap{position:relative;overflow:hidden;}
.img-wrap{width:100%;float:left;position:relative;}
.img-wrap .img{max-width:30em;height:15em;margin:10px auto;border-radius:0.3125em;background-size:cover;background-repeat:no-repeat;background-position:50% 50%;box-shadow:0 0 10px rgba(0,0,0,0.8),inset 0 0 1px rgba(255,255,255,0.6);}
.img-wrap:nth-child(1) .img{background-image:url("http://www.jiawin.com/demos/swipe-mobile-touch-slider/images/explorations1.jpg");}
.img-wrap:nth-child(2) .img{background-image:url("http://www.jiawin.com/demos/swipe-mobile-touch-slider/images/explorations2.jpg");}
.img-wrap:nth-child(3) .img{background-image:url("http://www.jiawin.com/demos/swipe-mobile-touch-slider/images/explorations3.jpg");}
.img-wrap:nth-child(4) .img{background-image:url("http://www.jiawin.com/demos/swipe-mobile-touch-slider/images/fiftyandtwothirds3.jpg");}

.list{max-width:40em;margin:15px auto 0;text-align:center;}
.list li{width:10px;height:10px;display:inline-block;margin:0 3px;border-radius:50%;box-shadow:inset 0 1px 3px rgba(0,0,0,0.89),0 0 1px 1px rgba(255,255,255,0.03);background-color:rgba(0,0,0,0.24);cursor:pointer;}
.list li.on{box-shadow:inset 0 1px 3px rgba(1,201,201,0.84),0 0 1px 1px rgba(255,255,255,0.03);background-color:rgba(0,223,253,1);background-image:linear-gradient(0deg,rgba(0,0,0,0.3),transparent);}

@media screen and (min-width:40em) {
  .img-wrap .img{max-width:40em;height:22.5em;}
}

</style>
<script type="text/javascript">
   (function(w){
  //var list = document.getElementById("list").getElementsByTagName("li");
  
  w.mySwipe = new Swipe(document.getElementById("slide"),{
    auto:5000,
    continuous:true,
    callback:function(i){
      $(".list>li").eq(i).addClass("on").siblings().removeClass("on");
      /*var l = list.length;
      while(l--){
        list[l].className = "";
      }
      list[i].className = "on";*/
    }
  })
})(this);
</script>