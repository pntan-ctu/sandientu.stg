<div class="container-fluil wrap-ncc">
    <div class="container">

        <div class="box-connections" style="background-color:lightseagreen;">
            <div class="head-buy-index row">
                <div class="col-md-6 col-sm-6 col-xs-7" style="vertical-align: middle;"><h3 class="h3-head-buy-index"><a href="{{url('ket-noi')}}" style="color: white;">Kết nối cung cầu</a></h3></div>
                <div class="col-md-6 col-sm-6 col-xs-5">
                    @include('web.trangchu.button_link')
                </div>
            </div>

            <div class="buy-center-linked-list-columns-index">
                <div class="row is-flex">
                    @if(isset($ads))
                    @foreach($ads as  $value) 

                    <div class="col-md-6 col-sm-6 col-xs-12">
                        <div class="buy-li1">
                            <div class="news-buy" style="border-radius: 10px;">
                                <div class="icon-buy-li">
                                    <a href="{{url('ket-noi'.'/'.$value->path)}}">
                                        <img alt="{{$value->title}}" @if($value->advertisingImages->count()>0) src="{{url("/image/80/80/".$value->advertisingImages->first()->image)}}" @else src="{{url('css/images/no-image.png')}}" @endif>
                                    </a>
                                </div>

                                <div class="wrap-buy-li">
                                    <h4><a class="post-link1" href="{{url('ket-noi'.'/'.$value->path)}}">{{$value->title}}</a></h4>
                                    <div class="info-buy">
                                        <span class="userport">
                                            <span><i class="fa fa-user" aria-hidden="true"></i>
                                                @if(isset($value->advertisingable))
                                                <a href="
                                                    {{
                                                        $value->advertisingable_type == 'App\Models\User' ?
                                                            $value->advertisingable->getUrlActivity() :
                                                            ($value->advertisingable_type == 'App\Models\Organization' ?
                                                                url('shop/'.$value->advertisingable->path) : "#")
                                                    
                                                    }}
                                                    ">
                                                    {{$value->advertisingable->name}}
                                                    @endif
                                                </a>
                                            </span>
                                        </span>
                                        <span style="margin-left: 1px;margin-right: 1px">|</span>
                                        <span class="port-time">		
                                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{ $value->created_at->toShortDateString()}}</span>
                                        </span>
                                        <span style="margin-left: 1px;margin-right: 1px">|</span>
                                        <span class="port-time">		
                                            <span><i class="fa fa fa-bars" aria-hidden="true"></i> {{ $value->advertisingCategory->name}}</span>
                                        </span>
                                    </div>
                                    <div class="post-desc">
                                        {{$value->content}}
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>	
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>  
</div>

<style type="text/css">
    .news-buy:hover{
        background-color:   lavender;
        transition-duration: 0.3s;
    }
</style>