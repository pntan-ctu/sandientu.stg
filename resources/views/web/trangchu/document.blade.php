<?php 
 $crrtime = time();
 $numTBDispay = 5;
 $numTBItem = count($documents);
 $numTBDispay = ($numTBDispay > $numTBItem)? $numTBItem:$numTBDispay;
 ?>
<div class="box-news-col">
    <div class="header-news-index">
        <a href="{{ url('van-ban-qppl')}}"><h3 class="h3-news-index"> Văn bản pháp luật</h3></a>
        <!--<span class="icon"><i class="fa fa-pagelines" aria-hidden="true"></i></span>-->
    </div>
    <div class="clearfix"></div>
    <div class="block-content">
        <div class="row">
            <div style="height:350px; overflow: hidden;" id='ThongBaoContent_{{$crrtime}}'>
            @if(isset($documents))
            @foreach ($documents as $key => $value)
                <div class="item-news-index">
<!--                    <div class="document-left-index">
                        {{$key}}
                    </div>-->
                    <div class="document-right-index">
                        <a href="{{ url('van-ban-qppl/'.$value->path)}}">{{$value->quote}}</a>
                    </div>
                </div>
            @endforeach
            @endif
            </div>
        </div>
    </div>    
</div>  

@push('scripts')
<script>
   
     <?php
    if($numTBDispay < $numTBItem){
        ?>
    $(function() {
    var runThongBao_{{$crrtime}} = function(){
        var ThongBao_{{$crrtime}} = $("#ThongBaoContent_{{$crrtime}}");
        var isHovered = $('#ThongBaoContent_{{$crrtime}}:hover').length>0;
        if(isHovered){
                setTimeout(function() {
                    runThongBao_{{$crrtime}}();
                  }, 5000 );    
                return;
        }
        var count = ThongBao_{{$crrtime}}.children().length;
        var x_next = {{$numTBDispay}};
        if(x_next == count) x_next = 0;
        var divId = ThongBao_{{$crrtime}}.children()[0];
        var divId_next = ThongBao_{{$crrtime}}.children()[x_next];
        $(divId).slideUp(500, function(){
                var temp = divId;
                $(divId).remove();
                ThongBao_{{$crrtime}}.append(temp);
            });
        $(divId_next).show();
            setTimeout(function() {
                runThongBao_{{$crrtime}}();
              }, 5000 );    
        return; 
        console.log(isHovered);
        
    }
    runThongBao_{{$crrtime}}();
    });
   
<?php    
}
?>
</script>
@endpush


