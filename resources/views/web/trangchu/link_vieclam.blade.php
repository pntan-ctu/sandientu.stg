<div class="mod-card-kncc row">
    <div class="col-sm-offset-3 col-md-offset-0 col-md-4 col-sm-3 col-xs-4">
        <div class="mod-card-item ">
            <a class="card-channels-link canmua-card" >
                <div class="card-channels-symbol">
                    <img style="border-radius: 50%;" src="{{url('css/images/vieclam.png') }}" alt="Cần mua">
                </div>
                <div class="card-channels-name  none-mobile">
                   Tin Việc làm
                </div>
            </a>
        </div>  
    </div>
    <div class="col-sm-3 col-xs-4 col-md-4">
        <div class="mod-card-item ">
            <a class="card-channels-link canban-card"> 
                <div class="card-channels-symbol">
                    <img src="{{url('css/images/dulich.png') }}" alt="Cần bán">
                </div>
                <div class="card-channels-name none-mobile">
                    Dịch vụ Du lịch
                </div>
            </a>
        </div>  
    </div>

    <div class="col-sm-3 col-xs-4 col-md-4">
        <div class="mod-card-item ">
            <a class="card-channels-link timdoitac-card"> 
                <div class="card-channels-symbol">
                    <img style="border-radius: 50%;" src="{{url('css/images/dautu.png') }}"alt="Tìm đối tác">
                </div>
                <div class="card-channels-name  none-mobile">
                    Dự Án Đầu Tư
                </div>
            </a>
        </div>  
    </div>
</div>

<style type="text/css">
     .mod-card-item{
        box-shadow: -3px 5px 2px;
     }
    .mod-card-item:hover{
        background-color: red;
        transition-duration: 0.3s;
        box-shadow: 0px 0px 0px;
    }
    .mod-card-item a:hover{
        color: white;
    }
</style>



