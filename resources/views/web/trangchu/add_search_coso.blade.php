@foreach( $data["data"] as $item )
    <div class="col-md-6 col-sm-6 col-xs-12">
        <div class="buy-li1">
            <div class="news-buy">
                <div class="icon-buy-li">
                    <a href="{{url('shop'.'/'.$item->path)}}">
                        <img src={{ isset($item->logo_image)? url('image/300/300/'.$item->logo_image): url('images/no-image.png') }}>
                    </a>
                </div>
                <div class="wrap-buy-li">
                    <h4><a class="post-link1" href="{{url('shop'.'/'.$item->path)}}">{{ $item->name }}</a></h4>
                    <div class="info-buy">
		                                        <span class="userport">
		                                            <span><i class="fa fa-user" aria-hidden="true"></i><a href="{{url('shop'.'/'.$item->path)}}">{{ $item->name }}</a></span>
		                                        </span>
                        <span style="margin-left: 1px;margin-right: 1px">|</span>
                        <span class="port-time">
		                                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> @if($item->created_at) {{ Carbon\Carbon::createFromTimeString($item->created_at)->toShortDateString() }} @else Không xác định @endif</span>
		                                        </span>
                    </div>
                    <div class="post-desc">{{ $item->address }}</div>
                </div>
            </div>
        </div>
    </div>
@endforeach
