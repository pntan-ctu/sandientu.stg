<div class="container-fluil wrap-ncc">
    <div class="container">
        <div class="box-ncc">
            <div class="box-header-ncc">Công ty lớn hợp tác trong các lĩnh vực</div>
            <div class="wrap-th">
                <div class="row">
                    @if(isset($trademarks))
                        @foreach($trademarks as $value)
                        <div class="col-md-2 col-sm-4 col-xs-6 item">
                            <div class="logoncc">
                                <a target="_blank" href="{{$value->url}}"><img src="{{isset($value->img_path)? url("/image/150/103/".$value->img_path):url("css/images/no-image.png")}}" alt="{{$value->title}}" /></a>
                            </div>  
                        </div>
                        @endforeach
                    @endif    
<!--                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/hioke-1520483686.png">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/logo-vinafoods.png">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/img1_19.jpg">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/vitifood-1520483697.gif">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/img1_19.jpg">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/logo-tkh.png">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/img1_19.jpg">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/vitifood-1520483697.gif">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/logo-ca-cao.png">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/img1_19.jpg">
                        </div>  
                    </div>
                    <div class="col-md-2 col-sm-4 col-xs-6 item">
                        <div class="logoncc">
                            <img src="css/images/toilyson.png">
                        </div>  
                    </div>-->
                </div> 
            </div>  
        </div>
    </div>  
</div> 