
<div class="box-product">
    <div class="box-header">
        <span>Sản phẩm khuyến mại</span>
        <span class="icon"><i class="fa fa-pagelines" aria-hidden="true"></i></span>
    </div>
    <div class="clearfix"></div>
    <div class="swiper-container-horizontal">
        <div class="regular slider row">
             @foreach ($productsSale as $key => $value)
            <div class="thumb-product item">
                <div class="img">
                    <a href="{{url('san-pham/'.$value->path)}}" title="">
                        <img src="{{url("/image/300/300/".$value->avatar_image)}}" alt="" >
                    </a>
                </div>
                <div class="wrap-info">
                    <h3><a href="{{url('san-pham/'.$value->path)}}">{{$value->name }}</a></h3>
                    <p class="discount">{{ number_format($value->price_sale,0,",",".") }} <font>VND</font></p>
                    <p class="true-price">{{number_format($value->price,0,",",".") }}<font>VND</font></p>
                    <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})"><div class="addcart1">Thêm vào giỏ</div></a>
                </div>
            </div>
             @endforeach
        </div>
    </div>    
</div>



