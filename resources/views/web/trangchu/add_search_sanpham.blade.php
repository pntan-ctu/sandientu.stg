@foreach( $data["data"] as $sp )
<div class="col-sm-3 col-xs-6">
    <div class="thumb-product item">
        <div class="img">
            <a href="{{url('san-pham'.'/'.$sp->path)}}" title=""><img src="{{ isset($sp->avatar_image)?url('image/80/80/'.$sp->avatar_image):url('css/images/no-image.png')}}" alt="{{$sp->name}}"></a>
        </div>
        <div class="wrap-info">
            <h3><a href="{{url('san-pham'.'/'.$sp->path)}}">{{ $sp->name }}</a></h3>
            <!-- @if(isset($sp->price) && $sp->price>0)
                    <p class="discount">{{isset($sp->price) ? number_format($sp->price,0,",",".").' ₫' : ''}}</p>
                    <a href="#"><div class="addcart1"></div></a>
                @else
                    <p class="discount">Giá: Liên hệ</p>
                    <button type="button" class="send-request" data-id="{{$sp->id}}">Gửi yêu cầu</button>
                @endif -->
            @if(isset($sp->price) && $sp->price>0)
            @if(isset($sp->price_sale) && $sp->price_sale>0)
            <div class="price-product">
                <span class="discount">{{isset($sp->price_sale) ? number_format($sp->price_sale,0,",",".").' ₫' : ''}}</span>
                <span class="true-price">{{isset($sp->price) ? number_format($sp->price,0,",",".").' ₫' : ''}}</span>
            </div>
            @else
            <p class="discount">{{isset($sp->price) ? number_format($sp->price,0,",",".").' ₫' : ''}}</p>
            @endif
            @else
            <p class="discount">Giá: Liên hệ</p>
            @endif
        </div>
        <div class="container-star" style="width: 100%; padding-left:10px">
            @if(isset($sp->price) && $sp->price>0)
            <span class="shopping-cart-index"><a href="javascript:void(0)" onclick="addItemToCart({{$sp->id}},{{$sp->organization_id}})"> <i class="fa fa-shopping-cart"></i></a> </span>
            @else
            <button type="button" class="send-request" data-id="{{$sp->id}}">Gửi yêu cầu</button>
            @endif
        </div>
        <div class="shop-info-index">
            <div class="">
                <img class="image-avatar-index" src="{{isset($sp->org_logo)? url("/image/20/20/".$sp->org_logo): url("css/images/no-image.png")}}">
            </div>

            <span class="shop-info-index-text">
                <a href='{{url('shop/'.$sp->org_path)}}'>{{$sp->org_name}}</a>
            </span>
        </div>
    </div>
</div>
@endforeach 