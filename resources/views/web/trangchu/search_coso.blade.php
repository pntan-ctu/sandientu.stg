@extends('layouts.web')
@section('title')
    Kết quả tìm kiếm
@endsection
@section('content')
    <style>
        .thumb-product.item .img img { width: 100%; height: 100%;}
    </style>
    <div class="contentMain">
        <div class="wrap-page">
            <div class="wrap-breadcrumb">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#" title="Trang chủ">Trang chủ</a></li>
                        <li><a href="/product" title="Sản phẩm">Tìm kiếm</a></li>
                        <li><a href="#" title="Thịt Bò Gác Bếp">{{ $text }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="container">
                <p class="cc-senoti">Tìm thấy <strong>{{ $data["count"] }}</strong> kết quả với từ khóa <strong>"{{ $text }}"</strong></p>
                <div class="menusearch cc-searchtab">
                    <ul class="sort-list nav nav-cc">
                        <li data-order="newest">
                            <a href="{{url('search?text='.$text.'&type=ket-noi&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Kết nối cung cầu</a>
                        </li>
                        <li class="" data-order="top_seller">
                            <a href="{{url('search?text='.$text.'&type=san-pham&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Sản phẩm</a>
                        </li>
                        <li class="active" class="" data-order="discount_percent,desc">
                            <a href="{{url('search?text='.$text.'&type=co-so&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Cơ sở SX - KD</a>
                        </li>
                        <li class="" data-order="discount_percent,desc">
                            <a href="{{url('search?text='.$text.'&type=tin-tuc&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Tin tức</a>
                        </li>
                        <li class="" data-order="discount_percent,desc">
                            <a href="{{url('search?text='.$text.'&type=dia-diem&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Địa điểm</a>
                        </li>
                    </ul>
                </div>
                <div class="row cc-serow">
                    <div class="col-md-9 cc-secol1">
                        <div class="conected-search">
                            <div class="heading-box-">
                                <div class="block-h3">
                                    <a href="{{url('search?text='.$text.'&type=co-so&choose=lien-quan')}}">
                                        <h3>CƠ SỞ SX - KD: </h3>
                                        <h4 name="results-count"> {{ $data["coso"]["total_row"] }} kết quả</h4>
                                    </a>
                                </div>
                            </div>
                            <div class="w-connected-search row is-flex" id="list-content">
                                @foreach( $data["coso"]["data"] as $item )
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="buy-li1">
                                            <div class="news-buy">
                                                <div class="icon-buy-li">
                                                    <a href="{{url('shop'.'/'.$item->path)}}">
                                                        <img src="{{ isset($item->logo_image)?url('image/300/300/'.$item->logo_image):url('images/no-image.png') }}" >
                                                    </a>
                                                </div>
                                                <div class="wrap-buy-li">
                                                    <h4><a class="post-link1" href="{{url('shop'.'/'.$item->path)}}">{{ $item->name }}</a></h4>
                                                    <div class="info-buy">
		                                        <span class="userport">
		                                            <span><i class="fa fa-user" aria-hidden="true"></i><a href="{{url('shop'.'/'.$item->path)}}">{{ $item->name }}</a></span>
		                                        </span>
                                                        <span style="margin-left: 1px;margin-right: 1px">|</span>
                                                        <span class="port-time">
		                                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> @if($item->created_at) {{ Carbon\Carbon::createFromTimeString($item->created_at)->toShortDateString() }} @else Không xác định @endif</span>
		                                        </span>
                                                    </div>
                                                    <div class="post-desc">{{ $item->address }}</div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                            @if( $data["coso"]["number_of_row_next"] > 0 )
                                <p class="fs-sevmore">
                                    <a data-keyword="i" id="load_page" data-type="ket-noi" href="javascript:;" data-page="2" onclick="load();">Xem thêm <strong>"{{ $data["coso"]["number_of_row_next"] }}"</strong> cơ sở</a>
                                </p>
                            @endif
                        </div>
                    </div>
                    <div class="col-md-3 cc-secol2">
                        <div class="ncc-right-topic">
                            <div class="heading-box-">
                                <div class="block-h3">
                                    <h3>sản phẩm</h3>
                                </div>
                            </div>
                            <div class="wrap-ncc-r row match-my-cols is-flex1">
                                @foreach( $data["sanpham"]["data"] as $sp )
                                    <div href="{{url('san-pham'.'/'.$sp->path)}}" class="ncc-rightlist col-md-12 col-sm-6">
                                        <a class="pull-left">
                                            <img class="img-ncc-right-list"  src="{{ isset($sp->avatar_image)? url('image/300/300/'.$sp->avatar_image): url('images/no-image.png') }}">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="{{url('san-pham'.'/'.$sp->path)}}">{{ $sp->name }}</a></h4>
                                            <p class="info-help" style="font-size: 13px;color: #666">
                                                <i class="fa fa-home " aria-hidden="true"></i>
                                                {{$sp->org_name}}
                                            </p>
                                        </div>
                                    </div>
                                @endforeach
                                <div style="width: 100%; text-align: right">
                                    <a href="{{url('search?text='.$text.'&type=san-pham&choose=lien-quan')}}">
                                        <u>Xem tất cả {{ $data["sanpham"]["total_row"] }} sản phẩm</u>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="ncc-right-topic">
                            <div class="heading-box-">
                                <div class="block-h3">
                                    <h3>tin tức</h3>
                                </div>
                            </div>
                            <div class="wrap-ncc-r row match-my-cols is-flex1">
                                @foreach( $data["tintuc"]["data"] as $sp )
                                    <div href="{{url('tin-tuc'.'/'.$sp->path)}}" class="ncc-rightlist col-md-12 col-sm-6">
                                        <a class="pull-left">
                                            <img class="img-ncc-right-list" class="" src="{{ isset($sp->img_path)?url('image/300/300/'.$sp->img_path):url('images/no-image.png') }}">
                                        </a>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="{{url('tin-tuc'.'/'.$sp->path)}}">{{ $sp->title }}</a></h4>
                                        </div>
                                    </div>
                                @endforeach
                                <div style="width: 100%; text-align: right">
                                    <a href="{{url('search?text='.$text.'&type=tin-tuc&choose=lien-quan')}}">
                                        <u>Xem tất cả {{ $data["tintuc"]["total_row"] }} tin tức</u>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    var page = 1;
    var type = "{{ $type }}";
    var text = "{{ $text }}";
    var count = parseInt("{{ $data["ketnoi"]["number_of_row_next"] }}");
    function load() {
        var url = "{{ route('load-page-search') }}";
        var data = {type:type,page:page,text:text};
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'html',
            success: function( data ){
                if(data){
                    $(data).hide().appendTo("#list-content").fadeIn("slow");
                    if((count - 12) < 12) {
                        jQuery("#load_page").hide();
                    }else {
                        page = page + 1;
                        count = count - 12;
                        jQuery("#load_page").html('Xem thêm <strong>"'+ count +'"</strong> sản phẩm');
                    }
                }else {
                    return;
                }
            }
        });
    }
</script>
@endpush