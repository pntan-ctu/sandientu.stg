<div class="box-news-col">
    <div class="header-news-index">
       <a href="{{ url('danh-muc-tin-tuc')}}"><h3 class="h3-news-index">Tin tức nổi bật</h3></a>
        <!--<span class="icon"><i class="fa fa-pagelines" aria-hidden="true"></i></span>-->
    </div>
    <div class="clearfix"></div>
    <div class="block-content">
        <div class="row">
            @if(isset($newsArticle))
            @foreach ($newsArticle as $key => $value)
            <div class="media">
                <a class="pull-left pull-left-img-news" href="{{url('tin-tuc/'.$value->path)}}">
                  <img class="media-object" src="{{url(isset($value->img_path)? "/image/200/200/".$value->img_path : 'css/images/no-image.png')}}" alt="{{$value->title}}">
                </a>
                <div class="media-body">
                  <h4 class="media-heading media-heading-cc"> <a href="{{url('tin-tuc/'.$value->path)}}">{{$value->title}}</a></h4>
                </div>
            </div>
            
            
            
            
<!--                <div class="item-news-index">
                    <div class="news-left-index">
                        <a href="{{url('tin-tuc/'.$value->path)}}">
                            <img  src="{{url(isset($value->img_path)? "/image/200/200/".$value->img_path : 'css/images/no-image.png')}}" alt="">
                        </a>
                    </div>
                    <div class="news-right-index">
                        <a href="{{url('tin-tuc/'.$value->path)}}">{{$value->title}}</a>
                        
                    </div>

                </div>-->
          
            @endforeach
            @endif
        </div>
    </div>    
</div>  




