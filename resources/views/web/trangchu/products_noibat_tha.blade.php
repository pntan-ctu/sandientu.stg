<div class="product-category-wrap">
    <!--    <div class="box-header">
            <span>Sản phẩm tiêu biểu của Thanh Hóa</span>
            <span class="icon"><i class="fa fa-pagelines" aria-hidden="true"></i></span>
        </div>
        <div class="clearfix"></div>-->
    <div class="row">
        <div class="col-md-2 col-xs-12">
            <div class="product-left-content">
                <p class="product-iton"><i class="fa fa-certificate" aria-hidden="true"></i></p>
                <p class="title-black">Sản phẩm tiêu biểu của <br> Thanh Hóa</p>
                <p class="view-more"><a class="view-more-link" href="#">Xem thêm</a></p>
            </div>
        </div>
        <div class="col-md-10 col-xs-12">
            <div class="box-product">

                <div class="clearfix"></div>
                <div class="swiper-container-horizontal">
                    <div class="products-noibat regular slider row ">
                        @foreach ($productsSale as $key => $value)
                        <div class="thumb-product item">
                            <div class="img">
                                <a href="{{url('san-pham/'.$value->path)}}" title="">
                                    <img src="{{url("/image/300/300/".$value->avatar_image)}}" alt="" >
                                </a>
                            </div>
                            <div class="wrap-info">
                                <h3><a href="{{url('san-pham/'.$value->path)}}">{{$value->name }}</a></h3>
                                <p class="discount">{{ number_format($value->price_sale,0,",",".") }} <font>VND</font></p>
                                <p class="true-price">{{number_format($value->price,0,",",".") }} <font>VND</font></p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>    
            </div>
        </div>

    </div>
</div>




