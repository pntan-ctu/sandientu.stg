@extends('layouts.web')
@section('title')
    Kết quả tìm kiếm
@endsection
@section('content')
    <style>
        .thumb-product.item .img img { width: 100%; height: 100%;}
    </style>
    <div class="contentMain">
        <div class="wrap-page">
            <div class="wrap-breadcrumb">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="#" title="Trang chủ">Trang chủ</a></li>
                        <li><a href="/product" title="Sản phẩm">Tìm kiếm</a></li>
                        <li><a href="#" title="Thịt Bò Gác Bếp">{{ $text }}</a></li>
                    </ul>
                </div>
            </div>
            <div class="container">
                <p class="cc-senoti">Tìm thấy <strong>{{ $data["count"] }}</strong> kết quả với từ khóa <strong>"{{ $text }}"</strong></p>
                <div class="menusearch cc-searchtab">
                    <ul class="sort-list nav nav-cc">
                        <li data-order="newest">
                            <a href="{{url('search?text='.$text.'&type=ket-noi&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Kết nối cung cầu</a>
                        </li>
                        <li class="" data-order="top_seller">
                            <a href="{{url('search?text='.$text.'&type=san-pham&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Sản phẩm</a>
                        </li>
                        <li class="" data-order="discount_percent,desc">
                            <a href="{{url('search?text='.$text.'&type=co-so&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Cơ sở SX - KD</a>
                        </li>
                        <li class="" class="" data-order="discount_percent,desc">
                            <a href="{{url('search?text='.$text.'&type=tin-tuc&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Tin tức</a>
                        </li>
                        <li class="active" data-order="discount_percent,desc">
                            <a onclick="search()" href="{{url('search?text='.$text.'&type=dia-diem&choose=lien-quan&lat='.$lat.'&long='.$long.'')}}">Địa điểm</a>
                        </li>
                    </ul>
                </div>
                <div class="row cc-serow">
                    <div class="col-md-9 cc-secol1">
                        <div class="conected-search">
                            <div style="clear:both; height: 10px;"></div>    
                            <div id="map" style="width:100%; height:500px"></div>    
                            @push('scripts')
                        <script>
                            function initMap1() {
                                var thanhhoa = new google.maps.LatLng(19.807812, 105.776747);

                                var map = new google.maps.Map(document.getElementById('map'), {
                                      scaleControl: true,
                                      center: thanhhoa,
                                      zoom: 10
                                });
                                
                                var organization100 = {!! $organization100 !!};
                                var url1 = "{{url('shop/')}}" + "/"; //"{{url('/')}}"
                                var url2 = "{{url('dia-diem-kinh-doanh/')}}";
                                $.each(organization100.data, function (index, value) {
                                    var thanhhoa1 = new google.maps.LatLng(value.map_lat, value.map_long);
                                    //var latLong = value.map_lat + "," + value.map_long;
                                    var title = '';//
                                    if(value.organization_type_id===100){
                                       title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
                                   }else if(value.organization_type_id===101){
                                       title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
                                   }else if(value.organization_type_id===0){
                                       title = "<b>" + value.name + "</b>";
                                   }else if(value.organization_type_id===1){
                                       title = "<b><a target='_blank' href='" + url2 + "'>" + value.name + "</a></b>";
                                   }
                                    var coordInfoWindow = new google.maps.InfoWindow();
                                    coordInfoWindow.setContent(createInfoWindowContent(value,title, map.getZoom()));

                                   var img_ico = '';//"{{url('/')}}" + "/css/images/cssx.png";
                                   if(value.organization_type_id===100){
                                       img_ico = "{{url('/')}}" + "/css/images/cssx1.png";
                                   }else if(value.organization_type_id===101){
                                       img_ico = "{{url('/')}}" + "/css/images/cskd.png";
                                   }else if(value.organization_type_id===0){
                                       img_ico = "{{url('/')}}" + "/css/images/branch.png";
                                   }else if(value.organization_type_id===1){
                                       img_ico = "{{url('/')}}" + "/css/images/market.png";
                                   }
                                   var marker = new google.maps.Marker({map: map, position: thanhhoa1,icon: img_ico});
                                      marker.addListener('click', function() {
                                        coordInfoWindow.open(map, marker);
                                      });

                                    map.addListener('zoom_changed', function() {
                                      coordInfoWindow.setContent(createInfoWindowContent(new thanhhoa1, map.getZoom()));
                                      coordInfoWindow.open(map);
                                    });
                                }); 
                            }

                            var TILE_SIZE = 256;

                            function createInfoWindowContent(value,latLong, zoom) {
                              var scale = 1 << zoom;

                              return [
                                  latLong,
                                //value.name,
                                //'Chi tiếṭ: ' + latLong,
                                ' ' + "<b>" +value.organization_name +"</b>",
                                'Địa chỉ: ' + value.address,
                                'Tel: ' + value.tel,
                                'Email: ' + value.email,
                                'Website: ' + value.website,
                                'Khoảng cách: ' + value.distance.toFixed(2)+' Km'
                              ].join('<br>');
                            }

                            // The mapping between latitude, longitude and pixels is defined by the web
                            // mercator projection.
                            function project(latLng) {
                              var siny = Math.sin(latLng.map_lat * Math.PI / 180);

                              // Truncating to 0.9999 effectively limits latitude to 89.189. This is
                              // about a third of a tile past the edge of the world tile.
                              siny = Math.min(Math.max(siny, -0.9999), 0.9999);

                              return new google.maps.Point(
                                  TILE_SIZE * (0.5 + latLng.map_long / 360),
                                  TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
                            }
                          </script>
                          <!--google - api - key-->

                          @endpush

                        </div>
                    </div>
                    <div class="col-md-3 cc-secol2">
                        <div class="ncc-right-topic">
                            <div class="heading-box-">
                                <div class="block-h3">
                                    <h3>SẢN PHẨM</h3>
                                </div>
                            </div>
                            <div class="wrap-ncc-r row match-my-cols is-flex1">
                                @if(count($arOrg)>0)
                                    @foreach( $arOrg as $sp )
                                        <div href="{{url('san-pham'.'/'.$sp->path)}}" class="ncc-rightlist col-md-12 col-sm-6">
                                            <a class="pull-left">
                                                <img class="img-ncc-right-list" title="" alt="" src="{{ isset($sp->avatar_image)?url('image/300/300/'.$sp->avatar_image):url('images/no-image.png') }}">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="{{url('san-pham'.'/'.$sp->path)}}">{{ $sp->name }}</a></h4>
                                                <p class="info-help" style="font-size: 13px;color: #666">
                                                    <i class="fa fa-home " aria-hidden="true"></i>
                                                    {{$sp->organization_name}} </p>
                                                <p class="info-help" style="font-size: 13px;color: #666">
                                                    <i class="fa fa-map-marker" aria-hidden="true"></i>
                                                    <?php echo(round($sp->distance,2)) ?> Km</p>
                                            </div>
                                        </div>
                                    @endforeach
                                @else
                                    @foreach( $data["sanpham"]["data"] as $sp )
                                        <div href="{{url('san-pham'.'/'.$sp->path)}}" class="ncc-rightlist col-md-12 col-sm-6">
                                            <a class="pull-left">
                                                <img class="img-ncc-right-list" title="" alt="" src="{{ isset($sp->avatar_image)?url('image/300/300/'.$sp->avatar_image):url('images/no-image.png') }}">
                                            </a>
                                            <div class="media-body">
                                                <h4 class="media-heading"><a href="{{url('san-pham'.'/'.$sp->path)}}">{{ $sp->name }}</a></h4>
                                                <p class="info-help" style="font-size: 13px;color: #666">
                                                    <i class="fa fa-home " aria-hidden="true"></i>
                                                    {{$sp->org_name}}
                                                </p>
                                            </div>
                                        </div>
                                    @endforeach
                                @endif
                                <div style="width: 100%; text-align: right">
                                    <a href="{{url('search?text='.$text.'&type=san-pham&choose=lien-quan')}}">
                                        <u>Xem tất cả {{ $data["sanpham"]["total_row"] }} sản phẩm</u>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script>
    function search() {
        if (navigator.geolocation) {
                     navigator.geolocation.getCurrentPosition(showPosition);
                    }
        function showPosition(position) {
            var text = "{{ $text }}";
        //text = text.replace("/", "");
            var url = "{{ url('search') }}" + "?text=" + text + "&type=dia-diem&choose=lien-quan&lat="+position.coords.latitude.toFixed(6)+"&long="+position.coords.longitude.toFixed(6);
            window.location.href = url;
        }
    }
    var page = 1;
    var type = "{{ $type }}";
    var text = "{{ $text }}";
    var count = parseInt("{{ $data["ketnoi"]["number_of_row_next"] }}");
    function load() {
        var url = "{{ route('load-page-search') }}";
        var data = {type:type,page:page,text:text};
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'html',
            success: function( data ){
                if(data){
                    $(data).hide().appendTo("#list-content").fadeIn("slow");
                    if((count - 12) < 12) {
                        jQuery("#load_page").hide();
                    }else {
                        page = page + 1;
                        count = count - 12;
                        jQuery("#load_page").html('Xem thêm <strong>"'+ count +'"</strong> sản phẩm');
                    }
                }else {
                    return;
                }
            }
        });
    }
</script>
@endpush