<div class="container-fluil wrap-ncc">
    <div class="container">
        <div class="box-ncc">
            <div class="box-header"><a href="{{url('shop')}}"> Tổ chức kinh doanh lĩnh vực bất dộng sản</a></div>
            <div class="wrap-cssx">
                <div class="business-list row">
                    @if(isset($organization))
                    @foreach($organization as  $value) 
                    <div class="col-md-4">
                        <div class="business-item">
                            <div class="img-business">
                                <a class="detail" href="{{url('shop/'.$value->path)}}">
                                    <img alt="{{$value->name}}" class="business-logo" src="{{isset($value->logo_image)? url("/image/350/350/".$value->logo_image):url("css/images/logo.png")}}">
                                   
                                </a>
                            </div>
                                <div class="business-info"><span class="name">
                                    <a  href="{{url('shop/'.$value->path)}}">
                                        {{$value->name}}
                                    </a></span>
                                    <span class="text">Địa chỉ: {{$value->address}}</span>
                                    <span class="text">&nbsp;</span>
                                </div>
                            </div>
                        </div>

                        @endforeach
                        @endif
                    </div>
                </div>  
            </div>
        </div>  
    </div> 