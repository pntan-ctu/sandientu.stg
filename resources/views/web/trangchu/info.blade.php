<div class="box-news">
    <div class="box-header">
        <span>Thông tin hữu ích</span>
        <!--<span class="icon"><i class="fa fa-pagelines" aria-hidden="true"></i></span>-->
    </div>
    <div class="clearfix"></div>
    <div class="block-content">
        <div class="row">
            <div class="col-md-4">
               @include('web.trangchu.news')
            </div>
            <div class="col-md-4">
               @include('web.trangchu.document')
            </div>
            <div class="col-md-4">
               @include('web.trangchu.videos')
            </div>
        </div>
    </div>    
</div>  




