@foreach( $data["data"] as $item )
    <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="buy-li1">
            <div class="news-buy">
                <div class="icon-buy-li">
                    <a href="{{url('tin-tuc'.'/'.$item->path)}}">
                        <img src="{{ isset($item->img_path)? url('image/80/80/'.$item->img_path): url('css/images/no-image.png') }}" >
                    </a>
                </div>
                <div class="wrap-buy-li">
                    <h4><a class="post-link1" href="{{url('tin-tuc'.'/'.$item->path)}}">{{ $item->title }}</a></h4>
                    <div class="info-buy">
		                                        <span class="userport">
		                                            <span><i class="fa fa-user" aria-hidden="true"></i><a href="#">{{ $item->name }}</a></span>
		                                        </span>
                        <span style="margin-left: 1px;margin-right: 1px">|</span>
                        <span class="port-time">
		                                            <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{ Carbon\Carbon::createFromTimeString($item->created_at)->toShortDateString() }}</span>
		                                        </span>
                    </div>
                    <div class="post-desc">{{ $item->summary }}</div>
                </div>
            </div>
        </div>
    </div>
@endforeach
