<div class="box-product2">
    <div class="box-header"><a href="{{url('danh-muc-san-pham')}}"> LĨNH VỰC BẤT ĐỘNG SẢN </a>
        <!--<span class="icon"><i class="fa fa-pagelines" aria-hidden="true"></i></span>-->
       <!--  <ul class="link-products-index">
            <li><a href="{{url('danh-muc-san-pham/khuyen-mai')}}"> Sản phẩm khuyến mại </a></li>
            <li><a href="{{url('danh-muc-san-pham/moi-ban')}}"> Sản phẩm mới </a></li>
            <li><a href="{{url('danh-muc-san-pham/mua-nhieu')}}"> Sản phẩm được mua nhiều </a></li>
            <li><a href="{{url('danh-muc-san-pham/quan-tam')}}"> Sản phẩm được xem nhiều </a></li>
        </ul> -->
    </div>
    <div class="clearfix"></div>
    <div class="wrap-product">
        <div class="row is-flex">
            @if(isset($productBDS))
            @foreach ($productBDS as $key => $value)
            <div class="col-md-1-5 col-sm-4 col-xs-6 thumb-product-list-item">
                <div class="thumb-product item">
                    <div class="img-item">
                        <div class="img">
                            <a href="{{url('san-pham/'.$value->path)}}" title="">
                                <img src="{{isset($value->avatar_image)? url("/image/300/300/".$value->avatar_image):url("css/images/no-image.png")}}" alt="{{$value->name }}">
                            </a>
                        </div>
                        <div class="cer-item">
                                @if($value->certificates->count() > 0)
                                @foreach($value->certificates as $cer)
                                <image class="img-certifications-index" src="{{url('image/50/50/'. $cer->certificateCategory->icon) }}" title="{{$cer->certificateCategory->name }}"  />
                                @endforeach
                                @endif
                        </div>
                    </div>
                    <div class="wrap-info">
                        <div style="">
                            <h3><a href="{{url('san-pham/'.$value->path)}}"><h6 style="text-transform: uppercase;">{{$value->name }}</h6></a></h3>
                        </div>
                        @if(isset($value->price) && $value->price>0)
                        @if(isset($value->price_sale) && $value->price_sale>0)
                        <div class="price-product">
                            <span class="discount" style="font-size: 15px;">{{isset($value->price_sale) ? number_format($value->price_sale,0,",",".").' ₫' : ''}}</span>
                            <span class="true-price">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</span>
                        </div>
                        <!-- <p class="discount">{{ number_format($value->price_sale,0,",",".") }} <font>VND</font></p> -->
                        <!-- <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})">
                            <div class="addcart1"></div>
                        </a> -->
                        @else
                        <p class="discount" style="font-size: 15px;">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                        <!-- <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})">
                            <div class="addcart1"></div>
                        </a> -->
                        @endif
                        @else
                        <p class="discount">Giá: Liên hệ</p>

                        @endif
                    </div>
                    <div class="container-star" style="width: 100%; padding-left:10px">
                        <input type="hidden" data-readonly value="{{($value->ratings->count()>0)? $value->ratings->sum('rate')/$value->ratings->count():'0'}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-1x" data-empty="fa fa-star-o fa-1x" />
                        @if(isset($value->price) && $value->price>0)
                        <span class="shopping-cart-index"><a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})"> <i class="fa fa-shopping-cart"></i></a> </span>
                        @else
                        <button type="button" class="send-request" data-id="{{$value->id}}">Gửi yêu cầu</button>
                        @endif
                    </div>
                    <div class="shop-info-index">
                        <div class="shop-info-index-avatar">
                            <img class="image-avatar-index" src="{{isset($value->Organization->logo_image)? url("/image/20/20/".$value->Organization->logo_image): url("css/images/no-image.png")}}">
                        </div>

                        <span class="shop-info-index-text">
                            <a href='{{url('shop/'.$value->Organization->path)}}' title="{{$value->Organization->name}}" >{{$value->Organization->name}}</a>
                        </span>
                    </div>

                </div>
            </div>
            @endforeach
            @endif
        </div>
    </div>
</div>