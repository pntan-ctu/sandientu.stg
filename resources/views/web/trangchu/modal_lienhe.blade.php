<div class="modal fade" id="modal-lienhe" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> Gửi Yêu Cầu </h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive">
                    <div class="box-body">
                        <div class="fs-dtrtcmtbox">
<!--                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" id="ho-ten" name="ho-ten" placeholder="Họ tên..." required autofocus>
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input id="email" type="email" class="form-control" id="email-user" name="email-user" placeholder="Email..."  required >
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" id="dia-chi" name="dia-chi" placeholder="Địa chỉ..." required autofocus>
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-12">
                                    <input id="name" type="text" class="form-control" id="phone-number" name="phone-number" placeholder="Số điện thoại..." required autofocus>
                                </div>
                            </div>-->
                            <textarea id="txtContent" name="txtContent" rows="3" class="f-cmttarea fsformsc" placeholder="Nội dung yêu cầu..."></textarea>
                            <div class="fs-dtrtbots clearfix"> 
                                <p>Bạn vui lòng nhập nội dung yêu cầu</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Hủy</button>
                <button id="btn-send-request-product" type="button" class="btn btn-primary"><i class="fa fa-check"></i>Gửi</button>
            </div>
        </div>
        <input type="hidden" id="productId" />
    </div>
</div>

