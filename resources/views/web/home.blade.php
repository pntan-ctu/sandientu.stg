<!-- <?php
   //echo $_SERVER['HTTP_HOST'];
?> -->
@extends('layouts.web') 
@section('title')
Trang chủ
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Sóc Trăng')
@section('og:title', 'Trang chủ')
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')
<h1 style="display:none;"> Kết nối cung cầu nông sản, thực phẩm an toàn </h1>
<h2 style="display:none;"> Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Sóc Trăng </h2>
<div class="contentMain">
    <div class="container">
        @include('web.trangchu.slide')
    </div>
    <br>
    <div class="container">
        <div style="margin-top:15px;">
        @include('web.trangchu.products_nongsan')
        
        </div>
        <div style="margin-top:15px;">
        @include('web.trangchu.products_batdongsan')
        
        </div>

         <div style="margin-top:15px;">
        @include('web.trangchu.products_vieclam')
        
        </div>
        <div style="margin-top:15px;">
        @include('web.trangchu.products_dulich')
        </div>
        <div style="margin-top:15px;">
        @include('web.trangchu.products_dautu')
        </div>
    </div>
    @include('web.trangchu.ketnoi_cungcau')
  
    <div class="container">
        @include('web.trangchu.info')        
    </div>
    @include('web.trangchu.thuonghieu')
</div>
@endsection
@push('scripts')
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-129219308-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-129219308-1');
</script>
@endpush