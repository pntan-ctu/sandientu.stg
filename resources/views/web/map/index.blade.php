@extends('layouts.web')
@section('title')
Bản đồ phân bố cơ sở
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')
@push('styles')
    <link href="{{ asset('css/map.css') }}" rel="stylesheet">
    <link href="{{ asset('css/contact.css') }}" rel="stylesheet">
@endpush    
<div class="wrap-page">
    <div class="wrap-breadcrumb">
            <div class="container">
                    <ul class="breadcrumb">
                           <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                            <li><a href="{{url('/map')}}" title="Bản đồ phân bố cơ sở">Bản đồ phân bố cơ sở </a></li>
                    </ul>
            </div>
    </div>
    <div class="container">
        <div class="box-news" style="margin-top: 0px;">
            <div class="block-content">
                <div class="row is-flex">
                    <div style = "position: relative; width:100%">
                        <div id="form-inline">
                            <div style="float: right; text-align: right; width: 50%">    
                                <form class="form-inline searchlh" action="{{url('map')}}" method="get">
                                    {!! Former::select('orgtype', '')->options(['-1' => '[ Chọn loại ]', '0' =>'Cơ sở kinh doanh', '1' =>'Cơ sở sản xuất', '2'=>'Chi nhánh', '3'=>'Địa điểm kinh doanh']) !!}
                                    <input style="height: 34px; width: 45%" placeholder="Thông tin tìm kiếm" id="keyword" type="text" name="keyword"/>
                                    <button class="iconSearchHeader" style="height: 34px; width: 8%; padding: 0px;"><i class="fa fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                        <div style="clear:both; height: 10px;"></div>    
                        <div id="map" style="width:100%; height:500px"></div>    
                        @push('scripts')
                        <script>
                            function initMap() {
                                var thanhhoa = new google.maps.LatLng(10.378745, 106.343033);

                                var map = new google.maps.Map(document.getElementById('map'), {
                                      scaleControl: true,
                                      center: thanhhoa,
                                      zoom: 10
                                });
                                var organization100 = {!!$organization100!!};
                                var url1 = "{{url('shop/')}}" + "/"; //"{{url('/')}}"
                                var url2 = "{{url('dia-diem-kinh-doanh/')}}" + "/";
                                $.each(organization100, function (index, value) {
                                    var thanhhoa1 = new google.maps.LatLng(value.map_lat, value.map_long);
                                    //var latLong = value.map_lat + "," + value.map_long;
                                    var title = '';//
                                    if(value.organization_type_id===100){
                                       title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
                                   }else if(value.organization_type_id===101){
                                       title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
                                   }else if(value.organization_type_id===0){
                                       title = "<b>" + value.name + "</b>";
                                   }else if(value.organization_type_id===1){
                                       title = "<b><a target='_blank' href='" + url2 + value.path +"'>" + value.name + "</a></b>";
                                   }
                                    var coordInfoWindow = new google.maps.InfoWindow();
                                    coordInfoWindow.setContent(createInfoWindowContent(value,title, map.getZoom()));

                                   var img_ico = '';//"{{url('/')}}" + "/css/images/cssx.png";
                                   if(value.organization_type_id===100){
                                       img_ico = "{{url('/')}}" + "/css/images/cssx1.png";
                                   }else if(value.organization_type_id===101){
                                       img_ico = "{{url('/')}}" + "/css/images/cskd.png";
                                   }else if(value.organization_type_id===0){
                                       img_ico = "{{url('/')}}" + "/css/images/branch.png";
                                   }else if(value.organization_type_id===1){
                                       img_ico = "{{url('/')}}" + "/css/images/market.png";
                                   }
                                   var marker = new google.maps.Marker({map: map, position: thanhhoa1,icon: img_ico});
                                      marker.addListener('click', function() {
                                        coordInfoWindow.open(map, marker);
                                      });

                                    map.addListener('zoom_changed', function() {
                                      coordInfoWindow.setContent(createInfoWindowContent(new thanhhoa1, map.getZoom()));
                                      coordInfoWindow.open(map);
                                    });
                                }); 
                            }
                            var TILE_SIZE = 256;
                            function createInfoWindowContent(value,latLong, zoom) {
                              var scale = 1 << zoom;
                              return [
                                  latLong,
                                //value.name,
                                //'Chi tiếṭ: ' + latLong,
                                'Địa chỉ: ' + value.address,
                                'Tel: ' + value.tel,
                                'Email: ' + value.email,
                                'Website: ' + value.website
                              ].join('<br>');
                            }
                            // The mapping between latitude, longitude and pixels is defined by the web
                            // mercator projection.
                            function project(latLng) {
                              var siny = Math.sin(latLng.map_lat * Math.PI / 180);

                              // Truncating to 0.9999 effectively limits latitude to 89.189. This is
                              // about a third of a tile past the edge of the world tile.
                              siny = Math.min(Math.max(siny, -0.9999), 0.9999);

                              return new google.maps.Point(
                                  TILE_SIZE * (0.5 + latLng.map_long / 360),
                                  TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
                            }
                          </script>
                          @endpush
                    </div>
                </div>	
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
    <script>
     $(document).ready(function () {
         var keyword = "{{$keyword}}";
        // var keyword = urlParam('keyword') ? urlParam('keyword') : '';
        var decodedUri = decodeURIComponent(keyword);
        $('#keyword').val(decodedUri);
     });
    </script>
        @endpush
