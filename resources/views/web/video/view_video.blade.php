@extends('layouts.web')
@section('title')
Thư viện Video
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("css/images/img1_07.jpg"))
@section('og:title', isset($video)?$video->title:'' )
@section('content')
<div class="block-videos">
    <div class="wrap-page">
        <div class="wrap-breadcrumb">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                        <li><a href="{{url('/video')}}">Thư viện Video</a></li>
                    </ul>
                </div>
            </div>
        <div class="container">
            <div class="col-md-8">
                <div class="block-videos-content">
                    @if(isset($video))
                  
                    <h2 class="title-detail">{{$video->title}}</h2>
                    <p class="time-news">
                        <span><i class="fa fa-clock-o"></i> {{$video->created_at->toShortDateString()}}</span>
                        <span>|</span>
                    </p>
                    {!! playVideo($video->youtube_url, 600 , 400) !!}
                    @endif
                   
                </div>
            </div>
            <div class="col-md-4">
                <div class="block-videos-same">
                    <h2>Video liên quan</h2>
                        @if(isset($videoSames))
                        @foreach($videoSames as $key => $value)
                        <div class="item-video-same">
                            <div class="img-news-hot">
                                <a href="{{url('video/'.$value->path)}}"> {!! getSameVideo($value->youtube_url) !!}</a>
                            </div>
                            <div class="title-video-right">
                                <h3> <a href="{{url('video/'.$value->path)}}"> {{$value->title}} </a></h3> 
                                <p class="time-hot">
                                    <span><i class="fa fa-clock-o"></i> {{$value->created_at->toShortDateString()}}</span>
                                </p> 
                            </div> 
                        </div>
                        @endforeach

                        @endif
                </div>
            </div>
        </div>	
    </div>
</div>
@endsection
