
@extends('layouts.web')
@section('title')
Thư viện Video
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("css/images/img1_07.jpg"))
@section('og:title', 'Thư viện Video' )
@section('content')
<div class="block-videos">
    <div class="wrap-page">
         <div class="wrap-breadcrumb">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                        <li><a href="{{url('/video')}}">Thư viện Video</a></li>
                    </ul>
                </div>
            </div>
        <div class="container">
            <div class="block-videos-title">
                <h1>Danh sách Video</h1>
            </div>
            
            <div class="block-videos-content row is-flex">
            @if(isset($videos))
                @foreach($videos as $key => $value)
                <div class="item-video-list col-md-3 col-sm-4 col-xs-6">
                    <div class="img-news-hot">
                        <a href="{{url('video/'.$value->path)}}"> {!! getSameVideo($value->youtube_url) !!}</a>
                    </div>
                    <div class="title-video-right">
                        <h3> <a href="{{url('video/'.$value->path)}}"> {{$value->title}} </a></h3> 
                        <p class="time-hot">
                            <span><i class="fa fa-clock-o"></i> {{$value->created_at->toShortDateString()}}</span>
                        </p> 
                    </div> 
                </div>
                @endforeach
            @endif
            </div>
        </div>	
    </div>
</div>
@endsection
