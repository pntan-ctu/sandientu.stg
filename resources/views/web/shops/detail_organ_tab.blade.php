<style>
   
</style>
<div class="tab-pane" id="tab{{$contentOrgani->count() }}">
    <div class="content pdp-mod-review">
        <div class="organization-info">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    @php
                        $status_txt = "";
                        $status = $Organization->status ?? 0;
                        switch ($status) {
                            case 0:
                                $status_txt = "Chưa duyệt";
                                break;
                            case 1:
                                $status_txt = "Đang hoạt động";
                                break;
                            case 2:
                                $status_txt = "Ngừng hoạt động";
                                break;
                            case 3:
                                $status_txt = "Khóa do vi phạm";
                                break;
                        }
                        $Organization->status_txt = $status_txt;

                        if($Organization->organization_type_id == OrgType::CSSX)
                        {
                            $labelArea = 'Vùng sản xuất';
                            $Organization->area_txt = isset($Organization->area) ? $Organization->area->name : "";
                            $Organization->area_url = isset($Organization->area) ? url("/vung-san-xuat/".$Organization->area->path) : "";
                        }

                        if($Organization->organization_type_id == OrgType::CSKD)
                        {
                            $labelArea = 'Địa điểm kinh doanh';
                            $Organization->area_txt = isset($Organization->commercialCenter) ? $Organization->commercialCenter->name : "";
                        }

                        $Organization->organization_level_id_txt = isset($Organization->organizationLevel) ? $Organization->organizationLevel->name : "";
                        $Organization->founding_date_txt = isset($Organization->founding_date) ? ($Organization->founding_date)->format('d/m/Y') : "";
                        $soGiayPhep = isset($Organization->founding_number) ? "Số ".$Organization->founding_number : "";
                        $soGiayPhep .= "Ngày cấp ".$Organization->founding_date_txt;
                        if(isset($Organization->founding_by_gov)) $soGiayPhep .= ", đơn vị cấp ".$Organization->founding_by_gov;
                        $Organization->founding_number_txt = $soGiayPhep;
                        $Organization->manage_org_txt = isset($Organization->manageOrganization) ? $Organization->manageOrganization->name : '';

                        $aLabel = ['Tên cơ sở', 'Địa chỉ', 'Điện thoại', 'Email', 'Website', 'Người đại diện', 'Quy mô',
                            'Trạng thái', $labelArea, 'Giấy phép đăng ký', 'Quản lý về VSATTP bởi'];
                        $aOrgInfo = ['name', 'address', 'tel', 'email', 'website', 'director', 'organization_level_id_txt',
                            'status_txt', 'area_txt', 'founding_number_txt', 'manage_org_txt'];
                        $count = count($aOrgInfo);
                        $aCert = $Organization->certificates ?? [];
                        $aBranch = $Organization->branches ?? [];
                        $aPartner = $Organization->partners ?? [];
                        $aProvider = [];
                        $aDeliverer = [];
                        foreach($aPartner as $par)
                        {
                            $type = $par->type ?? "";
                            if($type == "cung-cap") $aProvider[] = $par;
                            if($type == "phan-phoi") $aDeliverer[] = $par;
                        }
                    @endphp
                    <h4 class="col-md-12 col-xs-12">Thông tin {{$Organization->organizationType->name}}</h4>
                    <div style="clear:both; border: none"></div>
                    @for($i = 0; $i < $count; $i++)
                        <div class="row">
                            <div class="col-md-5 col-xs-5">{{$aLabel[$i]}}</div>
                            <div class="col-md-7 col-xs-7">
                                @if($aOrgInfo[$i] == 'area_txt')
                                 <a target="_blank" style="color:#5aa32a;" href="{{ $Organization->area_url }}">   {{ $Organization->{$aOrgInfo[$i]} }} </a>
                                @else
                                {{$Organization->{$aOrgInfo[$i]} ?? ""}}
                                @endif
                            </div>
                        </div>
                    @endfor

                    @if(count($aCert) > 0)
                        <h4 class="col-md-12 col-xs-12">Chứng nhận, xác nhận của cơ sở</h4>
                        @foreach ($aCert as $cer)
                            <div class="row">
                                <div class="col-md-5 col-xs-5"><b>{{ $cer->title ?? "" }}</b><br>{{ $cer->description ?? "" }}</div>
                                <div class="col-md-7 col-xs-7 cert-image-product">
                                   @if(isset($cer->image)) 
                                   <a href="{{url("/image/640/600/".$cer->image)}}" class="fancybox">
                                   <img src="{{url("/image/150/150/".$cer->image)}}">
                                   @endif
                                </a>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
                <div class="col-md-6 col-xs-12">
                    @if(count($aBranch) > 0)
                        <h4 class="col-md-12 col-xs-12">Các chi nhánh</h4>
                        @foreach ($aBranch as $i => $br)
                            <div class="row">
                                <div class="col-md-5 col-xs-5">{{$i + 1}}. {{$br->name ?? ""}}</div>
                                <div class="col-md-7 col-xs-7">Địa chỉ: {{$br->address ?? ""}}<br>Điện thoại: {{$br->tel ?? ""}}</div>
                            </div>
                        @endforeach
                    @endif

                    @if(count($aProvider) > 0)
                        <h4 class="col-md-12 col-xs-12">Các cơ sở cung cấp sản phẩm, nguyên vật liệu</h4>
                        @foreach ($aProvider as $i => $pr)
                            <div class="row">
                                <div class="col-md-5 col-xs-5">
                                    @if(isset($pr->organization->id))
                                    <a href="{{url('shop/'.$pr->organization->path)}}">{{$i + 1}}. {{$pr->name ?? ""}}</a>
                                    @else
                                    <span>{{$i + 1}}. {{$pr->name ?? ""}}</span>
                                    @endif
                                </div>
                                <div class="col-md-7 col-xs-7">Địa chỉ: {{$pr->address ?? ""}}<br>Điện thoại: {{$pr->tel ?? ""}}</div>
                                
                            </div>
                        @endforeach
                    @endif

                    @if(count($aDeliverer) > 0)
                        <h4 class="col-md-12 col-xs-12">Các cơ sở phân phối sản phẩm</h4>

                        @foreach ($aDeliverer as $i => $dr)
                            <div class="row">
                                <div class="col-md-5 col-xs-5">
                                    @if(isset($dr->organization->id))
                                        <a href="{{url('shop/'.$dr->organization->path)}}">{{$i + 1}}. {{$dr->name ?? ""}}</a>
                                    @else
                                        <span>{{$i + 1}}. {{$dr->name ?? ""}}</span>
                                    @endif
                                </div>
                                <div class="col-md-7 col-xs-7">Địa chỉ: {{$dr->address ?? ""}}<br>Điện thoại: {{$dr->tel ?? ""}}</div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(document).ready(function() {
        $('.fancybox').fancybox();
    });
</script>
@endpush
