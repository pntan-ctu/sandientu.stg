@extends('layouts.web')
@section('title')
{{$Organization->name}}
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("/image/300/300/".$Organization->logo_image))
@section('content')
<style>
    #modal-send-message td{border: none}
    #modal-send-message table{margin-bottom: 0px}
</style>
@php
    $userId = getUserId();
    $blackList = $Organization->blacklists;
    $isBlock = 0;
    foreach ($blackList as $bl){
        if($bl->user_id == $userId){
            $isBlock = 1;
            break;
        }
    }
@endphp
<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1>Cơ sở sản xuất kinh doanh</h1></div>
    </div>
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('/shop')}}">Cơ sở sản xuất kinh doanh</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="content-center">
            <div class="row">
                <div class="col-md-3 hidden-sm hidden-xs">
                    <div class="shop-page__info">
                        @include('web.products.productscate_left')
                    </div>
                </div>
                <div class="col-md-9 col-sm-12 col-xs-12">
                    <div class="shop-page-wrapper">

                        <div class="shop-page__info">

                            <h1>{{$Organization->name}}
                                @if($Organization->status==1)
                                <i class="fa fa-check-circle" title="Đã kiểm duyệt"></i>
                                <!--<img class="img_nhanchungnhan" style="margin-left:10px;margin-top:5px" src="{{url('css/images/checked.png')}}" title="đã chứng nhận"></h1>-->
                                @elseif($Organization->status==0)
                                <i class="fa fa-ban" title="Chưa kiểm duyệt"></i>
                                    <!--<img class="img_nhanchungnhan" style="margin-left:10px;margin-top:5px" src="{{url('css/images/unnamed.jpg')}}">-->
                                @endif
                               
                            </h1>
                            <div class="row section-seller-overview-horizontal">
                                <div class="col-md-5">
                                    <div class="section-seller-overview-horizontal__leading">
                                        <div class="section-seller-overview-horizontal__leading-background" style="background-image: url({{url('css/images/bg-page.jpg')}});"></div>
                                        <div class="section-seller-overview-horizontal__leading-background-mask"></div>
                                        <div class="section-seller-overview-horizontal__leading-content">
                                            <div class="section-seller-overview-horizontal__seller-portrait">
                                                <a class="section-seller-overview-horizontal__seller-portrait-link" href="#">
                                                    <div class="avatar-tp">
                                                        <div class="avatar__placeholder"></div>
                                                        <img class="avatar__img" src="{{ $Organization->getLogo(200, 200) }}">
                                                    </div>
                                                </a>
                                                <div class="section-seller-overview-horizontal__portrait-info">
                                                    <div class="section-seller-overview-horizontal__portrait-status">
                                                        <div class="section-seller-overview-horizontal__active-time">{{$Organization->status==0?'Chưa duyệt':($Organization->status==1?'Đang hoạt động':($Organization->status==2?'Ngừng hoạt động':'Khóa do vi phạm'))}}</div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="section-seller-overview-horizontal__buttons">
                                                <div class="section-seller-overview-horizontal__button">
                                                    <button class="button-outline button-outline--complement button-outline--fill button-add-follow-cssx">theo dõi</button>
                                                    <button class="button-remove-follow-cssx">Đang Theo Dõi</button>
                                                </div>
                                                <div class="section-seller-overview-horizontal__button">
                                                    <!--button class="button-outline button-outline--complement button-outline--fill button-outline-- "  onClick='openFormSendMessage()'>Gửi tin nhắn</button-->
                                                    <a class="button-outline button-outline--complement button-outline--fill button-outline-- "  href='{{ url("messenger/".$Organization->id) }}'>Chat ngay</a>
                                                </div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div class="cssx-product">
                                        <div class="like-cssx">
                                            <span class="">
                                            <svg width="24" height="20" class="svg-like">
                                            <path d="M19.469 1.262c-5.284-1.53-7.47 4.142-7.47 4.142S9.815-.269 4.532 1.262C-1.937 3.138.44 13.832 12 19.333c11.559-5.501 13.938-16.195 7.469-18.07z" stroke="#FF424F" stroke-width="1.5" fill="none" fill-rule="evenodd" stroke-linejoin="round"></path>
                                            </svg>
                                        </span>
                                        <span>Đã thích </span><span class="totalLike"> ({{ $Organization->favorites()->count()}}) </span>
                                        </div>
                                        
                                        <span>|</span>
                                        <button class="button-report-cssx" onclick="showModalViphamOrgan()"><i class="fa fa-flag-checkered"></i>&nbsp; Phản ánh vi phạm</button>
                                    </div>
                                   
                                        
                                </div>
                                <div class="col-md-7">		
                                    <div class="section-seller-overview-horizontal__seller-info-list pdp-mod-review">

                                        <div class="section-seller-overview__item mod-rating mod-rating-cssx">
                                            <div class="summary">
                                                <div class="score"><span>{{($ratings->count()>0)? number($ratings->sum('rate')/$ratings->count()):'0'}}</span> trên 5</div>
                                                <div class="average">
                                                    <div class="container-star " style="width:166.25px;height:30px">
                                                         <input type="hidden"  data-readonly value="{{($ratings->count()>0)? $ratings->sum('rate')/$ratings->count():'0'}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" />
                                                    </div>
                                                </div>
                                                <div class="count">
                                                    <span>
                                                        {{($ratings->count()>0)? $ratings->count():'0'}} đánh giá
                                                    </span>
                                                    <span>|</span>
                                                    <span>
                                                        {{$Organization->visits()->count()}} lượt xem
                                                    </span>
                                                
                                                </div>
                                            <div class="count">
                                                @if($Organization->certificates->count() > 0)
                                                @foreach($Organization->certificates as $cer)
                                                <image class="img-certifications-shops" src="{{url('image/50/50/'. $cer->certificateCategory->icon) }}" 
                                                     data-toggle="tooltip" data-placement="bottom"  data-original-title="<b>{{$cer->certificateCategory->name }} </b><hr style='margin:5px;'> {{$cer->certificateCategory->description}}" />
                                                @endforeach
                                                @endif
                                            </div>
                                            </div>
                                        </div>
                                        <div class="section-seller-overview__item mod-rating mod-rating-cssx">
                                            <div class="detail">
                                                <ul>
                                                    @for ($i = 5; $i >0 ; $i--)
                                                    <li>
                                                        <span>
                                                            <span class="star-number">{{ $i }}</span> SAO
                                                        </span>
                                                        <span class="progress-wrap">
                                                            <div class="pdp-review-progress">
                                                                <div class="bar bg"></div>
                                                                <div class="bar fg" style="width:{{($ratings->count()>0)? ($ratings->where('rate',$i)->count()/$ratings->count())*100:'0'}}%"></div>
                                                            </div>
                                                        </span>
                                                        <span class="percent">{{($ratings->count()>0)? $ratings->where('rate',$i)->count():'0'}}</span>
                                                    </li>
                                                    @endfor


                                                </ul>
                                            </div>
                                        </div>

                                    </div>		
                                </div>	
                            </div>
                        </div>
                    </div>
                    @php
                        $isProductTab = $_GET['page'] ?? 0;
                        $iPage = $_GET['page'] ?? 1;
                    @endphp
                    <div class="shop-page-menu">
                        <ul class="nav nav-tabs">
                            @foreach ($contentOrgani as $key => $value)
                            <li class="col-100 @if($key == 0 && $isProductTab == 0) active @endif" >
                                 <a  href="#tab{{$key}}" data-toggle="tab">{{$value->name }}</a>
                            </li>
                            @endforeach
                            <li class="col-100"><a href="#tab{{$contentOrgani->count() }}" data-toggle="tab" aria-expanded="false">Thông tin chi tiết</a></li>
                            <li class= "col-100 @if($isProductTab > 0) active @endif" ><a href="#tab{{$contentOrgani->count()+3 }}" data-toggle="tab" aria-expanded="false">Sản phẩm</a></li>
                            <li class="col-100"><a href="#tab{{$contentOrgani->count()+1 }}" data-toggle="tab" aria-expanded="false">Đánh giá</a></li>
                            <li class="col-100"><a href="#tab{{$contentOrgani->count()+2 }}" data-toggle="tab" aria-expanded="false">Hỏi & Đáp</a></li>
                            <li class="col-100"><a href="#tab{{$contentOrgani->count()+4 }}" data-toggle="tab" aria-expanded="false">Địa điểm</a></li>
                        </ul>
                        <div class="wrap-content-shop-detail">
                            <div class="tab-content">
                                @foreach ($contentOrgani as $key => $value)
                                <div class="tab-pane @if($key == 0 && $isProductTab == 0) active @endif" id="tab{{$key}}">
        <!--                            <div class="title-cssx">
                                        <h2 class="pdp-mod-section-title outer-title">{{$value->name }} </h2>
                                    </div>-->
                                    <div class="pdp-mod-detail">
                                        {!! $value->content !!}
                                    </div>
                                </div>
                                @endforeach
                                @include('web.shops.detail_organ_tab')
                                @include('web.shops.product_organ_tab')
                                @include('web.shops.rating_organ_tab')
                                @include('web.shops.qa_organ_tab')
                                @include('web.shops.maps_organ_tab')
                            </div>
                        </div>
                    </div>
        <!--            <div class="box-note">
                        <p>ATTP THA là một kênh kết nối nhà cung cấp thực phẩm sạch, thực phẩm hữu cơ tới người tiêu dùng. Người dùng có thể tìm hiểu thông tin của nơi cung cấp, đồng thời có thể đưa ra đánh giá để cùng tạo nên một cộng đồng nhà sản xuất, người tiêu dùng đáng tin cậy.</p>
                        <p><a href="#"> Mua hàng&nbsp;</a>- Ủng hộ nhà sản xuất có tâm <a href="#">ở đây</a></p>
                        <p>Đường dây nóng: <span>0918390393</span></p>
                    </div>-->
                </div>
            </div>        

        </div>
    </div>
</div>	
@include('web.shops.modal_rate_organ')
@include('web.shops.modal_vipham_organ')
<div class="modal fade" id="modal-send-message" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Gửi tin nhắn</h4>
            </div>
            <div class="modal-body">
                <table class="table" width="100%">
                    <tr>
                        <td width="80px">Tiêu đề</td>
                        <td>
                            <input type="hidden" id="org_id" value="{{$Organization->id}}" >
                            <input id="msgSubject" type="text" maxlength="511" style="width: 100%"/>
                        </td>
                    </tr>
                    <tr>
                        <td>Nội dung</td>
                        <td>
                            <textarea id="msgContent" rows="5" style="width: 100%"></textarea>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal">
                    <i class="fa fa-times"></i> Đóng
                </button>
                <button type="button" id="btnSend" class="btn btn-primary">
                    <i class="fa fa-sign-in"></i> Gửi
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
    $(function () {
        //var is_touch_device = ("ontouchstart" in window) || window.DocumentTouch && document instanceof DocumentTouch;
        $('[data-toggle="tooltip"]').tooltip({
            html:true,
           // trigger:  is_touch_device ? "click" : "hover",
        });  
        var checkLike = {{$favoriteCount}};
        if(checkLike <1){
            $(".like-cssx path").css("fill", "none");
        }
        else{
            $(".like-cssx path").css("fill", "rgb(255, 66, 79)");
        }
        var checkFollow = {{$followCount}};
        if(checkFollow <1){
             $(".button-add-follow-cssx").css("display", "block");
             $(".button-remove-follow-cssx").css("display", "none");
        }
        else{
            $(".button-add-follow-cssx").css("display", "none");
             $(".button-remove-follow-cssx").css("display", "block");
        }
        $('.like-cssx').on('click', function (e) {
            if(!checkLogin())
                return false;
            var color = $( 'path' ).css( "fill" );
            if(color == 'none'){
                $(".like-cssx path").css("fill", "rgb(255, 66, 79)");
            }
            else{
                $(".like-cssx path").css("fill", "none");
            }
            var form_data = new FormData();
            form_data.append('organId', {{$Organization->id}});
            $.ajax({
                url: "{{url('add-like-organization')}}",
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
            }).done(function (msg) {
                if (msg.message == 'success'){
                    $('.totalLike').html('');
                    $('.totalLike').html('('+msg.total+')');
                }
            });
        });
        
        // follow
        $('.button-add-follow-cssx, .button-remove-follow-cssx').on('click', function (e) {
            if(!checkLogin())
                return false;
            var display = $( '.button-add-follow-cssx' ).css( "display" );
            if(display == 'none'){
                $(".button-add-follow-cssx").css("display", "block");
                $(".button-remove-follow-cssx").css("display", "none");
            }
            else{
                var isBlock = {{ $isBlock }};
                if(isBlock == 1){
                    toastr.error("Không thể thực hiện do bạn đang bị chặn");
                    return;
                }

                $(".button-remove-follow-cssx").css("display", "block");
                $(".button-add-follow-cssx").css("display", "none");
            }
            var form_data = new FormData();
            form_data.append('organId', {{$Organization->id}});
            $.ajax({
                url: "{{url('add-follow')}}",
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
            }).done(function (msg) {
                
            });
        });
         
     });
    function showModalRateOrgan(){
        $('#modal-rate-organ').modal();
    }
    function validateRate(){
        var rate = $('.rating').rating('rate');
        if(!rate){
            alert("Bạn chưa đánh giá sản phẩm");
            return false;
        }
        if($('#txtNoteRating').val().trim() === ''){
            alert("Bạn chưa nhập ý kiến ");
            $('#txtNoteRating').focus();
            return false;
        }
        return true;
    }
    function saveRate(){
        if(!checkLogin())
                return false;
        if(!validateRate())
        {
            return false;
        }
        var rate = $('.rating').rating('rate');
        var form_data = new FormData();
        form_data.append('organId', {{$Organization->id}});
        form_data.append('rate', rate);
        form_data.append('comments', $('#txtNoteRating').val());
        $.ajax({
            url: "{{url('add-rate-organ')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
        }).done(function (msg) {
            if (msg.message == 'success'){
                toastr.success("Thêm đánh giá thành công !");
                $('#modal-rate-organ').modal('hide');
            }
        });
    }

    function showModalViphamOrgan(){
        $('#modal-vipham').modal();
    }
    function validateVipham(){
        if($('input[name=loaiViphamRadios]:checked').val() == null){
            alert("Bạn chưa chọn loại vi phạm ");
            return false;
        }
        if($('#txtVipham').val().trim() === ''){
            alert("Bạn chưa nhập ý kiến ");
            $('#txtVipham').focus();
            return false;
        }
        return true;
    }
    function themVipham(){
        if(!checkLogin())
            return false;
        if(!validateVipham())
            return false;
        var form_data = new FormData();
        form_data.append('typeId', {{$Organization->id}});
        form_data.append('comment', $('#txtVipham').val());
        form_data.append('loaiVipham', $('input[name=loaiViphamRadios]:checked').val());
        form_data.append('typeInfrin', 'Organization');
        $.ajax({
            url: "{{url('them-vi-pham')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
        }).done(function (msg) {
            if (msg.message == 'success'){
                toastr.success("Phản ánh vi phạm thành công !");
                $('#txtVipham').val('');
                $('#modal-vipham').modal('hide');
            }
        });
    }

    function openFormSendMessage(){
        if(!checkLogin()) {
            return false;
        }

        var isBlock = {{ $isBlock }};
        if(isBlock == 1){
            toastr.error("Không thể gửi tin nhắn do bạn đang bị chặn");
            return;
        }

        $("#msgSubject").val("");
        $("#msgContent").val("");
        $('#modal-send-message').modal();
    }

    $("#btnSend").click(function () {
        var orgId = $("#org_id").val();
        var subject = $("#msgSubject").val().trim();
        if(subject.length == 0){
            toastr.error("Bạn chưa nhập tiêu đề");
            return;
        }

        var content = $("#msgContent").val().trim();
        if(content.length == 0){
            toastr.error("Bạn chưa nhập nội dung");
            return;
        }

        if(content.length > 1023){
            toastr.error("Nội dung không được vượt quá 1023 ký tự");
            return;
        }

        $.ajax({
            url: "{{url('user/sendMessageToOrganization')}}",
            type: 'post',
            data: {
                org_id: orgId,
                msg_subject: subject,
                msg_content: content
            },
            success: function (result) {
                $("#modal-send-message").modal("hide");
                toastr.success("Gửi tin nhắn thành công.");
            },
            error: function(){
                toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
            }
        });
    });
</script>
@endpush