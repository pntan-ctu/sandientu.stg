@extends('layouts.web')
@section('title')
{{$message}}
@endsection
@section('content')

<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1>Cơ sở sản xuất kinh doanh</h1></div>
    </div>
    <div class="container">
        <div class="col-md-3 col-sm-12 col-xs-12">
            <div class="shop-page__info">
                @include('web.products.productscate_left')
            </div>
        </div>
        <div class="col-md-9 col-sm-12 col-xs-12">
            <div class="shop-page-wrapper">
                <div class="shop-page__info">
                    <h3> {{$message}} </h3>
                </div>
            </div>
        </div>
    </div>
</div>	
@endsection
