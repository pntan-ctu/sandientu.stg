<div class="tab-pane @if($isProductTab > 0) active @endif" id="tab{{$contentOrgani->count() + 3}}">
    <div class="content pdp-mod-review">
        <div class="organization-product">
            @include('web.products.productscate_table')
        </div>
    </div>
</div>