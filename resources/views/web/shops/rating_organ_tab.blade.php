<div class="tab-pane" id="tab{{$contentOrgani->count()+1 }}">
    <div class="content pdp-mod-review">
        <div class=" product-rating-overview">
            <div class="mod-title">
                <h2 class="pdp-mod-section-title outer-title">Đánh giá và nhận xét của: {{$Organization->name}}</h2>
            </div>
            <div class="mod-rating">
                <div class="summary">
                    <div class="score"><span>{{($ratings->count()>0)? number($ratings->sum('rate')/$ratings->count()):'0'}}</span> trên 5</div>
                    <div class="average">
                        <div class="container-star " style="width:166.25px;height:30px">
                            <input type="hidden"  data-readonly value="{{($ratings->count()>0)? $ratings->sum('rate')/$ratings->count():'0'}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" />
                        </div>
                    </div>
                    <div class="count">{{($ratings->count()>0)? $ratings->count():'0'}} đánh giá</div>
                </div>
                <div class="detail">
                    <ul>
                        @for ($i = 5; $i >0 ; $i--)
                        <li>
                            <span>
                                <span class="star-number">{{ $i }}</span> SAO
                            </span>
                            <span class="progress-wrap">
                                <div class="pdp-review-progress">
                                    <div class="bar bg"></div>
                                    <div class="bar fg" style="width:{{($ratings->count()>0)? ($ratings->where('rate',$i)->count()/$ratings->count())*100:'0'}}%"></div>
                                </div>
                            </span>
                            <span class="percent">{{($ratings->count()>0)? $ratings->where('rate',$i)->count():'0'}}</span>
                        </li>
                        @endfor
                    </ul>
                </div>
                <div class="">
                    <button type="button" onclick="showModalRateOrgan()" class="next-btn next-btn-primary next-btn-medium btn-rate">ĐÁNH GIÁ CƠ SỞ NÀY</button>
                </div>
            </div>	
        </div>
        <div class="mod-reviews">
            @if(isset($ratings))
            @foreach($ratings as $rating)
            <div class="item">
                <div class="top">
                    <span class="container-star starCtn pull-left" style="width:140px;height:16px">
                        <input type="hidden"  data-readonly value="{{$rating->rate}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" />
                    </span>
                    <span class="title left">&nbsp;</span>
                    <span class="right">{{ $rating->created_at->toShortDateString()}}</span>
                </div>
                <div class="middle">
                    <span>bởi<!-- --> <!-- -->
                        <a href='{{$rating->user->getUrlActivity()}}'>{{$rating->user->name}}</a>
                    </span>

                </div>
                <div class="content">{{ $rating->comment }}</div>
                <div class="bottom">
                </div>
            </div>
            @endforeach
            @endif

        </div>	
    </div>
</div>
