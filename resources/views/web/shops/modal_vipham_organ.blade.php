<div class="modal fade" id="modal-vipham" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-flag-checkered"></i> Phản ánh vi phạm </h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive">
                    <div class="box-body">
                        <div class="fs-dtrtcmtbox">
                            @if(isset($loaiVipham))
                            @foreach ($loaiVipham as $key => $value)
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="loaiViphamRadios" id="radios{{$value->id}}" value="{{ $value->id }}">
                                <label class="form-check-label" for="radios{{$value->id}}">
                                  {{ $value->name }}
                                </label>
                            </div>
                            @endforeach
                            @endif
<!--                            <select id="loaiVipham" class="form-control" onchange="" >
                                <option value="0">---- Chọn loại vi phạm ----</option>
                                @foreach ($loaiVipham as $key => $value)
                                    <option value="{{ $value->id }}">{{ $value->name }}</option>
                                @endforeach
                            </select>-->
                            <hr/>
                            <textarea id="txtVipham" name="txtVipham" rows="3" class="f-cmttarea fsformsc" placeholder="Vui lòng nhập ý kiến của bạn!"></textarea>
                            <div class="fs-dtrtbots clearfix"> 
                                <p>Một ý kiến có ích thường dài từ 100 ký tự trở lên</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Hủy</button>
                <button id="btn-delete" type="button" class="btn btn-primary" onClick='themVipham()'><i class="fa fa-check"></i>Gửi</button>
            </div>
        </div>
    </div>
</div>

