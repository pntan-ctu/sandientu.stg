<div class="tab-pane" id="tab{{$contentOrgani->count()+ 4 }}">
    <div class="content pdp-mod-review">
        
    @push('styles')
    <link href="{{ asset('css/map.css') }}" rel="stylesheet">
    @endpush    

        <div style = "position: relative; width:100%">
            <div style="clear:both;"></div>
            <div id="map" style="width:100%; height:500px"></div>    
            @push('scripts')
            <script>
                function initMap() {
                    var thanhhoa = new google.maps.LatLng(19.807812, 105.776747);

                    var map = new google.maps.Map(document.getElementById('map'), {
                          scaleControl: true,
                          center: thanhhoa,
                          zoom: 10
                    });
                    var organization100 = {!! $Organization->branches !!};
                    organization100.push({!! $Organization !!});
                    var url1 = "{{url('shop')}}/";
                    $.each(organization100, function (index, value) {
                        var thanhhoa1 = new google.maps.LatLng(value.map_lat, value.map_long);
                        var latLong = value.map_lat + "," + value.map_long;
                        var title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
                        var coordInfoWindow = new google.maps.InfoWindow();
                        coordInfoWindow.setContent(createInfoWindowContent(value,title, map.getZoom()));
                        var marker = new google.maps.Marker({map: map, position: thanhhoa1});
                          marker.addListener('click', function() {
                            coordInfoWindow.open(map, marker);
                          });

                        map.addListener('zoom_changed', function() {
                          coordInfoWindow.setContent(createInfoWindowContent(new thanhhoa1, map.getZoom()));
                          coordInfoWindow.open(map);
                        });
                    });
                }

                var TILE_SIZE = 256;

                function createInfoWindowContent(value,latLong, zoom) {
                  var scale = 1 << zoom;
                  return [
                      latLong,
                    //value.name,
                    //'Chi tiếṭ: ' + latLong,
                    'Địa chỉ: ' + value.address,
                    'Tel: ' + value.tel,
                    'Email: ' + value.email,
                    'Website: ' + value.website
                  ].join('<br>');
                }

                // The mapping between latitude, longitude and pixels is defined by the web
                // mercator projection.
                function project(latLng) {
                  var siny = Math.sin(latLng.map_lat * Math.PI / 180);

                  // Truncating to 0.9999 effectively limits latitude to 89.189. This is
                  // about a third of a tile past the edge of the world tile.
                  siny = Math.min(Math.max(siny, -0.9999), 0.9999);

                  return new google.maps.Point(
                      TILE_SIZE * (0.5 + latLng.map_long / 360),
                      TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
                }
                $(document).ready(function() {
                  google.maps.event.addDomListener(window, 'load', initMap);
                });
              </script>
             
              <!-- <script async defer   src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBtm_IVH680qlXf1N7sxKSrkLMM93KCl3c&sensor=false&callback=initMap">
              </script> -->
    @endpush
        </div>
    </div>	
</div>
