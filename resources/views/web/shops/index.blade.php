@extends('layouts.web')
@section('title')
Cơ sở SX, KD
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')

<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"></div>
    </div>
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('/shop')}}">Cơ sở sản xuất kinh doanh</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        
        <div class="">
            <div class="col-md-1-5 hidden-sm hidden-xs">
                @include('web.products.productscate_left')
            </div>
            <div class="col-md-4-5 cssxkd-wrap">
                <div class="toolbar">
                    <div class="sortsoffer">Danh sách Cơ sở sản xuất, kinh doanh</div>
                    @if($AreaId > 0)
                        <div><i style="color: green">(thuộc vùng sản xuất {{$AreaName}})</i></div>
                    @endif

                    @if($CommercialCenterId > 0)
                        <div><i style="color: green">(thuộc địa điểm kinh doanh {{$CommercialCenterName}})</i></div>
                    @endif
                </div>
                <div class="cs-grid">
                    <div class="cs-layout cs-list">
                        @foreach ($Organization as $key => $value)
                        <div  class="media">
                            <a href="{{url('shop/'.$value->path)}}" class="item-img pull-left">
                                <img class="" title="" alt="" src="{{isset($value->logo_image)? url("/image/350/350/".$value->logo_image):url("css/images/logo.png")}}" style="width: 100%; max-height: 250px">
                            </a>
                            <div class="item-info">
                                <div class="item-title"><a href="{{url('shop/'.$value->path)}}">{{ $value->name }}</a>
                                    @if($value->status==1)
                                    <i class="fa fa-check-circle" title="Đã kiểm duyệt"></i>
                                    @elseif($value->status==0)
                                    <i class="fa fa-ban" title="Chưa kiểm duyệt"></i>
                                    @endif
                                </div>
                                <div class="item-content">	
                                    <p>
                                        <b>Sản phẩm cung cấp:</b> @foreach ($value->products as $key => $product) <a href="{{url('san-pham/'.$product->path)}}">{{ $product->name.";" }} </a> @endforeach
                                    </p>
                                    <p><b>Địa chỉ: </b> {{ $value->address ." - ". ($value->region? $value->region->full_name:"")  }}</p>
                                    <!--<p><b>Địa điểm giao hàng: </b>Toàn quốc</p>-->
                                </div>
                                <div id="newbie_review" class="review_total">
                                    <input type="hidden"  data-readonly value="{{($value->ratings->count()>0)? $value->ratings->sum('rate')/$value->ratings->count():'0'}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" />
                                   
                                    <div class="num_total">( {{($value->ratings->count()>0)? $value->ratings->count():'0'}} người đánh giá )</div>
                                </div>
                            </div>
                        </div> 
                        @endforeach
                    </div>
                </div>
                @if (isset($_GET['area_id']))
                    {!! $Organization->appends(['area_id' => $_GET['area_id']])->render() !!}
                @else
                    @if (isset($_GET['commercial_center_id']))
                        {!! $Organization->appends(['commercial_center_id' => $_GET['commercial_center_id']])->render() !!}
                    @else {!! $Organization->render() !!}
                    @endif
                @endif
            </div>			 			
        </div>

    </div> 	

</div>
</div>	

@endsection
