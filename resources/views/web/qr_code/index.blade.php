@extends('layouts.web')
@section('title')
Chuyển đổi mã QRCode
@endsection
@section('content')
<div class="block-videos">
    <div class="wrap-page">
         <div class="wrap-breadcrumb">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                        <li><a href="#">Tạo QRCode</a></li>
                    </ul>
                </div>
            </div>
        <div class="container">
            <div class="buy-center" >
                <div class="row">
                    <div class="col-md-12">
                        <div class="blocks-contact-title" style="text-align: center">
                            <h3>Chuyển đổi sang mã QRCode</h3>
                        </div>
                        <form role="form" method="get" action="{{url('get-app-qrcode')}}"  class="row formcontact">
                            <div class="col-xs-3 " style="margin-bottom:20px;">
                            </div>
                            @if(@isset($name))
                            <div class="col-xs-5 " style="margin-bottom:20px;">
                                <input type="text" class="form-control" id="name" name="name" value="{{$name}}" placeholder="Nhập chuỗi cần chuyển đổi" required>
                            </div>
                            @else
                            <div class="col-xs-5 " style="margin-bottom:20px;">
                                <input type="text" class="form-control" id="name" name="name" placeholder="Nhập chuỗi cần chuyển đổi" required>
                            </div>
                            @endif
                            <div class="col-xs-1 " style="margin-bottom:20px;">
                                <button  type="submit" id="submit" class="btn btn-success">Covert</button>
                            </div>
                            <div class="col-xs-3 " style="margin-bottom:20px;">
                            </div>
                            <div class="col-xs-12 " style="margin-bottom:20px;text-align: center">
                                <h2>Chuỗi sau khi chuyển đổi</h2>
                                @if(@isset($qrCode))
                                <img src="data:image/png;base64, {{ base64_encode($qrCode) }} ">
                                @endif
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>	
    </div>
</div>
@endsection

