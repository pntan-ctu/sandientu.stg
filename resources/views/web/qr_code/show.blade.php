<html>
    <head>
        <script src="bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript">
            $(function() {
                var getMobileOperatingSystem = function() {
                      var userAgent = navigator.userAgent || navigator.vendor || window.opera;

                      // Windows Phone must come first because its UA also contains "Android"
                      if (/windows phone/i.test(userAgent)) {
                          return "Windows Phone";
                      }

                      if (/android/i.test(userAgent)) {
                          return "Android";
                      }

                      // iOS detection from: http://stackoverflow.com/a/9039885/177710
                      if (/iPad|iPhone|iPod/.test(userAgent) && !window.MSStream) {
                          return "iOS";
                      }

                      return "unknown";
                 };

                 var platform=getMobileOperatingSystem();
                 if(platform=="Android"){
                     window.location.href="{{ config('app.download_app_andoid', '') }}"
                 }
                 else if(platform=="iOS"){
                     window.location.href="{{ config('app.download_app_ios', '') }}";
                 }
                 else{
                     window.location.href="{{ config('app.url', '') }}";
                 }
            });
        </script>
    </head>
    <body>
    </body>
</html>