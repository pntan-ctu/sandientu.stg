@extends('layouts.web')
@section('title')
Danh sách vùng sản xuất
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('content')

<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1></h1></div>
    </div>
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('/vung-san-xuat')}}">Vùng sản xuất</a></li>
            </ul>
        </div>
    </div>
    <div class="container">

        <div class="">
            <div class="col-md-1-5 hidden-sm hidden-xs">
                @include('web.products.productscate_left')
            </div>
            <div class="col-md-4-5 cssxkd-wrap">
                <div class="toolbar">
                    <div class="sortsoffer">Danh sách vùng sản xuất</div>
                </div>
                <div class="cs-grid">
                    <div class="cs-layout cs-list">

                        @foreach ($areas as $key => $value)
                        <div  class="media">
                            <a href="{{url('vung-san-xuat/'.$value->path)}}" class="item-img pull-left">
                                <img class="" title="" alt="" src="{{isset($value->map) ? url("/image/350/350/".$value->map) : url('img/no-image.png')}}" style="width: 100%; max-height: 250px">
                            </a>
                            <div class="item-info">
                                <div class="item-title"><a href="{{url('vung-san-xuat/'.$value->path)}}">{{ $value->name }}</a></div>
                                <div class="item-content">	
                                    <p><b>Địa phương</b>: {{ $value->region->full_name }}</p>
                                    <p><b>Cơ quan quản lý</b>: {{ $value->manageOrganization->name }}</p>
                                </div>
                            </div>
                        </div> 

                        @endforeach


                    </div> 	
                </div>
                {!! $areas->render() !!}
            </div>			 			
        </div>

    </div> 	

</div>
</div>	

@endsection
