@extends('layouts.web')
@section('title')
{{$area->name}}
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
<style>
    .sec-title {background: #86bc42;padding: 5px;font-weight: bold;color:white}
    .pro-item{margin-top: 10px; margin-bottom: 10px; border-bottom: 1px solid #8080803d}
    .pro-item>a>div:first-child{width: 70px; height: 65px; float: left; border: 1px solid #8080803d; margin-bottom: 5px}
    .pro-item>a>div>img{width: 70px; height: 60px}
    .pro-item>a>div:nth-child(2){width: calc(100% - 70px); float: left; padding-left: 8px}

</style>

@section('content')
<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1></h1></div>
    </div>
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('/vung-san-xuat')}}">Vùng sản xuất</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="col-md-1-5 hidden-sm hidden-xs">
            <div class="">
                @include('web.products.productscate_left')
            </div>
        </div>
        <div class="col-md-4-5 col-sm-12 col-xs-12">
            <div class="area-page-wrapper">
                <div class="area-page__info" style="padding-bottom: 0px">
                    <h1>{{$area->name}}</h1>
                </div>
                <div class="row">
                    <div class="col-md-9 col-sm-9 col-xs-9 col-100" style="font-size: 16px; margin-top: 15px;">
                        <p><b>Địa phương</b>: {{ $area->region->full_name }}.</p>
                        @if(isset($area->map))
                        <div class="row" style="text-align: center; padding-top: 10px; padding-bottom: 5px;">
                            <img class="" title="" alt="" src="{{url("/image/500/350/".$area->map)}}" style="width: 80%; max-height: 350px">
                        </div>
                        @endif
                        <div class="row" style="margin-right: 5px; padding-top: 10px; text-align: justify">
                            {!! $area->instroduction !!}
                        </div>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 col-100">
                        <div class="sec-title">Các sơ sở trong vùng</div>
                        @foreach($organizations as $org)
                            <div class='rpro-item media'>
                                <a href="{{url("/shop/".$org->path)}}">
                                    <a class="pull-left" style="width: 40%">
                                        <img src="{{url("/image/70/60/".$org->logo_image)}}">
                                    </a>
                                    <div class="media-body">
                                        {{$org->name}}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                        @if($num_of_orgs>6)
                            <div class="" style="text-align: right"><a href="{{url("/shop?area_id=".$area->id)}}">Xem thêm >></a></div>
                        @endif
                        <br>

                        <div class="sec-title">Sản phẩm tiêu biểu</div>
                        @foreach($products as $p)
                            <div class='pro-item media'>
                                <a href="{{url("/san-pham/".$p->path)}}">
                                    <a class="pull-left" style="width: 40%">
                                        <img src="{{url("/image/70/60/".$p->avatar_image)}}">
                                    </a>
                                    <div class="media-body">
                                        {{$p->name}}
                                    </div>
                                </a>
                            </div>
                        @endforeach
                    </div>
                </div>    
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>	

@endsection