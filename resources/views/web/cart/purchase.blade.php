@extends('layouts.web')
@section('title')
Đặt hàng
@endsection
@section('content')
@php
    $userId = getUserId();
    $regionToId = $_GET['region'] ?? (Session::get($userId.'receiveRegionId') ?? ($infoOrder['region_id'] ?? 0));
    if($regionToId > 0) Session::put($userId.'receiveRegionId', $regionToId);
    $region = \App\Models\Region::find($regionToId);

    $receiveTel = $_GET['tel'] ?? (Session::get($userId.'receiveTel') ?? ($infoOrder['tel'] ?? ''));
    if(strlen($receiveTel) > 0) Session::put($userId.'receiveTel', $receiveTel);

    $receiveName = $_GET['name'] ?? (Session::get($userId.'receiveName') ?? ($infoOrder['name'] ?? ''));
    if(strlen($receiveName) > 0) Session::put($userId.'receiveName', $receiveName);

    $receiveAddress = $_GET['address'] ?? (Session::get($userId.'receiveAddress') ?? ($infoOrder['address'] ?? ''));
    if(strlen($receiveAddress) > 0) Session::put($userId.'receiveAddress', $receiveAddress);
@endphp
<div class="wrap-page">
    <div class="register-bg">
        <div class="block-title-h1">
            <div class="container"><h1>Thanh toán</h1></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="page-checkout col-md-10 col-md-offset-1">
                    <div class="checkout-address-selection__container">
                        <div class="checkout-address-selection__section-header-text">
                            <i class="fa fa-map-marker" aria-hidden="true"></i>
                            Địa chỉ nhận hàng
                            <a class="checkout-address-selection__change-btn" onclick="showModelInfoOrder()">Thay đổi</a>
                        </div>
                        <div class="checkout-address-selection__selected-address-summary">
                            <div class="checkout-address-row__user-detail name_info">{{$receiveName.' - '.$receiveTel}}</div>
                            <div class="checkout-address-row__address-summary address-full">{{ $receiveAddress. ' - '.($region->full_name ?? $infoOrder['full_name'])}}  </div>
                        </div>

                    </div>
                    <div class="checkout-product-ordered-list cart-page__content">
                        <div class="checkout-product-ordered-list__headers">
                            <div class="checkout-product-ordered-list__header">
                                <div class="checkout-product-ordered-list__title">Sản phẩm</div>
                            </div>
                            <div class="checkout-product-ordered-list__header">Đơn giá</div>
                            <div class="checkout-product-ordered-list__header">Số lượng</div>
                            <div class="checkout-product-ordered-list__header">Đơn vị tính</div>
                            <div class="checkout-product-ordered-list__header">Thành tiền</div>
                        </div>
                        <div class="checkout-product-ordered-list__content"> 
                            <div class="checkout-shop-order-group">
                                <div class="checkout-shop-order-group__orders">
                                    @if(Cart::content() != null)
                                    @php $totalShop =[]; $finalAmount = 0; $payOnlineAmount = 0; @endphp
                                    @foreach($Cart as $nameOrgan => $collection) 
                                        @php $countShop = 0; $weight = 0; @endphp
                                        <div class="cart-page-shop-header">
                                            <a class="cart-page-shop-header__shop-name" href="#">
                                                <span style="margin-left: 10px;">{{$nameOrgan}}</span>
                                            </a>
                                        </div>
                                        @foreach($collection as $product)
                                            @php $countShop += $product->qty; @endphp
                                            <div class="checkout-product-ordered-list-item__items">
                                                <div class="checkout-product-ordered-list-item__item">
                                                    <div class="checkout-product-ordered-list-item__header checkout-product-ordered-list-item__header--product">
                                                        <img class="checkout-product-ordered-list-item__product-image" src="{{url('/image/300/300/'.$product->options->avatar_image)}}" width="40" height="40">
                                                        <span class="checkout-product-ordered-list-item__product-info">
                                                            <span class="checkout-product-ordered-list-item__product-name">{{$product->name}}</span>
                                                        </span>
                                                    </div>
                                                    <div class="checkout-product-ordered-list-item__header">{{currency($product->price, 'VND') }}</div>
                                                    <div class="checkout-product-ordered-list-item__header">{{$product->qty}}</div>
                                                    @php
                                                        $weight += ($product->qty)*($product->options->weight_per_unit);
                                                    @endphp
                                                    <div class="checkout-product-ordered-list-item__header">{{$product->options->unit}}</div>
                                                    <div class="checkout-product-ordered-list-item__header">{{currency($product->subtotal, 'VND')}}</div>
                                                </div>
                                            </div>
                                        @endforeach

                                        @foreach($total as $name => $subtotal)
                                            @if($name == $nameOrgan)
                                                @php
                                                    $organization_id = $collection->first()->options->organization_id;
                                                    $totalShop[]= $organization_id;
                                                    $shipType = \App\Models\OrganizationShipType::with('shipType')->where('organization_id',$organization_id)->orderBy('is_default', 'desc')->get();
                                                    $shipTypeName = ""; $paramShipType = null;
                                                    if($shipType->count()> 0) {
                                                        if(isset($_GET['s'.$organization_id])) {
                                                            $paramShipType = \App\Models\OrganizationShipType::find($_GET['s'.$organization_id]);
                                                        }
                                                        $curShipType = $paramShipType ?? $shipType->first();
                                                        $shipTypeName = $curShipType->shipType->name;
                                                    }
                                                    $shipTypeId = $curShipType->shipType->id ?? 0;
                                                    $payType = \App\Models\OrganizationPayType::with('payType')->where('organization_id',$organization_id);
                                                    if($shipTypeId == 1) {
                                                        $payType = $payType->where('pay_type_id', '!=', 2);
                                                    }
                                                    $payType = $payType->orderBy('is_default', 'desc')->get();

                                                    $payTypeName = "Liên hệ trực tiếp"; $paramPayType = null;
                                                    if($payType->count()> 0 ) {
                                                        if(isset($_GET['p'.$organization_id])) {
                                                            $paramPayType = \App\Models\OrganizationPayType::find($_GET['p'.$organization_id]);
                                                        }
                                                        $curPayType = $paramPayType ?? $payType->first();
                                                        $payTypeName = $curPayType->payType->name;
                                                    }

                                                    $existPayOnline = 0;
                                                    foreach($payType as $pay) if($pay->pay_type_id == 3 || $pay->pay_type_id == 4) {$existPayOnline = 1; break;}

                                                    $currOrg = \App\Models\Organization::find($organization_id);
                                                    $regionFromId = $currOrg->region_id;
                                                    $transFee = 15000;//($shipTypeId == 3 || $shipTypeId == 4) ? \App\Http\Controllers\Web\ShoppingCartController::calculatorFeeTransport($regionFromId, $regionToId, $weight, $shipTypeId) : 0;
                                                    if($transFee < 0) {
                                                        echo "Dữ liệu không phù hợp. Vui lòng liên hệ ban quản trị.";
                                                        exit;
                                                    }
                                                    $timeEstimate = ($shipTypeId == 3 || $shipTypeId == 4) ? \App\Http\Controllers\Web\ShoppingCartController::getTransportTime($shipTypeId, $regionFromId, $regionToId, $weight) : "";

                                                    $subAmount = $subtotal + $transFee;
                                                    $feeCOD = 0; $curPayTypeId = $curPayType->pay_type_id ?? 1;
                                                    if($curPayTypeId == 2 && ($shipTypeId == 3 || $shipTypeId == 4)) {
                                                        $feeCOD = \App\Http\Controllers\Web\ShoppingCartController::calculatorFeeCOD($subAmount, $regionFromId, $regionToId);
                                                        $transFee += $feeCOD;
                                                        $subAmount += $feeCOD;
                                                    }
                                                    $finalAmount += $subAmount;
                                                    $payOnlineAmount += ($curPayTypeId == 3 || $curPayTypeId == 4) ? $subAmount : 0;
                                                @endphp

                                                <div class="info-order-ship">
                                                    <div class="info-price-order row">
                                                        <span style="color: #ff5722; font-weight: 500; padding-left: 15px; float: left"> Phương thức vận chuyển: </span>
                                                        @if($shipType->count()> 0)
                                                            <input type="hidden" id="shiptypeId{{$organization_id}}" value="{{$curShipType->id}}" />
                                                            <input type="hidden" id="organId{{$organization_id}}" value="{{$organization_id}}" />
                                                            <input type="hidden" id="weight{{$organization_id}}" value="{{$weight}}" />
                                                            <span class="col-md-5 ship-type-text{{$organization_id}}" style="padding-left: 5px">{{$shipTypeName}}</span>
                                                            <span class="col-md-2" style="text-align: center">
                                                                @if(strlen($timeEstimate) > 0 && ($shipTypeId == 3 || $shipTypeId <= 4))
                                                                    <input type="hidden" id="shipTimeEstimate{{$organization_id}}" value="{{$timeEstimate}} ngày" />
                                                                    {{ $timeEstimate }} ngày
                                                                @else
                                                                    <input type="hidden" id="shipTimeEstimate{{$organization_id}}" value="" />
                                                                @endif
                                                            </span>
                                                            @if($shipType->count()> 1 )
                                                                <a style="text-align: center" class="col-md-2 checkout-address-selection__change-btn" onclick="showModelInfoShip({{$shipType}},{{ $organization_id}})">Thay đổi</a>
                                                            @endif
                                                        @else
                                                            Liên hệ
                                                        @endif
                                                    </div>
                                                    <div class="info-price-right">
                                                        @if($shipTypeId == 3 || $shipTypeId == 4)
                                                            {{currency($transFee, 'VND')}}
                                                        @endif
                                                        <input type="hidden" id="transFee{{$organization_id}}" value="{{$transFee}}" />
                                                    </div>
                                                </div>
                                                <div class="info-order-ship">
                                                    <div class="info-price-order">
                                                        <span style="color: #ff5722; font-weight: 500;"> Phương thức thanh toán: </span>
                                                        @if($payType->count()> 0 )
                                                            <input type="hidden" id="payTypeId{{$organization_id}}" value="{{$curPayType->id ?? 0}}" data="{{$curPayTypeId}}" amount="{{$subAmount}}" />
                                                            <span class="pay-type-text{{$organization_id}}">{{$payTypeName}}</span>
                                                            @if($payType->count()> 1 )
                                                                <a class="checkout-address-selection__change-btn" style="width:88px" onclick="showModelInfoPay({{$payType}}, {{ $organization_id}}, {{$existPayOnline}} )">Thay đổi</a>
                                                            @endif
                                                        @else
                                                            Liên hệ
                                                        @endif
                                                    </div>
                                                    <div class="info-price-right"></div>
                                                </div>
                                                <div class="info-order-ship">
                                                    <span style="color: #ff5722; font-weight: 500; margin-right: 5px"> Phí giao dịch: </span> 0 đ
                                                </div>
                                                <div class="info-order-organi">
                                                    <div class="info-price-order">
                                                        <span class="total-price">Tổng tiền ({{$countShop}} sản phẩm): <!-- {{$curPayTypeId}},   --><!-- {{$payOnlineAmount}} --><!-- ,  {{$existPayOnline}} --></span>
                                                        <span class="price-organi">{{currency($subAmount, 'VND')}}</span>
                                                    </div>
                                                    <div class="info-price-right">
                                                        @if( Cart::count() > 0)
                                                            <button class="button-solid-organi" onclick="addOrderByOrgani({{$collection->first()->options->organization_id}}, {{$curPayTypeId}}, '{{currency($subAmount, 'VND')}}')" >Đặt hàng</button>
                                                        @endif
                                                    </div>                                                </div>
                                            @endif
                                        @endforeach
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>  
                    </div>
                    <div class="cart-page-footer">
                        <div class="ct-card" style="text-align: left;float: left;justify-content: flex-start;background-color: #ddd;color: red;">
                            <div class="cart-page-footer__gap" style="width: 100%"><i>Lưu ý</i></div>
                            <ul>
                                <li> - Để tránh các sai sót, rủi ro, quý khách cần kiểm tra kỹ các thông tin trước khi thanh toán </li>
                            </ul>
                        </div>
                        <div class="clear"></div>
                        <div class="ct-card">
                            @if($payOnlineAmount > 0 && $finalAmount - $payOnlineAmount > 0)
                                <div class="col-md-10" style="text-align: right; font-weight: 500">Số tiền thanh toán trực tuyến:</div>
                                <div class="col-md-2" style="text-align: right; color: rgb(255, 87, 34); font-size: 20px; font-weight: 400">{{currency($payOnlineAmount, 'VND')}}</div>
                                <div class="col-md-10" style="text-align: right; font-weight: 500">Số tiền thanh toán bằng hình thức khác:</div>
                                <div class="col-md-2" style="text-align: right; color: rgb(255, 87, 34); font-size: 20px; font-weight: 400">{{currency($finalAmount - $payOnlineAmount, 'VND')}}</div>
                            @endif
                            <div class="cart-page-footer__summary col-md-10">
                                <div class="cart-page-footer__first-summary">
                                    <div class="cart-page-footer-summary__subtotal-text">Tổng tiền ({{ Cart::count() != null?Cart::count() :0}} sản phẩm):</div>
                                    <div class="cart-page-footer-summary__subtotal-amount">{{currency($finalAmount, 'VND')}}</div>
                                </div>
                            </div>
                            <div class="cart-page-footer__checkout col-md-2">
                                @if( Cart::count() > 0 )
                                    <button class="button-solid" onclick="addOrder( {{json_encode($totalShop) }}, {{$payOnlineAmount}}, '{{currency($payOnlineAmount, 'VND')}}' )">Đặt hàng</button>
                                @endif
                            </div>
                        </div>    
                    </div>
                </div>  
            </div>  
        </div>
    </div>  
</div>  


@include('web.cart.modal_info_order')
@include('web.cart.modal_info_ship')
@include('web.cart.modal_info_pay')
@endsection
@push('scripts')
<script src="{{asset('js/jquery.validate.min.js')}}" type="text/javascript"></script>
<script src="{{asset('js/web/purchase.js')}}" type="text/javascript"></script>
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script>
    var regionId = {{Session::get($userId.'receiveRegionId') ?? 0}};
    if(regionId == 0) {
        showModelInfoOrder();
    }
</script>
@endpush

