@php
    $paths = explode("/", $region->path_primary_key ?? "");
    $tinhId = $paths[0]; $huyenId = $paths[1] ?? 0; $xaId = $paths[2] ?? 0;
    $huyens = \App\Models\Region::where('parent_id', '=', $tinhId)->select(['id', 'name'])->get();
    $xas = \App\Models\Region::where('parent_id', '=', $huyenId)->select(['id', 'name'])->get();
@endphp
<div class="modal fade" id="modal-info-order" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> Địa chỉ nhận hàng </h4>
            </div>
            <form id="frm_InfoOrder">
                <div class="modal-body">
                    <div class="box-body table-responsive">
                        <div class="box-body">
                            <div class="row" style="margin: 5px 0px">
                                <input  type="text" class="form-control" id="ho_ten" name="ho_ten" value='{{$receiveName ?? ''}}' placeholder="Họ tên..."  autofocus>
                            </div>
                            <div class="row" style="margin: 5px 0px">
                                <input type="text" class="form-control" id="phone_number" name="phone_number" value='{{$receiveTel ?? ''}}' placeholder="Số điện thoại..." >
                            </div>
                            <div class="row" style="margin: 5px 0px">

                                <select id="province" name="province" style="width: 100%;height: 30px;border-radius: 3px;padding-left: 8px">
                                    <option value="0">-- Chọn tỉnh/thành phố --</option>
                                    @foreach($provinces as $pro)
                                        <option value="{{$pro['id']}}" {{ $pro['id'] == $tinhId ? "selected='selected'" : "" }}>{{$pro['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row" style="margin: 5px 0px">
                                <select id="district" style="width: 100%;height: 30px;border-radius: 3px;padding-left: 8px">
                                    <option value="0">-- Chọn quận/huyện --</option>
                                    @foreach($huyens as $h)
                                        <option value="{{$h['id']}}" {{ $h['id'] == $huyenId ? "selected='selected'" : "" }}>{{$h['name']}}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="row" style="margin: 5px 0px">
                                <select id="village" style="width: 100%;height: 30px;border-radius: 3px;padding-left: 8px; @if($huyenId > 0 && count($xas) == 0) display: none @endif ">
                                    <option value="0">-- Chọn phường/xã --</option>
                                    @foreach($xas as $x)
                                        <option value="{{$x['id']}}" {{ $x['id'] == $xaId ? "selected='selected'" : "" }}>{{$x['name']}}</option>
                                    @endforeach
                                </select>
                            </div>

                            <div class="row" style="margin: 5px 0px">
                                <input  type="text" class="form-control" id="dia_chi" name="dia_chi" value="{{$receiveAddress ?? ''}}" placeholder="Số nhà, tên đường, thôn xóm..." >
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Hủy</button>
                    <button id="btn-edit-info-cart" type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Chấp nhận</button>
                </div>
            </form>
        </div>
        <input type="hidden" id="productId" />
    </div>
</div>

