<div class="modal fade" id="modal-info-ship" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"> Phương thức vận chuyển </h4>
            </div>
            <div class="modal-body" style="padding: 0px 15px">
                <div class="box-body table-responsive">
                    <div class="box-body">
                        <form id="frm_InfoOrder"  class="fs-dtrtcmtbox">
                            <div class="check-type-ship"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i> Hủy</button>
                <button id="btn-edit-info-cart" onclick="updateModelInfoShip()" class="btn btn-primary"><i class="fa fa-check"></i> Cập nhật</button>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="organiId" value="0" />

