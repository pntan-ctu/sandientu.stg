@extends('layouts.web')
@section('title')
Đặt hàng
@endsection
@php
    $orderNormal = $_GET['order_normal'] ?? 0; // tồn tại khi khách đặt hàng không chọn thanh toán online
@endphp
@section('content')
<div class="wrap-page">
    <div class="register-bg">
        <div class="block-title-h1">
            <div class="container"><h1>Thanh toán</h1></div>
        </div>
        <div class="container">
            <div class="row" style="text-align: center;margin-top: 30px">
                @if($orderNormal > 0)
                    <h3 style="color: blue">BẠN ĐÃ ĐẶT HÀNG THÀNH CÔNG</h3>
                    <br>
                    <a href="{{url('/')}}"><u>Quay về trang chủ</u></a>
                @else
                    @if(isset($Payment))
                        @if(isset($Payment->response_code))
                            @if($Payment->response_code == "00")
                                <h3 style="color: blue">BẠN ĐÃ THANH TOÁN THÀNH CÔNG</h3>
                                <br>
                                <a href="{{url('/')}}"><u>Quay về trang chủ</u></a>
                            @else
                                <h3 style="color: red">THANH TOÁN KHÔNG THÀNH CÔNG. LỖI {{$Payment->response_code}}. VUI LÒNG LIÊN HỆ BAN QUẢN TRỊ.</h3>
                            @endif
                        @else
                            @if(!isset($_GET['vnptpayResponseCode']))
                                <h3 style="color: red">ĐƠN HÀNG CHƯA THANH TOÁN</h3>
                            @endif
                        @endif
                    @else
                        <h3 style="color: red">KHÔNG TÌM THẤY ĐƠN HÀNG</h3>
                    @endif
                @endif
            </div>
        </div>
    </div>  
</div>	
@endsection
@push('scripts')
<!--script src="{{asset('js/bootbox.min.js')}}" type="text/javascript"></script-->
<script src="{{asset('js/sweetalert.min.js')}}"></script>
<script>
    var pResponseCode = '<?= $_GET['vnptpayResponseCode'] ?? '' ?>';
    var pData = '<?= $_GET['data'] ?? '' ?>';
    if(pResponseCode != '') {
        if(pResponseCode == '00') {
            swal({
                title: "Thông báo",
                text: "Bạn đã thanh toán thành công!",
                icon: "success",
            }).then((value) => {
                window.location.href = '?data=' + pData
            });
            /*bootbox.alert({
                title: "Thông báo",
                message: '<i class="fa fa-check" style="color: blue;font-size: 15px;margin-right: 5px"></i>Bạn đã thanh toán thành công!',
                size: "small",
                callback: function () {
                    window.location.href = '?data=' + pData
                }
            });*/
        }
        else {
            /*bootbox.alert({
                title: "Thông báo",
                message: 'Đơn hàng chưa thanh toán.',
                size: "small",
                callback: function () {
                    window.location.href = '?data=' + pData
                }
            });*/
            window.location.href = '?data=' + pData
        }
    }
</script>
@endpush

