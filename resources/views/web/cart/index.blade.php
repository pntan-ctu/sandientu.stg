@extends('layouts.web')
@section('title')
Giỏ hàng
@endsection
@section('content')
<div class="wrap-page">
    <div class="register-bg">
        <div class="block-title-h1">
            <div class="container"><h1>Giỏ hàng</h1></div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-10 col-md-offset-1">
                    <div class="cart-page__content">
                        <div class="cart-page-product-headers">
                            <div class="cart-page-product-header">
                                <div class="cart-page-product-header__title">Sản phẩm</div>
                            </div>  
                            <div class="cart-page-product-header">Đơn giá</div>
                            <div class="cart-page-product-header">Số lượng</div>
                            <!-- <div class="cart-page-product-header">Diện tích</div> -->
                            <div class="cart-page-product-header">Số tiền</div>
                            <div class="cart-page-product-header">Thao tác</div>
                        </div>
                        <div> 
                            <div class="cart-page-shop-section">
                                <div class="cart-page-shop-section__items">
                                    @if(Cart::content() != null)
                                    @foreach($Cart as $nameOrgan => $collection) 
                                    <div class="cart-page-shop-header">
                                        <a class="cart-page-shop-header__shop-name" href="#">
                                            <span style="margin-left: 10px;">{{$nameOrgan}}</span>
                                        </a>
                                        <!--<div class="checkout-product-ordered-list-item-shop-info__chat-button"><i class="fa fa-comments" aria-hidden="true"></i>Chat ngay</div>-->
                                    </div>
                                        @foreach($collection as $product)
                                        <div class="cart-item">
                                            <div class="cart-item__head cart-item__cell-overview">
                                                <a href="{{url('san-pham/'.$product->options->path)}}" class="img-cart"><img src="{{url('/image/300/300/'.$product->options->avatar_image)}}"></a>
                                                <div class="cart-item-overview__product-name-wrapper"><a href="{{url('san-pham/'.$product->options->path)}}">{{$product->name}}</a></div>
                                            </div>
                                            <div class="cart-item__head cart-item__cell-unit-price">
                                                <span class="cart-item__unit-price cart-item__unit-price--after">{{currency($product->price, 'VND') }} </span>
                                               <!--  <span class="cart-item__unit-price cart-item__unit-price--before">
                                                    {{ $product->it5_dientichsudung }}</span>
 -->
                                            </div>
                                            <div class="cart-item__head cart-item__cell-quantity">
                                                <span class="item-quantity-prefix"></span>
                                                <!--<span class="item-quantity-value">{{$product->qty}}</span>-->
                                                <button class="reduced items-count" onclick="reduced({{$product->id}})" type="button">
                                                    <i class="icon-minus"> - </i>
                                                </button>
                                                <input type="text" name="quantity" onchange="changeText({{$product->id}})" value="{{$product->qty}}" size="2" id="qty{{$product->id}}" class="item-quantity-value qty" maxlength="12">

                                                <button class="increase items-count" onclick="increase({{$product->id}})" type="button">
                                                    <span> + </span>
                                                </button>
                                            </div>
                                            <div class="cart-item__head  cart-item__cell-total-unit{{$product->id}}">{{$product->options->unit}}</div>
                                            <div class="cart-item__head  cart-item__cell-total-price{{$product->id}}">{{currency($product->subtotal, 'VND')}}</div>
                                            <div class="cart-item__head cart-item__cell-actions">
                                                <button onclick="removeItemCart({{$product->id}})" class="cart-item__action btn-remove-cart-item">Xóa</button>
                                            </div>
                                        </div>
                                        @endforeach
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>  
                    </div>
                    <div class="cart-page-footer">
                        <div class="ct-card">
                            <div class="cart-page-footer__summary col-md-10">
                                <div class="cart-page-footer__first-summary">
                                    <div class="cart-page-footer-summary__subtotal-text">Tổng tiền hàng ({{ Cart::count() != null?Cart::count() :0}} sản phẩm):</div>
                                    <div class="cart-page-footer-summary__subtotal-amount">{{ Cart::total() != null? Cart::total(0,'.','.') :0}} ₫ </div>
                                </div>
                            </div>
                            <div class="cart-page-footer__checkout col-md-2">
                                <button @if( Cart::count() == null || Cart::count() <1)  disabled @endif class="button-solid btn-purchase">Mua hàng</button>
                            </div>
                        </div>    
                    </div>
                </div>  
            </div>  
        </div>
    </div>  
</div>	

@endsection
@push('scripts')
<script>
    $(function () {
        $('.btn-purchase').on('click', function (e) {
            var countCart= {{ Cart::count() }};
            
            if(countCart <1){
                alert('Không có sản phẩm nào trong giỏ hàng');
                return false;
            }
                
            location.href= "{{url('mua-hang')}}";
        });
    });
    function removeItemCart(id){
        var form_data = new FormData();
        form_data.append('productId', id);
        $.ajax({
        url: "{{url('remove-cart-item')}}",
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
        }).done(function (msg) {
            if (msg.message == 'success'){
                toastr.success("Đã xóa thành công !");
                location.reload();
            }
        });
    }
    function increase(id){
        var result = document.getElementById('qty'+id);
        var qty = result.value;
        if (!isNaN(qty)){
            result.value++;
        }
        editAmountCart(id, result.value)
        return false;
    }
    function reduced(id){
         var result = document.getElementById('qty'+id);
        var qty = result.value;
        if (!isNaN(qty) && qty > 1)
            result.value--;
         editAmountCart(id, result.value)
        return false;
    }
    function changeText(id){
        var result = document.getElementById('qty'+id);
        var qty = result.value;
        if (!isNaN(qty) && qty > 0)
            editAmountCart(id, result.value);
        else {
            result.value =1;
            editAmountCart(id, result.value);
        }
        return false;
    }
    function editAmountCart(id, amount){
        var form_data = new FormData();
        form_data.append('productId', id);
        form_data.append('amount', amount);
        $.ajax({
            url: "{{url('edit-amount-cart')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
        }).done(function (msg) {
            if (msg.status == 'success'){
                $('.cart-item__cell-total-price'+id).html(''+ numberFormat.format(msg.subtotal)+'');
                $('.cart-page-footer-summary__subtotal-amount').html(''+ msg.totalPrice+' ₫ ');
                $('.cart-page-footer-summary__subtotal-text').html('Tổng tiền hàng ('+ msg.totalcount+' sản phẩm):');
                $('.cart-count').html('');
                $('.cart-count').html('' + msg.totalcount + '');
            }
        });
    }
</script>
@endpush

