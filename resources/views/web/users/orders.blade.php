@extends('layouts.user')

@section('title')
    Theo dõi đơn hàng
@endsection

@push('styles')
    <style>
        #search-form {
            padding: 0px;
            text-align: left;
        }
        #keyword{
            min-width: 200px;
        }
    </style>
@endpush

@section('content')
    <style>
        .or_status{ font-size: 16px; font-style: italic; color: red}
    </style>

    <div class="user-container">
        <div class="">
            <div class="box" id="box-area">
                <div class="box-header">
                    {!! Former::openInline()->id('search-form')->addClass('pull-left col-xs-12') !!}
                    {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
                    {!! Former::select('status', 'Trạng thái')->options(['-1' => 'Tất cả', '1' => 'Chưa xử lý', '2' => ' Chờ xác nhận', '3' => ' Đã hủy', '4' => ' Hoàn tất']) !!}
                    {!! Former::primary_submit('Tìm kiếm') !!}
                    {!! Former::close() !!}
                    <div class="clearfix"></div>
                </div>
                
                <!-- /.box-header -->
                <div class="box-body-table">
                    <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                        <colgroup>
                            <col width="35%">
                            {{--<col width="20%">
                            <col width="10%">
                            <col width="10%">--}}
                            <col width="10%">
                            <col width="15%">
                            <col width="15%">
                            <col width="5%">
                        </colgroup>
                        <thead>
                        <tr role="row" class="heading">
                            <th>Nhà cung cấp</th>
                            {{--<th>Địa chỉ</th>
                            <th>Điện thoại</th>
                            <th>Tin nhắn</th>--}}
                            <th>Tổng tiền</th>
                            <th class=" text-center">Thời gian đặt</th>
                            <th>Trạng thái</th>
                            <th></th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
        </div>

    </div>
    <div id="myModal" class="modal fade" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Thông tin đơn hàng <span class="or_status">(Chưa xử lý)</span></h4>
                </div>
                <div class="modal-body">
                    <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                        <colgroup>
                            <col width="35%">
                            <col width="15%">
                            <col width="15%">
                            <col width="15%">
                        </colgroup>
                        <thead>
                        <tr role="row" class="heading">
                            <th>Tên hàng hóa</th>
                            <th>Số lượng</th>
                            <th>Đơn giá</th>
                            <th>Thành tiền</th>
                        </tr>
                        </thead>
                        <tbody>

                        </tbody>
                    </table>
                </div>
                <div class="modal-footer form-group">
                    <button type="button" class="btn btn-primary" status="0" order_id="0" id="action">Xác nhận</button>
                    <button type="button" class="btn btn-danger" status="0" id="delete">Hủy đơn</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
@endsection
@push('scripts')
<script src="js/sdc-crud.js"></script>
<script src="js/web/order.js"></script>
<!-- <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script> -->
<script>
    $(function () {
        //sdcApp.fixCKEditorModal();
        Order('{{ route('orders.index') }}');
    })
    jQuery("#action").click(function () {
        var status = jQuery(this).attr("status");
        var order_id = jQuery(this).attr("order_id");
        var url = "{{ route('orders.actOrder') }}";
        var data = {id:order_id,status:status};
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'JSON',
            success: function( msg ) {
                if(msg.id){
                    $('#datatable').DataTable().ajax.reload(null, false);
                    $('#myModal').modal('hide');
                }
            }
        });

    })
    jQuery("#delete").click(function () {
        var status = jQuery(this).attr("status");
        var order_id = jQuery(this).attr("order_id");
        var url = "{{ route('orders.huyOrder') }}";
        var data = {id:order_id,status:status};
        jQuery.ajax({
            type: "POST",
            url: url,
            data: data,
            dataType: 'JSON',
            success: function( msg ) {
                if(msg.id){
                    $('#datatable').DataTable().ajax.reload(null, false);
                    $('#myModal').modal('hide');
                }
            }
        });

    })
    function show_list(id) {
        var url = "{{ url('user/orders/showOrder/') }}"+'/'+id;
        window.location.href = url;
    }
</script>
@endpush