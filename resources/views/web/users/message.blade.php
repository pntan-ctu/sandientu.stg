@extends('layouts.'.$Layout)
@section('title')
    Tin nhắn
@endsection
@php
    $tab_id = $_GET['tab'] ?? 0;
    $midUrl = $Layout == "user" ? "user" : "business/$MsgableId";
@endphp
@push('styles')
    <style>
        .modal-body>table tr td:first-child{font-weight: bold !important;}
        .bold{font-weight: bold}
        #msgTime{float: right}
        #msgReplyContent, #msgReplySubject{width: 100%}
        #msgReplyContent{height: 80px}
        .msg-tab .nav.nav-tabs{
            margin: 20px 20px 0 20px;
            border-bottom: 1px solid #eee;
        }
        .msg-tab .nav-tabs>li{
            height: 40px;
        }
        .msg-tab .nav-tabs>li>a:focus, .msg-tab .nav-tabs>li>a:hover{
            border:none;
            background-color: #f4f4f4;
            border-radius: 0;
        }
        
        .msg-tab .nav-tabs>li.active>a, .msg-tab .nav-tabs>li.active>a:focus, .msg-tab .nav-tabs>li.active>a:hover{
            border:none;
            background-color: #f4f4f4;
            border-radius: 0;
            text-transform: uppercase;
            color: #333;
        }
        .msg-tab .nav-tabs>li>a{
            color: #333;
        }
    </style>
@endpush
@section('content')
    <div class="msg-tab">
        <ul class="nav nav-tabs" style="">
            <li @if($tab_id == 0) class="active" @endif><a href="#tab-1" data-toggle="tab" aria-expanded="true">Tin nhắn đến</a></li>
            <li @if($tab_id == 1) class="active" @endif><a href="#tab-2" data-toggle="tab" aria-expanded="false">Tin nhắn đi</a></li>
        </ul>
        <div class="wrap-content-shop-detail">
            <div class="tab-content">
                <div class="tab-pane @if($tab_id == 0) active @endif" id="tab-1">
                    <table class="table table-bordered table-striped table-hover" width="100%">
                        <tr>
                            <th>Tiêu đề</th>
                            <th width="20%">Người gửi</th>
                            <th width="20%" style="text-align: center">Ngày gửi</th>
                            <th></th>
                        </tr>
                        @foreach($MessagesReceive as $msgReceive)
                            <tr>
                                <td @empty($msgReceive['read_at']) class="bold msg{{$msgReceive['id']}}" @endempty>
                                    {{$msgReceive['subject']}}
                                </td>
                                <td @empty($msgReceive['read_at']) class="bold msg{{$msgReceive['id']}}" @endempty>
                                    {{$msgReceive['name']}}
                                </td>
                                <td @empty($msgReceive['read_at']) class="bold msg{{$msgReceive['id']}}" @endempty style="text-align: center">
                                    {{$msgReceive['ngay_gui']}}
                                </td>
                                <td align="right">
                                    <button title="Xem chi tiết" class="btn btn-link fa fa-commenting-o"
                                            onclick="view_detail(1, {{$msgReceive['id']}})"></button>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                    {!! $MessagesReceive->appends(['tab' => 0, 'page_send' => $MessagesSend->currentPage()])->render() !!}
                </div>
                <div class="tab-pane @if($tab_id == 1) active @endif" id="tab-2">
                    <table class="table table-bordered table-striped table-hover" width="100%">
                        <tr>
                            <th>Tiêu đề</th>
                            <th width="15%">Người nhận</th>
                            <th width="20%" style="text-align: center">Ngày gửi</th>
                            @if(count($MessagesSend) > 0)
                                @if($MessagesSend[0]['sendable_type'] == 'App\Models\Organization')
                                    <th width="15%">Người gửi</th>
                                @endif
                            @endif
                            <th></th>
                        </tr>
                        @foreach($MessagesSend as $msgSend)
                            <tr>
                                <td>{{$msgSend['subject']}}</td>
                                <td>{{$msgSend['name']}}</td>
                                <td align="center">{{$msgSend['ngay_gui']}}</td>
                                @if(count($MessagesSend) > 0)
                                    @if($MessagesSend[0]['sendable_type'] == 'App\Models\Organization')
                                        <td>{{$msgSend['nguoi_gui']}}</td>
                                    @endif
                                @endif
                                <td align="right"><button title="Xem chi tiết" class="btn btn-link fa fa-commenting-o"
                                                          onclick="view_detail(0, {{$msgSend['id']}})"></button>
                            </tr>
                        @endforeach
                    </table>
                    {!! $MessagesSend->appends(['tab' => 1, 'page_receive' => $MessagesReceive->currentPage()])->render() !!}
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="modal-message-detail" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" style="font-weight: bold">Chi tiết tin nhắn</h4>
                </div>
                <div class="modal-body row" style="padding: 0px 30px;">
                    <table class="table table-bordered table-striped table-hover" width="100%">
                        <tr>
                            <td id="lblSender" style="width: 100px">Người gửi</td>
                            <td><span id="msgSender"></span><span id="msgTime"></span></td>
                        </tr>
                        <tr><td>Tiêu đề</td><td id="msgSubject"></td></tr>
                        <tr><td>Nội dung</td><td id="msgContent"></td></tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal" id="btnReply">
                        <i class="fa fa-reply"></i><span id="lblNewMessage"> Trả lời</span>
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-times"></i> Đóng
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="modal-reply-message" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" style="font-weight: bold">Gửi tin nhắn</h4>
                </div>
                <div class="modal-body row" style="padding: 0px 30px;">
                    <table class="table table-bordered table-striped table-hover" width="100%">
                        <tr>
                            <td style="width: 100px">Người nhận</td>
                            <td><span id="msgReceiver"></span></td>
                        </tr>
                        <tr>
                            <td>Tiêu đề</td>
                            <td>
                                <input id="msgReplySubject" type="text" maxlength="511" />
                            </td>
                        </tr>
                        <tr>
                            <td>Nội dung</td>
                            <td>
                                <textarea id="msgReplyContent"></textarea>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnReplyMessage">
                        <i class="fa fa-sign-in"></i> Gửi
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-times"></i> Đóng
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        // type: 1 - tin gui den, 0 - tin gui di
        function view_detail(type, msg_id){
            $.ajax({
                url: 'user/getMessage/' + msg_id,
                success: function (data) {
                    $("#lblSender").text(type == 1 ? "Người gửi" : "Người nhận");
                    $("#msgSender").text(type == 1 ? data.nguoi_gui : data.nguoi_nhan);
                    $("#msgSubject").text(data.subject);
                    $("#msgContent").text(data.content);
                    $("#msgTime").text(data.ngay_gui);
                    $("#lblNewMessage").text(type == 1 ? " Trả lời" : " Gửi tin mới");
                    $("#btnReplyMessage").attr("onclick", "reply_message("+msg_id+")")
                    $("#modal-message-detail").modal("show");
                    if(type == 0) {
                        $("#btnReply").hide();
                    }
                    else{
                        $("#btnReply").show();
                    }
                    if(type == 1 && data.da_doc == 0) {
                        $.ajax({
                            url: 'user/markAsReadMessage/' + msg_id,
                            success: function () {
                                $(".msg" + msg_id).removeClass("bold");
                                var numOfMsg = parseInt($(".navbar-right .numNoty").text());
                                var numMsg = numOfMsg - 1;
                                $("li.li-tb").attr('title', 'Bạn có ' + numMsg + ' tin nhắn chưa đọc');
                                $("span.numNoty").text(numMsg);
                                if(numMsg == 0) {
                                    $("li.li-tb").hide();
                                }
                            }
                        });
                    }
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        };

        $("#btnReply").click(function() {
            $("#msgReceiver").text($("#msgSender").text());
            var subject = $("#msgSubject").text();
            if(subject.substring(0, 3) != "Re:") {
                subject = "Re: " + subject;
            }
            $("#msgReplySubject").val(subject);
            $("#msgReplyContent").val("");
            $("#modal-reply-message").modal("show");
        });

        function reply_message(msg_id) {
            var msg_subject = $("#msgReplySubject").val().trim();
            if(msg_subject.length == 0){
                toastr.error("Bạn chưa nhập tiêu đề");
                return;
            }

            var msg_content = $("#msgReplyContent").val().trim();
            if(msg_content.length == 0){
                toastr.error("Bạn chưa nhập nội dung");
                return;
            }

            if(msg_content.length > 1023){
                toastr.error("Nội dung không được vượt quá 1023 ký tự");
                return;
            }

            $.ajax({
                url: 'user/replyMessage',
                type: 'post',
                data: {
                    id: msg_id,
                    msg_subject: msg_subject,
                    msg_content: msg_content,
                    typeObjectSend: "{{$Layout}}"
                },
                success: function (result) {
                    $("#modal-reply-message").modal("hide");
                    toastr.success("Gửi tin nhắn thành công.");
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        };
    </script>
@endpush