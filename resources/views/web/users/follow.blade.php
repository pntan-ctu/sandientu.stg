@extends('layouts.user')
@section('title')
    Theo dõi cơ sở
@endsection
@section('content')
<div class="form-ads-container">
    <div class="col-main-chil">
        <div class="" style="padding:20px 20px; background: #fff"">
            <div class="posts">
                <div class="row is-flex">
                    @if(isset($follow))
                    @foreach($follow as $value)
                    @if(@isset($value->organization))
                    <div class="col-sm-4 col-md-3 col-xs-6">
                        <div class="thumb-product thumb-cs-like-user item">
                            <div class="img">
                                <a href="{{url("shop/".$value->organization->path)}}" title="">
                                    <img style="" src="{{url("image/300/300/".$value->organization->logo_image)}}" alt="" ></a>
                            </div>
                            <div class="wrap-info">
                                <h3><a href="{{url("shop/".$value->organization->path)}}">{{ $value->organization->name }}</a></h3>
                            </div>
                        </div>
                        <a href="javascript:void(0)" onclick="destroy_organization({{$value->id}})" class="destroy">Remove</a>
                    </div>
                    @endif
                    @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
        var url = "{{url('/')}}"; 
        var userId = {{getUserId()}};
        var destroy_organization = function( organizationId ){
            if (confirm("Bạn chắc chắn muốn xóa khỏi mục theo dõi ?")) {
                var form_data = new FormData();
                form_data.append('organizationId', organizationId);
                form_data.append('amount', 1);
                $.ajax({
                    url: 'user/follow/'+organizationId,
                    method: 'DELETE',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function () {
                    toastr.success("Xóa thành công");
                    window.location.href = window.location.href;
                    },
                    error: function(){
                        toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                    }
                })
            }
            if(userId<1){
                 toastr.error("Bạn chưa đăng nhập !");
                 return false;
            }
            
    }
</script>
@endpush