@extends('layouts.web')
@section('title')
    {{$User->name}}
@endsection
@php
    $tab_id = $_GET['tab'] ?? 0;
    $isBlock = 0;
    foreach ($User->blacklists as $itemBlacklist){
        if($itemBlacklist->organization_id == getOrgId()){
            $isBlock = 1;
            break;
        }
    }
@endphp
@push('styles')
    <style>
        #tableBlockUser td{border: none}
    </style>
@endpush
@section('content')
    <div class="wrap-page">
        <div class="block-title-h1">
            <div class="container"></div>
        </div>        
        <div class="wrap-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                    <li><a>Trang hoạt động của người dùng</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="col-md-1-5 hidden-sm hidden-xs">
                <div class="shop-page__info">
                    @include('web.products.productscate_left')
                </div>
            </div>
            <div class="col-md-4-5 cssxkd-wrap">
                <div class="shop-page-wrapper">
                    <div class="shop-page__info">
                        <div class="row" style="margin-left: 0px">
                            @if(isset($User->userProfile->avatar))
                                <img style="max-height: 100px; max-width: 100px; float: left" src="{{url("/image/100/100/".$User->userProfile->avatar)}}" />
                            @else
                                <img style="max-height: 100px; max-width: 100px; float: left" src="{{url("/css/images/no-image.png")}}" />
                            @endif
                            <div style="margin-left: 10px; float: left">
                                <h3>{{$User->name}}</h3>
                                <i class="glyphicon glyphicon-time"></i> Gia nhập ngày: {{$User->created_at->toShortDateString()}}
                                <div style="width: 100%;">
                                    <button class="button-report-user" onclick="showModalViphamUser()"><i class="fa fa-flag-checkered"></i>&nbsp;Phản ánh vi phạm</button>
                                    @if(isUserBusiness() && getRoleAccess() == App\RoleAccess::BUSINESS)
                                        |
                                            <button id="btnBlock" class="button-report-user" @if($isBlock == 1) style="display: none" @endif>
                                                <u style="color: red">Chặn người dùng này</u>
                                            </button>
                                            <button id="btnUnblock" class="button-report-user" @if($isBlock == 0) style="display: none" @endif>
                                                <u style="color: red">Bỏ chặn người dùng này</u>
                                            </button>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 0px; margin-top: 10px">
                            - Đã đặt <b>{{$OrderInfo[0]->total_order ?? 0}}</b> đơn hàng trong 12 tháng.
                            Hoàn thành <b>{{$OrderInfo[0]->success_order ?? 0}}</b> đơn hàng.
                        </div>

                        @if(isUserBusiness() && getRoleAccess() == App\RoleAccess::BUSINESS)
                            <div class="row" style="margin-left: 0px; margin-top: 10px">
                                - Đã đặt <b>{{$OrderInfo[0]->total_org_order ?? 0}}</b> đơn hàng tại <b>{{$OrgName}}</b>.
                                Hoàn thành <b>{{$OrderInfo[0]->success_org_order ?? 0}}</b> đơn hàng.
                            </div>
                        @endif
                    </div>
                </div>
                <div class="shop-page-menu">
                    <ul class="nav nav-tabs">
                        <li @if($tab_id == 0) class="active" @endif><a href="#tab-1" data-toggle="tab" aria-expanded="true">Đánh giá & Bình luận</a></li>
                        <li @if($tab_id == 1) class="active" @endif><a href="#tab-2" data-toggle="tab" aria-expanded="false">Hỏi đáp</a></li>
                    </ul>
                    <div class="wrap-content-shop-detail">
                        <div class="tab-content">
                            <div class="tab-pane @if($tab_id == 0) active @endif" id="tab-1">
                                @foreach($Comments as $cm)
                                <div class="row" style="border-bottom: 1px dotted #ccc; margin-bottom: 10px; margin-left: 0px">
                                    @isset($cm->object_path)
                                        <div>
                                            @if($cm->ratingable_type == "App\Models\Organization")
                                                Cơ sở:
                                            @else
                                                Sản phẩm:
                                            @endif
                                            <a href="{{$cm->object_path}}">{{$cm->object_name}}</a>
                                        </div>
                                    @endisset
                                    <div style="font-size: 12px; color: #7f7474;">{{$cm->created_at->format('h:m:s d/m/Y')}}</div>
                                    <div>
                                        @for($i = 0; $i < $cm->rate; $i++)
                                            <i class="fa fa-star"></i>
                                        @endfor
                                    </div>
                                    <i class="fa fa-comment-o"></i> {{$cm->comment}}
                                </div>
                                @endforeach
                                <div class="row">
                                {!! $Comments->appends(['tab' => 0, 'page_qa' => $QuestionAnswers->currentPage()])->render() !!}
                                </div>
                            </div>

                            <div class="tab-pane @if($tab_id == 1) active @endif" id="tab-2">
                                @foreach($QuestionAnswers as $qa)
                                    <div class="row" style="border-bottom: 1px dotted #ccc; margin-bottom: 10px; margin-left: 0px">
                                        @isset($qa->object_path)
                                            <div>
                                                <a href="{{$qa->object_path}}">{{$qa->object_name}}</a>
                                                @if($qa->question_answerable_type == "App\Models\Organization")
                                                    Cơ sở:
                                                @else
                                                    Sản phẩm:
                                                @endif
                                            </div>
                                        @endisset
                                            <div style="font-size: 12px; color: #7f7474;">{{$qa->created_at->format('h:m:s d/m/Y')}}</div>
                                        <i class="fa fa-comment-o"></i> {{$qa->content}}
                                    </div>
                                @endforeach
                                <div class="row">
                                {!! $QuestionAnswers->appends(['tab' => 1, 'page_rating' => $Comments->currentPage()])->render() !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>
    <div class="modal fade in" id="modal-block-user" style="display: none; padding-right: 17px;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" style="font-weight: bold">Chặn người dùng</h4>
                </div>
                <div class="modal-body row" style="padding: 0px 30px;">
                    <table id="tableBlockUser" class="table" width="100%">
                        <tr>
                            <td style="width: 55px">Lý do:</td>
                            <td><textarea id="reason" style="width: 100%" rows="4"></textarea></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td colspan="2">Lưu ý: Một người dùng bị chặn sẽ không thể đặt hàng tại cơ sở của bạn.</td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" id="btnSave">
                        <i class="fa fa-check"></i> Lưu
                    </button>
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-times"></i> Đóng
                    </button>
                </div>
            </div>
        </div>
    </div>
@include('web.shops.modal_vipham_organ')
@endsection
@push('scripts')
<script src="../js/bootbox.min.js"></script>
<script>
    function showModalViphamUser(){
        $('#modal-vipham').modal();
    }
    function validateVipham(){
        if( $('input[name=loaiViphamRadios]:checked').val() == null){
            alert("Bạn chưa chọn loại vi phạm ");
            return false;
        }
        if($('#txtVipham').val().trim() === ''){
            alert("Bạn chưa nhập ý kiến ");
            $('#txtVipham').focus();
            return false;
        }
        return true;
    }
    function themVipham(){
        if(!checkLogin())
            return false;
        if(!validateVipham())
            return false;
        var form_data = new FormData();
        form_data.append('typeId', {{$User->id}});
        form_data.append('comment', $('#txtVipham').val());
        var selValue = $('input[name=loaiViphamRadios]:checked').val(); 
        form_data.append('loaiVipham', selValue);
        form_data.append('typeInfrin', 'User');
        $.ajax({
            url: "{{url('them-vi-pham')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
        }).done(function (msg) {
            if (msg.message == 'success'){
                toastr.success("Phản ánh vi phạm thành công !");
                $('#txtVipham').val('');
                $('#modal-vipham').modal('hide');
            }
        });
    }

    $("#btnBlock").click(function(){
        $("#reason").val("");
        $('#modal-block-user').modal();
    });

    $("#btnSave").click(function(){
        var reason = $("#reason").val().trim();
        if(reason.length == 0){
            toastr.error("Bạn chưa nhập lý do");
            return;
        }

        if(reason.length > 255){
            toastr.error("Lý do không được quá 255 ký tự");
            return;
        }

        $.ajax({
            url: '{{url('block-user')}}',
            type: 'post',
            data: {
                user_id: '{{ $User->id }}',
                reason: reason
            },
            success: function (result) {
                $("#modal-block-user").modal("hide");
                toastr.success("Chặn người dùng thành công");
                $("#btnBlock").hide();
                $("#btnUnblock").show();
            },
            error: function(){
                toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
            }
        });

        //Bỏ confirm
        /*bootbox.confirm(
            "Bạn chắc chắn muốn chặn người dùng này?",
            function(result) {
                if (result) {
                }
            }
        );*/
    });

    $("#btnUnblock").click(function(){
        bootbox.confirm(
            "Bạn chắc chắn muốn bỏ chặn người dùng này?",
            function(result) {
                if (result) {
                    $.ajax({
                        url: '{{url('unblock-user')}}',
                        type: 'post',
                        data: {
                            user_id: '{{ $User->id }}'
                        },
                        success: function (result) {
                            toastr.success("Bỏ chặn người dùng thành công");
                            $("#btnUnblock").hide();
                            $("#btnBlock").show();
                        },
                        error: function(){
                            toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                        }
                    });
                }
            }
        );
    });
</script>
@endpush