<style>
    #frmUpdatePass label{padding-right: 0; line-height: 32px;}
</style>
<div class="user-container">
    <!--<h1 class="user-header">
        Thông tin cá nhân
    </h1>-->
    <div class="row">
        <div class="col-md-6 col-xs-12 user-info-right ">
            <form name="userInfo" id="userInfo"  enctype="multipart/form-data">
                <input name="userId" type="hidden" id="userId" value="{{ Auth::user()->id }}" />
                <div class="form-group row">
                    <label for="name" class="col-md-4 col-form-label text-md-right">Họ tên</label>

                    <div class="col-md-8">
                        <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ Auth::user()->name }}" required autofocus>

                        @if ($errors->has('name'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('Địa chỉ E-Mail') }}</label>

                    <div class="col-md-8">
                        <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ Auth::user()->email }}" required readonly>

                        @if ($errors->has('email'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row {{$errors->has('region_id')?'has-error':''}} ">
                    <label for="region_id" class="control-label col-lg-4 col-sm-4">Tỉnh/Thành phố:</label>
                    <div class="col-lg-8 col-sm-8 tree-search">
                        @include("components/select_search",['tree'=>$provincial,"level"=>0,"path"=>null,"root"=>null,
                        'parent_id'=>(isset($userProfile->region)?$userProfile->region->region->region->id:null),'name'=>'provincial',
                        'placeholder'=>'Chọn Tỉnh/Thành phố'])
                    </div>
                </div>
                <div class="form-group row {{$errors->has('region_id')?'has-error':''}} ">
                    <label for="region_id" class="control-label col-lg-4 col-sm-4">Quận/huyện:</label>
                    <div class="col-lg-8 col-sm-8 tree-search">
                        @include("components/select_search",['tree'=>$district,"level"=>0,"path"=>null,"root"=>null,
                        'parent_id'=>(isset($userProfile->region)?$userProfile->region->region->id:null),'name'=>'district',
                        'placeholder'=>'Chọn Quận/Huyện'])
                    </div>
                </div>
                <div class="form-group row {{$errors->has('region_id')?'has-error':''}} ">
                    <label for="region_id" class="control-label col-lg-4 col-sm-4">Xã/Phường:</label>
                    <div class="col-lg-8 col-sm-8 tree-search">
                        @include("components/select_search",['tree'=>$commune,"level"=>0,"path"=>null,"root"=>null,
                        'parent_id'=>(isset($userProfile->region)?$userProfile->region->id:null),'name'=>'commune',
                        'placeholder'=>'Chọn Xã/Phường'])
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-md-4 col-form-label text-md-right">SN/Thôn/Xóm</label>

                    <div class="col-md-8">
                        <input id="address" type="text" class="form-control{{ $errors->has('address') ? ' is-invalid' : '' }}" name="address" value="{{ isset($userProfile)?$userProfile->address:'' }}" >

                        @if ($errors->has('address'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('address') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label for="address" class="col-md-4 col-form-label text-md-right">Điện thoại</label>

                    <div class="col-md-8">
                        <input id="tel" type="text" class="form-control{{ $errors->has('tel') ? ' is-invalid' : '' }}" name="tel" value="{{ isset(Auth::user()->tel)?Auth::user()->tel:'' }}" >

                        @if ($errors->has('tel'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('tel') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="dayOfBirth" class="col-md-4 col-form-label text-md-right">Ngày sinh</label>

                    <div class="col-md-8">
                        <input id="dayOfBirth" type="text" class="form-control{{ $errors->has('dayOfBirth') ? ' is-invalid' : '' }}" name="dayOfBirth" value="{{ isset($userProfile->date_of_birth)? $userProfile->date_of_birth->toShortDateString():'' }}" >

                        @if ($errors->has('dayOfBirth'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('dayOfBirth') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
               
               <div class="form-group row">
                    <label for="sex" class="col-md-4 col-form-label text-md-right">Giới tính</label>

                    <div class="col-md-8">
                        <label for="sex" class="radio-inline"><input required value="1" id="sex" type="radio" name="sex" {{ (isset($userProfile) && $userProfile->sex === 1) ? 'checked' : '' }}>Nam</label>
                        <label for="sex1" class="radio-inline"><input required value="0" id="sex1" type="radio" name="sex" {{ (isset($userProfile) && $userProfile->sex === 0) ? 'checked' : '' }}>Nữ</label>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="identification" class="col-md-4 col-form-label text-md-right">Số CMND/TCCCN</label>

                    <div class="col-md-8">
                        <input id="identification" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="identification" value="{{ isset($userProfile)? $userProfile->identification : '' }}" >

                        @if ($errors->has('identification'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('identification') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group row">
                    <label for="identification_date" class="col-md-4 col-form-label text-md-right">Ngày cấp</label>

                    <div class="col-md-8">
                        <input id="identification_date" type="text" class="form-control{{ $errors->has('identification_date') ? ' is-invalid' : '' }}" name="identification_date" value="{{ isset($userProfile->identification_date)? $userProfile->identification_date->toShortDateString():'' }}" >

                        @if ($errors->has('identification_date'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('identification_date') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>
               
                <div class="form-group row">
                    <label for="identification_by" class="col-md-4 col-form-label text-md-right">Nơi cấp</label>

                    <div class="col-md-8">
                        <input id="identification_by" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="identification_by" value="{{ isset($userProfile)? $userProfile->identification_by : '' }}" >

                        @if ($errors->has('identification_by'))
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $errors->first('identification_by') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="mb-0" style="text-align: right; padding-top: 8px;">
                    <!--<div class="col-md-6 offset-md-4">-->
                    <button type="button" onclick="updateInfoUser()" class="btn btn-success">
                        {{ __('Lưu thông tin') }}
                    </button>
                    <!--</div>-->
                </div>
            </form>
        </div>
        <div class="col-md-6 col-xs-12">
            <div class="upload-img-container">
                <div class="upload-img-border">
                    <div class="upload-img-wrap">
                        <img class="media-object" src="{{Auth::user()->getAvatar(128, 128)}}" width="128" height="128">
                        <!-- @if(isset($userProfile->avatar))
                        <img class="media-object" src="{{Auth::user()->getAvatar(128, 128)}}" width="128" height="128">
                        @else
                        <img class="media-object" src="{{url('css/images/avatar_128.jpg')}}" alt="..." width="128" height="128">
                        @endif -->
                    </div>
                    @if(isset($userProfile)&& isset($userProfile->avatar) )
                    <a href="javascript:void(0)" onclick="removeImage()" class="upload-img-clear-btn">Remove</a>
                    @endif
                </div>
                <div class="upload-img-wrap-desc">
                    <label class="upload-img-desc">Ảnh đại diện</label>
                    <div class="btn btn-default btn-file">
                        <i class="fa fa-picture-o"></i> Chọn ảnh đại diện
                        <input type="file" id="avatar" name="avatar" onChange='uploadImage($(this)[0].files);'>
                    </div>
                    <p class="upload-desc">
                        <i class="fa fa-info-circle"></i>
                        <!-- react-text: 635 -->Hỗ trợ định dạng .jpg, .png, .gif, .jpeg với dung lượng không lớn hơn 2MB.<!-- /react-text -->
                    </p>
                </div>
            </div>

            <div class="frm-reset-pass" style="border-top:1px solid #eee; margin-top: 10px;">
                <form name="frmUpdatePass" id="frmUpdatePass" class="user-info-right " enctype="multipart/form-data">

                    <h3 class="text-center" style="padding-top: 15px;padding-bottom: 15px;font-weight: 600; text-transform: uppercase;font-size: 18px;">Đổi mật khẩu</h3>

                    @php
                        $user = auth()->user();
                    @endphp
                    @isset($user->password_reset)
                    <div class="text-center" style="padding-top: 0px;">
                        <label id='messPass' style='padding-top: 0px;; color: red;'>Bạn vui lòng thay đổi mật khẩu trước khi sử dụng !</label>
                    </div>
                    @endisset

                    <div class="form-group row">
                        <label for="password-current" class="col-md-5 col-form-label text-md-right">{{ __('Mật khẩu hiện tại') }}</label>
                        <div class="col-md-7">
                            <input id="password-current" type="password" class="form-control" name="password_current" required>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-5 col-form-label text-md-right">{{ __('Mật khẩu mới') }}</label>
                        <div class="col-md-7">
                            <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                            @if ($errors->has('password'))
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                            @endif
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password-confirm" class="col-md-5 col-form-label text-md-right">{{ __('Nhập lại mật khẩu mới') }}</label>
                        <div class="col-md-7">
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                        </div>
                    </div>

                    <div class="mb-0" style="text-align: right;">
                        <button type="button" onclick="updatePassword()" class="btn btn-primary">
                            {{ __('Đổi mật khẩu') }}
                        </button>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $("#provincial").change(function(){
       var idProvincial = $(this).val();
       $.get("user/profile/district/"+idProvincial,function(data){
           $("#district").html(data);
       });
    });
    
    $("#district").change(function(){
       var idDistrict = $(this).val();
       $.get("user/profile/commune/"+idDistrict,function(data){
           $("#commune").html(data);
           $("#district").val(idDistrict);
       });
    });

    $('#dayOfBirth').datepicker({
        language: 'vi',
        autoclose: true
    });
    
    $('#identification_date').datepicker({
        language: 'vi',
        autoclose: true
    });
</script>
@endpush