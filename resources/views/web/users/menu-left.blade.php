
<div class="user-menu-left">
    <!-- <div class="profiles">
        <p class="image-avartar"><img class="image-avartar" src="{{auth()->user()->getAvatar(45, 45)}}" height="45" width="45" alt=""></p>
        <p class="name">Trang cá nhân của</p>
        <h5>{{auth()->user()->name}}</h5>
    </div> -->
    <div class="my-business-wrap">
        <!-- <div class="my-business-menu-item is-active">
            <a class="my-business-menu-wrap-item" href="{{url('user/profile')}}">
                <i class="sdc sdc-home"></i>
                <span class="my-business-menu-content">Bảng tin</span>
            </a>
        </div> -->
        <div class="my-business-menu-item @if(Route::current()->getName() == 'messenger') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('messenger')}}">
                <i class="sdc fa fa-comments"></i>
                <span class="my-business-menu-content">Tin nhắn</span>
            </a>
        </div>

        <div class="my-business-menu-item @if(Route::current()->getName() == 'profile' || Route::current()->getName() == 'orders.index' || Route::current()->getName() == 'orders.showOrder') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/orders')}}">
                <i class="sdc fa fa-shopping-cart"></i>
                <span class="my-business-menu-content">Theo dõi đơn hàng</span>
            </a>
        </div>

       <!--  <div class="my-business-menu-item @if(Route::current()->getName() == 'advertising.index' || Route::current()->getName() == 'advertising.create') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/advertising')}}">
                <i class="sdc fa fa-random"></i>
                <span class="my-business-menu-content">Đăng tin cung cầu</span>
            </a>
        </div> -->

         <!-- <div class="my-business-menu-item @if(Route::current()->getName() == 'dangtuyendung.index' || Route::current()->getName() == 'dangtuyendung.taomoi') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/dangtuyendung')}}">
                <i class="sdc fa fa-vcard"></i>
                <span class="my-business-menu-content">Đăng tin tuyển dụng</span>
            </a>
        </div>

         <div class="my-business-menu-item @if(Route::current()->getName() == 'dangdulich.index' || Route::current()->getName() == 'dangdulich.taomoi') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/dangdulich')}}">
                <i class="sdc fa fa-plane"></i>
                <span class="my-business-menu-content">Đăng tin du lịch</span>
            </a>
        </div>

         <div class="my-business-menu-item @if(Route::current()->getName() == 'dangduan.index' || Route::current()->getName() == 'dangduan.taomoi') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/dangduan')}}">
                <i class="sdc fa fa-bank"></i>
                <span class="my-business-menu-content">Đăng tin dự án cần đầu tư</span>
            </a>
        </div> -->

        <!-- <div class="my-business-menu-item @if(Route::current()->getName() == 'advertising.index') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/advertising')}}">
                <i class="sdc fa fa-history"></i>
                <span class="my-business-menu-content">Lịch sử đăng tin cung cầu</span>
            </a>
        </div> -->

        <div class="my-business-menu-item @if(Route::current()->getName() == 'favorite.index') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/favorite')}}">
                <i class="sdc fa fa-heart"></i>
                <span class="my-business-menu-content">Tin đăng, Công ty yêu thích</span>
            </a>
        </div>

        <div class="my-business-menu-item @if(Route::current()->getName() == 'follow.index') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/follow')}}">
                <i class="sdc fa fa-share-alt-square"></i>
                <span class="my-business-menu-content">Theo dõi cơ sở</span>
            </a>
        </div>

        <div class="my-business-menu-item @if(Route::current()->getName() == 'user.profile') menu-checked @endif ">
            <a class="my-business-menu-wrap-item" href="{{url('user/profile')}}">
                <i class="sdc fa fa-user"></i>
                <span class="my-business-menu-content">Thông tin cá nhân</span>
            </a>
        </div>

        @if (isUserMember())
        <div class="my-business-logout">
            <a class="my-business-name" href="{{url('user/register-business')}}">
                <i class="sdc fa fa-sign-out fa-building"></i>
                <span class="my-business-menu-content">Đăng ký cơ sở của bạn</span>
            </a>
        </div>
        @endif

        @if (isUserBusiness())
        <div class="my-business-logout">
            <a class="my-business-name" href="{{route('org', ['organizationId'=>getOrgId()])}}">
                <i class="sdc fa fa-sign-out fa-building"></i>
                <span class="my-business-menu-content">Trang cơ sở của tôi</span>
            </a>
        </div>
        @endif

        @if (isUserGov())
        <div class="my-business-logout">
            <a class="my-business-name" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="sdc fa fa-sign-out"></i>
                <span class="my-business-menu-content">Đăng xuất</span>
            </a>
        </div>
        @endif
        
    </div>
</div>
