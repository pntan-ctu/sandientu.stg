@extends('layouts.user')
@section('title')
Thông tin cá nhân
@endsection
@section('content')

<div>
   
        @include('web.users.form-profiles')
   
</div>
@endsection

<script type="text/javascript">

    function uploadImage(files) {
        var form_data = new FormData();
        var valid_extensions = /(\.jpg|\.jpeg|\.gif|\.png)$/i;
        if (files.length > 0 && valid_extensions.test(files[0].name.toLowerCase())) {
            $(".upload-img-wrap").html('');
            $(".upload-img-wrap").html("<img width='128' height='128' src='" + (window.URL || window.webkitURL).createObjectURL(files[0]) + "'>");
            form_data.append('avatar', files[0]);
            form_data.append('userId', {{ Auth::user()->id }});
            $.ajax({
                url: "{{url('user/profile/change-avatar')}}",
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (msg) {
            if (msg.status == 'success')
                toastr.success("Cập nhật thành công !");
                $('.upload-img-border').append('<a href="javascript:void(0)" class="upload-img-clear-btn" onclick="removeImage()">Remove</a>');
            });
        } else {
            alert('Không đúng định dạng ảnh');
            $(".upload-img-wrap").html('');
        }
    }
    function removeImage() {
        var form_data = new FormData();
        form_data.append('avatar', '');
        form_data.append('userId', {{ Auth::user()->id }});
        $.ajax({
            url: "{{url('user/profile/change-avatar')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        }).done(function (msg) {
            if (msg.status == 'success'){
                $(".upload-img-wrap").html('<img class="media-object" src="{{url('css/images/no-image.png')}}" width="128" height="128">');
                $('.upload-img-clear-btn').hide(true);
                $('#avatar').val('');
                toastr.success("Xóa thành công !");
            }
        });
    }
    function updateInfoUser() {
        var form_data = sdcApp.getFormDataAndType($("#userInfo"));
        $.ajax({
        url: "{{url('user/profile/update')}}",
                method: 'POST',
                data: form_data.data,
                contentType: form_data.contentType,
                processData: false
        }).done(function (msg) {
            toastr[msg.status](msg.msg);
        });
    }

    function updatePassword() {
        var form_data = sdcApp.getFormDataAndType($("#frmUpdatePass"));
        $.ajax({
            url: "{{url('user/profile/change-password')}}",
            method: 'POST',
            data: form_data.data,
            contentType: form_data.contentType,
            processData: false
        }).done(function (msg) {
            toastr[msg.status](msg.msg);
            if(msg.status == "success") {
                $("#messPass").html("Đổi mật khẩu thành công");
                $("#messPass").removeAttr("style");
                $("#messPass").attr("style", "color: blue; padding-left: 15px");
                $("#password-current").val('');
                $("#password").val('');
                $("#password-confirm").val('');
            }
            else{
                $("#messPass").html(msg.msg);
            }
        }).fail(function() {
            window.location.href = window.location.href;
        });
    }
</script>