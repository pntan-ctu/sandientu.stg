@extends('layouts.user')
@section('title')
    Sản phẩm, cơ sở yêu thích
@endsection
@section('content')
<div class="form-ads-container">
    <div class="">
        <div class="" style="padding: 0 20px; background: #fff">
            <div class="modul-name1">
                SẢN PHẨM YÊU THÍCH
            </div>
            <div class="posts">
                <div class="row is-flex">
                    @foreach($products as $item)
                    @if(@isset($item->favoriteable))
                    <div class="col-sm-4 col-md-3 col-xs-6">
                        <div class="thumb-product thumb-product-like-user item">
                            <div class="img">
                                <a href="{{url("san-pham/".$item->favoriteable->path)}}" title="">
                                    <img style="" src="{{url("image/300/300/".$item->favoriteable->avatar_image)}}" alt="" >
                                </a>
                            </div>
                            <div class="wrap-info">
                                <h3><a href="{{url("san-pham/".$item->favoriteable->path)}}">{{ $item->favoriteable->name }}</a></h3>
                                <p class="price">
                                    @if(@isset($item->favoriteable->price))
                                        {{ currency($item->favoriteable->price , 'VND') }} 
                                    @endif
                                </p>

                            </div>
                        </div>
                        <a href="javascript:void(0)" onclick="destroy({{$item->id}})" class="destroy">Remove</a>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
        <div class="" style="padding: 0 20px; background: #fff">
            <div class="modul-name1">
                CƠ SỞ YÊU THÍCH
            </div>
            <div class="posts">
                <div class="row is-flex">
                    @foreach($organzations as $value)
                    @if(@isset($value->favoriteable))
                    <div class="col-sm-4 col-md-3 col-xs-6">
                        <div class="thumb-product thumb-cs-like-user item">
                            <div class="img">
                                <a href="{{url("shop/".$value->favoriteable->path)}}" title="">
                                    <img style="" src="{{url("image/300/300/".$value->favoriteable->logo_image)}}" alt="" ></a>
                            </div>
                            <div class="wrap-info">
                                <h3><a href="{{url("shop/".$value->favoriteable->path)}}">{{ $value->favoriteable->name }}</a></h3>
                            </div>
                        </div>
                        <a href="javascript:void(0)" onclick="destroy({{$value->id}})" class="destroy">Remove</a>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
        var url = "{{url('/')}}"; 
        var userId = {{getUserId()}};
        var destroy = function( productId ){
            if (confirm("Bạn chắc chắn muốn xóa khỏi mục yêu thích ?")) {
                var form_data = new FormData();
                form_data.append('productId', productId);
                form_data.append('amount', 1);
                $.ajax({
                    url: 'user/favorite/'+productId,
                    method: 'DELETE',
                    data: form_data,
                    contentType: false,
                    processData: false,
                    headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    },
                    success: function () {
                    toastr.success("Xóa thành công");
                    window.location.href = window.location.href;
                    },
                    error: function(){
                        toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                    }
                })
            }
            if(userId<1){
                 toastr.error("Bạn chưa đăng nhập !");
                 return false;
            }
            
    }
</script>
@endpush