@extends('layouts.user')

@section('content')
<div class="wrap-page-users">
    <div class="block-title-h1">
        <div class="container"><h1></h1></div>
    </div>
    <div class="container">

        <div class="box-news">
            <div class="row">
                @include('web.users.menu-left')
                @include('web.users.form-profiles')
            </div>
        </div>

    </div>
</div>

@endsection

<script type="text/javascript">

    function uploadImage(files) {
        var form_data = new FormData();
        var valid_extensions = /(\.jpg|\.jpeg|\.gif|\.png)$/i;
        if (files.length > 0 && valid_extensions.test(files[0].name.toLowerCase())) {
            $(".upload-img-wrap").html('');
            $(".upload-img-wrap").html("<img width='128' height='128' src='" + (window.URL || window.webkitURL).createObjectURL(files[0]) + "'>");
            form_data.append('avatar', files[0]);
            form_data.append('userId', {{ Auth::user()->id }});
            $.ajax({
                url: "{{url('profile/edit')}}",
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (msg) {
                if(msg.message == 'success')
                    toastr.success("Cập nhật thành công !");
               $('.upload-img-border').append('<a href="javascript:void(0)" class="upload-img-clear-btn" onclick="removeImage()">Remove</a>');
               
            });
        } else {
            alert('Không đúng định dạng ảnh');
            $(".upload-img-wrap").html('');

        }
    }
     function removeImage() {
            var form_data = new FormData();
            form_data.append('avatar', '');
            form_data.append('userId', {{ Auth::user()->id }});
            $.ajax({
                url: "{{url('profile/edit')}}",
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            }).done(function (msg) {
                if(msg.message == 'success'){
                    $(".upload-img-wrap").html('<img class="media-object" src="{{url('css/images/avatar_128.jpg')}}" alt="..." width="128" height="128">');
                    $('.upload-img-clear-btn').hide(true);
                    $('#avatar').val('');
                    toastr.success("Xóa thành công !");
                }
            });
      
    }
     function updateInfoUser() {
        var form_data = sdcApp.getFormDataAndType($("#userInfo"));
        $.ajax({
            url: "{{url('profile/updateUserProfile')}}",
            method: 'POST',
            data: form_data.data,
            contentType: form_data.contentType,
            processData: false
        }).done(function (msg) {
            if(msg.message == 'success')
                toastr.success("Cập nhật thành công !");
        });
    }
</script>