@extends('layouts.user')

@section('title')
Đăng ký Công Ty kinh doanh
@endsection

@push('styles')
    <style>
        td {
            vertical-align: middle !important;
            text-align: center !important;
        }

        .title-step {
            font-weight: bold;
            font-size: 20px;
            color: #d91c5f;
        }

        .fa-icon {
            width: 14px;
        }

        .right-container {
            flex: 1;
            padding-left: 10px;
        }

        .left-container {
            text-align: center;
        }

        .form-container {
            width: 100%;
        }

        .btn-file {
            margin-top: 5px;
            width: 100%;
        }

        .image_avatar>img {
            border: solid 1px gainsboro;
            padding: 4px;
        }

        .user-container {
            width: 100%;
        }

        #area_gr {
            display: none
        }

        #commer_gr {
            display: none
        }
    </style>
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
@endpush
@section('content')
    <div class="user-container">
        <div class="toolbarUser">
            <div class="box" id="box-product">
                <!-- <div class="box-header1">
                    <div class="title-step rm-padding" id="title">
                        {{isset($organization)?"Sửa thông tin cơ sở SXKD":"Đăng ký cơ sở SX, KD"}}
                    </div>
                    <div class="clearfix">
                </div> -->
                <div id="primary_info_container">
                    <!-- /.box-header -->
                    <div class="box-body">
                        <?php Former::populate($organization) ?>
                        {!! Former::horizontal_open_for_files(route('user.register-business'))->novalidate() !!}
                        <div class="form-container">
                            {{--upload anh--}}
                            <div class="col-md-3 col-sm-2 col-xs-12 {{$errors->has('logo_image')?'has-error':''}} ">
                                <div class="image_avatar">
                                    <img width="200" height="200"
                                         src={{ isset($organization->logo_image)?url('image/200/200/'.$organization->logo_image):url('img/no-image.png') }} >
                                </div>
                                <div class="btn bg-maroon btn-file">
                                    <i class="fa fa-picture-o"></i> Ảnh Logo Công Ty
                                    <input type="file" class="form-control" id="image_upload_input" name="logo_image"/>
                                </div>
                                <span class="help-block">{{$errors->first('logo_image')}}</span>
                            </div>

                            <div class="col-md-9 col-sm-10 col-xs-12">
                                {!! Former::setOption('TwitterBootstrap3.labelWidths', ['large' => 3, 'small' => 4]) !!}
                                <div class="form-group required {{$errors->has('manage_organization_id')?'has-error':''}} ">
                                    <label for="manage_organization_id" class="control-label col-lg-3 col-sm-4">Cơ quan
                                        Quản Lý <sup>*</sup></label>
                                    <div class="col-lg-9 col-sm-8 tree-search">
                                        
                                        @if($orgTypeTree == null) <script> alert('Có lỗi xảy ra trong quá trình load dữ liệu!');</script> @endif
                                        
                                        @include("components/select_search",['tree'=>$orgTypeTree,"level"=>0,"path"=>null,"root"=>null,
                                        'parent_id'=>null,'name'=>'manage_organization_id',
                                        'placeholder'=>'--- Hãy chọn cơ quan quản lý xét duyệt ---'])
                                    </div>
                                </div>

                                {!! Former::text('name','Tên Công Ty ')->required()!!}

                                {{--<div class="form-group required {{$errors->has('organization_type_id')?'has-error':''}} ">--}}
                                    {{--<label for="organization_type_id" class="control-label col-lg-3 col-sm-4" id="text">Loại--}}
                                        {{--doanh nghiệp <sup>*</sup></label>--}}
                                    {{--<div class="col-lg-9 col-sm-8 tree-search">--}}
                                        {{--<select class="form-control" id="organization_type" name="organization_type_id">--}}
                                            {{--<option value="">--- Chọn loại doanh nghiệp ---</option>--}}
                                            {{--@foreach($orgTypeBusiness as $key=>$val)--}}
                                                {{--<option value="{{ $val->id }}">{{ $val->name }}</option>--}}
                                            {{--@endforeach--}}
                                        {{--</select>--}}
                                    {{--</div>--}}
                                {{--</div>--}}

                                {!! Former::select("organization_type_id","Loại doanh nghiệp ")->addOption('--- Chọn loại doanh nghiệp ---', '')->fromQuery($orgTypeBusiness,'name','id')->required() !!}

                                <div id="area_gr" class="form-group required">
                                    <label for="area_id" class="control-label col-lg-3 col-sm-4" id="text">Vùng sản
                                        xuất </label>
                                    <div class="col-lg-9 col-sm-8 tree-search">
                                        <select class="form-control" id="area" name="area_id">
                                            <option value="">-- Chọn vùng sản xuất --</option>
                                            @foreach($areaAll as $k1=>$v1)
                                                <option value="{{ $v1->id }}">{{ $v1->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div id="commer_gr" class="form-group required">
                                    <label for="commercial_center_id" class="control-label col-lg-3 col-sm-4" id="text">Địa
                                        điểm kinh doanh </label>
                                    <div class="col-lg-9 col-sm-8 tree-search">
                                        <select class="form-control" id="commer" name="commercial_center_id">
                                            <option value="">-- Chọn Địa điểm kinh doanh --</option>
                                            @foreach($commerAll as $k2=>$v2)
                                                <option value="{{ $v2->id }}">{{ $v2->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>

                                <div class="form-group required {{$errors->has('region_id')?'has-error':''}} ">
                                    <label for="region_id" class="control-label col-lg-3 col-sm-4">Tỉnh, huyện, xã
                                        <sup>*</sup></label>
                                    <div class="col-lg-9 col-sm-8 tree-search">
                                        @include("components/select_search",['tree'=>$regions,"level"=>0,"path"=>null,"root"=>null,
                                        'parent_id'=>(isset($organization)?$organization->region_id:null),'name'=>'region_id',
                                        'placeholder'=>'--- Hãy chọn địa phương quản lý ---'])
                                    </div>
                                </div>
                                
                                {!! Former::text('address','SN/thôn/xóm ')->prepend('<i class="fa fa-home fa-icon"></i>')->required()  !!}
                                {!! Former::text('tel','Điện thoại ')->addClass('tel')->prepend('<i class="fa fa-phone fa-icon"></i>')->required() !!}
                                {!! Former::radios('Ngành quản lý')->radios($departments)->inline()->required()!!}
                                {!! Former::select("founding_type","Loại giấy tờ")->addOption('--- Chọn loại giấy tờ ---', '')->fromQuery($founding_type,'name','id')->required() !!}
                                <!-- <input type="text" id="founding_type" name="founding_type" value="2" style="display:none;"> -->
                                {!! Former::text('founding_number','Số giấy phép kinh doanh ')->addClass('founding_number')->required() !!}
                                {!! Former::text('founding_by_gov','Cơ quan cấp phép ')->addClass('founding_by_gov')->required() !!}
                                {!! Former::text('founding_date','Ngày cấp')->addClass('founding_date form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->required()
                                 ->forceValue(($organization && $organization->founding_date) ? $organization->founding_date->toShortDateString() : null)!!}
                                {!! Former::text('director','Người đại diện ')->addClass('director')->prepend('<i class="fa fa-user fa-icon"></i>')->required() !!}
                                {!! Former::select("organization_level_id","Quy mô ")->addClass('organization_level_id')->addOption('--- Chọn quy mô ---', '')->fromQuery($organizationLevel,'name','id')->required()!!}
                                {!! Former::text('email','Email ')->addClass('email')->prepend('<i class="fa fa-envelope-o fa-icon"></i>') !!}
                                {!! Former::text('website','Website ')->addClass('website')->prepend('<i class="fa fa-globe fa-icon"></i>') !!}
                                {!! Former::text('fanpage','FanPage ')->addClass('fanpage')->prepend('<i class="fa fa-facebook fa-icon"></i>') !!}
                                {!! Former::text('map','Bản đồ ')->addClass('map')->prepend('<i class="fa  fa-map-marker fa-icon"></i>')->placeholder("19.808241, 105.777592") !!}
                                <div class="form-group">
                                    <div class="col-sm-offset-3 col-sm-9">
                                        <div class="checkbox"><label> <input id="accept" name="accept" type="checkbox"> Tôi đồng ý với điều khoản sử dụng và chính sách của Phần mềm </label></div>
                                    </div>
                                </div>
                                <div class="right-button">
                                    <button disabled type="submit" id="save" class="btn Normal btn-primary">
                                        <i class="fa  fa-save"></i>
                                        Lưu thông tin
                                    </button>
                                </div>
                            </div>
                        </div>
                        {!! Former::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script src="js/manage/organization.js"></script>
    <script>
        $('#organization_type_id').on("change", function () {
            var organization_type_id = parseInt(jQuery(this).val());
            var url = "{{ route('user.get-org-type') }}";
            var data = {id: organization_type_id};
            jQuery.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: 'JSON',
                success: function (data) {
                    jQuery("#area_gr").hide();
                    jQuery("#commer_gr").hide();
                    if (data.status == 1) {
                        jQuery("#area_gr").fadeIn();
                    } else if (data.status == 2) {
                        jQuery("#commer_gr").fadeIn();
                    }
                }
            });
        });

        $("#accept").on('change',function () {
            if($("#accept").prop('checked')){
                $("#save").prop('disabled', false);
            }else{
                $("#save").prop('disabled', true);
            }
        })
        $(function () {
            var organization = Organization('{{ route('product.create', ['organizationTypeId' => $orgTypeModel->id]) }}');
            organization.init_create("{{$orgTypeModel->id}}");
        })
    </script>

@endpush