@extends('layouts.web')
@section('title')
Lấy tọa độ vị trí hiện tại
@endsection
@section('content')
<div class="block-videos">
    <div class="wrap-page">
         <div class="wrap-breadcrumb">
                <div class="container">
                    <ul class="breadcrumb">
                        <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                        <li><a href="{{url('/toa-do')}}">Lấy tọa độ vị trí hiện tại</a></li>
                    </ul>
                </div>
            </div>
        <div class="container">
            <div class="buy-center" >
                <div class="row">
                    <div class="col-md-12">
                        <div class="blocks-contact-title" style="margin-top:20px; text-align: center">
                            <h3>Lấy tọa độ vị trí hiện tại</h3>
                        </div>
                        <form role="form" method="get" action="#" class="row formcontact" style="margin-top:20px; margin-bottom:100px;">
                            <div class="col-xs-3 ">
                            </div>
                            <div class="col-xs-5 ">
                                <input type="text" class="form-control" id="cunrent_location" name="cunrent_location" placeholder="Tọa độ vị trí hiện tại" readonly style="text-align: center">
                            </div>
                            <div class="col-xs-1 ">
                                <!-- <button type="submit" id="submit" class="btn btn-success" onclick="getLocation()">Covert</button> -->
                                <a onclick="getLocation()" class="btn btn-success" style="cursor: pointer;">Cập nhật</a>
                            </div>
                            <div class="col-xs-3 "">
                            </div>
                        </form> 
                    </div>
                </div>
            </div>
        </div>  
    </div>
</div>
@endsection

@push('scripts')

<script>
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showLocation);
        } else { 
            x.innerHTML = "Định vị GPS không hỗ trợ trình duyệt này !";
        }
    }

    function showLocation(position) {
        $('#cunrent_location').val(position.coords.latitude.toFixed(6) + ', ' + position.coords.longitude.toFixed(6));
        //$('#map_long').val(position.coords.longitude.toFixed(6));
    }

    $(function () {        
        getLocation();
    })
</script>

@endpush

