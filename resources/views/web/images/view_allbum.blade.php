@extends('layouts.web')
@section('title')
Thư viện Video
@endsection
@section('content')

<link href="{{ asset('css/jgallery.min.css?v=1.6.0') }}" rel="stylesheet">

<div class="block-allbums">
    <div class="wrap-page">
         <div class="wrap-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                    <li><a href="{{url('/thu-vien-anh')}}">Thư viện ảnh</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="col-md-12 block-videos-content">
                <h2 class="title-detail">{{$allbum->title}}</h2>
                    <p class="time-news">
                    </p>
                    <hr>
                <div id="gallery" >
                    @if(isset($images))
                        @foreach($images as $key => $value)
                        <a href="{{url("/image/600/600/".$value->img_path)}}">
                            <img src="{{url("/image/100/100/".$value->img_path)}}" />
                        </a>
                        @endforeach

                    @endif
                </div>
            </div>
            
        </div>	
    </div>
</div>
@endsection
 
@push('scripts')
<script type="text/javascript" src="{{asset('js/jgallery/jgallery.min.js?v=1.6.0')}}"></script>
<script type="text/javascript" src="{{asset('js/jgallery/touchswipe.min.js')}}"></script>
<script type="text/javascript" src="{{asset('js/jgallery/tinycolor-0.9.16.min.js')}}"></script>
<script>
$( function() {
    $('#gallery').jGallery(
        {
         backgroundColor: 'black',
         textColor: 'white',
         browserHistory: false,
        }
    );
} );
</script>
@endpush