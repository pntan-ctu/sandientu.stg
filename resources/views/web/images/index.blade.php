@extends('layouts.web')
@section('title')
Thư viện Video
@endsection
@section('content')
<div class="block-videos">
    <div class="wrap-page">
        <div class="wrap-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                    <li><a href="{{url('/thu-vien-anh')}}">Thư viện ảnh</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="block-videos-title">
                <h1>Danh sách Album ảnh</h1>
            </div>
            <div class="">
                <div class="block-videos-content row is-flex">
                    @if(isset($allbums))
                    @foreach($allbums as $key => $value)
                    
                        <div class="item-video-list col-md-3 col-sm-4 col-xs-6">
                            <div class="logo-allbum">
                                <a href="{{url('thu-vien-anh/'.$value->path)}}">
                                    <img src="{{url("/image/300/300/".$value->img_path)}}" />   
                                </a>
                            </div>
                            <div class="title-allbum-right">
                                <h3> <a href="{{url('thu-vien-anh/'.$value->path)}}"> {{$value->title}} </a></h3> 
                            </div> 
                        </div>

                    @endforeach
                    @endif
                </div>
            </div> 
        </div>	
    </div>
</div>
@endsection
