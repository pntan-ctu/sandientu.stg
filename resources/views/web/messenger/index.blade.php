@extends('layouts.user')
@section('title')
    Tin nhắn
@endsection

@push('styles')
    <style>
        #frame {
            min-width: 360px;
            max-width: 1000px;
            height: 92vh;
            min-height: 300px;
            max-height: 720px;
            background: #fff;
            position: relative;
        }

        @media screen and (max-width: 360px) {
            #frame {
                width: 100%;
                height: 100vh;
            }
        }

        #frame #sidepanel {
            float: left;
            min-width: 200px;
            max-width: 300px;
            width: 40%;
            height: 100%;
            background: #f0f4f8;
            color: #f5f5f5;
            overflow: hidden;
            position: relative;
            border-right: 1px solid #d8e5ef;
            border-left: 1px solid #d8e5ef;
            border-bottom: 1px solid #d8e5ef;
        }

        @media screen and (max-width: 735px) {
            #frame #sidepanel {
                width: 60px;
                min-width: 60px;
            }
            #frame #sidepanel #contacts ul li.contact .wrap img {
                margin:12px 6px 6px 6px;
            }
            #frame #sidepanel #contacts ul li.contact .wrap {
                width: 100%;
            }
            #frame #sidepanel #contacts ul li.contact {
                padding: 6px 0 46px 8px;
                color: #333;
                font-size: 13px;
                float: left;
            }
            #frame #sidepanel #contacts ul li.contact:active {
                float:left;
                background-color: #c7edfc;
            }
            #frame #sidepanel #contacts {
                height: calc(100% - 149px);
                overflow-x: hidden;
            }

            #frame #sidepanel #contacts::-webkit-scrollbar {
                display: none;
            }

            #frame #sidepanel #search {
                display: none;
            }
            #frame #sidepanel #contacts ul li.contact .wrap .meta {
                display: none;
            }
            #frame #sidepanel #profile {
                width: 100%;
                margin: 0 auto;
                padding: 5px 0 0 0;
                background: #f0f4f8;
            }
            #frame #sidepanel #profile .wrap {
                height: 55px;
            }
            #frame #sidepanel #profile .wrap #status-options {
                width: 60px;
                margin-top: 57px;
            }
            #frame #sidepanel #profile .wrap #status-options.active {
                margin-top: 62px;
            }
            #frame #sidepanel #profile .wrap p {
                display: none;
            }
            #frame #sidepanel #profile .wrap i.expand-button {
                display: none;
            }
            #frame #sidepanel #profile .wrap #status-options:before {
                margin-left: 23px;
            }
            #frame #sidepanel #profile .wrap #status-options ul li {
                padding: 15px 0 35px 22px;
            }
            #frame #sidepanel #profile .wrap #status-options ul li span.status-circle:before {
                height: 18px;
                width: 18px;
            }
            #frame #sidepanel #profile .wrap #status-options ul li span.status-circle {
                width: 14px;
                height: 14px;
            }
            #frame #sidepanel #profile .wrap #status-options ul li p {
                display: none;
            }
            #frame #sidepanel #bottom-bar button:nth-child(1) {
                border-right: none;
                border-bottom: 1px solid #f0f4f8;
            }
            #frame #sidepanel #bottom-bar button {
                float: none;
                width: 100%;
                padding: 15px 0;
            }
            #frame .content .message-input .wrap button {padding: 16px 0}
            #frame .content .message-input .wrap button .fa{margin-right: 16px;}
            #frame .content .message-input .wrap .attachment {
                margin-top: 17px;
                right: 65px;
            }
            #frame .content .message-input .wrap input {
                padding: 15px 32px 16px 8px;
            }
            #frame .content .messages ul li p {
                max-width: 300px;
            }
            #frame .content .messages {
                max-height: calc(100% - 105px);
            }
            #frame .content {
                width: calc(100% - 60px)!important;
                min-width: 300px !important;
            }
            #frame #sidepanel #bottom-bar button i {
                font-size: 1.3em;
            }
            #frame #sidepanel #bottom-bar button span {
                display: none;
            }
            #frame #sidepanel #profile .wrap img {
                width: 40px;
            }
        }

        #frame #sidepanel #profile {
            width: 90%;
            margin: 10px auto 0px;
        }

        #frame #sidepanel #profile.expanded .wrap {
            height: 210px;
            line-height: initial;
        }

        #frame #sidepanel #profile.expanded .wrap p {
            margin-top: 20px;
        }

        #frame #sidepanel #profile.expanded .wrap i.expand-button {
            -moz-transform: scaleY(-1);
            -o-transform: scaleY(-1);
            -webkit-transform: scaleY(-1);
            transform: scaleY(-1);
            filter: FlipH;
            -ms-filter: "FlipH";
        }

        #frame #sidepanel #profile .wrap {
            height: 60px;
            line-height: 60px;
            overflow: hidden;
            -moz-transition: 0.3s height ease;
            -o-transition: 0.3s height ease;
            -webkit-transition: 0.3s height ease;
            transition: 0.3s height ease;
        }

        #frame #sidepanel #profile .wrap img {
            width: 50px;
            border-radius: 50%;
            padding: 3px;
            border: 2px solid #e74c3c;
            height: auto;
            float: left;
            cursor: pointer;
            -moz-transition: 0.3s border ease;
            -o-transition: 0.3s border ease;
            -webkit-transition: 0.3s border ease;
            transition: 0.3s border ease;
        }

        #frame #sidepanel #profile .wrap img.online {
            border: 2px solid #2ecc71;
            height: 50px;
        }

        #frame #sidepanel #profile .wrap img.away {
            border: 2px solid #f1c40f;
        }

        #frame #sidepanel #profile .wrap img.busy {
            border: 2px solid #e74c3c;
        }

        #frame #sidepanel #profile .wrap img.offline {
            border: 2px solid #95a5a6;
        }

        #frame #sidepanel #profile .wrap p {
            float: left;
            margin-left: 15px;
            margin-top: -3px;
            color: #333;
        }

        #frame #sidepanel #profile .wrap i.expand-button {
            float: right;
            margin-top: 23px;
            font-size: 0.8em;
            cursor: pointer;
            color: #435f7a;
        }

        #frame #sidepanel #profile .wrap #status-options {
            position: absolute;
            opacity: 0;
            visibility: hidden;
            width: 150px;
            margin: 70px 0 0 0;
            border-radius: 6px;
            z-index: 99;
            line-height: initial;
            background: #fff;
            -moz-transition: 0.3s all ease;
            -o-transition: 0.3s all ease;
            -webkit-transition: 0.3s all ease;
            transition: 0.3s all ease;
        }

        #frame #sidepanel #profile .wrap #status-options.active {
            opacity: 1;
            visibility: visible;
            margin: 75px 0 0 0;
        }

        #frame #sidepanel #profile .wrap #status-options:before {
            content: '';
            position: absolute;
            width: 0;
            height: 0;
            border-left: 6px solid transparent;
            border-right: 6px solid transparent;
            border-bottom: 8px solid #435f7a;
            margin: -8px 0 0 24px;
        }

        #frame #sidepanel #profile .wrap #status-options ul {
            overflow: hidden;
            border-radius: 6px;
        }

        #frame #sidepanel #profile .wrap #status-options ul li {
            padding: 15px 0 30px 18px;
            display: block;
            cursor: pointer;
        }

        #frame #sidepanel #profile .wrap #status-options ul li:hover {
            background: #496886;
        }

        #frame #sidepanel #profile .wrap #status-options ul li span.status-circle {
            position: absolute;
            width: 10px;
            height: 10px;
            border-radius: 50%;
            margin: 5px 0 0 0;
        }

        #frame #sidepanel #profile .wrap #status-options ul li span.status-circle:before {
            content: '';
            position: absolute;
            width: 14px;
            height: 14px;
            margin: -3px 0 0 -3px;
            background: transparent;
            border-radius: 50%;
            z-index: 0;
        }

        #frame #sidepanel #profile .wrap #status-options ul li p {
            padding-left: 12px;
        }

        #frame #sidepanel #profile .wrap #status-options ul li#status-online span.status-circle {
            background: #2ecc71;
        }

        #frame #sidepanel #profile .wrap #status-options ul li#status-online.active span.status-circle:before {
            border: 1px solid #2ecc71;
        }

        #frame #sidepanel #profile .wrap #status-options ul li#status-away span.status-circle {
            background: #f1c40f;
        }

        #frame #sidepanel #profile .wrap #status-options ul li#status-away.active span.status-circle:before {
            border: 1px solid #f1c40f;
        }

        #frame #sidepanel #profile .wrap #status-options ul li#status-busy span.status-circle {
            background: #e74c3c;
        }

        #frame #sidepanel #profile .wrap #status-options ul li#status-busy.active span.status-circle:before {
            border: 1px solid #e74c3c;
        }

        #frame #sidepanel #profile .wrap #status-options ul li#status-offline span.status-circle {
            background: #95a5a6;
        }

        #frame #sidepanel #profile .wrap #status-options ul li#status-offline.active span.status-circle:before {
            border: 1px solid #95a5a6;
        }

        #frame #sidepanel #profile .wrap #expanded {
            padding: 100px 0 0 0;
            display: block;
            line-height: initial !important;
        }

        #frame #sidepanel #profile .wrap #expanded label {
            float: left;
            clear: both;
            margin: 0 8px 5px 0;
            padding: 5px 0;
        }

        #frame #sidepanel #profile .wrap #expanded input {
            border: none;
            margin-bottom: 6px;
            background: #f0f4f8;
            border-radius: 3px;
            color: #f5f5f5;
            padding: 7px;
            width: calc(100% - 43px);
        }

        #frame #sidepanel #profile .wrap #expanded input:focus {
            outline: none;
            background: #fff;
        }

        #frame #sidepanel #search {
            border-top: 1px solid #f0f4f8;
            border-bottom: 1px solid #f0f4f8;
            font-weight: 300;
            color: #333;
            border-bottom: 1px solid #d8e5ef;
        }
        #frame #sidepanel #search .fa-search:before{
            color: #888;
        }

        #frame #sidepanel #search label {
            position: absolute;
            margin: 10px 0 0 20px;
        }

        #frame #sidepanel #search input {
            font-family: "proxima-nova", "Source Sans Pro", sans-serif;
            padding: 10px 0 10px 46px;
            width: 100%;
            border: none;
            background: #f0f4f8;
            color: #f5f5f5;
        }

        #frame #sidepanel #search input:focus {
            outline: none;
            background: none;
            color: #333
        }

        #frame #sidepanel #search input::-webkit-input-placeholder {
            color: #f5f5f5;
        }

        #frame #sidepanel #search input::-moz-placeholder {
            color: #f5f5f5;
        }

        #frame #sidepanel #search input:-ms-input-placeholder {
            color: #f5f5f5;
        }

        #frame #sidepanel #search input:-moz-placeholder {
            color: #f5f5f5;
        }

        #frame #sidepanel #contacts {
            height: calc(100% - 153px);
            overflow-x: hidden;
        }

        #frame #sidepanel #contacts.expanded {
            height: calc(100% - 334px);
        }

        #frame #sidepanel #contacts::-webkit-scrollbar {
            width: 8px;
            background: #f0f4f8;
        }

        #frame #sidepanel #contacts::-webkit-scrollbar-thumb {
            background-color: #b8cbdb;
        }

        #frame #sidepanel #contacts ul li.contact {
            position: relative;
            padding: 10px 0 10px 0;
            font-size: 13px;
            cursor: pointer;
            color: #333;
        }

        #frame #sidepanel #contacts ul li.contact:hover {
            background: #f0f4f8;
        }

        #frame #sidepanel #contacts ul li.contact.active {
            background: #c7edfc;
            color: #333;
        }

        #frame #sidepanel #contacts ul li.contact.active span.contact-status {
            border: 2px solid #f0f4f8 !important;
        }

        #frame #sidepanel #contacts ul li.contact .wrap {
            width: 88%;
            margin: 0 auto;
            position: relative;
        }

        #frame #sidepanel #contacts ul li.contact .wrap span {
            position: absolute;
            left: 0;
            margin: -9px 0 0 -2px;
            width: 30px;
            height: 20px;
            border-radius: 50%;
            border: 2px solid #2ec670;
            background: white;
        }

        #frame #sidepanel #contacts ul li.contact .wrap span.online {
            background: #2ecc71;
        }

        #frame #sidepanel #contacts ul li.contact .wrap span.away {
            background: #f1c40f;
        }

        #frame #sidepanel #contacts ul li.contact .wrap span.busy {
            background: #e74c3c;
        }

        #frame #sidepanel #contacts ul li.contact .wrap img {
            width: 40px; border-radius: 50%; float: left; margin-right: 10px; min-height: 40px; margin-top: 2px;height: 40px;
        }

        #frame #sidepanel #contacts ul li.contact .wrap .meta {
            padding: 5px 0 0 0;
        }

        #frame #sidepanel #contacts ul li.contact.unread .wrap .meta .name {
            font-weight: 600;
        }

        #frame #sidepanel #contacts ul li.contact.unread .wrap .meta .preview {
            font-weight: 600;
            opacity: 1;
        }

        #frame #sidepanel #contacts ul li.contact .wrap .meta .preview {
            margin: 5px 0 0 0;
            padding: 0 0 1px;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
            -moz-transition: 1s all ease;
            -o-transition: 1s all ease;
            -webkit-transition: 1s all ease;
            transition: 1s all ease;
            opacity: .8;
            font-size: 14px;
            display: none;
        }

        #frame #sidepanel #contacts ul li.contact .wrap .meta .preview span {
            position: initial;
            border-radius: initial;
            background: none;
            border: none;
            padding: 0 2px 0 0;
            margin: 0 0 0 1px;
            opacity: .5;
        }

        #frame #sidepanel #bottom-bar {
            position: absolute;
            width: 100%;
            bottom: 0;
        }

        #frame #sidepanel #bottom-bar button {
            float: left;
            border: none;
            width: 50%;
            padding: 10px 0;
            background: #f0f4f8;
            color: #f5f5f5;
            cursor: pointer;
            font-size: 0.85em;
            font-family: "proxima-nova", "Source Sans Pro", sans-serif;
        }

        #frame #sidepanel #bottom-bar button:focus {
            outline: none;
        }

        #frame #sidepanel #bottom-bar button:nth-child(1) {
            border-right: 1px solid #f0f4f8;
        }

        #frame #sidepanel #bottom-bar button:hover {
            background: #fff;
            color: #333;
        }

        #frame #sidepanel #bottom-bar button i {
            margin-right: 3px;
            font-size: 1em;
        }

        #frame .content {
            float: right;
            width: 60%;
            height: 100%;
            overflow: hidden;
            position: relative;
        }

        @media screen and (min-width: 900px) {
            #frame .content {
                width: calc(100% - 300px);
            }
        }

        #frame .content .contact-profile {
            width: 100%;
            height: 55px;
            line-height: 55px;
            background: #fff;
            border-bottom: 1px solid #d8e5ef
        }

        #frame .content .contact-profile img {
            width: 42px;
            border-radius: 50%;
            float: left;
            margin: 9px 12px 0 15px;
            min-height: 42px;
            display: none;
        }

        #frame .content .contact-profile p {
            float: left;
            font-size: 20px;
            font-weight: 300;
            padding-left: 15px;
        }

        #frame .content .contact-profile .social-media {
            float: right;
        }

        #frame .content .contact-profile .social-media i {
            margin-left: 14px;
            cursor: pointer;
        }

        #frame .content .contact-profile .social-media i:nth-last-child(1) {
            margin-right: 20px;
        }

        #frame .content .contact-profile .social-media i:hover {
            background: #fff;
            color: #333;
        }

        #frame .content .messages {
            height: auto;
            min-height: calc(100% - 115px);
            max-height: calc(100% - 115px);
            overflow-y: scroll;
            overflow-x: hidden;
            width: 100%;
        }

        #frame .content .messages::-webkit-scrollbar {
            width: 8px;
            background: #fff;
        }

        #frame .content .messages::-webkit-scrollbar-thumb {
            background-color: #b8cbdb;
        }

        #frame .content .messages ul li {
            display: inline-block;
            clear: both;
            float: left;
            margin: 10px 15px 10px 15px;
            width: calc(100% - 25px);
            font-size: 0.9em;
        }

        #frame .content .messages ul li:nth-last-child(1) {
            margin-bottom: 20px;
        }

        #frame .content .messages ul li.sent p {
            background: #c7edfc;
            color: #333;
        }

        #frame .content .messages ul li.sent img {
            margin: -2px 8px 0px 0px;
            height: 42px;
        }

        #frame .content .messages ul li.replies img {
            float: right;
            margin: -5px 8px;
            height: 42px;
        }

        #frame .content .messages ul li.replies p {
            background: #f0f4f8;
            float: right;
        }

        #frame .content .messages ul li.replies div.timestamp {
            clear: both;
            float: right;
        }

        #frame .content .messages ul li img {
            width: 42px;
            border-radius: 50%;
            float: left;
        }

        #frame .content .messages ul li p {
            display: inline-block;
            padding: 10px 15px;
            border-radius: 20px;
            max-width: 205px;
            line-height: 130%;
        }

        #frame .content .messages ul li div.timestamp {
            opacity: .8;
            font-size: 0.8em;
        }

        #frame .content .message-input {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 99;
            border-top:1px solid #d8e5ef;
            border-bottom:1px solid #d8e5ef;
            background-color: #fff;
        }

        #frame .content .message-input .wrap {
            position: relative;
        }

        #frame .content .message-input .wrap input {
            font-family: "proxima-nova", "Source Sans Pro", sans-serif;
            float: left;
            border: none;
            width: calc(100% - 60px);
            padding: 15px 32px 15px 15px;
            font-size: 15px;
            color: #333;
        }

        #frame .content .message-input .wrap input:focus {
            outline: none;
        }

        #frame .content .message-input .wrap .attachment {
            position: absolute;
            right: 60px;
            z-index: 4;
            margin-top: 10px;
            font-size: 1.1em;
            color: #435f7a;
            opacity: .5;
            cursor: pointer;
        }

        #frame .content .message-input .wrap .attachment:hover {
            opacity: 1;
        }

        #frame .content .message-input .wrap button {
            float: right;
            border: none;
            width: 50px;
            padding: 12px 0;
            cursor: pointer;
            background: #fff;
            color: #b8cbdb;
            font-size: 16px;
        }

        #frame .content .message-input .wrap button:hover {
            background: #fff;
            color: #333;
        }

        #frame .content .message-input .wrap button:focus {
            outline: none;
        }

        .content{padding:0 !important;}
        li.active .subject{color: #333;font-size: 13px;}
        div.contact-profile .name{font-weight: bold}
        .badge {padding: 3px 3px;color: blue}
    </style>
@endpush
@section('content')
@php
    $user = auth()->user();
@endphp
<div id="frame">
    <div id="sidepanel">
        <div id="profile">
            <div class="wrap">
                <img id="profile-img" src="{{$user->getAvatar(45, 45)}}" class="online" alt=""/>
                <p>{{$user->name}}</p>
            </div>
        </div>
        <div id="search">
            <label for=""><i class="fa fa-search" aria-hidden="true"></i></label>
            <input type="text" class="search" placeholder="Tìm người dùng, cơ sở..."/>
        </div>
        <div id="contacts">
            <ul class="list">
            </ul>
        </div>
    </div>
    <div class="content">
        <div class="contact-profile">
            <img src="{{asset('img/no-avatar.jpg')}}" alt=""/>
            <p class="name">
            </p>
        </div>
        <div class="messages">
            <ul>
            </ul>
        </div>
        <div class="message-input">
            <div class="wrap">
                <input type="text" placeholder="Nhập nội dung tin nhắn..." disabled maxlength="1024"/>
                <!--i class="fa fa-paperclip attachment" aria-hidden="true"></i-->
                <button class="submit" disabled><i class="fa fa-paper-plane" aria-hidden="true"></i></button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<!-- Moment -->
<script src="{{asset('bower_components/moment/min/moment.min.js')}}"></script>
<script src="{{asset('plugins/moment/locale/vi.js')}}"></script>

<script src="{{asset('plugins/blockUI/jquery.blockUI.min.js')}}"></script>
<script src="{{asset('js/sdc.js')}}"></script>
<script src="{{asset('js/app.js')}}"></script>
<script src="{{asset('js/messenger.js')}}"></script>
<script>
    $(function () {
        moment.locale('vi');
        Messenger({
            userId: {{ auth()->user()->id }},
            baseUrl: "{{ route('messenger') }}",
            rootUrl: "{{ config('app.url') }}/",
            activeThreadId: {!! json_encode($activeThreadId, JSON_HEX_TAG) !!},
            temporaryThread: {!! json_encode($temporaryThread, JSON_HEX_TAG) !!}
        });
    });
</script>
@endpush