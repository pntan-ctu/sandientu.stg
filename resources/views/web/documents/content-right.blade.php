<div class="col-md-3">
        <div class="blocks-document-title">
            <h3>Tin tức mới nhất</h3>
        </div>
        <div class="grid-news">
            @if(isset($newArticles))
            @foreach ($newArticles as $key => $value)
            <div class="media">
                <a class="pull-left" href="{{url('tin-tuc/'.$value->path)}}">
                    <img class="media-object" src="{{url("/image/0/0/".$value->img_path)}}" style="width: 64px; height: 64px;">
                </a>
                <div class="media-body">
                    <h4 class="media-news media-heading"> <a href="{{url('tin-tuc/'.$value->path)}}">{{$value->title}}</a></h4>
                    <div class="info-help">
                        <span><i class="fa fa-file-text-o" aria-hidden="true"></i>
                           {{$value->NewsGroup->name}}</span> 
                        <span style="margin-left: 10px;margin-right: 10px">|</span>
                        <span><i class="fa fa-clock-o" aria-hidden="true"></i> {{$value->created_at->format('d-m-Y')}}</span>
                    </div>
                </div>
            </div>
            @endforeach
            @endif
        </div> 
</div>
