@extends('layouts.web')
@section('title')
Văn bản về ATTP
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:title', 'Danh sách văn bản')
@section('content')
<div class="blocks-document">
    <div class="wrap-page">
        <div class="wrap-breadcrumb">
            <div class="container">
                <ul class="breadcrumb">
                    <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                    <li><a href="{{url('van-ban-qppl')}}" title="Văn bản">Văn bản</a></li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="content-center" >
                <div class="row">
                    <div class="col-md-9">
                        <div class="blocks-document-title">
                            <h3>Văn bản về attp</h3>
                        </div>
                        <div class="padding_search"
                             <div class="list-docs">
                                <div class="searchVB">
                                    <div class="head">
                                        Tìm kiếm nâng cao
                                    </div>
                                    <div class="bg">
                                        <div class="container">
                                            <div class="content">
                                                <form method="post" id="search-form">
                                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="label1 col-sm-3 control-label">Từ khóa</label>
                                                                <div class="col-sm-9 control">
                                                                    <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label label1">Loại văn bản</label>
                                                                <div class="col-sm-9 control">
                                                                    {!! Former::select('idType')->raw()->addOption('--Tất cả--', '0')->fromQuery($docType, 'name', 'id')->addClass('doc-selects') !!}
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label label1">Cơ quan ban hành</label>
                                                                <div class="col-sm-9 control">
                                                                    {!! Former::select('idOrgan')->raw()->addOption('--Tất cả--', '0')->fromQuery($docOrgan, 'name', 'id')->addClass('doc-selects') !!}
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>    
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="form-group">
                                                                <label class="col-sm-3 control-label label1">Lĩnh vực văn bản</label>
                                                                <div class="col-sm-9 control">
                                                                    {!! Former::select('idField')->raw()->addOption('--Tất cả--', '0')->fromQuery($docField, 'name', 'id')->addClass('doc-selects') !!}
                                                                </div>
                                                                <div class="clearfix"></div>
                                                            </div>    
                                                        </div>
                                                    </div>    
                                                    <div class="row-end">
                                                        <button type="submit" style="width: 90px; margin-top:0px; background-color: #5aa32a !important; border: 1px solid #5aa32a !important; color: white; height:36px; border-radius:4px;" id="searchSubmit">Tìm kiếm</button>
                                                        <button type="reset" style="width: 90px; margin-top:0px; background-color: #aaa !important; border: 1px solid #aaa !important; color: white; height:36px; border-radius:4px;margin-left: 10px;" id="searchReset">Làm mới</button>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="docs-title">
                                    <h3 class=""></h3>
                                </div>	

                                <div class="box-body">

                                    <table class="table table-bordered table-striped table-hover" id="datatable123" width="100%">
                                        <colgroup>
                                            <col width="20%">
                                            <col width="60%">
                                            <col width="20%">
                                        </colgroup>
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th>Số/Ký hiệu</th>
                                                <th style="text-align: center">Trích yếu</th>
                                                <th>Ngày ký</th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>
                    </div>
                    @include('web.documents.content-right')
                </div>	
            </div>
        </div>
    </div>
</div>
@endsection
@push('styles')
<link rel="stylesheet" href="{{ asset('bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css') }}">
@endpush
@push('scripts')
<!-- DataTables -->
<script src="{{ asset('bower_components/datatables.net/js/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('js/sdc.js') }}" ></script>
<script src="{{ asset('js/sdc-crud.js') }}" ></script>
<script src="{{ asset('js/web/documents.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js') }}"></script>
<script>
$(function () {
    Documents('{{ route('van-ban-qppl.index') }}');
});
</script>
@endpush