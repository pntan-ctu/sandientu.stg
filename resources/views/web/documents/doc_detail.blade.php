@extends('layouts.web')

@section('title')
{{isset($doc)? $doc->quote:"Văn bản"}}
@endsection

@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:title', isset($doc)? $doc->quote:"Văn bản")
@section('og:image', url("css/images/img1_07.jpg"))
@section('content')
<div class="blocks-document">
<div class="wrap-page">
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('van-ban-qppl')}}" title="Văn bản">Văn bản</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="content-center" >
            <div class="row">
                <div class="col-md-9">
                        <div class="blocks-document-title">
                            <h3 >Chi tiết văn bản</h3>
                        </div>	
                    <div class="docs-content" style="padding-top: 20px;">
                            @if(isset($doc))
                            <table class="docs-content-detail" width="100%" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td width="20%">Số ký hiệu: </td>
                                        <td>{{$doc->number_symbol}}</td>
                                    </tr>
                                    <tr>
                                        <td>Trích yếu: </td>
                                        <td>{{$doc->quote}}</td>
                                    </tr>
                                    <tr>
                                        <td>Ngày ban hành: </td>
                                        <td>{{$doc->sign_date->toShortDateString()}}</td>
                                    </tr>
                                    <tr>
                                        <td>Lĩnh vực: </td>
                                        <td>{{$doc->documentField->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Loại văn bản: </td>
                                        <td>{{$doc->documentType->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Cơ quan ban hành: </td>
                                        <td>{{$doc->documentOrgan->name}}</td>
                                    </tr>
                                    <tr>
                                        <td>Tệp đính kèm: </td>
                                        <td>
                                            @if(isset($doc->document_path)) 
                                            <a href="{{url('download/'.$doc->document_path) }}"><i class="fa fa-download"></i>Tải tệp đính kèm</a>
                                            | <a style="color: #0000ff;" target="_blank" href="https://docs.google.com/viewer?url={{url('download/'.$doc->document_path) }}">  Xem trước</a>
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            @endif
                        </div>	
                </div> 
                @include('web.documents.content-right')
            </div>
        </div>
    </div>
</div>
</div>
@endsection
