@extends('layouts.web')

<style>
    .media{border-bottom: 1px solid #8080803d;}
</style>
@section('title')
Địa điểm kinh doanh
@endsection
@section('content')

<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1></h1></div>
    </div>
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('/dia-diem-kinh-doanh')}}">Địa điểm kinh doanh</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="">
            <div class="col-md-4-5 cssxkd-wrap">
                <div id="form-inline">
                    <div style="float: right; text-align: right; width: 50%">    
                        <form class="form-inline searchlh" action="{{url('dia-diem-kinh-doanh')}}" method="get">
                            <input style="height: 34px; width: 50%; padding-left: 10px;" placeholder="Thông tin tìm kiếm" id="keyword" type="text" name="keyword"/>
                            <button class="iconSearchHeader" style="height: 34px; width: 10%; padding: 0px; background-color: #5aa32a;"><i class="fa fa-search"></i></button>
                        </form>
                    </div>
                </div>
                <div id="map" style="width:100%; height:700px"></div>    
                @push('scripts')
                <script>
                    function initMap3() {
                        var thanhhoa = new google.maps.LatLng(19.807812, 105.776747);

                        var map = new google.maps.Map(document.getElementById('map'), {
                              scaleControl: true,
                              center: thanhhoa,
                              zoom: 10
                        });    
                        var organization100 = {!!$organization100!!};
                        var url1 = "{{url('shop/')}}" + "/"; //"{{url('/')}}"
                        var url2 = "{{url('dia-diem-kinh-doanh/')}}" + "/";
                        $.each(organization100, function (index, value) {
                            var thanhhoa1 = new google.maps.LatLng(value.map_lat, value.map_long);
                            //var latLong = value.map_lat + "," + value.map_long;
                            var title = '';//
                            if (value.organization_type_id === 100) {
                                title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
                            } else if (value.organization_type_id === 101) {
                                title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
                            } else if (value.organization_type_id === 0) {
                                title = "<b>" + value.name + "</b>";
                            } else if (value.organization_type_id === 1) {
                                title = "<b><a target='_blank' href='" + url2 + value.path +"'>" + value.name + "</a></b>";
                            }
                            var coordInfoWindow = new google.maps.InfoWindow();
                            coordInfoWindow.setContent(createInfoWindowContent(value, title, map.getZoom()));

                            var img_ico = '';//"{{url('/')}}" + "/css/images/cssx.png";
                            if (value.organization_type_id === 100) {
                                img_ico = "{{url('/')}}" + "/css/images/cssx1.png";
                            } else if (value.organization_type_id === 101) {
                                img_ico = "{{url('/')}}" + "/css/images/cskd.png";
                            } else if (value.organization_type_id === 0) {
                                img_ico = "{{url('/')}}" + "/css/images/branch.png";
                            } else if (value.organization_type_id === 1) {
                                img_ico = "{{url('/')}}" + "/css/images/market.png";
                            }
                            var marker = new google.maps.Marker({map: map, position: thanhhoa1, icon: img_ico});
                            marker.addListener('click', function () {
                                coordInfoWindow.open(map, marker);
                            });

                            map.addListener('zoom_changed', function () {
                                coordInfoWindow.setContent(createInfoWindowContent(new thanhhoa1, map.getZoom()));
                                coordInfoWindow.open(map);
                            });
                        });
                    }

                    var TILE_SIZE = 256;

                    function createInfoWindowContent(value, latLong, zoom) {
                        var scale = 1 << zoom;

                        return [
                            latLong,
                            //value.name,
                            //'Chi tiếṭ: ' + latLong,
                            'Địa chỉ: ' + value.address
     
                        ].join('<br>');
                    }

                    // The mapping between latitude, longitude and pixels is defined by the web
                    // mercator projection.
                    function project(latLng) {
                        var siny = Math.sin(latLng.map_lat * Math.PI / 180);

                        // Truncating to 0.9999 effectively limits latitude to 89.189. This is
                        // about a third of a tile past the edge of the world tile.
                        siny = Math.min(Math.max(siny, -0.9999), 0.9999);

                        return new google.maps.Point(
                                TILE_SIZE * (0.5 + latLng.map_long / 360),
                                TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
                    }
                </script>
                @endpush
            </div>
            <div class="col-md-1-5 cssxkd-wrap">
                <div class="toolbar">
                    <div class="sortsoffer">Danh sách địa điểm kinh doanh</div>
                </div>
                <div class="cs-grid">
                    <div class="cs-layout cs-list">
                        @foreach ($commercialCenters as $key => $value)
                        <div class="media diadiem">
                            <div class="item-title"><a href="{{url('dia-diem-kinh-doanh/'.$value->path)}}">{{ $value->name }}</a></a></div>
                            <div class="item-content">
                                <p><b>Địa chỉ</b>: {{ $value->address . ', ' . $value->region->full_name }}<br></p>
                                <p><b>Cơ quan quản lý</b>: {{ $value->manageOrganization->name }}</p>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
                <div style="padding-top: 10px;">
                    {!! $commercialCenters->render() !!}
                </div>
            </div>			 			
        </div>
    </div>
</div>
</div>	

@endsection
