@extends('layouts.web')

<style>
    .sec-title {background: #86bc42;padding: 5px;font-weight: bold;color:white}
    .pro-item{margin-top: 10px; margin-bottom: 10px; border-bottom: 1px solid #8080803d}
    .pro-item>a>div:first-child{width: 70px; height: 65px; float: left; border: 1px solid #8080803d; margin-bottom: 5px}
    .pro-item>a>div>img{width: 70px; height: 60px}
    .pro-item>a>div:nth-child(2){width: calc(100% - 70px); float: left; padding-left: 8px}

</style>

@section('content')
<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1></h1></div>
    </div>
     <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('/dia-diem-kinh-doanh')}}">Địa điểm kinh doanh</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="col-md-1-5 hidden-sm hidden-xs">
            <div class="">
                @include('web.products.productscate_left')
            </div>
        </div>
        <div class="col-md-4-5 col-sm-12 col-xs-12">
            <div class="area-page-wrapper">
                <div class="commercial-page__info" style="padding-bottom: 0px">
                    <h1>{{$CommercialCenter->name}}</h1>
                </div>
                <div class="row">
                    <div class="col-md-9 col-sm-9 col-xs-9 col-100" style="font-size: 16px; margin-top: 15px;">
                        <p class=""><b>Địa phương</b>: {{ $CommercialCenter->region->path_name }}.</p>
                        <p class="" style="margin-right: 0px; text-align: justify">
                            {{ $CommercialCenter->introduction }}
                        </p>
                    </div>
                    <div class="col-md-3 col-sm-3 col-xs-3 col-100" >
                        <div class="right-commercial">
                            <div class="sec-title">Các sơ sở kinh doanh</div>
                            @foreach($organizations as $org)
                                <div class=' pro-item'>
                                    <a href="{{url("/shop/".$org->path)}}">
                                        <div>
                                            <img src="{{url("/image/70/60/".$org->logo_image)}}">
                                        </div>
                                        <div>
                                            {{$org->name}}
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            @if($num_of_orgs>6)
                                <div class="" style="text-align: right"><a href="{{url("/shop?commercial_center_id=".$CommercialCenter->id)}}">Xem thêm >></a></div><br>
                            @endif

                            <div class="sec-title">Sản phẩm tiêu biểu</div>
                            @foreach($products as $p)
                                <div class='row pro-item'>
                                    <a href="{{url("/san-pham/".$p->path)}}">
                                        <div>
                                            <img src="{{url("/image/70/60/".$p->avatar_image)}}">
                                        </div>
                                        <div>
                                            {{$p->name}}
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                        </div>
                    </div>    
                </div>    
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>	

@endsection