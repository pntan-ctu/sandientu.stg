@extends('layouts.web')
@section('title')
{{$Product->name}} 
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:title', $Product->name)
@section('og:image', url("/image/300/300/".$Product->avatar_image))
@section('content')
<script src="{{ asset('js/jquery.min.js') }}" type="text/javascript" charset="utf-8"></script>
<script src="{{ asset('js/cloud-zoom.1.0.2.js') }}" type="text/javascript" charset="utf-8"></script>

<div class="wrap-page">
    <div class="block-title-h1">
        <!-- <div class="container" style="padding-bottom: 20px;"><h1>Bất động sản</h1> </div> -->
    </div>
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('danh-muc-san-pham/'.$Product->productCategory->path.'')}}" title="Sản phẩm">{{$Product->productCategory->name}}</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
        <div class="zoom-section ">
            <h1 class="title block-mobile">{{$Product->name}}
                @if($Product->certificates->count() > 0)
                    @foreach($Product->certificates as $cer)
                    <image class="img-certifications" src="{{url('image/50/50/'. $cer->certificateCategory->icon) }}" 
                           data-toggle="tooltip" data-placement="bottom" title="{{$cer->certificateCategory->description}}" />
                    @endforeach
                @endif
            </h1>
            <div class="row">  
                <div class="col-md-6 col-sm-6 col-xs-12 images-product-detail">

                    <div class="zoom-img-product">
                        <div class="col-md-3 col-sm-3 col-xs-3 image-left"> 
                            <p>
                                @foreach ($productImages as $key => $image)
                                @if($key<5)
                                <a href="{{url("/image/800/800/".$image->image)}}" class='cloud-zoom-gallery' title='Red' rel="useZoom: 'zoom1', smallImage: '{{url("/image/600/600/".$image->image)}}' ">
                                    <img width="80" height="80" class="zoom-tiny-image" src="{{url("/image/100/100/".$image->image)}}" />
                                </a>
                                @endif
                                @endforeach
                            </p> 
                        </div>   
                        <div class="col-md-9 col-sm-9 col-xs-9 image-right" >	  
                            <div class="zoom-small-image">
                                <a href='{{url("/image/800/800/".$Product->avatar_image)}}' class = 'cloud-zoom' id='zoom1' rel="adjustX: 10, adjustY:-4">
                                    <img style="" src="{{url("/image/450/422/".$Product->avatar_image)}}"  /></a>
                            </div>
                        </div>	
                    </div>
                    <div class="clearfix"></div>
                    <div class="pro-share">
                        <div class="row">
                            <div class="col-md-5 item-col-product">
                                @include('components.share_social')
                            </div>
                            <div class="col-md-3 item-col-product" style="border-left:1px solid #e4e4e4">
                                <span class="like-product">
                                    <svg width="24" height="20" class="svg-like">
                                        <path d="M19.469 1.262c-5.284-1.53-7.47 4.142-7.47 4.142S9.815-.269 4.532 1.262C-1.937 3.138.44 13.832 12 19.333c11.559-5.501 13.938-16.195 7.469-18.07z" stroke="#FF424F" stroke-width="1.5" fill="none" fill-rule="evenodd" stroke-linejoin="round"></path>
                                    </svg>
                                 </span>
                                <span>Đã thích</span><span class="totalLike">({{ $Product->favorites()->count()}}) </span>
                            </div>
                            <div class="col-md-4 item-col-product" style="border-left:1px solid #e4e4e4">
                                <button class="button-report-product" onclick="showModalViphamProduct()"><i class="fa fa-flag-checkered"></i>&nbsp;Phản ánh vi phạm</button>
                            </div>
                        </div>    
                    </div>
                </div>	
                <div class="col-md-6 col-sm-6 col-xs-12 infomation-product-detail"	>		
                    <div class="zoom-desc">       
                        <div class="scroll-detail">
                            <h1 class="title none-mobile">{{$Product->name}}
                                @if($Product->has_check >0)
                                <i class="fa fa-check-circle fa-check-circle-sp" data-toggle="tooltip" data-placement="bottom" data-original-title="Đã có tem truy xuất nguồn gốc sản phẩm"></i>
                                @endif
                                @if($Product->certificates->count() > 0)
                                    @foreach($Product->certificates as $cer)
                                    <image class="img-certifications" src="{{url('image/50/50/'. $cer->certificateCategory->icon) }}" 
                                           data-toggle="tooltip" data-placement="bottom" data-original-title="<b>{{$cer->certificateCategory->name }} </b><hr style='margin:5px;'> {{$cer->certificateCategory->description}}" />
                                    @endforeach
                                @endif
                            </h1>
                            <div class="pdp-review-summary">
                                <div class="container-star">
                                    <input type="hidden"  data-readonly value="{{($ratings->count()>0)? $ratings->sum('rate')/$ratings->count():'0'}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-1x" data-empty="fa fa-star-o fa-1x" />
                                </div>
                                <a class="pdp-link">{{($ratings->count()>0)? $ratings->count():'0'}} đánh giá</a>
                                <div class="pdp-review-summary__divider"></div>
                                <a class="pdp-link">{{$qas != null ? $qas->count():0}} câu hỏi</a>
                                <div class="pdp-review-summary__divider"></div>
                                <span class="pdp-link">{{$Product->visits()->count()}} lượt xem</span>
                            </div>
                            <div class="flex v-center">
                                @if(isset($Product->price) && $Product->price>0)
                                <span class="price-old">{{isset($Product->price_sale) ? number_format($Product->price,0,",",".").' ₫' : ''}} </span>
                                <span class="price-detail">&nbsp;{{ isset($Product->price_sale) ? number_format($Product->price_sale,0,",",".")  :  number_format($Product->price,0,",",".") }} ₫</span>
                                @else
                                <span class="price-detail">Liên hệ</span>
                                @endif
                            </div>

                            <div class="group-size-sx">
                                <div class="row-sx nsx"><label class="label-sx">Nhà đơn vị bán:</label>
                                    <a style="color: #61ae00;font-size: 17px;" href="{{url(isset($Organization)?'/shop/'.$Organization->path:'#')}}">{{isset($Organization)?$Organization->name :''}}
                                    </a>
                                     @if($Organization->certificates->count() > 0)
                                        @foreach($Organization->certificates as $cer)
                                        <image class="img-certifications-shops-sp" src="{{url('image/50/50/'. $cer->certificateCategory->icon) }}" 
                                             data-toggle="tooltip" data-placement="bottom"  data-original-title="<b>{{$cer->certificateCategory->name }} </b><hr style='margin:5px;'> {{$cer->certificateCategory->description}}" />
                                        @endforeach
                                    @endif
                                </div>
                                <div class="row-sx vsx"><label class="label-sx">Địa chỉ người bán:</label>{{ isset($Organization)? ($Organization->address . ", " . $Organization->region->full_name) : ' ' }}</div>
                                 <div class="row-sx vsx"><label class="label-sx">Số điện thoại: </label>&nbsp {{ $Product->it5_sdtlienhe }}</div>
                                <div class="bds row-sx vsx"><label class="label-sx">Diện tích sử dụng:</label> {{ $Product->it5_dientichsudung }} &nbsp m²
                                <div class="bds row-sx vsx"><label class="label-sx">Vị trí bất động sản:</label> &nbsp {{ $Product->it5_vitri }}</div>
                                <div class="bds row-sx vsx"><label class="label-sx">Ngang x Dài:</label>  &nbsp {{ $Product->it5_ngangdai }} </div>
                                <div class="bds row-sx vsx"><label class="label-sx">Số tờ bản đồ:</label> &nbsp 1</div>
                                <div class="bds row-sx vsx"><label class="label-sx">Số thửa:</label>  &nbsp 10 </div>
                                <div class="bds row-sx vsx"><label class="label-sx">Giấy tờ pháp lý: </label>  &nbsp {{ $Product->it5_giaytophaply }}</div>
                                <div class="row-sx pp"><label class="label-sx" >Trạng thái:</label>
                                    @if($Product->status==0)
                                    <span style="color: #f44336;font-weight: 500;">Chưa kinh doanh</span>
                                    @elseif($Product->status==1)
                                    <span style="">Còn hàng</span>
                                    @elseif($Product->status==2)
                                    <span style="color: #333;font-weight: 500;">Theo mùa vụ</span>
                                    <span> {{isset($Product->duration)?': '.$Product->duration: ''}} </span> 
                                    @elseif($Product->status==3)
                                    <span style="color: #e20d21;">Hết hàng</span>
                                    @else
                                    <span style="">Ngừng kinh doanh</span>
                                    @endif
                                </div>
                                <!-- <div class="row-sx pp"><label class="label-sx" >Đơn vị tính:</label>
                                    <span style="">{{ isset($Product->unit)? $Product->unit : ' ' }}</span>
                                </div> -->
                              
                                @if($Product->productLinks->count()>0)
                                    <div class=" bds row-sx pp"><label class="label-sx" >Dự án gốc:</label>
                                        @foreach($Product->productLinks as $link)
                                        @if(isset($link->linkProduct->path))
                                        <a class="bds" href="{{url('san-pham/'.$link->linkProduct->path)}}"><span style="color: #61ae00;font-weight: 400;">{{isset($link->linkProduct)? $link->linkProduct->name:''}} |</span></a>
                                        @endif
                                        @endforeach
                                    </div>
                                @endif
                                <!-- @if( ($Product->has_check >0) && isset($Product->check_temp_image))
                                    <div class="row-sx qr"><label class="label-sx" >Tem nguồn gốc:</label>
                                     <img src="{{url("/image/128/128/".$Product->check_temp_image)}}" />
                                    </div>
                                @endif -->
                                
                            </div>	
                            <input style='display:none;' type="text" name="quantity" value="1" size="2" id="qty" class="input-text qty" maxlength="12">
                            <!-- <div class="group-size size">
                                <div class="">
                                    <label class="label-sx">Số lượng:</label>
                                    <span>
                                        <input type="text" name="quantity" value="1" size="2" id="qty" class="input-text qty" maxlength="12">
                                        <button class="reduced items-count" onclick="var result = document.getElementById('qty'); var qty = result.value; if (!isNaN(qty) && qty > 0)
                                                result.value--;
                                                return false;" type="button">
                                            <i class="icon-minus"> - </i>
                                        </button>
                                        <button class="increase items-count" onclick="var result = document.getElementById('qty');
                                                var qty = result.value;
                                                if (!isNaN(qty))
                                                    result.value++;
                                                return false;" type="button">
                                            <span> + </span>
                                        </button>
                                    </div>
                                </span>
                            </div> -->
                            <div class="row button-array">
                                @if(isset($Product->price) && $Product->price>0)
                                <div class="col-md-4 col-sm-5 padding-right-5 add">
                                    <button type="button" class="btn-add-cart buy-now btn" onclick="addItemsCartProductsDetail()" ><i class="fa fa-shopping-cart"></i> Cho vào giỏ</button>
                                </div>
                                <div class="col-md-4 col-sm-4 padding-right-5 action">
                                    <button type="button" class="btn-add-order buy-now btn" >Đặt hàng</button>
                                </div>
                                @else
                                <div class="col-md-4 col-sm-4 padding-right-5 add chat">
                                    <button type="button"  class="chat btn send-request" data-id="{{$Product->id}}"><i class="fa fa-comments"></i>Gửi yêu cầu</div>
                                @endif
                            </div>		
                        </div>
                    </div>
                </div>
            </div>	
        </div>
        @include('web.products.shop_info_product')
        <div class="row">			
            <div class="wrap-detail">
                @include('web.products.product_tab')
            </div>
        </div>
        <div class="row">
            @if($Product->id_parent == 40)
                @include('web.products.maps_polygon')
            @endif
            
            @include('web.products.products_relation_shop')
            <!-- @include('web.products.products_lienquan') -->
        </div>
    </div>
</div>	
@include('web.products.modal_rate')
@include('web.shops.modal_vipham_organ')
@endsection
@push('scripts')
<script>
    $(document).ready(function(){
        var id_parent = <?php echo $Product->id_parent;  ?>;
       if(id_parent == 50){
        $(".bds").hide();
       }
    }); 
</script>
<script type="text/javascript">
    $("#upQty").click(function(){
        var qty = $("#qty").val();
        qty++;
            $("#qty").val(qty);
       
    });
    $("#downQty").click(function(){
        var qty = $("#qty").val();
        if(qty >1 ){
        qty--;
        $("#qty").val(qty);
       }
    });
    $(function () {
        //var is_touch_device = ("ontouchstart" in window) || window.DocumentTouch && document instanceof DocumentTouch;
         $('[data-toggle="tooltip"]').tooltip({
             html:true,
             //trigger:  is_touch_device ? "click" : "hover",
         });   
        var checkLike = {{$favoriteCount}};
        if(checkLike <1){
            $(".like-product path").css("fill", "none");
          }
        else{
            $(".like-product path").css("fill", "rgb(255, 66, 79)");
          }
        
//        $('.btn-add-cart').on('click', function (e) {
//            
//        });
        $('.btn-add-order').on('click', function (e) {
            var userId = {{getUserId()}};
            if(userId <0){
                toastr.error('Bạn chưa đăng nhập');
                return false;
            }
            addItemsCartProductsDetail();
            setTimeout(function(){
                location.href= "{{url('mua-hang')}}";
            }, 3000)                    
        });
        $('.like-product').on('click', function (e) {
            var userId = {{getUserId()}};
            if(userId <0){
                toastr.error('Bạn chưa đăng nhập');
                return false;
            }
            var color = $( 'path' ).css( "fill" );
            if(color == 'none'){
                $(".like-product path").css("fill", "rgb(255, 66, 79)");
            }
            else{
                $(".like-product path").css("fill", "none");
            }
            var form_data = new FormData();
            form_data.append('productId', {{$Product->id}});
            $.ajax({
                url: "{{url('add-like-product')}}",
                method: 'POST',
                data: form_data,
                contentType: false,
                processData: false,
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
            }).done(function (msg) {
                if (msg.message == 'success'){
                    $('.totalLike').html('');
                    $('.totalLike').html('('+msg.total+')');
                   
                }
            });
        });
    });
    function showModalRate(){
        $('#modal-rate').modal();
    }
    function showModalViphamProduct(){
        $('#modal-vipham').modal();
    }
    function validateVipham(){
        if( $('input[name=loaiViphamRadios]:checked').val() == null){
            alert("Bạn chưa chọn loại vi phạm ");
            return false;
        }
        if($('#txtVipham').val().trim() === ''){
            alert("Bạn chưa nhập ý kiến ");
            $('#txtVipham').focus();
            return false;
        }
        return true;
    }
    function themVipham(){
        if(!checkLogin())
            return false;
        if(!validateVipham())
            return false;
        var form_data = new FormData();
        form_data.append('typeId', {{$Product->id}});
        form_data.append('comment', $('#txtVipham').val());
        var selValue = $('input[name=loaiViphamRadios]:checked').val(); 
        form_data.append('loaiVipham', selValue);
        form_data.append('typeInfrin', 'Product');
        $.ajax({
            url: "{{url('them-vi-pham')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
        }).done(function (msg) {
            if (msg.message == 'success'){
                toastr.success("Phản ánh vi phạm thành công !");
                $('#txtVipham').val('');
                $('#modal-vipham').modal('hide');
            }
        });
    }
    function validate(){
        var userId = {{getUserId()}};
        if(userId <0){
            toastr.error('Bạn chưa đăng nhập');
            return false;
        }
        var qty = $('#qty').val();
        if(qty <1){
             toastr.error('Số lượng cần mua phải lớn hơn 0');
            return false;
        }
        return true;
    }
    function addItemsCartProductsDetail(){
        if(!validate())
                return false;
        var form_data = new FormData();
        form_data.append('productId', {{$Product->id}});
        form_data.append('amount', $('#qty').val());
        form_data.append('organization_id', {{$Product->organization_id}});
        $.ajax({
            url: "{{url('add-cart')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
        }).done(function (msg) {
            if (msg.status == 'success'){
                $('.cart-count').html('');
                $('.cart-count').html(''+msg.totalQty+'');
                toastr.success("Thêm sản phẩm thành công !");
                return true;
            }
            else {
                toastr.error(msg.message);
                return false;
            }
        });
    }
   
</script>
@endpush