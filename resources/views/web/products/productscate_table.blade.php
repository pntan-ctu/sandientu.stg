<div class="wrap-product product-both">
    <div class="">
        <div class="posts row is-flex">
            @foreach ($ProductCate as $key => $value)
            <div class="col-sm-4 col-md-3 col-xs-6">
                <div class="thumb-product item">
                    <div class="img-item">
                        <div class="img">
                            <a href="{{url('san-pham/'.$value->path)}}" title="">
                                <img src="{{isset($value->avatar_image)? url("/image/300/300/".$value->avatar_image):url("css/images/no-image.png")}}" alt="{{$value->name }}">
                            </a>
                        </div>
                        <div class="cer-item">
                            @if($value->certificates->count() > 0)
                            @foreach($value->certificates as $cer)
                            <image class="img-certifications-index" src="{{url('image/50/50/'. $cer->certificateCategory->icon) }}" title="{{$cer->certificateCategory->name }}" />
                            @endforeach
                            @endif
                        </div>
                    </div>
                    <div class="wrap-info">
                        <h3><a href="{{url('san-pham/'.$value->path)}}">{{ $value->name }}</a></h3>
                        @if(isset($value->price) && $value->price>0)
                        @if(isset($value->price_sale))
                        <div class="price-product">
                            <span class="discount">{{isset($value->price_sale) ? number_format($value->price_sale,0,",",".").' ₫' : ''}}</span>
                            <span class="true-price">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</span>
                        </div>
                        <!-- <p class="discount">{{ number_format($value->price_sale,0,",",".") }} <font>VND</font></p>
                            <p class="true-price">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                            <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})"><div class="addcart1"></div></a> -->
                        @else
                        <p class="discount">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                        <!-- <p class="discount">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                        <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})">
                            <div class="addcart1"></div>
                        </a> -->
                        @endif
                        @else
                        <p class="discount">Giá: Liên hệ</p>
                        <!-- <button type="button" class="send-request" data-id="{{$value->id}}">Gửi yêu cầu</button> -->
                        @endif
                    </div>
                    <div class="container-star" style="width: 100%; padding-left:10px">
                        <input type="hidden" data-readonly value="{{($value->ratings->count()>0)? $value->ratings->sum('rate')/$value->ratings->count():'0'}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-1x" data-empty="fa fa-star-o fa-1x" />
                        <!-- <span class="shopping-cart-index"><a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})"> <i class="fa fa-shopping-cart"></i></a> </span> -->
                        @if(isset($value->price) && $value->price>0)
                        <span class="shopping-cart-index"><a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})"> <i class="fa fa-shopping-cart"></i></a> </span>
                        @else
                        <button type="button" class="send-request" data-id="{{$value->id}}">Gửi yêu cầu</button>
                        @endif
                    </div>
                    <div class="shop-info-index">
                        <div class="shop-info-index-avatar">
                            <img class="image-avatar-index" src="{{isset($value->Organization->logo_image)? url("/image/20/20/".$value->Organization->logo_image): url("css/images/no-image.png")}}">
                        </div>

                        <span class="shop-info-index-text">
                            <a href='{{url('shop/'.$value->Organization->path)}}'>{{$value->Organization->name}}</a>
                        </span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</div>

{!! $ProductCate->render() !!}