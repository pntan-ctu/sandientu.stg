<div class="col-md-12 col-sm-12 col-xs-12 content-product-detail">
    <ul class="nav nav-tabs">
        <!-- @foreach ($contentProduct as $key => $value)
        <li class="col-100 @if($key==0) active @endif" >
             <a href="#tab{{$key}}" data-toggle="tab">{{$value->name }}</a>
        </li>
        @endforeach -->
         <li class="col-100 active " >
             <a href="#tab{{$key}}" data-toggle="tab">Giới thiệu bất động sản</a>
        </li>
        <li class="col-100"><a href="#tab{{$contentProduct->count() }}" data-toggle="tab" aria-expanded="false">Thông tin chi tiết</a></li>
        <li class="col-100"><a href="#tab{{$contentProduct->count()+1 }}" data-toggle="tab" aria-expanded="false">Đánh giá bất động sản</a></li>
        <li class="col-100"><a href="#tab{{$contentProduct->count()+2 }}" data-toggle="tab" aria-expanded="false">Hỏi & Đáp</a></li>
        <!-- <li class="col-100"><a href="#tab{{$contentProduct->count()+3 }}" data-toggle="tab" aria-expanded="false">Địa điểm</a></li> -->
    </ul>
    <div class="wrap-content-product">
        <div class="tab-content">
            @foreach ($contentProduct as $key => $value)
            <div class="tab-pane @if($key==0) active @endif" id="tab{{$key}}">
                <div class="mod-title">
                    <h2 class="pdp-mod-section-title outer-title">{{$value->name }} {{$Product->name}}</h2>
                </div>
                <div class="pdp-mod-detail" style="padding: 0 15px">
                    {!! $value->content!!}
                </div>
            </div>
            @endforeach
            @include('web.products.detail_product_tab')
            <div class="tab-pane" id="tab{{$contentProduct->count()+1 }}">
                <div class="content pdp-mod-review">
                    <div class=" product-rating-overview">
                        <div class="mod-title">
                            <h2 class="pdp-mod-section-title outer-title">Đánh giá và nhận xét của: {{$Product->name}}</h2>
                        </div>
                        <div class="mod-rating">
                            <div class="summary">
                                <div class="score"><span>{{($ratings->count()>0)? number($ratings->sum('rate')/$ratings->count()):'0'}}</span> trên 5</div>
                                <div class="average">
                                    <div class="container-star " style="width:166.25px;height:30px">
                                        <input type="hidden"  data-readonly value="{{($ratings->count()>0)? $ratings->sum('rate')/$ratings->count():'0'}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" />
                                    </div>
                                </div>
                                <div class="count">{{($ratings->count()>0)? $ratings->count():'0'}} đánh giá</div>
                            </div>
                            <div class="detail">
                                <ul>
                                    @for ($i = 5; $i >0 ; $i--)
                                    <li>
                                        <span>
                                            <span class="star-number">{{ $i }}</span> SAO
                                        </span>
                                        <span class="progress-wrap">
                                            <div class="pdp-review-progress">
                                                <div class="bar bg"></div>
                                                <div class="bar fg" style="width:{{($ratings->count()>0)? ($ratings->where('rate',$i)->count()/$ratings->count())*100:'0'}}%"></div>
                                            </div>
                                        </span>
                                        <span class="percent">{{($ratings->count()>0)? $ratings->where('rate',$i)->count():'0'}}</span>
                                    </li>
                                    @endfor
                                </ul>
                            </div>
                            <div class="">
                                <button type="button" onclick="showModalRate()" class="next-btn next-btn-primary next-btn-medium btn-rate">ĐÁNH GIÁ SẢN PHẨM NÀY</button>
                            </div>
                        </div>	
                    </div>
                    <div class="mod-reviews">
                        @if(isset($ratings))
                         @foreach($ratings as $rating)
                            <div class="item">
                                <div class="top">
                                    <span class="container-star starCtn pull-left" style="width:140px;height:16px">
                                        <input type="hidden"  data-readonly value="{{$rating->rate}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" />
                                    </span>
                                    <span class="title left">&nbsp;</span>
                                    <span class="right">{{ $rating->created_at->toShortDateString()}}</span>
                                </div>
                                <div class="middle">
                                    <span>bởi<!-- --> <!-- -->
                                        <a href='{{ isset($rating->user)?$rating->user->getUrlActivity():"#"}}'>{{isset($rating->user)? $rating->user->name:''}}</a>
                                    </span>
                                </div>
                                <div class="content">{{ $rating->comment }}</div>
                                <div class="bottom">
                                </div>
                            </div>
                         @endforeach
                        @endif
                        
                    </div>	
                </div>
            </div>
            @include('web.products.qa_tab')
        </div>
    </div>
</div>
