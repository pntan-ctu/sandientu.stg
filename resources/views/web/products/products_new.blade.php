<div class="col-md-1-5">
    <div class="sidebar-right-detail">
        <h3 class="sidebar-title">Sản phẩm mới</h3>
        <div class="products-content-right row is-flex">
            @foreach ($ProductNew as $key => $value)
            <div class="col-md-4 col-sm-4 col-xs-6 products-new-right">
                <div class="thumb-product item ">
                    <div class="img">
                        <a href="{{url('san-pham/'.$value->path)}}" title=""><img src="{{isset($value->avatar_image) ? url("/image/300/300/".$value->avatar_image): url("css/images/no-image.png")}}" alt=""></a>
                    </div>
                    <div class="wrap-info">
                        <h3><a href="{{url('san-pham/'.$value->path)}}">{{$value->name }}</a></h3>
                        <!--   <p class="price">{{$value->price }} <font>VND</font> </p>-->
                        <!-- <p>{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                    <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})"><div class="addcart1"></div></a> -->
                        @if(isset($value->price) && $value->price>0)
                        @if(isset($value->price_sale) && $value->price_sale>0)
                        <p class="discount">{{ number_format($value->price_sale,0,",",".") }}
                            <font>VND</font>
                        </p>
                        <p class="true-price">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                        <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})">
                            <div class="addcart1"></div>
                        </a>
                        @else
                        <p class="discount">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                        <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})">
                            <div class="addcart1"></div>
                        </a>
                        @endif
                        @else
                        <p class="discount">Giá: Liên hệ</p>
                        <button type="button" class="send-request" data-id="{{$value->id}}">Gửi yêu cầu</button>
                        @endif
                    </div>
                </div>
            </div>

            @endforeach
        </div>
    </div>
</div> 