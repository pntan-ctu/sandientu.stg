<div class="modal fade" id="modal-rate" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-book"></i> Đánh giá sản phẩm này </h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive">
                    <div class="box-body">
                      
                            <div class="fs-dtrtcmtbox">
                                <p class="fs-dtrtcmti2">Bạn chấm sản phẩm này bao nhiêu sao?</p> 
                                <div class="fs-dtrtbhov"> 
                                    <div class="fs-dtrtbig box-star "> 
                                        <input type="hidden" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-2x" data-empty="fa fa-star-o fa-2x" />
                                    </div> 
                                </div>
                              
                                <textarea id="txtNoteRating" name="txtNoteRating" rows="3" class="f-cmttarea fsformsc" placeholder="Vui lòng nhập nhận xét của bạn!"></textarea>
                                <div class="fs-dtrtbots clearfix"> 
                                    <p>Một đánh giá có ích thường dài từ 100 ký tự trở lên</p>
                                   
                                </div>
                            </div>
                      
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-times"></i>Hủy</button>
                <button id="btn-delete" type="button" class="btn btn-primary" onClick='saveRate()'><i class="fa fa-check"></i>Gửi</button>
            </div>
        </div>
    </div>
</div>
<script>
    function validateRate(){
        var rate = $('.rating').rating('rate');
        if(!rate){
            alert("Bạn chưa đánh giá sản phẩm");
            return false;
        }
        if($('#txtNoteRating').val().trim() === ''){
            alert("Bạn chưa nhập ý kiến");
            $('#txtNoteRating').focus();
            return false;
        }
        return true;
    }
    function saveRate(){
        if({{getUserId()}} <1){
            alert('Bạn chưa đăng nhập');
            return false;
        }
        if(!validateRate())
        {
            return false;
        }
        var rate = $('.rating').rating('rate');
        var form_data = new FormData();
        form_data.append('productId', {{$Product->id}});
        form_data.append('rate', rate);
        form_data.append('comments', $('#txtNoteRating').val());
        $.ajax({
            url: "{{url('add-rate')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (msg) {
            if (msg.message == 'success'){
                toastr.success("Thêm đánh giá thành công !");
                $('#modal-rate').modal('hide');
            }
        });
    }
</script>