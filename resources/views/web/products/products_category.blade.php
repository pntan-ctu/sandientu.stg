
<?php $data = isset($children)?$children:$ProductCate ?>

@if(isset($data->products))
    @foreach($data->products as $item)
      <div class="col-sm-4 col-md-3 col-xs-6">
        <div class="thumb-product item">
            <div class="img">
                <a href="{{ $item->path }}" title="">
                    <img src="{{url("storage/".$item->avatar_image)}}" alt="" ></a>
            </div>
            <div class="wrap-info">
                <h3><a href="{{url('/products-detail')}}">{{ $item->name }}</a></h3>
                <p class="price">{{ $item->price }} <font>VND</font> </p>
            </div>
        </div>
    </div>

    @endforeach
@endif
@if(isset($data->children)&&count($data->children) > 0)
    @foreach($data->children as $child) 
        @include('web.products.products_category',['children' =>$child])
    @endforeach
@endif

