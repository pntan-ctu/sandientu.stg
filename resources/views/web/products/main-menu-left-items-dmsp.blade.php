@foreach($items as $item)
<li class="has-sub" >
    <a href="{{ $item->url() }}">
        {{ $item->title }}
    </a>
    @if($item->hasChildren())
    <i class="fa fa-plus fa-sub sub-dropmenu" aria-expanded="false"></i>
    @endif
    @if($item->hasChildren())
    <ul>
        @include('web.products.main-menu-left-items-dmsp', ['items' => $item->children()])
    </ul>
    @endif
</li>
@endforeach