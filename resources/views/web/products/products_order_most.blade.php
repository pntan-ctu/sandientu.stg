<div class='sp-ban-chay'>
    <div class="title-bc"><a>Sản phẩm bán chạy</a></div>
    <div class="list-sp-ban-chay">
        @foreach ($ProductOrder as $key => $value)
        <div class="col-md-2 col-sm-4 col-xs-6">
            <div class="thumb-product item">
                <div class="img">
                    <a href="{{url('san-pham/'.$value->path)}}" title="">
                        <img src="{{isset($value->avatar_image) ? url("/image/300/300/".$value->avatar_image): url("css/images/no-image.png")}}" alt=""></a>
                </div>
                <div class="wrap-info">
                    <h3><a href="{{url('san-pham/'.$value->path)}}">{{$value->name }}</a></h3>
                    <!--                         <p class="discount">{{$value->price }} <font>VND</font> </p>-->
                    <p class="discount">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                    <a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})">
                        <div class="addcart1"></div>
                    </a>
                    <!--                        <p  class="discount">100,000 <font>VND</font></p>
                        <p class="true-price">7,000 <font>VND</font></p>-->
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div> 