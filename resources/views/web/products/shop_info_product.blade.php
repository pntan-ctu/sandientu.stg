<div class="product-page-seller-info v-center">
    <div class="product-page-seller-info__section-1">
        <div class="product-page-seller-info__header-wrapper">
            <a class="product-page-seller-info__header-portrait">
                <div class="avatar-tp">
                    <img class="avatar__img" @if(isset($Organization) && isset($Organization->logo_image)) src="{{url("/image/0/0/".$Organization->logo_image)}}" @else src="{{url('dist/img/user2-160x160.jpg')}}" @endif >
                    
                    <!--<img class="avatar__img" src="{{url("/image/0/0/".$Organization->logo_image)}}">-->
                </div>
            </a>
            <div class="product-page-seller-info__header-info">
                <a class="product-page-seller-info__name-status" href="{{url(isset($Organization)?'/shop/'.$Organization->path:'#')}}">
                    <div class="product-page-seller-info__shop-name">{{isset($Organization)? $Organization->name:''}}</div>
                    <!--<div class="product-page-seller-info__active-time">Online 48 phút trước</div>-->
                </a>
                <div class="product-page-seller-info__buttons">
                    <a class="product-page-seller-info__view-shop" href="{{url(isset($Organization)?'shop/'.$Organization->path:'#')}}">
                    <button class="btn btn-light btn--s ">Ghé thăm công ty</button></a>
                    <!--<button class="btn btn-light btn--s btn--inline">theo dõi</button>-->
                </div>
            </div>
        </div>
    </div>
    <div class="product-page-seller-info__section-2">
        <div class="product-page-seller-info__item ">
            <div class="product-page-seller-info__item__wrapper">
                <div class="product-page-seller-info__item__header">
                    <span class="product-page-seller-info__item__primary-text">{{$countProducts}}</span>
                </div>
                <div class="product-page-seller-info__item__complement-text">Tin đăng</div>
            </div>
            <div class="product-page-seller-info__item__separator"></div>	
        </div>	
        <div class="product-page-seller-info__item">
            <div class="product-page-seller-info__item__wrapper">
                <div class="product-page-seller-info__item__header">
                    <span class="product-page-seller-info__item__primary-text">{{($Organization->ratings->count()>0)? $Organization->ratings->count():'0'}}</span>
                </div>
                <div class="product-page-seller-info__item__complement-text">Đánh giá</div>		
            </div>
            <div class="product-page-seller-info__item__separator"></div>
        </div>
        <div class="product-page-seller-info__item">
            <div class="product-page-seller-info__item__wrapper">
                <div class="product-page-seller-info__item__header">
                    <span class="product-page-seller-info__item__primary-text">{{($Organization->questionAnswers->count()>0)? $Organization->questionAnswers->count():'0'}}</span>
                </div>
                <div class="product-page-seller-info__item__complement-text">Lượt hỏi đáp</div>		
            </div>
            <div class="product-page-seller-info__item__separator"></div>
        </div>

        <div class="product-page-seller-info__item">
            <div class="product-page-seller-info__item__wrapper">
                <div class="product-page-seller-info__item__header">
                    <span class="product-page-seller-info__item__primary-text">{{isset($Organization->created_at)?$Organization->created_at->toShortDateString():''}}</span>
                </div>
                <div class="product-page-seller-info__item__complement-text">Ngày tham gia</div>		
            </div>
            <div class="product-page-seller-info__item__separator"></div>
        </div>
    </div>
</div>