@extends('layouts.web')
@section('title')
{{$title}}
@endsection
@section('description', 'Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa')
@section('og:title', $title)
@section('content')
<style>
    a{
        cursor: pointer;
    }
</style>
<body onload="onloadPage()" >
<div class="wrap-page">
    <div class="block-title-h1">
        <div class="container"><h1>Sản phẩm</h1></div>
    </div>
    <div class="wrap-breadcrumb">
        <div class="container">
            <ul class="breadcrumb">
                <li><a href="{{url('/')}}" title="Trang chủ">Trang chủ</a></li>
                <li><a href="{{url('/danh-muc-san-pham')}}" title="Sản phẩm">{{$title}}</a></li>
            </ul>
        </div>
    </div>
    <div class="container">
       
        <div class="">
            <div class="col-md-1-5 filter-product-left">
                @include('web.products.productscate_left')
                <div class="refine-search">
                    <div class="search-list">
                        <div class="filter-custom">
                             <div class="subheading">
                                <span>Quy mô cơ sở</span>
                            </div>
                            <div class="search-item">
                                <div>
                                    @if(\App\Models\OrganizationLevel::all() != null )
                                    @foreach( \App\Models\OrganizationLevel::all() as $value)
                                        <input type="checkbox" id="T{{$value->id}}" onchange="setLocation()" class="filled-in" value="{{$value->id}}" name="filter_category[]">
                                        <label for="T{{$value->id}}"> {{$value->name}}</label>
                                    @endforeach
                                    @endif

                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
                <div class="refine-search">
                    <div class="search-list">
                        <div class="filter-custom">
                             <div class="subheading">
                                <span>Giấy chứng nhận, xác nhận</span>
                            </div>
                            <div class="search-item">
                             
                                <div>
                                @if(\App\Models\CertificateCategory::where('type',1) != null )
                                    @foreach( \App\Models\CertificateCategory::where('type',1)->get() as $value)
                                        <input type="checkbox" id="C{{$value->id}}" onchange="setLocation()" class="filled-in" value="{{$value->id}}" name="filter_cert[]">
                                        <label for="C{{$value->id}}"> {{$value->name}}</label>
                                    @endforeach
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    
                </div>
            </div>
            <div class="col-md-4-5">
                <div class="col-main-chil">
                    <div class="toolbar">
                        <div class="row">	
                            <div class="col-md-5 col-sm-5">
                                <div class="sortsoffer">
                                    {{$title}}
                                  
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-7">
                                <div id="sort-by">
                                    <label >Sắp xếp:</label>
                                    <select id="input-sort" class="form-control" onchange="setLocation()">
                                        <option value="default"  >Mặc định</option>
                                        <option value="name-asc">Tên (A - Z)</option>
                                        <option value="name-desc">Tên (Z - A)</option>
                                        <option value="price-asc">Giá (Thấp &gt; Cao)</option>
                                        <option value="price-desc">Giá (Cao &gt; Thấp)</option>
<!--                                        <option value="5">Đánh giá (Cao nhất)</option>
                                        <option value="6">Đánh giá (Thấp nhất)</option>-->
                                    </select>
                                </div>
                                <div id="limiter">
                                    <label>Xem:</label>
                                    <select id="input-limit" class="form-control" onchange="setLocation()">
                                        <option value="12" >12</option>
                                        <option value="25">25</option>
                                        <option value="50">50</option>
                                        <option value="100">100</option>
                                    </select>
                                </div>
                            </div>
                        </div>		
                    </div>
                    @include('web.products.productscate_table')
                   
                   
                </div>
            </div>
        </div> 	
    </div>
</div>	
</body>

@endsection

<script type="text/javascript">
   function onloadPage(){
        $(document).ready(function(){
            var size = urlParam('size');
            var sort = urlParam('sort');
            if(urlParam('level')!== null && urlParam('level') !== 0) {
                var urlLevels = decodeURIComponent(urlParam('level'));
                var levels = urlLevels.split(',');
                $.each(levels, function( index, value) {
                    $("#T"+value+"").attr('checked', true);
                });
            }
            if(urlParam('certs')!== null && urlParam('certs') !== 0) {
                var urlLevels = decodeURIComponent(urlParam('certs'));
                var levels = urlLevels.split(',');
                $.each(levels, function( index, value) {
                    $("#C"+value+"").attr('checked', true);
                });
            }
            $('#input-limit option[value='+size+'    ]').attr('selected','selected');  
            $('#input-sort option[value='+sort+'    ]').attr('selected','selected');  
        });
   }
   function setLocation(){
        var sUrlParams = "";
        if($('#input-limit'))
            sUrlParams +="&size="+ $('#input-limit').val();
         //var size = $('#input-limit').val();
        if($('#input-sort'))
            sUrlParams +="&sort="+ $('#input-sort').val();
        //var sort = $('#input-sort').val();
        var levels = new Array();
        $.each($("input[name='filter_category[]']:checked"), function() {
          levels.push($(this).val());
        });
        var certs = new Array();
        $.each($("input[name='filter_cert[]']:checked"), function() {
            certs.push($(this).val());
        });
        var curentPage = urlParam('page');
//        if(curentPage === null || curentPage === 0)
//            curentPage=1;
        var curentPage = {{$ProductCate->currentPage()}} ;
        window.location.href = window.location.href.replace( /[\?#].*|$/,
        "?page="+curentPage+sUrlParams+"&level="+levels+"&certs="+certs);
   }
   function loadData() {
    var limit = $('#input-limit').val();
     var sort = $('#input-sort').val();
     var form_data = new FormData();
     form_data.append('id', 1);
        $.ajax({
            url: '' ,
            type: 'GET',
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
             data:{sort: sort, limit: limit},
            success: function (data1) {
               // console.log(data1.html);
                  $('.posts').html(data1.html);
            },
            error: function (msg) {

            }
         });
    }
            

</script>

