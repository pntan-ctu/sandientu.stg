<div class="sp-lien-quan">
    <div class="title-bc"><a>Các tin đăng liên quan</a></div>
    <div class="list-sp-lien-quan row">
        @foreach ($ProductRelation as $key => $value)
        <div class="col-md-2 col-sm-4 col-xs-6 thumb-product-list-item">
            <div class="thumb-product item ">
                <div class="img">
                    <a href="{{url('san-pham/'.$value->path)}}" title=""><img src="{{isset($value->avatar_image) ? url("/image/300/300/".$value->avatar_image): url("css/images/no-image.png")}}" alt=""></a>
                </div>
                <div class="wrap-info">
                    <h3><a href="{{url('san-pham/'.$value->path)}}">{{$value->name }}</a></h3>
                    @if(isset($value->price) && $value->price>0)
                    @if(isset($value->price_sale) && $value->price_sale>0)
                    <div class="price-product">
                        <span class="discount">{{isset($value->price_sale) ? number_format($value->price_sale,0,",",".").' ₫' : ''}}</span>
                        <span class="true-price">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</span>
                    </div>
                    @else
                    <p class="discount">{{isset($value->price) ? number_format($value->price,0,",",".").' ₫' : ''}}</p>
                    @endif
                    @else
                    <p class="discount">Giá: Liên hệ</p>
                    @endif
                </div>
                <div class="container-star" style="width: 100%; padding-left:10px">
                    <input type="hidden" data-readonly value="{{($value->ratings->count()>0)? $value->ratings->sum('rate')/$value->ratings->count():'0'}}" class="rating rating-tooltip-manual" data-filled="fa fa-star fa-1x" data-empty="fa fa-star-o fa-1x" />
                    @if(isset($value->price) && $value->price>0)
                    <span class="shopping-cart-index"><a href="javascript:void(0)" onclick="addItemToCart({{$value->id}},{{$value->organization_id}})"> <i class="fa fa-shopping-cart"></i></a> </span>
                    @else
                    <button type="button" class="send-request" data-id="{{$value->id}}">Gửi yêu cầu</button>
                    @endif
                </div>
                <div class="shop-info-index">
                    <div class="shop-info-avatar">
                        <img class="image-avatar-shop-prod" src="{{isset($value->Organization->logo_image)? url("/image/20/20/".$value->Organization->logo_image): url("css/images/no-image.png")}}">
                    </div>

                    <span class="shop-info-shop-name-text">
                        <a href='{{url('shop/'.$value->Organization->path)}}' title="{{$value->Organization->name}}">{{$value->Organization->name}}</a>
                    </span>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</div> 