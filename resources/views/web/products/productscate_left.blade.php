<link href="{{ asset('css/bolocsanpham.css') }}" rel="stylesheet">
<div class="woocommerce widget_product_categories">
    <h2 class="widget-title-h2">Danh mục</h2>
    <div id='cssmenu'>
        <ul>
            <li><a id="all_products" href="{{url('danh-muc-san-pham')}}"  style="cursor: pointer;">Tất cả ({{App\Business\ProductBussiness::getListForView()->count()}})</a></li>
            <li><a href="{{url('danh-muc-san-pham/khuyen-mai')}}">Sản phẩm khuyến mại</a></li>
            <li><a href="{{url('danh-muc-san-pham/moi-ban')}}">Sản phẩm mới</a></li>
            <li><a href="{{url('danh-muc-san-pham/mua-nhieu')}}">Sản phẩm được mua nhiều</a></li>
            <li><a href="{{url('danh-muc-san-pham/quan-tam')}}">Sản phẩm được xem nhiều</a></li>
            @include('web.products.main-menu-left-dmsp')

        </ul>
    </div>
</div>

<div class="clearfix"></div>

@push('scripts')
<script src="{{asset('js/menu_dmsp_left.js')}}"></script>

@endpush