<div class="tab-pane" id="tab{{$contentProduct->count() }}">
    <div class="content pdp-mod-review">
        <div class="product-info">
            <div class="row">
                <div class="col-md-6 col-xs-12">
                    <h4 class="col-md-12 col-xs-12">Thông tin bất động sản</h4>
                    @php
                        $count = count($productInfos);
                    @endphp
                    @for($i = 0; $i < $count; $i++) 
                        <div class="row">
                            <div class="col-md-5 col-xs-5">{{$labelProductInfos[$i]}}</div>
                            <div class="col-md-7 col-xs-7">{{ $Product->{$productInfos[$i]} ?? "" }}</div>
                        </div> 
                    @endfor
                    <!-- @if(count($proCerts) > 0)
                        <h4 class="col-md-12 col-xs-12">Chứng nhận, xác nhận của sản phẩm</h4>
                        @foreach ($proCerts as $pcer)
                        <div class="row">
                            <div class="col-md-7 col-xs-6"><b>{{$pcer->title ?? ""}}</b><br>{{$pcer->description ?? ""}}</div>
                            <div class="col-md-5 col-xs-6 cert-image-product">
                                @if(isset($pcer->image))
                                <a href="{{url("/image/640/600/".$pcer->image)}}" class="fancybox">
                                    <img src="{{url("/image/150/150/".$pcer->image)}}">
                                </a>
                                @endif
                            </div>
                        </div>
                        @endforeach
                    @endif -->
                </div>
                <div class="col-md-6 col-xs-12">
                @php
                    $count = count($orgInfos);
                @endphp
                    <!-- <h4 class="col-md-12 col-xs-12">Thông tin {{$Organization->organizationType->name}}</h4> -->
                    <h4 class="col-md-12 col-xs-12">Thông tin chủ tài sản</h4>

                    @for($i = 0; $i < $count; $i++) 
                    <div class="row">
                        <div class="col-md-5 col-xs-5">{{$labelOrgInfos[$i]}}</div>
                        <div class="col-md-7 col-xs-7">
                            @if($orgInfos[$i] == 'area_txt' && $Organization->organization_type_id == OrgType::CSSX)
                                <a target="_blank" style="color:#5aa32a;" href="{{ $Organization->area_url }}">   {{ $Organization->{$orgInfos[$i]} }} </a>
                            @else
                                {{$Organization->{$orgInfos[$i]} ?? ""}}
                            @endif
                        </div>
                    </div>
                    @endfor
                     @if(count($orgCerts) > 0)
                        <h4 class="col-md-12 col-xs-12">Chứng nhận, xác nhận của cơ sở</h4>
                        @foreach ($orgCerts as $ocer)
                            <div class="row">
                                <div class="col-md-5 col-xs-5"><b>{{$ocer->title ?? ""}}</b><br>{{$ocer->description ?? ""}}</div>
                                <div class="col-md-7 col-xs-7 ">
                                    @if(isset($ocer->image))
                                    <a href="{{url("/image/640/600/".$ocer->image)}}" class="fancybox">
                                        <img src="{{url("/image/150/150/".$ocer->image)}}">
                                    </a>
                                    @endif
                                </div>
                            </div>
                        @endforeach
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script>
    $(document).ready(function() {
        $('.fancybox').fancybox();
    });
</script>
@endpush