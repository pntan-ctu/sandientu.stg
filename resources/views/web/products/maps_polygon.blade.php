<link rel="stylesheet" href="https://unpkg.com/leaflet@1.6.0/dist/leaflet.css"
   integrity="sha512-xwE/Az9zrjBIphAcBb3F6JVqxf46+CDLwfLMHloNu6KEQCAWi6HcDUbeOfBIptF7tcCzusKFjFw2yuvEpDL9wQ=="
   crossorigin=""/>
   <script src="https://unpkg.com/leaflet@1.6.0/dist/leaflet.js"
   integrity="sha512-gZwIG9x3wUXg2hdXF6+rVkLF/0Vi9U8D2Ntg4Ga5I5BZpVkVxlJWbSQtXPSiUTtC0TjtGOmxa1AJPuV0CPthew=="
   crossorigin=""></script>

<script src="http://code.jquery.com/jquery-2.1.4.min.js"></script>
<style type="text/css">
#map {
    width: 100%;
    height:500px;
}
</style>
<div class='sp-ban-chay'>
    <div class="title-bc"><a>Định vị vị trí</a></div>
    <div id="map" class="list-sp-lien-quan row">
    </div>
    </div>
</div> 

<script language="javascript">      
        <?php if($Product->it5_maxa == '' || $Product->it5_soto == '' || $Product->it5_sothua =='') {?>
      
             var map = new L.Map('map');    

               L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://www.example.com/">Example</a>',
                    maxNativeZoom:19,
                    maxZoom:25
                }).addTo(map);

                  var center = new L.LatLng(10.360122352164796,106.36286705732346); 
                 map.setView(center, 17);
            
          <?php } else {?>
          var maxa = <?php echo $Product->it5_maxa; ?>;
          var soto = <?php echo $Product->it5_soto; ?>;
          var sothua=<?php echo $Product->it5_sothua; ?>;
          var token = null;
          fetch('https://tiengianglis.vbgis.vn/api/get-arcgis-token?username=tgg_lis&password=Vnptlis@2015', {
            method: 'post'
          }).then(function(response) {
            return response.json();
          }).then(function(data) {
           fetch('https://tigis.vbgis.vn/arcgis/rest/services/ViLIS/ViLIS/MapServer/0/query?where=MaXa%3D%27'+maxa+'%27+AND+SoHieuToBanDo+%3D+'+soto+'+AND+SoThuTuThua+%3D+'+sothua+'&text=&objectIds=&time=&geometry=&geometryType=esriGeometryPolygon&inSR=&spatialRel=esriSpatialRelIntersects&relationParam=&outFields=*&returnGeometry=true&returnTrueCurves=true&maxAllowableOffset=&geometryPrecision=&outSR=&returnIdsOnly=false&returnCountOnly=false&orderByFields=&groupByFieldsForStatistics=&outStatistics=&returnZ=false&returnM=false&gdbVersion=&returnDistinctValues=false&resultOffset=&resultRecordCount=&f=geojson&token='+data)
            .then(response => response.json())
            .then(data => {          

                var center = [ data.features[0].geometry.coordinates[0][3][1] , data.features[0].geometry.coordinates[0][3][0] ];
            console.log(center);

                var map = new L.Map('map');    

               L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://www.example.com/">Example</a>',
                    maxNativeZoom:19,
                    maxZoom:25
                }).addTo(map);

                 // var center = new L.LatLng(10.360122352164796,106.36286705732346); 
                 map.setView(center, 17);
                 console.log(data);
                 var layer1 = L.geoJson(data, {
                     style: function (feature) {
                    return { opacity: 10, weight: 1, color:'red' };
                        }
                    }).addTo(map);
                 map.scrollWheelZoom.disable();
                 var DaCapGCN;
                 if(data.features[0].properties.DaCapGCN == 1) DaCapGCN ="Đã cấp"; else DaCapGCN="Chưa cấp";
                 fetch('https://tiengianglis.vbgis.vn/api/get-thongtinthuadat?maXa='+maxa+'&soTo='+soto+'&soThua='+sothua, {
            method: 'post'
          }).then(function(response) {
            return response.json();
          }).then(function(data1) { 
                 layer1.bindPopup(
                  "<h4 style='text-align:center;'>THÔNG TIN THỬA ĐẤT</h4><br>"
                  +"<b>Tên chủ thửa:&nbsp</b>"+data.features[0].properties.TenChu 
                  +"<br> <b>Số CMND:&nbsp</b>"+data1.data.CMND
                  +"<br> <b>Diện tích:&nbsp</b>"+data.features[0].properties.DienTich+"&nbsp m²"
                  +"<br> <b>Địa chỉ:&nbsp</b>"+data.features[0].properties.DiaChi
                  +"<br> <b>Giấy chứng nhận QSDĐ:&nbsp</b>"+ DaCapGCN
                  +"<br> <b>Thông tin quy hoạch:&nbsp</b>"+data1.check +".<br>(Lưu ý: Thông tin này chỉ mang tính chất tham khảo, để biết thông tin chính xác xin liên hệ trực tiếp văn phòng đăng ký đất)"
                  ).openPopup();
               });
            });           
            
          }); 
      <?php } ?>
   </script>
   
    