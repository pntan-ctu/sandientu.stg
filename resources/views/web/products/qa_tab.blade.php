<div class="tab-pane" id="tab{{$contentProduct->count()+2 }}">
    <div class="content pdp-mod-review">
        <div class=" product-rating-overview">
            <div class="mod-title">
                <h2 class="pdp-mod-section-title outer-title">Hỏi và Đáp của: {{$Product->name}}</h2>
            </div>
            <div class="fs-dtrtcmtbox">
                <textarea id="txtNoteQA" name="txtNoteQA" rows="3" class="f-cmttarea fsformsc" placeholder="Vui lòng nhập câu hỏi của bạn!"></textarea>
                <div class="fs-dtrtbots clearfix"> 
                </div>
                <div class="qa-btn-footer">
                    <button id="btn-delete" type="button" class="btn btn-primary" onClick='saveAnswer(0)'>Gửi câu hỏi</button>
                </div>
            </div>
            <div class="mod-reviews">
            @if(isset($qas))
            @foreach($qas as $qa)
            <div class="item">
               <div class="">
                    <img width="30" height="30" @if(isset($qa->user->UserProfile ) && isset($qa->user->UserProfile->avatar)) src="{{url("/image/30/30/".$qa->user->UserProfile->avatar)}}" @else src="{{url('css/images/no-image.png')}}" @endif>
                         <strong class="f-cmname"><!-- --> <!-- -->
                             <a href='{{ isset($qa->user)? $qa->user->getUrlActivity(): "#"}}'>{{isset($qa->user)? $qa->user->name:''}}</a>
                         </strong>
                    <span class="f-cmtime">{{ $qa->created_at->toShortDateString()}}</span>
                </div>
                <div class="content f-cmmain">{{ $qa->content }}</div>
                <p class="f-cmbott" onclick="showDivAnswer({{$qa->id}})"> <span class="answer-comment">Trả lời</span> </p>
                @if($qa->questionAnswers->where('status',1)->count()>0)
                <div class="f-cmt-reply">
                 @foreach( $qa->questionAnswers->where('status',1) as $answers)
                 <div class="f-cmt-ask">
                 <div class="f-cmname">
                     <img width="30" height="30" @if(isset($answers->user->UserProfile ) && isset($answers->user->UserProfile->avatar)) src="{{url("/image/30/30/".$answers->user->UserProfile->avatar)}}" @else src="{{url('css/images/no-image.png')}}" @endif>
                     <a href='{{$answers->user->getUrlActivity()}}'>{{$answers->user->name}}</a>
                     <span class="f-cmtime">{{ $answers->created_at->toShortDateString()}}</span>
                 </div>
                 <div class="f-cmmain">{{$answers->content }}</div>
                 </div>
                @endforeach
                </div>
                @endif
                <div id="commentAnswer{{$qa->id}}" class="fs-dtrtcmtbox commentAnswer" style="display: none;">
                    <textarea id="txtNoteAnswer{{$qa->id}}" name="txtNoteAnswer{{$qa->id}}" rows="3" class="f-cmttarea fsformsc" placeholder="Vui lòng nhập câu trả lời của bạn!"></textarea>
                <div class="fs-dtrtbots clearfix"> 
                </div>
                <div class="qa-btn-footer">
                    <button id="btn-delete" type="button" class="btn btn-primary" onClick='saveAnswer({{$qa->id}})'>Gửi câu trả lời</button>
                </div>
            </div>
            </div>
            @endforeach
            
            {!! $qas->render() !!}
            
            @endif
        </div>	
        </div>
    </div>
</div>
<script>
    function showDivAnswer(id){
        $('#commentAnswer'+id).show();
    }
    function validateQA(){
        if($('#txtNoteQA').val().trim() === ''){
            alert("Bạn chưa nhập câu hỏi");
            $('#txtNoteQA').focus();
            return false;
        }
        return true;
    }
    function validateAnswer(id){
        if($('#txtNoteAnswer'+id).val().trim() === ''){
            alert("Bạn chưa nhập câu trả lời");
            $('#txtNoteAnswer'+id).focus();
            return false;
        }
        return true;
    }
   
    function saveAnswer(questionId){
         var form_data = new FormData();
        if({{getUserId()}} <1){
            alert('Bạn chưa đăng nhập');
            return false;
        }
        if(questionId == 0){
            if(!validateQA())
            {
                return false;
            }
             form_data.append('qaContent', $('#txtNoteQA').val());
        }
        else{
            if(!validateAnswer(questionId))
            {
                return false;
            }
            form_data.append('qaContent', $('#txtNoteAnswer'+questionId).val());
        }
        form_data.append('typeId', {{$Product->id}});
        form_data.append('questionId', questionId);
        $.ajax({
            url: "{{url('qa-product')}}",
            method: 'POST',
            data: form_data,
            contentType: false,
            processData: false,
            headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
        }).done(function (msg) {
            if (msg.message == 'success'){
                if(questionId == 0){
                    toastr.success("Thêm câu hỏi thành công !");
                    $('#txtNoteQA').val('');
                }
                else{
                    toastr.success("Thêm câu trả lời thành công !");
                    $('#txtNoteAnswer'+questionId).val('');
                }
                
            }
        });
    }
</script>