@if($level==0)
    <select class="form-control" name="parent_id">
        <option value="0">-- Danh mục gốc --</option>
        @endif
        @foreach($tree as $item)
            <option value="{{$item->id}}" {{isset($parent_id) && $item->id==$parent_id?"selected":""}} @if($item->children->count() > 0) @endif >
                @if($item->children->count() > 0) @endif @for($i=0;$i<$level;$i++)
                    &nbsp; @endfor {!! '- '.$item->name !!}
            </option>
            @if($item->children->count() > 0)
                @include('components.tree_product_category', array('tree' => $item->children,'level'=>$level+4,'parent_id'=>$parent_id))
            @endif
        @endforeach
        @if($level==0)
    </select>
@endif