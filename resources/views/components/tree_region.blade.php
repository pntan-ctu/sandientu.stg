@if($level==0)
    <select class="form-control" name="region_id">
        @if(isset($root))
            <option value="0">{{$root}}</option>
        @endif
        @endif
        @foreach($tree as $item)
            <option value="{{$item->id}}" {{isset($region_id) && $item->id==$region_id?"selected":""}} @if($item->children != null) @endif >
                @if($item->children != null) @endif @for($i=0;$i<$level;$i++)
                    &nbsp; @endfor {!! '- '.$item->name !!}
            </option>
            @if($item->children != null > 0)
                @include('components.tree_region', array('tree' => $item->children,'level'=>$level+4,'region_id'=>$region_id))
            @endif
        @endforeach
        @if($level==0)
    </select>
@endif