
@foreach($tree as $item)
    <option class="a" level={{$level}} value="{{$item->id}}" path="{{isset($path) ? $path . " / " . $item->name : $item->name}}" {{isset($parent_id) && $item->id==$parent_id?"selected":""}}>{{$item->name}}</option>
    @if(isset($item->children)&&count($item->children)>0)
        @include("components/select_search_item",['tree'=>$item->children,'level'=>$level+1,"path"=>isset($path) ? $path . " / " . $item->name : $item->name,'parent_id'=>$parent_id]);
    @endif
@endforeach