<div class="share-social">
    <span>Chia sẻ tới:</span>
    <a class="social-button"
   href="https://www.facebook.com/sharer/sharer.php?u={{Request::url()}}"
   data-size="large"><i class="fa fa-facebook-official"></i></a>
    <a class="twitter-share-button social-button"
       href="https://twitter.com/intent/tweet?url={{Request::url()}}"
       data-size="large"><i class="fa fa-twitter"></i> </a>
    <div class="zalo-share-button social-button" data-href="{{Request::url()}}" data-oaid="579745863508352884" data-layout="2" data-color="blue" data-customize=false></div>
</div>

@push('scripts')
<script src="https://sp.zalo.me/plugins/sdk.js"></script>
<script>
var popupSize = {
    width: 780,
    height: 550
};
$(document).on('click', '.social-button', function (e) {
    var verticalPos = Math.floor(($(window).width() - popupSize.width) / 2),
            horisontalPos = Math.floor(($(window).height() - popupSize.height) / 2);

    var popup = window.open($(this).prop('href'), 'social',
            'width=' + popupSize.width + ',height=' + popupSize.height +
            ',left=' + verticalPos + ',top=' + horisontalPos +
            ',location=0,menubar=0,toolbar=0,status=0,scrollbars=1,resizable=1');

    if (popup) {
        popup.focus();
        e.preventDefault();
    }

});

</script>
@endpush

