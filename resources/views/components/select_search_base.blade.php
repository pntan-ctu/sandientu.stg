@push('styles')
    <link href="bower_components/select2/dist/css/select2.css" rel="stylesheet"/>
    <style>
        .select2-container--default .select2-selection--single .select2-selection__rendered {
            line-height: 20px;
            padding: 0px;
        }
    </style>
@endpush
{!!  Former::select("$name","$label")->options(['value1' => 'Label 1'])->select("$selected")->addClass('select-search') !!}

@push('scripts')
    <script src="bower_components/select2/dist/js/select2.js"></script>
    <script>

        $(function () {
            $('.select-search').select2({
                width: '100%'
            });
        })
    </script>
@endpush