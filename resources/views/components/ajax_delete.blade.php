<div class="modal modal-danger fade" id="modal-delete" style="display: none;">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span></button>
                <h4 class="modal-title">Thông báo</h4>
            </div>
            <div class="modal-body">
                <p class="message_alert">Bạn chắc chắn muốn xóa? Chọn 'Đồng ý' để xóa</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-outline pull-left" data-dismiss="modal">Thoát</button>
                <button type="button" class="btn btn-outline" onclick='ajax_delete()'>Đồng ý</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<script>
    var url = undefined;
    function onClickDelete(route,message) {
        url = route;
        $(".message_alert").html(message);
        $("#modal-delete").modal("show");
    }

    function ajax_delete() {
        $("#modal-delete").modal("hide");
        if(url===undefined)
            return;
        $.ajax({
            type: 'DELETE',
            dataType: 'json',
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: url,
            success: function (data) {
                alert(data.msg);
            }
        });
    }
</script>

