{{--Truyen tham so--}}
{{--@include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>'-- Danh mục gốc --','parent_id'=>($productCategory?$productCategory->parent_id:null,'name'=>'region_id',
                                'placeholder'=>'Hãy chọn địa phương quản lý'])--}}



@push('styles')
    <style>
        .icon {
            margin-right: 10px !important;
            z-index: 9999999 !important;
        }

        .icon_class_0 {
            color: #3300ff;
            font-weight: bold;
        }

        .icon_class_1 {
            color: #969696;
            font-weight: bold;
        }

        .icon_class_2 {
            color: #249878;
            font-weight: bold;
        }

        .icon_class_3 {
            color: #bcbf07;
            font-weight: bold;
        }
    </style>
@endpush

<select class="js-example-basic-single col-lg-12" id="{{isset($name)?$name:'product_category_id'}}"
        name="{{isset($name)?$name:'product_category_id'}}">
    @if(isset($placeholder)&& !isset($parent_id))
        <option value=""></option>
    @endif
    @if(isset($root))
        <option level=0 value='0' path="{{$root}}"> {{$root}}</option>
    @endif
    @include("components/select_search_item",['tree'=>$tree,"level"=>$level,"path"=>$path,'parent_id'=>$parent_id]);
</select>
@if($errors->has(isset($name)?"$name":'parent_id'))
    <span class="help-block">{{$errors->first(isset($name)?"$name":'parent_id')}}</span>
@endif

@push('scripts')
    <script>
        $(document).ready(function () {
            $('{{isset($name)?"#".$name:'.js-example-basic-single'}}').select2({
                width: '100%',
                placeholder: "{{isset($placeholder)?$placeholder :''}}",
                templateResult: function (data) {
                    // We only really care if there is an element to pull classes from
                    if (!data.element) {
                        return data.text;
                    }

                    var $element = $(data.element);
                    var level = $element.attr('level');
                    var pad = level * 20;
                    var icon_class = "icon_class";
                    if (level == 0)
                        icon_class = "icon_class_0";
                    if (level == 1)
                        icon_class = "icon_class_1";
                    if (level == 2)
                        icon_class = "icon_class_2";
                    if (level == 3)
                        icon_class = "icon_class_3";

                    var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right ' + icon_class + ' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                    var $wrapper = $(wraptext);
                    return $wrapper;
                },
                templateSelection: function (state) {
                    if (!state.id) {
                        return state.text;
                    }
                    return $(state.element).attr('path');
                }
            });
        });

        // $('.js-example-basic-single').on('select2:open', function () {
        //     console.log(1);
        //     $('.icon').on('click', function(e) {
        //         e.preventDefault();
        //         e.stopPropagation();
        //         console.log('clicked');
        //     });
        // });

        // $('.js-example-basic-single').on('select2:select', function (data,options) {
        //     var target;
        //     if(data!=null){
        //         target = $(data.target);
        //     }
        //     console.log(target);
        //     if(target && target.hasClass('icon')){
        //         alert('icon');
        //     }
        // });
    </script>
@endpush

