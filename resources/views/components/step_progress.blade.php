{{--complete để hoàn thành--}}
<div class="container-step">
    <div class="col-xs-12 bs-wizard" style="border-bottom:0;">
        <div class="col-xs-4 bs-wizard-step {{$step==1&&!isset($product_id)?"active":"complete"}}" id="step1">
            <div class="text-center bs-wizard-stepnum"></div>
            <div class="progress">
                <div class="progress-bar"></div>
            </div>
            <button class="bs-wizard-dot" step="1"><span class="num_step">1</span></button>
            <div class="bs-wizard-info text-center">Thông tin chính</div>
        </div>
        <div class="col-xs-4 bs-wizard-step {{$step==2||isset($product_id)?"complete":"disabled"}}" id="step2"><!-- active -->
            <div class="text-center bs-wizard-stepnum"></div>
            <div class="progress">
                <div class="progress-bar"></div>
            </div>
            <button class="bs-wizard-dot" step="2"><span class="num_step">2</span></button>
            <div class="bs-wizard-info text-center">Thông tin chi tiết</div>
        </div>
        <div class="col-xs-4 bs-wizard-step {{$step==3||isset($product_id)?"active":"disabled"}}" id="step3"><!-- active -->
            <div class="text-center bs-wizard-stepnum"></div>
            <div class="progress">
                <div class="progress-bar"></div>
            </div>
            <button class="bs-wizard-dot" step="3"><span class="num_step">3</span></button>
            <div class="bs-wizard-info text-center">Giấy tờ pháp lý</div>
        </div>
    </div>
</div>

@push('styles')
    <style>
        /*step*/
        .bs-wizard {
            border-bottom: solid 1px #e0e0e0;
            padding: 0 0 20px 0;
        }

        .bs-wizard > .bs-wizard-step {
            padding: 0;
            position: relative;
        }

        .bs-wizard > .bs-wizard-step + .bs-wizard-step {
        }

        .bs-wizard > .bs-wizard-step .bs-wizard-stepnum {
            color: #595959;
            font-size: 16px;
            margin-bottom: 5px;
        }

        .bs-wizard > .bs-wizard-step .bs-wizard-info {
            font-weight: 600;
            color: #717171;
            font-size: 14px;
        }

        .bs-wizard > .bs-wizard-step > .bs-wizard-dot {
            position: absolute;
            width: 30px;
            height: 30px;
            display: block;
            background: #7acf42;
            top: 23px;
            left: 50%;
            margin-top: -15px;
            margin-left: -15px;
            border-radius: 50%;
        }

        .bs-wizard > .bs-wizard-step > .bs-wizard-dot:after {
            content: ' ';
            width: 20px;
            height: 20px;
            background: #7acf42;
            border-radius: 50px;
            position: absolute;
            top: 4px;
            left: 4px;
        }

        .bs-wizard > .bs-wizard-step > .progress {
            position: relative;
            border-radius: 0px;
            height: 8px;
            box-shadow: none;
            margin: 20px 0;
        }

        .bs-wizard > .bs-wizard-step > .progress > .progress-bar {
            width: 0px;
            box-shadow: none;
            background: #fbe8aa;
        }

        .bs-wizard > .bs-wizard-step.complete > .progress > .progress-bar {
            width: 100%;
        }

        .bs-wizard > .bs-wizard-step.active > .progress > .progress-bar {
            width: 50%;
        }

        .bs-wizard > .bs-wizard-step:first-child.active > .progress > .progress-bar {
            width: 0%;
        }

        .bs-wizard > .bs-wizard-step:last-child.active > .progress > .progress-bar {
            width: 100%;
        }

        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot {
            background-color: #7acf42;
        }

        .bs-wizard > .bs-wizard-step.disabled > .bs-wizard-dot:after {
            opacity: 0;
        }

        .bs-wizard > .bs-wizard-step:first-child > .progress {
            left: 50%;
            width: 50%;
        }

        .bs-wizard > .bs-wizard-step:last-child > .progress {
            width: 50%;
        }

        .bs-wizard > .bs-wizard-step.disabled a.bs-wizard-dot {
            pointer-events: none;
        }

        .num_step {
            top: 4px;
            left: 10px;
            position: absolute;
            color: white;
            z-index: 999;
            font-weight: 600;
        }

        /*end step*/
    </style>
@endpush