{{--Truyen tham so--}}
{{--@include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>'-- Danh mục gốc --','parent_id'=>($productCategory?$productCategory->parent_id:null,'name'=>'region_id',
                                'placeholder'=>'Hãy chọn địa phương quản lý'])--}}



@push('styles')
    <style>
        .icon {
            margin-right: 10px !important;
            z-index: 9999999 !important;
        }

        .icon_class_0 {
            color: #3300ff;
            font-weight: bold;
        }

        .icon_class_1 {
            color: #969696;
            font-weight: bold;
        }

        .icon_class_2 {
            color: #249878;
            font-weight: bold;
        }

        .icon_class_3 {
            color: #bcbf07;
            font-weight: bold;
        }
    </style>
@endpush

<select class="js-example-basic-single col-lg-12" id="{{isset($name)?$name:'parent_id'}}"
        name="{{isset($name)?$name:'parent_id'}}">
    <option value="">--Chọn Huyện/TP/TX--</option>
    <option value="819">Cái Bè</option>
    <option value="820">Cai Lậy</option>
    <option value="821">Châu Thành</option>
    <option value="822">Chợ Gạo</option>
    <option value="824">Gò Công Đông</option>
    <option value="823">Gò Công Tây</option>
    <option value="815">Mỹ Tho</option>
    <option value="825">Tân Phú Đông</option>
    <option value="818">Tân Phước</option>
    <option value="817">TX Cai Lậy</option>
    <option value="816">TX Gò Công</option>
</select>

@endpush

