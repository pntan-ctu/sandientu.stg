@if (session('msg-success'))
	<script>
	toastr.success('{{ session('msg-success') }}')
	</script>
@endif
@if (session('msg-error'))
	<script>
	toastr.error('{{ session('msg-error') }}')
	</script>
@endif