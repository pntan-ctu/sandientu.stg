<style>
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    #cke_1_contents{
        height: 200px !important;
    }
</style>
<?php Former::populate($area); ?>
@if($area)
    {!! Former::open(route('area.update', $area->id), 'put') !!}
@else
    {!! Former::open(route('area.store')) !!}
@endif
{!! Former::text('name', 'Tên vùng:') !!}
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Địa phương: </label>
    <div class="col-lg-10 col-sm-8 tree-search">
        @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>'-- Danh mục gốc --','parent_id'=>($area?$area->region_id:null),'name'=>'region_id'])
        @if(isset($none_create) && $none_create == 1)
            <input type="hidden" id="region" name="region" >
        @elseif(isset($none_create) && $none_create == 2)
            <input type="hidden" id="region" name="region" value="{{$area->region_id}}">
        @endif
    </div>
</div>
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Giới thiệu : </label>
    <div class="col-lg-10 col-sm-8">
        <textarea class="ckeditor" name="instroduction" id="my-editor">@if(isset($area->instroduction)) {!! html_entity_decode($area->instroduction) !!} @endif</textarea>
    </div>
</div>
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Ảnh bản đồ : </label>
    <div class="col-lg-10 col-sm-8">
        <img id="logo-img" title="Chọn ảnh bản đồ: " onclick="document.getElementById('add-new-logo').click();"
            @if(isset($area->map) && $area->map)
                src="{{ url('image/200/200/'.$area->map) }}"
            @else
                src="{{ url('css/images/no-image.png') }}"
            @endif
        />
        <input type="file" style="display: none" id="add-new-logo" name="map" accept="image/*" onchange="addNewLogo(this)"/>
    </div>
</div>
{!! Former::close() !!}
<script type="text/javascript">
    var addNewLogo = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Hiển thị ảnh vừa mới upload lên
                $('#logo-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            //submit form để upload ảnh
        }
    }
</script>
<script>
    $('select[name="region_id"]').change(function(){
           $('#region').val(this.value); 
        });
</script>