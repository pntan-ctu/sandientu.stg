@extends('layouts.app')

@section('title')
    Quản lý vùng sản xuất
@endsection

@section('decription')
@endsection

@push('styles')
<link href="bower_components/select2/dist/css/select2.css" rel="stylesheet"/>
<style>
    .icon {
        margin-right: 5px;
        z-index: 9999999;
    }

    .select2-container--default .select2-selection--single .select2-selection__rendered {
        line-height: 20px;
        padding: 0px;
    }
    .tree-search>span{
        width:100% !important;
    }
</style>
@endpush
@section('content')
    <div class="box" id="box-area">
        <div class="box-header">
            <form method="POST" id="search-form" class="form-inline pull-left" role="form">
                <div class="form-group">
                    <label for="keyword">Từ khoá</label>
                    <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
                </div>
                {{--<div class="form-group">
                    <label for="active">Trạng thái</label>
                    <select name="active" class="form-control">
                        <option value="-1">Tất cả</option>
                        <option value="0">Không kích hoạt</option>
                        <option value="1">Đang kích hoạt</option>
                    </select>
                </div>--}}

                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </form>
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                <colgroup>
                    <col width="30%">
                    <col width="20%">
                    <col width="20%">
                    <col width="20%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th class="all">Tên vùng sản xuất</th>
                    <th>Địa chỉ</th>
                    <th>Cơ quan quản lý</th>
                    <th>Bản đồ</th>
                    <th class="all"></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
<script src="js/sdc-crud.js"></script>
<script src="js/manage/area.js"></script>
<script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
<script src="bower_components/select2/dist/js/select2.js"></script>
<script>
    $(function () {
        sdcApp.fixCKEditorModal();
        Area('{{ route('area.index') }}');
    })
</script>
@endpush