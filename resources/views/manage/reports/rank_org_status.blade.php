@extends('layouts.app')

@section('title')
    Thống kê số lượng cơ sở
@endsection

@section('decription')

@endsection

@push('styles')
    <style>
        #district{padding: 4px;margin-left: 5px;}
        .box-header span{margin: 0 5px 0 25px}
        .box-header button {
            border: none;
            padding: 5px 10px;
            border-radius: 4px;
            background: #3c8dbc;
            color: white;
            margin-left: 10px;
        }
    </style>
@endpush

@section('content')
    <div class="box" id="box-users">
        <div class="box-header" style="border-bottom: 1px solid #80808038">
            @if(count($Districts) > 0)
            <span>Chọn huyện:</span>
            <select id="district">
                <option value="0">-- Toàn tỉnh --</option>
                @foreach($Districts as $dis)
                <option value="{{$dis->id}}">{{$dis->name}}</option>
                @endforeach
            </select>
            @endif
            <div style="float: right">
                <button id="btnExport"><i class="fa fa-file-excel-o"></i> Xuất Excel</button>
            </div>
        </div>
        <div class="box-body" style="height: 375px">
            <iframe id="reportViewer" width="100%" height="100%" frameborder="no"></iframe>
        </div>
    </div>
@endsection
@push('scripts')
    <script>
        function getReport(docType) {
            var huyenId = $("#district").val();
            var urlReport = 'manage/report-org-status-view?doc_type='+docType+'&huyen_id='+huyenId;
            sdcApp.blockUI();
            $('#reportViewer').attr('src', urlReport);
            sdcApp.unblockUI();
        }

        getReport("html");

        $("#district").change(function () {
            getReport("html");
        });

        $("#btnExport").click(function () {
            getReport("excel");
        });
    </script>
@endpush