@extends('layouts.app')

@section('title')
    Thống kê cơ sở,sản phẩm bị phản ánh vi phạm
@endsection

@section('decription')

@endsection

@push('styles')
    <style>
        .report_type{padding: 4px;margin-left: 5px;}
        #from_date, #to_date{height: 30px; width: 100px; padding-left: 5px}
        .day, .prev, .next{cursor: pointer}
        .box-header span{margin: 0 5px 0 25px}
        .box-header button {
            border: none;
            padding: 5px 10px;
            border-radius: 4px;
            background: #3c8dbc;
            color: white;
            margin-left: 10px;
        }
    </style>
@endpush

@section('content')
    <div class="box" id="box-users">
        <div class="box-header" style="border-bottom: 1px solid #80808038">
            Thống kê phản ánh vi phạm theo:
            <select class="report_type">
                <option value="org">Cơ sở</option>
                <option value="product">Sản phẩm</option>
            </select>
            <span style="display: none;">Từ ngày: </span>
            <input type="text" class="datepicker" id="from_date" style="display: none;" />
            <span style="display: none;">đến ngày: </span>
            <input type="text" class="datepicker" id="to_date" style="display: none;" />
            <div style="float: right">
                <button id="btnStatistic"><i class="fa fa-dribbble"></i> Thống kê</button>
                <button id="btnExport"><i class="fa fa-file-excel-o"></i> Xuất Excel</button>
            </div>
        </div>
        <div class="box-body" style="height: 375px">
            <iframe id="reportViewer" width="100%" height="100%" frameborder="no"></iframe>
        </div>
    </div>
@endsection
@push('scripts')
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script>
        $('.datepicker').inputmask('date');
        $('.datepicker').datepicker({
            language: 'vi',
            autoclose: true
        });
        //$('#to_date').datepicker('setDate', new Date());
        var date = new Date();
        date.setMonth(date.getMonth(), 1);
        //$('#from_date').datepicker('setDate', date);

        function getReport(reportType, docType) {
            var from_date = $('#from_date').val();
            var to_date = $('#to_date').val();

            var urlReport = 'manage/report-infringements-org-product-view?doc_type='+docType+'&report_type='+reportType;
                urlReport += '&from_date='+from_date+'&to_date='+to_date;
            sdcApp.blockUI();
            $('#reportViewer').attr('src', urlReport);
            sdcApp.unblockUI();
        }

        getReport("org", "html");
        $('.box-header span').show();
        $('.box-header input').show();

        $(".report_type").change(function () {
            var rType = this.value;
            if(rType == "product") {
                $('.box-header span').show();
                $('.box-header input').show();
            }
            if(rType == "product"){
                $('.box-header span').show();
                $('.box-header input').show();
            }
            getReport(rType, "html");
        });

        $("#btnStatistic").click(function () {
            var rType = $(".report_type").val();
            getReport(rType, "html");
        });

        $("#btnExport").click(function () {
            var rType = $(".report_type").val();
            getReport(rType, "excel");
        });
    </script>
@endpush