<style type="text/css">
    sup{
        color: #EA3838 !important;
    }
    #cke_1_contents{
        height: 200px !important;
    }
</style>
<?php Former::populate($commercialCenter); ?>
@if($commercialCenter)
    {!! Former::open(route('commercial-center.update', $commercialCenter->id), 'put') !!}
@else
    {!! Former::open(route('commercial-center.store')) !!}
@endif
        {!! Former::text('name', 'Tên địa điểm KD:')->required() !!}
        <div class="form-group">
            <label for="name" class="control-label col-lg-2 col-sm-4">Tỉnh, huyện, xã:<sup>*</sup> </label>
            <div class="col-lg-10 col-sm-8 tree-search">
                @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>'-- Chọn địa chỉ --','parent_id'=>(isset($commercialCenter)?$commercialCenter->region_id:null),'name'=>'region_id'])
                @if(isset($none_create) && $none_create == 1)
                    <input type="hidden" id="region" name="region" >
                @elseif(isset($none_create) && $none_create == 2)
                    <input type="hidden" id="region" name="region" value="{{$commercialCenter->region_id}}">
                @endif
            </div>
        </div>
        {!! Former::text('address', 'Thôn, phố, SN:')->required() !!}
        {!! Former::textarea('introduction')->id('txtEditor')->label('Giới thiệu:')->rows(6) !!}

        @if(isset($none_create) && $none_create == 1)
        <div class="form-group">
            <label for="name" class="control-label col-lg-2 col-sm-4">Tọa độ GPS : </label>
            <div class="col-lg-10 col-sm-8 ">
                <label for="name" class="control-label col-lg-2 col-sm-4">Latitude : </label>
                <div class="col-lg-3 col-sm-4 ">
                    <input class="form-control def-txt-input" id="map_lat" placeholder="19.808241" type="text" name="map_lat">
                </div>
                
                <label for="name" class="control-label col-lg-2 col-sm-4">Longitude : </label>
                <div class="col-lg-3 col-sm-4 ">
                    <input class="form-control def-txt-input" id="map_long" placeholder="105.777592" type="text" name="map_long">
                </div>
                 <a onclick="getLocation()">Vị trí hiện tại ?</a>
            </div>
        </div>
        @elseif(isset($none_create) && $none_create == 2)
        <div class="form-group">
            <label for="name" class="control-label col-lg-2 col-sm-4">Tọa độ GPS : </label>
            <div class="col-lg-10 col-sm-8 ">
                <label for="name" class="control-label col-lg-2 col-sm-4">Latitude : </label>
                <div class="col-lg-3 col-sm-4 ">
                    <input class="form-control def-txt-input" value="{{$commercialCenter->map_lat}}" id="map_lat" placeholder="19.808241" type="text" name="map_lat">
                </div>
                
                <label for="name" class="control-label col-lg-2 col-sm-4">Longitude : </label>
                <div class="col-lg-3 col-sm-4 ">
                    <input class="form-control def-txt-input" value="{{$commercialCenter->map_long}}" id="map_long" placeholder="105.777592" type="text" name="map_long">
                </div>
                 <a onclick="getLocation()">Vị trí hiện tại ?</a>
            </div>
        </div>
        @endif
        {!! Former::close() !!}
<script>
    $('select[name="region_id"]').change(function(){
        $('#region').val(this.value); 
    });
</script>
<script src="plugins/easyui/jquery.easyui.min.js"></script>
<script src="plugins/dropdown-tree/resources/dropdowntree.js"></script>
<script>
    var $inputs = $(".def-txt-input");
    $inputs.on("paste", function() {
        var $this = $(this);
        $this.val("");
        $this.one("input.fromPaste", function(){
            $currentInputBox = $(this);
            var pastedValue = $currentInputBox.val();
            pasteValues(pastedValue);
        });
    });
    function pasteValues(element) {
        var values = element.split(",");
        if (values.length == 2)
        {
            $('#map_lat').val(values[0]);
            $('#map_long').val(values[1]);
        }
    };
</script>  
<script>
    var x = document.getElementById("demo");
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else { 
            x.innerHTML = "Geolocation is not supported by this browser.";
        }
    }

    function showPosition(position) {
        $('#map_lat').val(position.coords.latitude.toFixed(6));
        $('#map_long').val(position.coords.longitude.toFixed(6));
    }
</script>