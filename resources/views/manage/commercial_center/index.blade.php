@extends('layouts.app')

@section('title')
    Quản lý địa điểm kinh doanh
@endsection

@section('decription')
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                <colgroup>
                    <col width="20%">
                    <col width="20%">
                    <col width="25%">
                    <col width="20%">
                    <col width="15%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Tên địa điểm kinh doanh</th>
                    <th>Địa chỉ</th>
                    <th>Giới thiệu</th>
                    <th>Ngày tạo</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/commercial_center.js"></script>
    <script>
        CommercialCenter('{{ route('commercial-center.index') }}');
    </script>
@endpush