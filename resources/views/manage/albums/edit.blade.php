@extends('layouts.app')

@section('title')
Quản lý  Albums
@endsection

@section('decription')

@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title">Sửa Album</h3>
            </div>
            <!-- /.box-header -->

            <!-- / form -->
            <?= Former::vertical_open_for_files()->method('PUT')->action(route('albums.update', ['id' => $album->id])) ?>
            <div class="box-body">
                <div class="col-md-offset-2 col-md-8">
                    <div class="form-group">
                        <label> Tên nhóm album :<sup>*</sup></label>
                        <select class="form-control required" name="albumGroup">
                            @foreach ($album_group as $value)
                            @if($value->active == 1)
                            {
                                <option 
                                    @if($album->group_id == $value->id)
                                        {{'selected'}}
                                    @endif
                                    value="{{$value->id}}">{{$value->name }}
                                </option>
                            }
                            @endif
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group required">
                        <?= Former::text('title')->label('Tên Album : ')->autofocus()->required()->value($album->title) ?>
                        <?= Former::text('description')->label('Nội dung : ')->rows(4)->value($album->description) ?>
                        <div class="form-group">
                            <label class ="control-label col-lg-2 col-sm-4">Ảnh đại diện :</label>
                            <div class="col-lg-10 col-sm-8">
                                         <img id="logo-img" title="Chọn ảnh đại diện" onclick="document.getElementById('add-new-logo').click();"
                                        @if(isset($album->img_path) && $album->img_path)
                                            src="{{ url('image/200/200/'.$album->img_path) }}"
                                        @else
                                            src="{{ asset('storage/image/no-image.png') }}"
                                        @endif
                                />
                                <input type="file" style="display: none" id="add-new-logo" name="avatar" accept="image/*" onchange="addNewLogo(this)"/>
                            </div>    
                        </div>
                        <div style="padding-top:40px;">
                        {!! Former::text('no','Thứ tự :')->required()->value($album->no) !!}
                        <?= Former::checkbox('active')->label('Kích hoạt : ')->inline()->check($album->active == 1 ? true : false) ?>
                        </div>
                    </div>



                </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
                <div class="text-center">
                    <button type="submit" class="btn btn-success">
                        <i class="fa fa-save"></i>
                        Cập nhật
                    </button>
                </div>
            </div>
            <?= Former::close() ?>
            <!-- /end form -->
        </div>
    </div>
</div>
<style type="text/css">
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    sup{
        color: #EA3838 !important;
    }
</style>
<script type="text/javascript">
    var addNewLogo = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Hiển thị ảnh vừa mới upload lên
                $('#logo-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            //submit form để upload ảnh
        }
    }
</script>
@endsection
