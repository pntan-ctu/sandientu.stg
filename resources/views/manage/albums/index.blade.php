@extends('layouts.app')

@section('title')
Quản lý  Albums
@endsection

@section('decription')
@endsection

@section('content')
<div class="modal fade" id="modal-upload" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-book"></i> Thêm ảnh vào Album </h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <form action="/multiuploads" id="upload_form" method="post" enctype="multipart/form-data">

                                    <input name="albumId" type="hidden" id="albumId" />
                                    <div class="form-group">
                                        <label class="control-label">Thêm ảnh: </label>
                                        <div class="btn btn-default btn-file">
                                            <i class="fa fa-picture-o"></i> Chọn ảnh
                                            <!--<input type="file" id="avatar" name="avatar" onChange='setTextFilename($(this).val());'>-->
                                            <input type="file" class="form-control" name="photos[]" multiple onChange='setTextFilename($(this)[0].files);' />
                                        </div>

                                    </div>
 <!--&nbsp; <span class="label label-info" id="upload-file-info"></span>-->
                                </form>
                                <div id="img-table" class="img-table">
                                    <table class="table table-bordered table-striped table-hover" id="datatableImg" width="100%">
                                         <colgroup>
                                            <col width="20%">
                                            <col width="80%">
                                            <col width="20%">
                                        </colgroup>
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th>STT</th>
                                                <th>Ảnh</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Hủy</button>
                <button id="btn-delete" type="button" class="btn btn-primary" onClick='albums.upload()'> Upload Ảnh</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box" id="album-box">
            <div class="box-header">
                {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
                {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
                {!! Former::select('active', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chưa hiển thị', '1' => 'Đang hiển thị']) !!}
                {!! Former::primary_submit('Tìm kiếm') !!}
                {!! Former::close() !!}
                <div class="right-button">
                    <a type="button" class="btn Normal btn-success" href="{{'manage/albums/create'}}">
                        <i class="fa fa-plus"></i>
                        Thêm mới
                    </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                    <thead>
                        <tr role="row" class="heading">
                            <th class="all">Ảnh đại diện</th>
                            <th>Nhóm Album</th>
                            <th class="all">Tên Album</th>
                            <th>Đường dẫn</th>
                            <th>Nội dung</th>
                            <th>Người tạo</th>
                            <th>Thứ tự</th>
                            <th>Hiển thị trên website</th>
                            <th class="all"></th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
</div>

@endsection
@push('scripts')
<script>
    var tableLenghs=0;
    function validateImages(files) {
        var valid_extensions = /(\.jpg|\.jpeg|\.gif|\.png|\.jpe|\.jfif|\.tif|\.tiff|\.bmp|\.dib)$/i;
        if (files.length > 0){
            for (var i = 0; i < files.length; i++) {
                if (!valid_extensions.test(files[i].name.toLowerCase())) {
                  toastr.error('Không đúng định dạng ảnh: '+files[i].name);
                  return false;
                  }
            }
        }
        else{
            toastr.error('Bạn chưa chọn file ảnh!');
            return false;
        }
            
        return true;
    }
    function setTextFilename(files) {
       if(!validateImages(files)){
           return false;
       }
        for (var i = 0; i < files.length; i++) {
            $('#img-table tbody').append("");
           // table.rows.add($(table_rows)).draw();
            $(".img-table tbody").append("<tr><td>"+(tableLenghs+i+1)+" </td><td><img width='100' height='100' src='" + URL.createObjectURL(files[i]) + "'></td><td></td></tr>");
        }
    }
    function closeModal() {
    oTableImg.draw();
    $('#modal-upload').modal('hide');
    }
    var albums = function () {
        var box;
        var searchForm;
        var tableId;
        var oTable;
        var albumId = 0;
        var tableIdImg;
        var oTableImg;
//        function closeModal() {
//            oTableImg.draw();
//            $('#modal-upload').modal('hide');
//        }
        function upload() {
            //var form_data = new FormData($("#upload_form")[0]);
            //form_data.append('id', $('#albumId').val());
            var formData = sdcApp.getFormDataAndType($("#upload_form"));
            //formData.append('id', $('#albumId').val());
            $.ajax({
                url: 'manage/multiuploadsimages',
                method: 'POST',
                data: formData.data,
                contentType: formData.contentType,
                // dataType: 'json',
                processData: false
            }).done(function (data) {

                $('#modal-upload').modal('hide');

            });

        }
        function setTextFilename2(files) {
            for (var i = 0; i < files.length; i++) {
                $('#img-table tbody').append("");
               // var count = oTableImg.count()
                console.log(oTableImg)
               // oTableImg.rows.add("<tr><td>" + (i + 1) + "</td><td><img width='100' height='100' src='" + URL.createObjectURL(files[i]) + "'></td></tr>").draw();
                $(".img-table tbody").append("<tr><td>" + (count + 1) + "</td><td><img width='100' height='100' src='" + URL.createObjectURL(files[i]) + "'></td></tr>");
            }

        }
        return {
            init: function (baseUrl, url) {
                box = $('#album-box');
                searchForm = $('#search-form');
                tableId = '#datatable';
                oTable = $(tableId).DataTable({
                    "rowId": 'id',
                    "ajax": {
                        "url": baseUrl + '/data',
                        "data": function (d) {
                            d.keyword = searchForm.find('input[name=keyword]').val();
                            d.active = searchForm.find('select[name=active]').val();
                        }
                    },
                    "columns": [
                        {"data": 'img_path',"orderable": false,
                            "render": function (value) {
                                               return '<img src=" '+"image"+"/"+"150"+"/"+"150"+"/"+value+' " style="width:100%">';
                            },
                            "width": "10%"
                        },
                        {"data": 'group_name', "width": "10%"},
                        {"data": 'title', "width": "10%"},
                        {"data": 'path',"orderable": false,"width": "10%"},
                        {"data": 'description',"orderable": false, "width": "10%"},
                        {"data": 'user_name',"orderable": false, "width": "10%"},
                        {"data": 'no', "width": "10%"},
                        {"data": 'active',"orderable": false,
                            "class": "text-center",
                            "render": function (value, type, data) {
                                if(value == 1)
                                    return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                                else
                                    return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                            },
                            "width": "10%"
                        },
                        {
                            "data": 'id',
                            "orderable": false,
                            "class": 'text-center',
                            "render": function (value, type, data) {
                                return '<button class="btn btn-sm  btn-upload" data-toggle="modal" ><i class="fa fa-upload"></i></button>'
                                        + sdcApp.renderCommonTblActionCell(value, baseUrl);
                            },
                            "width": "10%"
                        },
                        {"data": 'id', "visible": false}
                    ],
                    "order": [4, 'desc']
                });
        //table detail album
                var oTableImg = null;
                $('#datatable tbody').on('click', '.btn-upload', function (e) {
                    e.preventDefault();
                    var btn = $(this);
                    albumId = oTable.row(btn.closest('tr')).id();
                    $('#albumId').val(albumId);
                    $(".img-table tbody").html("");
                    tableIdImg = '#datatableImg';
                    var dataUrl = 'manage/getImagesAlbum/' + albumId;
                    if (oTableImg === null){
                        oTableImg = $(tableIdImg).DataTable({
                            "rowId": 'id',
                            "ajax": dataUrl,
                            "columns": [
                                {
                                    "data": 'id',
                                    "render": function (value, type, data, meta) {
                                        tableLenghs = meta.row+1;
                                        return meta.row+1;
                                    }
                                },
                                {"data": 'img_path',"orderable": false,
                                    "render": function (value) {
                                                       return '<img src=" '+"image"+"/"+"150"+"/"+"150"+"/"+value+' ">';
                                    }  },
                                {
                                    "data": 'id',
                                    "orderable": false,
                                    "class": 'text-center',
                                    "render": function (value, type, data) {
                                       // return '<a href="manage/deleteImagesAlbum' + '/' + value + '" class="btn btn-link btn-xs btn-del"><i class="glyphicon glyphicon-trash"></i></a>'
                                        return '<a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>';
                                    }
                                },
                                //{"data": 'id', "visible": false}
                            ]
                            //"order": [4, 'desc']
                        });
                        sdcApp.bindTblDelEvent(tableIdImg, oTableImg, "{{ route('album-detail.index') }}", 0);
                    }
                    else{
                        oTableImg.ajax.url(dataUrl).load();
                    }
                    $('#modal-upload').modal();
                });
                sdcApp.bindTblDelEvent(tableId, oTable,baseUrl, 0);

                searchForm.on('submit', function (e) {
                    e.preventDefault();
                    oTable.draw();
                });

            },
            upload: upload,
           // setTextFilename2: setTextFilename2,
            //closeModal: closeModal,
        };
    }();

    $(function () {
        albums.init('{{ route('albums.index') }}','{{ url('storage') }}' )
    });


</script>
@endpush
