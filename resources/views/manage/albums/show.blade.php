@extends('layouts.app')

@section('content')

<div class="box">
    <div class="box-body">
        <div class="col-md-offset-2 col-md-8">

            <?= Former::text('title')->label('Tên danh mục : ')->value($alb->title)->readonly() ?>
            <?= Former::textarea('description')->label('Miêu tả : ')->rows(4)->value($alb->description)->readonly() ?>
            <?= Former::number('no')->min(0)->label('Số thứ tự : ')->value($alb->no)->readonly() ?>
            <?= Former::checkbox('active')->label('Kích hoạt : ')->inline()->check($alb->active == 1 ? true : false)->readonly() ?>
            <div class="form-group">
                  @if($alb->img_path)
                <label class="custom-file-label" for="customFile">Ảnh đại diện: </label>
                <image src="{{url($alb->img_path)}}" class="img-responsive" style="display: inline;" />
                @endif
            </div>
        </div>
    </div>
</div><style type="text/css">
    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        background-color: #fff;
        opacity: 1;
    }
</style>

@stop
