@extends('layouts.app')
@section('title')
    Quản lý danh mục sản phẩm
@endsection

@section('decription')
@endsection

@push('styles')
    <style>
        .icon {
            margin-right: 10px;
            z-index: 9999999;
        }

        .tree-search > span {
            width: 100% !important;
        }

        .icon_class_0 {
            color: #3300ff;
            font-weight: bold;
        }

        .icon_class_1 {
            color: #969696;
            font-weight: bold;
        }

        .icon_class_2 {
            color: #249878;
            font-weight: bold;
        }

        .icon_class_3 {
            color: #bcbf07;
            font-weight: bold;
        }

        #keyword {
            width: 300px;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="plugins/easyui/icon.css">
    <link rel="stylesheet" type="text/css" href="plugins/easyui/easyui.css">
@endpush
@section('content')
    <div class="box" id="box-product-category">
        <div class="box-header">
            <div id="search-form" class="form-inline pull-left col-xs-6" role="form">
                <div class="form-group">
                    <label for="keyword">Từ khoá</label>
                    <input type="text" class="form-control" name="keyword" id="keyword"
                           placeholder="Nhập từ khoá cần tìm">
                </div>

                <a class="btn btn-primary " id="btn-search">Tìm kiếm</a>
            </div>
            <div class="right-button">
                <button type="button" id="btn-add" class="btn Normal btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <div class="box-body">
            <table id="treeGrid" class="easyui-treegrid table table-bordered table-striped table-hover" width="100%"
                   url="manage/product-category/data_grid"
                   rownumbers="true"
                   nowrap="false"
                   idField="id" treeField="name"
                   remoteFilter="false">
                <thead>
                <tr>
                    <th field="name" width="45%">Tên danh mục</th>
                    <th field="description" width="45%">Miêu tả</th>
                    <th field="id" width="10%" formatter="formatAction" align="center"></th>
                </tr>
                </thead>
            </table>
        </div>
    </div>

    <div class="modal fade in" id="modal-product-category" style="display: none; padding-right: 17px">
        <div class="modal-dialog" style="width: 1000px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Danh mục sản phẩm</h4>
                </div>
                <div class="modal-body ">

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnClose">Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade" id="modal-delete" style="display: none;">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="bootbox-close-button close" data-dismiss="modal" aria-hidden="true">×
                    </button>
                    <h4 class="modal-title">Xác nhận xóa</h4>
                </div>
                <div class="modal-body">
                    <div class="bootbox-body">
                        <i class="fa fa-warning" style="color:red; font-size: 24px"></i> Bạn có chắc chắn muốn xóa
                        <strong>Chỉ thị</strong>?
                    </div>
                </div>
                <div class="modal-footer">
                    <button data-bb-handler="cancel" type="button" class="btn btn-default" data-dismiss="modal">
                        <i class="fa fa-times"></i> Huỷ
                    </button>
                    <button data-bb-handler="confirm" type="button" class="btn btn-primary" onclick='ajax_delete()'>
                        <i class="fa fa-check"></i> Xóa
                    </button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')

    <script src="js/manage/product-category.js"></script>
    <script src="plugins/easyui/jquery.easyui.min.js"></script>
    <script>
        $(function(){
            $('#treeGrid').datagrid('loaded');
        })
    </script>
@endpush