<?php Former::populate($productCategory) ?>
@if($productCategory)
    {!! Former::open_for_files(route('product-category.update',$productCategory->id),'put') !!}
@else
    {!! Former::open_for_files(route('product-category.store')) !!}
@endif
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Danh mục cha: </label>
    <div class="col-lg-10 col-sm-8 tree-search">
        {{--@include('components.tree_product_category',array('tree' =>$tree,'level'=>0,'parent_id'=>($productCategory?$productCategory->parent_id:null),'root'=>'-- Danh mục gốc --' ))--}}
        @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>'-- Danh mục gốc --','parent_id'=>($productCategory?$productCategory->parent_id:null)])
    </div>
</div>
{!! Former::text('name','Tên danh mục ') !!}
{!! Former::textarea('description','Miêu tả ')->rows(4)  !!}
<div class="form-group">
    <label for="icon" class="control-label col-lg-2 col-sm-4">Icon: </label>
    <div class="col-lg-10 col-sm-8">
        <img id="product-category-icon" title="Chọn ảnh: " onclick="document.getElementById('icon').click();" width="50" style="cursor: pointer"
            src="@if(isset($productCategory->icon)) {{ url('image/200/200/'.$productCategory->icon) }} @else {{ url('css/images/no-image.png') }} @endif"
        />
        <input type="file" style="display: none" id="icon" name="icon" accept="image/*" onchange="addNewIcon(this)" />
    </div>
</div>
{!! Former::radios('Ngành quản lý')->radios($departments)->inline()!!}
{!! Former::close() !!}

<script type="text/javascript">
    var addNewIcon = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Hiển thị ảnh vừa mới upload lên
                $('#product-category-icon').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            //submit form để upload ảnh
        }
    }
</script>