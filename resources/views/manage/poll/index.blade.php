@extends('layouts.app')

@section('title')
Quản lý Thăm dò ý kiến người dùng
@endsection

@section('decription')
@endsection

@section('content')
<div class="modal fade" id="modal-add" data-backdrop="static">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><i class="fa fa-book"></i> Các ý kiến thăm dò </h4>
            </div>
            <div class="modal-body">
                <div class="box-body table-responsive">
                    <div class="box-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <form action="manage/pollAnswer" id="add_form" method="post">
                                    <input name="pollId" type="hidden" id="pollId" />
                                    <div class="input_fields_wrap">
                                        <div style="margin-bottom: 15px; float: right;">
                                            <button class=" add_field_button btn Normal btn-success" ><i class="fa fa-plus"></i> Thêm ý kiến</button>
                                        </div>
                                        <div style="margin-bottom:20px;">
                                            {!! Former::text('mytext[]','') !!}
                                        </div>
                                    </div>

                                </form>
                                <div id="img-table" class="img-table">
                                    <table class="table table-bordered table-striped table-hover" id="datatableImg" width="100%"> 
                                        <colgroup>
                                            <col width="30%">
                                            <col width="20%">
                                            <col width="20%">
                                            <col width="20%">
                                            <col width="10%">
                                        </colgroup>
                                        <thead>
                                            <tr role="row" class="heading">
                                                <th>Ý kiến</th>
                                                <th>Bình chọn</th>
                                                <th>Thứ tự</th>
                                                <th>Hiển thị</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal"> Đóng</button>
                <button id="btn-delete" type="button" class="btn btn-primary" onClick='polls.upload()'>Lưu</button>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-xs-12">
        <div class="box" id="poll-box">
            <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('active', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chưa hiển thị', '1' => 'Đang hiển thị']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
                <div class="right-button">
                    <a type="button" class="btn Normal btn-success" href="{{'manage/poll/create'}}">
                        <i class="fa fa-plus"></i>
                        Thêm mới
                    </a>
                </div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                    <colgroup>
                        <col width="30%">
                        <col width="15%">
                        <col width="15%">
                        <col width="15%">
                        <col width="15%">
                        <col width="10%">
                    </colgroup>
                    <thead>
                        <tr role="row" class="heading">
                            <th>Câu hỏi thăm dò</th>
                            <th>Bình chọn</th>
                            <th>Thứ tự</th>
                            <th>Người tạo</th>
                            <th>Hiển thị trên website</th>
                            <th></th>
                        </tr>
                    </thead>
                </table>

            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
</div>

@endsection
@push('scripts')
<script>
        var tableLenghs=0;
    function setTextFilename(files) {
       // alert(tableLenghs);
        for (var i = 0; i < files.length; i++) {
            $('#img-table tbody').append("");
           // table.rows.add($(table_rows)).draw();
            $(".img-table tbody").append("<tr><td>"+(tableLenghs+i+1)+" </td><td><img width='100' height='100' src='" + URL.createObjectURL(files[i]) + "'></td><td></td></tr>");
        }

    }
    function closeModal() {
    oTableImg.draw();
    $('#modal-add').modal('hide');
    }
    var polls = function () {
        var box;
        var searchForm;
        var tableId;
        var oTable;
        var pollId = 0;
        var tableIdImg;
        var oTableImg;
//        function closeModal() {
//            oTableImg.draw();
//            $('#modal-add').modal('hide');
//        }
        function upload() {
            //var form_data = new FormData($("#upload_form")[0]);
            //form_data.append('id', $('#pollId').val());
            var formData = sdcApp.getFormDataAndType($("#add_form"));
            //formData.append('id', $('#pollId').val());
            $.ajax({
                url: 'manage/pollAnswer',
                method: 'POST',
                data: formData.data,
                contentType: formData.contentType,
                processData: false
            }).done(function (data) {

                $('#modal-add').modal('hide');
                $('input[name="mytext[]"]').val('');
                $('.input_hd').remove();
            });

        }
        return {
            init: function (baseUrl, url) {
                box = $('#poll-box');
                searchForm = $('#search-form');
                tableId = '#datatable';
                oTable = $(tableId).DataTable({
                    "rowId": 'id',
                    "ajax": {
                        "url": baseUrl + '/data',
                        "data": function (d) {
                            d.keyword = searchForm.find('input[name=keyword]').val();
                            d.active = searchForm.find('select[name=active]').val();
                        }
                    },
                    "columns": [
                        {"data": 'name'},
                        {"data": 'votes',"orderable": false},
                        {"data": 'no'},
                        //{"data": 'username'},
                        {"data": 'user_name',"orderable": false},
                        {"data": 'public',
                            class: "text-center",
                            "render": function (value, type, data) {
                                if(value == 1)
                                    return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                                else
                                    return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                            }
                        },
                        {
                            "data": 'id',
                            "orderable": false,
                            "class": 'text-center',
                            "render": function (value, type, data) {
                                return '<button class="btn btn-sm  btn-upload" data-toggle="modal" title="Ý kiến" ><i class="fa fa-upload"></i></button>\n\
                                        <a href="manage/poll/'+data.id+'/edit" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="glyphicon glyphicon-pencil"></i></a>\n\
                                        <a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>';
                            }
                        },
                        {"data": 'id', "visible": false}
                    ],
                    "order": [4, 'desc']
                });
        //table poll answer
                var oTableImg = null;
                $('#datatable tbody').on('click', '.btn-upload', function (e) {
                    e.preventDefault();
                    var btn = $(this);
                    pollId = oTable.row(btn.closest('tr')).id();
                    $('#pollId').val(pollId);
                    $(".img-table tbody").html("");
                    tableIdImg = '#datatableImg';
                    var dataUrl = 'manage/getPollAnswer/' + pollId;
                    if (oTableImg === null){
                        oTableImg = $(tableIdImg).DataTable({
                            "rowId": 'id',
                            "ajax": dataUrl,
                            "columns": [
                                {"data": 'name'},
                                {"data": 'votes',"orderable": false},
                                {"data": 'no'},
                                {"data": 'public',
                                    class: "text-center",
                                    "render": function (value, type, data) {
                                        if(value == 1)
                                            return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                                        else
                                            return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                                    }
                                },
                                {
                                    "data": 'id',
                                    "orderable": false,
                                    "class": 'text-center',
                                    "render": function (value, type, data) {
                                        return '<a href="manage/pollAnswer/'+data.id+'/edit" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="glyphicon glyphicon-pencil"></i></a><a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>';
                                    }
                                },
                                //{"data": 'id', "visible": false}
                            ]
                            //"order": [4, 'desc']
                        });
                        sdcApp.bindTblDelEvent(tableIdImg, oTableImg, "{{ route('pollAnswer.index') }}", 0);
                    }
                    else{
                        oTableImg.ajax.url(dataUrl).load();
                    }
                    $('#modal-add').modal();
                });
                sdcApp.bindTblDelEvent(tableId, oTable,baseUrl, 0);

                searchForm.on('submit', function (e) {
                    e.preventDefault();
                    oTable.draw();
                });

            },
            upload: upload,
        };
    }();

    $(function () {
        polls.init('{{ route('poll.index') }}','{{ url('storage') }}' )
    });


</script>

<script>
    $(document).ready(function() {
    var max_fields      = 100; //maximum input boxes allowed
    var wrapper         = $(".input_fields_wrap"); //Fields wrapper
    var add_button      = $(".add_field_button"); //Add button ID
    var x = 1; //initlal text box count
    $(add_button).click(function(e){ //on add input button click
        e.preventDefault();
        if(x < max_fields){ //max input box allowed
            x++; //text box increment
            $(wrapper).append('<div class="input_hd" style="margin-bottom:10px;" >{!! Former::text('mytext[]','') !!}<a href="#" class="remove_field">Xóa ý kiến</a></div>'); //add input box
        }
    });
    
    $(wrapper).on("click",".remove_field", function(e){ //user click on remove text
        e.preventDefault(); $(this).parent('div').remove(); x--;
    })
});
</script>
@endpush