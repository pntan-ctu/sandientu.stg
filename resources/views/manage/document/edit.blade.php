
<?php
Former::populate($doc);
$rules = ['number_symbol' => 'required|max:255',
    'sign_date' => 'required',
    'quote' => 'required',
    'description' => 'required',];
?>
@if($doc)
{!! Former::open(route('document.update', $doc->id), 'put')->rules($rules); !!}
@else
{!! Former::open(route('document.store'))->rules($rules); !!}
@endif
{!! Former::text('number_symbol', 'Số/Ký hiệu') !!}
{!! Former::text('sign_date','Ngày ký')->addClass('form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->forceValue($doc?$doc->sign_date->toShortDateString():null) !!}
{!! Former::select('document_type_id', 'Loại văn bản')->addOption('', '')->fromQuery($docType, 'name', 'id')->addClass('doc-select')->select($doc?$doc->document_type_id:null) !!}
{!! Former::select('document_organ_id', 'Cơ quan ban hành')->addOption('', '')->fromQuery($docOrgan, 'name', 'id')->addClass('doc-select')->select($doc?$doc->document_organ_id:null) !!}
{!! Former::select('document_field_id', 'Lĩnh vực văn bản')->addOption('', '')->fromQuery($docField, 'name', 'id')->addClass('doc-select')->select($doc?$doc->document_field_id:null) !!}
{!! Former::text('sign_by', 'Người ký: ') !!}
{!! Former::textarea('quote')->id('txtQuote')->label('Trích yếu: ')!!}
<div class="form-group">
    <div class="col-lg-12">
        <textarea class="ckeditor" name="description" id="documentEditor">
            @if(isset($doc->description)) {!! html_entity_decode($doc->description) !!} @endif
        </textarea>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-lg-2 col-sm-4">Tệp đính kèm: </label>
    <div class="btn btn-default btn-file">
        <i class="fa fa-file-word-o"></i> Chọn file
        <input type="file" id="avatar" name="fileAttachment" onChange='setTextFilename($(this).val());'>
    </div>
    &nbsp; <span class="label label-info" id="upload-file-info">{{$fileName}}</span>
</div>

{!! Former::close() !!}