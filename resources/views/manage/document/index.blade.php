@extends('layouts.app')

@section('title')
Quản lý văn bản
@endsection

@section('decription')
@endsection

@section('content')
<div class="box" id="box-area">

    <div class="box-header">
        <form method="POST" id="search-form" class="form-horizontal form-input" role="form">

            <div class="form-group">
                <div class="col-lg-2">
                    {!! Former::select('idDocType')->raw()->addOption('--Loại văn bản--', '0')->fromQuery($docType, 'name', 'id')->addClass('doc-selects') !!}
                </div>
                <div class="col-lg-2">
                    {!! Former::select('idDocOrgan')->raw()->addOption('--Cơ quan ban hành--', '0')->fromQuery($docOrgan, 'name', 'id')->addClass('doc-selects') !!}
                </div>
                <div class="col-lg-2">
                    {!! Former::select('idDocField')->raw()->addOption('--Lĩnh vực văn bản--', '0')->fromQuery($docField, 'name', 'id')->addClass('doc-selects') !!}
                </div>
                <div  class="col-lg-4">
                    <div class="col-lg-8" >
                        <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
                    </div>
                    <button type="submit" class="btn btn-primary col-lg-4">Tìm kiếm</button>
                </div>
                <div class="col-lg-2">
                    <button id="btn-add" type="button" class="btn btn-success">
                        <i class="fa fa-plus"></i>
                        Thêm mới
                    </button>
                </div>
            </div>


        </form>
    </div>
    <div class="box-body">

        <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
            <colgroup>

                <col width="15%">
                <col width="60%">
                <col width="15%">
                <col width="10%">
            </colgroup>
            <thead>
                <tr role="row" class="heading">

                    <th>Số/Ký hiệu</th>
                    <th>Trích yếu</th>
                    <th>Ngày ký</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection
@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
@endpush
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/document.js"></script>
    <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script>
        $(function () {
            sdcApp.fixCKEditorModal();
            Document('{{ route('document.index') }}');
        })
    </script>
@endpush