@extends('layouts.app')

@section('title')
Tổng quan
@endsection

@section('decription')
@endsection

@push('styles')
    <style>
      .small-boxes {
            position: relative;
            min-height: 1px;
            padding-right: 5px;
            padding-left: 5px;
        }
      .small-row {
            padding-right: 10px;
            padding-left: 10px;
        }
    </style>
@endpush

@section('content')
<div>
    <!-- Small boxes (Stat box) -->
      <div class="row small-row">
        <div class="col-lg-2 small-boxes">
          <!-- small box -->
          <div class="small-box bg-aqua">
            <div class="inner">
              <h3>{{$organization0}}</h3>

              <p>Cơ sở SX, KD chưa duyệt</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{url('manage/orgtype/100/organization')}}" class="small-box-footer">Chi tiết <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>        
        <!-- ./col -->
        <div class="col-lg-2 small-boxes">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$advertisings}}</h3>

              <p>Tin cung cầu chưa duyệt</p>
            </div>
            <div class="icon">
              <i class="ion ion-bag"></i>
            </div>
            <a href="{{url('manage/advertisings')}}" class="small-box-footer">Chi tiết <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->        
        <div class="col-lg-2 small-boxes">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$contact}}</h3>

              <p>Phản hồi chưa xử lý</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{{url('manage/contact')}}" class="small-box-footer">Chi tiết <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 small-boxes">
          <!-- small box -->
          <div class="small-box bg-green">
            <div class="inner">
              <h3>{{$rating}}</h3>

              <p>Đánh giá chưa duyệt</p>
            </div>
            <div class="icon">
              <i class="ion ion-stats-bars"></i>
            </div>
            <a href="{{url('manage/rating')}}" class="small-box-footer">Chi tiết <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 small-boxes">
          <!-- small box -->
          <div class="small-box bg-yellow">
            <div class="inner">
              <h3>{{$questionanswer}}</h3>

              <p>Hỏi đáp chưa duyệt</p>
            </div>
            <div class="icon">
              <i class="ion ion-person-add"></i>
            </div>
            <a href="{{url('manage/question-answer')}}" class="small-box-footer">Chi tiết <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
        <!-- ./col -->
        <div class="col-lg-2 small-boxes">
          <!-- small box -->
          <div class="small-box bg-red">
            <div class="inner">
              <h3>{{$infringement}}</h3>

              <p>Phản ánh VP chưa XL</p>
            </div>
            <div class="icon">
              <i class="ion ion-pie-graph"></i>
            </div>
            <a href="{{url('manage/infringements')}}" class="small-box-footer">Chi tiết <i class="fa fa-arrow-circle-right"></i></a>
          </div>
        </div>
      </div>
      <!-- /.row -->
      <!-- Main row -->
      <div class="row small-row">
        <!-- /.Left col -->
        <!-- right col (We are only adding the ID to make the widgets sortable)-->
<!--        <section class="col-lg-5 connectedSortable">-->

          <!-- Custom tabs (Charts with tabs)-->
          <div class="col-lg-6 small-boxes">
              <div class="nav-tabs-custom" style="float: left;width:100%;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-inbox"></i> Quy mô doanh nghiệp</li>
                </ul>
                <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="revenue-chart" style="position: relative; height: 300px;"></div>
                </div>
              </div>
          </div>    
          <div class="col-lg-6 small-boxes">
              <div class="nav-tabs-custom" style="float: right;width:100%;">
                <!-- Tabs within a box -->
                <ul class="nav nav-tabs pull-right">
                  <li class="pull-left header"><i class="fa fa-handshake-o"></i> Tỉ lệ xử lý đơn hàng</li>
                </ul>
                <div class="tab-content no-padding">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="sales-chart" style="position: relative; height: 300px;"></div>
                </div>
              </div>
          </div>
          <div class="clear"></div>
          <!-- /.nav-tabs-custom -->

<!--        </section>-->
        <!-- right col -->
      </div>

      <div class="row">
          <div class="col-xs-6">
        <!-- Left col -->
<!--        <section class="col-lg-7 connectedSortable">-->

          <!-- Map box -->
          <div class="box box-solid ">
            <div class="box-header">
              <!-- tools box -->
              <div class="pull-right box-tools">
<!--                <button type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip"
                        title="Date range">
                  <i class="fa fa-calendar"></i></button>-->
                <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                        data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                  <i class="fa fa-minus"></i></button>
              </div>
              <!-- /. tools -->

              <i class="fa fa-map-marker"></i>

              <h3 class="box-title">
                Phân bố cơ sở SX, KD
              </h3>
            </div>
            <div class="box-body">
              <div id="world-map" style="height: 500px;"></div>
            </div>
          </div>



      </div>
  
    

      <div class="col-xs-6">
          <div class="box box-solid ">
          <div class="box-header">
            <!-- tools box -->
            <div class="pull-right box-tools">
<!--                <button type="button" class="btn btn-primary btn-sm daterange pull-right" data-toggle="tooltip"
                      title="Date range">
                <i class="fa fa-calendar"></i></button>-->
              <button type="button" class="btn btn-primary btn-sm pull-right" data-widget="collapse"
                      data-toggle="tooltip" title="Collapse" style="margin-right: 5px;">
                <i class="fa fa-minus"></i></button>
            </div>
            <!-- /. tools -->

            <i class="fa fa-bar-chart"></i>

            <h3 class="box-title">
              Thống kê truy cập
            </h3>
          </div>
          <div class="box-body">
              <div id="embed-api-auth-container"></div>
              <div class="col-xs-6">
                  <div class="nav-tabs-custom" style="float: left;width:100%;">
                      <div id="chart-container"></div>
                      <div id="view-selector-container"></div>
                  </div>
              </div>    
              <div class="col-xs-6">
                  <div class="nav-tabs-custom" style="float: right;width:100%;">
                      <div id="chart-container1"></div>
                      <div id="view-selector-container1"></div>
                  </div>
              </div>
              <div class="clear"></div>
          </div>
      </div>
      </div>
  </div>
      <!-- /.row (main row) -->
</div>
@endsection


@push('scripts')

<script type="text/javascript" src="https://www.google.com/jsapi"></script>

<script>
//      function initMap() {
//        var uluru = {lat: 19.807443, lng: 105.774642};
//        var map = new google.maps.Map(document.getElementById('world-map'), {
//          zoom: 11,
//          center: uluru
//        });
//        var marker = new google.maps.Marker({
//          position: uluru,
//          map: map
//        });
//      }
        function initMap() {
            var thanhhoa = new google.maps.LatLng(10.374270, 106.341842);

            var map = new google.maps.Map(document.getElementById('world-map'), {
                  scaleControl: true,
                  center: thanhhoa,
                  zoom: 10
            });

            var organization100 = {!!$organization100!!};
            var url1 = "{{url('shop/')}}" + "/"; //"{{url('/')}}"
            var url2 = "{{url('dia-diem-kd/')}}";
            $.each(organization100, function (index, value) {
                var thanhhoa1 = new google.maps.LatLng(value.map_lat, value.map_long);
                //var latLong = value.map_lat + "," + value.map_long;
                var title = '';//
                if(value.organization_type_id===100){
                   title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
               }else if(value.organization_type_id===101){
                   title = "<b><a target='_blank' href='" + url1 + value.path + "'>" + value.name + "</a></b>";
               }else if(value.organization_type_id===0){
                   title = "<b>" + value.name + "</b>";
               }else if(value.organization_type_id===1){
                   title = "<b><a target='_blank' href='" + url2 + "'>" + value.name + "</a></b>";
               }
                var coordInfoWindow = new google.maps.InfoWindow();
                coordInfoWindow.setContent(createInfoWindowContent(value,title, map.getZoom()));

               var img_ico = '';//"{{url('/')}}" + "/css/images/cssx.png";
               if(value.organization_type_id===100){
                   img_ico = "{{url('/')}}" + "/css/images/cssx1.png";
               }else if(value.organization_type_id===101){
                   img_ico = "{{url('/')}}" + "/css/images/cskd.png";
               }else if(value.organization_type_id===0){
                   img_ico = "{{url('/')}}" + "/css/images/branch.png";
               }else if(value.organization_type_id===1){
                   img_ico = "{{url('/')}}" + "/css/images/market.png";
               }
               var marker = new google.maps.Marker({map: map, position: thanhhoa1,icon: img_ico});
                  marker.addListener('click', function() {
                    coordInfoWindow.open(map, marker);
                  });

                map.addListener('zoom_changed', function() {
                  coordInfoWindow.setContent(createInfoWindowContent(new thanhhoa1, map.getZoom()));
                  coordInfoWindow.open(map);
                });
            }); 
        }

        var TILE_SIZE = 256;

        function createInfoWindowContent(value,latLong, zoom) {
          var scale = 1 << zoom;

          return [
              latLong,
            //value.name,
            //'Chi tiếṭ: ' + latLong,
            'Địa chỉ: ' + value.address,
            'Tel: ' + value.tel,
            'Email: ' + value.email,
            'Website: ' + value.website
          ].join('<br>');
        }

        // The mapping between latitude, longitude and pixels is defined by the web
        // mercator projection.
        function project(latLng) {
          var siny = Math.sin(latLng.map_lat * Math.PI / 180);

          // Truncating to 0.9999 effectively limits latitude to 89.189. This is
          // about a third of a tile past the edge of the world tile.
          siny = Math.min(Math.max(siny, -0.9999), 0.9999);

          return new google.maps.Point(
              TILE_SIZE * (0.5 + latLng.map_long / 360),
              TILE_SIZE * (0.5 - Math.log((1 + siny) / (1 - siny)) / (4 * Math.PI)));
        }

      google.load('visualization', '1.0', {'packages':['corechart']});
      google.setOnLoadCallback(drawChart);
      function drawChart() {
        var data = new google.visualization.DataTable();
        data.addColumn('string', 'Topping');
        data.addColumn('number', 'Slices');
        data.addRows([
          ['Cá nhân, hộ gia đình', {!!$organization1!!}],
          ['Trang trại, xưởng', {!!$organization2!!}],
          ['Hợp tác xã', {!!$organization3!!}],
          ['Doanh nghiệp siêu nhỏ', {!!$organization4!!}],
          ['Doanh nghiệp nhỏ', {!!$organization5!!}],
          ['Doanh nghiệp vừa', {!!$organization6!!}],
          ['Doanh nghiệp lớn', {!!$organization7!!}]
        ]);
        var options = {'title':'Quy mô doanh nghiệp',is3D:true};
        var chart = new google.visualization.PieChart(document.getElementById('revenue-chart'));
        chart.draw(data, options);

        var data1 = new google.visualization.DataTable();
        data1.addColumn('string', 'Topping');
        data1.addColumn('number', 'Slices');
        data1.addRows([
          ['Chưa xử lý', {!!$order1!!}],
          ['Đã xử lý', {!!$order2!!}],
          ['Đã hủy', {!!$order3!!}],
          ['Đã xác nhận', {!!$order4!!}]
        ]);
        var options1 = {'title':'Tỉ lệ xử lý đơn hàng',pieHole: 0.3};
        var chart1 = new google.visualization.PieChart(document.getElementById('sales-chart'));
        chart1.draw(data1, options1);
      }
</script>

<script async defer
         src="https://maps.googleapis.com/maps/api/js?key={{config('app.google_map_api_key')}}&sensor=false&callback=initMap">
</script>
<script>
    (function(w,d,s,g,js,fs){
      g=w.gapi||(w.gapi={});g.analytics={q:[],ready:function(f){this.q.push(f);}};
      js=d.createElement(s);fs=d.getElementsByTagName(s)[0];
      js.src='https://apis.google.com/js/platform.js';
      fs.parentNode.insertBefore(js,fs);js.onload=function(){g.load('analytics');};
    }(window,document,'script'));
</script>
<script>

    gapi.analytics.ready(function() {

      /**
       * Authorize the user immediately if the user has already granted access.
       * If no access has been created, render an authorize button inside the
       * element with the ID "embed-api-auth-container".
       */
      gapi.analytics.auth.authorize({
        container: 'embed-api-auth-container',
        clientid: '{{config("app.google_analytic_api_key")}}'
      });


      /**
       * Create a new ViewSelector instance to be rendered inside of an
       * element with the id "view-selector-container".
       */
      var viewSelector = new gapi.analytics.ViewSelector({
        container: 'view-selector-container'
      });
      var viewSelector1 = new gapi.analytics.ViewSelector({
        container: 'view-selector-container1'
      });

      // Render the view selector to the page.
      viewSelector.execute();
      viewSelector1.execute();


      /**
       * Create a new DataChart instance with the given query parameters
       * and Google chart options. It will be rendered inside an element
       * with the id "chart-container".
       */
      var dataChart = new gapi.analytics.googleCharts.DataChart({
        query: {
          metrics: 'ga:sessions',
          dimensions: 'ga:date',
          'start-date': '30daysAgo',
          'end-date': 'yesterday'
        },
        chart: {
          container: 'chart-container',
          type: 'LINE',
          options: {
            width: '100%'
          }
        }
      });

      var dataChart1 = new gapi.analytics.googleCharts.DataChart({
        query: {
          metrics: 'ga:sessions',
          dimensions: 'ga:country',
          'start-date': '30daysAgo',
          'end-date': 'yesterday',
          sort: '-ga:sessions'
        },
        chart: {
          container: 'chart-container1',
          type: 'PIE',
          options: {
            width: '100%',
            pieHole: 4/9
          }
        }
      });
      /**
       * Render the dataChart on the page whenever a new view is selected.
       */
      viewSelector.on('change', function(ids) {
        dataChart.set({query: {ids: ids}}).execute();
      });
      
       viewSelector1.on('change', function(ids) {
        dataChart1.set({query: {ids: ids}}).execute();
  });
    });
</script>
@endpush