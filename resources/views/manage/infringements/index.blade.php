@extends('layouts.app')

@section('title')
    Quản lý phản ánh vi phạm
@endsection

@section('decription')

@endsection

@push('styles')
    <style>
        .nav-tabs li a {font-weight:bold}
        select {width: 100%}
        .modal-body .col-md-9 div {display: flex}
        .modal-body .col-md-9 div span{margin-left: 5px}
    </style>
@endpush

@section('content')
    <div class="box">
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_1" data-toggle="tab">Cơ sở SX/KD</a></li>
                <li><a href="#tab_2" data-toggle="tab">Sản phẩm</a></li>
                <li><a href="#tab_3" data-toggle="tab">Người dùng</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_1">
                    <div class="box-body">
                        <table class="table table-bordered table-striped table-hover" id="dtInfringementOrganizations" width="100%">
                            <colgroup>
                                <col width="15%">
                                <col width="12%">
                                <col width="15%">
                                <col width="20%">
                                <col width="10%">
                                <col width="12%">
                                <col width="8%">
                                <col width="8%">
                            </colgroup>
                            <thead>
                                <tr role="row" class="heading">
                                    <th>Tên cơ sở</th>
                                    <th>Trạng thái</th>
                                    <th>Loại vi phạm</th>
                                    <th>Nội dung</th>
                                    <th>Ngày đăng</th>
                                    <th>Người đăng</th>
                                    <th>Xử lý</th>
                                    <th></th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="tab_2">
                    <div class="box-body">
                        <table class="table table-bordered table-striped table-hover" id="dtInfringementProducts" width="100%">
                            <colgroup>
                                <col width="15%">
                                <col width="12%">
                                <col width="15%">
                                <col width="20%">
                                <col width="10%">
                                <col width="12%">
                                <col width="8%">
                                <col width="8%">
                            </colgroup>
                            <thead>
                            <tr role="row" class="heading">
                                <th>Tên sản phẩm</th>
                                <th>Trạng thái</th>
                                <th>Loại vi phạm</th>
                                <th>Nội dung</th>
                                <th>Ngày đăng</th>
                                <th>Người đăng</th>
                                <th>Xử lý</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>

                <div class="tab-pane" id="tab_3">
                    <div class="box-body">
                        <table class="table table-bordered table-striped table-hover" id="dtInfringementUsers" width="100%">
                            <colgroup>
                                <col width="15%">
                                <col width="12%">
                                <col width="15%">
                                <col width="20%">
                                <col width="10%">
                                <col width="12%">
                                <col width="8%">
                                <col width="8%">
                            </colgroup>
                            <thead>
                            <tr role="row" class="heading">
                                <th>Tên người dùng</th>
                                <th>Trạng thái</th>
                                <th>Loại vi phạm</th>
                                <th>Nội dung</th>
                                <th>Ngày đăng</th>
                                <th>Người đăng</th>
                                <th>Xử lý</th>
                                <th></th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>

            </div>
            <!-- /.tab-content -->
        </div>
    </div>

    <div class="modal fade in" id="modalPerformOrganization" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Xử lý phản ánh vi phạm</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="org_id" value="0" />
                    <div class="row">
                        <div class="col-md-3">Cơ sở:</div>
                        <div class="col-md-9"><b><span id="orgName"></span></b></div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-3">Hình thức xử lý:</div>
                        <div class="col-md-9">
                            <div><input type="radio" name="org_status" value="1"><span>Đang hoạt động</span></div>
                            <div><input type="radio" name="org_status" value="2"><span>Ngừng hoạt động</span></div>
                            <div><input type="radio" name="org_status" value="3"><span>Khóa do vi phạm</span></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-times"></i> Đóng
                    </button>
                    <button type="button" class="btn btn-primary" id="btnPerformOrg">
                        <i class="fa fa-check"></i> Lưu
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade in" id="modalPerformProduct" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Xử lý phản ánh vi phạm</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="product_id" value="0" />
                    <div class="row">
                        <div class="col-md-3">Sản phẩm:</div>
                        <div class="col-md-9"><b><span id="productName"></span></b></div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-3">Hình thức xử lý:</div>
                        <div class="col-md-9">
                            <div><input type="radio" name="product_active" value="1"><span>Mở khóa</span></div>
                            <div><input type="radio" name="product_active" value="2"><span>Khóa</span></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-times"></i> Đóng
                    </button>
                    <button type="button" class="btn btn-primary" id="btnPerformProduct">
                        <i class="fa fa-check"></i> Lưu
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

    <div class="modal fade in" id="modalPerformUser" style="display: none">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Xử lý phản ánh vi phạm</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="user_id" value="0" />
                    <div class="row">
                        <div class="col-md-3">Người dùng:</div>
                        <div class="col-md-9"><b><span id="userName"></span></b></div>
                    </div>
                    <div class="row" style="margin-top: 10px">
                        <div class="col-md-3">Hình thức xử lý:</div>
                        <div class="col-md-9">
                            <div><input type="radio" name="user_status" value="0"><span>Khóa</span></div>
                            <div><input type="radio" name="user_status" value="1"><span>Mở khóa</span></div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">
                        <i class="fa fa-times"></i> Đóng
                    </button>
                    <button type="button" class="btn btn-primary" id="btnPerformUser">
                        <i class="fa fa-check"></i> Lưu
                    </button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/infringement.js"></script>
    <script>
        Infringements('manage/infringements/getData/Organization', '#dtInfringementOrganizations');
        Infringements('manage/infringements/getData/Product', '#dtInfringementProducts');
        Infringements('manage/infringements/getData/User', '#dtInfringementUsers');

        function perform(type, id, name, status)
        {
            if(type == 0) {
                $("#orgName").html(name);
                $("input[name='org_id']").val(id);
                var radios = document.getElementsByName('org_status');
                var count = radios.length;
                for (var i = 0, length = count; i < length; i++) {
                    radios[i].checked = radios[i].value == status;
                }
                $("#modalPerformOrganization").modal("show");
            }

            if(type == 1) {
                $("#productName").html(name);
                $("input[name='product_id']").val(id);
                var radios = document.getElementsByName('product_active');
                var count = radios.length;
                for (var i = 0, length = count; i < length; i++) {
                    radios[i].checked = radios[i].value == status;
                }
                $("#modalPerformProduct").modal("show");
            }

            if(type == 2) {
                $("#userName").html(name);
                $("input[name='user_id']").val(id);
                var radios = document.getElementsByName('user_status');
                var count = radios.length;
                for (var i = 0, length = count; i < length; i++) {
                    radios[i].checked = radios[i].value == status;
                }
                $("#modalPerformUser").modal("show");
            }
        }

        $(function () {
            $("#btnPerformOrg").click(function(){
                bootbox.confirm(
                    "Bạn chắc chắn muốn thực hiện?",
                    function(result) {
                        if (result) {
                            var orgId =  $("input[name='org_id']").val();
                            var orgStatus = $("input[name='org_status']:checked").val();
                            $.ajax({
                                url: 'manage/infringements/perform/' + orgId + '_' + orgStatus,
                                success: function (data) {
                                    $("#modalPerformOrganization").modal("hide");
                                    $('#dtInfringementOrganizations').DataTable().ajax.reload(null, false);
                                    toastr.success("Đã lưu thông tin");
                                },
                                error: function () {
                                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                                }
                            });
                        }
                    }
                );
            });

            $("#btnPerformProduct").click(function(){
                bootbox.confirm(
                    "Bạn chắc chắn muốn thực hiện?",
                    function(result) {
                        if (result) {
                            var productId =  $("input[name='product_id']").val();
                            var productActive = $("input[name='product_active']:checked").val();
                            $.ajax({
                                url: 'manage/infringements/perform/' + productId + '_' + productActive,
                                success: function (data) {
                                    $("#modalPerformProduct").modal("hide");
                                    $('#dtInfringementProducts').DataTable().ajax.reload(null, false);
                                    toastr.success("Đã lưu thông tin");
                                },
                                error: function () {
                                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                                }
                            });
                        }
                    }
                );
            });

            $("#btnPerformUser").click(function(){
                bootbox.confirm(
                    "Bạn chắc chắn muốn thực hiện?",
                    function(result) {
                        if (result) {
                            var userId =  $("input[name='user_id']:checked").val();
                            var userStatus = $("#user_status").val();
                            $.ajax({
                                url: 'manage/infringements/perform/' + userId + '_' + userStatus,
                                success: function (data) {
                                    $("#modalPerformUser").modal("hide");
                                    $('#dtInfringementUsers').DataTable().ajax.reload(null, false);
                                    toastr.success("Đã lưu thông tin");
                                },
                                error: function () {
                                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                                }
                            });
                        }
                    }
                );
            });
        });
    </script>
@endpush