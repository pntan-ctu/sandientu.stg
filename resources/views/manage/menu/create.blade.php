<?php Former::populate($menu); ?>
{!! Former::open(route('menu.store')) !!}
{!! Former::hidden('organization_type_id')->value($orgTypeId) !!}
{!! Former::hidden('id') !!}
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Menu cha:</label>
    <div class="col-lg-10 col-sm-8 tree-search">
        @include("components/select_search",['tree'=>$cboMenuData,"level"=>0,"path"=>null,"root"=>"-- Lựa chọn --",'parent_id'=>($menu?$menu->parent_id:null),'name'=>'parent_id'])
    </div>
</div>
{!! Former::text('name', 'Tên menu') !!}
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Module:</label>
    <div class="col-lg-10 col-sm-8 tree-search">
        @include("components/select_search",['tree'=>$cboModuleData,"level"=>0,"path"=>null,"root"=>"-- Lựa chọn --",'parent_id'=>($menu?$menu->module_id:null),'name'=>'module_id'])
    </div>
</div>
{!! Former::text('no', 'STT')->type('number')->value($nextNo)->setAttribute('max', '2147483646')->setAttribute('min', '1') !!}
{!! Former::close() !!}