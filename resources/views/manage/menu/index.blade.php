@extends('layouts.app')

@section('title')
    Quản lý Menu
@endsection

@section('decription')

@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="plugins/easyui/icon.css">
    <link rel="stylesheet" type="text/css" href="plugins/easyui/easyui.css">
    <style>
        label[for="organization_type_id"]{margin-right: 20px !important;}
        .icon {margin-right: 5px;z-index: 9999999}
        .select2-container--default .select2-selection--single .select2-selection__rendered {padding: 0px}
        .tree-search>span{width:100% !important}
    </style>
@endpush

@section('content')
    <div class="box" id="box-menu">
        <div class="box-header">
            <form method="POST" id="search-form" class="form-inline pull-left" role="form">
                {!! Former::hidden('menu_id')->value(0) !!}
                {!! Former::select('organization_type_id')->fromQuery($organizationTypes, 'name', 'id')->label('Loại đơn vị') !!}
            </form>
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success btn-update">
                    <i class="fa fa-plus"></i>
                    Thêm
                </button>
                <button id="btn-edit" type="button" class="btn btn-success btn-update" disabled>
                    <i class="fa fa-edit"></i>
                    Sửa
                </button>
                <button id="btn-remove" type="button" class="btn btn-success">
                    <i class="fa fa-remove"></i>
                    Xóa
                </button>
            </div>
        </div>
        <div class="box-body row">
            <ul id="treeMenu" checkbox="false" class="easyui-tree"></ul>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/menu.js"></script>
    <script src="plugins/easyui/jquery.easyui.min.js"></script>
    <script>
        $(function () {
            Menu(' {{ route('menu.index') }}');
            $('#treeMenu').tree({
                data: {!! $treeMenuData !!}
            });

            $('#treeMenu').tree({
                onClick: function(){
                    document.getElementById("btn-edit").disabled = false;
                }
            });
        });

        function reloadTree(){
            $.ajax({
                url: 'manage/menu/getTreeData/' + $('#organization_type_id').val(),
                success: function (data) {
                    $('#treeMenu').tree('loadData', data);
                    /*$('#treeMenu').tree({data: data});*/
                    document.getElementById("btn-edit").disabled = true;
                    sdcApp.showLoading(false);
                }
            });
        }

        $('#organization_type_id').change(function(){
            reloadTree();
        });

        $("#btn-remove").click(function() {
            var node = $('#treeMenu').tree('getSelected');

            if(node == null)
            {
                toastr.error("Bạn chưa chọn chuyên mục cần xóa");
                return;
            }

            if($('#treeMenu').tree('isLeaf', node.target)) {
                if(node.news_count > 0) {
                    toastr.error("Không thể xóa do chuyên mục này đang có tin bài");
                    return;
                }

                bootbox.confirm(
                    "Bạn chắc chắn muốn xóa <b>"+node.text+"</b>?",
                    function(result){
                        if(result){
                            $.ajax({
                                type: "DELETE",
                                url: 'manage/menu/' + node.id,
                                success: function (data) {
                                    $('#treeMenu').tree('remove', node.target);
                                }
                            });
                        }
                    });
            }
            else toastr.error("Không thể xóa do đang có chuyên mục con");
        });

        $("#btn-add").click(function () {
            $("input[name='menu_id']").val(0);
        });

        $("#btn-edit").click(function () {
            var node = $('#treeMenu').tree('getSelected');
            if(node == null)
            {
                toastr.error("Bạn chưa chọn menu cần sửa");
                return false;
            }
            $("input[name='menu_id']").val(node.id);
        });
    </script>
@endpush
