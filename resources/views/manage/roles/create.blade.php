<?php
Former::populate($role);
$rules = ['name' => 'max:255', 'title' => 'max:255'];
?>
@if($role)
    {!! Former::open(route('roles.update', $role->id), 'put')->rules($rules); !!}
@else
    {!! Former::open(route('roles.store'))->rules($rules); !!}
@endif
{!! Former::select('organization_type_id')->fromQuery($organizationTypes, 'name', 'id')->label('Loại đơn vị')->setAttribute('disabled', 'disabled') !!}
{!! Former::hidden('scope') !!}
{!! Former::text('name', 'Tên nhóm') !!}
{!! Former::text('title', 'Tiêu đề') !!}
{!! Former::close() !!}
<script>
    var typeId = $("#organization_type_id").val();
    $(".bootbox-body #organization_type_id").val(typeId);
    $(".bootbox-body input[name='scope']").val(typeId);
</script>