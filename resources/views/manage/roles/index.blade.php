@extends('layouts.app')

@section('title')
    Quản lý nhóm người dùng
@endsection

@section('decription')
    
@endsection

@push('styles')
    <style>
        #dtModule_wrapper{max-height: 300px !important; overflow-y: auto; overflow-x: hidden}
        .modal-body{overflow: auto !important;}
        .group{font-weight: bold;background-color: lightgray !important;}
        #dtModule_wrapper>div:nth-child(3){display: none !important;}
        .box-header form label{margin-right: 10px}
        .box-header form label[for="organization_type_id"]{margin-left: 35px}
        .box-header form button{margin-left: 10px}
    </style>
@endpush

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            <form method="POST" id="search-form" class="form-inline pull-left" role="form">
                <div class="form-group">
                    <label for="keyword">Từ khoá</label>
                    <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
                </div>
                <div class="form-group">
                    {!! Former::select('organization_type_id')->fromQuery($organizationTypes, 'name', 'id')->label('Loại đơn vị') !!}
                </div>

                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </form>
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="35%">
                    <col width="45%">
                    <col width="10%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Tên nhóm</th>
                    <th>Tiêu đề</th>
                    <th>Phân quyền</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="modal fade in" id="frmPhanQuyen" style="display: none; padding-right: 17px">
        <div class="modal-dialog" style="width: 800px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Phân quyền</h4>
                </div>
                <div class="modal-body row">
                    <form style="display: none" method="get" id="frmPhanQuyen_search" class="form-inline pull-left" role="form">
                        <div class="form-group">
                            <input type="hidden" name="role_id" value="0" />
                        </div>
                        <button type="submit" id="btnSearch" class="btn btn-primary" style="margin-left: 10px">Tìm kiếm</button>
                    </form>
                    <table class="table table-bordered table-striped table-hover" id="dtModule" width="100%">
                        <colgroup>
                            <col width="5%">
                            <col width="95%">
                        </colgroup>
                        <thead>
                        <tr role="row" class="heading">
                            <th><input type="checkbox" name="select_all" value="1" id="select-all"></th>
                            <th>Tên module</th>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnClose">Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSave">Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/role.js"></script>
    <script src="js/manage/phan_quyen.js"></script>
    <script>
        Role('{{ route('roles.index') }}');
        PhanQuyen('{{ route('roles.get-modules') }}');

        function showFormPhanQuyen(roleId, orgTypeId) {
            $('input[name="organization_type_id"]').val(orgTypeId);
            $('input[name="role_id"]').val(roleId);
            $("#frmPhanQuyen").modal("show");
            $("#btnSearch").trigger('click');
        }

        $("#btnSave").click(function(){
            var roleId = $('input[name="role_id"]').val();

            var sModuleId = '';
            $('input[name="module[]"]:checked').each(function(){
                var value = $(this).val();
                sModuleId += ',' + value;
            });

            if(sModuleId.length > 0)
                sModuleId = sModuleId.substring(1);

            var scope = $('.box-header #organization_type_id').val();

            $.ajax({
                url: '{{route('roles.update-module-roles')}}',
                method: 'POST',
                data: {'module_ids': sModuleId, 'role_id': roleId, 'scope': scope},
                dataType: 'json',
            }).done(function (rs) {
                if (rs.success) {
                    $("#btnClose").trigger('click');
                    $('#dtModule').DataTable().ajax.reload(null, false);
                }
                sdcApp.showLoading(false);
            }).fail(function () {
                sdcApp.showLoading(false);
            });
        });

        // Handle click on "Select all" control
        $('#select-all').on('click', function(){
            // Check/uncheck checkboxes for all rows in the table
            $('input[name="module[]"]').prop('checked', this.checked);
        });

        function chkModuleChange()
        {
            var rowChecked = $('input[name="module[]"]:checked').length;
            var numberOfRow = $('input[name="module[]"]').length;
            $('#select-all').prop('checked', rowChecked == numberOfRow);
        }
    </script>
@endpush