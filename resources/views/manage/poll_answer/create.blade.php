@extends('layouts.app')

@section('title')
Quản lý Thăm dò ý kiến
@endsection

@section('decription')
Quản lý Thăm dò ý kiến
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-offset-2 col-md-8">
<?php Former::populate($pollAnswer); ?>
@if($pollAnswer)
    {!! Former::open(route('pollAnswer.update', $pollAnswer->id), 'put') !!}
@else
    {!! Former::open(route('pollAnswer.store')) !!}
@endif

         {!! Former::text('name', 'Nội dung ý kiến:')->required() !!}
         {!! Former::number('no','Thứ tự:')->required() !!}
        {!! Former::checkbox('public', 'Xuất bản') !!}
    <div class="text-center">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-save"></i>
                Cập nhật
            </button>
        <button type="button" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-reply"></i>
            <a style="color: #ffff" href="manage/poll"> Trở lại </a>
        </button>
    </div>
{!! Former::close() !!}
    <!-- /end form -->
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
@endsection
