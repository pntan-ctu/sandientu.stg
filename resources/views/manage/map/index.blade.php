@extends('layouts.app')

@section('title')
Bản đồ cơ sở sản xuất và cơ sở kinh doanh
@endsection

@section('decription')
Vị trí trên google map
@endsection

@section('content')

<div class="box box-info">
    <div class="form-group">
                <div class="col-lg-6">
                    @include("components/select_search",['tree'=>$organization100,"level"=>0,"path"=>null,"root"=>'--Tất cả--','parent_id'=>null,'name'=>'cboCososx'])
                </div>
                <div class="col-lg-6">
                    @include("components/select_search",['tree'=>$organization101,"level"=>0,"path"=>null,"root"=>'--Tất cả--','parent_id'=>null,'name'=>'cboCosokd'])
                </div>
    </div>
<!--    <div class="row"> 
        <div class="col-lg-6 box-tools pull-right">
            <div class="form-group">
                <select class="form-control select2" name="cososanxuat" aria-hidden="true">
                    <option value="0">-Cơ sở sản xuất-</option>
                </select>
            </div>
        </div>
        <div class="col-lg-6 box-tools pull-right">
            <div class="form-group">
                <select class="form-control select2" name="cosokinhdoanh" aria-hidden="true">
                    <option value="0">-Cơ sở kinh doanh-</option>
                </select>
            </div>
        </div>
    </div>-->
    <div class="row"> 
        <div class="col-lg-12">
            <div id="googleMap" style="width:100%; height: 1000px; margin:3px;">Google Map</div>    
        </div>

    </div>
<?php // print_r($organization)?>
</div>
@endsection

@push('scripts')
<script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBtm_IVH680qlXf1N7sxKSrkLMM93KCl3c&sensor=false"></script>
<script>
    var cu = <?php echo($organization100); ?>;
    var gmap = new google.maps.LatLng(10.765974, 106.689422);
    var marker;
    function initialize()
    {
        var locations = [
            ['<b>Bondi Beach</b>', -33.890542, 151.274856, 4],
            ['Coogee Beach', -33.923036, 151.259052, 5],
            ['Cronulla Beach', -34.028249, 151.157507, 3],
            ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
            ['Maroubra Beach', -33.950198, 151.259302, 1]
        ];

        var mapProp = {
            center: new google.maps.LatLng(-33.890542, 151.274856),
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.Lite
        };

        var map = new google.maps.Map(document.getElementById("googleMap")
                , mapProp);

        var styles = [
            {
                featureType: 'road.arterial',
                elementType: 'all',
                stylers: [
                    {hue: '#fff'},
                    {saturation: 100},
                    {lightness: -48},
                    {visibility: 'on'}
                ]
            }, {
                featureType: 'road',
                elementType: 'all',
                stylers: [

                ]
            },
            {
                featureType: 'water',
                elementType: 'geometry.fill',
                stylers: [
                    {color: '#adc9b8'}
                ]
            }, {
                featureType: 'landscape.natural',
                elementType: 'all',
                stylers: [
                    {hue: '#809f80'},
                    {lightness: -35}
                ]
            }
        ];

        var styledMapType = new google.maps.StyledMapType(styles);
        map.mapTypes.set('Styled', styledMapType);

        var infowindow = new google.maps.InfoWindow();

        var marker, i;

        for (i = 0; i < locations.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    infowindow.setContent(locations[i][0]);
                    infowindow.open(map, marker);
                }
            })(marker, i));
        }

//        marker = new google.maps.Marker({
//            map: map
//            draggable: true,
//            animation: google.maps.Animation.DROP,
//            position: gmap
//        });
//        google.maps.event.addListener(marker, 'click', toggleBounce);

    }

    function toggleBounce() {

        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endpush
