@extends('layouts.app')

@section('content')
    <div class="box">
        <div class="box-body">
            <?php Former::populate($user); ?>
            @if($user)
                {!! Former::open(route('users.update', $user->id), 'put') !!}
            @else
                {!! Former::open(route('users.store')) !!}
            @endif
            {!! Former::text('name', 'Họ tên') !!}
            {!! Former::text('email') !!}
            {!! Former::password('password', 'Mật khẩu') !!}
            {!! Former::password('password_confirmation', 'Xác nhận mật khẩu') !!}
            {!! Former::checkbox('active', 'Kích hoạt') !!}
            <?php
            $formActions = Former::actions()->primary_submit('Lưu');
            $formActions->default_link('Đóng', route('users.index'));
            ?>
            {!! $formActions !!}
            {!! Former::close() !!}
        </div>
    </div>
@endsection
