@extends('layouts.app')

@section('title')
    <a class="title" href="{{route('organization.index', ['organizationTypeId'=>$orgTypeModel->id])}}">
        Quản lý {{$orgTypeModel->name}}
    </a>
@endsection

@section('decription')
@endsection

@push('styles')
    <style>
        td {
            vertical-align: middle !important;
            text-align: center !important;
        }

        .title-step {
            font-weight: bold;
            font-size: 20px;
            color: #d91c5f;
        }

        .fa-icon {
            width: 14px;
        }

        .right-container {
            flex: 1;
            padding-left: 10px;
        }

        .left-container {
            /*width: 200px;*/
            text-align: center;
        }

        .form-container {
            width: 100%;
        }

        .btn-file {
            margin-top: 5px;
            width: 200px;
        }

        .image_avatar>img {
            border: solid 1px gainsboro;
            padding: 4px;
        }

    </style>
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
@endpush
@section('content')
    <div class="box" id="box-product">
        <div class="box-header">
            <div class="col-lg-12 title-step rm-padding" id="title">
                {{isset($organization)?"Sửa thông tin":"Thêm mới"}}
            </div>
        </div>
        <div id="primary_info_container">
            <!-- /.box-header -->
            <div class="box-body">
                <?php Former::populate($organization) ?>
                @if($organization)
                    {!! Former::horizontal_open_for_files(route('organization.update', ['organizationTypeId'=>$orgTypeModel->id, 'id'=>$organization->id]),'put')->novalidate() !!}
                @else
                    {!! Former::horizontal_open_for_files(route('organization.store', ['organizationTypeId'=>$orgTypeModel->id]))->novalidate() !!}
                @endif
                <div class="form-container">
                    {{--upload anh--}}
                    <div class="left-container col-md-3 col-xs-12  {{$errors->has('logo_image')?'has-error':''}} ">
                        <div class="image_avatar">
                            <img width="200" height="200"
                                src={{ isset($organization->logo_image)?url('image/200/200/'.$organization->logo_image):url('img/no-image.png') }} >
                        </div>
                        <div class="btn bg-maroon btn-file">
                            <i class="fa fa-picture-o"></i> Ảnh thương hiệu
                            <input type="file" class="form-control" id="image_upload_input" name="logo_image"/>
                        </div>
                        <span class="help-block">{{$errors->first('logo_image')}}</span>
                    </div>

                    <div class="right-container  col-md-9 col-xs-12">
                        {!! Former::setOption('TwitterBootstrap3.labelWidths', ['large' => 3, 'small' => 4]) !!}
                        @if($orgTypeModel->id == OrgType::CSSX)
                            {!! Former::text('name','Tên cơ sở sản xuất:')->required() !!}
                        @elseif($orgTypeModel->id == OrgType::CSKD)
                            {!! Former::text('name','Tên cơ sở kinh doanh:')->required() !!}
                        @endif
                        @if($orgTypeModel->id == OrgType::CSSX)
                            {!! Former::select("area_id","Vùng sản xuất:")->addOption('--- Chọn vùng sản xuất ---', '')->fromQuery($areaAll,'name','id') !!}
                        @elseif($orgTypeModel->id == OrgType::CSKD)
                            {!! Former::select("commercial_center_id","Địa điểm kinh doanh:")->addOption('--- Chọn địa điểm kinh doanh ---', '')->fromQuery($commerAll,'name','id') !!}
                        @endif
                        <div class="form-group required {{$errors->has('region_id')?'has-error':''}} ">
                            <label for="region_id" class="control-label col-lg-3 col-sm-4">Tỉnh, huyện, xã:
                                <sup>*</sup></label>
                            <div class="col-lg-9 col-sm-8 tree-search">
                                @include("components/select_search",['tree'=>$regions,"level"=>0,"path"=>null,"root"=>null,
                                'parent_id'=>(isset($organization)?$organization->region_id:null),'name'=>'region_id',
                                'placeholder'=>'Hãy chọn địa phương quản lý'])
                            </div>
                        </div>
                        {!! Former::text('address','SN/thôn/xóm:')->prepend('<i class="fa fa-home fa-icon"></i>')->required()  !!}
                        {!! Former::text('tel','Điện thoại:')->addClass('tel')->prepend('<i class="fa fa-phone fa-icon"></i>')->required() !!}
                        {!! Former::radios('Ngành quản lý:')->radios($departments)->inline()->required()!!}
                        {!! Former::select("founding_type","Loại giấy tờ:")->addOption('--- Chọn loại giấy tờ ---', '')->fromQuery($founding_type,'name','id')->required() !!}
                        {!! Former::text('founding_number','Giấy tờ số:')->addClass('founding_number')->required() !!}
                        {!! Former::text('founding_by_gov','Cơ quan cấp phép:')->addClass('founding_by_gov')->required() !!}
                        {!! Former::text('founding_date','Ngày cấp:')->addClass('founding_date form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->required()
                         ->forceValue(($organization && $organization->founding_date) ? $organization->founding_date->toShortDateString() : null) !!}
                        {!! Former::text('director','Người đại diện:')->addClass('director')->prepend('<i class="fa fa-user fa-icon"></i>')->required() !!}
                        {!! Former::select("organization_level_id","Quy mô:")->addClass('organization_level_id')->addOption('--- Chọn quy mô ---', '')->fromQuery($organizationLevel,'name','id')->required() !!}
                        {!! Former::text('email','Email:')->addClass('email')->prepend('<i class="fa fa-envelope-o fa-icon"></i>') !!}
                        {!! Former::text('website','Website:')->addClass('website')->prepend('<i class="fa fa-globe fa-icon"></i>') !!}
                        {!! Former::text('fanpage','FanPage:')->addClass('fanpage')->prepend('<i class="fa fa-facebook fa-icon"></i>') !!}
                        {!! Former::text('map','Bản đồ:')->addClass('map')->prepend('<i class="fa  fa-map-marker fa-icon"></i>')->placeholder("19.808241, 105.777592") !!}
                        {!! Former::select('status','Trạng thái:')->fromQuery($status,'name','id') !!}

                    </div>

                    <div style="padding-left: 15px; padding-right: 15px;">
                        <a style="color: #ffff" href="{{route('organization.index', ['organizationTypeId'=>$orgTypeModel->id])}}">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                <i class="fa fa-reply"></i>
                                Quay lại danh sách
                            </button>
                        </a>
                        <div class="right-button">
                            <button type="submit" class="btn btn-success">
                                <i class="fa  fa-save"></i>
                                Lưu thông tin
                            </button>
                        </div>
                    </div>

                </div>
                {!! Former::close() !!}
            </div>
        </div>
    </div>

@endsection
@push('scripts')
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script src="js/manage/organization.js"></script>
    <script>

        $(function () {
            var organization = Organization('{{ route('product.create', ['organizationTypeId' => $orgTypeModel->id]) }}');
            organization.init_create("{{$orgTypeModel->id}}");
        })
    </script>

@endpush
