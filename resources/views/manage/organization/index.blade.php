@extends('layouts.app')
@section('title')
    Quản lý {{$orgTypeModel->name}}
@endsection

@section('decription')
@endsection

@push('styles')
    <style>
        td {
            vertical-align: middle !important;
        }

        #search-form {
            padding: 0px;
        }
        #keyword{
            min-width: 200px;
        }
        td .fa{font-size: 18px; margin-top: 2px}
        td .fa-flickr{color: blue}
        td .fa-times{color: red}
        .modal-body>* {float: left}
        .modal-body>div{width: calc(100% - 120px);margin-left: 15px}
        #forwardOrgName, #forwardTenCS, #forwardAddressCS{margin-left: 10px}
        label[for="status"]{margin: 0px 10px 0px 30px}
        label[for="keyword"]{margin: 0px 10px}
    </style>
@endpush

@section('content')
    <div class="box" id="box-product-category">
        <div class="box-header">
            <form method="POST" id="search-form" class="form-inline pull-left" role="form">
                <div class="form-group">
                    <label for="keyword">Từ khoá</label>
                    <input type="text" class="form-control" name="keyword" id="keyword" placeholder="Nhập từ khoá cần tìm">
                </div>
                {!! Former::select('status', 'Trạng thái')->options(['-1' => '-- Tất cả --', 0 =>'Chưa duyệt', 1=>'Đang hoạt động', 2=>'Ngừng hoạt động', 3=>'Khóa do vi phạm']) !!}
                {!! Former::primary_submit('Tìm kiếm')->id('search') !!}
            </form>
            <div class="right-button">
                <a type="button" class="btn btn-success"
                   href="{{ route('organization.create', ['organizationTypeId' => $orgTypeModel->id]) }}">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </a>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="responsive table table-bordered table-striped table-hover" id="datatable" width="100%">
                <thead>
                <tr role="row" class="heading">
                    <th class="all">Tên cơ sở SXKD</th>
                    <th class="min-mobile-p">Địa chỉ</th>
                    <th class="min-tablet-p">Cơ quan quản lý</th>
                    <th class="min-tablet-p">Trạng thái</th>
                    <th class="min-tablet-p"></th>
                </tr>
                </thead>
            </table>
        </div>
        <div class="modal fade in" id="modal-move-org" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" style="font-weight: bold">Chuyển cơ quan quản lý</h4>
                    </div>
                    <div class="modal-body row" style="padding: 30px;">
                        <label>Chọn cơ quan: </label>
                        <div>
                            @include("components/select_search",['tree'=>$TreeOrgGov,"level"=>0,"path"=>null,"root"=>null,
                             'parent_id'=>null,'name'=>'org_id'])
                        </div>
                        <input type="hidden" id="destinationManageOrgId" value="0">
                        <input type="hidden" id="currentOrgId" value="0">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnSave">
                            <i class="fa fa-check"></i> Lưu
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <i class="fa fa-times"></i> Đóng
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade in" id="modal-resolve-move" style="display: none">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" style="font-weight: bold">Xử lý tiếp nhận</h4>
                    </div>
                    <div class="modal-body row">
                        <div>Tên cơ sở:<label id="forwardTenCS">Tên cơ sở</label></div>
                        <div>Địa chỉ:<label id="forwardAddressCS">Địa chỉ cơ sở</label></div>
                        <div>Cơ quan chuyển:<label id="forwardOrgName">Tên cơ quan chuyển</label></div>
                        <input type="hidden" id="currentForwardOrgId" value="0">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="btnAccept">
                            <i class="fa fa-check"></i> Tiếp nhận
                        </button>
                        <button type="button" class="btn btn-primary" id="btnDecline">
                            <i class="fa fa-hand-paper-o"></i> Không tiếp nhận
                        </button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">
                            <i class="fa fa-times"></i> Đóng
                        </button>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/organization.js"></script>
    <script>
        $(function () {
            var organization = Organization('{{ route('organization.index', ['organizationTypeId'=>$orgTypeModel->id]) }}', '');
            organization.init_index();

            $('select[name="org_id"]').change(function(){
                $('#destinationManageOrgId').val(this.value);
            });
        });

        function showFormMove(orgId)
        {
            $('#currentOrgId').val(orgId);
            $("#select2-org_id-container").text('-- Chọn cơ quan tiếp nhận --');
            $('#modal-move-org').modal();
        }

        $("#btnSave").click(function(){
            var currentOrgId = $('#currentOrgId').val();
            var destinationManageOrgId = $('#destinationManageOrgId').val();

            if(!(destinationManageOrgId > 0)) {
                toastr.error("Bạn chưa chọn cơ quan quản lý chuyển đến");
                return;
            }

            $.ajax({
                url: '{{url('manage/organization/move')}}',
                type: 'post',
                data: {
                    currentOrgId: currentOrgId,
                    toOrgId: destinationManageOrgId
                },
                success: function (result) {
                    $("#modal-move-org").modal("hide");
                    toastr.success("Chuyển tiếp thành công");
                    $('#datatable').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        });

        function cancelMove(orgId)
        {
            $.ajax({
                url: '{{url('manage/organization/cancelMove')}}',
                type: 'post',
                data: {
                    orgId: orgId
                },
                success: function (result) {
                    toastr.success("Hủy chuyển tiếp thành công");
                    $('#datatable').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        }

        function showFormResolveMove(orgId)
        {
            $.ajax({
                url: '{{url('manage/organization/getOrgInfo')}}',
                type: 'post',
                data: {
                    orgId: orgId
                },
                success: function (result) {
                    $('#currentForwardOrgId').val(orgId);
                    $('#forwardTenCS').text(result.ten_cs);
                    $('#forwardAddressCS').text(result.dia_chi);
                    $('#forwardOrgName').text(result.ten_co_quan);
                    $('#modal-resolve-move').modal();
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        }

        function resolveMove(choice){
            var currentOrgId = $('#currentForwardOrgId').val();
            $.ajax({
                url: '{{url('manage/organization/resolveMove')}}',
                type: 'post',
                data: {
                    orgId: currentOrgId,
                    choice: choice
                },
                success: function (result) {
                    $("#modal-resolve-move").modal("hide");
                    toastr.success("Đã xử lý");
                    $('#datatable').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        }

        $("#btnAccept").click(function(){
            resolveMove(1);
        });

        $("#btnDecline").click(function(){
            resolveMove(0);
        });
    </script>
@endpush