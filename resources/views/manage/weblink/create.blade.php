<style type="text/css">
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    sup{
        color: #EA3838 !important;
    }
</style>
<?php Former::populate($weblink); ?>
@if($weblink)
    {!! Former::open(route('weblink.update', $weblink->id), 'put' ) !!}
@else
    {!! Former::open(route('weblink.store')) !!}
@endif
{!! Former::text('title', 'Tiêu đề :')->required() !!}
{!! Former::text('url','Link :')->required() !!}
{!! Former::number('no','Thứ tự :')->required() !!}
<div class="form-group">
    <label class ="control-label col-lg-2 col-sm-4">Ảnh đại diện :</label>
    <div class="col-lg-10 col-sm-8">
        <img id="logo-img" title="Chọn ảnh đại diện" onclick="document.getElementById('add-new-logo').click();"
            @if(isset($weblink->img_path) && $weblink->img_path)
                src="{{ url('image/200/200/'.$weblink->img_path) }}"
            @else
                src="{{ asset('css/images/no-image.png') }}"
            @endif
        />
    <input type="file" style="display: none" id="add-new-logo" name="avatar" accept="image/*" onchange="addNewLogo(this)"/>
    </div>    
</div>
{!! Former::select("group_id","Nhóm hiển thị :")->fromQuery($group_id,'name','id') !!}
{!! Former::checkbox('active', 'Hiển thị trên website') !!}
{!! Former::checkbox('new_window', 'Mở cửa sổ mới') !!}
{!! Former::close() !!}
<script type="text/javascript">
    var addNewLogo = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Hiển thị ảnh vừa mới upload lên
                $('#logo-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            //submit form để upload ảnh
        }
    }
</script>