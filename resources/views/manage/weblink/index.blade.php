@extends('layouts.app')

@section('title')
    Quản lý Liên kết Web
@endsection

@section('decription')
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select("active","Nhóm hiển thị :")->addOption('[ Tất cả ]', '')->fromQuery($group_id,'name','id') !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
                
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="20%">
                    <col width="15%">
                    <col width="15%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Ảnh liên kết</th>
                    <th>Tên Website</th>
                    <th>Địa chỉ</th>
                    <th>Hiển thị</th>
                    <th>Cửa sổ mới</th>
                    <th>Người tạo</th>
                    <th>Thứ tự</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/weblinks.js"></script>
    <script>
        Weblink('{{ route('weblink.index') }}');
    </script>
@endpush