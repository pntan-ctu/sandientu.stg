<?= Former::vertical_open_for_files()->method('PUT')->action(route('docorgan.update', ['id' => $docOrgan->id])) ?>
<?= Former::text('name')->label('Tên danh mục : ')->autofocus()->required()->value($docOrgan->name) ?>
<?= Former::number('no')->min(0)->label('Số thứ tự : ')->required()->value($docOrgan->no) ?>
<?= Former::checkbox('active')->label('Kích hoạt : ')->inline()->check($docOrgan->active == 1 ? true : false) ?>
<?= Former::close() ?>
   