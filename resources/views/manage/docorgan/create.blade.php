
<?php
Former::populate($docOrgan);
$rules = ['name' => 'required|max:255',
    'no' => 'required'];
?>

{!! Former::open(route('docorgan.store'))->rules($rules); !!}
{!! Former::text('name', 'Cơ quan ban hành') !!}
{!! Former::number('no', 'Số thứ tự') !!}
{!! Former::checkbox('active', 'Kích hoạt') !!}
{!! Former::close() !!}
