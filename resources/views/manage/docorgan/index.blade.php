@extends('layouts.app')

@section('title')
Quản lý danh mục
@endsection

@section('decription')
Cơ quan ban hành
@endsection

@section('content')
<div class="box" id="box-area">
    <div class="box-header">
        <form method="POST" id="search-form" class="form-inline pull-left" role="form">
            <div class="form-group">
                <label for="keyword">Từ khoá</label>
                <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
            </div>
            <button type="submit" class="btn btn-primary">Tìm kiếm</button>
        </form>
        <div class="right-button">
            <button id="btn-add" type="button" class="btn btn-success">
                <i class="fa fa-plus"></i>
                Thêm mới
            </button>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
            <colgroup>
                <col width="45%">
                <col width="25%">
                <col width="20%">
                <col width="10%">
            </colgroup>
            <thead>
                <tr role="row" class="heading">
                    <th>Cơ quan ban hành</th>
                    <th>Thứ tự</th>
                    <th>Trạng thái</th>
                    <th></th>
                </tr>
            </thead>
        </table>
    </div>
</div>
@endsection
@push('scripts')
<script src="js/sdc-crud.js"></script>
<script src="js/manage/document-organ.js"></script>
<script>
    DocumentOrgan('{{ route('docorgan.index') }}');
</script>
@endpush