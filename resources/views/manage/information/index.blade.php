@extends('layouts.app')

@section('title')
    Thông tin tổ chức
@endsection

@section('decription')
    Cung cấp thông tin liên hệ cho người truy cập
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-body">
            <form method="post" action="manage/information">
                <input type="hidden" name="_token" value="{{csrf_token()}}">
                <input type="hidden" name="id" value="{{$information["id"]}}">
                 @if(isset($information))
                <div class="form-horizontal  form-container">
                    <div class="form-group">
                        <label for="name" class="control-label col-lg-3 col-sm-4">Tên CQ/tổ chức: </label>
                        <div class="col-lg-6 col-sm-8">
                            <input class="form-control" readonly="" id="name" type="text" name="name" value="{{$information["name"]}}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="region_id" class="control-label col-lg-3 col-sm-4">Địa phương phụ trách: </label>
                        <div class="col-lg-6 col-sm-8 tree-search">
                             <input class="form-control" readonly="" id="website" type="text" name="website" value="{{$sUrl}}">    
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="region_id" class="control-label col-lg-3 col-sm-4">Địa chỉ liên hệ: </label>
                        <div class="col-lg-6 col-sm-8 tree-search">
                             <input class="form-control" id="address" type="text" name="address" required value="{{$information["address"]}}">    
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="region_id" class="control-label col-lg-3 col-sm-4">Điện thoại liên hệ: </label>
                        <div class="col-lg-6 col-sm-8 tree-search">
                            <input class="form-control" id="tel" type="number" name="tel" required value="{{$information["tel"]}}">    
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="region_id" class="control-label col-lg-3 col-sm-4">Thư điện tử (email): </label>
                        <div class="col-lg-6 col-sm-8 tree-search">
                             <input class="form-control" id="email" type="text" name="email" required value="{{$information["email"]}}">    
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="region_id" class="control-label col-lg-3 col-sm-4">Tọa độ bản đồ: </label>
                        <div class="col-lg-6 col-sm-8 tree-search">
                             <input class="form-control" id="map" type="text" name="map" required value="{{$information["map"]}}">    
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="text-center">
                        <button type="submit" class="btn btn-success">
                            <i class="fa fa-save"></i>
                            Lưu thay đổi
                        </button>
                    </div>
                </div>
                 @endif
            </form>

                
        </div>
        <!-- /.box-body -->
    </div>
@endsection