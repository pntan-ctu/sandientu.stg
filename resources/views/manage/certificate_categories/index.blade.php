@extends('layouts.app')

@section('title')
    Quản lý loại chứng nhận, xác nhận
@endsection

@section('decription')
@endsection

@section('content')
    <style>
        .wysihtml5-sandbox{ width: 100% !important;}
    </style>
    <div class="box" id="box-area">
        <div class="box-header">
            <form method="POST" id="search-form" class="form-inline pull-left" role="form">
                <div class="form-group">
                    <label for="keyword">Từ khoá</label>
                    <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
                </div>
                <div class="form-group">
                    <label for="type">Đối tượng</label>
                    <select name="type" class="form-control">
                        <option value="-1">Tất cả</option>
                        <option value="0">Cho tổ chức</option>
                        <option value="1">Cho sản phẩm</option>
                    </select>
                </div>

                <button type="submit" class="btn btn-primary">Tìm kiếm</button>
            </form>
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="10%">
                    <col width="45%">
                    <col width="15%">
                    <col width="20%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Biểu tượng</th>
                    <th>Loại chứng nhận, xác nhận</th>
                    <th>Cấp độ</th>
                    <th>Ngày tạo</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
<script src="js/sdc-crud.js"></script>
<script src="js/manage/cert_cate.js"></script>
<script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
<script>
    $(function () {
        sdcApp.fixCKEditorModal();
        Cert_cate('{{ route('certificates-category.index') }}');
    })

</script>
@endpush