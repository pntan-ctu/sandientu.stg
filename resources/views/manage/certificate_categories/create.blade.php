<style type="text/css">
    #logo-img{ width: 100px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    sup{
        color: #EA3838 !important;
    }
</style>
<?php Former::populate($cert_cate); ?>
@if($cert_cate)
    {!! Former::open(route('certificates-category.update', $cert_cate->id), 'put') !!}
@else
    {!! Former::open(route('certificates-category.store')) !!}
@endif
{!! Former::text('name', 'Tên :') !!}
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Đối tượng : </label>
    <div class="col-lg-10 col-sm-8">
        <select class="form-control" name="type" aria-hidden="true">
                <option value="0" @if(isset($cert_cate->type) && $cert_cate->type == 0)selected="selected"@endif>Cho tổ chức</option>
                <option value="1" @if(isset($cert_cate->type) && $cert_cate->type == 1)selected="selected"@endif>Cho sản phẩm</option>
        </select>
    </div>
</div>
{!! Former::number('rank', 'Cấp độ :') !!}
{!! Former::textarea('description','Thông tin :')->rows(4)  !!}
<!-- Longdv comment chuyen sang textarea o tren
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Thông tin : </label>
    <div class="col-lg-10 col-sm-8">
        <textarea class="ckeditor" name="description" id="my-editor">@if(isset($cert_cate->description)) {!! html_entity_decode($cert_cate->description) !!} @endif</textarea>
    </div>
</div> -->
<div class="form-group">
    <label class ="control-label col-lg-2 col-sm-4">Biểu tượng :</label>
    <div class="col-lg-10 col-sm-8">
                 <img id="logo-img" title="Chọn ảnh đại diện" onclick="document.getElementById('add-new-logo').click();"
                @if(isset($cert_cate->icon) && $cert_cate->icon)
                    src="{{ url('image/100/100/'.$cert_cate->icon) }}"
                @else
                    src="{{ asset('css/images/no-image.png') }}"
                @endif
        />
        <input type="file" style="display: none" id="add-new-logo" name="icon" accept="image/*" onchange="addNewLogo(this)"/>
    </div>    
</div>
{!! Former::close() !!}
<script type="text/javascript">
    var addNewLogo = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Hiển thị ảnh vừa mới upload lên
                $('#logo-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            //submit form để upload ảnh
        }
    }
</script>