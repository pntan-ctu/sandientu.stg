@extends('layouts.app')

@section('title')
Thống kê gửi SMS
@endsection

@section('decription')

@endsection

@section('content')
<div class="box" id="box-users">
    <div class="box-header">
        <form action="manage/sms/search" method="POST" id="search-form" class="form-inline pull-left" role="form">
            <input type="hidden" name="_token" value="{{csrf_token()}}">
            <div class="form-group">
                <label for="fromdate">Từ ngày</label>
                <input type="date" class="form-control" name="fromdate" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <label for="todate">Đến ngày</label>
                <input type="date" class="form-control" name="todate" />
            </div>
            <button type="submit" class="btn btn-primary">Thống kê</button>
        </form>
        <div class="right-button">
            <a type="button" class="btn Normal btn-success" href="{{'manage/sms'}}">
                <i class="fa fa-plus"></i>
                Gửi SMS
            </a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
            <colgroup>
                <col width="5%">
                <col width="55%">
                <col width="15%">
                <col width="15%">
                <col width="20%">
            </colgroup>
            <thead>
                <tr role="row" class="heading">
                    <th>STT</th>
                    <th>Nội dung</th>
                    <th>Số nhận</th>
                    <th>Ngày gửi</th>
                    <th>Số lượng SMS</th>
                </tr>
            </thead>
        </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/list_sms.js"></script>
    <script>
        //SmsHistory('manage/sms_history');
        SmsHistory('{{ route('sms-history.index') }}');
    </script>
@endpush