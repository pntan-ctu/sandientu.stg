@extends('layouts.app')

@section('title')
    Quản lý bình luận tin tức
@endsection

@section('decription')

@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('active', 'Trạng thái')->options(['-1' => 'Tất cả', '1' => 'Đã duyệt', '0' => 'Chưa duyệt']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="dtNewsComments" width="100%">
                <colgroup>
                    <col width="23%">
                    <col width="22%">
                    <col width="15%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Tin tức</th>
                    <th>Nội dung bình luận</th>
                    <th>Người bình luận</th>
                    <th>Email</th>
                    <th>Ngày đăng</th>
                    <th>Trạng thái</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/news_comment.js"></script>
    <script>
        NewsComments('manage/news-comments');

        function setActive(iCommentId) {
            $.ajax({
                url: 'manage/news-comments/approve/' + iCommentId,
                success: function () {
                    toastr.success("Bạn đã duyệt bình luận.");
                    $('#dtNewsComments').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        }

        function setInactive(iCommentId) {
            $.ajax({
                url: 'manage/news-comments/disapprove/' + iCommentId,
                success: function () {
                    toastr.success("Bạn đã bỏ duyệt bình luận.");
                    $('#dtNewsComments').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        }
    </script>
@endpush