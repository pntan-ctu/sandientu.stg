{!! Former::populate($newsComment); !!}
{!! Former::vertical_open(route('news-comments.update', $newsComment->id), 'put') !!}
{!! Former::text('name', 'Họ tên')->required() !!}
{!! Former::text('email', 'Email')->required() !!}
{!! Former::textarea('comment', 'Nội dung bình luận')->setAttribute('style', 'height: 100px')->required() !!}
{!! Former::close() !!}