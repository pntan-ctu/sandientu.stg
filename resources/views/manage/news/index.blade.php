@extends('layouts.app')

@section('title')
    Quản lý tin bài
@endsection

@section('description')
    (some text description)
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="plugins/easyui/icon.css">
    <link rel="stylesheet" type="text/css" href="plugins/easyui/easyui.css">
    <link rel="stylesheet" type="text/css" href="plugins/dropdown-tree/resources/dropdowntree.css">
    <style>
        #datatable2_wrapper>div:nth-child(2)>div{max-height: 300px !important;overflow: auto !important;}
    </style>
@endpush

@section('content')
    <div class="box" id="box-news-group">
        <div class="box-header">
            <form method="POST" id="search-form" class="form-inline pull-left" role="form" style="width: calc(100% - 300px);">
                <div class="form-group" style="width: calc(100% - 280px);">
                    <label for="keyword" style="width: 88px">Từ khoá</label>
                    <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm" style="width: calc(100% - 100px);">
                </div>
                <div class="form-group">
                    <label for="active" style="width: 70px;margin-left: 20px">Trạng thái</label>
                    <select name="active" class="form-control">
                        <option value="-1">Tất cả</option>
                        <option value="1">Xuất bản</option>
                        <option value="0">Soạn thảo</option>
                    </select>
                </div>
                <div style="display: flex;align-items: center;margin-top: 5px">
                    <label required style="width: 100px">Chuyên mục</label>
                    <div class="dropdown dropdown-tree" id="treeNewsGroups" style="float: right;width: calc(100% - 106px);margin-right: 10px"></div>

                <input type="hidden" name="sGroupId" id="sGroupId" value="2">
                <button type="submit" id="btnSearchNews" class="btn btn-primary">Search</button></div>
            </form>
            <div class="right-button">
                <a type="button" class="btn Normal btn-success" href="{{route('news.create')}}">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </a>

            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="5%">
                    <col width="29%">
                    <col width="22%">
                    <col width="12%">
                    <col width="10%">
                    <col width="10%">
                    <col width="6%">
                    <col width="6%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>STT</th>
                    <th>Tiêu đề</th>
                    <th>Chuyên mục</th>
                    <th>Người đăng</th>
                    <th>Ngày đăng</th>
                    <th>Trạng thái</th>
                    <th>Tin liên quan</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="modal fade in" id="frmRelateNews" style="display: none; padding-right: 17px">
        <div class="modal-dialog" style="width: 1000px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title">Chọn tin tức liên quan</h4>
                </div>
                <div class="modal-body row">
                    <div class="col-md-3" style="border-right: 1px solid gainsboro; overflow: auto; max-height: 370px">
                        <label style="margin-right: 5px">Chuyên mục tin</label>
                        <ul id="treeNewsGroupsRelate" checkbox="true" class="easyui-tree"></ul>
                    </div>
                    <div class="col-md-9">
                        <form method="get" id="frmRelateNews" class="form-inline pull-left" role="form">
                            <div class="form-group">
                                <input type="hidden" name="newsId" value="0" />
                                <input type="hidden" name="sGroupRelateId" value="2" />
                                <input type="hidden" name="sRelateId" value="0" />
                            </div>
                            <div class="form-group">
                                <label for="keyword" style="margin-right: 8px">Từ khoá</label>
                                <input type="text" class="form-control" name="keyword" placeholder="Nhập từ khoá cần tìm">
                            </div>
                            <button type="submit" id="btnSearchRelateNews" class="btn btn-primary" style="margin-left: 10px">Tìm kiếm</button>
                        </form>
                        <table class="table table-bordered table-striped table-hover" id="datatable2" width="100%">
                            <colgroup>
                                <col width="5%">
                                <col width="35%">
                                <col width="50%">
                            </colgroup>
                            <thead>
                            <tr role="row" class="heading">
                                <th></th>
                                <th>Chuyên mục</th>
                                <th>Tiêu đề</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCloseRelateNews">Đóng</button>
                    <button type="button" class="btn btn-primary" id="btnSaveRelateNews">Lưu</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/news.js"></script>
    <script src="js/manage/relate_news.js"></script>
    <script src="plugins/easyui/jquery.easyui.min.js"></script>
    <script src="plugins/dropdown-tree/resources/dropdowntree.js"></script>
    <script>
        News('{{ route('news.index') }}');
        RelateNews('{{ route('news.get-relate-news') }}');

        /*-------------- combobox tree checkbox: select news group ---------------*/
        var options = {
            title : 'Trang chủ',
            data: {!! $treeGroupData !!},
            maxHeight: 1000,
            selectChildren : true,
            clickHandler: function(element){
                $("#treeNewsGroups").SetTitle(element[0].dataset.url);
                $("#treeNewsGroups").click();
                $("input[name=sGroupId]").val(element[0].dataset.value);
                $("#btnSearchNews").trigger('click');
            },
            checkHandler: function(element){
                $("#treeNewsGroups").SetTitle($(element).find("a").first().text());
            },
            closedArrow: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
            openedArrow: '<i class="fa fa-caret-down" aria-hidden="true"></i>',
            multiSelect: false
        };

        $("#treeNewsGroups").DropDownTree(options);
        $("#treeNewsGroups").Expand(0);

        $("select[name=active]").change(function(){
            $("#btnSearchNews").trigger('click');
        });

        /*-------------- tree checkbox: select news groups relate ---------------*/
        $('#treeNewsGroupsRelate').tree({
            data: {!! $treeRelateGroupData !!},
            onCheck: function(node){
                var nodes = $('#treeNewsGroupsRelate').tree('getChecked');
                var sIdGroup = "";
                nodes.forEach(function(node) {
                    sIdGroup += "," + node.id;
                });

                if(sIdGroup.length > 0)
                    sIdGroup = sIdGroup.substring(1);
                else
                    sIdGroup = "0";

                $("input[name=sGroupRelateId]").val(sIdGroup);
                   $("#btnSearchRelateNews").trigger('click');
            }
        });

        function showFormRelateNews(sTitle, iNewsId, iGroupId, sRelateNews)
        {
            $(".modal-title").html("Tin tức liên quan: "+sTitle);
            var node0 = $('#treeNewsGroupsRelate').tree('find', 1);
            $('#treeNewsGroupsRelate').tree('uncheck', node0.target);
            $('#treeNewsGroupsRelate').tree('collapseAll', node0.target);
            $('input[name="newsId"]').val(iNewsId);
            $('input[name="sRelateId"]').val(sRelateNews);
            $('input[name="keyword"]').val('');
            var node = $('#treeNewsGroupsRelate').tree('find', iGroupId);
            //$('#treeNewsGroupsRelate').tree('check', node.target);
            //$('#treeNewsGroupsRelate').tree('select', node.target);
            if(iGroupId > 2) $('#treeNewsGroupsRelate').tree('expandTo', node.target);
            else if(iGroupId == 2)
            {
                var childs = $('#treeNewsGroupsRelate').tree('getChildren', node.target);
                $('#treeNewsGroupsRelate').tree('expandTo', childs[0].target);
            }

            /*var aId = sRelateNews.split(",");
            aId.forEach(function (id) {
                $("input[id=chk"+id+"]").prop('checked', true);
            });*/

            $("#frmRelateNews").modal("show");
            $("#btnSearchRelateNews").trigger('click');
        }

        $("#btnSaveRelateNews").click(function(){
            var iNewsId = $('input[name="newsId"]').val();
            var sIdRelate = '';
            $('input[name="relate-news[]"]:checked').each(function(){
                var value = $(this).val();
                sIdRelate += ',' + value;
            });

            if(sIdRelate.length > 0)
                sIdRelate = sIdRelate.substring(1);

            $.ajax({
                url: '{{route('news.update-relate-news')}}',
                method: 'POST',
                data: {'sIdRelate': sIdRelate, 'iNewsId': iNewsId},
                dataType: 'json',
            }).done(function (rs) {
                if (rs.success) {
                    $("#btnCloseRelateNews").trigger('click');
                    $('#datatable').DataTable().ajax.reload(null, false);
                }
                sdcApp.showLoading(false);
            }).fail(function () {
                sdcApp.showLoading(false);
            });
        });
    </script>

    <!-- CK Editor -->
    <script src="bower_components/ckeditor/ckeditor.js"></script>

@endpush