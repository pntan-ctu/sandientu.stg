@php
    $img_path = isset($news['img_path']) ? $news['img_path'] : '';
    $imgUrl = isset($news['img_path']) ? '/image/100/90/'.$img_path : 'css/images/choose-image.png';
    $group_id = isset($news['group_id']) ? $news['group_id'] : -1;
    $group = isset($news['url']) ? $news['url'] : 'Trang chủ';
@endphp

@extends('layouts.app')

@section('title')
    Nội dung tin bài
@endsection

@section('description')
    (some text description)
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="plugins/easyui/icon.css">
    <link rel="stylesheet" type="text/css" href="plugins/easyui/easyui.css">
    <link rel="stylesheet" type="text/css" href="plugins/dropdown-tree/resources/dropdowntree.css">
    <style>
        #news_avatar{height: 90px; width: 100px; border: 1px solid gray; cursor: pointer; background-size: 100px 90px;}
        #btnRemoveImage{margin: 3px 0px 0px 10px;padding: 3px 8px;}
        label[for="summary"], label[for="txtEditor"], label[for="tags"]{margin-top: 20px}
    </style>
@endpush

@section('content')
    <div class="box">
        <div class="box-body">
            {!! Former::populate(isset($news) ? $news : null); !!}
            <form accept-charset="utf-8" class="form-horizontal" method="POST" action="{{route('news.store')}}"
                  enctype="multipart/form-data" onsubmit="return validate()">
                {{ csrf_field() }}
                {!! Former::hidden('id') !!}
                <div class="row">
                    <div class="col-md-2">
                        <label style="margin-left: 10px">Ảnh đại diện</label>
                        <div id="news_avatar" style="background-image: url('{{ $imgUrl }}')"></div>
                        {!! Former::file('img_path', '', 'true','multipart/form-data')
                            ->setAttribute('style', 'display: none')
                            ->setAttribute('onChange', 'setTextFilename($(this)[0].files);')
                            ->setAttribute('accept', "image/*")
                        !!}
                        <button id="btnRemoveImage" class="btn btn-default" style="margin-left: 7px">
                            <i class="fa fa-times"></i>
                            Xóa ảnh
                        </button>
                        {!! Former::hidden('image_url')->value($img_path)!!}

                    </div>
                    <div class="col-md-10">
                        <div class="form-group required">
                            <label required>Chuyên mục tin</label>
                            <div class="dropdown dropdown-tree" id="treeNewsGroups" style="margin-right: 18px;margin-bottom: 15px;"></div>
                            {!! Former::hidden('group_id')->value(1) !!}
                            {!! Former::text('title', 'Tiêu đề')->setAttribute('maxlength', '500')->setAttribute('style', 'width: 98%') !!}
                        </div>
                    </div>
                </div>
                {!! Former::textarea('summary')->label('Tóm tắt ')->rows(4) !!}
                {!! Former::textarea('content')->id('txtEditor')->label('Nội dung ') !!}
                {!! Former::text('tags', 'Từ khóa')->setAttribute('maxlength', '255') !!}
                <div class="row" style="margin-top: 20px">
                    <div class="col-md-2" style="padding-left: 35px">{!! Former::checkbox('hotnews', 'Tin nóng')->setAttribute('style', 'margin-top: -30px') !!}</div>
                    <div class="col-md-2">{!! Former::checkbox('comment', 'Cho phép bình luận')->setAttribute('style', 'margin-top: -30px') !!}</div>
                    <div class="col-md-2">{!! Former::checkbox('shared', 'Cho phép chia sẻ')->setAttribute('style', 'margin-top: -30px') !!}</div>
                    <div class="col-md-2">{!! Former::checkbox('status', 'Xuất bản')->setAttribute('style', 'margin-top: -30px') !!}</div>
                </div>

                <div style="text-align: right; margin-top: 5px">
                    <!--button type="button" class="btn Normal btn-success" data-toggle="modal" data-target="#modal-relate-news">
                        Tin tức liên quan
                    </button-->
                    <a href="manage/news" class="btn btn-default">
                        <i class="fa fa-times"></i>
                        Thoát
                    </a>

                    <button type="submit" id="btnSave" class="btn btn-primary" >
                        <i class="fa fa-check"></i>
                        Lưu
                    </button>
                </div>
            </form>
        </div>
    </div>


@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="bower_components/ckeditor/ckeditor.js"></script>
    <script src="plugins/easyui/jquery.easyui.min.js"></script>
    <script src="plugins/dropdown-tree/resources/dropdowntree.js"></script>
    <script>
        CKEDITOR.replace('txtEditor');
        CKEDITOR.editorConfig = function( config )
        {
            config.height = '500px';
        };

        //$("#txtEditor").html("<b>Nội dung tin bài</b>");

        $("#news_avatar").click(function (){
            $("#img_path").trigger('click');
        });

        /*-------------- combobox tree checkbox: select news group ---------------*/
        var options = {
            title : '{{ $group }}',
            data: {!! $treeGroupData !!},
            maxHeight: 1000,
            selectChildren : true,
            clickHandler: function(element){
                $("#treeNewsGroups").SetTitle(element[0].dataset.url);
                $("#treeNewsGroups").click();
                $("input[name=group_id]").val(element[0].dataset.value);
            },
            checkHandler: function(element){
                $("#treeNewsGroups").SetTitle($(element).find("a").first().text());
            },
            closedArrow: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
            openedArrow: '<i class="fa fa-caret-down" aria-hidden="true"></i>',
            multiSelect: false
        };

        $("#treeNewsGroups").DropDownTree(options);
        var group_id = {{ $group_id }};
        if(group_id > 0)
            $("#treeNewsGroups").ExpandTo(group_id);
        else
            $("#treeNewsGroups").Expand(group_id);

        function validate()
        {
            var title = $("#title").val().trim();
            if(title.length == 0)
            {
                toastr.error('Bạn chưa nhập tiêu đề')
                return false;
            }

            /*var img_path = $("input[name=img_path]").val().trim();
            var image_url = $("input[name=image_url]").val().trim();
            if(img_path.length == 0 && image_url.length == 0)
            {
                toastr.error("Bạn chưa chọn ảnh đại diện");
                return false;
            }*/

            var summary = $("#summary").val().trim();
            if(summary.length == 0)
            {
                toastr.error("Bạn chưa nhập tóm tắt");
                return false;
            }

            var content = CKEDITOR.instances.txtEditor.getData();//$("#txtEditor").val().trim();
            if(content.length == 0)
            {
                toastr.error("Bạn chưa nhập nội dung");
                return false;
            }

            return true;
        }

        function setTextFilename(files) {
            $("#news_avatar").attr('style', "background-image: url('" + URL.createObjectURL(files[0]) + "')");
        }

        $("#btnRemoveImage").click(function(event){
            event.preventDefault();
            $("#news_avatar").attr('style', "background-image: url('css/images/choose-image.png')");
            $("#img_path").val('');
            $("input[name=image_url]").val('');
        });
    </script>
@endpush