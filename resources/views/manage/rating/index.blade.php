@extends('layouts.app')

@section('title')
    Kiểm duyệt nội dung đánh giá, bình luận
@endsection

@section('decription')
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('active_search', 'Trạng thái')->options(['0' => 'Chờ duyệt', '1' => 'Đã duyệt', '2' => 'Không duyệt', '-1' => '[ Tất cả ]']) !!}
            {!! Former::select('type_search', 'Loại')->options(['organizations' => 'Cơ sở','products' => 'Sản phẩm']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <button type="button" class="btn btn-primary" id="btnSave">Duyệt</button>
            <button type="button" class="btn btn-primary btn-danger" id="btnNoSave">Không Duyệt</button>
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="7%">
                    <col width="10%">
                    <col width="30%">
                    <col width="20%">
                    <col width="10%">
                    <col width="13%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th><input type="checkbox" name="select_all" value="1" id="select-all">Chọn</th>
                    <th>Trạng thái</th>
                    <th>Nội dung đánh giá</th>
                    <th>Đánh giá cho</th>
                    <th>Điểm</th>
                    <th>Người ĐG</th>
                    <th>Xem chi tiết</th>                     
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/rating.js"></script>
    <script>
        Rating('{{ route('rating.index') }}');
    </script>
    <script>
        $("#btnSave").click(function(){
            var sModuleId = '';
            var noCheck = '';
            $('input[name="module[]"]:checked').each(function(){
                var value = $(this).val();
                sModuleId += ',' + value;
            });
            $('input[name="module[]"]:not(:checked)').each(function(){
                var value = $(this).val();
                noCheck += ',' + value;
            });
            if(noCheck.length > 0)
                noCheck = noCheck.substring(1);
            
            if(sModuleId.length > 0)
            {
            sModuleId = sModuleId.substring(1); 
            $.ajax({
                url: 'manage/rating/checked',
                method: 'POST',
                data: {'id': sModuleId, 'no_check':noCheck},
                dataType: 'json',
                success: function () {
                    toastr.success("Cập nhật thành công");
                    $('#datatable').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Bạn chưa chọn nội dung đánh giá !");
                }
            })
        }
        });
        
            $('#select-all').on('click', function(){
            // Check/uncheck checkboxes for all rows in the table
            $('input[name="module[]"]').prop('checked', this.checked);
        });
        
        $("#btnNoSave").click(function(){
            var sModuleId = '';
            var noCheck = '';
            $('input[name="module[]"]:checked').each(function(){
                var value = $(this).val();
                sModuleId += ',' + value;
            });
            $('input[name="module[]"]:not(:checked)').each(function(){
                var value = $(this).val();
                noCheck += ',' + value;
            });
            if(noCheck.length > 0)
                noCheck = noCheck.substring(1);
            if(sModuleId.length > 0){
                sModuleId = sModuleId.substring(1);
            $.ajax({
                url: 'manage/rating/checked',
                method: 'POST',
                data: {'id_nosave': sModuleId, 'no_check':noCheck},
                dataType: 'json',
                success: function () {
                    toastr.success("Cập nhật thành công");
                    $('#datatable').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Bạn chưa chọn nội dung đánh giá !");
                }
            })
        }
        });
            $('#select-all').on('click', function(){
            // Check/uncheck checkboxes for all rows in the table
            $('input[name="module[]"]').prop('checked', this.checked);
        });
    </script>
@endpush