<style>
    sup{color: red !important;}
    label{width: 149px !important;}
    label[for="fanpage"]{text-align: left !important; margin-left: -21px !important;}
    .row>div:first-child>.form-group>div{float: left;width: 180px;}
    .row>div:nth-child(2)>.form-group>div{float: left; margin-left: -57px; padding-right: 0px;}
    .endrow>div{float:left}
    .endrow>div:first-child>div{width: 179px !important; margin-left: 14px !important;}
    .endrow>div:nth-child(2)>div{width: 179px !important;}
    .endrow>div:nth-child(3)>div{width: 155px !important;}
</style>
{!! Former::populate($adv); !!}
{!! Former::open(route('advertisings.update', $adv->id), 'put') !!}
{!! Former::radios('category_id', 'Loại thông tin')->radios($categories)->addClass('category_id')->inline() !!}
{!! Former::text('title', 'Tiêu đề')->setAttribute('maxlength', '511')->required() !!}
{!! Former::textarea('content', 'Nội dung')->setAttribute('style', 'height: 100px')->required() !!}
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Loại sản phẩm<sup>*</sup></label>
    <div class="col-lg-10 col-sm-8 tree-search">
        @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>null,'parent_id'=>($productCategory?$productCategory->id:null)])
    </div>
</div>
<div class="form-group">
    <label id="khu-vuc-label" name="khu-vuc-label" for="khu-vuc-text" class="control-label col-lg-2 col-sm-4 region-label">@if($adv->category_id==1){{'Khu vực giao hàng'}}
        @elseif($adv->category_id==2){{'Khu vực bán hàng'}} @else($adv->category_id==3){{'Khu vực'}} @endif</label>
    <div class="col-lg-10 col-sm-8">
        <input type="text" id="khu-vuc-text" name="khu-vuc-text" class="form-control" disabled="disabled" value="{{isset($adv->region)?$adv->region->full_name:''}}">
    </div>
</div>
{!! Former::text('address', 'Địa chỉ liên hệ')->setAttribute('maxlength', '255')->required() !!}
<div class="row">
    <div class="col-lg-5 col-sm-5">
        {!! Former::text('tel', 'Điện thoại')->setAttribute('maxlength', '32')->required() !!}
    </div>
    <div class="col-lg-7 col-sm-7">
        {!! Former::text('fanpage', 'Facebook')->setAttribute('maxlength', '255') !!}
    </div>
</div>
<div class="row endrow">
    {!! Former::text('from_date', 'Từ ngày')->addClass('form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->forceValue($adv&&$adv->from_date?$adv->from_date->toShortDateString():null)->required() !!}
    {!! Former::text('to_date', 'Đến ngày')->addClass('form-control pull-right datepicker')->prepend('<i class="fa fa-calendar fa-icon"></i>')->forceValue($adv&&$adv->to_date?$adv->to_date->toShortDateString():null)->required() !!}
    {!! Former::select('status', 'Trạng thái')->options(['0' => 'Chờ duyệt', '1' => 'Đã duyệt', '2' => 'Không duyệt'])->required() !!}
</div>
{!! Former::close() !!}

<script>
    $(function () {
        $(".category_id").on('change', function () {
            if ($(this).val() == '1') {
                $('#khu-vuc-label').html('Khu vực giao hàng <sup>*</sup>');
            } else if ($(this).val() == '2') {
                $('#khu-vuc-label').html('Khu vực bán hàng <sup>*</sup>')
            } else {
                $('#khu-vuc-label').html('Khu vực <sup>*</sup>')
            }
        });
    });
</script>