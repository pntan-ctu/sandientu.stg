@extends('layouts.app')

@section('title')
    Kiểm duyệt tin kết nối cung cầu
@endsection

@section('decription')

@endsection

@push('styles')
    <style>
        .icon {margin-right: 5px;z-index: 9999999}
        .select2-container--default .select2-selection--single .select2-selection__rendered {padding: 0px}
        .tree-search>span{width:100% !important}
        .day, .prev, .next{cursor: pointer}
    </style>
@endpush

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('status', 'Trạng thái')->options(['0' => 'Chờ duyệt', '1' => 'Đã duyệt', '2' => 'Không duyệt', '-1' => '[ Tất cả ]']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover responsive" id="dtAdvertisings" width="100%">
                <thead>
                <tr role="row" class="heading">
                    <th class="all">Chuyên mục</th>
                    <th>Loại sản phẩm</th>
                    <th>Tiêu đề</th>
                    <th>Người đăng</th>
                    <th>Từ ngày</th>
                    <th>Đến ngày</th>
                    <th class="all">Trạng thái</th>
                    <th class="all"></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/advertising.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script>
        Advertisings("{{ route('advertisings.index') }}");
        function setActive(iAdvertisingId) {
            $.ajax({
                url: 'manage/advertisings/approve/' + iAdvertisingId,
                success: function () {
                    toastr.success("Bạn đã duyệt quảng cáo.");
                    $('#dtAdvertisings').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        }

        function setInactive(iAdvertisingId) {
            $.ajax({
                url: 'manage/advertisings/disapprove/' + iAdvertisingId,
                success: function () {
                    toastr.success("Bạn đã hủy đăng quảng cáo.");
                    $('#dtAdvertisings').DataTable().ajax.reload(null, false);
                },
                error: function(){
                    toastr.error("Đã có lỗi xảy ra. Vui lòng liên hệ ban quản trị.");
                }
            });
        }
    </script>
@endpush