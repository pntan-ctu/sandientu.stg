@extends('layouts.app')

@section('title')
    Quản lý SlideShow
@endsection

@section('decription')
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('active', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chưa kích hoạt', '1' => 'Kích hoạt']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="25%">
                    <col width="25%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                    <col width="10%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Ảnh slideshow</th>
                    <th>Tiêu đề</th>
                    <th>Link</th>
                    <th>Người tạo</th>
                    <th>Thứ tự</th>
                    <th>Kích hoạt</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/slideshow.js"></script>
    <script>
        SlideShow('{{ route('slideshow.index') }}');
    </script>
@endpush