<style type="text/css">
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    sup{
        color: #EA3838 !important;
    }
</style>
<?php Former::populate($slideshow); ?>
@if($slideshow)
    {!! Former::open(route('slideshow.update', $slideshow->id), 'put' ) !!}
@else
    {!! Former::open(route('slideshow.store')) !!}
@endif
{!! Former::text('title', 'Tiêu đề :')->required() !!}
{!! Former::text('link','Link :')->required() !!}
{!! Former::number('no','Thứ tự :')->required() !!}
<div class="form-group">
    <label class ="control-label col-lg-2 col-sm-4">Ảnh đại diện :</label>
    <div class="col-lg-10 col-sm-8">
                 <img id="logo-img" title="Chọn ảnh đại diện" onclick="document.getElementById('add-new-logo').click();"
                @if(isset($slideshow->image_path) && $slideshow->image_path)
                    src="{{ url('image/200/200/'.$slideshow->image_path) }}"
                @else
                    src="{{ asset('css/images/no-image.png') }}"
                @endif
        />
        <input type="file" style="display: none" id="add-new-logo" name="image_path" accept="image/*" onchange="addNewLogo(this)"/>
        @if(isset($none_create) && $none_create == 1)
            <input type="hidden" id="image" name="image" >
        @elseif(isset($none_create) && $none_create == 2)
            <input type="hidden" id="image" name="image" value="{{$slideshow->image_path}}">
        @endif
        <br>
        <br>
        <span style="color: red;"><i>*Khuyến khích hậu kỳ hình ảnh (945px X 300px) trước khi khi upload!</i></span>
    </div>    
</div>
{!! Former::checkbox('active', 'Hiển thị trên website') !!}
{!! Former::close() !!}
<script type="text/javascript">
    var addNewLogo = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Hiển thị ảnh vừa mới upload lên
                $('#logo-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            //submit form để upload ảnh
        }
    }
</script>
<script>
    $('input[name="image_path"]').change(function(e){
        $('#image').val(e.target.files[0].name);
        });
</script>