@extends('layouts.app')

@section('title')
    Thống kê duyệt thông tin
@endsection

@section('decription')
    Tình hình kiểm duyệt thông tin của các cấp quản lý
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box" id="album-box">
            <div class="box-header">
                <form class="form-inline" action="{{url('manage/report-verify-org')}}" method="get">
                    {!! Former::text('keyword', 'Từ khóa ')->placeholder('Từ khóa') !!}
                    {!! Former::primary_submit('Tìm kiếm') !!}
                    {!! Former::close() !!}
                </form>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                    <thead>
                        <tr role="row" class="heading">
                            <th>Cơ quan quản lý</th>
                            <th class="text-center">Cơ sở chưa kiểm duyệt</th>
                            <th class="text-center">Phản hồi chưa xử lý</th>
                        </tr>
                    </thead>
                    <tbody>
                        @if(isset($list))
                        @foreach($list as $item)
                        <tr id="2" role="row" class="odd">
                            <td >{{$item->name}}</td>
                            <td class="text-center">{{$item->cs_chua_duyet}} / {{$item->cs_dang_ky}}</td>
                            <td class="text-center">{{$item->ph_chua_xl}} / {{$item->ph_da_gui}}</td>
                        </tr>
                        @endforeach
                </table>
                
                <div class="row"></div>
                    <div class="col-sm-5"></div>
                    <div class="col-sm-7">
                        <div style="float: right" class="dataTables_paginate paging_simple_numbers">
                            {{$list->links()}}
                            @endif
                        </div>
                    </div>
            </div>
        </div>
        <!-- /.box-body -->
    </div>
</div>
@endsection
