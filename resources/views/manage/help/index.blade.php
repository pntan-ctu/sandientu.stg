@extends('layouts.app')

@section('title')
Quản lý Trợ giúp
@endsection

@section('decription')
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('active', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chưa phê duyệt', '1' => 'Đã phê duyệt']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                        <col width="30%">
                        <col width="15%">
                        <col width="15%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                        <col width="10%">
                    </colgroup>
                <thead>
                <tr role="row" class="heading">
                            <th>Câu hỏi</th>
                            <th>Người gửi</th>
                            <th>Email</th>
                            <th>Số điện thoại</th>
                            <th>Địa chỉ</th>
                            <th>Kiểm duyệt</th>
                            <th></th>
                        </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/help.js"></script>
    <script src="{{ asset('bower_components/ckeditor/ckeditor.js') }}"></script>
    <script src="bower_components/select2/dist/js/select2.js"></script>
    <script>
        $(function () {
            sdcApp.fixCKEditorModal();
            Help('{{ route('help.index') }}');
        })
    </script>
@endpush
