<style>
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    #cke_1_contents{
        height: 200px !important;
    }
    .hidden{
        display: none;
      }
</style>
<?php Former::populate($help); ?>
@if($help)
    {!! Former::open(route('help.update', $help->id), 'put' ) !!}
@else
    {!! Former::open(route('help.store')) !!}
@endif
    {!! Former::text('email', 'Email người gửi :') !!}
    {!! Former::number('phone', 'Điện thoại:') !!}
    {!! Former::text('address', 'Địa chỉ :') !!}
    {!! Former::textarea('question', 'Câu hỏi :')->rows(3)->required() !!}
@if(isset($none_create) && $none_create == 1)
    <div class="form-group">
        <label for="name" class="control-label col-lg-2 col-sm-4">Trả lời : </label>
        <div class="col-lg-10 col-sm-8">
            <textarea class="ckeditor" name="answers" id="my-editor">@if(isset($help->answers)) {!! html_entity_decode($help->answers) !!} @endif</textarea>
        </div>
    </div>
    <div class="form-group">
        <label class="control-label col-lg-3 col-sm-4">Thêm vào câu hỏi thường gặp :</label>
        <div class="col-lg-1 col-sm-1">
            <div class="checkbox input_fields_wrap">
                <input class=" add_field_button" id="common_status" name="common_status" type="checkbox"/>
            </div>
        </div>
        <div id="div_common">
        <label for="common" class="control-label col-lg-2 col-sm-4">Số thứ tự :</label>
        <div class="col-lg-2 col-sm-2">
            <input class="form-control" type="text" id="common" name="common" >
        </div>
    </div>
    </div>
@endif
@if(isset($none_create) && $none_create == 2)
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Trả lời : </label>
    <div class="col-lg-10 col-sm-8">
        <textarea class="ckeditor" name="answers" id="my-editor">@if(isset($help->answers)) {!! html_entity_decode($help->answers) !!} @endif</textarea>
    </div>
</div>
<div class="form-group">
    <label class="control-label col-lg-4 col-sm-4">Thêm vào câu hỏi thường gặp :</label>
    <div class="col-lg-1 col-sm-1">
        <div class="checkbox input_fields_wrap">
            <input class=" add_field_button" id="common_status" name="common_status" type="checkbox"
            @if($help->common>0)
            checked="checked"
            @endif>
        </div>
    </div>
    <div id="div_common">
    <label for="common" class="control-label col-lg-2 col-sm-4">Số thứ tự :</label>
    <div class="col-lg-2 col-sm-2">
        <input class="form-control" type="text" id="common" name="common" value="{{$help->common}}">
    </div>
</div>
</div>
@endif
<script>
    $("#common_status").change(function() {
    if(this.checked) {
        $("#div_common").show();
    } else {
         $("#common").val(0);
         $("#div_common").hide();
    }
});
</script>
{!! Former::close() !!}
