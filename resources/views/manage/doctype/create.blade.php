
<?php
Former::populate($docType);
$rules = ['name' => 'required|max:255'];
?>
{!! Former::open(route('doctype.store'))->rules($rules); !!}
{!! Former::text('name', 'Loại văn bản') !!}
{!! Former::close() !!}
