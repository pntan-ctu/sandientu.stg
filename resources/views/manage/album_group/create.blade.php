<style type="text/css">
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    sup{
        color: #EA3838 !important;
    }
</style>
<?php Former::populate($album_group); ?>
@if($album_group)
    {!! Former::open(route('album-group.update', $album_group->id), 'put' ) !!}
@else
    {!! Former::open(route('album-group.store')) !!}
@endif
{!! Former::text('name', 'Tên nhóm Album :')->required() !!}
{!! Former::textarea('description','Nội dung :')->rows(5) !!}
{!! Former::number('no','Thứ tự :')->required() !!}
{!! Former::checkbox('active', 'Hiển thị trên website') !!}
{!! Former::close() !!}