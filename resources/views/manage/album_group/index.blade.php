@extends('layouts.app')

@section('title')
    Quản lý Nhóm Album 
@endsection

@section('decription')
    Quản lý Nhóm Album 
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('active', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chưa kích hoạt', '1' => 'Kích hoạt']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                <thead>
                <tr role="row" class="heading">
                    <th class="all">Tên nhóm Album</th>
                    <th>Nội dung</th>
                    <th>Người tạo</th>
                    <th>Thứ tự</th>
                    <th class="all">Kích hoạt</th>
                    <th class="all"></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
@endpush
@push('scripts')
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/album_group.js"></script>
    <script>
        AlbumGroup('{{ route('album-group.index') }}');
    </script>
@endpush