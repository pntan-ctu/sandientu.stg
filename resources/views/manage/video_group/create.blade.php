<style type="text/css">
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    sup{
        color: #EA3838 !important;
    }
</style>
<?php Former::populate($video_group); ?>
@if($video_group)
    {!! Former::open(route('video-group.update', $video_group->id), 'put') !!}
@else
    {!! Former::open(route('video-group.store')) !!}
@endif
{!! Former::text('name', 'Tên nhóm video :')->required() !!}
{!! Former::textarea('description', 'Nội dung :')->rows(5) !!}
{!! Former::number('no', 'Thứ tự :')->required() !!}
{!! Former::checkbox('active', 'Kích hoạt') !!}
{!! Former::close() !!}