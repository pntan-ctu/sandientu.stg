@extends('layouts.app')

@section('title')
Gửi SMS
@endsection

@section('decription')

@endsection

@section('content')
<!--<meta name="csrf-token" content="{{ csrf_token() }}">-->
@push('styles')
    <link href="https://fonts.googleapis.com/css?family=Roboto+Slab:400,700" rel="stylesheet">
    <link rel="stylesheet" href="css/sms/kendo.common-material.min.css" />
    <link rel="stylesheet" href="css/sms/kendo.material.min.css" />
    <link rel="stylesheet" href="css/sms/kendo.material.mobile.min.css" />
@endpush
@push('scripts')
<!--    <script src="js/sms/jquery.min.js"></script>-->
    <script src="js/sms/kendo.all.min.js"></script>
    
@endpush
<div id="example">
    <form id="sms_form" accept-charset="utf-8" class="form-horizontal" method="POST" action="{{route('sms.store')}}"
                  enctype="multipart/form-data">
    {{ csrf_field() }}
    <div style="float:left;width:49%;">
    <div class="demo-section k-content">
        <div>
            <div id="treeview"></div>
        </div>
    </div>
    <div id="result"></div>
    <div id="result1">{!! Former::hidden('tel')->id('tel') !!}</div>
    </div>
    <div style="float: right;width:49%">
        
        <?= Former::textarea('content')->label('Nội dung ')->rows(4) ?>
        <br/>
        <div id="charactersRemaining" style="color: red;">Bạn đã nhập: 0 ký tự!</div>
        <br/>
        <input type="submit" id="btnSave" class="btn Normal btn-success" value="Gửi"/><!--  data-toggle="modal" data-target="#myModal"-->
        
    </div>
    <div class="clear"></div>
    </form>
    <!-- popup -->
        <div id="myModal" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Thông báo</h4>
              </div>
              <div class="modal-body">
                <p>Gửi tin nhắn thành công!.</p>
              </div>
              <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Đóng</button>
              </div>
            </div>

          </div>
        </div>
</div>
@endsection
@push('scripts')
    
    <script>
//        $.ajaxSetup({
//            headers: {
//              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
//            }
//          });
        $("#treeview").kendoTreeView({
            checkboxes: {
                checkChildren: true
            },

            check: onCheck,

            dataSource: [{
                {!! $tree !!}       
            }]
        });

        // function that gathers IDs of checked nodes
        function checkedNodeIds(nodes, checkedNodes) {
            for (var i = 0; i < nodes.length; i++) {
                if (nodes[i].checked && nodes[i].id!='0') {
                    checkedNodes.push(nodes[i].id);
                }

                if (nodes[i].hasChildren) {
                    checkedNodeIds(nodes[i].children.view(), checkedNodes);
                }
            }
        }

        // show checked node IDs on datasource change
        function onCheck() {
            var checkedNodes = [],
                treeView = $("#treeview").data("kendoTreeView"),
                message;

            checkedNodeIds(treeView.dataSource.view(), checkedNodes);

            if (checkedNodes.length > 0) {
                message = checkedNodes.join(",");
            } else {
                message = "";
            }

            //$("#result").html(message);
            $('#result1 input').val(message);
        }
        
        $(document).ready(function () {
 
            var form = $('#sms_form');

            form.submit(function(e) {

                var title = $("#content").val().trim();
                var tel = $("#tel").val();
                if(tel.length == 0)
                {
                    bootbox.alert("Bạn chưa chọn người gửi!");
                    return false;
                }
                if(title.length == 0)
                {
                    bootbox.alert("Bạn chưa nhập nội dung!");
                    return false;
                }

                e.preventDefault();
                $.ajax({
                    url     : form.attr('action'),
                    type    : form.attr('method'),
                    data    : form.serialize(),
                    dataType: 'json',
                    success : function ( json )
                    {
                        // Success
                        //bootbox.alert("Gửi tin nhắn thành công!");
                        if(json.msg === "success"){
                            bootbox.alert("Gửi tin nhắn thành công!");
                        }
                    },
                    error: function( json )
                    {
                        if(json.status === 422) {
                            var errors = json.responseJSON;
                            $.each(json.responseJSON, function (key, value) {
                                $('.'+key+'-error').html(value);
                            });
                        } else {
                            // Error
                            // Incorrect credentials
                            // alert('Incorrect credentials. Please try again.')
                        }
                    }
                });
            });

        });
    
        var el;                                                    

        function countCharacters(e) {                                    
            var textEntered, countRemaining, counter;          
            textEntered = document.getElementById('content').value;  
            counter = textEntered.length;
            countRemaining = document.getElementById('charactersRemaining'); 
            countRemaining.textContent = "Bạn đã nhập: " + counter + " ký tự!";       
        }
        el = document.getElementById('content');                   
        el.addEventListener('keyup', countCharacters, false);
        
        function validate()
        {
            var title = $("#content").val().trim();
            var tel = $("#tel").val();
            var token1 = $("#_token").val();
            // if(tel.length == 0)
            // {
            //    bootbox.alert("Bạn chưa chọn người gửi!");
            //    return false;
            // }
            if(title.length == 0)
            {
                bootbox.alert("Bạn chưa nhập nội dung!");
                return false;
            }
            else{
                $.ajax({
                    type:'POST',
                    url:'{{route('sms.store')}}',
                    data:{
                        'content': title,
                        'tel':tel,
                        '_token': '{{ csrf_token() }}'
                    },
                    dataType: 'json',
                    success:function(msg){
                        if(msg == 'ok'){
                            bootbox.alert("Gửi tin nhắn thành công!");
                        }else{
                            bootbox.alert("Có lỗi!");
                        }
                    }
                });
            }

//            return true;
        }
    </script>
@endpush

@push('styles')
    <style>
        #treeview .k-sprite {
            background-image: url("css/sms/images/coloricons-sprite.png");
        }

        .rootfolder { background-position: 0 0; }
        .folder     { background-position: 0 -16px; }
        .pdf        { background-position: 0 -32px; }
        .html       { background-position: 0 -48px; }
        .image      { background-position: 0 -64px; }
    </style>

@endpush
