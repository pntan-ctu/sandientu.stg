@extends('layouts.app')

@section('title')
    Quản lý Chuyên mục tin
@endsection

@section('description')
    (some text description)
@endsection

@push('styles')
    <link rel="stylesheet" type="text/css" href="plugins/easyui/icon.css">
    <link rel="stylesheet" type="text/css" href="plugins/easyui/easyui.css">
    <link rel="stylesheet" type="text/css" href="plugins/dropdown-tree/resources/dropdowntree.css">
    <style>
        label[for=active]{margin-top: 30px !important;}
        div[class=checkbox]{margin-top: -30px !important;}
        input, textarea{margin-bottom: 15px !important;}
    </style>
@endpush

@section('content')
    <div class="box" id="box-news-group">
        <div class="box-body row">
            <div class="col-md-8">
                <ul id="treeNewsGroups" checkbox="false" class="easyui-tree"></ul>
            </div>
            <div class="col-md-4" style="text-align: right">
                <!--button id="btn-add" type="button" class="btn btn-success" data-toggle="modal" data-target="#modal-group-news" -->
                <button id="btn-add" type="button" class="btn btn-success" data-toggle="modal">
                    <i class="fa fa-plus"></i>
                    Thêm
                </button>
                <button id="btn-edit" type="button" class="btn btn-success">
                    <i class="fa fa-edit"></i>
                    Sửa
                </button>
                <button id="btn-remove" type="button" class="btn btn-success">
                    <i class="fa fa-remove"></i>
                    Xóa
                </button>
            </div>
        </div>
        <!-- /.box-body -->
    </div>

    <div class="modal fade in" id="modal-group-news" style="display: none; padding-right: 17px;">
        <div class="modal-dialog" style="width: 800px">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">×</span></button>
                    <h4 class="modal-title" style="border-bottom: 1px solid gainsboro;font-weight: bold;padding-bottom: 8px;">Thêm chuyên mục tin</h4>
                </div>
                <form accept-charset="utf-8" class="form-horizontal" method="POST" action="{{route('news-group.store')}}"
                    onsubmit="return validate()">
                    {{ csrf_field() }}
                    <div class="modal-body row" style="padding: 0px 30px;">
                        {!! Former::hidden('id')->value(0) !!}
                        {!! Former::hidden('parent_id')->value(1) !!}
                        <div class="form-group">
                            <label for="name" class="control-label col-lg-2 col-sm-4">Chuyên mục cha</label>
                            <div class="col-lg-10 col-sm-8">
                                <div class="dropdown dropdown-tree" id="cboTreeNewsGroups"></div>
                            </div>
                        </div>
                        {!! Former::text('name', 'Tên chuyên mục') !!}
                        {!! Former::textarea('description', 'Mô tả') !!}
                        <div class="row">
                            <div class="col-md-6">{!! Former::text('no', 'STT')->type('number')->setAttribute('max', '2147483646')->setAttribute('min', '1') !!}</div>
                            <div class="col-md-6" style="padding-left: 100px">{!! Former::checkbox('active', 'Kích hoạt') !!}</div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal" id="btnCloseGroupNews">Đóng</button>
                        <button type="submit" class="btn btn-primary" id="btnSaveGroupNews">Lưu</button>
                    </div>
                </form>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>

@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="plugins/easyui/jquery.easyui.min.js"></script>
    <script src="plugins/dropdown-tree/resources/dropdowntree.js"></script>
    <script>
        $('#treeNewsGroups').tree({
            data: {!! $treeNewsGroupData !!}
        });

        /*********** comboxbox tree news group on add, edit dialog ***********/
        var options = {
            title : 'Trang chủ',
            data: {!! $cboTreeGroupData !!},
            maxHeight: 1000,
            selectChildren : true,
            clickHandler: function(element){
                $("#cboTreeNewsGroups").SetTitle(element[0].dataset.url);
                $("#cboTreeNewsGroups").click();
                var iParentId = element[0].dataset.value;
                $("input[name=parent_id]").val(iParentId);
                $.ajax({
                    url: 'manage/news-group/getNextNo/' + iParentId,
                    success: function (data) {
                        $("#no").val(data);
                    }
                });
            },
            checkHandler: function(element){
                $("#cboTreeNewsGroups").SetTitle($(element).find("a").first().text());
            },
            closedArrow: '<i class="fa fa-caret-right" aria-hidden="true"></i>',
            openedArrow: '<i class="fa fa-caret-down" aria-hidden="true"></i>',
            multiSelect: false
        };

        $("#cboTreeNewsGroups").DropDownTree(options);

        $("#btn-add").click(function(){
            var node = $('#treeNewsGroups').tree('getSelected');
            /*if(node == null)
            {
                toastr.error("Bạn chưa chọn chuyên mục cha");
                return;
            }*/

            $("input[name=id]").val(0);
            $("#name").val('');
            $("#description").val('');
            $("#active").prop('checked', false);
            if(node != null && node.id != {{ $PREFIX_NEWS_GROUP_ID }}) {
                $("#no").val(node.next_no);
                $("input[name=parent_id]").val(node.id);
                $("#cboTreeNewsGroups").SetTitle(node.url);
                $("#cboTreeNewsGroups").ExpandTo(node.id);
            }
            else {
                var iParentId = 2;
                $("input[name=parent_id]").val(iParentId);
                $.ajax({
                    url: 'manage/news-group/getNextNo/' + iParentId,
                    success: function (data) {
                        $("#no").val(data);
                    }
                });
                $("#cboTreeNewsGroups").Expand(2);
            }
            $("#modal-group-news").modal("show");
        });

        $("#btn-edit").click(function(){
            $(".modal-title").html("Sửa chuyên mục tin");
            var node = $('#treeNewsGroups').tree('getSelected');
            if(node == null)
            {
                toastr.error("Bạn chưa chọn chuyên mục cần sửa");
                return;
            }

            if(node.id < 3)
            {
                toastr.error("Không thể sửa chuyên mục gốc");
                return;
            }

            var parent = $('#treeNewsGroups').tree('getParent', node.target);
            $("#cboTreeNewsGroups").SetTitle(parent.url);
            $("input[name=id]").val(node.id);
            $("input[name=parent_id]").val(node.parent_id);
            $("#cboTreeNewsGroups").ExpandTo(node.parent_id);
            $("#cboTreeNewsGroups").Hide(node.id);
            $("#name").val(node.text);
            $("#description").val(node.description);
            $("#no").val(node.no);
            $("#active").prop('checked', node.active == 1);
            $("#modal-group-news").modal("show");
        });

        $("#btn-remove").click(function() {
            var node = $('#treeNewsGroups').tree('getSelected');

            if(node == null)
            {
                toastr.error("Bạn chưa chọn chuyên mục cần xóa");
                return;
            }

            if(node.id < 3)
            {
                toastr.error("Không thể xóa chuyên mục gốc");
                return;
            }

            if($('#treeNewsGroups').tree('isLeaf', node.target)) {
                if(node.news_count > 0) {
                    toastr.error("Không thể xóa do chuyên mục này đang có tin bài");
                    return;
                }

                bootbox.confirm(
                    "Bạn chắc chắn muốn xóa <b>"+node.text+"</b>?",
                    function(result){
                        if(result){
                            $.ajax({
                                url: 'manage/news-group/delete/' + node.id,
                                success: function (data) {
                                    $('#treeNewsGroups').tree('remove', node.target);
                                }
                            });
                        }
                });
            }
            else toastr.error("Không thể xóa do đang có chuyên mục con");
        });

        function validate()
        {
            var name = $("#name").val().trim();
            if(name.length == 0)
            {
                toastr.error("Bạn chưa nhập tên chuyên mục");
                return false;
            }
            return true;
        }
    </script>
@endpush
