<?= Former::vertical_open_for_files()->method('PUT')->action(route('docfield.update', ['id' => $docField->id])) ?>
<?= Former::text('name')->label('Tên danh mục : ')->autofocus()->required()->value($docField->name) ?>
<?= Former::number('no')->min(0)->label('Số thứ tự : ')->value($docField->no) ?>
<?= Former::checkbox('active')->label('Kích hoạt : ')->inline()->check($docField->active == 1 ? true : false) ?>
<?= Former::close() ?>
  
