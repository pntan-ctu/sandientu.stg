
<?php
Former::populate($docField);
$rules = ['name' => 'required|max:255',
    'no' => 'required'];
?>

{!! Former::open(route('docfield.store'))->rules($rules); !!}

{!! Former::text('name', 'Lĩnh vực văn bản') !!}
{!! Former::number('no', 'Số thứ tự') !!}
{!! Former::checkbox('active', 'Kích hoạt') !!}
{!! Former::close() !!}
