@extends('layouts.app')

@section('title')
    Quản lý Menu Web
@endsection

@section('decription')
    Quản lý Menu Web
@endsection

@section('content')
<div class="box" id="box-users">
    <div class="box-header">

        <div class="right-button">
            <a type="button" class="btn Normal btn-success" href="{{'manage/hor-menu/create'}}">
                <i class="fa fa-plus"></i>
                Thêm mới
            </a>
        </div>
    </div>
    <!-- /.box-header -->
    <div class="box-body">
        <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
            <colgroup>
                <col width="30%">
                <col width="15%">
                <col width="15%">
                <col width="15%">
                <col width="15%">
                <col width="10%">
            </colgroup>
            <thead>
                <tr role="row" class="heading" >
                    <th>Tên Menu</th>
                    <th>Đường dẫn</th>
                    <th>Thứ tự</th>
                    <th>Hiển thị trên website</th>
                    <th>Cửa sổ mới</th>
                    <th></th>
                </tr>
            </thead>
             @if(isset($horMenu))
                <?php  hormenu($horMenu) ?>
             @endif
        </table>
    </div>
    <!-- /.box-body -->
</div>
@endsection
@push('scripts')
<script>
        function deleteItem(id) {
            if (confirm("Bạn có chắc chắn muốn xóa ")){
              document.getElementById("form_delete"+id).submit();
            }
        }
    
</script>
@endpush
@php
    function hormenu($data, $parent = 0,$str=""){
        foreach($data as $val){
            $id = $val["id"];
            $name = $val["title"];
            $link = $val["url"];
            $no = $val["no"];
            $active = $val["active"];
            $new_tab = $val["new_tab"];
            if($val["parent_id"]== $parent){
                echo '<tr>';
                if($val["parent_id"]==null)
                {
                    echo'<td><b>'.$str.''.$name.'</b></td>';
                }
                else
                {
                    echo'<td>'.$str.''.$name.'</td>';
                }
                echo'<td>'.$link.'</td>
                <td style="text-align:center">'.$no.'</td>';
                echo'<td style="text-align:center">';
                if($active == 1)
                {
                    echo '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                }
                else{
                    echo '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                }
                echo'</td>';
                echo'<td style="text-align:center">';
                if($new_tab == 1)
                {
                    echo '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                }
                else{
                    echo '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                }
                echo'</td>';
                echo'<td style="text-align:center">
                    <form id="form_delete'.$id.'" action="'.route("hor-menu.destroy",['id'=>$id]).'" method="POST">'
                        . method_field("DELETE").' '.csrf_field().
                    '<a href="manage/hor-menu/'.$id.'/edit" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="glyphicon glyphicon-pencil"></i></a>
                    <button onClick="deleteItem('.$id.')" type="button" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></button>
                    </form>
                <td>
                </tr>';
            hormenu($data,$id,$str."&emsp;&emsp;");
            }
        }
        
}
@endphp
