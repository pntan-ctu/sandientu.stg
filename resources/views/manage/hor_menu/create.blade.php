@extends('layouts.app')

@section('title')
    Quản lý Menu Web
@endsection

@section('decription')
    Quản lý Menu Web
@endsection
@push('styles')
    <style>
        .colors {
            display: none;
        }
        sup{
        color: #EA3838 !important;
        }   
    </style>
@endpush
@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-offset-2 col-md-8">

<?php Former::populate($horMenu); ?>
@if($horMenu)
    {!! Former::open(route('hor-menu.update', $horMenu->id), 'put') !!}
@else
    {!! Former::open(route('hor-menu.store')) !!}
@endif
    <div class="form-group">
        <label for="name" class="control-label col-lg-2 col-sm-4">Menu cha : </label>
        <div class="col-lg-10 col-sm-8 tree-search">
            @include("components/select_search",['tree'=>$tree,"level"=>0,"path"=>null,"root"=>'-- Menu gốc --','parent_id'=>(isset($horMenu)?$horMenu->parent_id:null),'name'=>'id'])
        </div>
    </div>

    {!! Former::text('title','Tên menu : ')->required() !!}
    {!! Former::select('mtype','Đường dẫn :')->required()->options(['1'=>'Tin tức','2'=>'Sản phẩm','3'=>'Link']) !!}
    <div id="3" class="colors 0">
        {!! Former::text('link','Link') !!}
    </div>
    <div id="2" class="form-group colors 1">
        <label for="name" class="control-label col-lg-2 col-sm-4">Sản phẩm : </label>
        <div class="col-lg-10 col-sm-8 tree-search">
            @include("components/select_search",['tree'=>$treeproduct,"level"=>0,"path"=>null,'parent_id'=>(isset($horMenu)?$horMenu->relate_id:null),'name'=>'product_id'])
        </div>
    </div>
    <div id="1" class="form-group colors 2">
        <label for="name" class="control-label col-lg-2 col-sm-4">Tin tức: </label>
        <div class="col-lg-10 col-sm-8 tree-search">
         @include("components/select_search",['tree'=>$treenew,"level"=>0,"path"=>null,'parent_id'=>(isset($horMenu)?$horMenu->relate_id:null),'name'=>'new_id'])
        </div>
    </div>
    @if(isset($none_create)&& $none_create == 1)
    {!! Former::text('no','Thứ tự:') !!}
    @endif
    {!! Former::checkbox('active', 'Hiển thị trên website') !!}
    {!! Former::checkbox('new_tab', 'Mở cửa sổ mới') !!}
        <div class="text-center">
        <button type="submit" class="btn btn-success">
            <i class="fa fa-save"></i>
                Cập nhật
            </button>
        </div>
    {!! Former::close() !!}
                <!-- /end form -->
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script>
  $(function() {
      var iId = $('#mtype').val();
      $('.colors[id=' + iId + ']').show();
    $('#mtype').change(function(){
      $('.colors').hide();
      $('#' + $(this).val()).show();
      });
  });
</script>
@endpush
