<style>
    .checkbox{
        float: left !important;
        margin-right: 30px !important;
    }
    #active{margin-left: 0px !important}
    sup {
    color: red;
    }
</style>
@php
Former::populate($user);
$rules = ['tel' => 'max:11'];
$roles = isset($roles) ? $roles : array();
@endphp
@if($user)
    {!! Former::open(route('users.update', $user->id), 'put')->rules($rules) !!}
    {!! Former::text('name', 'Họ tên')->required() !!}
    {!! Former::text('email')->readonly()->required() !!}
@else
    {!! Former::open(route('users.store'))->rules($rules) !!}
    {!! Former::text('name', 'Họ tên')->required() !!}
    {!! Former::text('email')->required() !!}
@endif
{!! Former::text('tel', 'Di động') !!}
@if($user)
    <input type="hidden" id="organization_id" name="organization_id" >
    @if($user->organization_id > 0)
    <div class="form-group">
        <label for="organization_id" class="control-label col-lg-2 col-sm-4">Cơ quan quản lý</label>
        <div class="col-lg-10 col-sm-8 tree-search" >
             @include("components/select_search",['tree'=>$orgTypeTree,"level"=>0,
             "path"=>null,"root"=>'-- Chọn cơ quan quản lý --','parent_id'=>($user?$user->organization_id:null),'name'=>'organization_id_edit'])
        </div>
    </div>
    @php
    if(count($roles)) {
        $sDraw = "echo Former::checkboxes('role[]', 'Phân quyền')->id('check_role')->required()->checkboxes([";
        foreach ($roles as $r){
            $sDraw .= "'". $r['title'] . "'" . "=> ['name' => 'role[" . $r['name'] . "]', 'value' => '" . $r['name'] . "'],";
        }
        $sDraw .= "])";
        foreach ($roles as $r){
            $sDraw .= "->check('role[" . $r['role_id'] . "]')";
        }
        $sDraw .= ";";
        eval($sDraw);
    }
    @endphp
    @endif
@else
    <div class="form-group">
        <label for="organization_id" class="control-label col-lg-2 col-sm-4">Cơ quan quản lý<sup>*</sup></label>
        <div class="col-lg-10 col-sm-8 tree-search">
             @include("components/select_search_user",['tree'=>$orgTypeTree,"level"=>0,
             "path"=>null,"root"=>'-- Chọn cơ quan quản lý --','parent_id'=>($user?$user->organization_id:null),'name'=>'organization_id'])
        </div>
    </div>
    <div class="form-group">
        <label for="roles" class="control-label col-lg-2 col-sm-4">Phân quyền<sup>*</sup></label>
        <div class="col-lg-10 col-sm-8 " id="roles">
             
        </div>
    </div>
@endif
{!! Former::checkbox('active', 'Kích hoạt') !!}
{!! Former::close() !!}

<script>
    $('#organization_id_edit').prop('disabled',true);
    $("button[class='btn btn-primary']").click(function() {      
        @if($user)
        var org_id = {{ $user->organization_id }};
        if(org_id == 0) {
            document.getElementById("organization_id").value = '0';
        } else {
            if($('#check_role:checked').length ==0) {
                toastr.error("Bạn chưa chọn phân quyền cho tài khoản");
                return false;
            } else {
                var id_org = $('#organization_id_edit').val();
                if(id_org > 0) {
                    document.getElementById("organization_id").value = id_org;
                } else {
                    document.getElementById("organization_id").value = '0';
                }
            }
        }
        @else
        if($('#roles input:checked').length == 0)
        {
            toastr.error("Bạn chưa chọn phân quyền cho tài khoản");
            return false;
        }
        @endif
    });
    
//    $('select[name="organization_id"]').change(function(){
//           $('#organization').val(this.value); 
//        });
//    $("button[class='btn btn-primary']").click(function(){
//        var check = $("select[name='organization_id']").val();
//        if(check <= 0) {
//            toastr.error("Bạn chưa chọn cơ quan quản lý");
//            return false;
//        }
//        if($('#roles input:checked').length == 0)
//        {
//            toastr.error("Bạn chưa chọn phân quyền cho tài khoản");
//            return false;
//        }
//    });

</script>
<script type="text/javascript">
    $("#organization_id").change(function(){
       var orgId = $('#organization_id').val();
       $.get("manage/users/getRoles/"+orgId,function(data){
           $("#roles").html(data);
       });
    });
</script>