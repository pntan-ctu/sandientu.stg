@extends('layouts.app')

@section('title')
    Quản lý người dùng
@endsection

@section('decription')
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('active_search', 'Trạng thái')->options(['-1' => 'Tất cả', '0' => 'Chưa kích hoạt', '1' => 'Kích hoạt']) !!}
            @if (count($memberTypes) > 2)
            {!! Former::select('member_type', 'Loại tổ chức')->options($memberTypes) !!}
            @endif
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
            <div class="right-button">
                <button id="btn-add" type="button" class="btn btn-success">
                    <i class="fa fa-plus"></i>
                    Thêm mới
                </button>
            </div>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
                <thead>
                <tr role="row" class="heading">
                    <th class="all">Họ tên</th>
                    <th class="min-tablet-p">Email</th>
                    <th class="min-tablet-l">Đơn vị</th>
                    <th class="min-tablet-p">Mật khẩu khởi tạo</th>
                    <th class="all">Kích hoạt</th>
                    <th>Reset mật khẩu</th>
                    <th class="all"></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('styles')
    <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
    <link href="bower_components/select2/dist/css/select2.css" rel="stylesheet"/>
@endpush
@push('scripts')
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
    <script src="bower_components/select2/dist/js/select2.js"></script>
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/user.js"></script>
    <script>
        User('{{ route('users.index') }}');

        function resetPassword(iUserId, sName) {
            bootbox.confirm(
                "Bạn chắc chắn muốn reset mật khẩu cho <b>" + sName + "</b>?",
                function(result) {
                    if (result) {
                        $.ajax({
                            url: 'manage/users/reset-password/' + iUserId,
                        }).done(function(rs) {          
                            toastr.success("Reset mật khẩu thành công !");
                            $('#datatable').DataTable().ajax.reload(null, false);
                        });
                    }
                }
            );
        }
    </script>
@endpush