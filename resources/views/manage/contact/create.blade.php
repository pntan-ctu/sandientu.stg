<?php Former::populate($contact); ?>
{!! Former::open(route('contact.update', $contact->id), 'put' ) !!}
<div class="form-group">
    <label for="name" class="control-label col-lg-2 col-sm-4">Chọn cơ quan : </label>
    <div class="col-lg-10 col-sm-8 tree-search">
           @include("components/select_search",['tree'=>$orgTypeTree,"level"=>0,"path"=>null,"root"=>null,
            'parent_id'=>null,'name'=>'org_id',"root"=>'-- Chọn cơ quan chuyển tiếp --'])
            <input type="hidden" id="org" name="org" >
    </div>
</div>
{!! Former::close() !!}
<script>
    $('select[name="org_id"]').change(function(){
           $('#org').val(this.value); 
        });
</script>
