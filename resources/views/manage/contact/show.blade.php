@extends('layouts.app')

@section('title')
    Xử lý thông tin phản hồi
@endsection

@section('decription')
    Thông tin phản hồi từ khách truy cập
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <!-- /.box-header -->
            <div class="box-body">
                <div class="col-md-offset-1 col-md-10">
                    <?php Former::populate($contact); ?>
                    @if($contact)
                        {!! Former::open(route('contact.update', $contact->id), 'put' ) !!}
                    @else
                        {!! Former::open(route('contact.store')) !!}
                    @endif
                    {!! Former::textarea('content', 'Nội dung:')->rows(5)->readonly() !!}
                    {!! Former::text('name', 'Người gửi:')->readonly() !!}
                    {!! Former::email('email', 'Email :')->readonly() !!}
                    {!! Former::number('phone', 'Điện thoại :')->readonly() !!}
                    @if($contact->forward_log)
                    {!! Former::text('forward_log', 'Chuyển tiếp bởi:')->readonly() !!}
                    @endif
                    <div class="form-group">
                        <label for="file" class="control-label col-lg-2 col-sm-4">Tệp đính kèm:</label>
                        <div class="col-lg-10 col-sm-8">
                                @if(!empty($files))
                                    @foreach($files as $item)
                                    <a href="{{url('download/'.$item->path) }}"><i class="fa fa-download"></i>{{$item->file_name}}</a>&emsp;
                                    @endforeach
                                @else
                                <p style="font-style: italic">Không có file đính kèm</p>
                                @endif
                        </div>
                    </div>
                    <div class="text-center">
                        <a style="color: #ffff" href="manage/contact">
                            <button type="button" class="btn btn-danger" data-dismiss="modal">
                                <i class="fa fa-reply"></i>
                                Quay lại 
                            </button>
                        </a>
                    </div>
                    {!! Former::close() !!}
                    <!-- /end form -->
                </div>
            </div>
            <!-- /.box-body -->
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script>
    function closeModal() {
        oTableImg.draw();
        $('#modal-add').modal('hide');
    }
</script>
@endpush