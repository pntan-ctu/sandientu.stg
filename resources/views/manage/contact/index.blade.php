@extends('layouts.app')

@section('title')
    Xử lý thông tin phản hồi
@endsection

@section('decription')
@endsection

@section('content')
    <div class="box" id="box-users">
        <div class="box-header">
            {!! Former::openInline()->id('search-form')->addClass('pull-left') !!}
            {!! Former::text('keyword', 'Từ khóa')->placeholder('Từ khóa') !!}
            {!! Former::select('active', 'Nhóm hiển thị')->options(['-1' => 'Tất cả', '0' => 'Chưa xử lý', '1' => 'Đã xử lý']) !!}
            {!! Former::primary_submit('Tìm kiếm') !!}
            {!! Former::close() !!}
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table class="table table-bordered table-striped table-hover" id="datatable" width="100%">
                <colgroup>
                    <col width="30%">
                    <col width="20%">
                    <col width="15%">
                    <col width="10%">
                    <col width="10%">
                    <col width="15%">
                </colgroup>
                <thead>
                <tr role="row" class="heading">
                    <th>Tiêu đề</th>
                    <th>Cơ quan tiếp nhận</th>
                    <th>Ngày gửi</th>
                    <th>Trạng thái</th>
                    <th>Xử lý</th>
                    <th></th>
                </tr>
                </thead>
            </table>
        </div>
        <!-- /.box-body -->
    </div>
@endsection
@push('scripts')
    <script src="js/sdc-crud.js"></script>
    <script src="js/manage/contact.js"></script>
    <script>
        Contact('{{ route('contact.index') }}');
    </script>
@endpush