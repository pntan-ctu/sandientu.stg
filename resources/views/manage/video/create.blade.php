<style type="text/css">
    #logo-img{ width: 200px; cursor: pointer}
    .lv1{ display: none}
    .lv2{ display: none}
    .active{ display: block !important;}
    sup{
        color: #EA3838 !important;
    }
</style>
<?php Former::populate($video); ?>
@if($video)
    {!! Former::open(route('video.update', $video->id), 'put') !!}
@else
    {!! Former::open(route('video.store')) !!}
@endif

<div class="form-group">
    <label class ="control-label col-lg-2 col-sm-4">Tên nhóm video :<sup>*</sup></label>
    <div class="col-lg-10 col-sm-8">
        
        <select class="form-control" name="group_id">
                    @foreach ($video_group as $key)
                    @if($key->active == 1)
                    {
                        <option 
                            @if(isset($none_create) && $none_create == 1 && $video->group_id == $key->id)
                                {{'selected'}}
                            @endif
                            value="{{$key->id}}">{{$key->name }}
                        </option>
                    }    
                    @endif    
                    @endforeach
            </select>
    </div>    
</div>
{!! Former::text('title', 'Tiêu đề :')->required() !!}
{!! Former::textarea('description', 'Nội dung :')->rows(5) !!}
<div class="form-group">
    <label class ="control-label col-lg-2 col-sm-4">Ảnh đại diện :</label>
    <div class="col-lg-10 col-sm-8">
                 <img id="logo-img" title="Chọn ảnh đại diện" onclick="document.getElementById('add-new-logo').click();"
                @if(isset($video->img_path) && $video->img_path)
                    src="{{ url('image/200/200/'.$video->img_path) }}"
                @else
                    src="{{ asset('css/images/no-image.png') }}"
                @endif
        />
        <input type="file" style="display: none" id="add-new-logo" name="avatar" accept="image/*" onchange="addNewLogo(this)"/>
    </div>    
</div>
{!! Former::number('width', 'Chiều ngang :')->required() !!}
{!! Former::number('height', 'Chiều cao :')->required() !!}
{!! Former::url('youtube_url', 'Link Youtube :')->required() !!}
{!! Former::checkbox('active', 'Kích hoạt :') !!}
{!! Former::close() !!}
<script type="text/javascript">
    var addNewLogo = function(input){
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                //Hiển thị ảnh vừa mới upload lên
                $('#logo-img').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
            //submit form để upload ảnh
        }
    }
</script>
@push('scripts')
<script>
    $(document).ready(function() {
    var add_button      = $(".btn-primary"); //Add button ID
    var username = document.getElementById('group_id').value;
    $(add_button).click(function(e){ //on add input button click
           if (username == ''){
                alert('Bạn chưa nhập trường');
            }
            else{return true;}
            return false;
        }
    });
    
});
</script>
@endpush