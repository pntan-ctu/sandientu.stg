<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title') - {{ config('app.name', 'ATTP') }}</title>
        <base href="{{asset('')}}">
        <!-- Favicon -->
        @include("layouts.includes.favicon")

        <!-- Styles link declare -->
        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,600,700" rel="stylesheet">

        <!-- Bootstrap 3.3.7 -->
        <link href="bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
        <!-- Font Awesome -->
        <link href="bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link href="css/style-responsive.css" rel="stylesheet">
        <!-- Select2 -->
        <link rel="stylesheet" href="bower_components/select2/dist/css/select2.min.css">
        <!-- Pages -->
        <link href="css/style-index.css" rel="stylesheet">
        <link href="css/slick.css" rel="stylesheet">
        <link href="css/slick-theme.css" rel="stylesheet">
        <link href="css/nhomsp.css" rel="stylesheet">
        <link href="css/chitietsanpham.css" rel="stylesheet">
        <link href="css/news.css" rel="stylesheet">
        <link href="css/help.css" rel="stylesheet">
        <link href="css/cososxkd.css" rel="stylesheet">
        <link href="css/chitietcssxkd.css" rel="stylesheet">
        <link href="css/profiles.css" rel="stylesheet">
        <link href="css/ketnoicc.css" rel="stylesheet">
        <link href="css/favorite.css" rel="stylesheet">
        <link rel="stylesheet" href="plugins/toastr/toastr.min.css">
        <!-- Ionicons -->
        <link href="bower_components/Ionicons/css/ionicons.min.css" rel="stylesheet">
        <!-- DataTables -->
        <link href="bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
        <link href="plugins/datatables-responsive/responsive.bootstrap.min.css" rel="stylesheet">
        <!-- Theme style -->
        <link href="dist/css/AdminLTE.min.css" rel="stylesheet">
        <!-- AdminLTE Skins. Choose a skin from the css/skins
           folder instead of downloading all of them to reduce the load. -->
        <link href="dist/css/skins/_all-skins.min.css" rel="stylesheet">
        <!-- Morris chart -->
        <link href="bower_components/morris.js/morris.css" rel="stylesheet">
        <!-- jvectormap -->
        <link href="bower_components/jvectormap/jquery-jvectormap.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link href="css/layouts/user_layouts.css" rel="stylesheet">
        <!-- Google Font -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic" rel="stylesheet">
        <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css">
        <!-- Stack styles : Push style here -->
        @stack('styles')

        <!-- Styles define -->
        <style>
           
        </style>        
    </head>

    <body class="hold-transition skin-green-light sidebar-mini">
    @include('components/loading_show')
        <header>
            @include('layouts.includes.web-header')
        </header>
        <div class="page-area">
            <div class="container">
                <div class="wrapper" style="height: auto; min-height: 100%;">
                    <aside class="main-sidebar main-sidebar-left">
                        <section class="sidebar">
                            <div class="user-panel">
                                <div class="pull-left image">
                                  <div class="image-sidebar"><img src="{{auth()->user()->getAvatar(45, 45)}}" height="45" width="45" class="img-circle"></div>
                                </div>
                                <div class="pull-left info">
                                    <p class="name-static">Trang cá nhân của</p>
                                    <!--<div class="org-name">Trang cá nhân của</div>-->
                                    <div class="org-name"><a href="{{route('profile')}}">{{auth()->user()->name}}</a></div>
                                </div>
                              </div>

                             @include('web.users.menu-left')
                        </section>
                    </aside>
                    <div class="content-wrapper" style="min-height: 399px;">
                        <section class="modul-name main-header">
                            <a href="#" class="sidebar-toggle toggle-btn-collaspe" data-toggle="push-menu" role="button">
                                <span class="sr-only">Toggle navigation</span>
                            </a>
                            @yield('title')
                        </section>
                        <content>
                            @yield('content')
                        </content>
                    </div>
                </div>
            </div>
        </div>
        <footer>
            @include('layouts.includes.web-footer')
        </footer>

        <!-- Scripts declare here -->
        <script src="js/jquery-2.2.0.min.js" type="text/javascript"></script>
        <!--<script src="https://code.jquery.com/jquery-2.2.0.min.js" type="text/javascript"></script>-->
        <!--<script src="{{ asset('js/app.js') }}" defer></script>-->
        <script src="js/bootstrap.min.js" type="text/javascript"></script>
        <script src="js/slick.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/common.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/jquery.mobile-menu.min.js" type="text/javascript" charset="utf-8"></script>
        <script src="js/owl.carousel.min.js" type="text/javascript" charset="utf-8"></script>
        <!-- Toastr -->
        <script src="plugins/toastr/toastr.min.js"></script>
        <!-- Bootstrap 3.3.7 -->
        <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
        <!-- DataTables -->
        <script src="bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
        <script src="bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
        <script src="plugins/datatables-responsive/dataTables.responsive.min.js"></script>
        <script src="plugins/datatables-responsive/responsive.bootstrap.min.js"></script>
        <!-- Slimscroll -->
        <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
        <!-- FastClick -->
        <script src="bower_components/fastclick/lib/fastclick.js"></script>
        <!-- Inputmask -->
        <script src="js/inputmask/jquery.inputmask.bundle.min.js"></script>
        <script src="js/inputmask/sdc-aliases.js"></script>
        <!-- Select2 -->
        <script src="bower_components/select2/dist/js/select2.full.min.js"></script>
        <!-- Toastr -->
        <script src="plugins/toastr/toastr.min.js"></script>
        <!-- Block UI -->
        <script src="plugins/blockUI/jquery.blockUI.min.js"></script>
        <!-- AdminLTE App -->
        <script src="dist/js/adminlte.min.js"></script>
        <!-- Bootboxjs -->
        <script src="js/bootbox.min.js"></script>
        <!-- SDC App -->
        <script src="js/sdc.js"></script>
        <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.js"></script>

        <script src="bower_components/bootstrap-datepicker/dist/locales/bootstrap-datepicker.vi.min.js"></script>
        <!-- Moment -->
        <script src="bower_components/moment/min/moment.min.js"></script>
        <script src="{{ asset('js/web/layouts-web.js') }}" type="text/javascript" charset="utf-8"></script>
        <script src="{{ asset('js/typeahead.bundle.js') }}"></script>
         <!-- google api -->
        <script src="https://maps.googleapis.com/maps/api/js?key={{config('app.google_map_api_key')}}&libraries=places"> </script>
        <!-- Stack script : Push scrip here -->
        @stack('scripts')

        <!-- Scripts processes -->
        <script type="text/javascript">
            $(document).on('ready', function () {
                $('#btnExpandUserpanel').on('click', function (e) {
                    var display = $( '.expand-user-panel' ).css( "display" );
                    if(display === 'none')
                        $('.expand-user-panel').css("display", "block");
                    else
                        $('.expand-user-panel').css("display", "none");
                });
            });
            var url = "{{url('/')}}";
            var userId = {{getUserId()}};
            $('#dayOfBirth').datepicker({
                language: 'vi',
                autoclose: true
            });
            var url = "{{url('/')}}";
                $('#toDate').datepicker({
                    language: 'vi',
                    autoclose: true
                });
        </script>
        <!-- Scripts processes -->

    </body>   

</html>