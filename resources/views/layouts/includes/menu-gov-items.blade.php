@foreach($items as $item)
  @if($item->hasChildren() || $item->url())
    <li@lm-attrs($item) @if($item->hasChildren()) class="treeview" @endif @lm-endattrs>
      <a @if(!$item->hasChildren()) href="{!! $item->url() !!}" @endif  style="cursor: pointer;">
        {!! $item->title !!}
        @if($item->hasChildren())
        <span class="pull-right-container">
          <i class="fa fa-angle-left pull-right"></i>
        </span>
        @endif
      </a>

      @if($item->hasChildren())
      <ul class="treeview-menu">
        @include('layouts.includes.menu-gov-items', ['items' => $item->children()])
      </ul>
      @endif
    </li>
  @endif
@endforeach