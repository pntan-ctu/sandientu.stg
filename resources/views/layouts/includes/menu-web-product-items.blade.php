@foreach($items as $key => $item)
<li @if($item->hasChildren()) class="dropdown dropdown-submenu" @endif>
     <a @if($item->hasChildren()) class="dropdown-toggle" @endif href="{!! $item->url() !!}">
    {!! $item->title !!}
   
    </a>
    @if($item->hasChildren())
        <i class="fa fa-angle-right pull-right subDropdown"></i>
    @endif
    @if($item->hasChildren())
    <ul id="menu{{$key}}" class="dropdown-menu">
        @include('layouts.includes.menu-web-product-items', ['items' => $item->children()])
    </ul>
    @endif
</li>
@endforeach