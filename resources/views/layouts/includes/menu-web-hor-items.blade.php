@foreach($items as $item)
<li @if($item->hasChildren()) class="dropdown menu-sub-hor" @endif>
    <a @if($item->hasChildren()) class="dropdown-toggle"  @endif href="{{ $item->url() }}">
    {!! $item->title !!}
    </a>
    @if($item->hasChildren())
        <i class="fa fa-angle-down subDropdown" ></i>
    @endif
    @if($item->hasChildren())
    <ul class="dropdown-menu" style="width:250px;">
        @include('layouts.includes.menu-web-hor-items', ['items' => $item->children()])
    </ul>
    @endif
</li>
@endforeach 