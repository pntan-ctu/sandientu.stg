<style>
    .twitter-typeahead {
        width: 50% !important;
        float: left;
        border-right: 1px solid #eee;
    }

    .pac-card {
        width: 36%;
        float: left;
        position: absolute;
        left: 50%;
        top: 0;
    }

    .numNoty {
        padding: 2px 3px;
        position: absolute;
        top: 2px;
        margin-left: 12px;
        background-color: red !important;
    }

    @media(max-width: 768px) {
        .numNoty {
            top: -5px;
        }
    }
</style>
@php
$numberOfUnreadMsg = 0;
$userId = getUserId();
if($userId > 0)
{
$user = \App\Models\User::find($userId);
$numberOfUnreadMsg = $user->newThreadsCount();
}
@endphp
<!-- Đăt chung để gọi hàm script logout cho nhiều vị trí -->
<form id="logout-form" action="{{ route('logout') }}" method="post" style="display: none;">@csrf</form>

<div class="top-header container-fluil none-mobile" style=" background-color: red; font-size:15px;">
    <div class="container">
        <!-- <ul class="nav navbar-nav navbar-left">
            <li class="li-shopcard"><a href="#">Kênh người bán</a></li>
            <li class="li-download"><a href="#">Tải ứng dụng</a></li>
        </ul> -->
        <!-- <ul class="nav navbar-nav navbar-left">
            @if(config('app.version', '') == 'DEV')
            <li class=""><a>Phiên bản: DEVERLOPER</a></li>
            @elseif(config('app.version', '') == 'DEMO')
            <li class=""><a>Phiên bản: DEMO</a></li>
            @elseif(config('app.version', '') == 'BETA')
            <li class=""><a>Phiên bản đang hoạt động thử nghiệm</a></li>
            @elseif(config('app.version', '') == 'TG1.0')
            <li class=""><a>Phiên bản 1.0</a></li>
            @endif
        </ul> -->

        <ul class="nav navbar-nav navbar-right" style="margin-right:40px; ">
            @if(config('app.version', '') == 'DEMO')
            <li class=""><a href="{{ route('toa-do') }}" title="Lấy tọa độ vị trí hiện tại của bạn">Lấy tọa độ hiện tại</a></li>
            @endif

            @if($userId > 0)
            <li class="li-tb" title="Bạn có {{$numberOfUnreadMsg}} tin nhắn chưa đọc">
                <a href="{{url('messenger')}}">
                    @if ($numberOfUnreadMsg > 0)
                    <span class="label label-warning numNoty">{{$numberOfUnreadMsg}}</span>
                    @endif
                    <img src="{{url('img/messenger-icon.png')}}" style="width: 22px">
                </a>
            </li>
            @endif
            <li class="li-tg"><a href="{{ route('tro-giup') }}"><b>Trợ giúp</b></a></li>

            @guest
            <li class="li-dn">
                <a class="nav-link" href="{{ route('register') }}"><b>Đăng ký</b></a>
            </li>
            <li class="li-dk">
                <a class="nav-link" href="{{ route('login') }}"><b>Đăng nhập</b></a>
            </li>
            @else
            <!-- @if (getOrgId() > 0)                                             
            <li class="li-ql">
                @if (getOrgTypeId() < 100)
                <a class="nav-link" href="{{ route('manage') }}"> Quản lý</a>
                @else
                <a class="nav-link" href="{{ route('org', ['organizationId'=>getOrgId()]) }}">Cơ sở SX, KD</a>
                @endif
            </li>
            @endif -->
            @if (isUserGov())
            <li class="li-ql">
                <a class="nav-link" href="{{ route('manage') }}"> <b>Cơ quan quản lý</b></a>
            </li>
            @endif
            @if(getRoleAccess() == \App\RoleAccess::BUSINESS)
            <!-- START VAI TRO CO SỞ -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <img src="{{getOrgModel()->getLogo(25, 25)}}" class="user-image" alt="">
                    <span class="hidden-xs">{{ getOrgModel()->name }}</span>
                </a>
                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        <!--<img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle" >-->
                        <img src="{{getOrgModel()->getLogo(100, 100)}}" class="img-circle" alt="">
                        <p>{{ getOrgModel()->name }}
                            <small>({{ Auth::user()->name }})</small>
                        </p>
                    </li>
                    <!-- Menu switch -->
                    @if(isUserBusiness())
                    <li class="user-body">
                        <div class="row">
                            <div class="text-center">
                                <a onclick="switchRoleAccess('{{\App\RoleAccess::PERSONAL}}')" style="cursor: pointer;">Chuyển vai trò: Cá nhân</a>
                            </div>
                        </div>
                    </li>
                    @endif
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <!-- <div class="col-xs-6 text-center">
                                <a href="{{ route('advertising-org.create', ['organizationId'=>getOrgId()]) }}">Đăng cung cầu</a>
                            </div> -->
                            <div class="col-xs-12 text-center">
                                <a href="{{ route('order.index', ['organizationId'=>getOrgId()]) }}">Quản lý đơn hàng</a>
                            </div>
                        </div>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{ route('org', ['organizationId'=>getOrgId()]) }}" class="btn btn-default btn-flat">Trang cá nhân</a>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng xuất</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- END VAI TRO CO SỞ -->
            @else
            <!-- START VAI TRO CÁ NHÂN -->
            <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                    <img src="{{Auth::user()->getAvatar(25, 25)}}" class="user-image" alt="">
                    <span class="hidden-xs"><b>{{ Auth::user()->name }}</b></span>
                </a>
                <ul class="dropdown-menu" style="background: white;">
                    <!-- User image -->
                    <li class="user-header">
                        <!--<img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle" >-->
                        <img src="{{Auth::user()->getAvatar(100, 100)}}" class="img-circle" alt="">
                        <p>{{ Auth::user()->name }}
                            <small>{{ Auth::user()->email }}</small>
                        </p>
                    </li>
                    <!-- Menu switch -->
                    @if(isUserBusiness())
                    <li class="user-body">
                        <div class="row">
                            <div class="text-center">
                                <a onclick="switchRoleAccess('{{\App\RoleAccess::BUSINESS}}')" style="cursor: pointer;">Chuyển vai trò: Cơ sở SXKD</a>
                            </div>
                        </div>
                    </li>
                    @endif
                    <!-- Menu Body -->
                    <li class="user-body">
                        <div class="row">
                            <div class="col-xs-6 text-center">
                                <a href="{{url('user/advertising/create')}}">Đăng cung cầu</a>
                            </div>
                            <div class="col-xs-6 text-center">
                                <a href="{{url('/user/orders')}}">Đơn hàng của tôi</a>
                            </div>
                        </div>
                    </li>
                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-left">
                            <a href="{{ route('profile') }}" class="btn btn-default btn-flat">Trang cá nhân</a>
                        </div>
                        <div class="pull-right">
                            <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng xuất</a>
                        </div>
                    </li>
                </ul>
            </li>
            <!-- END VAI TRO CÁ NHÂN -->
            @endif
            @endguest
        </ul>
    </div>
</div>
<div class="banner container-fluil">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="logo">
                    <a href="{{url('/')}}"><img src="{{url('css/images/logostg.png')}}" alt="Kết nối cung cầu nông sản, thực phẩm an toàn"></a>
                </div>
            </div>
            <div class="col-md-8 wrap-search">
                <div id="search" class="search-form" style="width: 100%;">
                    <input type="text" value="@if(isset($text) && $text) {{ $text }} @endif" id="text-search" name="search" style=" padding-left: 4%;width: 96%" placeholder="Tìm kiếm thông tin sản phẩm" />
                    <div class="pac-card" id="pac-card">
                        <div id="pac-container">
                            <input type="hidden" id="lat" value="0" name="lat" />
                            <input type="hidden" id="lng" value="0" name="lng" />
                            <input id="pac-input" type="text" placeholder="Nhập vào vị trí của bạn" style="width: 85%; float: left; padding-left: 5%;" />
                            <div id="get-location" title="Bấm vào đây để lấy tọa độ vị trí hiện tại của bạn!" style=" width: 10%;float:right;position: absolute;right: 5%;top:4px" class="check-location-desk click">
                                <img src="{{url('css/images/location.png')}}" alt="Bấm vào đây để lấy tọa độ vị trí hiện tại của bạn!" style="cursor: pointer;">
                            </div>
                        </div>
                    </div>
                    <button class="iconSearchHeader" style="width: 14%;float: right;top:0;position:absolute;" onclick="search();"><i class="fa fa-search"></i></button>

                    <div id="mapId"></div>
                </div>

            </div>
            <div class="col-md-1 wrap-search">
                <div class="card-wrapper">
                    <a class="btn-post-cart" href="{{url('get-cart')}}">
                        <i class="fa fa-shopping-cart"></i>
                        <span class="cart-count"> {{ session('cart')!= null? '0' : '' }}</span>
                    </a>
                    @include('layouts.includes.cart-hover')
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluil">
    <div class="navbar navbar-default" role="navigation">
        <div class="">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <ul class="block-mobile regis">
                    @if($userId > 0)
                    <li class="li-tb" title="Bạn có {{$numberOfUnreadMsg}} tin nhắn chưa đọc">
                        <a href="{{url('messenger')}}">
                            @if ($numberOfUnreadMsg > 0)
                            <span class="label label-warning numNoty">{{$numberOfUnreadMsg}}</span>
                            @endif
                            <img src="{{url('img/messenger-icon.png')}}" style="width: 22px">
                        </a>
                    </li>
                    @endif

                    @if(config('app.version', '') == 'DEMO')
                    <li class=""><a href="{{ route('toa-do') }}" title="Lấy tọa độ vị trí hiện tại của bạn">Lấy tọa độ hiện tại</a></li>
                    @else
                    <li class="li-tg"><a href="{{ route('tro-giup') }}">Trợ giúp</a></li>
                    @endif

                    @guest
                    <li class="li-dn">
                        <a class="nav-link" href="{{ route('register') }}">Đăng ký</a>
                    </li>
                    <li class="li-dk">
                        <a class="nav-link" href="{{ route('login') }}">Đăng nhập</a>
                    </li>
                    @else

                    <!-- @if (getOrgId() > 0)                                             
                    <li class="li-ql">
                        @if (getOrgTypeId() < 100)
                        <a class="nav-link" href="{{ route('manage') }}"> Quản lý</a>
                        @else
                        <a class="nav-link" href="{{ route('org', ['organizationId'=>getOrgId()]) }}">Cơ sở SX, KD</a>
                        @endif
                    </li>
                    @endif -->

                    @if (isUserGov())
                    <li class="li-ql">
                        <a class="nav-link" href="{{ route('manage') }}"> Cơ quan quản lý</a>
                    </li>
                    @endif

                    @if(getRoleAccess() == \App\RoleAccess::BUSINESS)
                    <!-- START VAI TRO CO SỞ -->
                    <li class="dropdown user user-menu">
                        <a href="#" id="btnExpandOrganpanel" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <img src="{{getOrgModel()->getLogo(25, 25)}}" class="user-image" alt="">
                            <span class="hidden-xs">{{ getOrgModel()->name }}</span>
                        </a>
                        <ul class="dropdown-menu expand-organ-panel">
                            <!-- User image -->
                            <li class="user-header">
                                <!--<img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle" >-->
                                <img src="{{getOrgModel()->getLogo(100, 100)}}" class="img-circle" alt="">
                                <p>{{ getOrgModel()->name }}
                                    <small>({{ Auth::user()->name }})</small>
                                </p>
                            </li>
                            <!-- Menu switch -->
                            @if(isUserBusiness())
                            <li class="user-body">
                                <div class="row">
                                    <div class="text-center">
                                        <a onclick="switchRoleAccess('{{\App\RoleAccess::PERSONAL}}')" style="cursor: pointer;">Chuyển vai trò: Cá nhân</a>
                                    </div>
                                </div>
                            </li>
                            @endif
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        <a href="{{ route('advertising-org.create', ['organizationId'=>getOrgId()]) }}">Đăng cung cầu</a>
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <a href="{{ route('order.index', ['organizationId'=>getOrgId()]) }}">Quản lý đơn hàng</a>
                                    </div>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ route('org', ['organizationId'=>getOrgId()]) }}" class="btn btn-default btn-flat">Trang cơ sở</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng xuất</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- END VAI TRO CO SỞ -->
                    @else
                    <!-- START VAI TRO CÁ NHÂN -->
                    <li class="dropdown user user-menu">
                        <a href="#" id="btnExpandUserpanel" class="dropdown-toggle" data-toggle="dropdown" aria-expanded="true">
                            <img src="{{Auth::user()->getAvatar(25, 25)}}" class="user-image" alt="">
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu expand-user-panel">
                            <!-- User image -->
                            <li class="user-header">
                                <!--<img src="{{url('dist/img/user2-160x160.jpg')}}" class="img-circle" >-->
                                <img src="{{Auth::user()->getAvatar(100, 100)}}" class="img-circle" alt="">
                                <p>{{ Auth::user()->name }}
                                    <small>{{ Auth::user()->email }}</small>
                                </p>
                            </li>
                            <!-- Menu switch -->
                            @if(isUserBusiness())
                            <li class="user-body">
                                <div class="row">
                                    <div class="text-center">
                                        <a onclick="switchRoleAccess('{{\App\RoleAccess::BUSINESS}}')" style="cursor: pointer;">Chuyển vai trò: Cơ sở SXKD</a>
                                    </div>
                                </div>
                            </li>
                            @endif
                            <!-- Menu Body -->
                            <li class="user-body">
                                <div class="row">
                                    <div class="col-xs-6 text-center">
                                        <a href="{{url('user/advertising/create')}}">Đăng cung cầu</a>
                                    </div>
                                    <div class="col-xs-6 text-center">
                                        <a href="{{url('/user/orders')}}">Đơn hàng của tôi</a>
                                    </div>
                                </div>
                            </li>
                            <!-- Menu Footer-->
                            <li class="user-footer">
                                <div class="pull-left">
                                    <a href="{{ route('profile') }}" class="btn btn-default btn-flat">Trang cá nhân</a>
                                </div>
                                <div class="pull-right">
                                    <a class="btn btn-default btn-flat" href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Đăng xuất</a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <!-- END VAI TRO CÁ NHÂN -->
                    @endif

                    @endguest
                </ul>
            </div>
            <div class="navbar-collapse collapse container menu-hor">
                <ul class="nav navbar-nav">
                    <li class="dropdown dmsp">
                        <a class="dropdown-toggle toggle-button-dmsp" class="navbar-toggle" data-toggle="collapse"><i class="fa fa-list"></i>DANH MỤC SẢN PHẨM</a>

                        @include('layouts.includes.menu-web-product')
                    </li>
                    @include('layouts.includes.menu-web-hor')
                </ul>

            </div>
        </div>
    </div>

</div>

@push('scripts')
<!-- <script src="https://maps.googleapis.com/maps/api/js?key={{config('app.google_map_api_key')}}&sensor=false&libraries=places&callback=initialize" async defer>
</script> -->

<script>
    $('#get-location').click(function() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        }

        function showPosition(position) {
            var latitude = position.coords.latitude.toFixed(6);
            var longitude = position.coords.longitude.toFixed(6);
            $('#lat').val(latitude);
            $('#lng').val(longitude);
            var geocoder = new google.maps.Geocoder;
            geocodeLatLng(geocoder, latitude, longitude);
        }

        function geocodeLatLng(geocoder, latitude, longitude) {
            var latlng = {
                lat: parseFloat(latitude),
                lng: parseFloat(longitude)
            };
            geocoder.geocode({
                'location': latlng
            }, function(results, status) {
                console.log(results);
                $('#pac-input').val(results[4].address_components[0].long_name + ", " +
                    results[4].address_components[1].long_name + ", " +
                    results[4].address_components[2].long_name + ", " +
                    results[4].address_components[3].long_name);
            });
        }
    });
    // This example requires the Places library. Include the libraries=places
    // parameter when you first load the API. For example:
    function initialize() {
        if ("{{Request::get('type')}}" && "{{Request::get('type')}}" == 'dia-diem') {
            //Bản đồ tìm kiếm
            initMap1();
        }
        if ("{{Route::current()->getName()}}" == 'map.index') {
            //Bản đồ phân bố cơ sở
            initMap();
        }
        if ("{{Route::current()->getName()}}" == 'dia-diem-kinh-doanh.index') {
            //Bản đồ cho địa điểm kinh doanh
            initMap3();
        } else {
            //Tìm vị trí hiện tại
            initMap2();
        }
    }

    function initMap2() {
        var map = new google.maps.Map(document.getElementById('mapId'), {
            center: {
                lat: -33.8688,
                lng: 151.2195
            },
            zoom: 13
        });
        var card = document.getElementById('pac-card');
        var input = document.getElementById('pac-input');
        var types = document.getElementById('type-selector');
        var strictBounds = document.getElementById('strict-bounds-selector');

        map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);

        var autocomplete = new google.maps.places.Autocomplete(input);

        // Bind the map's bounds (viewport) property to the autocomplete object,
        // so that the autocomplete requests use the current map bounds for the
        // bounds option in the request.
        autocomplete.bindTo('bounds', map);

        // Set the data fields to return when the user selects a place.
        autocomplete.setFields(
            ['address_components', 'geometry', 'icon', 'name']);

        var infowindow = new google.maps.InfoWindow();
        var infowindowContent = document.getElementById('infowindow-content');
        infowindow.setContent(infowindowContent);
        var marker = new google.maps.Marker({
            map: map,
            anchorPoint: new google.maps.Point(0, -29)
        });
        autocomplete.addListener('place_changed', function() {
            infowindow.close();
            marker.setVisible(false);
            var place = autocomplete.getPlace();
            if (!place.geometry) {
                // User entered the name of a Place that was not suggested and
                // pressed the Enter key, or the Place Details request failed.
                window.alert("No details available for input: '" + place.name + "'");
                return;
            }

            // If the place has a geometry, then present it on a map.
            if (place.geometry.viewport) {
                map.fitBounds(place.geometry.viewport);
                $('#lat').val(place.geometry.location.lat());
                $('#lng').val(place.geometry.location.lng());
            } else {
                map.setCenter(place.geometry.location);
                map.setZoom(17); // Why 17? Because it looks good.
                $('#lat').val(place.geometry.location.lat());
                $('#lng').val(place.geometry.location.lng());

            }
            marker.setPosition(place.geometry.location);
            marker.setVisible(true);
            var address = '';
            if (place.address_components) {
                address = [
                    (place.address_components[0] && place.address_components[0].short_name || ''),
                    (place.address_components[1] && place.address_components[1].short_name || ''),
                    (place.address_components[2] && place.address_components[2].short_name || '')
                ].join(' ');
            }
            infowindow.open(map, marker);
        });

        // Sets a listener on a radio button to change the filter type on Places
        // Autocomplete.

    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
<script>
    function switchRoleAccess(toRole) {
        $.ajax({
            url: "{{url('switch-role-access')}}",
            type: 'POST',
            data: {
                role: toRole
            },
            success: function(msg) {
                toastr.success("Bạn đã chuyển sang vai trò: " + (toRole == '{{ \App\RoleAccess::BUSINESS }}' ? 'Cơ sở SXKD' : 'Cá nhân'));
                location.reload();
            },
            dataType: "text"
        });
    }

    function search() {
        var text = $("#text-search").val();
        if(text.length > 0) {
            var url = "{{ url('search') }}" + "?text=" + text + "&type=san-pham&choose=lien-quan";
            var lat = $('#lat').val();
            var long = $('#lng').val();
            if (lat.length > 0 && long.length > 0 && lat > 0 && long > 0) {
                url = "{{ url('search') }}" + "?text=" + text + "&type=dia-diem&choose=lien-quan&lat=" + lat + "&long=" + long;
            }
            window.location.href = url;
        }

        // Check for Geolocation API permissions
        /*navigator.permissions.query({
            name: 'geolocation'
        })
        .then(function(permissionStatus) {
            console.log('geolocation permission state is ', permissionStatus.state);
            if (permissionStatus.state == 'granted') {
                navigator.geolocation.getCurrentPosition(function(position) {
                    var text = jQuery("#text-search").val();
                    if (text && text != '') {
                        var lat = $('#lat').val();
                        var lng = $('#lng').val();
                        var url = "{{ url('search') }}" + "?text=" + text + "&type=dia-diem&choose=lien-quan&lat=" + lat + "&long=" + lng;
                        window.location.href = url;
                    } else {
                        return;
                    }
                });
            }
            else {
                var text = jQuery("#text-search").val();
                if (text && text != '') {
                    var url = "{{ url('search') }}" + "?text=" + text + "&type=san-pham&choose=lien-quan";
                    window.location.href = url;
                } else {
                    return;
                }
            }
        });*/
    }
    $('#text-search').keypress(function(e) {
        var key = e.which;
        if (key == 13) // the enter key code
        {
            search();
        }
    });
    $(document).ready(function() {
        var bloodhound = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.whitespace,
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '{{url("/find?q=%QUERY%")}}',
                wildcard: '%QUERY%'
            },
        });
        $('#text-search').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'users',
            source: bloodhound,
            display: function(data) {
                return data.name //Input value to be set when you select a suggestion.
            },
            templates: {
                empty: [
                    //                    '<div class="list-group search-results-dropdown"><div class="list-group-item">Không có gợi ý ...</div></div>'
                ],
                header: [
                    '<div class="list-group search-results-dropdown">'
                ],
                suggestion: function(data) {
                    return '<div style="font-weight:normal; margin-top:-10px ! important;" class="list-group-item">' + data.name + '</div></div>'
                }
            }
        });
    });
</script>
@endpush 