<ul class="sidebar-menu" data-widget="tree">
    @include('layouts.includes.menu-org-items', ['items' => $mainMenuOrg->roots()])
</ul>

@if (isUserBusiness())
<div class="my-business-logout">
    <a class="my-business-name" href="{{ route('profile') }}">
    	<i class="sdc fa fa-user"></i> 
    	<span class="my-business-menu-content">Trang cá nhân của tôi</span>
    </a>
</div>
@endif