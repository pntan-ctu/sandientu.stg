<!--<div class="container-fluil wrap-top-ft">
    <div class="bg-top-ft">
        <div class="container">
            <div class="row top-ft">
                <div class="">
                    <div class="col-md-4 col-md-offset-4 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                        <div class="social new-letter">
                            <form id="ss-form">
                                <p>NEWSLETTER</p>
                                <div class="form-group form-email">
                                    <input type="email" class="exportInput" placeholder="Nhập email của bạn">
                                    <button type="submit" class="btn btn-success newsletter_button"><i class="fa fa-envelope"></i></button>
                                </div>
                            </form>
                            <p>Mạng xã hội</p>
                            <div class="social-icon">
                                <a href="#" title="" target="_blank">
                                    <i class="fa fa-facebook-square"></i>
                                </a>
                                <a href="#" title="" target="_blank">
                                    <i class="fa fa-instagram"></i>
                                </a>
                            </div>  
                        </div>
                    </div>  
                </div>  
            </div> 
        </div>   
    </div>  
</div>-->
<div class="ft-before">
    <div class="container">
        <div class="row">
            <div class="col-md-4" style="margin-bottom: 20px;">
                <div class="about-footer">
                    <!--<div class="summary">Hỗ trợ kết nối giữa các cơ sở sản xuất với cơ sở kinh doanh và người tiêu dùng nhằm đẩy mạnh sản xuất, kinh doanh, tiêu thụ sản phẩm nông sản, thực phẩm an toàn thông qua website, mạng xã hội, đã được các cơ quan chức năng quản lý, kiểm soát chặt chẽ...</div>-->
                                        
                    <div class="footer-logo widget" >
                        <h3 class="widget-title" >TẢI ỨNG DỤNG DI ĐỘNG </h3>
                       <!--  <span style="float:left; width: 84px; border:1px solid #ddd; margin-right: 10px;"><img  src="{{url('css/images/qrcode-app.png')}}" class="attachment-full size-full" alt="Tải ứng dụng di động phần mềm kết nối cung cầu"></span> -->

                        <div style="">
                            <div><a href="" target="_blank"><img style="margin-top: 10px;" width="100" height="20" src="{{url('css/images/adr.png')}}" class="attachment-full size-full" alt="Tải ứng dụng trên Google Play"> </a></div>
<!-- {{ config('app.download_app_andoid', '') }} -->
                            <div><a href="" target="_blank"><img style="margin-top: 16px;" width="80" height="20" src="{{url('css/images/ios.png')}}" class="attachment-full size-full" alt="Tải ứng dụng trên App Store"> </a></div>
                        </div>    <!-- {{ config('app.download_app_ios', '') }} -->
                    </div>
                    <!-- <iframe style="margin-top: 20px;" src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fnongsanantoanthanhhoa.vn&tabs=272&width=340&height=154&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="360" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe> -->
                      <!--  <iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2FS%25C3%25A0n-Giao-D%25E1%25BB%258Bch-B%25E1%25BA%25A5t-%25C4%2590%25E1%25BB%2599ng-S%25E1%25BA%25A3n-T%25E1%25BB%2589nh-Ti%25E1%25BB%2581n-Giang-105963587540682%2F%3Fmodal%3Dadmin_todo_tour%26ref%3Dadmin_to_do_step_controller&tabs=timeline&width=340&height=154&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="340" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
                --></div>
            </div>
            <div class="col-md-8">
                <div class="row" >
                    <div class="col-md-4 col-xs-6 widget">
                        <h3 class="widget-title">THÔNG TIN</h3>
                        <ul class="widget-menu">
                            <li class="munu-footer"><a href="{{url('about/gioi-thieu')}}">Giới thiệu</a></li>
                            <li class="munu-footer"><a href="{{url('about/quy-che-hoat-dong')}}">Quy chế hoạt động</a></li>
                            <li class="munu-footer"><a href="{{url('about/dieu-khoan-tham-gia')}}">Điều khoản tham gia</a></li>
                            <li class="munu-footer"><a href="{{url('about/privacy-policy')}}">Chính sách bảo mật</a></li>
                            <!-- <li class="munu-footer"><a href="{{url('document/tai-lieu-hdsd-nguoi-dung.pdf')}}" target="brank">Hướng dẫn sử dụng</a></li> -->
                            <li class="munu-footer"><a target="brank">Hướng dẫn sử dụng</a></li>

                        </ul>
                        <!-- <div class="footer-logo" style="margin-top: 15px;"><a href="#"><img width="150" height="80" src="{{url('css/images/dadangky.png')}}" class="attachment-full size-full" alt=""></a></div> -->
                    </div>
                    <div class="col-md-4 col-xs-6  widget">
                        <h3 class="widget-title">CƠ QUAN QUẢN LÝ</h3>
                        <ul class="widget-menu">
                            <li class="munu-footer"><a href="{{url('about/ubnd-tinh-thanh-hoa')}}">UBND Tỉnh Hậu Giang  </a></li>
                            <li class="munu-footer"><a href="{{url('about/cac-so-ban-nganh')}}">Sở, ban, ngành</a></li>
                            <li class="munu-footer"><a href="{{url('about/ubnd-huyen-thi-tp')}}">UBND huyện, thị, thành phố</a></li>
                            <li class="munu-footer"><a href="{{url('about/ubnd-xa-phuong')}}">UBND xã, phường, thị trấn</a></li>
                            <li class="munu-footer"><a href="{{url('lien-he')}}">Thông tin liên hệ</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4 col-xs-6  widget">
                       <h3 class="widget-title">THAM GIA CÙNG CHÚNG TÔI</h3>
                        <ul class="widget-menu">
                            <li class="munu-footer"><a href="{{url('about/co-so-sxkd')}}">Công ty kinh doanh Nông Sản </a></li>
                            <li class="munu-footer"><a href="{{url('about/nguoi-tieu-dung')}}">Người mua Nông Sản</a></li>
                        </ul>
                       
                        @if(config('app.dangky_bct', ''))
                        <a target="_blank" rel="noopener noreferrer" href="http://online.gov.vn/HomePage/WebsiteDisplay.aspx?DocId=18375" style="margin-top: 15px;">
                            <img width="150" height="80" src="{{url('css/images/dadangky.png')}}" class="attachment-full size-full" alt="">
                        </a>
                        <a target="_blank" rel="noopener noreferrer" href="http://online.gov.vn/HomePage/AppDisplay.aspx?DocId=29" style="margin-top: 15px;">
                            <img width="150" height="80" src="{{url('css/images/dadangky.png')}}" class="attachment-full size-full" alt="">
                        </a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</div>
<div class="end-ft" style="background-color: red; font-size:15px;">
    <span>© 2019 Sàn giao dịch điện tử Hậu Giang.</span>| <span class="hotline-ft">02373961818</span>|<span class="email-ft">vpdpattp@haugiang.gov.vn</span>
</div>
