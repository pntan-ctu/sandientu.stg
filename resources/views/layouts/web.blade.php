<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!--<META HTTP-EQUIV="Pragma" CONTENT="no-cache">-->
        <link rel="canonical" href="{{Request::url()}}"/>
        <meta http-equiv='cache-control' content='no-cache'>

        <!-- SEO -->
        <meta name="description" content="Kết nối cung cầu nông sản, thực phẩm an toàn trên địa bàn tỉnh Tiền Giang">
        <meta name="title" content="Kết nối cung cầu nông sản, thực phẩm an toàn">
        <meta name="keywords" content="thực phẩm sạch, nông sản tiền giang, an toàn thức phẩm, kết nối cung cầu,
              attp, nông nghiệp sạch, thực phẩm hữu cơ, nongsanantoantiengiang, thực phẩm, tiền giang, Kết nối cung cầu nông sản thực phẩm an toàn," >
        <!-- end SEO -->
        <!-- tag share facebook -->
        <meta property="og:description" content="@yield('description')">
        <meta property="og:title" content="@yield('og:title')">
        <meta property="og:image" content="@yield('og:image')">
        <meta property="og:site_name" content="Kết nối cung cầu nông sản, thực phẩm an toàn">
        <meta property="og:url" content="{{Request::url()}}" />
        <meta property="og:type" content="article" />
        <!-- end tag facebook -->
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <title>@yield('title') - {{ config('app.name') }}</title>
        <!-- <title>@yield('title') - Kết nối cung cầu nông sản, thực phẩm an toàn</title> -->
        <!-- Favicon -->
        @include("layouts.includes.favicon")
        <!-- Fonts -->
        <link rel="dns-prefetch" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">
        <!-- Bootstrap 3.3.7 -->
        <link rel="stylesheet" href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{ asset('bower_components/font-awesome/css/font-awesome.min.css')}}">
        <!-- Select2 -->
        <link rel="stylesheet" href="{{ asset('bower_components/select2/dist/css/select2.min.css')}}">
        <link href="{{ asset('css/style-index.css') }}" rel="stylesheet">
        <link href="{{ asset('css/style-responsive.css') }}" rel="stylesheet">
        <!--<link href="{{ asset('css/ketnoi_cc.css') }}" rel="stylesheet">-->
        <link href="{{ asset('css/slick.css') }}" rel="stylesheet">
        <link href="{{ asset('css/slick-theme.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{ asset('css/nhomsp.css') }}">
        <link href="{{ asset('css/chitietsanpham.css') }}" rel="stylesheet">
        <link href="{{ asset('css/news.css') }}" rel="stylesheet">
        <link href="{{ asset('css/help.css') }}" rel="stylesheet">
        <link href="{{ asset('css/cososxkd.css') }}" rel="stylesheet">
        <link href="{{ asset('css/chitietcssxkd.css') }}" rel="stylesheet">
        <link href="{{ asset('css/login.css') }}" rel="stylesheet">
        <link href="{{ asset('css/giohang.css') }}" rel="stylesheet">
        <link href="{{ asset('css/ketnoicc.css') }}" rel="stylesheet">
        <link rel="stylesheet" href="{{asset('plugins/toastr/toastr.min.css') }}">
        <link href="{{ asset('css/bootstrap-rating.css') }}" rel="stylesheet">
        <link href="{{ asset('css/document.css') }}" rel="stylesheet">
        <link href="{{ asset('css/video.css') }}" rel="stylesheet">
        <link href="{{ asset('css/images.css') }}" rel="stylesheet">
        <link href="{{ asset('css/ketquatimkiem.css') }}" rel="stylesheet">
        <link href="{{ asset('css/jquery.fancybox.css') }}" type="text/css" media="screen" rel="stylesheet">
        <style type="text/css">
            .invalid-feedback {
                display: inline-block;
                color: red;    
                font-weight: normal;
            }
        </style>

        <!-- Stack styles : Push style here -->
        @stack('styles')
    </head>
    <body>
        @include('components/loading_show')
        <header>
            @include('layouts.includes.web-header')
        </header>

    <content>
        @yield('content')
        @include('web.trangchu.modal_lienhe')
    </content>

    <footer>
        @include('layouts.includes.web-footer')
    </footer>
    <div id="fb-root"></div>
    <!-- Scripts declare here -->
    <script src="{{ asset('js/jquery-2.2.0.min.js') }}" type="text/javascript"></script>
    <!--<script src="{{ asset('js/app.js') }}" defer></script>-->
    <script src="{{ asset('js/common.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
    <!-- Select2 -->
    <script src="{{ asset('bower_components/select2/dist/js/select2.full.min.js') }}"></script>
    <!-- Toastr -->
    <script src="{{ asset('plugins/toastr/toastr.min.js') }}"></script>
    <!-- Block UI -->
    <script src="{{ asset('plugins/blockUI/jquery.blockUI.min.js') }}"></script>

    <script src="{{ asset('js/slick.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('js/jquery.fancybox.pack.js') }}" type="text/javascript" ></script>
    <script src="{{ asset('js/jquery.mobile-menu.min.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('js/owl.carousel.min.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('js/bootstrap-rating.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src="{{ asset('js/web/layouts-web.js') }}" type="text/javascript" charset="utf-8"></script>
    <script src='https://www.google.com/recaptcha/api.js?hl=vi'></script>
    <script src="{{ asset('js/typeahead.bundle.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key={{config('app.google_map_api_key')}}&libraries=places"> </script>
    <!-- Stack script : Push scrip here -->
    @stack('scripts')

    <!-- Scripts processes -->
    <script type="text/javascript">
var url = "{{url('/')}}";
var userId = {{getUserId()}};
var urlAds = "{{route('ket-noi')}}";
//        (function(d, s, id) {
//        var js, fjs = d.getElementsByTagName(s)[0];
//        if (d.getElementById(id)) return;
//        js = d.createElement(s); js.id = id;
//        js.src = "https://connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v3.0";
//        fjs.parentNode.insertBefore(js, fjs);
//      }(document, 'script', 'facebook-jssdk'));
    </script>
    
</body>

</html>
