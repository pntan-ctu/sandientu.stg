<?php

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Authors\AbilityName;
use App\OrgType;
use App\Constants;

class BouncerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // =================== NHÓM QUYỀN MẶC ĐỊNH BAN ĐẦU (KHÔNG ĐƯỢC THAY ĐỐI) ==================

        //Tạo role (nhóm) Quản trị hệ thống
        $role_admin = Bouncer::role()->firstOrCreate([
            'name' => 'admin',
            'title' => 'Quản trị hệ thống',
            'scope' => OrgType::ADMIN,
        ]);
        
        //Tạo role (nhóm) UBND Tỉnh (VPĐP)
        $role_ubnd_tinh = Bouncer::role()->firstOrCreate([
            'name' => 'ubnd-tinh',
            'title' => 'UBND Tỉnh (VPĐP)',
            'scope' => OrgType::VPDP,
        ]);

        //Tạo role (nhóm) Sở Y tế
        $role_so_yte = Bouncer::role()->firstOrCreate([
            'name' => 'so-yte',
            'title' => 'Sở Y tế',
            'scope' => OrgType::SYT,
        ]);

        //Tạo role (nhóm) Sở NN&PTNT
        $role_so_nn_ptnt = Bouncer::role()->firstOrCreate([
            'name' => 'so-nn-ptnt',
            'title' => 'Sở NN&PTNT',
            'scope' => OrgType::SNN,
        ]);

        //Tạo role (nhóm) Sở Công thương
        $role_so_ct = Bouncer::role()->firstOrCreate([
            'name' => 'so-ct',
            'title' => 'Sở Công thương',
            'scope' => OrgType::SCT,
        ]);

        //Tạo role (nhóm) UBND Huyện
        $role_huyen = Bouncer::role()->firstOrCreate([
            'name' => 'ubnd-huyen',
            'title' => 'UBND Huyện',
            'scope' => OrgType::HUYEN,
        ]);

        //Tạo role (nhóm) UBND Xã
        $role_xa = Bouncer::role()->firstOrCreate([
            'name' => 'ubnd-xa',
            'title' => 'UBND Xã',
            'scope' => OrgType::XA,
        ]);

        //Tạo role (nhóm) Chủ cơ sở sản xuất
        $role_own_cssx = Bouncer::role()->firstOrCreate([
            'name' => Constants::ROLE_CHU_CSSX, //'chu-cssx',
            'title' => 'Chủ CSSX',
            'scope' => OrgType::CSSX,
        ]);

        //Tạo role (nhóm) Chủ cơ sở kinh doanh
        $role_own_cskd = Bouncer::role()->firstOrCreate([
            'name' => Constants::ROLE_CHU_CSKD, //'chu-cskd',
            'title' => 'Chủ CSKD',
            'scope' => OrgType::CSKD,
        ]);

        /*============ TẠO CÁC ABILITY (QUYỀN) CỦA HỆ THỐNG ==============*/

        //Tạo ability (quyền) toàn quyền và gán role admin có ability toàn quyền này
        //Bouncer::allow($role_admin)->everything(['group' => 'Toàn quyền', 'title' => 'Toàn quyền quản lý tất cả']);

        //Toàn quyền
        Bouncer::ability()->firstOrCreate([
            'name' => '*',
            'group' => 'Toàn quyền',
            'title' => 'Toàn quyền quản lý tất cả',
            'entity_type' => '*',
        ]);

        //Quản lý vùng sản xuất
        Bouncer::ability()->firstOrCreate([
            'name' => '*',
            'group' => 'Quản lý vùng sản xuất',
            'title' => 'Tất cả',
            'entity_type' => App\Models\Area::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_AREA,
            'group' => 'Quản lý vùng sản xuất',
            'title' => 'Thuộc địa phương quản lý',
            'entity_type' => App\Models\Area::class,
        ]);

        //Quản lý địa điểm kinh doanh
        Bouncer::ability()->firstOrCreate([
            'name' => '*',
            'group' => 'Quản lý địa điểm kinh doanh',
            'title' => 'Tất cả',
            'entity_type' => App\Models\CommercialCenter::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_COMMERCIAL_CENTER,
            'group' => 'Quản lý địa điểm kinh doanh',
            'title' => 'Thuộc địa phương quản lý',
            'entity_type' => App\Models\CommercialCenter::class,
        ]);

        //Quản lý cơ sở SXKD
        Bouncer::ability()->firstOrCreate([
            'name' => '*',
            'group' => 'Quản lý cơ sở và kiểm duyệt các thông tin của cơ sở',
            'title' => 'Tất cả cơ sở',
            'entity_type' => App\Models\Organization::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_ORG_GOV,
            'group' => 'Quản lý cơ sở và kiểm duyệt các thông tin của cơ sở',
            'title' => 'Thuộc địa phương quản lý',
            'entity_type' => App\Models\Organization::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_ORG_GOV_HEALTH,
            'group' => 'Quản lý cơ sở và kiểm duyệt các thông tin của cơ sở',
            'title' => 'Thuộc địa phương quản lý - Lĩnh vực y tế',
            'entity_type' => App\Models\Organization::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_ORG_GOV_AGRICULTURE,
            'group' => 'Quản lý cơ sở và kiểm duyệt các thông tin của cơ sở',
            'title' => 'Thuộc địa phương quản lý - Lĩnh vực nông nghiệp',
            'entity_type' => App\Models\Organization::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE,
            'group' => 'Quản lý cơ sở và kiểm duyệt các thông tin của cơ sở',
            'title' => 'Thuộc địa phương quản lý - Lĩnh vực công thương',
            'entity_type' => App\Models\Organization::class,
        ]);

        //Kiểm duyệt tin cung cầu        
        Bouncer::ability()->firstOrCreate([
            'name' => '*',
            'group' => 'Kiểm duyệt tin cung cầu',
            'title' => 'Tất cả',
            'entity_type' => App\Models\Advertising::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_CENSORSHIP_ADVERTISING,
            'group' => 'Kiểm duyệt tin cung cầu',
            'title' => 'Thuộc địa phương quản lý',
            'entity_type' => App\Models\Advertising::class,
        ]);

        //Xử lý thanh viên vi phạm, thông tin phản hồi
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_VERIFY_MEMBER_INFRINGMENT,
            'group' => 'Xử lý thông tin phản ánh vi phạm',
            'title' => 'Thành viên vi phạm',
            'entity_type' => App\Models\Infringement::class,
        ]);

        //Xử lý thông tin phản hồi
        Bouncer::ability()->firstOrCreate([
            'name' => '*',
            'group' => 'Xử lý thông tin phản hồi',
            'title' => 'Tất cả thông tin',
            'entity_type' => App\Models\Contact::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_VERIFY_CONTACT,
            'group' => 'Xử lý thông tin phản hồi',
            'title' => 'Chỉ thông tin thuộc CQQL tiếp nhận',
            'entity_type' => App\Models\Contact::class,
        ]);

        //Thống kê, báo cáo
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_REPORT_ALL,
            'group' => 'Thống kê, báo cáo',
            'title' => 'Dữ liệu trên toàn hệ thống',
            'entity_type' => '*',
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_REPORT_SCOPE,
            'group' => 'Thống kê, báo cáo',
            'title' => 'Dữ liệu thuộc phạm vi quản lý',
            'entity_type' => '*',
        ]);

        //Quản lý thông tin hữu ích
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_NEWS_ARTICLE,
            'group' => 'Quản lý thông tin hữu ích',
            'title' => 'Tin tức và bình luận',
            'entity_type' => \App\Models\NewsArticle::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_DOCUMENT,
            'group' => 'Quản lý thông tin hữu ích',
            'title' => 'Văn bản',
            'entity_type' => \App\Models\Document::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_ALBUM,
            'group' => 'Quản lý thông tin hữu ích',
            'title' => 'Thư viện ảnh',
            'entity_type' => \App\Models\Album::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_VIDEO,
            'group' => 'Quản lý thông tin hữu ích',
            'title' => 'Thư viện video',
            'entity_type' => \App\Models\Video::class,
            'scope' => OrgType::VPDP,
        ]);

        //Tiện ích web
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_SLIDE,
            'group' => 'Quản lý tiện ích web',
            'title' => 'Slide ảnh',
            'entity_type' => \App\Models\SlideShow::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_WEBLINK,
            'group' => 'Quản lý tiện ích web',
            'title' => 'Liên kết web',
            'entity_type' => \App\Models\Weblink::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_POLL,
            'group' => 'Quản lý tiện ích web',
            'title' => 'Thăm dò ý kiến',
            'entity_type' => \App\Models\Poll::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_HELP,
            'group' => 'Quản lý tiện ích web',
            'title' => 'Quản lý trợ giúp',
            'entity_type' => \App\Models\Help::class,
            'scope' => OrgType::VPDP,
        ]);

        //Tin nhắn SMS
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_SMS_SEND,
            'group' => 'Tin nhắn SMS',
            'title' => 'Gửi SMS',
            'entity_type' => \App\Models\SmsHistory::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_SMS_HISTORY,
            'group' => 'Tin nhắn SMS',
            'title' => 'Lịch sử gửi SMS',
            'entity_type' => \App\Models\SmsHistory::class,
            'scope' => OrgType::VPDP,
        ]);

        //Quản lý danh mục hệ thống
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_LIST_PRODUCT_CATEGORY,
            'group' => 'Quản lý danh mục hệ thống',
            'title' => 'Loại sản phẩm',
            'entity_type' => \App\Models\ProductCategory::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_LIST_CERTIFICATE_CATEGORY,
            'group' => 'Quản lý danh mục hệ thống',
            'title' => 'Loại chứng nhận, xác nhận',
            'entity_type' => \App\Models\CertificateCategory::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_LIST_NEWS_GROUP,
            'group' => 'Quản lý danh mục hệ thống',
            'title' => 'Chuyên mục tin tức',
            'entity_type' => \App\Models\NewsGroup::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_LIST_DOCUMENT_FIELD,
            'group' => 'Quản lý danh mục hệ thống',
            'title' => 'Lĩnh vực văn bản',
            'entity_type' => \App\Models\DocumentField::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_LIST_DOCUMENT_TYPE,
            'group' => 'Quản lý danh mục hệ thống',
            'title' => 'Loại văn bản',
            'entity_type' => \App\Models\DocumentType::class,
            'scope' => OrgType::VPDP,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_LIST_DOCUMENT_ORGAN,
            'group' => 'Quản lý danh mục hệ thống',
            'title' => 'Cơ quan ban hành văn bản',
            'entity_type' => \App\Models\DocumentOrgan::class,
            'scope' => OrgType::VPDP,
        ]);

        //Các quyền quản lý người dùng
        //Quyền * này bao gồm cả user thuộc adim (ogrtype = 1)
        Bouncer::ability()->firstOrCreate([
            'name' => '*',
            'group' => 'Quản lý người dùng',
            'title' => 'Tất cả người dùng',
            'entity_type' => App\Models\User::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_USER_INTERNAL_GOV,
            'group' => 'Quản lý người dùng',
            'title' => 'Cán bộ nội bộ của CQQL',
            'entity_type' => App\Models\User::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_USER_CHILD_GOV,
            'group' => 'Quản lý người dùng',
            'title' => 'Cán bộ của các CQQL cấp dưới',
            'entity_type' => App\Models\User::class,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_USER_MEMBER,
            'group' => 'Quản lý người dùng',
            'title' => 'Thành viên đăng ký',
            'entity_type' => App\Models\User::class,
        ]);

        //Hệ thống
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::MANAGE_GOV_INFO,
            'group' => 'Hệ thống',
            'title' => 'Thông tin tổ chức',
            'entity_type' => '*',
        ]);

        // =================== QUYỀN CHO TRANG CƠ SỞ ==================
        //Các quyền của cơ sở sản xuất
        $ability_own_cssx = Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::ORG_OWNE,
            'group' => 'Quản lý cơ sở sản xuất',
            'title' => 'Toàn quyền (chủ cơ sở)',
            'entity_type' => '*',
            'scope' => OrgType::CSSX,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::ORG_EXCHANGE,
            'group' => 'Quản lý cơ sở sản xuất',
            'title' => 'Các chức năng giao dịch',
            'entity_type' => '*',
            'scope' => OrgType::CSSX,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::ORG_MANAGE,
            'group' => 'Quản lý cơ sở sản xuất',
            'title' => 'Các chức năng quản lý',
            'entity_type' => '*',
            'scope' => OrgType::CSSX,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::ORG_INFO,
            'group' => 'Quản lý cơ sở sản xuất',
            'title' => 'Thông tin của cơ sở',
            'entity_type' => '*',
            'scope' => OrgType::CSSX,
        ]);

        //Cac quyền của cơ sở kinh doanh
        $ability_own_cskd = Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::ORG_OWNE,
            'group' => 'Quản lý cơ sở kinh doanh',
            'title' => 'Toàn quyền (chủ cơ sở)',
            'entity_type' => '*',
            'scope' => OrgType::CSKD,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::ORG_EXCHANGE,
            'group' => 'Quản lý cơ sở kinh doanh',
            'title' => 'Các chức năng giao dịch',
            'entity_type' => '*',
            'scope' => OrgType::CSKD,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::ORG_MANAGE,
            'group' => 'Quản lý cơ sở kinh doanh',
            'title' => 'Các chức năng quản lý',
            'entity_type' => '*',
            'scope' => OrgType::CSKD,
        ]);
        Bouncer::ability()->firstOrCreate([
            'name' => AbilityName::ORG_INFO,
            'group' => 'Quản lý cơ sở kinh doanh',
            'title' => 'Thông tin của cơ sở',
            'entity_type' => '*',
            'scope' => OrgType::CSKD,
        ]);

        // =================== TẠO USER ADMIN VÀ GÁN QUYỀN  =======================

        //Tạo người dùng quản trị viên của hệ thống
        $user_admin = User::whereEmail('admin@email.com')->first();
        if ($user_admin === null) {
            $user_admin = User::create([
                'email' => 'admin@email.com',
                'name' => 'Quản trị viên',
                'password' => bcrypt('Qu4Tuy3tV01!'),
                'active' => 1,
                'organization_id' => 1,
            ]);
        } else {
            $user_admin->update([
                'name' => 'Quản trị viên',
                'password' => bcrypt('Qu4Tuy3tV01!'),
                'active' => 1,
                'organization_id' => 1,
            ]);
        }

        // =================== QUYỀN CHO CÁC NHÓM MẶC ĐỊNH ==================

        Bouncer::scope()->to(OrgType::ADMIN);
        Bouncer::allow($role_admin)->everything();
        //Gán quyền cho user admin
        Bouncer::assign('admin')->to($user_admin);

        Bouncer::scope()->to(OrgType::VPDP);
        Bouncer::allow($role_ubnd_tinh)->everything();

        /*Bouncer::scope()->to(OrgType::SYT);
        Bouncer::allow($role_so_yte)->everything();

        Bouncer::scope()->to(OrgType::SNN);
        Bouncer::allow($role_so_nn_ptnt)->everything();

        Bouncer::scope()->to(OrgType::SCT);
        Bouncer::allow($role_so_ct)->everything();

        Bouncer::scope()->to(OrgType::HUYEN);
        Bouncer::allow($role_huyen)->everything();

        Bouncer::scope()->to(OrgType::XA);
        Bouncer::allow($role_xa)->everything();*/

        //Gán quyền cho chủ cơ sở sản xuất
        Bouncer::scope()->to(OrgType::CSSX);
        Bouncer::allow($role_own_cssx)->to($ability_own_cssx);

        //Gán quyền cho chủ cơ sở kinh doanh
        Bouncer::scope()->to(OrgType::CSKD);
        Bouncer::allow($role_own_cskd)->to($ability_own_cskd);

    }
}
