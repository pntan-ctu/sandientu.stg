<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamp("order_date")->nullable();
            $table->text("comment")->nullable();
            $table->integer("organization_id")->nullable();
            $table->integer("user_id");
            $table->string("fullname",255);
            $table->string("address",255);
            $table->integer("region_id");
            $table->string("email",255)->nullable();
            $table->string("tel",30)->nullable();
            $table->decimal('totalmoney', 15, 2);
            $table->smallInteger("status");
            $table->string("ship_type",255)->nullable();
            $table->string("pay_type",255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
