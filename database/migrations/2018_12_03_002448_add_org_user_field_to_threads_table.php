<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Cmgmyr\Messenger\Models\Models;

class AddOrgUserFieldToThreadsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table(Models::table('threads'), function (Blueprint $table) {
            $table->integer('organization_id');
            $table->integer('user_id');
            $table->index(['organization_id'], 'idx_organization_id');
            $table->index(['user_id'], 'idx_user_id');
            $table->index(['organization_id','user_id'], 'idx_organization_user');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table(Models::table('threads'), function (Blueprint $table) {
            $table->dropIndex('idx_organization_id');
            $table->dropIndex('idx_user_id');
            $table->dropIndex('idx_organization_user');
            $table->removeColumn('organization_id');
            $table->removeColumn('user_id');
        });
    }
}
