<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('video_groups', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 255);
            $table->string('path', 255);
            $table->text('description')->nullable();
            $table->integer('create_by');
            $table->addColumn('tinyInteger', 'no', ['lenght' => 1, 'default' => '0']);
            $table->addColumn('tinyInteger', 'active', ['lenght' => 1, 'default' => '1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('video_groups');
    }
}
