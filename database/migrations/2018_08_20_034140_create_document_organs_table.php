<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentOrgansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_organs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("name",255);
            $table->addColumn('tinyInteger', 'no', ['lenght' => 1, 'default' => '0']);
            $table->addColumn('tinyInteger', 'active', ['lenght' => 1, 'default' => '1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_organs');
    }
}
