<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsArticlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_articles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('group_id');
            $table->string('title', 500);
            $table->string('path', 500);
            $table->text('summary');
            $table->longtext('content');
            $table->string('img_path');
            $table->integer('view_count');
            $table->integer('hotnews');
            $table->integer('comment');
            $table->integer('shared');
            $table->string('tags',255);
            $table->string('relate_news',255);
            $table->integer('status');
            $table->integer('create_by');
            $table->integer('edit_by');
            $table->integer('publish_by');
            $table->timestamp('publish_at')->nullable();
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
            $table->integer('delete_by');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('news_articles');
    }
}
