<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHorMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hor_menus', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("parent_id");
            $table->string("title",255);
            $table->integer("mtype");
            $table->integer("relate_id");
            $table->string("link",300);
            $table->addColumn('tinyInteger', 'no', ['lenght' => 1, 'default' => '0']);
            $table->addColumn('tinyInteger', 'active', ['lenght' => 1, 'default' => '1']);
            $table->addColumn('tinyInteger', 'new_tab', ['lenght' => 1, 'default' => '0']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hor_menus');
    }
}
