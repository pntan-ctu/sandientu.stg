<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileMetadatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file_metadatas', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string("path",190);
            $table->string("file_name",190);
            $table->string("type", 64);
            $table->integer("size", 10);
            $table->bigInteger("upload_by");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file_metadatas');
    }
}
