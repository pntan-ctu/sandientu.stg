<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('documents', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("document_field_id");
            $table->integer("document_type_id");
            $table->integer("document_organ_id");
            $table->string("number_symbol",512);
            $table->string("path",512);
            $table->string("document_path",255);
            $table->timestamp("illegal_date");
            $table->string("quote",255);
            $table->string("sign_by",255);
            $table->string("duty",255);
            $table->string("description",255);
            $table->timestamp("sign_date")->nullable();
            $table->timestamp("enable_date")->nullable();
            $table->tinyInteger("status");
            $table->integer("view_count");
            $table->timestamps();
            $table->timestamp('deleted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('documents');
    }
}
