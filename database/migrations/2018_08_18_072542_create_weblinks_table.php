<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWeblinksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weblinks', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title', 255);
            $table->string('url', 255);
            $table->string('img_path', 255)->nullable();
            $table->integer('no');
            $table->integer('create_by');
            $table->addColumn('tinyInteger', 'active', ['lenght' => 1, 'default' => '1']);
            $table->addColumn('tinyInteger', 'new_window', ['lenght' => 1, 'default' => '1']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weblinks');
    }
}
