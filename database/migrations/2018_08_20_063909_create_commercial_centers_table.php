<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommercialCentersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('commercial_centers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("region_id");
            $table->string("name",512);
            $table->string("path",512);
            $table->string("address",512);
            $table->text("introduction");
            $table->string("map",100);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('commercial_centers');
    }
}
