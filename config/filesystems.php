<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Default Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Here you may specify the default filesystem disk that should be used
    | by the framework. The "local" disk, as well as a variety of cloud
    | based disks are available to your application. Just store away!
    |
    */

    'default' => env('DISK_STORE_FILE', 'local'),

    /*
    |--------------------------------------------------------------------------
    | Default Cloud Filesystem Disk
    |--------------------------------------------------------------------------
    |
    | Many applications store files both locally and in the cloud. For this
    | reason, you may specify a default "cloud" driver here. This driver
    | will be bound as the Cloud disk implementation in the container.
    |
    */

    'cloud' => env('FILESYSTEM_CLOUD', 's3'),

    /*
    |--------------------------------------------------------------------------
    | Filesystem Disks
    |--------------------------------------------------------------------------
    |
    | Here you may configure as many filesystem "disks" as you wish, and you
    | may even configure multiple disks of the same driver. Defaults have
    | been setup for each driver as an example of the required options.
    |
    | Supported Drivers: "local", "ftp", "sftp", "s3", "rackspace"
    |
    */

    'disks' => [

        'local' => [
            'driver' => 'local',
            'root' => storage_path('app/uploadfiles'),
        ],

        'public' => [
            'driver' => 'local',
            'root' => storage_path('app/public'),
            'url' => env('APP_URL').'/storage',
            'visibility' => 'public',
        ],

        's3' => [
            'driver' => 's3',
            'key' => env('AWS_ACCESS_KEY_ID'),
            'secret' => env('AWS_SECRET_ACCESS_KEY'),
            'region' => env('AWS_DEFAULT_REGION'),
            'bucket' => env('AWS_BUCKET'),
            'endpoint' => env('AWS_URL'),
            'use_path_style_endpoint' => true,
            'cache' => [
                'store' => env('AWS_CACHE_STORE', 'file'),
                'expire' => 600,
                'prefix' => env(
                    'AWS_CACHE_PREFIX',
                    str_slug(env('APP_NAME', 'laravel'), '_').'_aws_cache'
                ),
            ],
        ]

    ],

    /*
    |--------------------------------------------------------------------------
    | Giới hạn dung lượng file cho phép upload
    |--------------------------------------------------------------------------
    | disk_thumb: Chọn nơi lưu trữ file ảnh được thumbnail
    | max_size_file : Giới hạn cho các file dữ liệu khác ảnh, dùng ở UploadFileUtil
    | max_size_file_image : Giới hạn cho các file ảnh, dùng ở UploadFileUtil
    | max_width_image : Giới hạn kích thước ảnh, dùng ở UploadFileUtil
    | max_height_image : Giới hạn kích thước ảnh, dùng ở UploadFileUtil
    | size_thumb_images : Giới hạn các kích cỡ thumbnail, dùng ở ThumbnailImageUtil                    
    |
    */
    'disk_thumb' => env('DISK_THUMB_IMAGE', 'public'),
    'max_size_file_image' => env('MAX_SIZE_FILE_IMAGE', 5000000),
    'max_size_file' => env('MAX_SIZE_FILE', 20000000),
    'max_width_image' => env('MAX_WIDTH_IMAGE', 1366),
    'max_height_image' => env('MAX_HEIGHT_IMAGE', 768),
    'size_thumb_images' => [0, 25, 50, 75, 100, 150, 200, 250, 300, 350, 400, 450, 500, 600, 700, 800, 900, 1000, 1200],

];
