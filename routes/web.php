<?php

/*
|--------------------------------------------------------------------------|
| Test Routes - Phần dùng để test, sẽ xóa khi deploy
|--------------------------------------------------------------------------|
*/
Route::prefix('test')->group(function () {
    Route::get('/', 'TestController@index')->name('test');
    Route::post('/one', 'TestController@one')->name('test-one');
    Route::post('/multi', 'TestController@multi')->name('test-multi');
    Route::get('/download', 'TestController@download')->name('test-download');
    Route::get('/test', 'TestController@test');
    Route::get('/test-payment/{name}/{amount}', 'TestController@testPayment');
    Route::get("/exportExcel/{type}", "TestController@export");
});

/*
  |--------------------------------------------------------------------------
  | Public Web Routes - Các request không yêu cầu phải đăng nhập được viết ở đây
  |--------------------------------------------------------------------------
  | Các request cho tất cả người truy cập
  |
 */
  Route::get('/rd','rd@getData' );
  Route::get('/testme', function () {
    return view('test1');
});
//Trang chủ web
Route::get('/', 'HomeController@web')->name('home');
Route::get('/home', 'HomeController@web');

//Xác thực người dùng
Auth::routes(['verify' => true]);

//Xác thực qua MXH
Route::get('/redirect/{social}', 'SocialAuthController@redirect');
Route::get('/callback/{social}', 'SocialAuthController@callback');

//Chức năng tìm kiếm
//Route::get('/search/{text}/{type}/{choose}', 'HomeController@search')->name('search');
Route::get('/search', 'HomeController@search')->name('search');
Route::post('/load-page-search', 'HomeController@load')->name('load-page-search');
Route::get('/find', 'HomeController@searchAuto');

//Sản phẩm
Route::resource('danh-muc-san-pham', 'Web\ProductCategoryWebController');
Route::resource('san-pham', 'Web\ProductsWebController');

//Cơ sở SXKD
Route::resource('shop', 'Web\OrganizationsWebController');

//Vùng sản xuất
Route::resource('vung-san-xuat', 'Web\AreaWebController');

//Địa điểm kinh doanh: Chợ, siêu thị, TTTM
Route::resource('dia-diem-kinh-doanh', 'Web\CommercialCenterWebController');

//Tin tức
Route::resource('danh-muc-tin-tuc', 'Web\NewsGroupsWebController');
Route::resource('tin-tuc', 'Web\NewsWebController');
Route::resource('news-comments', 'Web\NewsCommentsWebController');

//Tin kết nối cung cầu
Route::get('ket-noi', 'Web\AdsWebController@connections')->name('ket-noi');
Route::get('/ket-noi/{path}', 'Web\AdsWebController@showAds'); 

//Liên hệ, phản hồi thông tin
Route::resource('lien-he', 'Web\ContactController');
//Route::get('/ket-noi/{typeAds}', 'Web\AdsWebController@showTypeAds')->name('ket-noi.type');
//Route::get('lien-he', 'Web\ContactController@index');
//Route::post('lien-he', 'Web\ContactController@search');

//Văn bản
Route::get('van-ban-qppl/data', 'Web\DocumentController@indexData')->name('van-ban-qppl.indexData');
Route::get('van-ban-qppl', 'Web\DocumentController@index')->name('van-ban-qppl.index');
Route::get('van-ban-qppl/{path}', 'Web\DocumentController@show')->name('van-ban-qppl.show');

//Bản đồ phân bố
Route::resource('map', 'Web\MapsController');
//Route::get('map/search', 'Web\MapsController@search')->name('search');

//Thư viện video
Route::get('video', 'Web\VideoWebController@index');
Route::get('/video/{path}', 'Web\VideoWebController@showVideo');

//Thu viện ảnh
Route::get('/thu-vien-anh/', 'Web\ImagesWebController@index');
Route::get('/thu-vien-anh/{path}', 'Web\ImagesWebController@showImage');

//Trợ giúp, hỏi đáp
Route::get('/tro-giup', 'Web\HelpWebController@index')->name('tro-giup');
Route::post('add-question', 'Web\HelpWebController@addHelps');
Route::get('/tro-giup/{path}', 'Web\HelpWebController@show');

//Bài viết thông tin giới thiệu cho footer
Route::get('/about/{path}', 'Web\NewsWebController@about');

// Hoạt động của người dùng
Route::get('/user-activity/{username}', 'Web\UserActivityController@index')->name('user-activity');

//QRcode
Route::get('get-app-qrcode', 'Web\QRCodeController@index');
Route::get('get-app', 'Web\QRCodeController@show');

//Get location
Route::get('toa-do', 'Web\GetLocationController@index')->name('toa-do');

/*
  |--------------------------------------------------------------------------
  | Auth Web Routes - Các request yêu cầu phải đăng nhập được viết ở đây
  |--------------------------------------------------------------------------
  | VD: các request cho thành viên, vd: cập nhật thông tin cá nhân, đặt hàng...
  |
 */
Route::post('add-cart', 'Web\ShoppingCartController@addItemsToCart')->name('add-cart');
Route::get('get-cart', 'Web\ShoppingCartController@getCart')->name('get-cart');
Route::get('get-momo-notify', 'Web\ShoppingCartController@getMomonotify')->name('get-momo-notify');
Route::get('get-product-cart', 'Web\ShoppingCartController@getProductsCart')->name('get-product-cart');
Route::middleware(['auth'])->group(function () {
    //Chuyển đổi vai trò Cá nhân và Cơ sở
    Route::post('switch-role-access', 'HomeController@switchRoleAccess')->name('switch-role-access');
    //Đặt hàng
    Route::post('remove-cart-item', 'Web\ShoppingCartController@removeCartItem')->name('remove-cart-item');
    Route::get('mua-hang', 'Web\ShoppingCartController@purchase')->name('mua-hang');
    Route::post('dat-hang', 'Web\ShoppingCartController@addOrder')->name('dat-hang');
    Route::post('edit-amount-cart', 'Web\ShoppingCartController@editAmountCart')->name('edit-amount-cart');
    Route::post('add-order-organi', 'Web\ShoppingCartController@addOrderOrgani');
    Route::get('thong-bao-dat-hang', 'Web\ShoppingCartController@result')->name('thong-bao-dat-hang');
    //Đánh giá sản phẩm
    Route::post('add-rate', 'Web\ProductsWebController@addRate');
    //Đánh giá cơ sở sản xuất kinh doanh
    Route::post('add-rate-organ', 'Web\OrganizationsWebController@addRateOrgan');
    //Hỏi đáp sản phẩm
    Route::resource('qa-product', 'Web\ProductQAController');
    //Hỏi đáp cơ sở sản xuất
    Route::resource('qa-organization', 'Web\OrganQAController');
    //Thêm sản phẩm yêu thích
    Route::post('add-like-product', 'Web\ProductsWebController@addLikeProduct');
    //Thêm cơ sở sxkd yêu thích
    Route::post('add-like-organization', 'Web\OrganizationsWebController@addLikeOrgani');
    //Thêm theo dõi cơ sở sxkd
    Route::post('add-follow', 'Web\OrganizationsWebController@addFollow');
    //Báo cáo bi phạm
    Route::post('them-vi-pham', 'Web\InfringementCategoryWebController@themVipham');
    //Gửi yêu cầu liên hệ
    Route::post('send-request-product', 'Web\RequestProductsController@sendRequestProduct');
    //Chặn người dùng mua hàng của cơ sở
    Route::post('block-user', 'Web\OrganizationsWebController@blockUser');
    //Bỏ chặn người dùng mua hàng của cơ sở
    Route::post('/unblock-user', 'Web\OrganizationsWebController@unblockUser');

    
    //================= PROFLE USER ==========
    Route::prefix('user')->group(function () {
        //Trang chính của user (profile hoặc tin nhắn): Message: Web\UserMessageController@index
        Route::get('/', 'Web\OrderController@index')->name('profile');

        //Sửa thông cá nhân
        Route::get('/profile', 'Web\ProfileController@index')->name('user.profile');
        Route::post('profile/change-avatar', 'Web\ProfileController@changeAvatar');
        Route::post('profile/update', 'Web\ProfileController@update');
        Route::post('profile/change-password', 'Web\ProfileController@changePassword');
        Route::get('profile/district/{id}', 'Web\ProfileController@getDistrict');
        Route::get('profile/commune/{id}', 'Web\ProfileController@getCommune');

        //Lịch sử mua hàng
        Route::post('orders/getOrder', 'Web\OrderController@getOrder')->name('orders.getOrder');
        Route::get('orders/showOrder/{id}', 'Web\OrderController@showOrder')->name('orders.showOrder');
        Route::post('orders/actOrder', 'Web\OrderController@actOrder')->name('orders.actOrder');
        Route::post('orders/huyOrder', 'Web\OrderController@huyOrder')->name('orders.huyOrder');
        Route::post('orders/addComment', 'Web\OrderController@addComment')->name('orders.addComment');
        Route::get('orders/data', 'Web\OrderController@indexData')->name('orders.indexData');
        Route::resource('orders', 'Web\OrderController');

        //Đăng ký doanh nghiệp
        Route::get('/register-business', 'Web\RegisterBusinessController@index')->name('user.register-business-index');
        Route::post('/register-business/get-type/', 'Web\RegisterBusinessController@getType')->name('user.get-org-type');
        Route::post('/register-business', 'Web\RegisterBusinessController@register')->name('user.register-business');
        
        //Đăng tin rao vặt
        Route::get('advertising/data', 'Web\AdsController@indexData')->name('advertising.indexData');
        Route::get('advertising/product_link', 'Web\AdsController@getProductLink')->name('advertising.getProductLink');
        Route::resource('advertising', 'Web\AdsController');
        //Route::get('createAds', 'Web\AdsController@createAds')->name('user.createAds');
        //Route::post('updateAds', 'Web\AdsController@updateAds')->name('user.updateAds');

        //Đăng tin việc làm - du lục và đầu tư
         Route::resource('dangtuyendung', 'it5Controllers\vieclamController');
         Route::get('dangtuyendung/create', 'it5Controllers\vieclamController@create')->name('dangtintuyendung.create');



        //Sản phẩm, cơ sở yêu thích
        Route::get('favorite/data', 'Web\FavoriteController@indexData')->name('favorite.indexData');
        Route::resource('favorite', 'Web\FavoriteController');
        
        //Theo dõi cơ sở
        Route::get('follow/data', 'Web\FollowController@indexData')->name('follow.indexData');
        Route::resource('follow', 'Web\FollowController');

        // Tin nhắn
        Route::get('messages', 'Web\UserMessageController@index')->name('user.messages');
        Route::get('getMessage/{id}', 'Web\UserMessageController@getMessage');
        Route::get('markAsReadMessage/{id}', 'Web\UserMessageController@markAsReadMessage');
        Route::post('replyMessage', 'Web\UserMessageController@replyMessage');
        Route::post('sendMessageToOrganization', 'Web\UserMessageController@sendMessageToOrganization');
    });
    Route::prefix('messenger')->group(function () {
        Route::get('/{org?}', 'Web\MessengerController@index')->name('messenger')->where('org', '[0-9]+');
//        Route::get('create', ['as' => 'messages.create', 'uses' => 'Web\MessengerController@create']);
        Route::get('/threads', 'Web\MessengerController@fetchThreads')->name('messenger.threads');
        Route::get('/threads/{thread}/messages', 'Web\MessengerController@fetchMessages')
            ->name('messenger.threads.messages');
        Route::post('/threads/{thread}/messages', 'Web\MessengerController@store')
            ->name('messenger.threads.messages.store');
        Route::get('/threads/{org?}', 'Web\MessengerController@fetchThreads')->name('messages.threads')
            ->where('org', '[0-9]+');
    });

    Route::get('get-regions/{parent_id}', 'Web\ShoppingCartController@getRegions');
});