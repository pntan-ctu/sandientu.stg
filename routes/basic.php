<?php

//Common view image
Route::get('/image/{width}/{height}/{path}', 'UtilController@viewImage')->name('image'); //View theo path
Route::get('/image-id/{width}/{height}/{id}', 'UtilController@viewImageId')->name('image-id'); //View theo id file_metadata

//Common dowload file
Route::get('/download/{path}', 'UtilController@download')->name('download'); //Download theo path
Route::get('/download-id/{id}', 'UtilController@downloadById')->name('download-id'); //Download theo id file_metadata

Route::middleware(['auth'])->group(function () {
    //Common upload image, file
    Route::prefix('upload')->group(function () {
        Route::post('/image', 'UtilController@uploadImage')->name('upload-image');
        Route::post('/images', 'UtilController@multiUploadImage')->name('upload-images');
        Route::post('/file', 'UtilController@uploadFile')->name('upload-file');
        Route::post('/files', 'UtilController@multiUploadFile')->name('upload-files');
        //Upload for FCKEditor
        Route::post('/image-cke', 'UtilController@uploadImageCKEditor')->name('upload-image-cke');
        Route::post('/file-cke', 'UtilController@uploadFileCKEditor')->name('upload-file-cke');
    });
});
