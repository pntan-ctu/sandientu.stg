<?php

/*
  |--------------------------------------------------------------------------|
  | Manage Routes - Phần quản trị dành cho cơ quan chính quyền
  |--------------------------------------------------------------------------|
  |
 */
Route::prefix('manage')->group(function () {

    Route::get('/', 'Manage\HomeController@manage')->name('manage');
    Route::get('/home', 'Manage\HomeController@manage');

    Route::post('product-category/data_grid', 'Manage\ProductCategoryController@dataGrid')->name('product-category.data_grid');
    Route::get('product-category/data', 'Manage\ProductCategoryController@indexData')->name('product-category.indexData');
    Route::resource('product-category', 'Manage\ProductCategoryController');

    Route::get('users/data', 'Manage\UserController@indexData')->name('users.indexData');
    Route::get('users/reset-password/{user}', 'Manage\UserController@resetPassword');
    Route::resource('users', 'Manage\UserController');
    Route::get('users/getRoles/{orgId}', 'Manage\UserController@getRoles');

    Route::get('album-group/data', 'Manage\AlbumGroupsController@indexData')->name('album-group.indexData');
    Route::resource('album-group', 'Manage\AlbumGroupsController');

    Route::get('albums/data', 'Manage\AlbumsController@indexData')->name('albums.indexData');
    Route::resource('/albums', 'Manage\AlbumsController');

    Route::post('/multiuploadsimages', 'Manage\AlbumsController@uploadSubmit');
    Route::get('/getImagesAlbum/{albumId}', 'Manage\AlbumsController@getImagesAlbum');
    Route::resource('/album-detail', 'Manage\AlbumDetailController');

    Route::get('help/data', 'Manage\HelpController@indexData')->name('help.indexData');
    Route::resource('help', 'Manage\HelpController');

    Route::get('contact/data', 'Manage\ContactController@indexData')->name('contact.indexData');
    Route::resource('contact', 'Manage\ContactController');
    Route::get('contact/show/{id}', 'Manage\ContactController@showcontact');
    Route::get('contact/convert/{id}', 'Manage\ContactController@convert');

    Route::get('weblink/data', 'Manage\WeblinkController@indexData')->name('weblink.indexData');
    Route::resource('weblink', 'Manage\WeblinkController');

    Route::get('poll/data', 'Manage\PollController@indexData')->name('poll.indexData');
    Route::resource('poll', 'Manage\PollController');
    Route::get('/getPollAnswer/{pollId}', 'Manage\PollController@getPollAnswer');
    Route::resource('/pollAnswer', 'Manage\PollAnswerController');

    Route::get('slideshow/data', 'Manage\SlideShowController@indexData')->name('slideshow.indexData');
    Route::resource('slideshow', 'Manage\SlideShowController');

    Route::get('video-group/data', 'Manage\VideoGroupController@indexData')->name('video-group.indexData');
    Route::resource('video-group', 'Manage\VideoGroupController');

    Route::get('video/data', 'Manage\VideoController@indexData')->name('video.indexData');
    Route::resource('video', 'Manage\VideoController');
    
    Route::get('rating/data', 'Manage\RatingController@indexData')->name('rating.indexData');
    Route::resource('rating', 'Manage\RatingController');
    Route::post('rating/checked', 'Manage\RatingController@updateChecked')->name('roles.checked');
    
    Route::get('question-answer/data', 'Manage\QuestionAnswerController@indexData')->name('question-answer.indexData');
    Route::resource('question-answer', 'Manage\QuestionAnswerController');
    Route::post('question-answer/checked', 'Manage\QuestionAnswerController@updateChecked')->name('roles.checked');
    
    Route::get('hor-menu/data', 'Manage\HorMenuController@indexData')->name('horMenu.indexData');
    Route::resource('hor-menu', 'Manage\HorMenuController');

    Route::get('area/data', 'Manage\AreaController@indexData')->name('area.indexData');
    Route::resource('area', 'Manage\AreaController');
    
    Route::get('commercial-center/data', 'Manage\CommercialCenterController@indexData')->name('commercial-center.indexData');
    Route::resource('commercial-center', 'Manage\CommercialCenterController');
    
    Route::resource('report-verify-org', 'Manage\ReportVerifyOrgController');
    Route::get('report-verify-org', 'Manage\ReportVerifyOrgController@index');
    
    Route::get('information', 'Manage\InformationController@getInfo');
    Route::post('information', 'Manage\InformationController@postInfo');

    Route::get('news-group/data', 'Manage\NewsGroupController@indexData')->name('news-group.indexData');
    Route::get('news-group/delete/{id}', 'Manage\NewsGroupController@destroy')->name('news-group.delete');
    Route::get('news-group/getNextNo/{parent_id}', 'Manage\NewsGroupController@getNextNo');
    Route::resource('news-group', 'Manage\NewsGroupController');

    Route::get('news/data', 'Manage\NewsController@indexData')->name('news.indexData');
    Route::get('news/dataRelate', 'Manage\NewsController@relateData')->name('news.relateData');
    Route::get('news/edit/{id}', 'Manage\NewsController@edit')->name('news.edit');
    Route::get('news/delete/{id}', 'Manage\NewsController@destroy')->name('news.delete');
    Route::post('news/update-relate-news', 'Manage\NewsController@updateRelateNews')->name('news.update-relate-news');
    Route::get('news/get-relate-news', 'Manage\NewsController@getRelateNews')->name('news.get-relate-news');
    Route::get('news/get-relate-news/data', 'Manage\NewsController@getRelateNews');
    Route::resource('news', 'Manage\NewsController');

    Route::get('sms-history/data', 'Manage\SmsHistoryController@indexData')->name('sms-history.indexData');
    Route::resource('sms-history', 'Manage\SmsHistoryController');

    Route::resource('sms', 'Manage\SmsController');

    Route::get('certificates-category/data', 'Manage\CertificateCategoriesController@indexData')->name('certificates-category.indexData');
    Route::resource('certificates-category', 'Manage\CertificateCategoriesController');

    Route::get('docfield/data', 'Manage\DocumentFieldsController@indexData')->name('docfield.indexData');
    Route::resource('docfield', 'Manage\DocumentFieldsController');

    Route::get('docorgan/data', 'Manage\DocumentOrgansController@indexData')->name('docorgan.indexData');
    Route::resource('docorgan', 'Manage\DocumentOrgansController');

    Route::get('doctype/data', 'Manage\DocumentTypesController@indexData')->name('doctype.indexData');
    Route::resource('doctype', 'Manage\DocumentTypesController');

    Route::get('document/data', 'Manage\DocumentController@indexData')->name('document.indexData');
    Route::resource('document', 'Manage\DocumentController');

    Route::get('map/data', 'Manage\MapController@indexData')->name('doctype.indexData');
    Route::resource('map', 'Manage\MapController');

    Route::get('roles/data', 'Manage\RoleController@indexData')->name('roles.indexData');
    Route::get('roles/get-modules', 'Manage\RoleController@getModules')->name('roles.get-modules');
    Route::get('roles/get-modules/data', 'Manage\RoleController@getModules');
    Route::post('roles/update-module-roles', 'Manage\RoleController@updateModuleRoles')->name('roles.update-module-roles');
    Route::resource('roles', 'Manage\RoleController');

    Route::prefix('/orgtype/{organizationTypeId}')->group(function () {
        Route::get('organization/data', 'Manage\OrganizationController@indexData')->name('organization.indexData');
        Route::resource('organization', 'Manage\OrganizationController');
    });
    Route::post('organization/move', 'Manage\OrganizationController@move');
    Route::post('organization/cancelMove', 'Manage\OrganizationController@cancelMove');
    Route::post('organization/getOrgInfo', 'Manage\OrganizationController@getOrgInfo');
    Route::post('organization/resolveMove', 'Manage\OrganizationController@resolveMove');

    Route::get('news-comments/data', 'Manage\NewsCommentController@indexData')->name('news-comments.indexData');
    Route::get('news-comments/approve/{id}', 'Manage\NewsCommentController@setActive');
    Route::get('news-comments/disapprove/{id}', 'Manage\NewsCommentController@setInactive');
    Route::resource('news-comments', 'Manage\NewsCommentController');

    Route::get('menu/data', 'Manage\MenuController@indexData')->name('menu.indexData');
    Route::get('menu/getTreeData/{org_type_id}', 'Manage\MenuController@getTreeMenuData');
    Route::get('menu/getCboMenuData/{org_type_id}', 'Manage\MenuController@getCboMenuData');
    Route::get('menu/getNextNo/{parent_id}', 'Manage\MenuController@getNextNo');
    Route::resource('menu', 'Manage\MenuController');

    Route::get('advertisings/data', 'Manage\AdvertisingController@indexData')->name('advertisings.indexData');
    Route::get('advertisings/approve/{id}', 'Manage\AdvertisingController@setActive');
    Route::get('advertisings/disapprove/{id}', 'Manage\AdvertisingController@setInactive');
    Route::resource('advertisings', 'Manage\AdvertisingController');

    Route::get('infringements/data', 'Manage\InfringementController@indexData');
    Route::get('infringements/getData/{type}', 'Manage\InfringementController@getData');
    Route::get('infringements/getData/{type}/data', 'Manage\InfringementController@getData');
    Route::get('infringements/perform/{id}_{status}', 'Manage\InfringementController@perform');
    Route::resource('infringements', 'Manage\InfringementController');

    Route::get('report-rank-org', 'Manage\ReportRankOrgController@index');
    Route::get('report-rank-org-view', 'Manage\ReportRankOrgController@statistic');

    Route::get('report-rank-product', 'Manage\ReportRankProductController@index');
    Route::get('report-rank-product-view', 'Manage\ReportRankProductController@statistic');

    Route::get('report-org-status', 'Manage\ReportOrgController@indexStatus');
    Route::get('report-org-status-view', 'Manage\ReportOrgController@statisticStatus');
    
    Route::get('report-infringements-org-product', 'Manage\ReportInfringementsOrgProductController@index');
    Route::get('report-infringements-org-product-view', 'Manage\ReportInfringementsOrgProductController@reportType');
});
