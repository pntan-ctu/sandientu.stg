<?php

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
//
//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});get
Route::post('login', 'API\UserController@login');
Route::post('register', 'API\UserController@register');
route::get('search', 'API\HomeController@search');
route::get('get-homepage-data', 'API\HomeController@homePageData');
route::get('get-product-info-detail/{path}', 'API\ProductController@getProductInfoDetail');
route::get('get-organization-info-detail/{path}', 'API\OrganizationController@getOrganizationInfoDetail');
route::get('get-ads-detail/{path}', 'API\HomeController@getAdsDetail');
Route::post('register', 'API\UserController@register');
Route::post('send-contact', 'API\HomeController@sendContact');
route::get('get-list-areas', 'API\HomeController@getListAreas');
route::get('get-area-info/{path}', 'API\HomeController@getAreaInfo');
route::get('danh-muc-san-pham', 'API\HomeController@getProducts');
route::get('danh-muc-san-pham/{path}', 'API\HomeController@getProductInGroup');
route::get('dia-diem-kinh-doanh', 'API\HomeController@getCommercialCenters');
route::get('dia-diem-kinh-doanh/{path}', 'API\HomeController@getCommercialCenterInfo');
route::get('danh-muc-tin-tuc', 'API\HomeController@getNews');
route::get('tin-tuc/{path}', 'API\HomeController@getNewsDetail');
route::get('van-ban-qppl', 'API\HomeController@searchDocuments');
route::get('van-ban-qppl/{path}', 'API\HomeController@getDocumentDetail');
route::get('tro-giup', 'API\HomeController@getHelpData');
route::get('tro-giup/{path}', 'API\HomeController@getHelpDetail');

Route::group(['middleware' => 'auth:api'], function () {
    Route::post('details', 'API\UserController@details');
    Route::post('logout', 'API\UserController@logout');

    Route::prefix('organization/{organizationId}')->group(function () {

        /*************************************Product Routes*******************************/
        Route::resource("products", "API\ProductController")->names([
            "index"=>"api.products.index",
            "create"=>"api.products.create",
            "store"=>"api.products.store",
            "show"=>"api.products.show",
            "edit"=>"api.products.edit",
            "update"=>"api.products.update",
            "destroy"=>"api.products.destroy"
        ]);
        //update info detail
        Route::post("products/{product_id}/info", "API\ProductController@updateInfoDetail");
        Route::get("products/{product_id}/info", "API\ProductController@getInfoDetail");
        // manage cerfiticate
        Route::resource("products/{product_id}/cer", "API\ProductCertificateController")->names([
            "index"=>"api.ProductCertificate.index",
            "create"=>"api.ProductCertificate.create",
            "store"=>"api.ProductCertificate.store",
            "show"=>"api.ProductCertificate.show",
            "edit"=>"api.ProductCertificate.edit",
            "update"=>"api.ProductCertificate.update",
            "destroy"=>"api.ProductCertificate.destroy"
        ]);
        /***********************************End Product Routes******************************/

        /*************************************Orders Routes*********************************/
        Route::resource("orders", "API\OrderController")->names([
            "index"=>"api.orders.index",
            "create"=>"api.orders.create",
            "store"=>"api.orders.store",
            "show"=>"api.orders.show",
            "edit"=>"api.orders.edit",
            "update"=>"api.orders.update",
            "destroy"=>"api.orders.destroy"
        ]);
        Route::get("orders_new/", "API\OrderController@getCountNewOrders");
        Route::post('orders/actOrder', 'API\OrderController@actOrder')->name('api.orders.actOrder');

        Route::resource("buys", "API\BuyController")->names([
            "index"=>"api.buys.index",
            "create"=>"api.buys.create",
            "store"=>"api.buys.store",
            "show"=>"api.buys.show",
            "edit"=>"api.buys.edit",
            "update"=>"api.buys.update",
            "destroy"=>"api.buys.destroy"
        ]);
        Route::post('buys/confirmReceived/{order_id}', 'API\BuyController@confirmReceived')->name('api.buys.confirmReceived');

        /*************************************Users Routes*********************************/
        Route::resource("users", "API\UserController")->names([
            "index"=>"api.users.index",
            "create"=>"api.users.create",
            "store"=>"api.users.store",
            "show"=>"api.users.show",
            "edit"=>"api.users.edit",
            "update"=>"api.users.update",
            "destroy"=>"api.users.destroy"
        ]);
        Route::get("users/{user}/resetpass","API\UserController@resetpass");

        /*************************************Advertisings Routes*********************************/
        Route::resource("advertisings","API\AdvertisingController")->names([
            "index"=>"api.advertisings.index",
            "create"=>"api.advertisings.create",
            "store"=>"api.advertisings.store",
            "show"=>"api.advertisings.show",
            "edit"=>"api.advertisings.edit",
            "update"=>"api.advertisings.update",
            "destroy"=>"api.advertisings.destroy"]);

        /*************** general information ***************/
        Route::get('getGeneralInfo', 'API\OrganizationController@getGeneralInfo');
        Route::post('saveGeneralInfo', 'API\OrganizationController@saveGeneralInfo');

        /*************** introduction information ***************/
        Route::get('getIntroductionInfo', 'API\OrganizationController@getIntroductionInfo');
        Route::post('saveIntroductionInfo', 'API\OrganizationController@saveIntroductionInfo');

        Route::resource("certificates", "API\CertificateController")->names([
            "index"=>"api.certificates.index",
            "create"=>"api.certificates.create",
            "store"=>"api.certificates.store",
            "show"=>"api.certificates.show",
            "edit"=>"api.certificates.edit",
            "update"=>"api.certificates.update",
            "destroy"=>"api.certificates.destroy"
        ]);

        Route::resource("branches", "API\BranchController")->names([
            "index"=>"api.branches.index",
            "create"=>"api.branches.create",
            "store"=>"api.branches.store",
            "show"=>"api.branches.show",
            "edit"=>"api.branches.edit",
            "update"=>"api.branches.update",
            "destroy"=>"api.branches.destroy"
        ]);

        Route::resource("{type}/partners", "API\PartnerController")->names([
            "index"=>"api.partners.index",
            "create"=>"api.partners.create",
            "store"=>"api.partners.store",
            "show"=>"api.partners.show",
            "edit"=>"api.partners.edit",
            "update"=>"api.partners.update",
            "destroy"=>"api.partners.destroy"
        ]);
        Route::get('{type}/get-all-orgs-link', 'API\PartnerController@getAllOrgLink')->name('api.partners.getAllOrgLink');
        Route::post('{type}/add-partner-link-org', 'API\PartnerController@addPartnerLinkOrg')->name('api.partners.addPartnerLinkOrg');

        Route::resource("pay-types", "API\OrgPayController")->names([
            "index"=>"api.OrgPay.index",
            "create"=>"api.OrgPay.create",
            "store"=>"api.OrgPay.store",
            "show"=>"api.OrgPay.show",
            "edit"=>"api.OrgPay.edit",
            "update"=>"api.OrgPay.update",
            "destroy"=>"api.OrgPay.destroy"
        ]);

        Route::resource("ship-types", "API\OrgShipController")->names([
            "index"=>"api.OrgShip.index",
            "create"=>"api.OrgShip.create",
            "store"=>"api.OrgShip.store",
            "show"=>"api.OrgShip.show",
            "edit"=>"api.OrgShip.edit",
            "update"=>"api.OrgShip.update",
            "destroy"=>"api.OrgShip.destroy"
        ]);
    });

    Route::get('get-user-info', 'API\UserController@getUserInfo')->name('api.getUserInfo');
    Route::post('update-user-info', 'API\UserController@updateUserInfo')->name('api.updateUserInfo');
    Route::post('update-profile', 'API\UserController@updateProfile')->name('api.updateProfile');
    Route::post('change-avatar', 'API\UserController@changeAvatar')->name('api.changeAvatar');
    Route::post('change-password', 'API\UserController@changePassword')->name('api.changePassword');
    Route::get('register-business', 'API\UserController@getDataRegisterBusiness')->name('api.getDataRegisterBusiness');
    Route::post('register-business', 'API\UserController@registerBusiness')->name('api.registerBusiness');
    Route::post('vnptpay-generate-data', 'API\VnptPayController@vnptpayGenerateData');

    Route::middleware(['gov'])->prefix("manager")->group(function(){
        Route::get('register-business', 'API\UserController@getDataRegisterBusiness')->name('apiManager.getDataRegisterBusiness');
        Route::post('register-business', 'API\UserController@registerBusiness')->name('apiManager.registerBusiness');
        Route::get('get-org-general-info/{org_id}', 'API\OrganizationController@getGeneralInfo')->name('apiManager.getOrgGeneralInfo');
        Route::post('save-org-general-info/{org_id}', 'API\OrganizationController@saveGeneralInfo')->name('apiManager.saveOrgGeneralInfo');
        Route::delete('delete-org/{org_id}', 'API\OrganizationController@delete')->name('apiManager.delete');
        Route::get('search-org', 'API\OrganizationController@search')->name('apiManager.searchOrg');
        Route::post('update-org-status', 'API\OrganizationController@updateStatus')->name('apiManager.updateOrgStatus');

        Route::get('manage-advertising', 'API\AdvertisingController@searchManage');
        Route::get('manage-advertising/{id}', 'API\AdvertisingController@showManage');
        Route::post('manage-advertising/{id}', 'API\AdvertisingController@setStatusManage');
        Route::delete('manage-advertising/{id}', 'API\AdvertisingController@deleteManage');

        Route::get('manage-infringement/{type}', 'API\InfringementController@searchManage');
        Route::get('manage-infringement/{type}/{id}', 'API\InfringementController@showManage');
        Route::post('manage-infringement/{type}/{id}', 'API\InfringementController@performManage');
        Route::get('manage-question-answer/{type}', 'API\QuestionAnswerController@searchQA');
        Route::post('resolve-question-answer/{id}', 'API\QuestionAnswerController@resolveQA');
    });
});

// VNPT PAY
Route::prefix('vnptpay')->group(function () {
    Route::post('confirm', 'API\VnptPayController@confirm');
});
