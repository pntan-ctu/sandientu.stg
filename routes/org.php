<?php

/*
  |--------------------------------------------------------------------------|
  | Organization Routes - Phần quản trị cho cơ sở SX, KD
  |--------------------------------------------------------------------------|
  |
 */
Route::prefix('business/{organizationId}')->group(function () {
    Route::POST('/product/create/getcatalog/{idcatalog}', 'Org\ProductController@getCatalogy');
    //Trang vào đầu tiên của cơ sở. Message: Org\OrgMessageController@index 
    Route::get('/', 'Org\HomeController@home')->name('org');
    Route::get('/home', 'Org\HomeController@home');
    
    //Route::get('/', 'Org\InfoOrgController@editGeneralInfo')->name('org');

    //Quản lý nhà cung cấp - phân phối
    Route::get('{type}/partner/data', 'Org\PartnerController@indexData')->name('partner.indexData');
    Route::resource('{type}/partner', 'Org\PartnerController');
    Route::get('{type}/partner-link/data', 'Org\PartnerController@indexDataOrgLink')->name('partner-link.indexDataOrgLink');
    Route::resource('{type}/partner-link', 'Org\PartnerController');
    Route::post('{type}/partner/postOrg', 'Org\PartnerController@createOrg');

    //Quản lý chi nhánh
    Route::get('branch/data', 'Org\BranchsController@indexData')->name('branch.indexData');
    Route::resource('branch', 'Org\BranchsController');
    
    //Thống kê doanh số bán hàng
    Route::get('report-sale/data', 'Org\ReportSaleController@indexData')->name('report-sale.indexData');
    Route::get('report-sale', 'Org\ReportSaleController@index')->name('report-sale.index');
    Route::get('report-sale/exportExcel', 'Org\ReportSaleController@exportExcel');
    
    //Danh sách đen
    Route::get('black-list/data', 'Org\BlackListController@indexData')->name('black-list.indexData');
    Route::resource('black-list', 'Org\BlackListController');

    //Quản lý đặt hàng
    Route::post('order/getOrder', 'Org\OrdersController@getOrder')->name('order.getOrder');
    Route::post('order/actOrder', 'Org\OrdersController@actOrder')->name('order.actOrder');
    Route::post('order/huyOrder', 'Org\OrdersController@huyOrder')->name('order.huyOrder');
    Route::get('order/data', 'Org\OrdersController@indexData')->name('order.indexData');
    Route::get('order/showOrder/{id}', 'Org\OrdersController@showOrder')->name('order.showOrder');
    Route::post('order/addComment', 'Org\OrdersController@addComment')->name('order.addComment');
    Route::resource('order', 'Org\OrdersController');

    //Quản lý lịch sử mua hàng
    Route::post('buys/actOrder', 'Org\BuyController@actOrder')->name('buys.actOrder');
    Route::post('buys/huyOrder', 'Org\BuyController@huyOrder')->name('buys.huyOrder');
    Route::get('buys/data', 'Org\BuyController@indexData')->name('buys.indexData');
    Route::get('buys/showOrder/{id}', 'Org\BuyController@showOrder')->name('buys.showOrder');
    Route::post('buys/addComment', 'Org\BuyController@addComment')->name('buys.addComment');
    Route::resource('buys', 'Org\BuyController');

    //Quản lý thông tin cơ sở
    Route::get('generalInfo', 'Org\InfoOrgController@editGeneralInfo')->name('generalInfo.create');
    Route::put('generalInfo/update', 'Org\InfoOrgController@updateGeneralInfo')->name('generalInfo.update');

    //Bài viết giới thiệu cơ sở
    Route::get('introInfo', 'Org\InfoOrgController@createIntroInfo')->name('introInfo.create');
    Route::post('introInfo/store', 'Org\InfoOrgController@storeIntroInfo')->name('introInfo.store');
    Route::put('introInfo/update/{orgInfoDetail_id}', 'Org\InfoOrgController@updateIntroInfo')->name('introInfo.update');

    //Quản lý sản phẩm
    Route::get('product/select-tree-search', 'Org\ProductController@selectTreeSearch');
    Route::get('product/data', 'Org\ProductController@indexData')->name('product.indexData');
    Route::get('product-links/data', 'Org\ProductController@indexDataProductLink')->name('product.indexDataProductLink');
    //----more info
    Route::get('product/get-view-more-info', 'Org\ProductController@getViewMoreInfo')->name('product.getViewMoreInfo');
    Route::post('product/store-more-info', 'Org\ProductController@storeMoreInfo')->name('product.store-info');
    //----certificate
    Route::get('product/get-view-certificate', 'Org\ProductController@getViewCertificate')->name('product.getViewCertificate');
    Route::resource('product', 'Org\ProductController');
    Route::resource('product-links', 'Org\ProductController');
    
    //Người theo dõi cơ sở
    Route::get('user-follow/data', 'Org\UserFollowController@indexData')->name('user-follow.indexData');
    Route::resource('user-follow', 'Org\UserFollowController');

    //Quản lý chứng chỉ org
    Route::get('certificates-org/data', 'Org\OrgCertificatesController@indexData')->name('certificates-org.indexData');
    Route::resource('certificates-org', 'Org\OrgCertificatesController');
    
    //Thanh toán, vận chuyển
    Route::get('pay-ship', 'Org\OrganizationPayTypeController@index');
    Route::resource('pay-type', 'Org\OrganizationPayTypeController');
    Route::resource('ship-type', 'Org\OrganizationShipTypeController');

    
    Route::get('{type_id}/certificates/data', 'Org\ProductCertificatesController@indexData')->name('certificates-pro.indexData');
    Route::resource('{type_id}/certificates', 'Org\ProductCertificatesController');

    Route::get('advertising-org/data', 'Web\AdsController@indexData')->name('advertising-org.indexData');
    Route::resource('advertising-org', 'Web\AdsController');

    Route::get('members/data', 'Org\MemberController@indexData')->name('members.indexData');
    Route::get('members/{id}/reset-password', 'Org\MemberController@resetPassword')->name('members.resetPassword');
    Route::resource('members', 'Org\MemberController');

    // Tin nhắn
    Route::get('messages', 'Org\OrgMessageController@index')->name('org.messages');

    Route::get('report-customer/data', 'Org\OrgReportCustomerController@indexData')->name('report-customer.indexData');
    Route::get('report-customer', 'Org\OrgReportCustomerController@index')->name('report-customer.index');
    Route::get('report-customer/exportExcel', 'Org\OrgReportCustomerController@exportExcel');

    //Route::post('sendMessageToFollowers', 'Org\OrgMessageController@sendMessageToFollowers');
    Route::post('sendMessageToFollowers', 'Web\MessengerController@sendMessageToFollowers');

    //route ajax
    Route::get("ajax/getForm",function(){
      return view('org.product.boxproductBDS');
    });
});



    