<?php

namespace App\Http\Requests;

class UpdateVideoRequest extends StoreVideoRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['group_id'] = 'required';
        $rules['title'] = 'required';
        return $rules;
    }
}
