<?php

namespace App\Http\Requests;

class UpdateBranchRequest extends StoreBranchRequest
{
    /**
     * Determine if the Area is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['name'] = 'required';
        return $rules;
    }
}
