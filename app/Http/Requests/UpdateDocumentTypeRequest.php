<?php

namespace App\Http\Requests;

class UpdateDocumentTypeRequest extends StoreDocumentTypeRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
        ];
    }
}
