<?php

namespace App\Http\Requests;

class UpdateOrganizationInfoRequest extends StoreOrganizationRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        //Bo o day de validate trong controller,
        //do chi bat nhap khi chua duyet va vi disable khi da duyet nen no ko ve server
        unset($rules['department_manage_id']);
        unset($rules['founding_type']);
        return $rules;
    }
}
