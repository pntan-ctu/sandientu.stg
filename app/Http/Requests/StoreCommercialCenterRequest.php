<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCommercialCenterRequest extends FormRequest
{
    /**
     * Determine if the Area is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:branches,name',
            'address' => 'required',
            'region'  =>'required|numeric|min:1',
            'map_lat'  =>'nullable|numeric|max:90|min:-90',
            'map_long'  =>'nullable|numeric|max:180|min:-180',
        ];
    }
    public function messages()
    {
        return [
            'region.required' => 'Trường Địa chỉ không được bỏ trống',
            'address.required' => 'Trường Số nhà không được bỏ trống',
            'region.min' => 'Trường Địa chỉ không được bỏ trống',
            'map_lat.numeric' => 'Trường Latitude phải là số',
            'map_lat.max' => 'Trường Latitude không được lớn hơn 90',
            'map_lat.min' => 'Trường Latitude phải tối thiểu là -90',
            'map_long.numeric' => 'Trường Latitude phải là số',
            'map_long.max' => 'Trường Latitude không được lớn hơn 90',
            'map_long.min' => 'Trường Latitude phải tối thiểu là -90',
        ];
    }
}
