<?php

namespace App\Http\Requests;

class UpdateOrganizationPayTypeRequest extends StoreOrganizationPayTypeRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['pay_type_id'] = "unique:organization_pay_types,pay_type_id,"
                . "$this->id,id,organization_id,{$this->organizationId}";
        
        return $rules;
    }
}
