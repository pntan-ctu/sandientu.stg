<?php

namespace App\Http\Requests;

class UpdateRoleRequest extends StoreRoleRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        
        $rules['name'] = "required|max:255|unique:roles,name,$this->id,id,".
        "organization_type_id,$this->organization_type_id";
        $rules['title'] = "max:255";

        return $rules;
    }
}
