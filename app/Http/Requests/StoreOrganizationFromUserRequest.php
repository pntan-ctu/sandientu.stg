<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrganizationFromUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'region_id' => 'required',
            'address' => 'required',
            'tel' => 'required|max:32',
            'director' => 'required|max:64',
            'organization_type_id' => 'required',
            'manage_organization_id' => 'required',
            //'founding_type' => 'required',
            //'founding_number' => 'required',
            //'founding_by_gov' => 'required|max:127',
            //'founding_date' => 'required',
            'organization_level_id' => 'required'
        ];
    }
}
