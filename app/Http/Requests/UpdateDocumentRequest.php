<?php

namespace App\Http\Requests;

class UpdateDocumentRequest extends StoreDocumentRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'number_symbol' => 'required|max:255',
            'document_field_id' => 'required',
            'document_type_id' => 'required',
            'document_organ_id' => 'required',
            'sign_date' => 'required|date_format:d/m/Y',
            'quote' => 'required|max:255',
            'sign_by' => 'required|max:255',
            'description' => 'required|max:1000',
        ];
    }
}
