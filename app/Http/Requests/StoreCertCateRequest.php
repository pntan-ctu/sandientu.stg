<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreCertCateRequest extends FormRequest
{
    /**
     * Determine if the Area is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255|unique:certificate_categories,name',
            'type' => 'required',
            'description' => 'required|max:255',
            'rank' => 'required|integer|numeric|min:1',
            'icon' => CommonRules::IMAGE,
        ];
    }
}
