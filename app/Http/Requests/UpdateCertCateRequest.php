<?php

namespace App\Http\Requests;

class UpdateCertCateRequest extends StoreCertCateRequest
{
    /**
     * Determine if the Area is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['name'] = 'required';
        $rules['rank'] = 'required|integer|numeric|min:1';
        return $rules;
    }
}
