<?php

namespace App\Http\Requests;

class UpdateDocumentOrganRequest extends StoreDocumentOrganRequest
{
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'no' => 'required',
        ];
    }
}
