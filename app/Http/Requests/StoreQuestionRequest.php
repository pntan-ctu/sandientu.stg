<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/30/18
 * Time: 09:00
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'answer_user' => 'required',

        ];
    }
}
