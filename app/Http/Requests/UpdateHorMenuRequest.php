<?php

namespace App\Http\Requests;

class UpdateHorMenuRequest extends StoreHorMenuRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['title'] = 'required';
        $rules['no'] = 'integer|numeric|min:1';
        return $rules;
    }
}
