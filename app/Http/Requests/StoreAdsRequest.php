<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/30/18
 * Time: 09:00
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreAdsRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'category_id' => 'required',
            'title' => 'required',
            'product_category_id' => 'required',
            'region_id' => 'required',
//            'to_date' => 'required',
            'address'=>'required',
            'tel'=>'required'

        ];
    }
}
