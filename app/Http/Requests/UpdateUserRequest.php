<?php

namespace App\Http\Requests;

class UpdateUserRequest extends StoreUserRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $id = $this->route('user')->id;
        $rules['email'] .= ",$id";
        //$rules['password'] = 'nullable|confirmed|min:6';
        return $rules;
    }
}
