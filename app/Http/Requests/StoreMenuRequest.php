<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMenuRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:128',
            'organization_type_id' => 'required',
            'module_id' => 'required',
            'no' => 'numeric|min:1|max:2147483646|nullable'
        ];
    }

    /**
     * Custom messages validate
     * @return array
     */
    public function messages()
    {
        return [
            'name.required' => 'Tên menu không được bỏ trống'
        ];
    }
}
