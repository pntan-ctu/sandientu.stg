<?php

namespace App\Http\Requests;

class UpdateDocumentFieldRequest extends StoreDocumentFieldRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'name' => 'required|max:255',
            'no' => 'required',
        ];
    }
}
