<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAdvertisingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:511',
            'address' => 'required|max:255',
            'tel' => 'required|max:32',
            'content' => 'required',
            'from_date' => 'required|date_format:d/m/Y',
            'to_date' => 'required|date_format:d/m/Y',
            'fanpage' => 'max:255',
        ];
    }

    /**
     * Custom messages validate
     * @return array
     */
    public function messages()
    {
        return [
            'title.required' => 'Trường tiêu đề không được bỏ trống',
            'content.required' => 'Trường nội dung không được bỏ trống'
        ];
    }
}
