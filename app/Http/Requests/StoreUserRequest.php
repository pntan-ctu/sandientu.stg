<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|max:255',
            //'username' => 'required|max:255|alpha_dash|unique:users,username',
            'email' => 'required|email|max:255|unique:users,email',
            //'date_of_birth' => 'nullable|date_format:d/m/Y',
            //'password' => 'required|confirmed|min:6',
            'active' => 'boolean',
            'organization_id' => 'required|integer|numeric|min:-1'
        ];
    }
    public function messages()
    {
        return [
        'organization_id.min' => 'Trường cơ quan quản lý không được bỏ trống',
        ];
    }
}
