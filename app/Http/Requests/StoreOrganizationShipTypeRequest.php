<?php

namespace App\Http\Requests;

use App\Models\ShipType;

use Illuminate\Foundation\Http\FormRequest;

class StoreOrganizationShipTypeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "ship_type_id" => "unique:organization_ship_types,ship_type_id,NULL,"
            . "id,organization_id,{$this->organizationId}",
        ];
    }
        
    public function messages()
    {
        return [
            'ship_type_id.unique' => 'Trường hình thức vận chuyển này đã tồn tại',
            
        ];
    }
}
