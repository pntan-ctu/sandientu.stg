<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/30/18
 * Time: 09:06
 */

namespace App\Http\Requests;

class UpdateAdsRequest extends StoreAdsRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        return $rules;
    }
}
