<?php

namespace App\Http\Requests;

class UpdateQuestionRequest extends StoreQuestionRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = parent::rules();
        $rules['answer_user'] = 'required';
        $rules['duty'] = 'required';
        $rules['content_answer'] = 'required';
        return $rules;
    }
}
