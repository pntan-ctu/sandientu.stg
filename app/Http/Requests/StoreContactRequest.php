<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContactRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:255',
            'name' => 'required|max:255',
            'content' => 'required|max:255',
            //'files' => CommonRules::FILE,
            'org'  =>'required',
            'g-recaptcha-response' => ['required', new \App\Rules\ValidRecaptcha],
        ];
    }
    public function messages()
    {
        return [
        'org.required' => 'Trường cơ quan tiếp nhận không được bỏ trống',
        'content.required' => 'Trường nội dung không được bỏ trống'
        ];
    }
}
