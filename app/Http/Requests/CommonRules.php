<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 9/22/18
 * Time: 14:48
 */

namespace App\Http\Requests;

class CommonRules
{
    const IMAGE = "max:5000000|mimes:bmp,dib,rle,jpg,jpeg,jpe,jfif,gif,tif,tiff,png";
    //. "|dimensions:max_width=1366,max_height=768"; kích cỡ tự resign
    const MSWORD = "max:20000000|mimes:doc,docx";
    const MSEXCEL = "max:20000000|mimes:xls,xlsx";
    const DOCUMENT = "max:20000000|mimes:doc,docx,xls,xlsx,ppt,pptx,pdf,txt";
    const FILE = "max:20000000|mimes:bmp,dib,rle,jpg,jpeg,jpe,jfif,gif,tif,tiff,png"
    . ",doc,docx,xls,xlsx,ppt,pptx,pdf,rar,zip,swf,txt";
}
