<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/30/18
 * Time: 09:00
 */

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuestionWebRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:255',
           // 'email' => 'required|string|max:255',
            'title' => 'required|string|max:255',
//            'content' => 'required|string',
            'g-recaptcha-response' => ['required', new \App\Rules\ValidRecaptcha]
        ];
    }
//    public function messages()
//    {
//        return [
//        'fullname.required' => 'Trường cơ quan tiếp nhận không được bỏ trống',
//        'content.required' => 'Trường nội dung không được bỏ trống'
//        ];
//    }
}
