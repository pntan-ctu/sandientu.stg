<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreNewsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // TODO check phân quyền
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required|max:500',
            //'img_path' => 'required',
            'group_id' => 'required',
            'summary' => 'required',
            'content' => 'required',
            'status' => 'boolean',
            'hotnews' => 'boolean',
            'shared' => 'boolean',
            'comment' => 'boolean'
        ];
    }
}
