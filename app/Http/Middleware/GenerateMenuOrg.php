<?php

namespace App\Http\Middleware;

use Closure;
use Lavary\Menu\Facade as Menu;
use App\Business\MenuBusiness;
use App\Business\OrganizationBusiness;
use App\Models\Organization;
use Illuminate\Support\Facades\View;

class GenerateMenuOrg
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $organizationId = (int)$request->organizationId;

        //Check tồn tại và có phải loại CS SX,KD không
        $organization = Organization::find($organizationId);
        if ($organization == null || $organization->organization_type_id < 100) {
            return redirect('home');
        }
        
        $ogrId = (int)getOrgId(); //organizationId của user đang login
        $ogrTypeId = (int)getOrgTypeId(); //organizationTypeId của user đang login

        //Nếu là khách hoặc thành viên thì không được vào trang QLCS
        if ($ogrTypeId <= 0) {
            return redirect('home');
        } else {
            //Nếu là user của CSSXKD thì chỉ được vào chính CSSXKD đó (không vào CS khác)
            if ($ogrTypeId >= 100) {
                if ($organizationId != $ogrId) {
                    return redirect(route('org', ['organizationId' => $ogrId]));
                }
            } else {
                //Nếu là user của cơ quan quản lý thì chỉ có quyền với cơ sở thuộc phạm vi
                if (!OrganizationBusiness::checkPermissionManage($organization)) {
                    return redirect('home');
                }
            }
        }
        
        //Set scope cho check quyen
        if ($ogrTypeId > 1) {
            \Silber\Bouncer\BouncerFacade::scope()->to($ogrTypeId);
        }

        //Truyền ra layout mảng menu
        /*Menu::make('mainMenuOrg', function ($menu) use ($organization) {
            $aMenus = MenuBusiness::getByOrganization($organization);

            $urlOrgHome = route('org', ['organizationId' => $organization->id]);
            $this->createMenus($menu, $aMenus, 0, $urlOrgHome);
        });*/

        //Truyền ra layout và blade đối tượng cơ ở hiện tại
        View::share('currentOrganizationObject', $organization);

        return $next($request);
    }

    /*public function createMenus($menu, $aMenus, $parent_id, $urlOrgHome)
    {
        foreach ($aMenus as $oMenu) {
            if ($oMenu->parent_id == $parent_id) {
                $url = isset($oMenu->url) ? "{$urlOrgHome}/{$oMenu->url}" : null;
                $submenu = $menu->add($oMenu->name, $url)
                    ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                    ->append("</span>")
                    ->active("{$url}/*");

                $this->createMenus($submenu, $aMenus, $oMenu->id, $urlOrgHome);
            }
        }
    }*/
}
