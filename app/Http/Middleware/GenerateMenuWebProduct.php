<?php

namespace App\Http\Middleware;

use Closure;
use Lavary\Menu\Facade as Menu;
use App\Business\MenuBusiness;
use App\Models\ProductCategory;

class GenerateMenuWebProduct
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*Menu::make('menuWebProducts', function ($menu) {
            $aMenus = ProductCategory::all();
            $this->createMenus($menu, $aMenus, 0);
        });*/

        return $next($request);
    }

    /*public function createMenus($menu, $aMenus, $parent_id)
    {
        foreach ($aMenus as $oMenu) {
            if ($oMenu->parent_id == $parent_id) {
                $submenu = $menu->add($oMenu->name, "danh-muc-san-pham/".$oMenu->path);
                //  ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                //  ->append("</span>");

                $this->createMenus($submenu, $aMenus, $oMenu->id);
            }
        }
    }*/
}
