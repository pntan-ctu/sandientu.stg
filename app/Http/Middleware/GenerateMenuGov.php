<?php

namespace App\Http\Middleware;

use Closure;
use Lavary\Menu\Facade as Menu;
use App\Business\MenuBusiness;

class GenerateMenuGov
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        //organizationTypeId của user đang login
        $ogrTypeId = (int)getOrgTypeId();
        //Nếu không phải quản trị hoặc cơ quan quản lý không được vào
        if ($ogrTypeId <= 0 || $ogrTypeId >= 100) {
            return redirect('home');
        }

        //Set scope cho check quyen
        if ($ogrTypeId > 1) {
            \Silber\Bouncer\BouncerFacade::scope()->to($ogrTypeId);
        }

        /*Menu::make('mainMenuGov', function ($menu) {
            $aMenus = MenuBusiness::getByCurrentUser();
            $this->createMenus($menu, $aMenus, 0);
        });*/

        return $next($request);
    }

    /*public function createMenus($menu, $aMenus, $parent_id)
    {
        foreach ($aMenus as $oMenu) {
            if ($oMenu->parent_id == $parent_id) {
                $submenu = $menu->add($oMenu->name, $oMenu->url)
                    ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                    ->append("</span>")
                    ->active("{$oMenu->url}/*");

                $this->createMenus($submenu, $aMenus, $oMenu->id);
            }
        }
    }*/
}
