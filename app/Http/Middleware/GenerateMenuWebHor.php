<?php

namespace App\Http\Middleware;

use Closure;
use Lavary\Menu\Facade as Menu;
use App\Models\HorMenu;

class GenerateMenuWebHor
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        /*Menu::make('menuWebHor', function ($menu) {
            $aMenus = HorMenu::orderBy('no', 'asc')->get();
            $this->createMenus($menu, $aMenus, 0);
        });*/

        return $next($request);
    }

    /*public function createMenus($menu, $aMenus, $parent_id)
    {
        foreach ($aMenus as $oMenu) {
            if ($oMenu->parent_id == $parent_id) {
                if ($oMenu->mtype == 1) {
                    if ($oMenu->parent_id == 0) {
                        $link = $oMenu->link;
                    } else {
                        $link = 'danh-muc-tin-tuc/' . $oMenu->link;
                    }
                    $submenu = $menu->add($oMenu->title, $link)
                            ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                            ->append("</span>");
                } else {
                    $submenu = $menu->add($oMenu->title, $oMenu->link)
                            ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                            ->append("</span>");
                }

                $this->createMenus($submenu, $aMenus, $oMenu->id);
            }
        }
    }*/
}
