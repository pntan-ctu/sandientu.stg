<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Auth\Access\AuthorizationException;

class GovRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isGov = isUserGov();
        if ($isGov) {
            return $next($request);
        } else {
            throw new AuthorizationException("Bạn không có quyền thực hiện tính năng này");
        }
    }
}
