<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Illuminate\Support\Facades\Route;
use Lavary\Menu\Facade as Menu;
use App\Business\MenuBusiness;
use App\Models\Organization;

class MenuOrgComposer
{
    protected $mainMenuOrg;

    public function __construct()
    {
        $this->initMenu();
    }

    public function initMenu()
    {
        $organizationId = (int)Route::current()->parameter('organizationId');
        $organization = Organization::find($organizationId);

        Menu::make('mainMenuOrg', function ($menu) use ($organization) {
            if ($organization != null) {
                $aMenus = MenuBusiness::getByOrganization($organization);
                $urlOrgHome = route('org', ['organizationId' => $organization->id]);
                $this->createMenus($menu, $aMenus, 0, $urlOrgHome);
            }
            $this->mainMenuOrg = $menu;
        });
    }

    public function createMenus($menu, $aMenus, $parent_id, $urlOrgHome)
    {
        foreach ($aMenus as $oMenu) {
            if ($oMenu->parent_id == $parent_id) {
                $url = isset($oMenu->url) ? "{$urlOrgHome}/{$oMenu->url}" : null;
                $submenu = $menu->add($oMenu->name, $url)
                    ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                    ->append("</span>")
                    ->active("{$url}/*");

                $this->createMenus($submenu, $aMenus, $oMenu->id, $urlOrgHome);
            }
        }
    }

    public function compose(View $view)
    {
        $view->with('mainMenuOrg', $this->mainMenuOrg);
    }
}
