<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Lavary\Menu\Facade as Menu;
use App\Business\MenuBusiness;

class MenuGovComposer
{
    protected $mainMenuGov;

    public function __construct()
    {
        $this->initMenu();
    }

    public function initMenu()
    {
        Menu::make('mainMenuGov', function ($menu) {
            $aMenus = MenuBusiness::getByCurrentUser();
            $this->createMenus($menu, $aMenus, 0);
            $this->mainMenuGov = $menu;

            //Temp create menu
            /*$menu1 = $menu->add('QUẢN LÝ CƠ SỞ','')
                ->prepend('<i class="fa fa-laptop"></i><span>')
                ->append('</span>');
            $menu1->add('Cơ sở sản xuất', 'co-so-san-xuat')
                ->prepend('<i class="fa fa-circle-o"></i><span>')
                ->append('</span>')
                ->active('tiep-nhan-yeu-cau/*');
            $menu1->add('Cơ sở kinh doanh', 'co-so-kinh-doanh')
                ->prepend('<i class="fa fa-circle-o"></i> ')
                ->append('</span>')
                ->active('xxx/*');*/
        });
    }

    public function createMenus($menu, $aMenus, $parent_id)
    {
        foreach ($aMenus as $oMenu) {
            if ($oMenu->parent_id == $parent_id) {
                $submenu = $menu->add($oMenu->name, $oMenu->url)
                    ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                    ->append("</span>")
                    ->active("{$oMenu->url}/*");

                $this->createMenus($submenu, $aMenus, $oMenu->id);
            }
        }
    }

    public function compose(View $view)
    {
        $view->with('mainMenuGov', $this->mainMenuGov);
    }
}
