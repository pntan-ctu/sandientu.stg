<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Lavary\Menu\Facade as Menu;
use App\Models\ProductCategory;

class MenuProductLeftComposer
{
    protected $menuWebProductsLeft;
    public function __construct()
    {
        $this->initMenu();
    }
    public function initMenu()
    {
        Menu::make('menuWebProductsLeft', function ($menu) {
            $aMenus = ProductCategory::all();
            $this->createMenus($menu, $aMenus, 0);
            $this->menuWebProductsLeft = $menu;
        });
    }
    public function createMenus($menu, $aMenus, $parent_id)
    {
        foreach ($aMenus as $oMenu) {
            if ($oMenu->parent_id == $parent_id) {
                $submenu = $menu->add($oMenu->name, "danh-muc-san-pham/" . $oMenu->path);
                $this->createMenus($submenu, $aMenus, $oMenu->id);
            }
        }
    }
    public function compose(View $view)
    {
        $view->with('menuWebProductsLeft', $this->menuWebProductsLeft);
    }
}
