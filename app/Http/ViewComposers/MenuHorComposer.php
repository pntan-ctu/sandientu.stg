<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
use Lavary\Menu\Facade as Menu;
use App\Models\HorMenu;

class MenuHorComposer
{
    protected $menuWebHor ;
    public function __construct()
    {
        $this->initMenu();
    }
    public function initMenu()
    {
        Menu::make('menuWebHor', function ($menus) {
            $aMenus = HorMenu::all()->where('active','1');
            $this->createMenus($menus, $aMenus, 0);
            $this->menuWebHor = $menus;
        });
    }
    public function createMenus($menu, $aMenus, $parent_id)
    {
        foreach ($aMenus as $oMenu) {
            if ($oMenu->parent_id == $parent_id) {
                if ($oMenu->mtype == 1) {
                    if ($oMenu->parent_id == 0) {
                        $link = $oMenu->link;
                    } else {
                        $link = 'danh-muc-tin-tuc/' . $oMenu->link;
                    }
                    $submenu = $menu->add($oMenu->title, $link)
                        ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                        ->append("</span>");
                } else {
                    $submenu = $menu->add($oMenu->title, $oMenu->link)
                        ->prepend("<i class='fa {$oMenu->icon}'></i><span>")
                        ->append("</span>");
                }

                $this->createMenus($submenu, $aMenus, $oMenu->id);
            }
        }
    }
    public function compose(View $view)
    {
        $view->with('menuWebHor', $this->menuWebHor);
    }
}
