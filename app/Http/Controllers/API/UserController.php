<?php
namespace App\Http\Controllers\API;

use App\Authors\AbilityName;
use App\Authors\OrgAuthor;
use App\Business\DepartmentManageBussiness;
use App\Business\OrganizationBusiness;
use App\Business\OrganizationTypeBusiness;
use App\Business\RegionBusiness;
use App\Constants;
use App\Http\Controllers\Web\ProfileController;
use App\Http\Requests\CommonRules;
use App\Models\Area;
use App\Models\CommercialCenter;
use App\Models\DepartmentManage;
use App\Models\Organization;
use App\Models\Region;
use Illuminate\Support\Facades\DB;
use Silber\Bouncer\BouncerFacade as Bouncer;
use App\Models\OrganizationLevel;
use App\Models\UserProfile;
use App\OrgType;
use App\Utils\UploadFileUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use App\Business\UserBusiness;
use App\Models\Role;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Support\Facades\Lang;

class UserController extends Controller
{
    use ThrottlesLogins;
    public $successStatus = 200;
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }
    /**
     * login api
     *
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {
        $this->validateLogin($request);
        if ($this->hasTooManyLoginAttempts($request)) {
            $this->fireLockoutEvent($request);
            $seconds = $this->limiter()->availableIn(
                $this->throttleKey($request)
            );
            return response()->json(['error'=>Lang::get('auth.throttle', ['seconds' => $seconds])], 429);
        }
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $success['token'] =  $user->createToken('MyApp')-> accessToken;
            $success['isUserGov'] = isUserGov();
            $success['isUserBusiness'] = isUserBusiness();
            $success['organization_id']=$user->organization_id;
            
            $this->clearLoginAttempts($request);
            return response()->json(['success' => $success], $this-> successStatus);
        } else {
            $this->incrementLoginAttempts($request);
            return response()->json(['error'=>'Unauthorised'], 401);
        }
    }
    
    /**
     * Register api
     *
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:127',
            'email' => 'required|unique:users,email|email|max:255',
            'password' => 'required|max:127',
            'c_password' => 'required|same:password',
        ]);
        if ($validator->fails()) {
            return response()->json(['error'=>$validator->errors()], 401);
        }
        $input = $request->all();
        $input['password'] = Hash::make($input['password']);
        $input['active'] = 1;
        $user = User::create($input);
        $success['token'] =  $user->createToken('MyApp')-> accessToken;
        $success['name'] =  $user->name;
        return response()->json(['success'=>$success], $this-> successStatus);
    }
    /**
     * details api
     *
     * @return \Illuminate\Http\Response
     */
    public function details()
    {
        $user = Auth::user();
        return response()->json(['success' => $user], $this-> successStatus);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, Request $request)
    {
        $input = $request->all();
          
        $search = isset($input["search"])?$input["search"]:"";
        $active=isset($input["active"])?(int)$input["active"]:-1;
        $perPage = isset($input["perPage"])?(int)$input["perPage"]:10;
        
        return UserBusiness::search(
            $organization_id,
            $search,
            $active,
            $perPage
        );
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($organization_id, Request $request)
    {
        $returnVal = array();
        $organization = Organization::find($organization_id);
        $orgTypeId = $organization->organization_type_id;
        $roles = Bouncer::role()->where('scope', '=', $orgTypeId)->get();
        $returnVal["roles"]= $roles;
        
        return $returnVal;
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organization_id, Request $request)
    {
        $input = $request->all();
        $input["organization_id"]=$organization_id;
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);
        return UserBusiness::save($input);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, User $user)
    {
        $returnVal = [];
        $organization = Organization::find($organization_id);
        $orgTypeId = $organization->organization_type_id;
        $roles = Bouncer::role()->where('scope', '=', $orgTypeId)->get();
        $assignRole = DB::table('bouncer_assigned_roles as a')
            ->join('bouncer_roles as b', 'b.id', '=', 'a.role_id')
            ->select(["b.id", "b.name"])->where('a.entity_id', "=", $user->id)
            ->where("a.entity_type", '=', 'App\Models\User')->get();
        $user->roles = $assignRole;
        $returnVal['user'] = $user;
        $returnVal['roles'] = $roles;
        return $returnVal;
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit($organization_id, User $user)
    {
        $returnVal = [];
        $organization = Organization::find($organization_id);
        $orgTypeId = $organization->organization_type_id;
        $roles = Bouncer::role()->where('scope', '=', $orgTypeId)->get();
        $assignRole = DB::table('bouncer_assigned_roles as a')
            ->join('bouncer_roles as b', 'b.id', '=', 'a.role_id')
            ->select(["b.id", "b.name"])->where('a.entity_id', "=", $user->id)
            ->where("a.entity_type", '=', 'App\Models\User')->get();
        $user->roles = $assignRole;
        $returnVal['user'] = $user;
        $returnVal['roles'] = $roles;
        return $returnVal;
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, Request $request, User $user)
    {
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);
        $input = $request->all();
        $input["organization_id"]=$organization_id;
        $input["id"]=$user->id;

        return UserBusiness::save($input);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, User $user)
    {
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);
        return UserBusiness::delete($user);
    }
    
    /**
     * ResetPass the specified resource from storage.
     *
     * @param  int  $organization_id
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function resetpass($organization_id, User $user)
    {
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);
        return UserBusiness::resetPass($user);
    }

    public function updateProfile(Request $request)
    {
        $input = $request->all();
        $validate = [
            'address' => 'max:255',
            'identification' => 'max:64',
            'identification_by' => 'max:128',
            'date_of_birth' => 'date_format:d/m/Y',
            'identification_date' => 'date_format:d/m/Y'
        ];
        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        $user = auth()->user();
        $userProfile = $user->userProfile;
        if (!isset($userProfile)) {
            $userProfile = new UserProfile();
            $userProfile->user_id = $user->id;
        }
        $userProfile->fill($input);
        $userProfile->save();
        return array("success" => true);
    }

    public function changeAvatar(Request $request)
    {
        $input = $request->all();
        $validate = ['avatar' => CommonRules::IMAGE];
        $validator = Validator::make($input, $validate);
        if ($validator->fails()) {
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        $img_path = '';
        if (isset($input["avatar"])) {
            $check = getimagesize($input["avatar"]);
            if (!$check) {
                return response()->json(['error' => array("avatar" => "File upload không phải là ảnh.")], 422);
            }

            $file = UploadFileUtil::uploadImageFile($input["avatar"]);
            $img_path = $file->path;
        }

        $user = auth()->user();
        $userProfile = $user->userProfile;
        if (!isset($userProfile)) {
            $userProfile = new UserProfile();
            $userProfile->user_id = $user->id;
        }
        if (strlen($img_path) > 0) {
            $userProfile->avatar = $img_path;
        }
        $userProfile->save();
        return array("success" => true);
    }

    /*
     * input: password_current, password, password_confirmation
     */
    public function changePassword(Request $request)
    {
        $profileController = new ProfileController();
        return $profileController->changePassword($request);
    }

    public function getDataRegisterBusiness(Request $request)
    {
        $result = [];
        $treeOrgGov = OrganizationBusiness::getTreeOrgGov();
        $result['treeOrgGov'] = $treeOrgGov;
        $orgTypeBusiness = OrganizationTypeBusiness::getTypeBusiness();
        $result['orgTypeBusiness'] = $orgTypeBusiness;
        $regions = RegionBusiness::getTreeWithParent(getRegionId());
        $result['regions'] = $regions;

        $departments = DepartmentManage::get();
        $result['departments'] = $departments;

        $foundingTypes = Constants::FOUNDING_TYPE;
        $rs = [];
        foreach ($foundingTypes as $k => $v) {
            $fType['id'] = $k;
            $fType['name'] = $v;
            $rs[] = $fType;
        }
        $result['founding_type'] = $rs;

        $organizationLevel = OrganizationLevel::get();
        $result['organization_levels'] = $organizationLevel;
        $result['areas'] = Area::all();
        $result['commercial_center'] = CommercialCenter::all();

        $isUserGov = isUserGov();
        if ($isUserGov) {
            $status = Constants::ORG_STATUS;
            $aStatus = [];
            foreach ($status as $k => $v) {
                $newItem['id'] = $k;
                $newItem['name'] = $v;
                $aStatus[] = $newItem;
            }
            $result['status'] = $aStatus;
        }

        return $result;
    }

    public function registerBusiness(Request $request)
    {
        if (!isUserGov()) {
            if (!isUserMember()) {
                return ['success' => false, 'msg' => 'Bạn chưa đăng ký thành viên.'];
            }
        }

        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->registerBusiness($request);
    }

    /**
     * logout api
     *
     * @return \Illuminate\Http\Response
     */
    public function logout(Request $request)
    {
        Auth::user()->token()->revoke();
        return ['success' => true, 'msg' => 'You have been logout'];
    }

    public function getUserInfo()
    {
        $user = Auth::user();
        $info = User::leftjoin('user_profiles', 'user_profiles.user_id', '=', 'users.id')
            ->where('users.id', '=', $user->id)->first();
        $regionId = $info->region_id ?? 0;
        $region = Region::find($regionId);
        $full_address = "";
        if (isset($info->address) && isset($region->full_name)) {
            $full_address = $info->address."-".$region->full_name;
        } else {
            $full_address = $info->address ?? $region->full_name ?? "";
        }
        $info->full_address = $full_address;
        unset($info->user_id);
        return $info;
    }

    public function updateUserInfo(Request $request)
    {
        $input = $request->all();
        $validate = [
            'name' => 'required',
            'tel'=>'nullable|numeric',
            'address' => 'max:255',
            'identification' => 'max:64',
            'identification_by' => 'max:128',
            'date_of_birth' => 'date_format:d/m/Y',
            'identification_date' => 'date_format:d/m/Y',
            'image' => CommonRules::IMAGE
        ];
        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        $user = auth()->user();
        $user->name=$input["name"];
        $user->tel = $input["tel"] ?? null;

        $userProfile = $user->userProfile;
        if (!isset($userProfile)) {
            $userProfile = new UserProfile();
            $userProfile->user_id = $user->id;
        }
        $userProfile->fill($input);

        if (isset($input["avatar"])) {
            $check = getimagesize($input["avatar"]);
            if (!$check) {
                return response()->json(['error' => array("avatar" => "File upload không phải là ảnh.")], 422);
            }

            $file = UploadFileUtil::uploadImageFile($input["avatar"]);
            if (isset($file->id) && strlen($file->path) > 0) {
                $userProfile->avatar = $file->path;
            }
        } else {
            $deleteAvatar = $input["delete_avatar"] ?? 0;
            if ($deleteAvatar == 1) {
                $userProfile->avatar = null;
            }
        }

        DB::beginTransaction();
        $userProfile->save();
        $user->save();
        DB::commit();
        return array("success" => true);
    }
    
    protected function validateLogin(Request $request)
    {
        $request->validate([
            $this->username() => 'required|string',
            'password' => 'required|string',
        ]);
    }
    public function username()
    {
        return 'email';
    }
}
