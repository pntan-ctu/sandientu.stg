<?php

namespace App\Http\Controllers\API;

use App\Models\Organization;
use App\Models\Partner;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\OrganizationBusiness;

class PartnerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, $type, Request $request)
    {
        $input = $request->all();
        $perPage = $input["perPage"] ?? 10;
        $keyword = $input["keyword"] ?? '';
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->getPartners($organization_id, $type, $keyword, $perPage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organization_id, $type, Request $request)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $input["type"] = $type;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->savePartner($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, $type, Partner $partner)
    {
        return $partner;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function edit($organization_id, Partner $partner)
    {
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, $type, Request $request, Partner $partner)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $input["type"] = $type;
        $input["id"] = $partner->id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->savePartner($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Partner $partner
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, $type, Partner $partner)
    {
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->deletePartner($partner, $organization_id);
    }

    public function getAllOrgLink($organization_id, $type, Request $request)
    {
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->getAllOrgLink($organization_id, $type, $request);
    }

    public function addPartnerLinkOrg($organization_id, $type, Request $request)
    {
        $orgBusiness = new OrganizationBusiness();
        $link_organization_id = $request->link_organization_id;
        return $orgBusiness->addPartnerLinkOrg($organization_id, $type, $link_organization_id);
    }
}
