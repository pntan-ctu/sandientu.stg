<?php

namespace App\Http\Controllers\API;

use App\Authors\AbilityName;
use App\Authors\ManageOrganizationAuthor;
use App\Models\Infringement;
use App\Models\InfringementCategory;
use App\Models\Organization;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfringementController extends Controller
{
    protected $orgAuthor;

    public function __construct()
    {
        $this->orgAuthor = new ManageOrganizationAuthor();
    }

    public function checkPermission($infringement)
    {
        $infringementOrg = null;
        if ($infringement->infringementable_type == Organization::class) {
            $infringementOrg = $infringement->infringementable;
        }

        if ($infringement->infringementable_type == Product::class) {
            $infringementOrg = $infringement->infringementable->organization;
        }

        $this->orgAuthor->canManage($infringementOrg);
    }

    /*
     * dùng cho cơ quan quản lý
     * truyền tham số page để lấy dữ liệu theo trang
     * $type: Organization, Product, User
     * $request->search: text input to search
     */
    public function searchManage($type, Request $request)
    {
        $this->orgAuthor->canDo();
        $search = $request->search ?? "";
        $status = $request->status ?? -1;
        $pageSize = $request->pageSize ?? 10;
        $webManageInfriCtr = new \App\Http\Controllers\Manage\InfringementController();
        $model = $webManageInfriCtr->preGetData($type, $search, $status);
        return $model->orderBy("infringements.created_at", "desc")->paginate($pageSize);
    }

    /*
     * dùng cho cơ quan quản lý
     * $type: Organization, Product, User
     */
    public function showManage($type, $infringement_id)
    {
        $webManageInfriCtr = new \App\Http\Controllers\Manage\InfringementController();
        $search = "";
        $status = -1;
        $infringement = $webManageInfriCtr->preGetData($type, $search, $status, $infringement_id)->get();
        $aXuLy = [];
        if ($type == 'Organization') {
            $aXuLy = [
                ['name' => 'Đang hoạt động', 'active' => 1],
                ['name' => 'Ngừng hoạt động', 'active' => 2],
                ['name' => 'Khóa do vi phạm', 'active' => 3],
            ];
        }

        if ($type == 'Product') {
            $aXuLy = [
                ['name' => 'Mở khóa', 'active' => 1],
                ['name' => 'Khóa', 'active' => 2]
            ];
        }

        if ($type == 'User') {
            $aXuLy = [
                ['name' => 'Khóa', 'active' => 0],
                ['name' => 'Mở khóa', 'active' => 1]
            ];
        }
        return ['infringement' => $infringement, 'xu_ly' => $aXuLy];
    }

    /*
     * dùng cho cơ quan quản lý
     * $request->status chia làm 3 trường hợp
     * 1. Org: 1 - Đang hoạt động, 2 - Ngưng hoạt động, 3 - Khóa do vi phạm
     * 2. Product: 1 - Mở khóa, 2 - Khóa
     * 3. User: 1 - Mở khóa, 0 - Khóa
     */
    public function performManage($type, $infringement_id, Request $request)
    {
        $status = $request->status;
        $webManageInfriCtr = new \App\Http\Controllers\Manage\InfringementController();
        $webManageInfriCtr->perform($infringement_id, $status);
        return ['success' => true];
    }
}
