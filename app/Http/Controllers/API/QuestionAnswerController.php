<?php

namespace App\Http\Controllers\API;

use App\Authors\AbilityName;
use App\Authors\ManageOrganizationAuthor;
use App\Models\Organization;
use App\Models\Product;
use App\Models\QuestionAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class QuestionAnswerController extends Controller
{
    protected $orgAuthor;

    public function __construct()
    {
        $this->orgAuthor = new ManageOrganizationAuthor();
    }

    public function checkPermission($questionAnswer)
    {
        $org = null;
        if ($questionAnswer->question_answerable_type == Organization::class) {
            $org = $questionAnswer->questionAnswerable;
        }

        if ($questionAnswer->question_answerable_type == Product::class) {
            $org = $questionAnswer->questionAnswerable->organization;
        }

        $this->orgAuthor->canManage($org);
    }

    /*
     * dùng cho cơ quan quản lý
     * truyền tham số page để lấy dữ liệu theo trang
     * $type: products, organizations
     * $request->search: text input to search
     */
    public function searchQA($type, Request $request)
    {
        $pageSize = $request->pageSize ?? 10;
        $webQuestionAnswerCtr = new \App\Http\Controllers\Manage\QuestionAnswerController();
        $myRequest = new Request();
        $myRequest->keyword = $request->search ?? "";
        $myRequest->type_search = $type;
        $myRequest->active_search = $request->active ?? -1;
        $model = $webQuestionAnswerCtr->getPreData($myRequest);
        return $model->paginate($pageSize);
    }

    /*
     * dùng cho cơ quan quản lý
     * $request->status chia làm 2 trường hợp
     * 1. type = organizations: 1 - Đang hoạt động, 2 - Ngưng hoạt động, 3 - Khóa do vi phạm
     * 2. type = products: 1 - Mở khóa, 2 - Khóa
     */
    public function resolveQA($questionAnswerId, Request $request)
    {
        $status = $request->status;
        $questionAnswer = QuestionAnswer::find($questionAnswerId);
        $questionAnswer->status = $status;
        $this->checkPermission($questionAnswer);
        $questionAnswer->updated_by = getUserId();
        $questionAnswer->save();
        return ['success' => true];
    }
}
