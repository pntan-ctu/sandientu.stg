<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Web\ProductsWebController;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\ProductBussiness;
use App\Business\ProductCategoryBusiness;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, Request $request)
    {
        $input = $request->all();
        
        $perPage = isset($input["perPage"])?(int)$input["perPage"]:10;
        $product_category_id = isset($input["product_category_id"])?(int)$input["product_category_id"]:0;
        $product_name = isset($input["product_name"])?$input["product_name"]:"";
        $active=isset($input["status"])?(int)$input["status"]:-1;
        
        return ProductBussiness::getListByOrganization(
            $organization_id,
            $product_category_id,
            $product_name,
            $active,
            $perPage
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $returnVal = array();
        
        $returnVal["categories"]= ProductCategoryBusiness::getTree();
        
        return $returnVal;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organization_id, Request $request)
    {
        $input = $request->all();
        $input["organization_id"]=$organization_id;
        $files = $request->file("file");
        if (is_array($files)) {
            $input["images"]=$files;
        }
        return ProductBussiness::save($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, Product $product)
    {
        $product->organization_id=$organization_id;
        
        $product->productCategory ;
        
        $product->productImages;
        return $product;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit($organization_id, Product $product)
    {
         $returnVal = array();
         $product->organization_id=$organization_id;
         $returnVal["categories"]= ProductCategoryBusiness::getTree();
         $product->productImages;
         $product->productLinks;
         $returnVal["product"]=$product;
         
         return $returnVal;
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, Request $request, Product $product)
    {
        $input = $request->all();
        $input["organization_id"]=$organization_id;
        $input["id"]=$product->id;
        $files = $request->file("file");
        if (is_array($files)) {
            $input["images"]=$files;
        }
        return ProductBussiness::save($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, Product $product)
    {
        $product["organization_id"]=$organization_id;
        return ProductBussiness::delete($product);
    }
    
    
    public function getInfoDetail($organization_id, $product_id)
    {
        
        return ProductBussiness::getInfoDetails($organization_id, $product_id);
    }
    
    public function updateInfoDetail($organization_id, $product_id, Request $request)
    {
        $input = $request->all();
        $input["product_id"]=$product_id;
        return ProductBussiness::saveProductInfoDetails($input);
    }

    public function getProductInfoDetail($path)
    {
        $productController = new ProductsWebController();
        $productInfo = $productController->getDataDetailOfProduct($path);
        unset($productInfo['labelProductInfos']);
        unset($productInfo['productInfos']);
        unset($productInfo['labelOrgInfos']);
        unset($productInfo['orgInfos']);
        return $productInfo;
    }
}
