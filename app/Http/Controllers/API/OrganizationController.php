<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Web\OrganizationsWebController;
use App\Models\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\OrganizationBusiness;
use Illuminate\Support\Facades\DB;

class OrganizationController extends Controller
{
    public function getGeneralInfo($organization_id)
    {
        return OrganizationBusiness::getGeneralInfo($organization_id);
    }

    public function saveGeneralInfo($organization_id, Request $request)
    {
        $input = $request->all();
        $input["organization_id"]=$organization_id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveGeneralInfo($input);
    }

    public function getIntroductionInfo($organization_id)
    {
        return OrganizationBusiness::getIntroductionInfo($organization_id);
    }

    public function saveIntroductionInfo($organization_id, Request $request)
    {
        $input = $request->all();
        $input["organization_id"]=$organization_id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveIntroductionInfo($input);
    }

    public function delete($organization_id, Request $request)
    {
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->delete($organization_id);
    }

    public function search(Request $request)
    {
        $perPage = $request->perPage ?? 10;
        $keyword = $request->search ?? "";
        $status = $request->status ?? -1;
        $orgTypeId = $request->organization_type_id ?? -1;
        $orgs = Organization::where('organization_type_id', '>=', 100);
        if (strlen($keyword) > 0) {
            $orgs = $orgs->where(function ($query) use ($keyword) {
                $query->where('name', 'like', '%' . $keyword . '%')
                    ->orWhere('address', 'like', '%' . $keyword . '%')
                    ->orWhere('tel', 'like', '%' . $keyword . '%');
            });
        }

        if ($status >= 0) {
            $orgs = $orgs->where('status', $status);
        }

        if ($orgTypeId > -1) {
            $orgs = $orgs->where('organization_type_id', $orgTypeId);
        }

        $orgs = $orgs->orderBy("name", 'asc')->paginate($perPage);
        return $orgs;
    }

    public function updateStatus(Request $request)
    {
        $orgBusiness = new OrganizationBusiness();
        $organization_id = $request->id;
        $status = $request->status;
        return $orgBusiness->updateStatus($organization_id, $status);
    }

    public function getOrganizationInfoDetail($path)
    {
        $organizationController = new OrganizationsWebController();
        return $organizationController->getOrganizationInfoDetail($path);
    }
}
