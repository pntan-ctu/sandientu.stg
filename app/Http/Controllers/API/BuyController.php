<?php

namespace App\Http\Controllers\API;

use App\Authors\AbilityName;
use App\Authors\OrgAuthor;
use App\Business\OrderBusiness;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Organization;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BuyController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_EXCHANGE;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);

        $status = $request->status ?? -1;
        $keyword = $request->keyword ?? '';
        $pageSize = $request->pageSize ?? 10;

        $orders = Order::join('organizations as a', 'orders.provider_organization_id', '=', 'a.id')
            ->where('orders.orderable_type', '=', Organization::class)
            ->where('orders.orderable_id', '=', $organization_id);

        if ($status > -1) {
            $orders = $orders->where('orders.status', '=', $status);
        } else {
            $orders = $orders->where('orders.status', '>', 0);
        }

        if (strlen($keyword) > 0) {
            $orders = $orders->where(function ($query) use ($keyword) {
                $query->where('orders.customer_name', 'like', '%' . $keyword . '%')
                    ->orWhere('orders.address', 'like', '%' . $keyword . '%')
                    ->orWhere('orders.email', 'like', '%' . $keyword . '%')
                    ->orWhere('orders.comment', 'like', '%' . $keyword . '%')
                    ->orWhere('a.name', 'like', '%' . $keyword . '%')
                    ->orWhere('a.address', 'like', '%' . $keyword . '%');
            });
        }

        $orders = $orders->selectRaw('orders.*, a.name as provider_name,
            a.address as provider_address, a.tel as provider_tel')
            ->orderBy('orders.order_date', 'desc')->paginate($pageSize);

        return $orders;
    }
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, $id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);

        $order = Order::find($id);
        if (isset($id) && $id) {
            $providerOrg = Organization::find($order["provider_organization_id"]);
            $data["status"] = "Chưa xử lý";
            if ($order->status == 2) {
                $data["status"] = "Đã xử lý";
            } elseif ($order->status == 3) {
                $data["status"] = "Đã hủy";
            } elseif ($order->status == 4) {
                $data["status"] = "Đã xác nhận";
            }
            $data["code"] = $order->code;
            $data["order_date"] = $order->order_date->format('d/m/Y H:i');
            $data["provider_organization_name"] = $providerOrg->name;
            $data["provider_organization_tel"] = $providerOrg->tel;
            $data["provider_organization_address"] = $providerOrg->address;
            $data["items"] = OrderItem::where("order_id", $id)->get();
        }
        return $data;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, Request $request, Order $order)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, $order_id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);

        $request = new Request();
        $request->id = $order_id;
        $buyController = new \App\Http\Controllers\Org\BuyController();
        return $buyController->huyOrder($organization_id, $request);
    }
    
    public function getCountNewOrders($organization_id)
    {
        return OrderBusiness::getCountNewOrders($organization_id);
    }

    public function confirmReceived($organization_id, $order_id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);

        $order = Order::find($order_id);

        //Check quyền
        if ($order == null
            || $order->orderable_type != Organization::class
            || $order->orderable_id != $organization_id) {
            throw new AuthorizationException();
        }

        if ($order->status == 2) {
            $order->status = 4;
            $order->save();
            return ['success' => true, 'msg' => 'Cập nhật thành công'];
        } else {
            return ['success' => false, 'msg' => 'Trạng thái đơn hàng đã thay đổi'];
        }
    }
}
