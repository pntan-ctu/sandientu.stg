<?php

namespace App\Http\Controllers\API;

use App\Models\Advertising;
use App\Models\AdvertisingImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\AdvertisingBussiness;
use App\Business\ProductCategoryBusiness;
use App\Business\RegionBusiness;
use App\Models\Product;
use Illuminate\Support\Facades\DB;

class AdvertisingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, Request $requets)
    {
        $input = $requets->all();
        
        $search = isset($input["search"])?$input["search"]:"";
        $status = isset($input["status"])?(int)$input["status"]:-1;
        $perPage = isset($input["perPage"])?(int)$input["perPage"]:10;
        
        return AdvertisingBussiness::search($search, $status, $perPage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($organization_id)
    {
        $returnVal = array();
        
        $returnVal["product_categories"]= ProductCategoryBusiness::getTree();
        $returnVal["regions"]=RegionBusiness::getTreeWithParent(RegionBusiness::REGIONIDROOT);
        $returnVal["product_links"] = Product::where('organization_id', getOrgId())
                ->get(['id', 'name', 'product_category_id']);
        
        return $returnVal;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organization_id, Request $request)
    {
        $input = $request->all();
        $files = $request->file("file");
        if (is_array($files)) {
            $input["images"]=$files;
        }
        return AdvertisingBussiness::save($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Advertising  $advertising
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, Advertising $advertising)
    {
        $advertising->advertisingImages;
        $advertising->advertisingCategory;
        $advertising->region;
        $advertising->productCategory;
        return $advertising;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Advertising  $advertising
     * @return \Illuminate\Http\Response
     */
    public function edit($organization_id, Advertising $advertising)
    {
        $returnVal = array();
        $returnVal["categories"]= ProductCategoryBusiness::getTree();
        $advertising->advertisingImages;
        $advertising->advertisingCategory;
        $advertising->region;

        $returnVal["product_categories"]= ProductCategoryBusiness::getTree();
        $returnVal["regions"]=RegionBusiness::getTreeWithParent(RegionBusiness::REGIONIDROOT);
        $returnVal["product_links"] = Product::where('organization_id', getOrgId())
                ->get(['id', 'name', 'product_category_id']);
        $returnVal["advertising"]=$advertising;
        return $returnVal;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Advertising  $advertising
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, Request $request, Advertising $advertising)
    {
        $input = $request->all();
        $input["id"]=$advertising->id;
        $files = $request->file("file");
        if (is_array($files)) {
            $input["images"]=$files;
        }
        return AdvertisingBussiness::save($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Advertising  $advertising
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, Advertising $advertising)
    {
        return AdvertisingBussiness::delete($advertising);
    }

    /*
     * dùng cho cơ quan quản lý
     */
    public function searchManage(Request $request)
    {
        $search = $request->search ?? "";
        $status = $request->status ?? -1;
        $perPage = $request->perPage ?? 10;
        $advBusiness = new AdvertisingBussiness();
        return $advBusiness->searchManage($search, $status, $perPage);
    }

    /*
     * dùng cho cơ quan quản lý
     */
    public function showManage($advertising_id)
    {
        $result = [];
        $advertising = Advertising::join('advertising_categories as b', 'b.id', '=', 'advertisings.category_id')
            ->join('product_categories as c', 'c.id', '=', 'advertisings.product_category_id')
            ->join('users', 'users.id', '=', 'advertisings.created_by')
            ->where('advertisings.id', '=', $advertising_id)
            ->select([DB::raw('advertisings.*'), 'b.name as advertising_category_name',
                'c.name as product_category_name', 'users.name as nguoi_dang'])
            ->get();
        $result['advertising'] = $advertising;
        $images = AdvertisingImage::where('advertising_id', '=', $advertising_id)->get();
        $result['images'] = $images;
        return $result;
    }

    /*
     * dùng cho cơ quan quản lý
     */
    public function deleteManage($advertising_id)
    {
        $advBusiness = new AdvertisingBussiness();
        return $advBusiness->deleteManage($advertising_id);
    }

    /*
     * dùng cho cơ quan quản lý
     * $request->status: 0 - chờ duyệt, 1 - duyệt, 2 - không duyệt
     */
    public function setStatusManage($advertising_id, Request $request)
    {
        $advBusiness = new AdvertisingBussiness();
        $status = $request->status;
        return $advBusiness->setStatusManage($advertising_id, $status);
    }
}
