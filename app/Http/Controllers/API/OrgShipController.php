<?php

namespace App\Http\Controllers\API;

class OrgShipController extends OrgPayShipController
{
    public function __construct()
    {
        $this->type = "ship";
    }
}
