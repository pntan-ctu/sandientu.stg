<?php

namespace App\Http\Controllers\API;

use App\Models\Certificate;
use App\Models\CertificateCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\OrganizationBusiness;

class ProductCertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, $product_id, Request $request)
    {
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->getProductCertificates($organization_id, $product_id);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($organization_id, $product_id, Request $request)
    {
        $result = [];
        $certificateCategory = CertificateCategory::where('type', 1)->get();
        $result['certificate_categories'] = $certificateCategory;
        $result['product_id'] = $product_id;
        return $result;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organization_id, $product_id, Request $request)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $input["product_id"] = $product_id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveProductCertificate($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Certificate $cer
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, $product_id, Certificate $cer)
    {
        $result = [];

        $certificateCategory = CertificateCategory::where('type', 1)->get();
        $result['certificate_categories'] = $certificateCategory;
        $result['product_id'] = $product_id;

        foreach ($certificateCategory as $cerCate) {
            if ($cerCate->id == $cer->certificate_category_id) {
                $cer->certificate_category_name = $cerCate->name;
                break;
            }
        }
        $result['certificate'] = $cer;

        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Certificate $cer
     * @return \Illuminate\Http\Response
     */
    public function edit($organization_id, $product_id, Certificate $cer)
    {
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Certificate $cer
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, $product_id, Request $request, Certificate $cer)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $input["product_id"] = $product_id;
        $input["id"] = $cer->id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveProductCertificate($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Certificate $cer
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, $product_id, Certificate $cer)
    {
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->deleteProductCertificate($cer, $organization_id);
    }
}
