<?php

namespace App\Http\Controllers\API;

use App\Models\OrganizationPayType;
use App\Models\PayType;

class OrgPayController extends OrgPayShipController
{
    public function __construct()
    {
        $this->type = "pay";
    }
}
