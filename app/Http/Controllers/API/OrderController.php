<?php

namespace App\Http\Controllers\API;

use App\Authors\AbilityName;
use App\Authors\OrgAuthor;
use App\Http\Controllers\Org\OrdersController;
use App\Models\Order;
use App\Business\OrderBusiness;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class OrderController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_EXCHANGE;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);

        $input = $request->all();
        $perPage = $input["perPage"] ?? 10;
        $search = $input["search"] ?? "";
        $status= $input["status"] ?? 0;
        return OrderBusiness::getListByOrganization(
            $organization_id,
            $search,
            $status,
            $perPage
        );
    }
    
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, Order $order)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);

        $order->provider_organization_id=$organization_id;
        $order->organization;
        $order->orderItems;
        
        return $order;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, Request $request, Order $order)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);

        $input = $request->all();
        $input["organization_id"]=$organization_id;
        $input["id"]=$order->id;
        
        return OrderBusiness::save($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, $order_id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organization_id, $this->abilityName);

        $request = new Request();
        $request->id = $order_id;
        $ordersController = new OrdersController();
        return $ordersController->huyOrder($organization_id, $request);
    }
    
    public function getCountNewOrders($organization_id)
    {
        return OrderBusiness::getCountNewOrders($organization_id);
    }

    public function actOrder($organization_id, Request $request)
    {
        $ordersController = new OrdersController();
        $order = $ordersController->actOrder($organization_id, $request);
        if (isset($order['id'])) {
            return ['success' => true, 'msg' => 'Cập nhật thành công'];
        } else {
            return ['success' => false, 'msg' => 'Không thể thực hiện'];
        }
    }
}
