<?php

namespace App\Http\Controllers\API;

use App\Models\OrganizationPayType;
use App\Models\OrganizationShipType;
use App\Models\ShipType;
use App\Models\PayType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\OrganizationBusiness;

class OrgPayShipController extends Controller
{
    protected $type;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, Request $request)
    {
        $input = $request->all();
        $perPage = $input["perPage"] ?? 10;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->getPayShipTypes($organization_id, $this->type, $perPage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = $this->type == "pay" ? PayType::class : ShipType::class;
        $types = $model::select('*')->orderBy('id', 'asc')->get();
        return $types;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organization_id, Request $request)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $input["type"] = $this->type;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveOrgShipPayType($input);
    }

    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, $orgPayShipTypeId)
    {
        $result = [];
        if ($this->type == "pay") {
            $types = PayType::select('*')->orderBy('id', 'asc')->get();
            $result['pay-types'] = $types;
            $result['org-pay-type'] = OrganizationPayType::findOrFail($orgPayShipTypeId);
        } else {
            $types = ShipType::select('*')->orderBy('id', 'asc')->get();
            $result['ship-types'] = $types;
            $result['org-ship-type'] = OrganizationShipType::findOrFail($orgPayShipTypeId);
        }
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ShipType $shiptype
     * @return \Illuminate\Http\Response
     */
    public function edit($organization_id, ShipType $shiptype)
    {
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, Request $request, $orgPayShipTypeId)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $input["id"] = $orgPayShipTypeId;
        $input["type"] = $this->type;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveOrgShipPayType($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ShipType $shiptype
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, $orgPayShipTypeId)
    {
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->deleteOrgShipPayType($organization_id, $orgPayShipTypeId, $this->type);
    }
}
