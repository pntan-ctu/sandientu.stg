<?php
namespace App\Http\Controllers\API;

use App\Business\OrganizationBusiness;
use App\Business\ProductBussiness;
use App\Constants;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\AdsWebController;
use App\Http\Controllers\Web\AreaWebController;
use App\Http\Controllers\Web\CommercialCenterWebController;
use App\Http\Controllers\Web\ContactController;
use App\Http\Controllers\Web\HelpWebController;
use App\Http\Controllers\Web\NewsGroupsWebController;
use App\Http\Controllers\Web\NewsWebController;
use App\Http\Controllers\Web\ProductCategoryWebController;
use App\Http\Requests\StoreContactRequest;
use App\Models\Advertising;
use App\Models\Area;
use App\Models\CertificateCategory;
use App\Models\CommercialCenter;
use App\Models\Document;
use App\Models\DocumentField;
use App\Models\DocumentOrgan;
use App\Models\DocumentType;
use App\Models\NewsArticle;
use App\Models\OrganizationLevel;
use App\Models\ProductCategory;
use App\Models\SlideShow;
use App\Models\Weblink;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function search(Request $request)
    {
        $lat = $request->lat ?? "";
        $long = $request->long ?? "";
        $type = $request->type ?? "";
        $text = $request->text ?? "";
        $choose = $request->choose ?? "";
        $homeWebController = new \App\Http\Controllers\HomeController();
        $data = $homeWebController->searchBusiness($type, $text, $choose, $lat, $long);
        return $data;
    }

    public function homePageData(Request $request)
    {
        $numOfProduct = $request->num_of_product ?? 10;
        $numOfAds = $request->num_of_adv ?? 6;
        $numOfNews = $request->num_of_news ?? 5;
        $numOfOrganizations = $request->num_of_organization ?? 5;
        $numOfTrademark = $request->num_of_trademark ?? 12;
        $numOfDocument = $request->num_of_document ?? 10;
        $homePageData = [];

        $slides = SlideShow::where('active', '=', 1)
            ->select(['id', 'title', 'image_path'])->orderBy("no", "asc")->get();
        $homePageData['slides'] = $slides;

        $productCategories = ProductCategory::whereNull('parent_id')->select(['id', 'path', 'name', 'icon'])->get();
        $homePageData['product_categories'] = $productCategories;

        $productsSaleoff = ProductBussiness::getListForView()->whereNotNull('price_sale')
            ->whereRaw('price_sale < price')->orderBy('rank', 'desc')->take($numOfProduct)->get();
        $homePageData['products_saleoff'] = $productsSaleoff;

        $productsCare = ProductBussiness::getListForView()
            ->orderBy('count_view', 'desc')->take($numOfProduct)->get();
        $homePageData['products_care'] = $productsCare;

        $advertising = Advertising::where('status', '=', 1)
            ->where(function ($query) {
                $query->where('to_date', '>=', date('Y-m-d'))->orWhereNull('to_date');
            })->orderBy('created_at', 'desc')->take($numOfAds)->get();
        $homePageData['advertising'] = $advertising;

        $organization = OrganizationBusiness::getListForView()
            ->orderBy('rank', 'desc')->take($numOfOrganizations)->get();
        $homePageData['organizations'] = $organization;

        $news = NewsArticle::where('status', '=', 1)
            ->where('group_id', '<>', Constants::PREFIX_NEWS_GROUP_ID)
            ->orderBy("created_at", "desc")->take($numOfNews)->get();
        $homePageData['news'] = $news;

        $trademarks = Weblink::where('active', '=', 1)->where('group_id', '=', 2)
            ->select(['id', 'title', 'img_path', 'url'])->take($numOfTrademark)->get();
        $homePageData['trademarks'] = $trademarks;

        $documents = Document::where('status', '=', 1)->orderBy('sign_date', 'desc')->take($numOfDocument)->get();
        $homePageData['documents'] = $documents;

        return $homePageData;
    }

    public function getAdsDetail($path)
    {
        $adsWebController = new AdsWebController();
        return $adsWebController->getAdsInfo($path);
    }

    public function sendContact(StoreContactRequest $request)
    {
        $contactWebController = new ContactController();
        return $contactWebController->store($request, 1);
    }

    public function getListAreas()
    {
        $areas = Area::orderBy('created_at', 'desc')->paginate(10);
        return $areas;
    }

    public function getAreaInfo($path)
    {
        $areaWebController = new AreaWebController();
        $data = $areaWebController->getAreaInfo($path);
        return $data;
    }

    public function getProducts(Request $request)
    {
        $productCategoryWebController = new ProductCategoryWebController();
        $products = $productCategoryWebController->getProducts($request);
        $rs['products'] = $products;
        $categories = [
            ['path' => 'danh-muc-san-pham', 'name' => 'Tất cả ('.$products['total'].')', 'icon' => null],
            ['path' => 'danh-muc-san-pham/khuyen-mai', 'name' => 'Sản phẩm khuyến mại', 'icon' => null],
            ['path' => 'danh-muc-san-pham/moi-ban', 'name' => 'Sản phẩm mới', 'icon' => null],
            ['path' => 'danh-muc-san-pham/mua-nhieu', 'name' => 'Sản phẩm mua nhiều', 'icon' => null],
            ['path' => 'danh-muc-san-pham/quan-tam', 'name' => 'Sản phẩm quan tâm', 'icon' => null]
        ];
        $productCategories = ProductCategory::whereNull('parent_id')
            ->select(['path', 'name', 'icon'])->get()->toArray();
        $rs['children_categories'] = array_merge($categories, $productCategories);
        $rs['org_levels'] = OrganizationLevel::all();
        $rs['certificate_categories'] = CertificateCategory::where('type', 1)->select(['id', 'name', 'icon'])->get();
        return $rs;
    }

    public function getProductInGroup(Request $request, $path)
    {
        $productCategoryWebController = new ProductCategoryWebController();
        $products = $productCategoryWebController->getProductInGroup($request, $path);
        $rs['products'] = $products;

        $productCategory = ProductCategory::where('path', '=', $path)->first();
        $productCategories = ProductCategory::where('parent_id', '=', $productCategory->id ?? 0)->get();
        $rs['children_categories'] = $productCategories;
        $rs['org_levels'] = OrganizationLevel::all();
        $rs['certificate_categories'] = CertificateCategory::where('type', 1)->select(['id', 'name', 'icon'])->get();
        return $rs;
    }

    public function getCommercialCenters(Request $request)
    {
        $pageSize = $request->pageSize ?? 5;
        $commercialCenters = CommercialCenter::orderBy('created_at', 'desc')->paginate($pageSize);
        return $commercialCenters;
    }

    public function getCommercialCenterInfo(Request $request, $path)
    {
        $commercialWebController = new CommercialCenterWebController();
        return $commercialWebController->getCommercialCenterInfo($path);
    }

    public function getNews(Request $request)
    {
        $pageSize = $request->pageSize ?? 5;
        $newGroupWebController = new NewsGroupsWebController();
        $news = $newGroupWebController->getNews($pageSize);
        return $news;
    }

    public function getNewsDetail(Request $request, $path)
    {
        $newsWebController = new NewsWebController();
        return $newsWebController->getNewsDetail($path);
    }

    public function searchDocuments(Request $request)
    {
        $rs = [];
        $docType = DocumentType::all();
        $rs['docType'] = $docType;
        $docOrgan = DocumentOrgan::all();
        $rs['docOrgan'] = $docOrgan;
        $docField = DocumentField::all();
        $rs['docField'] = $docField;
        $pageSize = $request->pageSize ?? 10;
        $document = Document::select(['id', 'number_symbol', 'quote','path', 'sign_date']);
        if ($request->filled('keyword')) {
            $keyword = $request->input('keyword');
            $document = $document->where(function ($query) use ($keyword) {
                $query->where('number_symbol', 'like', '%' . $keyword . '%')
                    ->orWhere('quote', 'like', '%' . $keyword . '%');
            });
        }
        if ($request->filled('idOrgan') && $request->input('idOrgan') != 0) {
            $document = $document->where('document_organ_id', '=', request('idOrgan'));
        }
        if ($request->filled('idType') && $request->input('idType') != 0) {
            $document = $document->where('document_type_id', '=', request('idType'));
        }
        if ($request->filled('idField') && $request->input('idField') != 0) {
            $document = $document->where('document_field_id', '=', request('idField'));
        }
        $document = $document->orderBy('sign_date', 'desc')->paginate($pageSize);
        $rs['document'] = $document;
        return $rs;
    }

    public function getDocumentDetail($path)
    {
        $doc = Document::where('path', $path)->first();
        if (!isset($doc)) {
            return "Không tồn tại văn bản";
        }
        return $doc;
    }

    public function getHelpData(Request $request)
    {
        $helpWebController = new HelpWebController();
        $helpData = $helpWebController->getHelpData($request);
        return $helpData;
    }

    public function getHelpDetail($path)
    {
        $helpWebController = new HelpWebController();
        $helpDetail = $helpWebController->getHelpDetail($path);
        return $helpDetail;
    }
}
