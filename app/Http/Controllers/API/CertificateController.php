<?php

namespace App\Http\Controllers\API;

use App\Models\Certificate;
use App\Models\CertificateCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\OrganizationBusiness;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, Request $request)
    {
        $input = $request->all();
        $perPage = $input["perPage"] ?? 10;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->getCertificates($organization_id, $perPage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $certificateCategory = CertificateCategory::where('type', 0)->get();
        return $certificateCategory;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organization_id, Request $request)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveCertificate($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, Certificate $certificate)
    {
        $result = [];
        $certificateCategory = CertificateCategory::where('type', 0)->get();
        $result['certificate_category'] = $certificateCategory;
        foreach ($certificateCategory as $cerCate) {
            if ($cerCate->id == $certificate->certificate_category_id) {
                $certificate->certificate_category_name = $cerCate->name;
                break;
            }
        }
        $result['certificate'] = $certificate;
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function edit($organization_id, Certificate $certificate)
    {
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, Request $request, Certificate $certificate)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $input["id"] = $certificate->id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveCertificate($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, Certificate $certificate)
    {
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->deleteCertificate($certificate, $organization_id);
    }
}
