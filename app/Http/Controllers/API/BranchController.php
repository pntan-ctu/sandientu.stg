<?php

namespace App\Http\Controllers\API;

use App\Business\RegionBusiness;
use App\Models\Branch;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\OrganizationBusiness;

class BranchController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organization_id, Request $request)
    {
        $input = $request->all();
        $perPage = $input["perPage"] ?? 10;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->getBranches($organization_id, $perPage);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tree = RegionBusiness::getTreeWithParent(RegionBusiness::REGIONIDROOT);
        return $tree;
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organization_id, Request $request)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveBranch($input);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function show($organization_id, Branch $branch)
    {
        $result = [];
        $result['branch'] = $branch;
        $tree = RegionBusiness::getTreeWithParent(RegionBusiness::REGIONIDROOT);
        $result['regions'] = $tree;
        return $result;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function edit($organization_id, Branch $branch)
    {
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function update($organization_id, Request $request, Branch $branch)
    {
        $input = $request->all();
        $input["organization_id"] = $organization_id;
        $input["id"] = $branch->id;
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->saveBranch($input);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Branch $branch
     * @return \Illuminate\Http\Response
     */
    public function destroy($organization_id, Branch $branch)
    {
        $orgBusiness = new OrganizationBusiness();
        return $orgBusiness->deleteBranch($branch, $organization_id);
    }
}
