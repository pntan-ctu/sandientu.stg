<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Branch;
use App\Models\CommercialCenter;
use App\Models\Organization;
use App\Business\MapsBussiness;

class MapsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $arOrg = array();
        $keyword = isset($request->keyword) ? $request->keyword :"";
        $orgtype = isset($request->orgtype) ? $request->orgtype: "-1";
        $arOrg = MapsBussiness::getMapsLocation($orgtype, $keyword);
        return view('web.map.index', ["organization100"=> json_encode($arOrg),"orgtype"=>$orgtype,"keyword"=>$keyword]);
    }
    
    public function search(Request $request)
    {
        //$arOrg = array();
        //$arOrg = MapsBussiness::getMapsLocation();
        
        //return view('web.map.index', ["organization100"=> json_encode($arOrg)]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        return view('manage/home');
    }
}
