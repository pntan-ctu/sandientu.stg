<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class QRCodeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index()
    {
        if (request()->filled('name')) {
            $name = request()->name;
        } else {
            $name = config('app.url', '') . '/get-app';
        }

        if (strrpos($name, 'nongsanantoanthanhhoa.vn') === false) {
            $qrCode = QrCode::errorCorrection('H')->format('png')->size(300)
                    //->merge('/public/android-icon-72x72.png')->encoding('UTF-8')
                    ->generate($name);
        } else {
             $qrCode = QrCode::errorCorrection('H')->format('png')->size(300)
                     ->merge('/public/android-icon-72x72.png')->encoding('UTF-8')
                     ->generate($name);
        }

        return view('web.qr_code.index', ['qrCode' => $qrCode, 'name' => $name]);
    }
    
    public function show()
    {
        return view('web.qr_code.show');
    }
}
