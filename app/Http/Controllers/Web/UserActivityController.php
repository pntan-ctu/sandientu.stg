<?php

namespace App\Http\Controllers\Web;

use App\Models\Order;
use App\Models\Organization;
use App\Models\Product;
use App\Models\QuestionAnswer;
use App\Models\Rating;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserActivityController extends Controller
{
    
    public function index(Request $request)
    {
        $currentOrgId = getOrgId();
        $user_name = request('username');
        $user =  User::findForUrlActivity($user_name);
        $user_id = $user->id;
        $itemPerPage = 10;

        /************ RATINGS ************/
        $ratings = Rating::where('created_by', $user_id)
            ->orderBy('created_at', 'desc')
            ->paginate($itemPerPage, ['*'], 'page_rating');
        $count = 0;
        $object = null;
        foreach ($ratings as $rt) {
            if ($rt['ratingable_type'] == "App\Models\Organization") {
                $object = Organization::where('id', $rt['ratingable_id'])
                    ->select(['name', 'path'])->get();
                $ratings[$count]['object_path'] = isset($object[0]['path']) ? "/shop/".$object[0]['path'] : "";
            } else {
                $object = Product::where('id', $rt['ratingable_id'])
                    ->select(['name', 'path'])->get();
                $ratings[$count]['object_path'] = isset($object[0]['path']) ? "/san-pham/".$object[0]['path'] : "";
            }

            $ratings[$count]['object_name'] = $object[0]['name'] ?? "";
            $count++;
        }

        /************ Q & A ************/
        $questionAnswers = QuestionAnswer::where('created_by', $user_id)
            ->orderBy('created_at', 'desc')
            ->paginate($itemPerPage, ['*'], 'page_qa');
        $count = 0;
        $object = null;
        foreach ($questionAnswers as $qa) {
            if ($qa['question_answerable_type'] == "App\Models\Organization") {
                $object = Organization::where('id', $qa['question_answerable_id'])
                    ->select(['name', 'path'])->get();
                $questionAnswers[$count]['object_path'] = isset($object[0]['path']) ? "/shop/".$object[0]['path'] : "";
            } else {
                $object = Product::where('id', $qa['question_answerable_id'])
                    ->select(['name', 'path'])->get();
                $questionAnswers[$count]['object_path']
                    = isset($object[0]['path']) ? "/san-pham/".$object[0]['path'] : "";
            }

            $questionAnswers[$count]['object_name'] = $object[0]['name'] ?? "";
            $count++;
        }

        $orderInfo = Order::where('orderable_id', '=', $user_id)
            ->where('orderable_type', '=', 'App\Models\User')
            ->select([DB::raw("sum(if(DATEDIFF(now(), order_date) <= 365,1,0)) AS total_order, 
                sum(if(status=4 and DATEDIFF(now(), order_date) <= 365,1,0)) as success_order,
                sum(if(provider_organization_id=$currentOrgId,1,0)) as total_org_order,
                sum(if(provider_organization_id=$currentOrgId and status=4,1,0)) as success_org_order")])
            ->get();

        $org = Organization::find($currentOrgId);
        $orgName = $org->name ?? "";
        // loại phản ánh vi phạm
        $loaiVipham = \App\Models\InfringementCategory::where("type", '=', 2)->get();
        return view('web.users.activity', [
            "User" => $user,
            'Comments' => $ratings,
            'QuestionAnswers' => $questionAnswers,
            'CurrentOrgId' => $currentOrgId,
            'OrderInfo' => $orderInfo,
            'OrgName' => $orgName,
            'loaiVipham' =>$loaiVipham,
        ]);
    }
}
