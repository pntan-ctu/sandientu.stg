<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class QABaseController extends Controller
{
    protected $model;

    public function index()
    {
    }

    public function store(Request $request)
    {
        $typeId = $request->typeId;
        $type = $this->model::find($typeId);
        $parentId = $request->questionId >0 ?$request->questionId : null;
        if (isset($type)) {
            $type->questionAnswers()->create([
                "parent_id" => $parentId,
                "content" => $request->qaContent,
                "status" => 0,
                "created_by" => getUserId(),
            ]);
            return response()->json(['message' => 'success', 'msg' => 'Lưu thông tin thành công!']);
        } else {
            return response()->json(['message' => 'error', 'msg' => 'Lưu thông tin không thành công!']);
        }
    }

    public function show($id)
    {
        //
    }

    public function update()
    {
    }

    public function destroy()
    {
    }
}
