<?php

namespace App\Http\Controllers\Web;

use App\Business\RegionBusiness;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserProfile;
use App\Models\Organization;
use App\Models\User;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\Region;
use App\Http\Requests\UpdateUserProfileRequest;

class ProfileController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userProfile = auth()->user()->userProfile;//UserProfile::find(Auth::user()->id);
        $provincial = Region::select(['*'])
                ->where('level', '=', 1)->get();
        $district = array();
        $commune = array();
        if (isset($userProfile->region)) {
            //$xaId = $userProfile->region->id;
            $huyenId = isset($userProfile->region->region) ? $userProfile->region->region->id : 0;
            $tinhId = isset($userProfile->region->region->region) ? $userProfile->region->region->region->id : 0;
            
            $district = Region::where('parent_id', '=', $tinhId)
                    ->where('level', '=', 2)->get();
            $commune = Region::where('parent_id', '=', $huyenId)
                    ->where('level', '=', 3)->get();
        }
        // return $userProfile;
        return view('web/users/index', ["userProfile" => $userProfile, 'provincial' => $provincial
                ,'district' => $district,'commune' => $commune]);
    }
    public function changeAvatar(Request $request)
    {
        $image = UploadFileUtil::uploadImage($request, 'avatar');
        $url = null;
        if (isset($image)) {
            $url = $image->path;
        }
        
        $user = UserProfile::find(getUserId());
        if (isset($user)) {
            $user->avatar = $url;
            $user->save();
        } else {
            $userProfile = new UserProfile();
            $userProfile->user_id = $request->userId;
            $userProfile->avatar = $url;
            $userProfile->save();
        }
        return response()->json(['status' => 'success', 'msg' => 'Thay đổi thành công !']);
    }

    public function update(UpdateUserProfileRequest $request)
    {
        $user = auth()->user();
        if (!isset($user)) {
            return response()->json(['status' => 'error', 'msg' => 'Không tìm thấy user !']);
        }
        $user->name = $request->name;
        $user->tel = $request->tel;
        $user->save();
        
        $userProfile = $user->userProfile;
        if (isset($userProfile)) {
            $userProfile->region_id = $request->commune;
            $userProfile->address = $request->address;
            $userProfile->date_of_birth = $request->dayOfBirth;
            $userProfile->sex = $request->sex;
            $userProfile->identification = $request->identification;
            $userProfile->identification_date = $request->identification_date;
            $userProfile->identification_by = $request->identification_by;
            $userProfile->save();
        } else {
            $userProfile = new UserProfile();
            $userProfile->user_id = $request->userId;
            $userProfile->region_id = $request->commune;
            $userProfile->address = $request->address;
            $userProfile->date_of_birth = $request->dayOfBirth;
            $userProfile->sex = $request->sex;
            $userProfile->identification = $request->identification;
            $userProfile->identification_date = $request->identification_date;
            $userProfile->identification_by = $request->identification_by;
            $userProfile->save();
        }
        return response()->json(['status' => 'success', 'msg' => 'Cập nhật thành công !']);
    }

    public function changePassword(Request $request)
    {
        $user = auth()->user();
        if (Hash::check($request->password_current, $user->password)) {
            $newPass = $request->password;
            $confirmPass = $request->password_confirmation;

            if (strlen($newPass) < 6) {
                return response()->json(['status' => 'error', 'msg' => 'Mật khẩu mới phải có ít nhất 6 ký tự']);
            }

            if ($newPass != $confirmPass) {
                return response()->json(['status' => 'error', 'msg' => 'Mật khẩu mới không giống nhau']);
            }

            $user->password = Hash::make($newPass);
            $user->password_reset = null;
            $user->save();
            return response()->json(['status' => 'success', 'msg' => 'Đổi mật khẩu thành công !']);
        } else {
            return response()->json(['status' => 'error', 'msg' => 'Mật khẩu hiện tại không đúng']);
        }
    }
    public function getDistrict($id)
    {
        $district = Region::where('parent_id', '=', $id)
                ->where('level', '=', 2)->get();
        //$opt = "<option value='0'>Chọn Quận/Huyện</option>";
        $opt = "<option class='a' level='0' value='0' path='Chọn Quận/Huyện'>Chọn Quận/Huyện</option>";
        foreach ($district as $item) {
            //$opt .= "<option value='".$item->id."'>".$item->name."</option>";
            $opt .= "<option class='a' level='0' value='{$item->id}' path='{$item->name}'>{$item->name}</option>";
        }
        return $opt;
    }
    public function getCommune($id)
    {
        $commune = Region::where('parent_id', '=', $id)
                ->where('level', '=', 3)->get();
//        $opt = "<option value='0'>Chọn Xã/Phường</option>";
        $opt = "<option class='a' level='0' value='0' path='Chọn Xã/Phường'>Chọn Xã/Phường</option>";
        foreach ($commune as $item) {
            $opt .= "<option class='a' level='0' value='{$item->id}' path='{$item->name}'>{$item->name}</option>";
        }
        return $opt;
    }
}
