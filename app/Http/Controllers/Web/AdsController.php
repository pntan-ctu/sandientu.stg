<?php

namespace App\Http\Controllers\Web;

use App\Business\AdvertisingCategoryBussiness;
use App\Http\Requests\StoreAdsRequest;
use App\Http\Requests\UpdateAdsRequest;
use App\Models\AdvertisingImage;
use App\Models\Organization;
use App\Models\Product;
use App\Models\ProductImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserProfile;
use App\Models\Advertising;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use App\Business\RegionBusiness;
use App\Models\ProductCategory;
use App\Http\Controllers\Manage\ProductCategoryController;
use App\Models\AdvertisingCategory;
use Yajra\DataTables\Facades\DataTables;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;
use Illuminate\Auth\Access\AuthorizationException;

class AdsController extends Controller
{
    protected $model;
    public $organizationId;

    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_EXCHANGE;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Request $request)
    {
        $this->organizationId = $request->route('organizationId', null);
        $this->orgAuthor = new OrgAuthor();

        if (isset($this->organizationId)) {
            $this->model = new Organization();
        } else {
            $this->model = new User();
        }
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (isset($this->organizationId)) {
            //Check quyền trong cơ sở
            $this->orgAuthor->canManageId($this->organizationId, $this->abilityName);

            return view('org/ads/index', ["ads" => null, 'organizationId' => $this->organizationId]);
        } else {
            return view('web/ads/index', ["ads" => null]);
        }
    }

    public function indexData(Request $request)
    {
        $this->organizationId = $request->route('organizationId', null);

        if (isset($this->organizationId)) {
            $this->model = new Organization();
            //Check quyền trong cơ sở
            $this->orgAuthor->canManageId($this->organizationId, $this->abilityName);
        } else {
            $this->model = new User();
        }

        $advertisingableId = $this->organizationId ?? getUserId();
        $type = $this->model::find($advertisingableId);

        $advertising = $type->advertisings()->with('advertisingCategoryName');
        return DataTables::eloquent($advertising)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('title', 'like', '%' . $keyword . '%')
                        ->orWhere('content', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('status') && $request->input('status') != -1) {
                $query->where('status', '=', request('status'));
            }
        })->make();
    }

    public function create()
    {
        if (isset($this->organizationId)) {
            //Check quyền trong cơ sở
            $this->orgAuthor->canManageId($this->organizationId, $this->abilityName);
        }

        //$ads = Advertising::find(1);
        $userProfile = UserProfile::find(Auth::user()->id);
        $regions = RegionBusiness::getTreeWithParent();
        $arr = ProductCategory::get(['id', 'parent_id', 'name']);
        $productCategoryController = new ProductCategoryController();
        $tree = $productCategoryController->getTreeCategory($arr);
        $adsCate = AdvertisingCategoryBussiness::renderRadio();
        $productLink = isset($this->organizationId) ?
            Product::where('organization_id', getOrgId())->get(['id', 'name', 'product_category_id']) : null;

        return view(
            'web.ads.create',
            ['regions' => $regions,
                'ads' => null,
                'tree' => $tree,
                'adsCate' => $adsCate,
                'userProfile' => $userProfile,
                'organizationId' => $this->organizationId,
                'layout' => isset($this->organizationId) ? 'org' : 'user',
                'productLink' => $productLink,
                'isMember' => isset($this->organizationId) ? false : true
            ]
        );
    }

    public function edit(Request $request, $id)
    {
        if (isset($this->organizationId)) {
            //Check quyền trong cơ sở
            $this->orgAuthor->canManageId($this->organizationId, $this->abilityName);

            $id = $request->route('advertising_org', null);
        }
        
        $ads = Advertising::find($id);

        if ($ads->status == 1) {
            return view('web.ads.error', ["msg" => "Tin đã duyệt không được sửa"]);
            //return response()->json(['status' => 'error', 'msg' => 'Tin đã duyệt không được sửa']);
        }

        //Check quyền
        if (isset($this->organizationId)) {
            if ($ads->advertisingable_type != Organization::class
                || $ads->advertisingable_id != $this->organizationId) {
                throw new AuthorizationException();
            }
        } else {
            if ($ads->advertisingable_type != User::class || $ads->advertisingable_id != getUserId()) {
                throw new AuthorizationException();
            }
        }

        $userProfile = UserProfile::find(Auth::user()->id);
        $regions = RegionBusiness::getTreeWithParent();
        $arr = ProductCategory::get(['id', 'parent_id', 'name']);
        $productCategoryController = new ProductCategoryController();
        $tree = $productCategoryController->getTreeCategory($arr);
        $adsCate = AdvertisingCategoryBussiness::renderRadio();
        $productLink = isset($this->organizationId) ?
            Product::where('organization_id', getOrgId())->get(['id', 'name', 'product_category_id']) : null ;
        $productImage = AdvertisingImage::where('advertising_id', '=', $id)->get();
        return view('web.ads.edit', ["userProfile" => $userProfile,
            'regions' => $regions,
            'ads' => $ads,
            'tree' => $tree,
            'adsCate' => $adsCate,
            'productLink' => $productLink,
            'productImage' => $productImage,
            'layout' => isset($this->organizationId) ? 'org' : 'user',
            'isMember' => isset($this->organizationId) ? false : true
        ]);
    }

    public function store(StoreAdsRequest $request)
    {
        if (isset($this->organizationId)) {
            //Check quyền trong cơ sở
            $this->orgAuthor->canManageId($this->organizationId, $this->abilityName);

            $type_id = $request->route('organizationId', null);
        } else {
            $type_id = getUserId();
        }

        $user = $this->model::find($type_id);
        $ads = $user->advertisings()->create([
            "category_id" => $request->category_id,
            'link_product_id' => $request->link_product_id == 0 ||
            $request->category_id != 2 ? null : $request->link_product_id,
            "product_category_id" => $request->product_category_id,
            "region_id" => $request->region_id,
            "title" => $request->title,
            "content" => $request->get('content'),
            "address" => $request->address,
            "tel" => $request->tel,
            "it5_mucluong" => $request->mucluong,
            "it5_quymoduan" => $request->quymoduan,
            "it5_cuocphithamquan" => $request->cuocphithamquan,
            "from_date" => now(),
            "to_date" => $request->to_date,
            "created_by" => Auth::user()->id,

        ]);
        $arr = [];
        if (isset($request->paths) && count($request->paths) > 0) {
            $paths = explode(",", $request->paths[0]);
            foreach ($paths as $item) {
                if (strlen($item) > 0) {
                    $arr[] = ['advertising_id' => $ads->id, 'image' => $item];
                }
            }
            if (count($arr) > 0) {
                AdvertisingImage::insert($arr);
            }
        }

        return response()->json(['status' => 'success', 'msg' => 'Lưu thông tin thành công!']);
    }

    public function show($id)
    {
    }

    public function update(UpdateAdsRequest $request, $id)
    {
        if (isset($this->organizationId)) {
            //Check quyền trong cơ sở
            $this->orgAuthor->canManageId($this->organizationId, $this->abilityName);

            $id = $request->route('advertising_org', null);
            $organizationId = $request->route('organizationId', null);
            $type = $this->model::find($organizationId);
        } else {
            $type = $this->model::find(Auth::user()->id);
        }

        $ads = Advertising::find($id);

        //Check quyền
        if (isset($this->organizationId)) {
            if ($ads->advertisingable_type != Organization::class
                || $ads->advertisingable_id != $this->organizationId) {
                throw new AuthorizationException();
            }
        } else {
            if ($ads->advertisingable_type != User::class || $ads->advertisingable_id != getUserId()) {
                throw new AuthorizationException();
            }
        }

        $ads->category_id = $request->category_id;
        $ads->link_product_id = $request->link_product_id == 0||
        $request->category_id != 2 ? null : $request->link_product_id;
        $ads->product_category_id = $request->product_category_id;
        $ads->region_id = $request->region_id;
        $ads->title = $request->title;
        $ads->content = $request->get('content');
        $ads->address = $request->address;
        $ads->tel = $request->tel;
        $ads->it5_mucluong = $request->mucluong;
        $ads->it5_quymoduan = $request->quymoduan;
        $ads->it5_cuocphithamquan = $request->cuocphithamquan;
        $ads->to_date = $request->to_date;
        $ads->updated_by = Auth::user()->id;
        if ($ads->status > 0) {
            $ads->status = 0;
        }
        $type->advertisings()->save($ads);

        AdvertisingImage::where('advertising_id', $id)->delete();
        $arr = [];
        if (isset($request->paths) && count($request->paths) > 0) {
            $paths = explode(",", $request->paths[0]);
            foreach ($paths as $item) {
                if (strlen($item) > 0) {
                    $arr[] = ['advertising_id' => $id, 'image' => $item];
                }
            }
            if (count($arr) > 0) {
                AdvertisingImage::insert($arr);
            }
        }

        return response()->json(['status' => 'success', 'msg' => 'Lưu thông tin thành công !']);
    }

    public function destroy(Request $request, $id)
    {
        if (isset($this->organizationId)) {
            //Check quyền trong cơ sở
            $this->orgAuthor->canManageId($this->organizationId, $this->abilityName);
            
            $id = $request->route('advertising_org', null);
        }

        $ads = Advertising::find($id);

        //Check quyền
        if (isset($this->organizationId)) {
            if ($ads->advertisingable_type != Organization::class
                || $ads->advertisingable_id != $this->organizationId) {
                throw new AuthorizationException();
            }
        } else {
            if ($ads->advertisingable_type != User::class || $ads->advertisingable_id != getUserId()) {
                throw new AuthorizationException();
            }
        }

        if ($ads) {
            $ads->delete();
            return response()->json(['status' => 'success', 'msg' => 'Xóa thông tin thành công']);
        }
        return response()->json(['status' => 'warning', 'msg' => 'Không tồn tại thông tin']);
    }

    public function getProductLink(Request $request)
    {
        $product_id = $request->get('product_id');
        $product = Product::where('id', $product_id)->get(['product_category_id', 'name']);
        $image = ProductImage::where('product_id', '=', $product_id)->get();
        return response()->json(['product' => $product, 'image' => $image]);
    }
//    public function connections() {
//        $ads = Advertising::where('status','=',1)->orderBy('created_at', 'desc')->paginate(9);
//        return view('web.ads.ket_noi', ["ads" => $ads]);
//    }
}
