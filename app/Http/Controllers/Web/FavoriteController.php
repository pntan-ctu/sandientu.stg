<?php

namespace App\Http\Controllers\Web;

use App\Models\Favorite;
use App\Models\Product;
use App\Models\Organization;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Propaganistas\LaravelIntl\Facades\Number;

class FavoriteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $favorite = $user->favorites;
        $products = array();
        $organzations = array();
        foreach ($favorite as $key => $value) {
            if ($value->favoriteable_type == Product::class) {
                $products[] = $value;//->favoriteable;
            } elseif ($value->favoriteable_type == Organization::class) {
                $organzations[] = $value;//->favoriteable;
            }
        }
        return view('web.users.favorite', ['products' =>  $products,
            'organzations' => $organzations,'favorite' => $favorite]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function show(Favorite $favorite)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function edit(Favorite $favorite)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Favorite $favorite)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Favorite  $favorite
     * @return \Illuminate\Http\Response
     */
    public function destroy(Favorite $favorite)
    {
        //Check quyền
        if ($favorite == null || $favorite->user_id != getUserId()) {
            throw new AuthorizationException();
        }

        $favorite->delete();
    }
}
