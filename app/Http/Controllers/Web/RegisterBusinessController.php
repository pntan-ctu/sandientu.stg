<?php

namespace App\Http\Controllers\Web;

use App\Business\CommercialCenterBusiness;
use App\Business\DepartmentManageBussiness;
use App\Business\OrganizationBusiness;
use App\Business\OrganizationTypeBusiness;
use App\Business\RegionBusiness; 
use App\Http\Requests\StoreOrganizationFromUserRequest;
use App\Models\Area;
use App\Models\CommercialCenter;
use App\Models\OrganizationLevel;
use App\Models\OrganizationType;
use App\OrgType;
use App\Constants;
use App\Utils\CommonUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\UserProfile;
use App\Models\Organization;
use App\Models\User;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Silber\Bouncer\BouncerFacade as Bouncer;

class RegisterBusinessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (!isUserMember()) { 
            abort(404, 'Page not found.');
        }

        $userProfile = User::find(Auth::user()->id);
        $orgTypeModel = OrganizationType::find(100);
        $orgTypeTree = OrganizationBusiness::getTreeOrgGov();
        $regions = RegionBusiness::getTreeWithParent(); //getRegionId()
        $orgTypeBusiness = OrganizationTypeBusiness::getTypeBusiness();
        $areas = null;
        $commercialCenters = null;
        $departments = DepartmentManageBussiness::renderRadio(0);
        $organizationLevel = OrganizationLevel::get();
        return view(
            'web/users/register-business',
            [
                'organization' => null,
                'orgTypeModel' => $orgTypeModel,
                'areaAll' => Area::all(),
                'commerAll' => CommercialCenter::all(),
                'orgTypeBusiness' => $orgTypeBusiness,
                'orgTypeTree' => $orgTypeTree,
                'regions' => $regions,
                'areas' => $areas,
                'commercialCenters' => $commercialCenters,
                'departments' => $departments,
                'organizationLevel' => $organizationLevel,
                'founding_type' => Constants::FOUNDING_TYPE
            ]
        );
        //return view('web/users/register-business', ["userProfile" => $userProfile, 'organization' => $organization]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function getType(Request $request)
    {
        if ($request->id == OrgType::CSSX) {
            $data["status"] = 1;
        } elseif ($request->id == OrgType::CSKD) {
            $data["status"] = 2;
        } else {
            $data["status"] = 0;
        }
        return $data;
    }

    public function register(StoreOrganizationFromUserRequest $request)
    {
        if (!isUserMember()) {
            abort(404, 'Page not found.');
        }

        Validator::make($request->all(), [
            'organization_type_id' => 'required'
        ])->validate();

        $organization = new Organization();
        $organization->fill($request->all());
        $validator = CommonUtil::validateFoundingNumber($request, 0);
        if ($validator->fails()) {
            return redirect(route('user.register-business-index'))
                ->withErrors($validator)
                ->withInput();
        }
        $validator = CommonUtil::validateMap($request, $organization);
        if ($validator->fails()) {
            return redirect(route('user.register-business-index'))
                ->withErrors($validator)
                ->withInput();
        }

        if (!isset($request->accept)) {
            return redirect(route('user.register-business-index'));
        }
        if ($request->hasFile('logo_image')) {
            $upload = UploadFileUtil::uploadImage($request, 'logo_image');
            $organization->logo_image = $upload->path;
        }
        if ($request->organization_type_id == OrgType::CSSX) {
            $organization["commercial_center_id"] = null;
        } elseif ($request->organization_type_id == OrgType::CSKD) {
            $organization["area_id"] = null;
        }
        $organization->manage_organization_id = $request->manage_organization_id;// se thay bang param
        $organization->organization_type_id = $request->organization_type_id;// se thay bang param
//        $organization->department_manage
//            = isset($request->department_manages) ? implode(",", $request->department_manages) : null;
        $organization->status = 0;
        $organization->created_by = getUserId();
        $organization->save();

        //Cập nhật cơ sở cho user
        $user = User::find(getUserId());
        $user->organization_id = $organization->id;
        $user->save();

        //Gán quyền chủ cơ sở cho user
        Bouncer::scope()->to($organization->organization_type_id);
        if ($organization->organization_type_id == OrgType::CSSX) {
            $user->assign(Constants::ROLE_CHU_CSSX);
        } elseif ($organization->organization_type_id == OrgType::CSKD) {
            $user->assign(Constants::ROLE_CHU_CSKD);
        }

        return redirect(route('org', ['organizationId' => $organization->id]));
    }
}
