<?php

namespace App\Http\Controllers\Web;

use App\Models\NewsComment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class NewsCommentsWebController extends Controller
{
    public function index()
    {
    }

    public function store(Request $request)
    {
        NewsComment::create([
            'name' => $request->fullname,
            'email' => $request->email,
            'comment' => $request->txtComment,
            'publish_by' => 0,
            'active' => 0,
            'article_id' => $request->article_id
        ]);
    }

    public function create()
    {
    }

    public function show($path)
    {
    }

    public function edit()
    {
//
    }

    public function update()
    {
    }

    public function destroy()
    {
    }
}
