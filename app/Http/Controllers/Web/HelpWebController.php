<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use App\Models\Help;

class HelpWebController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function getHelpData(Request $request)
    {
        $numberOfCommonHelps = $request->numberOfCommonHelps ?? 20;
        $numberOfQuestions = $request->numberOfQuestions ?? 20;
        $commonHelps = Help::where('status', 1)
            ->where('common', '>', 0)
            ->orderBy('common', 'asc')->take($numberOfCommonHelps)->get();
        $questions = Help::where('status', 1)
            ->where('common', 0)
            ->orderBy('created_at', 'desc')->paginate($numberOfQuestions);
        return ['questions'=>$questions, "commonHelps" => $commonHelps];
    }

    public function index(Request $request)
    {
        $helpData = self::getHelpData($request);
        return view('web.trogiup.index', $helpData);
    }

    public function getHelpDetail($path)
    {
        $commonHelps = Help::where('status', 1)
            ->where('common', '>', 0)
            ->orderBy('common', 'asc')->take(20)->get();
        $questions = Help::where('path', $path)->first();
        return ['questions' => $questions, "commonHelps" => $commonHelps];
    }

    public function show($path)
    {
        $helpDetail = self::getHelpDetail($path);
        return view('web.trogiup.detail', $helpDetail);
    }

    public function addHelps(\App\Http\Requests\StoreQuestionWebRequest $request)
    {
        Help::create([
           'name' => $request->name,
           'email' => $request->email,
           'phone' => $request->mobile,
           'address' => $request->address,
           'status' => 0,
           'question' => $request->title,
//           'content' => $request->content,
        ]);
        return response()->json(['status' => 'success', 'msg' => 'Lưu thông tin thành công !']);
    }
}
