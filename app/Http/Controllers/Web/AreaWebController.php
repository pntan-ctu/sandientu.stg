<?php

namespace App\Http\Controllers\Web;

use App\Models\Area;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrgType;
use Illuminate\Support\Facades\DB;

class AreaWebController extends Controller
{
    public function index()
    {
        $areas = Area::orderBy('created_at', 'desc')->paginate(10);
        return view('web.areas.index', ["areas" => $areas]);
    }

    public function store(Request $request)
    {
    }

    public function create()
    {
    }

    public function getAreaInfo($path)
    {
        $area = Area::where('path', $path)->firstOrFail();
        $aProduct = Area::join("organizations", "organizations.area_id", "=", "areas.id")
            ->join("products", "products.organization_id", "=", "organizations.id")
            ->select(['products.name', 'products.path', 'products.avatar_image'])
            ->where('areas.id', '=', $area->id)
            ->whereNull('products.deleted_at')
            ->orderBy('products.rank', 'desc')->limit(6)->get();
        $aOrganization = Area::join("organizations", "organizations.area_id", "=", "areas.id")
            ->select(['organizations.name', 'organizations.path', 'organizations.logo_image'])
            ->where('areas.id', '=', $area->id)
            ->whereNull('organizations.deleted_at');
        $iCount = $aOrganization->count();
        $aOrganization = $aOrganization->orderBy('organizations.rank', 'desc')->limit(6)->get();
        return [
            "area" => $area,
            "products" => $aProduct,
            "organizations" => $aOrganization,
            "num_of_orgs" => $iCount
        ];
    }

    public function show($path)
    {
        $data = self::getAreaInfo($path);
        return view('web.areas.area_detail', $data);
    }

    public function edit()
    {
//
    }

    public function update()
    {
    }

    public function destroy()
    {
    }
}
