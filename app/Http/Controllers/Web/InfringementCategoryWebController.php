<?php

namespace App\Http\Controllers\Web;

use App\Models\Organization;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class InfringementCategoryWebController extends Controller
{
    
    public function index()
    {
    }
    public function themVipham(Request $request)
    {
        $typeInfrin = $request->typeInfrin;
        //$sOr =substr(Organization::class, strrpos(Organization::class, '\\') + 1);
        if ($typeInfrin == 'Organization') {
            $model = Organization::find($request->typeId);
        } elseif ($typeInfrin == 'Product') {
            $model = Product::find($request->typeId);
        } else {
            $model = User::find($request->typeId);
        }
        if (isset($model)) {
                $model->infringements()->create([
                    "created_by" => getUserId(),
                    "category_id"=> $request->loaiVipham,
                    "comment"=> $request->comment,
                    "status"=> 0,
                ]);
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'error']);
    }
}
