<?php

namespace App\Http\Controllers\Web;

use App\Constants;
use App\Models\Payment;
use App\Models\Region;
use App\Models\Transportation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Product;
use Gloudemans\Shoppingcart\Facades\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\User;
use App\Models\UserProfile;
use App\Models\Organization;
use App\Business\RegionBusiness;
use App\Models\OrganizationShipType;
use App\Models\OrganizationPayType;
use Illuminate\Support\Facades\DB;

class ShoppingCartController extends Controller
{

    public function getmomonotify(){
$secretKey = 'Q6DNfTyPT71Y19BV8z9gndlQ4TejAeIL'; //Put your secret key in there

if (!empty($_GET)) {
    //Cart::destroy();

    $partnerCode = $_GET["partnerCode"];
    $accessKey = $_GET["accessKey"];
    $orderId = $_GET["orderId"];
    $localMessage = $_GET["localMessage"];
    $message = $_GET["message"];
    $transId = $_GET["transId"];
    $orderInfo = $_GET["orderInfo"];
    $amount = $_GET["amount"];
    $errorCode = $_GET["errorCode"];
    $responseTime = $_GET["responseTime"];
    $requestId = $_GET["requestId"];
    $extraData = $_GET["extraData"];
    $payType = $_GET["payType"];
    $orderType = $_GET["orderType"];
    $extraData = $_GET["extraData"];
    $m2signature = $_GET["signature"]; //MoMo signature
    $id_payment = Payment::max('id');
    echo $id_payment;
    $payment_m = Payment::findOrFail($id_payment);
    $payment_m->transaction_id = $transId;
    $payment_m->save();
    Cart::destroy();
    //Checksum
    $rawHash = "partnerCode=" . $partnerCode . "&accessKey=" . $accessKey . "&requestId=" . $requestId . "&amount=" . $amount . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo .
        "&orderType=" . $orderType . "&transId=" . $transId . "&message=" . $message . "&localMessage=" . $localMessage . "&responseTime=" . $responseTime . "&errorCode=" . $errorCode .
        "&payType=" . $payType . "&extraData=" . $extraData;

    $partnerSignature = hash_hmac("sha256", $rawHash, $secretKey);

    echo "<script>console.log('Debug huhu Objects: " . $rawHash . "' );</script>";
    echo "<script>console.log('Debug huhu Objects: " . $partnerSignature . "' );</script>";
echo "<script> alert('Đã thanh toán".$localMessage." cho đơn hàng".$orderId."'); </script>";

    // if ($m2signature == $partnerSignature) {
    //     if ($errorCode == '0') {
    //         $result = '<div class="alert alert-success"><strong>Payment status: </strong>Success</div>';
    //     } else {
    //         $result = '<div class="alert alert-danger"><strong>Payment status: </strong>' . $message .'/'.$localMessage. '</div>';
    //     }
    // } else {
    //     $result = '<div class="alert alert-danger">This transaction could be hacked, please check your signature and returned signature</div>';
    // }
} else {
    echo "<script> alert('trống'); </script>";
}
    $cart = Cart::content();
        $grouped = $cart->groupBy(function ($item, $key) {
            return $item->options->organization;
        });
    return view('web.cart.index', ['Cart' => $grouped]);
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $userProfile = UserProfile::find(Auth::user()->id);
        // return $userProfile;
        return view('web/users/index', ["userProfile" => $userProfile]);
    }

    public function addItemsToCart(Request $request)
    {
        // $user = User::find(getUserId());
        $product = Product::find($request->productId);
        //Check blacklist
        if ($this->checkBlackList($product)) {
            return response()->json(['status' => 'error',
                'message' => 'Bạn đã bị chặn đặt hàng của cơ sở này']);
        }
                
        $rows = Cart::search(function ($key, $value) use ($product) {
            return $key->id === $product->id;
        });
        $item = $rows->first();
        if (isset($item)) {
            Cart::update($item->rowId, $item->qty + $request->amount);
        } else {
            $cartInfo = [
                'id' => $product->id,
                'name' => $product->name,
                'price' => $product->price_sale ?? $product->price,
                'qty' => $request->amount,
                'options' => ['price_unit' => $product->price,
                    'avatar_image' => $product->avatar_image,
                    'path' => $product->path,
                    'organization' => $product->organization->name,
                    'organization_id' => $product->organization->id,
                    'unit' => $product->unit,
                    'weight_per_unit' => $product->weight_per_unit,
                ],
            ];
            $rowId = Cart::add($cartInfo);
        }
        return response()->json(['status' => 'success',
            'message'=> 'Thêm sản phẩm thành công',
            'totalQty' => Cart::count()]);
    }

    public function getProductsCart()
    {
        $cart = Cart::content();
        return response()->json(['message' => 'success',
                    'totalQty' => Cart::count(),
                    'product_cart' => $cart,
        ]);
    }

    public function getCart()
    {
        $cart = Cart::content();
        $grouped = $cart->groupBy(function ($item, $key) {
            return $item->options->organization;
        });
//        $groupCount = $grouped->map(function ($item, $key) {
//            return collect($item)->count();
//        });
        return view('web.cart.index', ['Cart' => $grouped]);
    }

    public function removeCartItem(Request $request)
    {
        $product = Product::find($request->productId);
        $rows = Cart::search(function ($key, $value) use ($product) {
            return $key->id === $product->id;
        });
        $item = $rows->first();
        if (isset($item)) {
            Cart::remove($item->rowId);
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'eror']);
    }

    public function purchase()
    {
        $userProfile = UserProfile::find(getUserId());
        $cart = Cart::content();
        $grouped = $cart->groupBy(function ($item, $key) {
            return $item->options->organization;
        });
        $subtotal = 0;
        $total = $grouped->map(function ($item) use ($subtotal) {
            foreach ($item as $pdt) {
                $subtotal += $pdt->price * $pdt->qty;
            }
            return $subtotal;
        });
       
        // thông tin đơn hàng
        $infoOrder = $this->getInfoOrder();
        $provinces = $this->getRegions(0);

        return view(
            'web.cart.purchase',
            ['Cart' => $grouped,
                "total" => $total,
                'userProfile' =>$userProfile,
                "infoOrder" => $infoOrder,
                "provinces" => $provinces
            ]
        );
    }

    /*
     * @author NGUYEN QUAN
     * $orders_id: List các order_id cách nhau bởi dấu gạch dưới (_)
     * $amount: Số tiền thanh toán qua VNPT PAY
     */
    private function initVnptPay($orders_id, $amount)
    {
        $url_base = Constants::VNPTPAY_URL_BASE;
        $api_key = config('app.vnptpay_api_key');
        $pay_code = md5($orders_id);
        $input = [
            "ACTION" => "INIT",
            "VERSION" => Constants::VNPTPAY_API_VERSION,
            "MERCHANT_SERVICE_ID" => Constants::VNPTPAY_MERCHANT_SERVICE_ID,
            "MERCHANT_ORDER_ID" => $pay_code,
            "AMOUNT" => $amount,
            "AMOUNT_DETAIL" => $amount,
            "SERVICE_ID" => "1",
            "DEVICE" => 1,
            "LOCALE" => "vi-VN",
            "CURRENCY_CODE" => "VND",
            "PAYMENT_METHOD" => "VNPTPAY",
            "DESCRIPTION" => "Thanh toan don hang " . $pay_code, // tieng Viet khong dau
            "CREATE_DATE" => date('YmdHis'),
            "CLIENT_IP" => $_SERVER['REMOTE_ADDR'] ?? ""
        ];
        $rs = "";
        foreach ($input as $k => $v) {
            if ($k != "SECURE_CODE") {
                $rs .= $v . "|";
            }
        }
        $rs .= config('app.vnptpay_secret_key');
        $input['SECURE_CODE'] = hash("sha256", $rs);

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($input));
        curl_setopt($curl, CURLOPT_URL, $url_base);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer ' . $api_key
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);

        if (!$result) {
            return ["success" => false, "msg" => "Không thể gọi service thanh toán VNPT PAY"];
        }

        curl_close($curl);
        $result = json_decode($result, true);
        if ($result['RESPONSE_CODE'] == "00") {
            // init VNPT PAY transaction success
            $payment = new Payment();
            $payment->pay_type_id = Constants::PAY_TYPE_VNPTPAY;
            $payment->currency = $input['CURRENCY_CODE'];
            $payment->description = $input['DESCRIPTION'];
            $payment->amount = $input['AMOUNT'];
            $payment->pay_code = $pay_code;
            $payment->save();

            $aOrderId = explode("_", $orders_id);
            foreach ($aOrderId as $oId) {
                $order = Order::findOrFail($oId);
                $order->payment_id = $payment->id;
                $order->save();
            }

            return [
                'status' => 'success',
                'message' => 'Chuyển hướng thanh toán',
                'do_payment' => 1,
                'redirect_url' => $result['REDIRECT_URL']
            ];
        } else {
            // init VNPT PAY transaction failure
            return [
                'status' => 'error',
                'message'=> 'Khởi tạo thanh toán thất bại. RESPONSE_CODE '
                    .$result['RESPONSE_CODE'].', '.$result['DESCRIPTION']
            ];
        }
    }

    /*
     * @author NGUYEN QUAN
     * gọi API điều tin một đơn hàng của VNPost
     */
    private function initVNPostShip($order, $shipTimeEstimate, $weight, $transportFee)
    {
        $orgId = $order->provider_organization_id;
        $org = Organization::findOrFail($orgId);
        $orgRegion = Region::find($org->region_id);
        $pathPrimaryKey = $orgRegion->path_primary_key;
        $path = explode('/', $pathPrimaryKey);
        $senderDistrict = $path[1] ?? 0;
        $vnpostSenderDistrict = DB::table('vnpost_regions')
            ->where('sdc_region_id', '=', $senderDistrict)->first();
        $vnpostSenderDistrictId = $vnpostSenderDistrict->id ?? 0;
        $senderAddress = (isset($org->address) ? $org->address."-" : "").$orgRegion->full_name;

        $receiveRegion = Region::find($order->region_id);
        $receivePathPrimaryKey = $receiveRegion->path_primary_key;
        $receivePath = explode('/', $receivePathPrimaryKey);
        $receiveDistrict = $receivePath[1] ?? 0;
        $vnpostReceiveDistrict = DB::table('vnpost_regions')
            ->where('sdc_region_id', '=', $receiveDistrict)->first();
        $vnpostReceiveProvinceId = $vnpostReceiveDistrict->parent_id ?? 0;
        $vnpostReceiveDistrictId = $vnpostReceiveDistrict->id ?? 0;
        $receiveAddress = $order->address."-".$receiveRegion->full_name;

        $vnpost_customer_code = config('app.vnpost_customer_code');
        $input = [
            'PosIdCollect' => 44, // THA
            'ItemCode' => '',
            'CustomerCode' => $vnpost_customer_code,
            'OrderNumber' => $order->id,
            'CODAmount' => $order->pay_type_id == 2 ? $order->total_money : 0,
            'SenderProvince' => 44, // THA
            'SenderDistrict' => $vnpostSenderDistrictId,
            'SenderAddress' => $senderAddress,
            'SenderName' => $org->name,
            'SenderEmail' => $org->email ?? "",
            'SenderTel' => $org->tel ?? "",
            'SenderDesc' => "Yeu cau van chuyen don hang ".$order->id,
            'Description' => "Giao hang trong ".$shipTimeEstimate,
            'ReceiverName' => $order->customer_name,
            'ReceiverAddress' => $receiveAddress,
            'ReceiverTel' => $order->tel,
            'ReceiverProvince' => $vnpostReceiveProvinceId,
            'ReceiverDistrict' => $vnpostReceiveDistrictId,
            'FlagConfig' => 2,
        ];
        $token = config('app.vnpost_token');
        $url_base = Constants::VNPOST_BASE_API_URL . "ReceiveOrder?token=$token";
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($input));
        curl_setopt($curl, CURLOPT_URL, $url_base);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json'
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        $result = curl_exec($curl);

        if (!$result) {
            return ["success" => false, "msg" => "Không thể gọi service báo tin vận chuyển hàng của VNPost"];
        }

        curl_close($curl);
        $result = json_decode($result, true);
        if ($result['Status'] ?? false) {
            // init VNPost Ship success
            $transportation = new Transportation();
            $transportation->ship_type_id = $order->ship_type_id;
            $transportation->order_id = $order->id;
            $transportation->weight = $weight;
            $transportation->amount_estimate = $transportFee;
            $transportation->time_estimate = $shipTimeEstimate;
            $transportation->save();

            return [
                'status' => 'success',
                'message' => 'Khởi tạo yêu cầu vận chuyển thành công'
            ];
        } else {
            // init VNPost Ship failure
            return [
                'status' => 'error',
                'message'=> 'Khởi tạo yêu cầu vận chuyển thất bại. Status '
                    .($result['Status'] ?? 'false').', '.($result['Message'] ?? '')
            ];
        }
    }

    public function addOrder(Request $request)
    {
        $cart = Cart::content();
        if ($cart->count()<1) {
            return response()->json(['status' => 'error','message'=>'Không có sản phẩm trong giỏ hàng' ]);
        }
        $grouped = $cart->groupBy(function ($item, $key) {
            return $item->options->organization_id;
        });
        $maxId = Order::max('id');

        $vnptpay_orders_id = ""; // list đơn hàng thanh toán vnptpay cách nhau bởi dấu gạch dưới _
        $vnptpay_amount = 0; // số tiền thanh toán vnptpay

        $momopay_amount = 0; // số tiền thanh toán momo
        $momopay_orders_id = "";
        foreach ($grouped as $organId => $collection) {
            $shipType= null;
            $payType = null;
            if ($request->has('ship_type_id'.$organId)) {
                $shipType = OrganizationShipType::with('shipType')
                        ->where('id', '=', $request->input('ship_type_id'.$organId))
                        ->get()->first();
            }
            if ($request->has('pay_type_id'.$organId)) {
                $payType = OrganizationPayType::with('payType')
                        ->where('id', '=', $request->input('pay_type_id'.$organId))
                        ->get()->first();
            }
            $subtotal = 0;
            foreach ($collection as $product) {
                $subtotal += $product->price * $product->qty;
            }
            $transportFee = $request->input('transport_fee'.$organId) ?? 0;
            $subtotal += $transportFee;

            /*************** start remove product from cart *********************/
            $rows = Cart::search(function ($cartItem, $rowId) use ($organId) {
                return $cartItem->options->organization_id === $organId;
            });
            foreach ($rows as $curRow) {
                $curPayTypeId = $payType->payType->id ?? 0;
                if ($curPayTypeId != Constants::PAY_TYPE_VNPTPAY && $curPayTypeId != Constants::PAY_TYPE_MOMO) {
                    //Cart::remove($curRow->rowId);
                }
            }
            /*************** end remove product from cart *********************/
            $maxId = $maxId + 1;

            DB::beginTransaction();

            $order = $this->createOrder(
                $organId,
                $request->ho_ten,
                $maxId,
                $request->dia_chi,
                $request->region_id,
                $request->phone_number,
                $subtotal,
                $shipType,
                $payType
            );

            $this->addItemsOrder($order->id, $collection);

            /* khi nào cho chạy vận chuyển vnpost thì uncomment
             * if ($order->ship_type_id >= 3 || $order->ship_type_id == 4) {
                $timeEstimate = $request->input('ship_time_estimate'.$organId);
                $weight = $request->input('weight'.$organId);
                $rsTrans = self::initVNPostShip($order, $timeEstimate, $weight, $transportFee);
                $rsStatus = $rsTrans['status'] ?? 'error';
                if ($rsStatus != 'success') {
                    return $rsTrans;
                }
            }*/
            DB::commit();

            if ($order->pay_type_id == Constants::PAY_TYPE_VNPTPAY) {
                $vnptpay_orders_id .= "_".$order->id;
                $vnptpay_amount += $order->total_money;
            }
            if($order->pay_type_id == Constants::PAY_TYPE_MOMO){
                $momopay_orders_id .= "_".$order->id;
                $momopay_amount += $order->total_money;
            }
        }

        //Cart::destroy();

        if ($vnptpay_amount > 0) {
            $vnptpay_orders_id = substr($vnptpay_orders_id, 1);
            return self::initVnptPay($vnptpay_orders_id, $vnptpay_amount);
        }
        else if($momopay_amount > 0){
            $momopay_orders_id = substr($momopay_orders_id, 1);
            return self::initMomoPay($momopay_orders_id,$momopay_amount);
        } 
        else {
            return response()->json([
                'status' => 'success',
                'message' => 'Đặt hàng thành công',
                'do_payment' => 0,
                'redirect_url' => url('thong-bao-dat-hang?order_normal=1')
            ]);
        }
    }

    public function addOrderOrgani(Request $request)
    {
        $cart = Cart::content();
        if ($cart->count()<1) {
            return response()->json(['status' => 'error','message'=>'Không có sản phẩm trong giỏ hàng' ]);
        }
        if ($request->has('ship_type_id')) {
            $shipType = OrganizationShipType::with('shipType')
                    ->where('id', '=', $request->ship_type_id)
                    ->get()->first();
        }
        if ($request->has('pay_type_id')) {
            $payType = OrganizationPayType::with('payType')
                    ->where('id', '=', $request->pay_type_id)
                    ->get()->first();
        }
        $organiId = intval($request->organiId);
        $rows = Cart::search(function ($cartItem, $rowId) use ($organiId) {
            return $cartItem->options->organization_id === $organiId;
        });
        $maxId = Order::max('id');
        $subtotal = 0;
        foreach ($rows as $product) {
            $subtotal += $product->price * $product->qty;
        }
        $transportFee = $request->transport_fee ?? 0;
        $subtotal += $transportFee;
        $maxId = $maxId + 1;

        DB::beginTransaction();

        $order = $this->createOrder(
            $organiId,
            $request->ho_ten,
            $maxId,
            $request->dia_chi,
            $request->region_id,
            $request->phone_number,
            $subtotal,
            $shipType,
            $payType
        );

        $this->addItemsOrder($order->id, $rows);

        /* khi nào cho chạy vận chuyển vnpost thì uncomment
         * if ($order->ship_type_id == 3 || $order->ship_type_id == 4) {
            $rsTrans = self::initVNPostShip($order, $request->ship_time_estimate, $request->weight, $transportFee);
            $rsStatus = $rsTrans['status'] ?? 'error';
            if ($rsStatus != 'success') {
                return $rsTrans;
            }
        }*/
        DB::commit();

        foreach ($rows as $product) {
            if (($payType->pay_type_id ?? 0) != Constants::PAY_TYPE_VNPTPAY && ($payType->pay_type_id ?? 0) != Constants::PAY_TYPE_MOMO) {
                Cart::remove($product->rowId);
            }
        }

        if ($order->pay_type_id == Constants::PAY_TYPE_VNPTPAY) {
            return self::initVnptPay($order->id, $order->total_money);
        }
        else if($order->pay_type_id == Constants::PAY_TYPE_MOMO){
            return self::initMomoPay($order->id, $order->total_money);
        }
        else {
            return response()->json([
                'status' => 'success',
                'message' => 'Đặt hàng thành công',
                'do_payment' => 0,
                'redirect_url' => url('thong-bao-dat-hang?order_normal=1')
            ]);
        }
    }

    public function createOrder(
        $organId,
        $ho_ten,
        $maxId,
        $dia_chi,
        $region_id,
        $phone_number,
        $subtotal,
        $shipType,
        $payType
    ) {
        $objBuy = getAccessingObject();
        $order = $objBuy->orders()->create([
            "provider_organization_id" => $organId,
            "code" => "HD" . str_pad("$maxId", 8, "0", STR_PAD_LEFT),
            "order_date" => now(),
            "customer_name" => $ho_ten,
            "address" => $dia_chi,
            "region_id" => $region_id,
            "tel" => $phone_number,
            "total_money" => $subtotal,
            "status" => 1,
            "ship_type_id" => $shipType != null ? $shipType->shipType->id : null,
            "ship_type_content" => $shipType != null ? $shipType->content : null,
            "pay_type_id" => $payType != null ? $payType->payType->id : null,
            "pay_type_content" => $payType != null ? $payType->content : null,
        ]);
        return $order;
    }
    public function addItemsOrder($orderId, $cart)
    {
        foreach ($cart as $product) {
            OrderItem::create([
                "order_id" => $orderId,
                "product_id" => $product->id,
                "product_name" => $product->name,
                "unit" => $product->unit,
                "price" => $product->price,
                "amount" => $product->qty,
                "money" => $product->price * $product->qty,
            ]);
        }
    }
    public function getInfoOrder()
    {
        $objBuy = getAccessingObject();
        $type = get_class($objBuy);

        $lastOrder = Order::where('orderable_type', '=', $type)
            ->where('orderable_id', '=', $objBuy->id)
            ->whereIn('status', [1, 2, 4])
            ->orderBy('order_date', 'desc')->first();

        $info = [];

        if (isset($lastOrder)) {
            $lastRegion = Region::find($lastOrder->region_id);
            $info = [
                "name"=> $lastOrder->customer_name,
                "tel" => $lastOrder->tel,
                "address" => $lastOrder->address,
                "region_id" => $lastOrder->region_id,
                "full_name" => $lastRegion->full_name ?? ''
            ];
        } elseif ($type == User::class) {
            $info = [
                "name"=> $objBuy->name,
                "tel" => $objBuy->tel ?? '',
                "address" => $objBuy->userProfile->address ?? '',
                "region_id" => $objBuy->userProfile->region_id ?? 0,
                "full_name" => (isset($objBuy->userProfile) &&isset($objBuy->userProfile->region))
                                ? $objBuy->userProfile->region->full_name : ''
            ];
        } elseif ($type == Organization::class) {
            $info = [
                "name"=> $objBuy->name,
                "tel" => $objBuy->tel ?? '',
                "address" => $objBuy->address,
                "region_id" => $objBuy->region_id,
                "full_name" =>$objBuy->region->full_name
            ];
        }

        return $info;
    }
    public function editAmountCart(Request $request)
    {
        $subtotal = 0;
        $product = Product::find($request->productId);
        if (!isset($product)) {
            return response()->json(['status' => 'eror','message' => 'Sản phẩm không tồn tại' ]);
        }
        if ($product->status == 1) {
            $rows = Cart::search(function ($key, $value) use ($product) {
                return $key->id === $product->id;
            });
            $item = $rows->count()>0 ? $rows->first(): null;
            if (isset($item) && $request->amount > 0) {
                Cart::update($item->rowId, $request->amount);
                $subtotal = Cart::get($item->rowId)->subtotal;
            }
        } else {
            return response()->json(['status' => 'eror','message' => 'Sản phẩm tạm hết hàng' ]);
        }
        return response()->json(['status' => 'success',
            'message' => 'Cập nhật thành công',
            'subtotal' => $subtotal,
            'id' => $request->productId,
            'totalPrice' => Cart::total(0, '.', '.'),
            'totalcount' => Cart::count(),
            ]);
    }
    public function checkBlackList($product)
    {
        //Check blacklist
        $organizations = $product->organization;
        $checkCount= $organizations->blacklists()->where('user_id', '=', getUserId())->count();
        if ($checkCount>0) {
            return true;
        }
        return false;
    }

    /*
     * @author NGUYEN QUAN
     * $orders_id: list các order_id (thanh toán bằng VNPT PAY) cách nhau bởi dấu gach dưới (_)
     */
    public function result(Request $request)
    {
        $payCode = $request->data ?? "";
        $payment = Payment::where('pay_code', $payCode)->orderBy('created_at', 'desc')->first();

        // remove product from cart
        if (isset($payment)) {
            if (isset($payment->response_code)) {
                if ($payment->response_code == "00") {
                    $orders = Order::where('payment_id', $payment->id)->select(['id'])->get();
                    foreach ($orders as $order) {
                        $aOrderItem = OrderItem::where('order_id', '=', $order->id)->get();
                        foreach ($aOrderItem as $oItem) {
                            $curProduct = Product::find($oItem->product_id);
                            $curRows = Cart::search(function ($key, $value) use ($curProduct) {
                                return $key->id === $curProduct->id;
                            });
                            $item = $curRows->first();
                            if (isset($item)) {
                                Cart::remove($item->rowId);
                            }
                        }
                    }
                }
            }
        }

        return view(
            'web.cart.result',
            ['Payment' => $payment]
        );
    }

    public function getRegions($parentId)
    {
        if ($parentId == 0) {
            $provinces = Region::whereNull('parent_id');
        } else {
            $provinces = Region::where('parent_id', '=', $parentId);
        }
        $provinces = $provinces->orderBy('name')->select(['id', 'name'])->get();
        return $provinces;
    }

    public static function calculatorMainFeeEco($weight, $provinceTo)
    {
        $tableName = $provinceTo > 1 ? 'transfer_fee' : 'transfer_province_fee';
        $columnName = 'fee'; // Hà Nội: $provinceToId == 1
        if ($provinceTo > 1) {
            $vnpostProvinceTo = DB::table('vnpost_regions')
                ->where('sdc_region_id', '=', $provinceTo)->first();
            $areaTo = $vnpostProvinceTo->area_id;
            $columnName = $areaTo == 1 ? 'in_area' : ($areaTo == 3 ? 'near_area' : 'distant_area');
        }

        $feeData = DB::table($tableName)->where('ship_type_id', '=', 3)
            ->where("weight_min", '<=', $weight)
            ->where("weight_min", '>=', 30000)
            ->orderBy('weight_min', 'asc')->get();

        $mainFee = $feeData[0]->$columnName;
        $size = count($feeData);
        for ($i = $size - 1; $i >= 1; $i--) {
            $topWeight = $i == $size - 1 ? $weight : $feeData[$i]->weight_max;
            $surplus = ceil(($topWeight - $feeData[$i - 1]->weight_max)/1000);
            $plusFee = $feeData[$i]->$columnName * $surplus;
            $mainFee += $plusFee;
        }
        return $mainFee;
    }

    public static function calculatorFeeTransport($regionFromId, $regionToId, $weight, $shipTypeId)
    {
        $regionFrom = Region::find($regionFromId);
        $fromPathKey = $regionFrom->path_primary_key ?? "";
        $fromKeys = explode("/", $fromPathKey);
        $provinceFrom = $fromKeys[0];

        if ($provinceFrom != 26) {
            return ['success' => false, 'message' => 'Tỉnh chuyển phát đi bắt buộc phải là Thanh Hóa'];
        }

        $districtFrom = $fromKeys[1] ?? 0;
        $villageFrom = $fromKeys[2] ?? 0;

        $regionTo = Region::find($regionToId);
        $toPathKey = $regionTo->path_primary_key ?? "";
        $toKeys = explode("/", $toPathKey);

        $provinceTo = $toKeys[0];
        $districtTo = $toKeys[1] ?? 0;
        $villageTo = $toKeys[2] ?? 0;

        $mainFee = 0;

        if ($shipTypeId == 3 && $weight > 30000) { // Logistics Eco - trên 30 kg
            $mainFee = self::calculatorMainFeeEco($weight, $provinceTo);
        } else {
            if ($provinceTo == 1 && $shipTypeId == 3) { // Chuyển đi Hà Nội
                $oProvinceFee = DB::table('transfer_province_fee')
                    ->where('ship_type_id', '=', $shipTypeId)
                    ->whereRaw("$weight between weight_min and weight_max")->first();
                $mainFee = $oProvinceFee->fee ?? 0;
            } else {
                $vnpostProvinceTo = DB::table('vnpost_regions')
                    ->where('sdc_region_id', '=', $provinceTo)->first();
                $areaTo = $vnpostProvinceTo->area_id;

                if ($shipTypeId == 4 && $weight > 30000) {
                    $oFee = DB::table('transfer_fee')->where('ship_type_id', '=', 4)
                        ->where("weight_max", '=', 30000)->first();
                    $oPlus = DB::table('transfer_fee')->where('ship_type_id', '=', 4)
                        ->where("fee_plus", '=', 1)->first();

                    $surplus = ceil(($weight - 30000)/500);
                    $columnName = $provinceFrom == $provinceTo ?
                        'in_province' : ($areaTo == 1 ? 'in_area' : 'distant_area');
                    $feePlus = $oPlus->$columnName * $surplus;
                    $mainFee = $oFee->$columnName + $feePlus;
                } else {
                    $oFee = DB::table('transfer_fee')->where('ship_type_id', '=', $shipTypeId)
                        ->whereRaw("$weight between weight_min and weight_max")->first();

                    if ($provinceFrom == $provinceTo) {
                        $mainFee = $oFee->in_province ?? 0;
                    } else {
                        if ($areaTo == 1) { // Chuyển nội vùng $areaFrom == $areaTo
                            $mainFee = $oFee->in_area ?? 0;
                        } else {
                            if ($areaTo == 3 && $shipTypeId == 3) { // chuyển cận vùng
                                $mainFee = $oFee->near_area ?? 0;
                            } else { // chuyển cách vùng $areaTo = 2
                                $mainFee = $oFee->distant_area ?? 0;
                            }
                        }
                    }
                }
            }
        }

        $vnpostDistrictFrom = DB::table('vnpost_regions')
            ->where('sdc_region_id', '=', $districtFrom)->first();
        $vnpostVillageFrom = DB::table('vnpost_regions')
            ->where('sdc_region_id', '=', $villageFrom)->first();
        $vnpostDistrictTo = DB::table('vnpost_regions')
            ->where('sdc_region_id', '=', $districtTo)->first();
        $vnpostVillageTo = DB::table('vnpost_regions')
            ->where('sdc_region_id', '=', $villageTo)->first();
        $districtFromFarArea = $vnpostDistrictFrom->far_area ?? 0;
        $villageFromFarArea = $vnpostVillageFrom->far_area ?? 0;
        $districtToFarArea = $vnpostDistrictTo->far_area ?? 0;
        $villageToFarArea = $vnpostVillageTo->far_area ?? 0;
        $fromFarArea = $districtFromFarArea == 1 || $villageFromFarArea == 1 ? 1 : 0;
        $toFarArea = $districtToFarArea == 1 || $villageToFarArea == 1 ? 1 : 0;

        $isFarArea =  (($shipTypeId == 3 && $toFarArea == 1) ||
            ($shipTypeId == 4 && ($fromFarArea == 1 || $toFarArea == 1))) ? 1 : 0;

        $petrolFee = $shipTypeId == 3 ? Constants::VNPOST_NORMAL_PETROL_FEE : Constants::VNPOST_FAST_PETROL_FEE;
        $farAreaFee = Constants::VNPOST_FAR_AREA_FEE;
        $totalFee = ($mainFee + $mainFee*$petrolFee + $isFarArea*$mainFee*$farAreaFee)*1.1;
        return $totalFee;
    }

    // thời gian vận chuyển của Logistic Eco thường nhanh hơn chuyển phát thường 1 ngày
    public static function getTransportTime($shipTypeId, $regionFromId, $regionToId, $weight)
    {
        $timeEstimate = "";

        $regionFrom = Region::find($regionFromId);
        $regionTo = Region::find($regionToId);
        $pathFrom = explode("/", $regionFrom->path_primary_key ?? "");
        $tinhFrom = $pathFrom[0] ?? 0;
        $pathTo = explode("/", $regionTo->path_primary_key ?? "");
        $tinhTo = $pathTo[0] ?? 0;
        $vnpostProvinceTo = DB::table('vnpost_regions')->where('sdc_region_id', '=', $tinhTo)->first();
        $areaTo = $vnpostProvinceTo->area_id ?? 0;

        if ($shipTypeId == 3) {
            if ($tinhFrom == $tinhTo) {
                $timeEstimate = "1 - 2";
            } else {
                if ($areaTo == 1) { // Nội vùng
                    $timeEstimate = $weight <= 30000 ? "2 - 4" : "2 - 3";
                } elseif ($areaTo == 3) { // Cận vùng
                    $timeEstimate = $weight <= 30000 ? "3 - 6" : "3 - 5";
                } elseif ($areaTo == 2) { // Cách vùng
                    $timeEstimate = $weight <= 30000 ? "5 - 8" : "5 - 7";
                }
            }
        } elseif ($shipTypeId == 4) {
            $emsAreaTo = $vnpostProvinceTo->ems_area_id ?? 0;
            if ($tinhTo == 26 || $tinhTo == 1 || $emsAreaTo == 7) {
                $timeEstimate = "1 - 2";
            } elseif ($tinhTo == 32 || $tinhTo == 50 || $emsAreaTo == 1) {
                $timeEstimate = "2";
            } elseif ($emsAreaTo >= 2 && $emsAreaTo <= 6) {
                $timeEstimate = "3";
            } else {
                $timeEstimate = "3 - 4";
            }
        }
        return $timeEstimate;
    }

    /*
     * Tính phí dịch vụ Phát Hàng Thu Tiền của khi phương thức vận chuyển là của VNPost
     * và phương thức thanh toán là COD. Kết quả được tính dự trên tổng phí (không COD) đã tính trước đó
     */
    public static function calculatorFeeCOD($amount, $regionFromId, $regionToId)
    {
        $regionFrom = Region::find($regionFromId);
        $fromPathKey = $regionFrom->path_primary_key ?? "";
        $fromKeys = explode("/", $fromPathKey);
        $provinceFrom = $fromKeys[0] ?? 0;

        $regionTo = Region::find($regionToId);
        $toPathKey = $regionTo->path_primary_key ?? "";
        $toKeys = explode("/", $toPathKey);
        $provinceTo = $toKeys[0] ?? 0;

        if ($provinceFrom * $provinceTo == 0) {
            return $amount;
        }

        $feePlus = 0;

        if ($provinceFrom == $provinceTo) {
            $feePlus = $amount <= 500000 ? 8000 : ($amount <= 1000000 ? 10000 : 0.01*$amount);
        } else {
            $min = 0.0012*$amount < 18000 ? 18000 : 0.0012*$amount;
            $feePlus = $amount <= 300000 ? 13000 : ($amount <= 600000 ? 15000 : ($amount <= 1000000 ? 17000 : $min));
        }
        return $feePlus;
    }

    // gọi service hủy đơn hàng bên VNPost
    public static function cancelTransportRequest($orderId)
    {
        $token = config('app.vnpost_token');
        $maKhachHang = config('app.vnpost_customer_code');
        $sParam = "token=$token&soDonHang=$orderId&maKhachHang=$maKhachHang";
        $url_base = Constants::VNPOST_BASE_API_URL . "huyDonHang?$sParam";
        $curl = curl_init();
        curl_setopt_array($curl, [CURLOPT_RETURNTRANSFER => 1, CURLOPT_URL => $url_base]);
        $result = curl_exec($curl);

        if (!$result) {
            return ["success" => false, "msg" => "Không thể gọi service hủy đơn hàng của VNPost", 'check' => 0];
        }

        curl_close($curl);
        $result = json_decode($result, true);
        $status = $result['Success'] ?? false;
        if (!$status) {
            return ["success" => false,
                "msg" => "VNPost không hủy yêu cầu vận chuyển đơn hàng. " . ($result['Message'] ?? ""), 'check' => 0];
        }
        DB::table('transportations')->where('order_id', '=', $orderId)->update(['status' => 3]);
        return ["success" => true, "msg" => "Gọi service hủy đơn hàng của VNPost thành công", 'check' => 0];
    }

    public function initMomoPay($order_Id, $amount){
        $url =Constants::MOMO_URL;//"https://test-payment.momo.vn/gw_payment/transactionProcessor";
        $accessKey =Constants::MOMO_ACCESS_KEY;//"j10IipKLkZArcYgr";
        $secretKey =Constants::MOMO_SECRET_KEY;//"Q6DNfTyPT71Y19BV8z9gndlQ4TejAeIL";
        $partnerCode =Constants::MOMO_PARTNER_CODE;//"MOMO5DXL20200217";
        //tạo request
        $requestId = time()."";
        $requestType = "captureMoMoWallet";
        $orderId = md5($order_Id).time();
        $orderInfo = "Thanh toán đơn hàng ".$orderId." qua Momo";
        $returnUrl = Constants::returnUrl;
        $notifyUrl = Constants::notifyUrl;
        $extraData = "";
        $rawHash = "partnerCode=" . $partnerCode . "&accessKey=" . $accessKey . "&requestId=" . $requestId . "&amount=" . $amount . "&orderId=" . $orderId . "&orderInfo=" . $orderInfo . "&returnUrl=" . $returnUrl . "&notifyUrl=" . $notifyUrl . "&extraData=" . $extraData;
        $signature = hash_hmac("sha256", $rawHash, $secretKey);
        $data = [
                    'partnerCode' => $partnerCode,
                    'accessKey' => $accessKey,
                    'requestId' => $requestId,
                    'amount' => "".$amount,
                    'orderId' => $orderId,
                    'orderInfo' => $orderInfo,
                    'returnUrl' => $returnUrl,
                    'notifyUrl' => $notifyUrl,
                    'extraData' => $extraData,
                    'requestType' => $requestType,
                    'signature' => $signature
                ];
        //gửi request 
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS,json_encode($data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen(json_encode($data)))
        );
        curl_setopt($ch, CURLOPT_TIMEOUT, 5);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
        //execute post
        $result = curl_exec($ch);
        if (!$result) {
            return ["success" => false, "msg" => "Không thể gọi service thanh toán Momo",
                    "pC" => $partnerCode, "sK" => $secretKey,
                    "aK" => $accessKey, "oI" => $order_id


                    ];
        }
        //close connection
        curl_close($ch);
       // $jsonResult = json_decode($result, true);  // decode json
        //nhận response, checksum và xử lý chuyển trang
        $result = json_decode($result, true);
        $m2signature = $result['signature']; //chữ ký của momo
        $newsignature = hash_hmac("sha256","requestId=".$result['requestId']."&orderId=".$result['orderId']."&message=".$result['message'].
"&localMessage=".$result['localMessage']."&payUrl=".$result['payUrl'].
"&errorCode=".$result['errorCode']."&requestType=".$result['requestType'], $secretKey);//chữ ký mới từ response 
        //checksum
        if($m2signature != $newsignature){
            return [
                'status' => 'error',
                'message'=> 'Dữ liệu có sai sót trên đường truyền hoặc đã bị hack'
            ];
        }
        if ($result['errorCode'] == "0") {
            // Khởi tạo thanh toán thành công
            $payment = new Payment();
            $payment->pay_type_id = Constants::PAY_TYPE_MOMO;
            $payment->currency = "VND";
            $payment->description = $data['orderInfo'];
            $payment->amount = $data['amount'];
            $payment->pay_code = $data['orderId'];
            $payment->save();

            $aOrderId = explode("_", $order_Id);
            foreach ($aOrderId as $oId) {
                $order = Order::findOrFail($oId);
                $order->payment_id = $payment->id;
                $order->save();
            }

            return [
                'status' => 'success',
                'message' => 'Chuyển hướng thanh toán',
                'do_payment' => 1,
                'redirect_url' => $result['payUrl']
            ];
        } else {
            // Khởi tạo thanh toán thất bại
            return [
                'status' => 'error',
                'message'=> 'Khởi tạo thanh toán thất bại. RESPONSE_CODE '
                    .$result['errorCode'].', '.$result['localMessage']
            ];
        }
    }

}
