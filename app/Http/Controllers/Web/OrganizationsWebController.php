<?php

namespace App\Http\Controllers\Web;

use App\Models\Blacklist;
use App\Models\CommercialCenter;
use App\Models\Organization;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\OrgType;
use App\Models\Area;
use Illuminate\Support\Facades\DB;
use App\Models\OrganizationInfoCfg;
use App\Models\Follow;
use App\Business\OrganizationBusiness;
use App\Constants;
use App\Business\ProductBussiness;

class OrganizationsWebController extends Controller
{
    public $allowPublic = Constants::ALLOW_PUBLIC_ORG_PRODUCT;
    public function index(Request $request)
    {
        $area_id = $request->area_id ?? 0;
        $commercial_center_id = $request->commercial_center_id ?? 0;

        $qryOrganization = OrganizationBusiness::getListForView();

        if ($area_id > 0) {
            $qryOrganization->where('area_id', '=', $area_id);
        } elseif ($commercial_center_id > 0) {
            $qryOrganization->where('commercial_center_id', '=', $commercial_center_id);
        }

        $lstOrganization = $qryOrganization->orderBy('rank', 'desc')->paginate(10);

        $area = Area::find($area_id);
        $areaName = $area->name ?? "";

        $commercialCenter = CommercialCenter::find($commercial_center_id);
        $commercialCenterName = $commercialCenter->name ?? "";

        return view('web.shops.index', [
            "Organization" => $lstOrganization, "AreaId" => $area_id, "AreaName" => $areaName,
            "CommercialCenterId" => $commercial_center_id, "CommercialCenterName" => $commercialCenterName
        ]);
    }

    public function store(Request $request)
    {
    }

    public function create()
    {
    }

    public function getOrganizationInfoDetail($path)
    {
        $organization = OrganizationBusiness::getModelForView($path);

        if (!isset($organization)) {
            abort(404);
        }

        $organization->visits()->increment();

        $organization->bookmarkViewOrganization(); //đánh dấu lên cache có lượt view

        $contentOrgani = $this->getOrganiInfoConfig($organization);

        // ratings
        $quryRatings = $organization->ratings()->where('status', '<', 2);
        if (!Constants::ALLOW_PUBLIC_RATING_FQA) {
            $quryRatings->where('status', '=', 1);
        }
        $ratings = $quryRatings->orderBy('created_at', 'desc')->get();

        // question answers
        $quryQas = $organization->questionAnswers()->whereNull('parent_id')->where('status', '<', 2);
        if (!Constants::ALLOW_PUBLIC_RATING_FQA) {
            $quryQas->where('status', '=', 1);
        }
        $qas = $quryQas->orderBy('created_at', 'desc')->paginate(10);

        // like sản phẩm
        if (getUserId()>0) {
            $favoriteCount = $organization->favorites()->where('user_id', getUserId())->count();
        } else {
            $favoriteCount = 0;
        }

        // follow cơ sở sản xuất
        if (getUserId()>0) {
            $followCount = Follow::where('user_id', getUserId())->where('organization_id', $organization->id)->count();
        } else {
            $followCount = 0;
        }

        $iItemPerPage = 12;
        $productCate = ProductBussiness::getProductsShopForView($organization->id)
            ->orderBy('name')->paginate($iItemPerPage);

        // loại phản ánh vi phạm
        $loaiVipham = \App\Models\InfringementCategory::where("type", '=', 0)->get();

        //Ghep địa chỉ
        $organization->address = $organization->address . ", " . $organization->region->full_name;

        return [
            "Organization" => $organization,
            "contentOrgani" => $contentOrgani,
            "ratings" => $ratings,
            "qas"=> $qas,
            "favoriteCount"=> $favoriteCount,
            "followCount" =>$followCount,
            "ProductCate" => $productCate,
            "loaiVipham" => $loaiVipham,
        ];
    }

    public function show($path)
    {
        $organizationDetailInfo = self::getOrganizationInfoDetail($path);
        return isset($organizationDetailInfo['message']) ?
            view('web.shops.shop_notfound', ["message"=>"Không tồn tại cơ sở này !"]) :
            view('web.shops.shop_detail', $organizationDetailInfo);
    }

    public function getOrganiInfoConfig($organization)
    {
        $id = $organization->id;
        $contentOrgani = OrganizationInfoCfg::leftJoin(
            'organization_info_details',
            function ($join) use ($id) {
                $join->on('organization_info_cfgs.key', '=', 'organization_info_details.key');
                $join->on('organization_info_details.organization_id', '=', DB::raw($id));
            }
        )->where('organization_info_cfgs.organization_type_id', '=', $organization->organization_type_id)->get();
        return $contentOrgani;
    }

    public function edit()
    {
//
    }

    public function update()
    {
    }

    public function destroy()
    {
    }

    public function addRateOrgan(Request $request)
    {
        $organization = Organization::find($request->organId);
        $msg = RatingsBaseController::insertRating($request, $organization);
        return $msg;
    }
    
    public function addLikeOrgani(Request $request)
    {
        $userId =  getUserId();
        $organization = Organization::find($request->organId);
        if (isset($organization)) {
            $favorite = $organization->favorites()->where('user_id', $userId);
            if ($favorite->count()<1) {
                $organization->favorites()->create([
                    "user_id" => $userId,
                ]);
            } else {
                $organization->favorites()->where('user_id', $userId)->delete();
            }
            return response()->json(['message' => 'success','total'=> $organization->favorites()->count()]);
        }
        return response()->json(['message' => 'error']);
    }

    public function addFollow(Request $request)
    {
        $follows = Follow::where('organization_id', $request->organId)->where('user_id', getUserId());
        if ($follows->count()<1) {
            Follow::create([
                "user_id" => getUserId(),
                "organization_id" => $request->organId
            ]);
        } else {
            $follows->delete();
        }
        return response()->json(['message' => 'success']);
    }

    public function themVipham(Request $request)
    {
        $userId =  getUserId();
        $organization = Organization::find($request->organId);
        if (isset($organization)) {
                $organization->infringements()->create([
                    "created_by" => $userId,
                    "category_id"=> $request->loaiVipham,
                    "comment"=> $request->comment,
                    "status"=> 0,
                ]);
            return response()->json(['message' => 'success']);
        }
        return response()->json(['message' => 'error']);
    }

    /* CHuyên dung hàm
    public function getAllOrgani()
    {
        $sWhere = '(organization_type_id='.OrgType::CSSX.' or organization_type_id='.OrgType::CSKD.')';
        if ($this->allowPublic == false) {
            $sWhere .= " and status > 0 and status < 2";
        } else {
            $sWhere .= " and status < 2";
        }
        $organization = Organization::whereRaw($sWhere);
        return $organization;
    }*/

    public function blockUser(Request $request)
    {
        if (!isUserBusiness()) {
            return false;
        }
        
        $blacklist = new Blacklist();
        $blacklist->organization_id = getOrgId();
        $blacklist->user_id = $request->user_id;
        $blacklist->reason = $request->reason;
        $blacklist->created_by = getUserId();
        $blacklist->save();
    }

    public function unblockUser(Request $request)
    {
        if (!isUserBusiness()) {
            return false;
        }
        
        $orgId = getOrgId();
        $blackLists = Blacklist::where('organization_id', '=', $orgId)
            ->where('user_id', '=', $request->user_id)->get();
        foreach ($blackLists as $bList) {
            $bList->delete();
        }
    }
}
