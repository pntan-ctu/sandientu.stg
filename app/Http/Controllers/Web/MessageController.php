<?php

namespace App\Http\Controllers\Web;

use App\Models\Message;
use App\Models\Order;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class MessageController extends Controller
{
    protected $model;

    public function index()
    {
        $user = auth()->user();
        $itemPerPage = 10;
        $orgId = getOrgId();
        
        $msgableId = $this->model == 'App\Models\User' ? $user->id : $orgId;
        $layout = $this->model == 'App\Models\User' ? 'user' : 'org';

        $messagesReceive = Message::where('messageable_id', '=', $msgableId)
            ->where('messageable_type', '=', $this->model)
            ->orderBy('created_at', 'desc')
            ->select(['id', 'subject', 'content', 'read_at',
                DB::raw("date_format(created_at, '%d/%m/%Y %H:%i') as ngay_gui"),
                DB::raw("case right(sendable_type,4) when 'User' 
					then (select name from users where users.id=sendable_id)
					else (select name from organizations where organizations.id=sendable_id)
			    end as name")
            ])->paginate($itemPerPage, ['*'], 'page_receive');

        $messagesSend = Message::where('sendable_id', '=', $msgableId)
            ->where('sendable_type', '=', $this->model)
            ->where('messageable_type', '!=', Order::class)
            ->orderBy('created_at', 'desc')
            ->select(['id', 'subject', 'content', 'sendable_type',
                DB::raw("date_format(created_at, '%d/%m/%Y %H:%i') as ngay_gui"),
                DB::raw("case right(messageable_type,4) when 'User' 
					then (select name from users where users.id=messageable_id)
					else (select name from organizations where organizations.id=messageable_id)
			    end as name"),
                DB::raw("(select name from users u where u.id=created_by) as nguoi_gui")
            ])->paginate($itemPerPage, ['*'], 'page_send');

        return view('web.users.message', [
            'MessagesReceive' => $messagesReceive,
            'MessagesSend' => $messagesSend,
            'MsgableId' => $msgableId,
            'Layout' => $layout
        ]);
    }

    public function getMessage($id)
    {
        $message = Message::where('id', '=', $id)
            ->orderBy('created_at', 'desc')
            ->select(['id', 'subject', 'content',
                DB::raw("date_format(created_at, '%d/%m/%Y %H:%i') as ngay_gui"),
                DB::raw("case right(sendable_type,4) when 'User' 
					then (select name from users where users.id=sendable_id)
					else (select name from organizations where organizations.id=sendable_id)
			    end as nguoi_gui"),
                DB::raw("case right(messageable_type,4) when 'User' 
					then (select name from users where users.id=messageable_id)
					else (select name from organizations where organizations.id=messageable_id)
			    end as nguoi_nhan"),
                DB::raw("case when read_at is null then 0 else 1 end as da_doc")
            ])->first();
        return $message;
    }

    public function markAsReadMessage($id)
    {
        $message = Message::find($id);
        $message->markAsRead();
    }

    public function replyMessage(Request $request)
    {
        $msgId = $request->id;
        $subject = $request->msg_subject;
        $content = $request->msg_content;
        $message = Message::find($msgId);
        $sendToObject = $message->sendable_type == "App\Models\User" ?
            User::find($message->sendable_id) : Organization::find($message->messageable_id);
        $user = auth()->user();
        $object = $request->typeObjectSend == "user" ? $user : getOrgModel();

        $object->sendMesage($subject, $content, $sendToObject);
    }

    /* Current login user send a message to an user */
    public function sendToUser($subject, $content, $receiveUserId)
    {
        $user = auth()->user();
        $receiveUser = User::find($receiveUserId);
        $user->sendMesage($subject, $content, $receiveUser);
    }

    /* Current login user send a message to an Organization */
    public function sendToOrganization($subject, $content, $receiveOrgId)
    {
        $user = auth()->user();
        $receiveOrg = Organization::find($receiveOrgId);
        $user->sendMesage($subject, $content, $receiveOrg);
    }

    /* Organization of current login user send a message to an user */
    public function orgSendToUser($subject, $content, $receiveUserId)
    {
        $org = getOrgModel();
        $receiveUser = User::find($receiveUserId);
        $org->sendMesage($subject, $content, $receiveUser);
    }

    /* Organization of current login user send a message to an Organization */
    public function orgSendToOrganization($subject, $content, $receiveOrgId)
    {
        $org = getOrgModel();
        $receiveOrg = Organization::find($receiveOrgId);
        $org->sendMesage($subject, $content, $receiveOrg);
    }
}
