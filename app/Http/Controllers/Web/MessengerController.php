<?php

namespace App\Http\Controllers\Web;

use App\Authors\AbilityName;
use App\Authors\OrgAuthor;
use App\Events\MessageSent;
use App\Http\Requests\StoreMessageRequest;
use App\Models\Organization;
use App\Models\User;
use App\OrgType;
use Carbon\Carbon;
use Cmgmyr\Messenger\Models\Message;
use Cmgmyr\Messenger\Models\Participant;
use App\Models\Thread;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class MessengerController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->middleware('auth');
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Show all of the message threads to the user.
     *
     * @return mixed
     */
    public function index(Organization $org = null)
    {
        $activeThreadId = null;
        $temporaryThread = null;
        $orgId = null;
        $user = auth()->user();
        if ($org) {
            $orgId = $org->id;
            // Chỉ hỗ trợ chat với các cơ sở
            if ($org->organization_type_id != OrgType::CSSX && $org->organization_type_id != OrgType::CSKD) {
                abort(404);
            }
            // Tìm thread giữa người dùng với cơ sở
            $activeThread = Thread::whereOrganizationId($org->id)->whereUserId(Auth::id())->first();
            // Nếu đã tồn tại
            if ($activeThread) {
                // Nếu người dùng không thuộc thành viên của thread, không cho phép truy cập
                if (!$activeThread->hasParticipant($user->id)) {
                    throw new AuthorizationException();
                }
                $activeThreadId = $activeThread->id;
            } else {
                // Tạo một thread tạm giữa người dùng và cơ sở
                $temporaryThread = new Thread(
                    [
                        'subject' => $user->name . ':' . $org->id,
                        'organization_id' => $orgId,
                    ]
                );
                $temporaryThread->id = 0;
                $activeThreadId = 0;
                $temporaryThread->user = ['id' => $user->id, 'name' => $user->name];
                $temporaryThread->organization = ['id' => $org->id, 'name' => $org->name];
            }
        }
        return view('web.messenger.index', compact('activeThreadId', 'temporaryThread'));
    }

    /**
     * Fetch all messages
     *
     * @return Message
     */
    public function fetchThreads(Request $request)
    {
        // All threads, ignore deleted/archived participants
        //$threads = Thread::getAllLatest()->get();
        // All threads that user is participating in
        $threads = Thread::forUser(Auth::id())->latest('updated_at')
            ->with(['user:id,name', 'organization:id,name,logo_image', 'user.userProfile:user_id,avatar'])
            ->has('user')->has('organization') // TODO lấy thread đã xóa cơ sở hoặc người dùng?
            ->get();
        // All threads that user is participating in, with new messages
        // $threads = Thread::forUserWithNewMessages(Auth::id())->latest('updated_at')->get();
        //$threads = auth()->user()->threadsWithNewMessages();

        $activeThread = null;
        $messages = null;
        // active_id
        //  empty: active thread đầu tiên
        //  0: không active thread nào
        //  còn lại: active thread có id = active_id
        // nếu url không định tuyến chat với một cơ sở cự thể nào => active thread đầu tiên
        if (!$request->filled('active_id')) {
            if ($threads->count() > 0) {
                $activeThread = $threads[0];
            }
        } elseif (request('active_id') != 0) { // khi định tuyến một thread cụ thể
            $activeThread = $threads->find(request('active_id'));
            if (!$activeThread) {
                abort(404);
            }
        }
        if ($activeThread) {
            $activeThread->markAsRead(Auth::id());
            $messages = $activeThread->messages()->with(['user:id,name', 'user.userProfile:user_id,avatar'])->get();
        }
        foreach ($threads as $thread) {
            $thread->class = $thread->isUnread(Auth::id()) ? 'unread' : '';
            $thread->latestMsg = $thread->latestMessage;
            $thread->latestMsg->load('user:id,name');
            $thread->unreadMessagesCount = $thread->userUnreadMessagesCount(Auth::id());
        }
        return compact('threads', 'activeThread', 'messages');
    }

    /**
     * Stores a new message thread.
     *
     * @return mixed
     */
    public function store(StoreMessageRequest $request, int $thread)
    {
        $user = Auth::user();
        $wasRecentlyCreatedThread = false;
        if ($thread) {
            $threadModel = Thread::findOrFail($thread);
            if (!$threadModel->hasParticipant($user->id)) {
                throw new AuthorizationException();
            }
        } elseif ($request->filled('org_id')) {
            $org = Organization::findOrFail(request('org_id'));
            // Chỉ cho chat với các cơ sở
            if ($org->organization_type_id != OrgType::CSSX && $org->organization_type_id != OrgType::CSKD) {
                abort(404);
            }
            $threadModel = Thread::firstOrCreate(
                ['organization_id' => $org->id, 'user_id' => Auth::id()],
                ['subject' => $user->name . ':' . $org->id]
            );
            $wasRecentlyCreatedThread = $threadModel->wasRecentlyCreated;
            if ($wasRecentlyCreatedThread) {
                // Thêm nhân viên của cơ sở vào thread
                $participants = $org->users()->whereKeyNot($user->id)->pluck('id')->toArray();
                $threadModel->addParticipant($participants);
                // TODO xử lý trường hợp nhân viên tạo sau thời điểm này
            }
        } else {
            throw ValidationException::withMessages(['org_id' => 'Mã thread hoặc đơn vị không được bỏ trống.']);
        }

        // Message
        $message = $threadModel->messages()->create(
            [
                'user_id' => $user->id,
                'body' => request('message'),
            ]
        );

        // Broadcast to recipients
        broadcast(new MessageSent($message, $wasRecentlyCreatedThread))->toOthers();

        if ($wasRecentlyCreatedThread) {
            // Sender: đánh dấu sender đã đọc
            Participant::create([
                'thread_id' => $threadModel->id,
                'user_id' => $user->id,
                'last_read' => new Carbon,
            ]);
//            $message->thread->makeVisible('organization');
            $message->thread->organization = $org->setVisible(['id', 'name']);
            $message->isNewThread = true;
        } else {
            $message->makeHidden('thread');
        }
        $user->setVisible(['id', 'name', 'email']);
        $message->user = $user;

        return $message;
    }

    public function fetchMessages(Thread $thread)
    {
        $thread->markAsRead(Auth::id());
        return $thread->messages()->with(['user:id,name', 'user.userProfile:user_id,avatar'])->get();
    }

    public function sendMessageToFollowers(int $organizationId, Request $request)
    {
        $user = Auth::user();
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $content = $request->msg_content;

        $followers = $organization->follows;
        foreach ($followers as $item) {
            $wasRecentlyCreatedThread = false;
            $receiveUser = User::find($item->user_id);
            $threadModel = Thread::firstOrCreate(
                ['organization_id' => $organization->id, 'user_id' => $item->user_id],
                ['subject' => $organization->name . ' - ' . $receiveUser->name]
            );
            $wasRecentlyCreatedThread = $threadModel->wasRecentlyCreated;
            $message = $threadModel->messages()->create(
                [
                    'user_id' => $user->id,
                    'body' => $content,
                ]
            );

            // Broadcast to recipients
            broadcast(new MessageSent($message, $wasRecentlyCreatedThread))->toOthers();

            if ($wasRecentlyCreatedThread) {
                // Thêm các nhân viên của cơ sở vào thread
                //$participants = $receiveUser->organization->users()->whereKeyNot($user->id)->pluck('id')->toArray();
                //$threadModel->addParticipant($participants);

                $threadModel->addParticipant($receiveUser->id); // only send message to user who follow org

                // Sender: đánh dấu sender đã đọc
                Participant::create([
                    'thread_id' => $threadModel->id,
                    'user_id' => $user->id,
                    'last_read' => new Carbon,
                ]);
                $message->thread->organization = $organization->setVisible(['id', 'name']);
                $message->isNewThread = true;
            } else {
                $message->makeHidden('thread');
            }
        }
    }
}
