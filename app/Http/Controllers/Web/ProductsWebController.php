<?php

namespace App\Http\Controllers\Web;

use App\Models\Product;
use App\Models\Rating;
use App\OrgType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Organization;
use Illuminate\Support\Facades\DB;
use App\Business\ProductBussiness;
use App\Constants;

class ProductsWebController extends Controller
{
    public function index()
    {
    }

    public function store(Request $request)
    {
    }

    public function create()
    {
        //        $AlbumGroup = AlbumGroup::all();
        //        return view('manage.albums.create', ['AlbumGroup' => $AlbumGroup]);
    }

    public function getDataDetailOfProduct($path)
    {
        $product = ProductBussiness::getModelForView($path); // Product::where('path', $path)->first();
        if (!isset($product)) {
            abort(404);
            // return ["message" => "Không tồn tại sản phẩm này !"];
        }

        // san phẩm mới
        $ProductNew = ProductBussiness::getListForView()->orderBy('created_at', 'desc')->take(3)->get();

        // sản phẩm bán chạy
        $ProductOrder = ProductBussiness::getListForView()->orderBy('count_order', 'desc')->take(6)->get();
        // sản phẩm liên quan
        $ProductRelation = ProductBussiness::getProductsRelationForView(
            $product->product_category_id,
            $product->id
        )
            ->orderBy('count_order', 'desc')->take(6)->get();
        // sản phẩm khác cùng cơ sở
        $ProductRelationShop = ProductBussiness::getProductsRelationShopForView(
            $product->organization_id,
            $product->id
        )
            ->orderBy('count_order', 'desc')->take(6)->get();
        // đếm số lượt view
        $product->visits()->increment();
        // đánh dấu lên cache có lượt view
        $product->bookmarkViewProduct();

        $countProducts = $this->getCountProducts($product->organization_id);
        $contentProduct = $this->getProductInfoConfig($product->id, $product->product_category_id);

        // ratings
        $quryRatings = $product->ratings()->where('status', '<', 2);
        if (!Constants::ALLOW_PUBLIC_RATING_FQA) {
            $quryRatings->where('status', '=', 1);
        }
        $ratings = $quryRatings->orderBy('created_at', 'desc')->get();

        // question answers
        $quryQas = $product->questionAnswers()->whereNull('parent_id')->where('status', '<', 2);
        if (!Constants::ALLOW_PUBLIC_RATING_FQA) {
            $quryQas->where('status', '=', 1);
        }
        $qas = $quryQas->orderBy('created_at', 'desc')->paginate(20);

        // like sản phẩm
        if (getUserId() > 0) {
            $favoriteCount = $product->favorites()->where('user_id', getUserId())->count();
        } else {
            $favoriteCount = 0;
        }

        // loại phản ánh vi phạm
        $loaiVipham = \App\Models\InfringementCategory::where("type", '=', 1)->get();
        // sản phẩm gốc
        $productLink = $product->productLinks;

        /************** THÔNG TIN CHI TIẾT SẢN PHẨM ******************/
        $status = "";
        $duration = isset($product->duration) ? " (".$product->duration.")" : "";
        $productStatus = $product->status ?? 0;
        switch ($productStatus) {
            case 0:
                $status = "Chưa kinh doanh";
                break;
            case 1:
                $status = "Còn hàng";
                break;
            case 2:
                $status = "Theo mùa vụ".$duration;
                break;
            case 3:
                $status = "Hết hàng";
                break;
            case 4:
                $status = "Ngừng kinh doanh";
                break;
        }
        $product->status_txt = $status;
        $product->has_check_txt = isset($product->has_check) && $product->has_check == 1 ? "Có" : "Không";
        $aLabelProductInfo = ['Tên bất động sản', 'Mã tin đăng', 'Trạng thái','Diện tích sử dụng(m²)','Giấy tờ pháp lý','Độ dài cụ thể Dài x Rộng'];
        $aProductInfo = ['name', 'code', 'status_txt','it5_dientichsudung','it5_giaytophaply','it5_ngangdai'];
        $aProCert = $product->certificates ?? [];

        /************** THÔNG TIN CHI TIẾT CƠ SỞ ******************/
        $Organization = $product->Organization;
        $status = "";
        $orgStatus = $Organization->status ?? 0;
        switch ($orgStatus) {
            case 0:
                $status = "Chưa duyệt";
                break;
            case 1:
                $status = "Đang hoạt động";
                break;
            case 2:
                $status = "Ngừng hoạt động";
                break;
            case 3:
                $status = "Khóa do vi phạm";
                break;
        }
        $Organization->status_txt = $status;
        if ($Organization->organization_type_id == OrgType::CSSX) {
            $labelArea = 'Vùng sản xuất';
            $Organization->area_txt = isset($Organization->area) ? $Organization->area->name : "";
            $Organization->area_url = isset($Organization->area) ?
                url("/vung-san-xuat/".$Organization->area->path) : "";
        }

        if ($Organization->organization_type_id == OrgType::CSKD) {
            $labelArea = 'Địa điểm kinh doanh';
            $Organization->area_txt = isset($Organization->commercialCenter) ?
                $Organization->commercialCenter->name : "";
        }

        $Organization->organization_level_id_txt = isset($Organization->organizationLevel) ?
            $Organization->organizationLevel->name : "";
        $Organization->founding_date_txt = isset($Organization->founding_date) ?
            ($Organization->founding_date)->format('d/m/Y') : "";
        $soGiayPhep = isset($Organization->founding_number) ? "Số ".$Organization->founding_number : "";
        $soGiayPhep .= ", ngày cấp ".$Organization->founding_date_txt;
        if (isset($Organization->founding_by_gov)) {
            $soGiayPhep .= ", đơn vị cấp ".$Organization->founding_by_gov;
        }
        $Organization->founding_number_txt = $soGiayPhep;
        $Organization->manage_org_txt = isset($Organization->manageOrganization) ?
            $Organization->manageOrganization->name : '';

        $aLabelOrg = ['Tên Công ty', 'Địa chỉ', 'Điện thoại', 'Email', 'Website',
            'Trạng thái'];
        $aOrgInfo = ['name', 'address', 'tel', 'email', 'website',
            'status_txt'];
        $aOrgCert = $Organization->certificates ?? [];

        return [
            "Product" => $product,
            "productImages" => $product->ProductImages,
            "Organization" => $product->Organization,
            "countProducts" => $countProducts,
            "contentProduct" => $contentProduct,
            "ProductNew" => $ProductNew,
            "ProductOrder" => $ProductOrder,
            "ratings" => $ratings,
            "qas" => $qas,
            "favoriteCount" => $favoriteCount,
            "loaiVipham" => $loaiVipham,
            "ProductRelation" => $ProductRelation,
            "ProductRelationShop" => $ProductRelationShop,
            "numberOfFavorites" => $product->favorites()->count(),
            "numberOfOrgQuestion" => $product->Organization->questionAnswers->count(),
            "labelProductInfos" => $aLabelProductInfo,
            "productInfos" => $aProductInfo,
            "proCerts" => $aProCert,
            "labelOrgInfos" => $aLabelOrg,
            "orgInfos" => $aOrgInfo,
            "orgCerts" => $aOrgCert
        ];
    }

    public function show($path)
    {
        $productDetailData = self::getDataDetailOfProduct($path);
        return isset($productDetailData['message']) ?
            view('web.shops.shop_notfound', ["message" => "Không tồn tại sản phẩm này !"]) :
            view('web.products.product_detail', $productDetailData);
    }

    public function getCountProducts($idOrgan)
    {
        $organization = Organization::find($idOrgan);
        $countProducts = $organization->Products->count();
        return $countProducts;
    }

    public function getProductInfoConfig($product_id, $product_category_id)
    {
        $productInfoConfig = DB::table("product_info_cfgs")
            ->leftJoin('product_info_details', function ($join) use ($product_id) {
                $join->on('product_info_cfgs.key', '=', 'product_info_details.key')
                    ->where('product_info_details.product_id', '=', $product_id);
            })
            ->whereNull('product_info_cfgs.product_category_id')
            ->orWhere('product_info_cfgs.product_category_id', '=', $product_category_id)
            ->get([
                'product_info_details.id', 'product_info_cfgs.key',
                'product_info_details.content', 'product_info_cfgs.name'
            ]);

        return $productInfoConfig;
    }

    public function edit()
    {
        //
    }

    public function update()
    {
    }

    public function destroy()
    {
    }
    public function addRate(Request $request)
    {
        $product = Product::find($request->productId);
        $msg = RatingsBaseController::insertRating($request, $product);
        return $msg;
    }
    public function updateRateToProduct($product)
    {
        $rates = Rating::where('ratingable_id', '=', $product->id)->where('status', '=', 1);
        //$sumRates = $rates->sum('rate');
        if (isset($rates)) {
            $avg_rates = $rates->sum('rate') / $rates->count();
            $product->avg_rate = $avg_rates;
            $product->count_rate = $rates->count();
            $product->save();
        }
    }
    public function addLikeProduct(Request $request)
    {
        $userId =  getUserId();
        $product = Product::find($request->productId);
        if (isset($product)) {
            $favorite = $product->favorites()->where('user_id', $userId);
            if ($favorite->count() < 1) {
                $product->favorites()->create([
                    "user_id" => $userId,
                ]);
            } else {
                $product->favorites()->where('user_id', $userId)->delete();
            }
            return response()->json(['message' => 'success', 'total' => $product->favorites()->count()]);
        }
        return response()->json(['message' => 'error']);
    }
}
