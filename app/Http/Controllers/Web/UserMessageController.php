<?php

namespace App\Http\Controllers\Web;

use App\Models\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserMessageController extends MessageController
{
    public function __construct()
    {
        $this->model = 'App\Models\User';
    }

    public function sendMessageToOrganization(Request $request)
    {
        $orgId = $request->org_id;
        $receiveOrg = Organization::find($orgId);
        $subject = $request->msg_subject;
        $content = $request->msg_content;

        $obj = getAccessingObject();
        $obj->sendMesage($subject, $content, $receiveOrg);
    }
}
