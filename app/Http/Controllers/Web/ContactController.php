<?php

namespace App\Http\Controllers\Web;

use App\Models\Contact;
use App\Models\ContactFile;
use App\Models\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use App\Business\OrganizationBusiness;
use App\Utils\CommonUtil;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\StoreContactRequest;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $qryOrg = Organization::where('organization_type_id', '>', 1)
                ->where('organization_type_id', '<', 100);
        if (request()->filled('keyword')) {
            $keyword = request()->keyword;
            $qryOrg->where('name', 'like', "%$keyword%");
        }
        $org = $qryOrg->paginate(5);
        $orgTypeTree = OrganizationBusiness::getTreeOrgGov();
        return view('web.contact.index', ['orgTypeTree' => $orgTypeTree,'org'=>$org]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        return view('manage/home');
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreContactRequest $request, $isMobile = 0)
    {
        $contact = new Contact;
        $contact->organization_id = $request->org_id;
        $contact->title = $request->title;
        $contact->content = $request->content;
        $contact->name = $request->name;
        $contact->email = $request->email;
        $contact->phone = $request->phone;
        $contact->status = 0;
        $contact->save();
        //$contact->forward_log = $contact->organization->name;
        //$contact->save();
        
        if ($request->hasFile('files')) {
            $files = UploadFileUtil::multiUploadFile($request, 'files');
            foreach ($files as $item) {
                $contact_file = new ContactFile;
                $contact_file->contact_id = $contact->id;
                $contact_file->filename = $item->path;
                $contact_file->save();
            }
        }
        
        if ($isMobile == 0) {
            return redirect('lien-he')->with('thongbao', 'Cảm ơn bạn đã gửi phản hồi về cho chúng tôi !');
        } else {
            return ['success' => true, 'msg' => 'Cảm ơn bạn đã gửi phản hồi về cho chúng tôi!'];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Contact $contact)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //
    }
}
