<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RequestProductsController extends Controller
{
    public function __construct()
    {
    }
    public function sendRequestProduct(Request $request)
    {
        $messageController = new MessageController();
        $product = \App\Models\Product::find($request->productId);
        if (!isset($product)) {
            return response()->json(['status' => 'error', 'msg' => 'Gửi không thành công!']);
        }
        $subject = "[Liên hệ sản phẩm]: ".$product->code." - ".$product->name;
        $content = $request->txtContent;
        $messageController->sendToOrganization($subject, $content, $product->organization_id);
        return response()->json(['status' => 'success', 'msg' => 'Gửi liên hệ thành công!']);
        //$object->sendMesage($subject, $content, $sendToObject);
    }
}
