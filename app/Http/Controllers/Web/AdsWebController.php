<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Organization;
use App\Models\Advertising;
use App\OrgType;
use App\Models\AdvertisingCategory;
use Illuminate\Support\Facades\Input;
use App\Models\ProductCategory;
use App\Business\RegionBusiness;
use App\Http\Controllers\Manage\ProductCategoryController;
use App\Business\AreaBusiness;
use Illuminate\Support\Facades\DB;
use App\Business\OrganizationBusiness;
use App\Models\AdvertisingImage;

class AdsWebController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function connections(Request $request)
    {
        $organization = OrganizationBusiness::getListForView()
                        ->orderBy('rank', 'desc')
                        ->take(5)->get();
        
        $query = Advertising::with(['advertisingImages',
            'createdByUser',
            'advertisingCategory'])
                ->where('status', '=', 1)
                ->where(function ($query) {
                    $query->where('to_date', '>=', date('Y-m-d'))
                            ->orWhereNull('to_date');
                });

        if ($request->has('typeAds')) {
            $typeAds = AdvertisingCategory::where('path', $request->typeAds);
            if (count($typeAds->get()) > 0) {
                $query->where('category_id', '=', $typeAds->first()->id);
            }
        }
        if ($request->has('keyword')) {
            $query->where('title', 'like', '%' . $request->keyword . '%');
        }
        if ($request->has('typeProduct') && $request->typeProduct != 0) {
            $arrCaterProducts = $this->getIdsCaterProducts($request->typeProduct);
            $query->whereIn('product_category_id', $arrCaterProducts);
        }
        if ($request->has('region') && $request->region != 0) {
            $arrRegions = $this->getIdsRegions($request->region);
            $query->whereIn('region_id', $arrRegions);
        }
        $ads = $query->orderBy('created_at', 'desc')->paginate(12);
        // loại sản phẩm
        $tree = $this->getTreeCateProduct();
        // vị trí
        $regions = RegionBusiness::getTreeWithParent(RegionBusiness::REGIONIDROOT);
        
        return view('web.ads.ket_noi', [
            "ads" => $ads->appends(Input::except('page')),
            'organization' => $organization,
            'keyWord' => $request->keyword,
            'tree' => $tree,
            'typeProductId' => $request->typeProduct,
            'regionsId' => $request->region,
            "regions" => $regions,
        ]);
    }

    public function getAdsInfo($path)
    {
        $organization = OrganizationBusiness::getListForView()
            ->orderBy('rank', 'desc')
            ->take(5)->get();
        $ads = Advertising::with('createdByUser')
            ->where('path', '=', $path)
            ->where(function ($query) {
                $query->where('to_date', '>=', date('Y-m-d'))
                    ->orWhereNull('to_date');
            })->firstOrFail();
        $typeObj = $ads->advertisingable_type;
        $rtnValueType = $ads->advertisingable;
        // đêm số lượt truy cập
        $ads->visits()->increment();
        // 10 tin kết nối cung cầu
        $ads_most = Advertising::where('status', '=', 1)
            ->where('path', '!=', $path)
            ->where(function ($query) {
                $query->where('to_date', '>=', date('Y-m-d'))
                    ->orWhereNull('to_date');
            })->orderBy('created_at', 'desc')
            ->take(10)->get();
        $avatar_url = isset($rtnValueType->logo_image) ? "/image/300/300/".$rtnValueType->logo_image :
            (isset($rtnValueType->userProfile->avatar) ?
                "/image/300/300/".$rtnValueType->userProfile->avatar : 'css/images/no-image.png');
        $regionFullName = $typeObj == 'App\Models\User' ? ($rtnValueType->UserProfile->region->full_name ?? '') :
            ($rtnValueType->region->full_name ?? '');

        return ['ads' => $ads,'ads_most' => $ads_most,
                'organization' => $organization,
                'avatar_url' => url($avatar_url),
                'advertisingable_name' => $rtnValueType->name ?? '',
                'region_full_name' => $regionFullName
            ];
    }

    public function showAds($path)
    {
        $data = self::getAdsInfo($path);
        return view(
            'web.ads.detail',
            $data
        );
    }
    public function getIdsRegions($regionId)
    {
        $arrRegions = array();
        $regions = RegionBusiness::getTreeWithParent($regionId);
        $this->createRegions($arrRegions, $regions);
        return $arrRegions;
    }
    public function createRegions(&$arrRegions, $regions)
    {
        foreach ($regions as $region) {
            $arrRegions[] = $region->id;
            if (isset($region->children)) {
                $this->createRegions($arrRegions, $region->children);
            }
        }
    }

    public function getTreeCateProduct()
    {
        $arr = ProductCategory::get(['id', 'parent_id', 'name']);
        $productCategoryController = new ProductCategoryController();
        $tree = $productCategoryController->getTreeCategory($arr);
        return $tree;
    }

    public function getIdsCaterProducts($parent_id)
    {
        $category = ProductCategory::find($parent_id);
        $arrCategory = array($category->id);
        $categories = ProductCategory::all();
        $this->createCategory($arrCategory, $categories, $category->id);
        return $arrCategory;
    }

    public function createCategory(&$arrCategory, $categories, $parent_id)
    {
        foreach ($categories as $category) {
            if ($category->parent_id == $parent_id) {
                $arrCategory[] = $category->id;
                $this->createCategory($arrCategory, $categories, $category->id);
            }
        }
    }
    public static function fullTextSearch()
    {
       
        
        /* Nếu có ảnh của tin "cung cầu" thì lấy ảnh đầu tiên
         * Nếu không có thì:
         *  - Nếu là "cá nhân" đăng tin => trả về Avatar của cá nhân
         *  - Nếu là "tổ chức" đăng tin => trả về logo_image của tổ chức, nếu logo null thì lại lấy ảnh cá nhân.
        */
        $imageField = "IFNULL((SELECT ai.image FROM advertising_images ai " .
                "WHERE ai.advertising_id=a.id ORDER BY ai.id LIMIT 0,1), " .
        "CASE WHEN a.advertisingable_type='App\Models\User' THEN " .
                        "(SELECT up.avatar " .
                        "FROM users u1 INNER JOIN user_profiles up on u1.id=up.user_id WHERE u1.id=u.id) " .
                    "ELSE IFNULL(" .
                            "(SELECT o.logo_image FROM organizations o WHERE o.id=a.advertisingable_id),".
                            "(SELECT up1.avatar FROM users u3 " .
                            "INNER JOIN user_profiles up1 on u3.id=up1.user_id WHERE u3.id=u.id)) " .
        "END) as image ";
        
        $data = DB::select("SELECT a.*, ".$imageField.
                            "FROM advertisings a " .
                            "INNER JOIN users u ON a.created_by=u.id " .
                            "WHERE  a.status=1 " .
                            "ORDER BY a.created_at desc");
                           
        
        return $data;
    }
}
