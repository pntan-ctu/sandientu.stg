<?php

namespace App\Http\Controllers\Web;

use App\Models\ProductCategory;
use App\Models\CertificateCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Business\ProductBussiness;
use Illuminate\Support\Facades\Input;
use App\Models\Product;
use Ratchet\App;

class ProductCategoryWebController extends Controller
{
    public function getProducts(Request $request)
    {
        $orderStyle = "desc";
        $columnName = 'rank';
        $levels = [];
        $size = 12;
        $certs = [];
        if (isset($request->size)) {
            $size = $request->size;
        }
        if (isset($request->sort)) {
            $sort = explode("-", $request->sort);
            if ($sort[0] != "default") {
                $columnName = $sort[0];
            }
            $orderStyle = count($sort) > 1 ? $sort[1] : "desc";
        }
        if (isset($request->level)) {
            $levels = explode(",", $request->level);
        }
        if (isset($request->certs)) {
            $certs = explode(",", $request->certs);
        }
        $productsCater = ProductBussiness::getListForView();
        $total = $productsCater->count();
        if (count($levels) > 0) {
            $productsCater = $productsCater
                ->join('organizations', function ($join) use ($levels) {
                    $join->on('products.organization_id', '=', 'organizations.id')
                        ->whereIn('organizations.organization_level_id', $levels);
                });
        }
        if (count($certs) > 0) {
            $productsCater = $productsCater->join('certificates', function ($join) use ($certs) {
                $join->on('products.id', '=', 'certificates.certificateable_id')
                    ->where('certificates.certificateable_type', Product::class)
                    ->whereIn('certificates.certificate_category_id', $certs);
            });
        }
        $productsCater = $productsCater->select('products.*')->orderBy('products.' . $columnName, $orderStyle)
            ->paginate($size);
        return [
            "ProductCate" => $productsCater->appends(Input::except('page')),
            "total" => $total,
            "title" => "Danh sách sản phẩm",
        ];
    }

    public function index(Request $request)
    {
        $data = self::getProducts($request);
        return view('web.products.productscate', $data);
    }

    public function getOrderBy(Request $request)
    {
        if (isset($request->sort)) {
            $sort = explode("-", $request->sort);
            $comlumnName = $sort[0];
            $orderStyle = $sort[1];
        }
    }

    public function getProductInGroup(Request $request, $path)
    {
        $columnName = "rank";
        $orderStyle = "desc";
        $levels = [];
        $certs = [];
        $size = 12;
        $categorynName = "";
        if (isset($request->size)) {
            $size = $request->size;
        }
        if (isset($request->sort)) {
            $sort = explode("-", $request->sort);
            if ($sort[0] != "default") {
                $columnName = $sort[0];
            }
            $orderStyle = count($sort) > 1 ? $sort[1] : "desc";
        }
        if (isset($request->level)) {
            $levels = explode(",", $request->level);
        }
        if (isset($request->certs)) {
            $certs = explode(",", $request->certs);
        }
        $productsList = ProductBussiness::getListForView();
        $total = $productsList->count();
        if ($path == 'khuyen-mai') {
            $categorynName = "Khuyến mại";
            $productsCater = $productsList->whereNotNull('price_sale')
                ->whereRaw('price_sale < price');
            $productsCater = $productsCater->orderBy('products.' . $columnName, $orderStyle)
                ->paginate($size);
        } elseif ($path == 'moi-ban') {
            $categorynName = "Mới bán";
            $columnOrder = "created_at";
            $productsCater = $this->getProductsOrderBy($columnOrder, $columnName, $orderStyle, $size);
        } elseif ($path == 'mua-nhieu') {
            $categorynName = "Mua nhiều";
            $columnOrder = "count_order";
            $productsCater = $this->getProductsOrderBy($columnOrder, $columnName, $orderStyle, $size);
        } elseif ($path == 'quan-tam') {
            $categorynName = "Quan tâm";
            $columnView = "count_view";
            $productsCater = $this->getProductsOrderBy($columnView, $columnView, $orderStyle, $size);
        } else {
            $category = ProductCategory::where('path', $path)->firstOrFail();
            $categorynName = $category->name;
            $arrCategory = array($category->id);
            $categories = ProductCategory::all();
            $this->createCategory($arrCategory, $categories, $category->id);
            $productsCater = $productsList->whereIn('product_category_id', $arrCategory);
            if (count($levels) > 0) {
                $productsCater = $productsCater
                    ->join('organizations', function ($join) use ($levels) {
                        $join->on('products.organization_id', '=', 'organizations.id')
                            ->whereIn('organizations.organization_level_id', $levels);
                    });
            }
            if (count($certs) > 0) {
                $productsCater = $productsCater->join('certificates', function ($join) use ($certs) {
                    $join->on('products.id', '=', 'certificates.certificateable_id')
                        ->where('certificates.certificateable_type', Product::class)
                        ->whereIn('certificates.certificate_category_id', $certs);
                });
            }
            $productsCater = $productsCater->orderBy('products.' . $columnName, $orderStyle)
                ->paginate($size);
        }
        return [
            "ProductCate" => $productsCater->appends(Input::except('page')),
            "title" => $categorynName,
            "total" => $total
        ];
    }

    public function show(Request $request, $path)
    {
        $products = self::getProductInGroup($request, $path);
        return view('web.products.productscate', $products);
    }
    public function getProductsOrderBy($columnOrder, $columnName, $orderStyle, $size)
    {
        $products = ProductBussiness::getListForView()
            ->orderBy($columnOrder, "desc")
            ->orderBy($columnName, $orderStyle)
            ->paginate($size);
        return $products;
    }

    public function createCategory(&$arrCategory, $categories, $parent_id)
    {
        foreach ($categories as $category) {
            if ($category->parent_id == $parent_id) {
                $arrCategory[] = $category->id;

                $this->createCategory($arrCategory, $categories, $category->id);
            }
        }
    }

    /* Chuyển sang dùng ProductBussiness::getListForView();
    public function getAllProducts()
    {
        $products = Product::where('products.active', '<', 2)
            ->where('products.status', '>', 0)
            ->where('products.status', '<', 4)
            ->where('products.active', '<', 2) //Không kiểm duyệt SP, chỉ bắt bị khóa VP
            ->whereHas('organization', function ($query) {
                    $query->where('organizations.status', '<', 2); //CS còn hoạt động
                    if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                        $query->where('organizations.status', '>', 0); //CS phải đc kiểm duyệt
                    }
                })->with('organization');
        return $products;
    }*/
}
