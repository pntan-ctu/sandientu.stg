<?php

namespace App\Http\Controllers\Web;

use App\Constants;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Organization;
use App\Models\Payment;
use App\Utils\UploadFileUtil;
use App\Business\OrderBusiness;
use Illuminate\Auth\Access\AuthorizationException;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    private function checkOrder($order)
    {
        if ($order == null || $order->orderable_type != User::class || $order->orderable_id != getUserId()) {
            throw new AuthorizationException();
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web.users.orders', ['user' =>  Auth::user()]);
    }

    public function indexData(Request $request)
    {
        return OrderBusiness::getOrderOfUsers(Auth::id(), $request->input('status'), $request->input('keyword'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getOrder(Request $request)
    {
        $data = Order::find($request->id);

        //Check quyền
        self::checkOrder($data);

        if (isset($request->id) && $request->id) {
            $data["data"] = OrderItem::where("order_id", $request->id)->get();
        }
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actOrder(Request $request)
    {
        $data = Order::find($request->id);

        //Check quyền
        self::checkOrder($data);

        if ($request->status == 1) {
            $data->status = 2;
            $data->save();
        } elseif ($request->status == 2) {
            $data->status = 4;
            $data->save();
        } else {
            $data->status = 1;
            $data->save();
        }

        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function huyOrder(Request $request)
    {
        $data = Order::find($request->id);

        //Check quyền
        self::checkOrder($data);

        $paymentId = $data->payment_id ?? 0;
        $payment = Payment::find($paymentId);
        $paymentStatus = $payment->response_code ?? "";
        if ($paymentStatus == "00") {
            return ['success' => false, 'msg' => 'Không thể xóa đơn hàng đã được thanh toán', 'check' => 0];
        }

        DB::beginTransaction();
        if ($data->status == 1) {
            if ($data->ship_type_id == 3 || $data->ship_type_id == 4) {
                $rsCallServiceVNPost = ShoppingCartController::cancelTransportRequest($data->id);
                $rs = $rsCallServiceVNPost['success'] ?? false;
                if (!$rs) {
                    return $rsCallServiceVNPost;
                }
            }
            $data->delete();
            DB::commit();
            return ["success" => true, 'msg' =>  'Xoá thành công', 'check' => 1];
        } else {
            return ["success" => true, 'msg' =>  'Trạng thái đơn hàng đã bị thay đổi', 'check' => 0];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showOrder($id)
    {
        $data = Order::find($id);

        //Check quyền
        self::checkOrder($data);

        $data["messages"] = $data->getMessages()->get();
        if (isset($id) && $id) {
            $data["data"] = OrderItem::where("order_id", $id)->get();
            $data["org"] = Organization::find($data["provider_organization_id"]);
        }
        $paymentId = $data->payment_id ?? 0;
        $payment = Payment::find($paymentId);
        $paymentStatus = $payment->response_code ?? "";
        return view('web.users.show-orders', ['data' =>  $data, 'payment_status' => $paymentStatus]);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function addComment(Request $request)
    {
        $data = Order::find($request->order_id);

        //Check quyền
        self::checkOrder($data);
        
        $data->addMesage($data["code"], $request->text, Auth::user());
        return $data;
    }
}
