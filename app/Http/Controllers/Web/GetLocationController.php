<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;

class GetLocationController extends Controller
{
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web.get_location.index');
    }
}
