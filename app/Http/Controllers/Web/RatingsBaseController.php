<?php

namespace App\Http\Controllers\Web;

class RatingsBaseController
{
    public static function insertRating($request, $models)
    {
        if (isset($models)) {
            $models->ratings()->create([
                "rate" => $request->rate,
                "comment" => $request->comments,
                "status" => 0,
                "created_by" => getUserId(),
            ]);
            return response()->json(['message' => 'success', 'msg' => 'Lưu thông tin thành công!']);
        } else {
            return response()->json(['message' => 'error', 'msg' => 'Lưu thông tin không thành công!']);
        }
    }
}
