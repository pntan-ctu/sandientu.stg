<?php

namespace App\Http\Controllers\Web;

use App\Models\DocumentType;
use App\Models\DocumentField;
use App\Models\DocumentOrgan;
use App\Models\Document;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use App\Models\NewsArticle;

class DocumentController extends Controller
{
    public function index()
    {
        $newArticles = NewsArticle::where('status', 1)->orderBy('created_at', 'desc')->take(5)->get();
        $docType = DocumentType::all();
        $docOrgan = DocumentOrgan::all();
        $docField = DocumentField::all();
        return view('web.documents.index', ['docType' => $docType, 'docOrgan' => $docOrgan,
            'docField' => $docField,"newArticles" =>$newArticles]);
    }
    
    public function indexData(Request $request)
    {
        $document = Document::select(['id', 'number_symbol', 'quote','path', 'sign_date'])
            ->orderBy('sign_date', 'desc');
        return DataTables::eloquent($document)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('number_symbol', 'like', '%' . $keyword . '%')
                                    ->orWhere('quote', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('idOrgan') && $request->input('idOrgan') != 0) {
                $query->where('document_organ_id', '=', request('idOrgan'));
            }
            if ($request->filled('idType') && $request->input('idType') != 0) {
                $query->where('document_type_id', '=', request('idType'));
            }
            if ($request->filled('idField') && $request->input('idField') != 0) {
                $query->where('document_field_id', '=', request('idField'));
            }
        })->make();
    }
    public function show($path)
    {
        $newArticles = NewsArticle::where('status', 1)->orderBy('created_at', 'desc')->take(5)->get();
        $doc = Document::where('path', $path)->firstOrFail();
        return view('web.documents.doc_detail', ['doc' => $doc,'newArticles'=>$newArticles]);
    }
}
