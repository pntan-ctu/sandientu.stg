<?php

namespace App\Http\Controllers\Web;

use App\Models\NewsArticle;
use App\Models\NewsComment;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Constants;

class NewsWebController extends Controller
{
    public function index()
    {
    }

    public function store(Request $request)
    {
    }

    public function create()
    {
    }

    public function getNewsDetail($path)
    {
        $newsArticle = NewsArticle::where('path', $path)
            ->where('status', '=', 1)->firstOrFail();
        $arrIdNews= explode(',', $newsArticle->relate_news);

        $newsRelate = NewsArticle::where('status', '=', 1)
            ->where('group_id', '<>', Constants::PREFIX_NEWS_GROUP_ID)
            ->whereIn('id', $arrIdNews)->orderBy("publish_at", "desc")->get();

        $NewsGroup = $newsArticle->NewsGroup;

        $newArticles = NewsArticle::where('status', '=', 1)
            ->where('group_id', '<>', Constants::PREFIX_NEWS_GROUP_ID)
            ->orderBy('created_at', 'desc')->take(5)->get();

        $newsComments = NewsComment::where('article_id', '=', $newsArticle->id)
            ->where('active', '=', 1)->orderBy('created_at', 'desc')->get();

        // đếm số lượt truy cập
        $newsArticle->visits()->increment();
        return ["NewsArticle" => $newsArticle,
            "NewsGroup" => $NewsGroup,
            "newArticles" =>$newArticles,
            "NewsRelate" =>$newsRelate,
            "NewsComments"=>$newsComments];
    }

    public function show($path)
    {
        $newsDetail = self::getNewsDetail($path);
        return view('web.news.news_detail', $newsDetail);
    }

    public function edit()
    {
    }

    public function update()
    {
    }

    public function destroy()
    {
    }

    public function about($path)
    {
        $newsArticle = NewsArticle::where('path', '=', $path)
            ->where('group_id', '=', \App\Constants::PREFIX_NEWS_GROUP_ID)
            ->firstOrFail();

        // đếm số lượt truy cập
        $newsArticle->visits()->increment();

        return view(
            'web.news.news_about',
            ["newsArticle" => $newsArticle]
        );
    }
}
