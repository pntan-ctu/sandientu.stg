<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Album;
use App\Models\AlbumDetail;

class ImagesWebController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index()
    {
        $allbums = Album::where('active', 1)->get();
        return view('web.images.index', ['allbums'=>$allbums]);
    }
    public function showImage($path)
    {
        $allbum = Album::where('path', $path)->first();
        $images = AlbumDetail::where('album_id', '=', $allbum->id)->get();
        return view('web.images.view_allbum', ['images' => $images, 'allbum'=>$allbum ]);
    }
}
