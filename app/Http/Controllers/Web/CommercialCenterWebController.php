<?php

namespace App\Http\Controllers\Web;

use App\Models\CommercialCenter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Business\MapsBussiness;

class CommercialCenterWebController extends Controller
{
    public function index()
    {
        $commercialCenters = CommercialCenter::orderBy('created_at', 'desc')->paginate(5);
        $arOrg = array();
        $keyword = "";
        if (request()->filled('keyword')) {
            $keyword = request()->keyword;
        }
        $arOrg = MapsBussiness::getMapsLocation(3, $keyword);
        return view('web.commercial_centers.index', ["commercialCenters" => $commercialCenters,
            "organization100"=> json_encode($arOrg)]);
    }
    public function store(Request $request)
    {
    }

    public function create()
    {
    }

    public function getCommercialCenterInfo($path)
    {
        $commercialCenter = CommercialCenter::where('path', $path)->firstOrFail();
        $aProduct = CommercialCenter::join(
            "organizations",
            "organizations.commercial_center_id",
            "=",
            "commercial_centers.id"
        )
            ->join("products", "products.organization_id", "=", "organizations.id")
            ->select(['products.name', 'products.path', 'products.avatar_image'])
            ->where('commercial_centers.id', '=', $commercialCenter->id)
            ->whereNull('products.deleted_at')
            ->orderBy('products.rank', 'desc')->limit(6)->get();

        $aOrganization = CommercialCenter::join(
            "organizations",
            "organizations.commercial_center_id",
            "=",
            "commercial_centers.id"
        )
            ->select(['organizations.name', 'organizations.path', 'organizations.logo_image'])
            ->where('commercial_centers.id', '=', $commercialCenter->id)
            ->whereNull('organizations.deleted_at');

        $iCount = $aOrganization->count();
        $aOrganization = $aOrganization->orderBy('organizations.rank', 'desc')->limit(6)->get();
        return [
            "CommercialCenter" => $commercialCenter,
            "products" => $aProduct,
            "organizations" => $aOrganization,
            "num_of_orgs" => $iCount
        ];
    }

    public function show($path)
    {
        $commercialCenterInfo = self::getCommercialCenterInfo($path);
        return view('web.commercial_centers.commercial_center_detail', $commercialCenterInfo);
    }
    public function edit()
    {
    }
    public function update()
    {
    }
    public function destroy()
    {
    }
}
