<?php

namespace App\Http\Controllers\Web;

use App\Models\Product;
use Illuminate\Http\Request;

class ProductQAController extends QABaseController
{
    public function __construct(Product $model)
    {
        $this->model = $model;
    }
}
