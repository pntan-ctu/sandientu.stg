<?php

namespace App\Http\Controllers\Web;

use App\Models\NewsGroup;
use App\Models\NewsArticle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Constants;

class NewsGroupsWebController extends Controller
{
    public function getNews($pageSize)
    {
        $arrNews = NewsArticle::where('status', '=', 1)
            ->where('group_id', '<>', Constants::PREFIX_NEWS_GROUP_ID)
            ->orderBy('created_at', 'desc')->paginate($pageSize);
        return $arrNews;
    }

    public function index()
    {
        $arrNews = self::getNews(9);
        return view('web.news.news_groups', ["NewsArticle" => $arrNews]);
    }

    public function create()
    {
    }

    public function store()
    {
    }

    public function update()
    {
    }

    public function show($path)
    {
        $newsGroup = NewsGroup::where('path', $path)->firstOrFail();
        $arrGroups = array($newsGroup->id);
        $newsGroups = NewsGroup::all();
        $this->createGroups($arrGroups, $newsGroups, $newsGroup->id);
        $arrNews = NewsArticle::where('status', '=', 1)
            ->where('group_id', '<>', Constants::PREFIX_NEWS_GROUP_ID)
            ->whereIn('group_id', $arrGroups)->orderBy('created_at', 'desc')
            ->paginate(9);

        return view('web.news.news_groups', ["NewsArticle" => $arrNews]);
    }

    public function createGroups(&$arrGroups, $newsGroups, $parent_id)
    {
        foreach ($newsGroups as $newsGroup) {
            if ($newsGroup->parent_id == $parent_id) {
                $arrGroups[] = $newsGroup->id;
                $this->createGroups($arrGroups, $newsGroups, $newsGroup->id);
            }
        }
    }

    public function edit()
    {
    }

    public function destroy()
    {
    }
}
