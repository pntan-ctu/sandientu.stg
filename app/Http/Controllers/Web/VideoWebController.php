<?php

namespace App\Http\Controllers\Web;

use App\Http\Controllers\Controller;
use App\Models\Video;

class VideoWebController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }
    public function index()
    {
        $video = Video::where('active', 1)->orderBy('created_at', 'desc')->get();
        return view('web.video.index', ['videos'=>$video]);
    }
    public function showVideo($path)
    {
        $video = Video::where('path', $path)->first();
        $videoSames = Video::where('path', '<>', $path)->get();
        return view('web.video.view_video', ['video' => $video, 'videoSames'=>$videoSames]);
    }
}
