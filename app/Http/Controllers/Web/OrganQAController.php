<?php

namespace App\Http\Controllers\Web;

use App\Models\Organization;
use Illuminate\Http\Request;

class OrganQAController extends QABaseController
{
    public function __construct(Organization $model)
    {
        $this->model = $model;
    }
}
