<?php

namespace App\Http\Controllers\Org;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SlideShow;
use App\Models\NewsArticle;
use App\Models\Organization;
use App\Models\Product;
use App\Models\User;
use App\Models\Branch;
use App\Models\CommercialCenter;
use App\Models\Advertising;
use App\Models\QuestionAnswer;
use App\Models\Rating;
use App\Models\Infringement;
use App\Models\Order;
use App\Models\Contact;
use App\OrgType;
use Illuminate\Support\Facades\Cache;
use App\Business\MapsBussiness;
use App\Authors\OrgAuthor;

class HomeController extends Controller
{
    protected $orgAuthor;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function home(int $organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canDashboard($organization);
        
        $order1 = Order::where('orders.provider_organization_id', '=', $organizationId)
                ->where('status', '=', '1')->get();
        $order2 = Order::where('orders.provider_organization_id', '=', $organizationId)
                ->where('status', '=', '2')->get();
        $order3 = Order::where('orders.provider_organization_id', '=', $organizationId)
                ->where('status', '=', '3')->get();
        $order4 = Order::where('orders.provider_organization_id', '=', $organizationId)
                ->where('status', '=', '4')->get();

        return view('org/home', ["organization" => $organization,
            "order1"=> count($order1),"order2"=> count($order2), "order3"=> count($order3), "order4"=> count($order4)]);
    }
}
