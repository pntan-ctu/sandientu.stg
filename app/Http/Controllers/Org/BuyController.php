<?php

namespace App\Http\Controllers\Org;

use App\Models\Certificate;
use App\Models\CertificateCategory;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Organization;
use App\Models\Payment;
use App\Utils\UploadFileUtil;
use App\Business\OrderBusiness;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Auth\Access\AuthorizationException;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class BuyController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_EXCHANGE;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        return view(
            'org.buys.index',
            ['organizationId' => $organizationId,
                'organization' => $organization]
        );
    }

    public function indexData($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return OrderBusiness::getOrderOfOrganizations(
            $organizationId,
            $request->input('status'),
            $request->input('keyword')
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function getBuy($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $data = Order::find($request->id);

        //Check quyền
        if ($data == null || $data->orderable_type != Organization::class || $data->orderable_id != $organizationId) {
            throw new AuthorizationException();
        }

        if (isset($request->id) && $request->id) {
            $data["data"] = OrderItem::where("order_id", $request->id)->get();
        }
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actOrder($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $data_new = Order::find($request->id);

        //Check quyền
        if ($data_new == null
            || $data_new->orderable_type != Organization::class
            || $data_new->orderable_id != $organizationId) {
            throw new AuthorizationException();
        }

        if ($request->status == 1) {
            $data_new->status = 2;
            $data_new->save();
        } elseif ($request->status == 2) {
            $data_new->status = 4;
            $data_new->save();
        } else {
            $data_new->status = 1;
            $data_new->save();
        }
        return $data_new;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function huyOrder($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $data = Order::find($request->id);

        //Check quyền
        if ($data == null || $data->orderable_type != Organization::class || $data->orderable_id != $organizationId) {
            throw new AuthorizationException();
        }

        $paymentId = $data->payment_id ?? 0;
        $payment = Payment::find($paymentId);
        $paymentStatus = $payment->response_code ?? "";

        if ($paymentStatus == "00") {
            return ['success' => false, 'msg' => 'Không thể xóa đơn hàng đã được thanh toán'];
        }

        if ($data->status == 1) {
            $data->delete();
            return ['msg' =>  'Xoá thành công', 'check' => 1];
        } else {
            return ['msg' =>  'Trạng thái đơn hàng đã bị thay đổi', 'check' => 0];
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showOrder($organizationId, $id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $data = Order::find($id);

        //Check quyền
        if ($data == null || $data->orderable_type != Organization::class || $data->orderable_id != $organizationId) {
            throw new AuthorizationException();
        }

        $data["messages"] = $data->getMessages()->get();
        if (isset($id) && $id) {
            $data["data"] = OrderItem::where("order_id", $id)->get();
            $data["org"] = Organization::find($data["provider_organization_id"]);
        }
        $paymentId = $data->payment_id ?? 0;
        $payment = Payment::find($paymentId);
        $paymentStatus = $payment->response_code ?? "";
        return view(
            'org.buys.show-orders',
            ['organizationId' => $organizationId, 'data' => $data, 'payment_status' => $paymentStatus]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function addComment($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $order = Order::find($request->order_id);
        
        //Check quyền
        if ($order == null
            || $order->orderable_type != Organization::class
            || $order->orderable_id != $organizationId) {
            throw new AuthorizationException();
        }

        $order->addMesage($order["code"], $request->text, Organization::find($organizationId));
        return $order;
    }
}
