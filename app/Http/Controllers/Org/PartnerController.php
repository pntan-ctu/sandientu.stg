<?php

namespace App\Http\Controllers\Org;

use App\Business\PartnerBusiness;
use App\Http\Requests\StorePartnerRequest;
use App\Http\Requests\UpdatePartnerRequest;
use App\Models\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Partner;
use App\Business\RegionBusiness;
use Illuminate\Auth\Access\AuthorizationException;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class PartnerController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $organizationId, $type)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return view('org.partners.index', ['organizationId' => $organizationId, 'type' => $type]);
    }

    public function indexData(int $organizationId, $type)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return PartnerBusiness::getAll($organizationId, $type, request());
    }
    
    public function indexDataOrgLink(int $organizationId, $type)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return PartnerBusiness::getAllOrgLink($organizationId, $type, request());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $organizationId, $type)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $arrRegion = Organization::where('organization_type_id', '>=', 100)->get();
        return view('org.partners.create', ['organizationId' => $organizationId, 'type' => $type,
            'partner' => null]);
    }
    
    public function createOrg(Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($request->organizationId, $this->abilityName);

        $idsub = substr($request->strId, 1);
        $id = explode(",", $idsub);
        if (is_array($id)) {
            foreach ($id as $idOrg) {
                $partnerOrg = Organization::find($idOrg);
                $partner = new Partner();
                $partner["organization_id"] = $request->organizationId;
                $partner["type"] = $request->type;
                $partner["link_organization_id"] = $idOrg;
                $partner["name"] = $partnerOrg->name;
                $partner["address"] = $partnerOrg->address;
                $partner["tel"] = $partnerOrg->tel;
                $partner["email"] = $partnerOrg->email;
                $partner["website"] = $partnerOrg->website;
                $partner->save();
            }
        } else {
            $partnerOrg = Organization::find($id);
            $partner = new Partner();
            $partner["organization_id"] = $request->organizationId;
            $partner["type"] = $request->type;
            $partner["link_organization_id"] = $id;
            $partner["name"] = $partnerOrg->name;
            $partner["address"] = $partnerOrg->address;
            $partner["tel"] = $partnerOrg->tel;
            $partner["email"] = $partnerOrg->email;
            $partner["website"] = $partnerOrg->website;
            $partner->save();
        }
        echo json_encode(array('success'=>true));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(int $organizationId, $type, StorePartnerRequest $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $partner = new Partner();
        $partner["organization_id"] = $organizationId;
        $partner["name"] = $request->name;
        $partner["address"] = $request->address;
        $partner["tel"] = $request->tel;
        $partner["email"] = $request->email;
        $partner["website"] = $request->website;
        $partner['type'] = $type;
        $partner->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(int $organizationId, Partner $partner)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(int $organizationId, $type, Partner $partner)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        //Check quyền
        if ($partner->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $arrRegion = Organization::where('organization_type_id', '>=', 100)->get();
        return view('org.partners.create', ['organizationId' => $organizationId, 'type' => $type,
            'partner' => $partner]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(int $organizationId, $type, UpdatePartnerRequest $request, Partner $partner)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        //Check quyền
        if ($partner->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $partner["link_organization_id"] = $request->link_organization_id;
        $partner["name"] = $request->name;
        $partner["address"] = $request->address;
        $partner["tel"] = $request->tel;
        $partner["email"] = $request->email;
        $partner["website"] = $request->website;
        $partner->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $organizationId, $type, Partner $partner)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        //Check quyền
        if ($partner->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $partner->delete();
    }
}
