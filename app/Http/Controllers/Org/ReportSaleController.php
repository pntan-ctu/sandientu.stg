<?php

namespace App\Http\Controllers\Org;

use App\Models\Organization;
use App\Models\Product;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class ReportSaleController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_MANAGE;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $organizationId)
    {
        $organization = Organization::find($organizationId);
        
        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        return view('org.report_sale.index', ['organizationId' => $organizationId,'organization' => $organization]);
    }

    public function indexData(int $organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $model = Order::join('order_items', 'orders.id', '=', 'order_items.order_id')
                ->join('products', 'order_items.product_id', '=', 'products.id')
                ->where('orders.provider_organization_id', '=', $organizationId)
                ->whereIn('orders.status', [2, 4])
                ->selectRaw('products.id as id,
                    products.name as name,
                    sum(order_items.amount) as amount,
                    sum(order_items.money) as money')
                ->groupBy('products.id');
        $data = DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('from_date')) {
                $fromDate = parseDate($request->from_date);
                $query->where('orders.order_date', '>=', $fromDate);
            }

            if ($request->filled('to_date')) {
                $toDate = parseDate_end($request->to_date);
                $query->where('orders.order_date', '<=', $toDate);
            }
        })->make();

        $mainData = $data->original['data'];

        $start = request('start');
        foreach ($mainData as $i => $j) {
            $mainData[$i]['stt'] = $start + $i + 1;
        }
        $data = json_decode(json_encode($data), true);
        $data['original']['data'] = $mainData;
        return $data['original'];
    }
    
    public function exportExcel($organizationId, Request $request)
    {
        $org = Organization::find($organizationId);
        $time = "";

        //Check quyền
        $this->orgAuthor->canManageObj($org, $this->abilityName);

        $data = Order::join('order_items', 'orders.id', '=', 'order_items.order_id')
                ->join('products', 'order_items.product_id', '=', 'products.id')
                ->where('orders.provider_organization_id', '=', $organizationId)
                ->whereIn('orders.status', [2, 4]);
                
        if ($request->filled('from_date')) {
            $fromDate = parseDate($request->from_date);
            $data = $data->where('orders.order_date', '>=', $fromDate);
            $time = "Từ ngày ".$request->from_date;
        }
        if ($request->filled('to_date')) {
            $toDate = parseDate_end($request->to_date);
            $data = $data->where('orders.order_date', '<=', $toDate);
            $time .= ($request->filled('from_date') ? " đ" : " Đ")."ến ngày ".$request->to_date;
        }
        $data = $data->selectRaw('products.id as id,
                    products.name as name,
                    sum(order_items.amount) as amount,
                    sum(order_items.money) as money')
                ->groupBy('products.id')
                ->orderBy("money", "desc")->get();
        
        $fileName = "tk_san_pham_cskdsx.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/org/report_sale/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);
        $sheet->setCellValue("A1", $org->name);
        $sheet->setCellValue("A4", $time);

        $i = 1;
        $row = 6;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['name']);
            $sheet->setCellValue("C$row", $d['amount']);
            $sheet->setCellValue("D$row", $d['money']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A7:D$row")->applyFromArray($style);

        $fileType="Xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName);
        header('Cache-Control: max-age=0');

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
    }
}
