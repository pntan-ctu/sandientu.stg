<?php

namespace App\Http\Controllers\Org;

use App\Business\OrganizationBusiness;
use App\Business\AreaBusiness;
use App\Business\CommercialCenterBusiness;
use App\Business\DepartmentManageBussiness;
use App\Business\RegionBusiness;
use App\Constants;
use App\Http\Controllers\Manage\OrganizationController;
use App\Http\Requests\UpdateOrganizationInfoRequest;
use App\Models\Organization;
use App\Models\OrganizationInfoDetail;
use App\Models\OrganizationLevel;
use App\OrgType;
use App\Utils\CommonUtil;
use App\Utils\UploadFileUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class InfoOrgController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    public function editGeneralInfo(int $organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $regions = RegionBusiness::getTreeWithParent();//$organization->manageOrganization->region_id
        if (isset($organization->area_id)) {
            $type = "CSSX";
            $areas = AreaBusiness::lists();
            $commercialCenters = null;
        } else {
            $type = "CSKD";
            $commercialCenters = CommercialCenterBusiness::lists();
            $areas = null;
        }

        $departments = DepartmentManageBussiness::renderRadio();
        if (isset($organization->map_lat)) {
            $organization->map = $organization->map_lat . ", " . $organization->map_long;
        }

        $organizationLevel = OrganizationLevel::get();
        $orgTypeTree = OrganizationBusiness::getTreeOrgGov();

        $status = Constants::ORG_STATUS;
        if ($organization->status == 0) {
            $status = [0 => $status[0]];
        } elseif ($organization->status > 2) {
            $status = [3 => $status[3]];
        } else {
            unset($status[0]);
            unset($status[3]);
        }
        return view(
            'org.info.create_general_info',
            ['organization' => $organization,
                'type' => $type,
                'orgTypeTree' => $orgTypeTree,
                'regions' => $regions,
                'areas' => $areas,
                'commercialCenters' => $commercialCenters,
                'departments' => $departments,
                'organizationLevel' => $organizationLevel,
                'founding_type' => Constants::FOUNDING_TYPE,
                'status' => $status
            ]
        );
    }

    public function updateGeneralInfo(UpdateOrganizationInfoRequest $request, int $organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $organization->fill($request->all());
        //Co so da duyet thi khong duoc thay doi ten
        if ($organization->status > 0) {
            unset($organization->name);
            unset($organization->department_manage_id);
            unset($organization->founding_type);
            unset($organization->founding_number);
            unset($organization->founding_by_gov);
            unset($organization->founding_date);
        } else {
            //Phai vaidate ở đây do 2 fiel nay phai dat disabled khi da duyet nen ko submit ve server
            $this->validate(
                $request,
                ['manage_organization_id' => 'required',
                'department_manage_id' => 'required',
                'founding_type' => 'required'],
                ['required' => 'Thông tin :attribute chưa được nhập'],
                ['manage_organization_id' => 'Cơ quan tiếp nhận',
                'department_manage_id' => 'Ngành quản lý',
                'founding_type' => 'Loại giấy tờ']
            );

            $organization->manage_organization_id = $request->manage_organization_id;
        }
        //CS chua duyet hoac da bi kho vi pham thi khong dc cap nhat trang thai
        if ($organization->status <= 0 || $organization->status > 2) {
            unset($organization->status);
        }
        $validator = CommonUtil::validateFoundingNumber($request, $organizationId);
        if ($validator->fails()) {
            return redirect(route('generalInfo.create', [$organizationId]))
                ->withErrors($validator)
                ->withInput();
        }
        $validator = CommonUtil::validateMap($request, $organization);
        if ($validator->fails()) {
            return redirect(route('generalInfo.create', [$organizationId]))
                ->withErrors($validator)
                ->withInput();
        }
        if ($request->hasFile('logo_image')) {
            $upload = UploadFileUtil::uploadImage($request, 'logo_image');
            $organization->logo_image = $upload->path;
        }
        $organization->save();
        return redirect(route('generalInfo.create', [$organizationId]));
    }

    public function createIntroInfo(int $organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $orgInfoConfig = $this->getOrgInfoConfig($organizationId, 100);
        return view('org/info/create_intro_info', [
            'info' => null, 'orgInfoConfig' => $orgInfoConfig, 'organizationId' => $organizationId
        ]);
    }

    public function storeIntroInfo(Request $request, int $organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $content = $request['content'];
        $key = $request['key'];
        if (isset($content)) {
            $orgInfo = [];
            foreach ($content as $k => $item) {
                $orgInfo[] = ['organization_id' => $organizationId, 'content' => $item, 'key' => $key[$k]];
            }
            OrganizationInfoDetail::insert($orgInfo);
        }

        return redirect()->route('introInfo.create', [$organizationId]);
    }

    public function updateIntroInfo(Request $request, $organizationId, $orgInfoDetail_id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        OrganizationInfoDetail::where('organization_id', $organizationId)->delete();
        $content = $request['content'];
        $key = $request['key'];
        if (isset($content)) {
            $orgInfo = [];
            foreach ($content as $k => $item) {
                $orgInfo[] = ['organization_id' => $organizationId, 'content' => $item, 'key' => $key[$k]];
            }
            OrganizationInfoDetail::insert($orgInfo);
        }

        return redirect()->route('introInfo.create', [$organizationId]);
    }

    public function getOrgInfoConfig($organization_id, $organization_type_id)
    {
        $orgInfoConfig = DB::table("organization_info_cfgs")
            ->leftJoin('organization_info_details', function ($join) use ($organization_id) {
                $join->on('organization_info_cfgs.key', '=', 'organization_info_details.key')
                    ->where('organization_info_details.organization_id', '=', $organization_id);
            })->where('organization_info_cfgs.organization_type_id', '=', $organization_type_id)
            ->get([
                'organization_info_details.id', 'organization_info_cfgs.key', 'organization_info_details.content',
                'organization_info_cfgs.name']);

        return $orgInfoConfig;
    }

//    public function validateMap($request, $organization)
//    {
//        $validator = Validator::make($request->all(), []);
//        $validator->after(function ($validator) use ($request, $organization) {
//            if (isset($request->map)) {
//                $arrMap = explode(",", $request->map);
//                if (count($arrMap) != 2) {
//                    $organization->map_lat = null;
//                    $organization->map_long = null;
//                    $validator->errors()
//                        ->add('map', 'Định dạng trường Bản đồ không đúng. Định dạng đúng: 19.808241, 105.777592');
//                } elseif (trim($arrMap[0]) != "" && trim($arrMap[1]) != "") {
//                    $organization->map_lat = floatval(trim($arrMap[0]));
//                    $organization->map_long = floatval(trim($arrMap[1]));
//                    if ($organization->map_lat < -90.0 || $organization->map_lat > 90.0 ||
//                        strlen($organization->map_lat) > 9) {
//                        $validator->errors()
//                            ->add('map', 'Trường ' . $organization->map_lat .
//                                ' phải có độ dài <= 8, có giá trị <= 90.00 và >= -90.00');
//                    } elseif ($organization->map_long < -180.0 || $organization->map_long > 180.0 ||
//                        strlen($organization->map_long) > 10) {
//                        $validator->errors()
//                            ->add('map', 'Trường ' . $organization->map_long .
//                                ' phải có độ dài <= 9, có giá trị <= 180.00 và >= -180.00');
//                    }
//                }
//            }
//        });
//        return $validator;
//    }
}
