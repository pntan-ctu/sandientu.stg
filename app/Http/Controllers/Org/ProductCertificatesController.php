<?php

namespace App\Http\Controllers\Org;

use App\Models\Certificate;
use App\Models\CertificateCategory;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductCertificatesController extends CertificatesBaseController
{

    public function __construct(Product $model)
    {
        parent::__construct();
        $this->model = $model;
    }

    public function create($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $typeId = $request->route('type_id', $organizationId);
        $certificateCategory = CertificateCategory::where('type', 1)->get();
        return view(
            'org.certificates.create_pro',
            ['certificateCategory' => $certificateCategory, 'cert' => null,
                'organizationId' => $organizationId, 'type_id' => $typeId]
        );
    }

    public function edit($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $typeId = $request->route('type_id', $organizationId);
        $certificateId = $request->route('certificate', null);
        $certificateCategory = CertificateCategory::where('type', 1)->get();
        $certificate = Certificate::where('id', $certificateId)->get()->first();

        //Check quyền certificate co thuoc SP ma user co quyen quan ly
        $this->orgAuthor->canManageObj($certificate->certificateable->organization, $this->abilityName);

        return view(
            'org.certificates.create_pro',
            ['certificateCategory' => $certificateCategory,
                'cert' => $certificate,
                'organizationId' => $organizationId, 'type_id' => $typeId]
        );
    }
}
