<?php

namespace App\Http\Controllers\Org; 

use App\Http\Controllers\Web\ShoppingCartController;
use App\Models\Certificate;
use App\Models\CertificateCategory;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Organization;
use App\Models\Payment;
use App\Utils\UploadFileUtil;
use App\Business\OrderBusiness;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;
use Illuminate\Auth\Access\AuthorizationException;

class OrdersController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_EXCHANGE;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        return view('org.orders.index', ['organizationId' => $organizationId, 'organization' => $organization]);
    }

    public function indexData($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return OrderBusiness::getOrderOfProviders(
            $organizationId,
            $request->input('status'),
            $request->input('keyword')
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getOrder($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $data = Order::find($request->id);
        $payment = Payment::find($data['payment_id']);
        //Check quyền
        if ($data == null || $data->provider_organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        if (isset($request->id) && $request->id) {
            $data["data"] = OrderItem::where("order_id", $request->id)->get();
        }
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function actOrder($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $data = Order::find($request->id);

        //Check quyền
        if ($data == null || $data->provider_organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        if ($request->status == 1) {
            $data->status = 2;
            $data->save();
        } else {
            $data->status = 1;
            $data->save();
        }
        return $data;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function huyOrder($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $data = Order::find($request->id);

        //Check quyền
        if ($data == null || $data->provider_organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $paymentId = $data->payment_id ?? 0;
        $payment = Payment::find($paymentId);
        $paymentStatus = $payment->response_code ?? "";
        if ($paymentStatus == "00") {
            return ['success' => false, 'msg' => 'Không thể xóa đơn hàng đã được thanh toán', 'check' => 0];
        }
        $data->status = 3;

        DB::beginTransaction();
        if ($data->ship_type_id == 3 || $data->ship_type_id == 4) {
            $rsCallServiceVNPost = ShoppingCartController::cancelTransportRequest($data->id);
            $rs = $rsCallServiceVNPost['success'] ?? false;
            if (!$rs) {
                return $rsCallServiceVNPost;
            }
        }
        $data->save();
        DB::commit();

        return ["success" => true, 'msg' =>  'Xoá thành công', 'check' => 1];
    }


    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function showOrder($organizationId, $id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $data = Order::find($id);
         $payment = Payment::find($data['payment_id']);
        //Check quyền
        if ($data == null || $data->provider_organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $data["messages"] = $data->getMessages()->get();
        if (isset($id) && $id) {
            $data["data"] = OrderItem::where("order_id", $id)->get();
            $data["org"] = Organization::find($data["provider_organization_id"]);
        }
        $paymentId = $data->payment_id ?? 0;
        $payment = Payment::find($paymentId);
        $paymentStatus = $payment->response_code ?? "";

        return view(
            'org.orders.show-orders',
            ['organizationId' => $organizationId, 'data' => $data, 'payment_status' => $paymentStatus,'payment'=>$payment]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function addComment($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $order = Order::find($request->order_id);
        
        //Check quyền
        if ($order == null || $order->provider_organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $order->addMesage($order["code"], $request->text, Organization::find($organizationId));
        return $order;
    }
}
