<?php

namespace App\Http\Controllers\Org;

use App\Models\PayType;
use App\Models\ShipType;
use App\Models\OrganizationPayType;
use App\Models\OrganizationShipType;
use App\Models\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOrganizationPayTypeRequest;
use App\Http\Requests\UpdateOrganizationPayTypeRequest;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Access\AuthorizationException;

class OrganizationPayTypeController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $pay = OrganizationPayType::where('organization_pay_types.organization_id', '=', $organizationId)
        ->join('pay_types', 'organization_pay_types.pay_type_id', '=', 'pay_types.id')
        ->select(
            'organization_pay_types.id as id',
            'organization_pay_types.content as content',
            'organization_pay_types.pay_type_id as pay_type_id',
            'pay_types.name as pay_types_name',
            'organization_pay_types.is_default'
        )->get();

        $ship = OrganizationShipType::where('organization_ship_types.organization_id', '=', $organizationId)
        ->join('ship_types', 'organization_ship_types.ship_type_id', '=', 'ship_types.id')
        ->select(
            'organization_ship_types.id as id',
            'organization_ship_types.content as content',
            'organization_ship_types.ship_type_id as ship_type_id',
            'ship_types.name as ship_types_name',
            'organization_ship_types.is_default'
        )->get();

        return view('org.pay_ship.index', ['organizationId' => $organizationId,
            'organization' => $organization,'pay'=>$pay,'ship' =>$ship]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $payType = PayType::select('*')->get();
        return view('org.pay_ship.create', ['organizationId' => $organizationId,
            'organization' => $organization,'payType' => $payType]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organizationId, StoreOrganizationPayTypeRequest $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $pay = new OrganizationPayType();
        $pay->pay_type_id = $request->pay_type_id;
        $pay->content = $request->content;
        $pay->organization_id = $organizationId;
        $isDefault = $request->is_default ?? 0;
        $pay->is_default = $isDefault;

        DB::beginTransaction();
        if ($isDefault == 1) {
            DB::table('organization_pay_types')
                ->where('organization_id', '=', $pay->organization_id)
                ->update(['is_default' => 0]);
        }
        $pay->save();
        DB::commit();

        return redirect('business/'.$organizationId.'/pay-ship');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrganizationPayType  $organizationPayType
     * @return \Illuminate\Http\Response
     */
    public function show(OrganizationPayType $organizationPayType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrganizationPayType  $organizationPayType
     * @return \Illuminate\Http\Response
     */
    public function edit(int $organizationId, $id)
    {
        $organization = Organization::find($organizationId);
        
        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $payType = PayType::all();
        $organizationPayType = OrganizationPayType::find($id);

        //Check quyền
        if ($organizationPayType == null || $organizationPayType->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        return view('org.pay_ship.edit', ['organizationId' => $organizationId,
            'organization' => $organization,'payType' => $payType,
            'organizationPayType' =>$organizationPayType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrganizationPayType  $organizationPayType
     * @return \Illuminate\Http\Response
     */
    public function update(int $organizationId, int $id, UpdateOrganizationPayTypeRequest $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $organizationPayType = OrganizationPayType::find($id);

        //Check quyền
        if ($organizationPayType == null || $organizationPayType->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $organizationPayType->pay_type_id = $request->pay_type_id;
        $organizationPayType->content = $request->content;
        $isDefault = $request->is_default ?? 0;
        $organizationPayType->is_default = $isDefault;

        DB::beginTransaction();
        if ($isDefault == 1) {
            DB::table('organization_pay_types')
                ->where('organization_id', '=', $organizationPayType->organization_id)
                ->where('id', '!=', $id)
                ->update(['is_default' => 0]);
        }
        $organizationPayType->save();
        DB::commit();

        return redirect('business/'.$organizationId.'/pay-ship');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrganizationPayType  $organizationPayType
     * @return \Illuminate\Http\Response
     */
    public function destroy($organizationId, $id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $organizationPayType = OrganizationPayType::find($id);

        //Check quyền
        if ($organizationPayType == null || $organizationPayType->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $organizationPayType->delete();
        
        return redirect('business/'.$organizationId.'/pay-ship');
    }
}
