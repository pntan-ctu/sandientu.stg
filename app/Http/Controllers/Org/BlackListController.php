<?php

namespace App\Http\Controllers\Org;

use App\Models\Blacklist;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Organization;
use App\Models\User;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Auth\Access\AuthorizationException;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class BlackListController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_MANAGE;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        return view('org.black_list.index', ['organizationId' => $organizationId, 'organization' => $organization]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function indexData(int $organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $model = Blacklist::where('blacklists.organization_id', '=', $organizationId)
        ->select(
            'blacklists.user_id as user_id',
            'blacklists.id as id',
            'blacklists.reason as reason',
            'users.name as user_name'
        )
        ->join('users', 'blacklists.user_id', '=', 'users.id');

        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('users.name', 'like', '%' . $keyword . '%');
                });
            }
        })->make();
    }

    /**
     * Show the form for create the specified resource.
     *
     * @param  \App\Models\Blacklist  $blacklist
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Blacklist  $blacklist
     * @return \Illuminate\Http\Response
     */
    public function show(Blacklist $blacklist)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Blacklist  $blacklist
     * @return \Illuminate\Http\Response
     */
    public function edit(Blacklist $blacklist)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Blacklist  $blacklist
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blacklist $blacklist)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Blacklist  $blacklist
     * @return \Illuminate\Http\Response
     */
    public function destroy($organizationId, $Id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $blacklist = Blacklist::find($Id);

        //Check quyền
        if ($blacklist == null || $blacklist->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $blacklist->delete();
    }
}
