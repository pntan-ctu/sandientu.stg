<?php

namespace App\Http\Controllers\Org;

use App\Business\CertBusiness;
use App\Http\Requests\StoreCertRequest;
use App\Http\Requests\UpdateCertRequest;
use App\Models\Certificate;
use App\Models\Organization;
use App\Models\Product;
use App\Utils\UploadFileUtil;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class CertificatesBaseController extends Controller
{
    protected $model;
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    public function index($organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        return view(
            'org.certificates.index',
            ['organizationId' => $organizationId, 'organization' => $organization]
        );
    }

    public function indexData($organizationId, Request $request)
    {
        //Check quyền, bỏ vì đã check ở dưới
        //$this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $typeId = $request->route('type_id', $organizationId);
        $type = $this->model->find($typeId);

        //Check quyền
        if ($type instanceof Organization) {
            $this->orgAuthor->canManageObj($type, $this->abilityName);
        } elseif ($type instanceof Product) {
            $this->orgAuthor->canManageObj($type->organization, $this->abilityName);
        }

        return CertBusiness::getCertOfOrganizations(
            $this->model,
            $typeId,
            request()->input('status'),
            request()->input('keyword')
        );
    }

    public function store($organizationId, StoreCertRequest $request)
    {
        //Check quyền, bỏ vì đã check ở dưới
        //$this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $typeId = $request->route('type_id', $organizationId);
        $type = $this->model->find($typeId);

        //Check quyền
        if ($type instanceof Organization) {
            $this->orgAuthor->canManageObj($type, $this->abilityName);
        } elseif ($type instanceof Product) {
            $this->orgAuthor->canManageObj($type->organization, $this->abilityName);
        }

        if (isset($request->image)) {
            $file = UploadFileUtil::UploadImage($request, 'image');
            $image = $file->path;
        }

        $type->certificates()->create([
            "certificate_category_id" => $request->certificate_category_id,
            "title" => $request->title,
            "description" => $request->description,
            "image" => isset($image) ? $image : '',
        ]);
    }

    public function show($id)
    {
        //
    }

    public function update($organizationId, UpdateCertRequest $request)
    {
        //Check quyền, bỏ vì đã check ở dưới
        //$this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $typeId = $request->route('type_id', $organizationId);
        $type = $this->model::find($typeId);

        //Check quyền SP hoac CS co quyen quan ly
        if ($type instanceof Organization) {
            $this->orgAuthor->canManageObj($type, $this->abilityName);
        } elseif ($type instanceof Product) {
            $this->orgAuthor->canManageObj($type->organization, $this->abilityName);
        }

        $certificateId = $request->route(
            'certificate',
            $certificateId = $request->route('certificates_org', null)
        );

        $certificate = Certificate::where('id', $certificateId)->get()->first();

        if ($certificate == null) {
            abort(404);
        }

        //Check quyền certificate co thuoc SP hoac CS ma user co quyen quan ly
        if ($certificate->certificateable instanceof Organization) {
            $this->orgAuthor->canManageObj($certificate->certificateable, $this->abilityName);
        } elseif ($certificate->certificateable instanceof Product) {
            $this->orgAuthor->canManageObj($certificate->certificateable->organization, $this->abilityName);
        }

        $certificate->fill($request->all());
        if (isset($request->image) && $request->image) {
            $file = UploadFileUtil::UploadImage($request, 'image');
            $certificate["image"] = $file->path;
        }
        $type->certificates()->save($certificate);
    }

    public function destroy($organizationId, Request $request)
    {
        //Check quyền, không cho đứng ở một cơ sở để quét các certifile của cơ sở khác
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $certificateId = $request->route(
            'certificate',
            $certificateId = $request->route('certificates_org', null)
        );

        $certificate = Certificate::where('id', $certificateId)->get()->first();

        if ($certificate == null) {
            abort(404);
        }

        //Check quyền certificate co thuoc SP hoac CS ma user co quyen quan ly
        if ($certificate->certificateable instanceof Organization) {
            $this->orgAuthor->canManageObj($certificate->certificateable, $this->abilityName);
        } elseif ($certificate->certificateable instanceof Product) {
            $this->orgAuthor->canManageObj($certificate->certificateable->organization, $this->abilityName);
        }

        $certificate->delete();
    }
}
