<?php

namespace App\Http\Controllers\Org;

use App\Models\Organization;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Web\MessageController;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class OrgMessageController extends MessageController
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->model = 'App\Models\Organization';
        $this->orgAuthor = new OrgAuthor();
    }

    /*
     * @author: Nguyễn Quân
     * Gửi tin nhắn cho tất cả những người theo dõi cơ sở.
     * Có thể gửi tin nhắn với vai trò là Co sở hoặc vai trò cá nhân
     */
    public function sendMessageToFollowers(int $organizationId, Request $request)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $subject = $request->msg_subject;
        $content = $request->msg_content;

        $obj = getAccessingObject(); // có thể là Org hoặc User
        $followers = $organization->follows;
        foreach ($followers as $item) {
            $user = User::find($item->user_id);
            if ($user != null) {
                //$obj->sendMesage($subject, $content, $user);
                $user->addMesage($subject, $content, $obj);
            }
        }
    }
}
