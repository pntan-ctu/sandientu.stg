<?php

namespace App\Http\Controllers\Org;

use App\Business\CertBusiness;
use App\Http\Controllers\Manage\ProductCategoryController;
use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest; 
use App\Models\CertificateCategory;
use App\Models\Product;
use App\Models\ProductImage;
use App\Models\ProductInfoDetail;
use App\Models\ProductLink;
use App\Utils\UploadFileUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Auth\Access\AuthorizationException;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;
use App\Business\ProductBussiness;

class ProductController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    public function index(int $organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return view('org.product.index', ['organizationId' => $organizationId]);
    }

    public function create($organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $arr = ProductCategory::get(['id', 'parent_id', 'name']);
        $productCategoryController = new ProductCategoryController();
        $tree = $productCategoryController->getTreeCategory($arr);
        // $status = [
        //     0 => 'Chưa kinh doanh', 1 => 'Còn hàng', 2 => 'Theo mùa vụ',
        //     3 => 'Hết hàng', 4 => 'Ngừng kinh doanh'
        // ];
        $status = [
            0 => 'Chưa kinh doanh', 1 => 'Còn hàng',
            3 => 'Hết hàng', 4 => 'Ngừng kinh doanh'
        ];
        $loaitour = [
            0 => 'Trong nước', 1 => 'Nước Ngoài',
            2 => 'Tự chọn', 3 => 'Booking', 4 => 'Thân thiết'
        ];
        $hinhthuctraluong = [
            0 => "Theo tháng" , 1 => "Theo ngày", 2 => "Theo tuần"
        ];
        $gioitinh = [
             0 => "Nam" , 1 => "Nữ", 2 => "Không phân biệt giới tính"
        ];
        $trinhdo = [
            0 => "Không yêu cầu" , 1 => "Trung Học Phổ Thông", 2 => "Trung Cấp", 3 => "Cao đẳng", 4 => "Đại học", 5 => "Sau đại học"
       ];
       $loaicongviec = [
        0 => "Toàn thời gian" , 1 => "Bán thời gian", 2 => "Online"
   ];
        //        if (isUserGov()) {
        //            $status[4] = 'Khóa';
        //        }
        return view('org.product.create', [
            'tree' => $tree, 'product' => null,
            "organizationId" => $organizationId,
            'listStatus' => $status, 'loaitour' =>$loaitour, 'listhinhthucluong' => $hinhthuctraluong, 'yeucaugioitinh' => $gioitinh, 'yeucautrinhdo' => $trinhdo,
            'loaicongviec' => $loaicongviec
        ]);
    }

    public function setProduct($organizationId, $product, $request, $check_temp_image)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);
        
        $product->fill($request->all());
        if ($product->status == 2) {
            $product->duration = $request->duration;
        } else {
            $product->duration = null;
        }
        $max = Product::max('id') + 1;
        $product->code = 'SP' . str_pad("$max", 8, "0", STR_PAD_LEFT);
        $product->organization_id = $organizationId;
        $product->avatar_image = $request->path_avatar;
        $product->it5_vitri = $request->it5_vitri;
        $product->it5_dientichsudung = $request->it5_dientichsudung;
        $product->it5_sdtlienhe = $request->it5_sdtlienhe;
        $product->it5_ngangdai = $request->it5_ngangdai;
        $product->it5_giaytophaply = $request->it5_giaytophaply;
        $product->it5_maxa = $request->sl_xa;
        $product->it5_soto = $request->it5_soto;
        $product->it5_sothua = $request->it5_sothua;
        $product->it5_mucluong = $request->it5_mucluong;
        $product->it5_thoigiankhoihanh = $request->it5_thoigiankhoihanh;
        $product->it5_noikhoihanh = $request->it5_noikhoihanh;
        $product->it5_noiden = $request->it5_noiden;
        $product->it5_thoigiandidukien= $request->it5_thoigiandidukien;
        $product->it5_phuongtiendi = $request->it5_phuongtiendi;
        $product->it5_soluongtuyendung = $request->it5_soluongtuyendung;
        $product->it5_trinhdotoithieu = $request->it5_trinhdotoithieu;
        $product->it5_tuoitoida = $request->it5_tuoitoida;
        $product->it5_tuoitoithieu = $request->it5_tuoitoithieu;
        $product->check_temp_image = $check_temp_image;
        $product->it5_loaicongviec = $request->it5_loaicongviec;
        $product->id_parent = $request->id_parent;
        $product->it5_diachiduan = $request->it5_diachiduan;
        $product->it5_quymoduan = $request->it5_quymoduan;
        $product->it5_vondautuduan = $request->it5_vondautuduan;
        $product->it5_soluongdautukeugoi = $request->it5_soluongdautukeugoi;
        $product->it5_vondaututoithieu = $request->it5_vondaututoithieu;
        $product->created_by = auth()->user()->id;
        $product->save();
    }

    public function store(int $organizationId, StoreProductRequest $request)
    {
        $check_temp_image = '';
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);
        // if ($request->hasFile('check_temp_image')) {
        //     $file = UploadFileUtil::UploadImage($request, 'check_temp_image');
        //     $check_temp_image = $file->path;
        // }
        if ($request->ajax()) {
            if (Product::where('name', $request->name)
                ->where('organization_id', $organizationId)->first()
            ) {
                return response()->json(
                    ['success' => false, 'msg' => 'Tên sản phẩm này đã tồn tại trên cơ sở của bạn']
                );
            }
            //Save product
            $product = new Product();
            $this->setProduct($organizationId, $product, $request, $check_temp_image);

            //Save product_image
            $arr = [];
            $product_id = $product->id;
            if (isset($request->paths)) {
                $paths = explode(",", $request->paths[0]);
                foreach ($paths as $item) {
                    if (strlen($item) > 0) {
                        $arr[] = ['product_id' => $product_id, 'image' => $item];
                    }
                }
                if (count($arr) > 0) {
                    ProductImage::insert($arr);
                }
            }

            $arrLink = [];
            if (isset($request->link_product_ids)) {
                $link_product_ids = explode(",", $request->link_product_ids[0]);
                foreach ($link_product_ids as $item) {
                    if (strlen($item) > 0) {
                        $arrLink[] = ['product_id' => $product_id, 'link_product_id' => $item];
                    }
                }
                if (count($arrLink) > 0) {
                    ProductLink::insert($arrLink);
                }
            }

            if (isset($request->descript) && strlen($request->descript) > 0) {
                $productLink = new ProductLink();
                $productLink->product_id = $product_id;
                $productLink->descript = $request->descript;
                $productLink->save();
            }
 
            return response()->json([
                'success' => true, 'product_id' => $product_id
            ]);
        }
    }

    public function storeMoreInfo($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);
        $product = Product::find($request->product_id);
        if ($product == null || $product->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        if ($request->ajax()) {
            ProductInfoDetail::where('product_id', $request->product_id)->delete();
            $content = $request['content'];
            $key = $request['key'];
            if (isset($content)) {
                $productInfo = [];
                foreach ($content as $k => $item) {
                    if (!empty($item)) {
                        $productInfo[] = ['product_id' => $request->product_id, 'content' => $item, 'key' => $key[$k]];
                    }
                }
                ProductInfoDetail::insert($productInfo);
            }
            return response()->json(['success' => true]);
        }
    }

    public function show(Product $product)
    {
        //
    }

    public function edit($organizationId, $id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $arr = ProductCategory::get(['id', 'parent_id', 'name']);
        $productCategoryController = new ProductCategoryController();
        $tree = $productCategoryController->getTreeCategory($arr);
        // $product = Product::where('id', $id)
        //     ->first(['id', 'product_category_id', 'path', 'code', 'name', 'unit',
        //         'price', 'status', 'avatar_image', 'duration', 'price_sale', 'weight_per_unit',
        //         'barcode', 'has_check', 'capacity', 'pack_style', 'preservation_method']);

        // if (!isset($product)) {
        //     return "Không tồn tại sản phẩm này";
        // }
        $product = Product::find($id);

        //Check quyền
        if ($product == null || $product->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $product_id = $product->id;
        $product_category_id = $product->product_category_id;
        $productInfoConfig = $this->getProductInfoConfig($product_id, $product_category_id);
        $certificates = CertBusiness::lists(1);
        $status = [
            0 => 'Chưa kinh doanh', 1 => 'Còn hàng', 2 => 'Theo mùa vụ',
            3 => 'Hết hàng', 4 => 'Ngừng kinh doanh'
        ];
        $loaitour = [
            0 => 'Trong nước', 1 => 'Nước Ngoài',
            2 => 'Tự chọn', 3 => 'Booking', 4 => 'Thân thiết'
        ];
        $hinhthuctraluong = [
            0 => "Theo tháng" , 1 => "Theo ngày", 2 => "Theo tuần"
        ];
        $gioitinh = [
            0 => "Nam" , 1 => "Nữ", 2 => "Không phân biệt giới tính"
        ];
        $trinhdo = [
            0 => "Không yêu cầu" , 1 => "Trung Học Phổ Thông", 2 => "Trung Cấp", 3 => "Cao đẳng", 4 => "Đại học", 5 => "Sau đại học"
       ];
       $loaicongviec = [
        0 => "Toàn thời gian" , 1 => "Bán thời gian", 2 => "Online"
   ];
        //        if (isUserGov()) {
        //            $status[4] = 'Khóa';
        //        }
        $productLink = DB::table('product_links')->where('product_id', '=', $product_id)
            ->leftJoin('products', 'product_links.link_product_id', '=', 'products.id')
            ->leftJoin('organizations', 'organizations.id', '=', 'products.organization_id')
            ->select([
                'product_links.link_product_id',
                'organizations.name as cssx', 'products.name','product_links.descript'
            ])
            ->get();
        return view(
            'org.product.create',
            [
                'tree' => $tree,
                'product' => $product,
                'loaitour' =>$loaitour,
                'productImage' => $product->ProductImages,
                'productConfigs' => $productInfoConfig,
                'organizationId' => $organizationId,
                'certificates' => $certificates,
                'listStatus' => $status,
                'productLink' => $productLink,
                'listhinhthucluong' => $hinhthuctraluong,
                'yeucaugioitinh' => $gioitinh,'yeucautrinhdo' => $trinhdo,
                'loaicongviec' => $loaicongviec
            ]
        );
    }

    public function update($organizationId, UpdateProductRequest $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        if ($request->ajax()) {
            $product = Product::where('id', $request->id)->first();

            //Check quyền
            if ($product == null || $product->organization_id != $organizationId) {
                throw new AuthorizationException();
            }

            $product->fill($request->all());
            if ($product->status == 2) {
                $product->duration = $request->duration;
            } else {
                $product->duration = null;
            }
            $product->avatar_image = $request->path_avatar;
            $product->updated_by = auth()->user()->id;
            $product->save();

            //Save product_image
            $arr = [];
            $product_id = $product->id;
            ProductImage::where('product_id', $product_id)->delete();
            if (isset($request->paths)) {
                $paths = explode(",", $request->paths[0]);
                foreach ($paths as $item) {
                    if (strlen($item) > 0) {
                        $arr[] = ['product_id' => $product_id, 'image' => $item];
                    }
                }
                if (count($arr) > 0) {
                    ProductImage::insert($arr);
                }
            }

            ProductLink::where('product_id', '=', $product_id)->delete();
            $arrLink = [];
            if (isset($request->descript) && strlen($request->descript) > 0) {
                $productLink = new ProductLink();
                $productLink->product_id = $product_id;
                $productLink->descript = $request->descript;
                $productLink->save();
            }
            if (isset($request->link_product_ids)) {
                $link_product_ids = explode(",", $request->link_product_ids[0]);
                foreach ($link_product_ids as $item) {
                    if (strlen($item) > 0) {
                        $arrLink[] = ['product_id' => $product_id, 'link_product_id' => $item];
                    }
                }
                if (count($arrLink) > 0) {
                    ProductLink::insert($arrLink);
                }
            }


            $product_category_id = $product->product_category_id;
            $productInfoConfig = $this->getProductInfoConfig($product_id, $product_category_id);
            return response()->json([
                'success' => true, 'product_id' => $product_id,
                'moreInfo' => view(
                    'org/product/moreInfo',
                    [
                        'product_id' => $product_id, 'productConfigs' => $productInfoConfig,
                        'organizationId' => $organizationId
                    ]
                )->render()
            ]);
        }
    }

    public function destroy($organizationId, Product $product)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        //Check quyền
        if ($product->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $product->delete();
        ProductLink::where('link_product_id', $product->id)->delete();
        return response()->json(['status' => 'success', 'msg' => 'Xóa sản phẩm thành công']);
    }

    public function indexData($organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $model = Product::select(
            [
                'path',
                'id',
                'organization_id',
                'product_category_id',
                'code',
                'name',
                'unit',
                'price',
                'price_sale',
                'avatar_image',
                'status',
                'duration',
                'updated_at',
                'it5_dientichsudung',
                'it5_donvitien',
                'it5_donvidientich'
            ]
        )->where('organization_id', $organizationId);
        return DataTables::eloquent($model)->filter(function ($query) {
            if (request()->filled('keyword')) {
                $keyword = request('keyword');
                $query->where('name', 'like', '%' . $keyword . '%');
            }
        })->make();
    }

    public function indexDataCert($organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return CertBusiness::getCertOfOrganizations(
            $organizationId,
            $request->input('status'),
            $request->input('keyword')
        );
    }

    public function uploadImage(Request $request)
    {
        $images = UploadFileUtil::MultiUploadImage($request, 'photos');
        $paths = [];
        if (isset($images)) {
            foreach ($images as $image) {
                $paths[] = $image->path;
            }
        }

        return response()->json(['success' => true, 'paths' => $paths]);
    }

    public function getProductInfoConfig($product_id, $product_category_id)
    {
        $productInfoConfig = DB::table("product_info_cfgs")
            ->leftJoin('product_info_details', function ($join) use ($product_id) {
                $join->on('product_info_cfgs.key', '=', 'product_info_details.key')
                    ->where('product_info_details.product_id', '=', $product_id);
            })
            ->where('product_info_cfgs.product_category_id', '=', null)
            ->whereOr('product_info_cfgs.product_category_id', '=', $product_category_id)
            ->get([
                'product_info_details.id', 'product_info_details.product_id',
                'product_info_cfgs.key', 'product_info_details.content', 'product_info_cfgs.name'
            ]);

        return $productInfoConfig;
    }

    public function getViewMoreInfo($organizationId, Request $request)
    {
        $product_id = $request->product_id;
        $product = Product::where('id', $product_id)->get(['product_category_id'])->first();
        $product_category_id = $product->product_category_id;
        $productInfoConfig = $this->getProductInfoConfig($product_id, $product_category_id);
        return response()->json(['moreInfo' => view(
            'org/product/moreInfo',
            [
                'product_id' => $product_id,
                'productConfigs' => $productInfoConfig, 'organizationId' => $organizationId
            ]
        )->render()]);
    }

    public function getViewCertifindexDaicate($organizationId, Request $request)
    {
        return response()->json(['success' => true, 'viewCertificate' => View(
            'org/product/certificateInfo',
            ['organizationId' => $organizationId]
        )->render()]);
    }

    public function getViewCertificate($organizationId, Request $request)
    {
        return response()->json(['success' => true, 'viewCertificate' => View(
            'org/product/certificateInfo',
            ['organizationId' => $organizationId]
        )->render()]);
    }

    public function indexDataProductLink($organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $model = Product::select(
            [
                'path',
                'id',
                'organization_id',
                'product_category_id',
                'code',
                'name',
                'updated_at'
            ]
        );

        return DataTables::eloquent($model)->filter(function ($query) {
            $query->select(
                'products.path as path',
                'products.id as id',
                'products.organization_id as organization_id',
                'products.product_category_id as product_category_id',
                'products.code as code',
                'products.name as name',
                'products.updated_at as updated_at',
                'organizations.name as cssx'
            )->leftJoin('organizations', 'products.organization_id', '=', 'organizations.id');
            if (request()->filled('keyword')) {
                $keyword = request('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('products.name', 'like', '%' . $keyword . '%')
                        ->orWhere('organizations.name', 'like', '%' . $keyword . '%');
                });
            }
            $product_category_id = request('product_category_id_modal');
            if ($product_category_id != 0) {
                $query->where('products.product_category_id', '=', $product_category_id);
            }
        })->make();
    }

    public function getCatalogy($org,$id)
    {
    $arr = ProductCategory::where('id','=',$id)->orWhere('parent_id','=',$id)->get(['id', 'parent_id', 'name']);
        $productCategoryController = new ProductCategoryController();
        $tree = $productCategoryController->getTreeCategory($arr);
        return View('components.select_search')->with(array('tree' => $tree,'level'=>0,'path'=>null,'root'=>'Chọn danh mục sản phẩm','parent_id'=>null));
        
    }
}
