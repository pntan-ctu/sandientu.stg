<?php

namespace App\Http\Controllers\Org;

use App\Models\PayType;
use App\Models\ShipType;
use App\Models\OrganizationPayType;
use App\Models\OrganizationShipType;
use App\Models\Organization;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreOrganizationShipTypeRequest;
use App\Http\Requests\UpdateOrganizationShipTypeRequest;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;
use Illuminate\Support\Facades\DB;
use Illuminate\Auth\Access\AuthorizationException;

class OrganizationShipTypeController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $shipType = ShipType::select('*')->get();
        return view('org.pay_ship.create', ['organizationId' => $organizationId,
            'organization' => $organization,'shipType' => $shipType]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organizationId, StoreOrganizationShipTypeRequest $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $organizationShipType = new OrganizationShipType();
        $organizationShipType->ship_type_id = $request->ship_type_id;
        $organizationShipType->content = $request->content;
        $organizationShipType->organization_id = $organizationId;
        $isDefault = $request->is_default ?? 0;
        $organizationShipType->is_default = $isDefault;

        DB::beginTransaction();
        if ($isDefault == 1) {
            DB::table('organization_ship_types')
                ->where('organization_id', '=', $organizationShipType->organization_id)
                ->update(['is_default' => 0]);
        }
        $organizationShipType->save();
        DB::commit();

        return redirect('business/'.$organizationId.'/pay-ship');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\OrganizationShipType  $organizationShipType
     * @return \Illuminate\Http\Response
     */
    public function show(OrganizationShipType $organizationShipType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\OrganizationShipType  $organizationShipType
     * @return \Illuminate\Http\Response
     */
    public function edit(int $organizationId, $id)
    {
        $organization = Organization::find($organizationId);
        
        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $shipType = ShipType::all();
        $organizationShipType = OrganizationShipType::find($id);

        //Check quyền
        if ($organizationShipType == null || $organizationShipType->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        return view('org.pay_ship.edit', ['organizationId' => $organizationId,
            'organization' => $organization,'shipType' => $shipType,
            'organizationShipType' =>$organizationShipType]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\OrganizationShipType  $organizationShipType
     * @return \Illuminate\Http\Response
     */
    public function update(int $organizationId, UpdateOrganizationShipTypeRequest $request, $id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $organizationShipType = OrganizationShipType::find($id);

        //Check quyền
        if ($organizationShipType == null || $organizationShipType->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $organizationShipType->ship_type_id = $request->ship_type_id;
        $organizationShipType->content = $request->content;
        $isDefault = $request->is_default ?? 0;
        $organizationShipType->is_default = $isDefault;

        DB::beginTransaction();
        if ($isDefault == 1) {
            DB::table('organization_ship_types')
                ->where('organization_id', '=', $organizationShipType->organization_id)
                ->where('id', '!=', $id)
                ->update(['is_default' => 0]);
        }
        $organizationShipType->save();
        DB::commit();

        return redirect('business/'.$organizationId.'/pay-ship');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\OrganizationShipType  $organizationShipType
     * @return \Illuminate\Http\Response
     */
    public function destroy($organizationId, $id)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $organizationShipType = OrganizationShipType::find($id);

        //Check quyền
        if ($organizationShipType == null || $organizationShipType->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $organizationShipType->delete();
        
        return redirect('business/'.$organizationId.'/pay-ship');
    }
}
