<?php

namespace App\Http\Controllers\Org;

use App\Business\BranchBusiness;
use App\Http\Requests\StoreBranchRequest;
use App\Http\Requests\UpdateBranchRequest;
use App\Models\Branch;
use App\Models\Organization;
use App\Utils\UploadFileUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use App\Utils\CommonUtil;
use App\Models\Region;
use App\Business\RegionBusiness;
use Illuminate\Auth\Access\AuthorizationException;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class BranchsController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $organizationId)
    {
        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        return view('org.branchs.index', ['organizationId' => $organizationId, 'organization' => $organization]);
    }

    public function indexData(int $organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return BranchBusiness::getAll($organizationId, request());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $tree = RegionBusiness::getTreeWithParent(RegionBusiness::REGIONIDROOT);
        return view('org.branchs.create', ['none_create' => 1,
            'organizationId' => $organizationId, 'branch' => null, 'tree' => $tree]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($organizationId, StoreBranchRequest $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $branch = new Branch();
        $branch["organization_id"] = $organizationId;
        $branch["name"] = $request->name;
        $branch["address"] = $request->address;
        $branch["tel"] = $request->tel;
        $branch["email"] = $request->email;
        $branch["region_id"] = $request->region_id;
        $branch["map_lat"]=$request->map_lat;
        $branch["map_long"]=$request->map_long;

        $branch->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($organizationId, Branch $branch)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($organizationId, Branch $branch)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        //Check quyền
        if ($branch->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $tree = RegionBusiness::getTreeWithParent(RegionBusiness::REGIONIDROOT);
        if (isset($branch->map_lat)) {
            $branch->map = $branch->map_lat . ", " . $branch->map_long;
        }

        return view('org.branchs.create', ['none_create' => 2,
            'organizationId' => $organizationId, 'branch' => $branch, 'tree' => $tree]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update($organizationId, UpdateBranchRequest $request, Branch $branch)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        //Check quyền
        if ($branch->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $branch["name"] = $request->name;
        $branch["address"] = $request->address;
        $branch["tel"] = $request->tel;
        $branch["email"] = $request->email;
        $branch["region_id"] = $request->region_id;
        $branch["map_lat"]=$request->map_lat;
        $branch["map_long"]=$request->map_long;

        $branch->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($organizationId, Branch $branch)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);
        
        //Check quyền
        if ($branch->organization_id != $organizationId) {
            throw new AuthorizationException();
        }

        $branch->delete();
    }
}
