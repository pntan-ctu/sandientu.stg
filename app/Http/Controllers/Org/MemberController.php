<?php

namespace App\Http\Controllers\Org;

use App\Http\Requests\StoreMemberRequest;
use App\Http\Requests\UpdateMemberRequest;
use App\Http\Controllers\Controller;
//use App\Models\Role;
//use App\Models\UserRole;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;
use App\Models\Organization;
use Silber\Bouncer\BouncerFacade as Bouncer;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;
use Illuminate\Support\Facades\Validator;
use Illuminate\Auth\Access\AuthorizationException;

class MemberController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $organizationId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        return view('org.members.index', ['organizationId' => $organizationId]);
    }

    public function indexData(int $organizationId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $userId = getUserId();
        $model = User::select(['id', 'name', 'email', 'active', 'created_at', 'password_reset',
            DB::raw("if(id=$userId, 1, 0) as is_me")])
            ->where('organization_id', '=', $organizationId);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $key = $request->input('keyword');
                $query->where(function ($query) use ($key) {
                    $query->where('name', 'like', '%' . $key . '%')
                    ->orWhere('email', 'like', '%' . $key . '%')
                    ->orWhere('tel', 'like', '%' . $key . '%');
                });
            }
            if ($request->filled('active_search') && $request->input('active_search') != -1) {
                $status = (int) request('active_search');
                $query->where('active', '=', $status);
            }
        })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(int $organizationId)
    {
        $organization = Organization::find($organizationId);
        $orgTypeId = $organization->organization_type_id;

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        Bouncer::scope()->to($orgTypeId);
        $roles = Bouncer::role()->where('scope', '=', $orgTypeId)->get();
        return view('org.members.create', ['organizationId' => $organizationId, 'member' => null, 'roles' => $roles]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(int $organizationId, StoreMemberRequest $request)
    {
        $organization = Organization::find($organizationId);
        $orgTypeId = $organization->organization_type_id;

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $user = new User();
        $user->fill($request->all());
        $sPass = str_random(8);
        $user->password_reset = $sPass;
        $user->password = Hash::make($sPass);
        $user->organization_id = $organizationId;
        $user->save();

        //Assign roles
        Bouncer::scope()->to($orgTypeId);
        if ($request->filled('role')) {
            $roles = $request->role;
            foreach ($roles as $r) {
                $user->assign($r);
            }
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $member
     * @return \Illuminate\Http\Response
     */
    public function show(int $organizationId, User $member)
    {
        // Với chức năng phức tạp cần xây dựng action này
        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $member
     * @return \Illuminate\Http\Response
     */
    public function edit(int $organizationId, User $member)
    {
        //Không cho đứng ở cơ sở này quản lý member cơ sở khác
        if ($member->organization_id != $organizationId) {
            abort(404);
        }

        //Khong cho sua chinh user hien tai dang login, chi dc phep cap nhat thong tin ca nhan
        $currentLoginUserId = getUserId();
        if ($currentLoginUserId == $member->id) {
            throw new AuthorizationException();
        }

        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $orgTypeId = $organization->organization_type_id;
        $userId = $member->id;

        Bouncer::scope()->to($orgTypeId);
        $roles = Bouncer::role()::leftjoin("bouncer_assigned_roles as b", function ($query) use ($userId) {
            $query->on('b.role_id', '=', 'bouncer_roles.id')
                ->where('b.entity_id', '=', $userId)
                ->where('b.entity_type', '=', 'App\Models\User');
        })
        //->where('bouncer_roles.scope', '=', $orgTypeId)
        ->select(['bouncer_roles.id', 'bouncer_roles.title', 'bouncer_roles.name',
            DB::raw('if(b.role_id>0, bouncer_roles.name, "") as role_id')
        ])->get();

        return view(
            'org.members.create',
            ['organizationId' => $organizationId,
            'member' => $member,
            'roles' => $roles]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUserRequest $request
     * @param  \App\Models\User $member
     * @return \Illuminate\Http\Response
     */
    public function update(int $organizationId, UpdateMemberRequest $request, User $member)
    {
        //Không cho đứng ở cơ sở này quản lý member cơ sở khác
        if ($member->organization_id != $organizationId) {
            abort(404);
        }

        //Khong cho sua chinh user hien tai dang login, chi dc phep cap nhat thong tin ca nhan
        $currentLoginUserId = getUserId();
        if ($currentLoginUserId == $member->id) {
            throw new AuthorizationException();
        }

        $organization = Organization::find($organizationId);

        //Check quyền
        $this->orgAuthor->canManageObj($organization, $this->abilityName);

        $orgTypeId = $organization->organization_type_id;

        unset($request['email']);
        $member->fill($request->all());
        if (!$request->filled('active')) {
            $member->active = 0;
        }
        unset($member->email);
        unset($member->organization_id);

        $member->save();

        //Remove roles
        Bouncer::scope()->to($orgTypeId);
        $allRoles = Bouncer::role()->where('scope', '=', $orgTypeId)->get();
        foreach ($allRoles as $cRole) {
            $member->retract($cRole->name);
        }

        //Assign roles
        if ($request->filled('role')) {
            $roles = $request->role;
            foreach ($roles as $r) {
                $member->assign($r);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $member
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $organizationId, User $member)
    {
        //Không cho đứng ở cơ sở này quản lý member cơ sở khác
        if ($member->organization_id != $organizationId) {
            abort(404);
        }

        //Khong cho sua chinh user hien tai dang login, chi dc phep cap nhat thong tin ca nhan
        $currentLoginUserId = getUserId();
        if ($currentLoginUserId == $member->id) {
            throw new AuthorizationException();
            //return ['success' => false, 'msg' => "Không thể xóa tài khoản của chính bạn !"];
        }

        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $member->delete();
    }

    public function resetPassword(int $organizationId, $iUserId)
    {
        $member = User::find($iUserId);
        if ($member == null || $member->organization_id != $organizationId) {
            abort(404);
        }

        //Khong cho sua chinh user hien tai dang login, chi dc phep cap nhat thong tin ca nhan
        $currentLoginUserId = getUserId();
        if ($currentLoginUserId == $member->id) {
            throw new AuthorizationException();
        }

        //Check quyền
        $this->orgAuthor->canManageId($organizationId, $this->abilityName);

        $sPass = str_random(8);
        $member->password_reset = $sPass;
        $member->password = Hash::make($sPass);
        $member->save();
    }
}
