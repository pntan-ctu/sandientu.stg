<?php

namespace App\Http\Controllers\Org;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\OrgType;
use App\Models\User;
use App\Models\Organization;
use App\Authors\OrgAuthor;
use App\Authors\AbilityName;

class OrgReportCustomerController extends Controller
{
    protected $orgAuthor;
    protected $abilityName = AbilityName::ORG_MANAGE;

    public function __construct()
    {
        $this->orgAuthor = new OrgAuthor();
    }

    public function index($orgId)
    {
        //Check quyền
        $this->orgAuthor->canManageId($orgId, $this->abilityName);

        return view('org.reports_customer.org_report_customer', ['OrgId' => $orgId]);
    }

    public function indexData($orgId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($orgId, $this->abilityName);

        $userClass = User::class;
        $userClass0 = str_replace("\\", "\\\\", $userClass);
        $orgClass = Organization::class;
        $orgClass0 = str_replace("\\", "\\\\", $orgClass);
        $model = Order::leftJoin('users', function ($join) use ($userClass) {
            $join->on('orders.orderable_id', '=', 'users.id')
            ->where('orders.orderable_type', '=', $userClass);
        })
        ->leftJoin('organizations', function ($join) use ($orgClass) {
            $join->on('orders.orderable_id', '=', 'organizations.id')
            ->where('orders.orderable_type', '=', $orgClass);
        })
        ->leftJoin('user_profiles', 'user_profiles.user_id', '=', 'users.id')
        //->whereNull('orders.deleted_at')
        ->whereIn('orders.status', [2,4])
        ->where('orders.provider_organization_id', '=', $orgId)
        ->selectRaw("orders.orderable_type,orders.orderable_id,
            if(orders.orderable_type='$orgClass0',organizations.name, users.name) as customer_name,
            if(orders.orderable_type='$orgClass0',organizations.address, user_profiles.address) as customer_address,
            if(orders.orderable_type='$orgClass0',organizations.tel, users.tel) as customer_tel,
            count(orders.id) as count_order, sum(orders.total_money) as total_money")
        ->groupBy('orders.orderable_type')
        ->groupBy('orders.orderable_id');

        $data = DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('from_date')) {
                $fromDate = parseDate($request->from_date);
                $query->where('orders.order_date', '>=', $fromDate);
            }

            if ($request->filled('to_date')) {
                $toDate = parseDate_end($request->to_date);
                $query->where('orders.order_date', '<=', $toDate);
            }
        })->make();

        $reportData = $data->original['data'];

        $start = request('start');
        foreach ($reportData as $i => $j) {
            $reportData[$i]['stt'] = $start + $i + 1;
        }
        $data = json_decode(json_encode($data), true);
        $data['original']['data'] = $reportData;
        return $data['original'];
    }

    public function exportExcel($orgId, Request $request)
    {
        //Check quyền
        $this->orgAuthor->canManageId($orgId, $this->abilityName);

        $org = Organization::find($orgId);
        $time = "";

        $userClass = User::class;
        $userClass0 = str_replace("\\", "\\\\", $userClass);
        $orgClass = Organization::class;
        $orgClass0 = str_replace("\\", "\\\\", $orgClass);
        $data = Order::leftJoin('users', function ($join) use ($userClass) {
            $join->on('orders.orderable_id', '=', 'users.id')
            ->where('orders.orderable_type', '=', $userClass);
        })
        ->leftJoin('organizations', function ($join) use ($orgClass) {
            $join->on('orders.orderable_id', '=', 'organizations.id')
            ->where('orders.orderable_type', '=', $orgClass);
        })
        ->leftJoin('user_profiles', 'user_profiles.user_id', '=', 'users.id')
        //->whereNull('orders.deleted_at')
        ->whereIn('orders.status', [2,4])
        ->where('orders.provider_organization_id', '=', $orgId);

        if ($request->filled('from_date')) {
            $fromDate = parseDate($request->from_date);
            $data = $data->where('orders.order_date', '>=', $fromDate);
            $time = "Từ ngày ".$request->from_date;
        }
        if ($request->filled('to_date')) {
            $toDate = parseDate_end($request->to_date);
            $data = $data->where('orders.order_date', '<=', $toDate);
            $time .= ($request->filled('from_date') ? " đ" : " Đ")."ến ngày ".$request->to_date;
        }
        $data = $data->selectRaw("orders.orderable_type,orders.orderable_id,
            if(orders.orderable_type='$orgClass0',organizations.name, users.name) as customer_name,
            if(orders.orderable_type='$orgClass0',organizations.address, user_profiles.address) as customer_address,
            if(orders.orderable_type='$orgClass0',organizations.tel, users.tel) as customer_tel,
            count(orders.id) as count_order, sum(orders.total_money) as total_money")
        ->groupBy('orders.orderable_type')
        ->groupBy('orders.orderable_id')
        ->orderBy("total_money", "desc")->get();
        
        $fileName = "tk_khach_hang_cskdsx.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/org/reports_customer/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);
        $sheet->setCellValue("A1", $org->name);
        $sheet->setCellValue("A4", $time);

        $i = 1;
        $row = 6;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['customer_name']);
            $sheet->setCellValue("C$row", $d['customer_address']);
            $sheet->setCellValue("D$row", $d['customer_tel']);
            $sheet->setCellValue("E$row", $d['count_order']);
            $sheet->setCellValue("F$row", $d['total_money']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A7:F$row")->applyFromArray($style);

        $fileType="Xlsx";
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename='.$fileName);
        header('Cache-Control: max-age=0');

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }
}
