<?php

namespace App\Http\Controllers;

use App\Business\AdvertisingBussiness;
use App\Business\NewsArticlesBussiness;
use App\Business\OrganizationBusiness;
use App\Business\ProductBussiness;
use Former\Form\Fields\Input; 
use Illuminate\Http\Request;
use App\Models\SlideShow;
use App\Models\Product;
use App\Models\NewsArticle;
use App\Models\Organization;
use App\Models\Advertising;
use App\Models\Video;
use App\Models\Document;
use App\Models\Weblink;
use App\OrgType;
use Illuminate\Support\Facades\Cache;
use Illuminate\View\View;
use App\Constants;
use App\Models\Branch;
use App\Models\CommercialCenter;
use App\Business\MapsBussiness;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return void
     */
    public function switchRoleAccess()
    {
        setRoleAccess(request()->role);

        return response()->json(['message' => 'success']);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function web()
    {
        //silde
        $slides = SlideShow::where('active', '=', 1)->orderBy("no", "asc")->get();

        // sản phẩm phổ biến
        $productPopu = ProductBussiness::getListForView()->orderBy('rank', 'desc')->take(15)->get();
        // Sản phẩm mới
        $productNew = ProductBussiness::getListForView()->orderBy('created_at', 'desc')->take(10)->get();

        //Nông sản
        $productNS = ProductBussiness::getListNS()->orderBy('created_at', 'desc')->take(10)->get();
        //Sản phẩm bất động sản
        $productBDS = ProductBussiness::getListBDS()->orderBy('created_at', 'desc')->take(10)->get();
        // Sản phẩn dịch vụ du lịch
        $productDVDL = ProductBussiness::getListDVDL()->orderBy('created_at', 'desc')->take(10)->get();
        //Sản phẩm dịch vụ việc làm
        $productDVVL = ProductBussiness::getListDVVL()->orderBy('created_at', 'desc')->take(10)->get();
        //Dự án đầu tư
        $productDADT = ProductBussiness::getListDADT()->orderBy('created_at', 'desc')->take(10)->get();
        // sản phẩm khyến mại - tạm bỏ do chưa dùng đến
        /*$productsSale = ProductBussiness::getListForView()->whereNotNull('price_sale')
                            ->whereRaw('price_sale < price')
                            ->orderBy('created_at', 'desc')->take(20)->get();*/
        // tin tức
        $newsArticle = NewsArticle::where('status', '=', 1)
            ->where('group_id', '<>', Constants::PREFIX_NEWS_GROUP_ID)
            ->orderBy("created_at", "desc")->take(5)->get();

        // cơ sở tiêu biểu
        $organization = OrganizationBusiness::getListForView()
            ->orderBy('rank', 'desc')
            ->take(6)->get();

        // kết nối cung cầu
        $ads = Advertising::orwhere('category_id', '=', 1)->orwhere('category_id', '=', 2)->orwhere('category_id', '=', 3)->where('status', '=', 1)
            ->where(function ($query) {
                $query->where('to_date', '>=', date('Y-m-d'))
                    ->orWhereNull('to_date');
            })->orderBy('created_at', 'desc')
            ->take(6)->get();

        $ads2 = Advertising::where(function ($query) {
                $query->orwhere('category_id', '=', 4)->orwhere('category_id', '=', 5)->orwhere('category_id', '=', 6);
            })->where('status', '=', 1)
            ->where(function ($query) {
                $query->where('to_date', '>=', date('Y-m-d'))
                    ->orWhereNull('to_date');
            })->orderBy('created_at', 'desc')
            ->take(6)->get();

        // lay quang cao canh slide, group_id=1 trong App\Constants::WEBLINK_GROUPS
        $advs = Weblink::where('active', '=', 1)->where('group_id', '=', 1)->take(2)->get();

        // lay thuong hieu dong hanh, weblink voi group_id = 2 trong App\Constants::WEBLINK_GROUPS
        $trademarks = Weblink::where('active', '=', 1)->where('group_id', '=', 2)->take(12)->get();

        //videos
        $video = Video::where('active', 1)->orderBy('created_at', 'desc')->first();

        // văn bản quy phạm pháp luật
        $documents = Document::where('status', '=', 1)->orderBy('sign_date', 'desc')->take(10)->get();

        return view('web/home', [
            'slides' => $slides,
            "productPopu" => $productPopu,
            //"productsSale" =>$productsSale, //Tạm bỏ do chưa dùng đến
            "newsArticle" => $newsArticle,
            'organization' => $organization,
            "ads" => $ads,
            "ads2" =>$ads2,
            "video" => $video,
            "documents" => $documents,
            "trademarks" => $trademarks,
            "advs" => $advs,
            "productNew" => $productNew,
            'productNS' => $productNS,
            'productBDS' => $productBDS,
            'productDVDL' => $productDVDL,
            'productDVVL' => $productDVVL,
            'productDADT' => $productDADT
        ]);
    }

    public function searchBusiness(
        $type,
        $text,
        $choose,
        $lat = "",
        $long = "",
        $numberOfRow = 12,
        $numberOfOtherRow = 3
    ) {
        $data = [];
        $numberOfRowInfo = $type == "dia-diem" ? $numberOfRow : $numberOfOtherRow;
        $numberOfRowProduct = $type == "san-pham" ? $numberOfRow : $numberOfOtherRow + ($type == "dia-diem" ? 1 : 0);
        $numberOfRowOrg = $type == "co-so" ? $numberOfRow : $numberOfOtherRow;
        $numberOfRowNews = $type == "tin-tuc" ? $numberOfRow : $numberOfOtherRow;

        $data["ketnoi"] = AdvertisingBussiness::fullTextSearch(0, $text, 1, $numberOfRowInfo);
        $sortType = $choose == "thap-cao" ? 1 : ($choose == "cao-thap" ? 2 : 0);
        $data["sanpham"] = ProductBussiness::fullTextSearch($text, 1, $numberOfRowProduct, $sortType);
        $data["coso"] = OrganizationBusiness::fullTextSearch($text, 1, $numberOfRowOrg);
        $data["tintuc"] = NewsArticlesBussiness::fullTextSearch($text, 1, $numberOfRowNews);
        $data["count"] = $data["ketnoi"]["total_row"] + $data["coso"]["total_row"]
            + $data["tintuc"]["total_row"] + $data["sanpham"]["total_row"];

        $rs = ['data' => $data, 'text' => $text, 'type' => $type, 'choose' => $choose, 'lat' => $lat, 'long' => $long];
        if ($type == "dia-diem") {
            // Bản đồ
            $arOrg = [];
            if (strlen($lat) > 0 && strlen($long) > 0) {
                $arOrg = ProductBussiness::fullTextSearchNearest($text, 10, $lat, $long);
            }
            $data["sanpham_ketnoi"] = $arOrg;
            $rs = array_merge($rs, ["organization100" => json_encode($arOrg), 'arOrg' => $arOrg]);
        }

        return $rs;
    }

    public function search(Request $request)
    {
        $lat = $request->lat ?? "";
        $long = $request->long ?? "";
        $type = $request->type ?? "";
        $text = $request->text ?? "";
        $choose = $request->choose ?? "";
        $data = self::searchBusiness($type, $text, $choose, $lat, $long);
        $view = str_replace("-", "", $type);
        return view("web.trangchu.search_$view", $data);
    }

    public function load(Request $request)
    {
        $data = array();
        $data["text"] = $request->text;
        $data["type"] = $request->type;
        $data["page"] = $request->page + 1;

        if ($request->type == "san-pham") {
            if ($request->choose == "lien-quan") {
                $data = ProductBussiness::fullTextSearch($request->text, $request->page + 1, 12, 0);
            } elseif ($request->choose == "thap-cao") {
                $data = ProductBussiness::fullTextSearch($request->text, $request->page + 1, 12, 1);
            } elseif ($request->choose == "cao-thap") {
                $data = ProductBussiness::fullTextSearch($request->text, $request->page + 1, 12, 2);
            }
            $output = view('web.trangchu.add_search_sanpham', ['data' => $data]);
            return $output;
        } elseif ($request->type == "co-so") {
            $data = OrganizationBusiness::fullTextSearch($request->text, $request->page + 1, 12);
            $output = view('web.trangchu.add_search_coso', ['data' => $data]);
            return $output;
        } elseif ($request->type == "tin-tuc") {
            $data = NewsArticlesBussiness::fullTextSearch($request->text, $request->page + 1, 12);
            $output = view('web.trangchu.add_search_tintuc', ['data' => $data]);
            return $output;
        } else {
            $data = AdvertisingBussiness::fullTextSearch(0, $request->text, $request->page + 1, 12);
            $output = view('web.trangchu.add_search_ketnoi', ['data' => $data]);
            return $output;
        }
    }
    public function searchAuto(Request $request)
    {
        //        return Product::where('name', 'LIKE', '%'.$request->q.'%')->get();
        $data = ProductBussiness::fullTextSearch($request->q, 1, 5, 0);
        return $data["data"];
    }
}
