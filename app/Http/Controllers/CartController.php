<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Product;

class CartController
{

    public $items = null;
    public $totalQty = 0;
    public $totalPrice = 0;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct($oldCart)
    {
        if ($oldCart) {
            $this->items = $oldCart->items;
            $this->totalQty = $oldCart->totalQty;
            $this->totalPrice = $oldCart->totalPrice;
        }
    }

//    public function __construct() {
//        //$this->middleware('auth');
//    }

    public function add($item, $id)
    {
        if ($item->price_sale == 0 || $item->price_sale == null) {
            $giohang = ['qty' => 0, 'price' => $item->price, 'item' => $item];
        } else {
            $giohang = ['qty' => 0, 'price' => $item->price_sale, 'item' => $item];
        }
       
        if (isset($this->items)) {
            if (array_key_exists($id, $this->items)) {
                $giohang = $this->items[$id];
            }
        }
        $giohang['qty'] ++;
        if ($item->price_sale == 0 || $item->price_sale == null) {
            $giohang['price'] = $item->price * $giohang['qty'];
        } else {
            $giohang['price'] = $item->price_sale * $giohang['qty'];
        }
        $this->items[$id] = $giohang;
       

        $this->totalQty++;
        if ($item->price_sale == 0 || $item->price_sale == null) {
            $this->totalPrice += $item->price;
        } else {
            $this->totalPrice += $item->price_sale;
        }
    }
}
