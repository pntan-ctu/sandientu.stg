<?php

namespace App\Http\Controllers\Manage;

use App\Models\QuestionAnswer;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Product;
use App\Models\Organization;
use App\OrgType;
use App\Authors\ManageOrganizationAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class QuestionAnswerController extends Controller
{
    protected $orgAuthor;

    public function __construct()
    {
        $this->orgAuthor = new ManageOrganizationAuthor();
    }

    public function checkPermission($questionAnswer)
    {
        $org = null;
        if ($questionAnswer->question_answerable_type == Organization::class) {
            $org = $questionAnswer->questionAnswerable;
        }

        if ($questionAnswer->question_answerable_type == Product::class) {
            $org = $questionAnswer->questionAnswerable->organization;
        }

        $this->orgAuthor->canManage($org);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->orgAuthor->canDo();

        return view('manage.Q_A.index');
    }

    public function getPreData(Request $request)
    {
        //Check quyền
        $this->orgAuthor->canDo();

        $orgId = getOrgId();
        if ($request->type_search == 'products') {
            $model = QuestionAnswer::join('products', 'question_answers.question_answerable_id', '=', 'products.id')
                ->join('organizations', 'products.organization_id', '=', 'organizations.id')
                ->where('question_answers.question_answerable_type', '=', Product::class)
                ->where('organizations.status', '=', 1)
                ->select(
                    'question_answers.content as content',
                    'question_answers.id as id',
                    'question_answers.created_by as created_by',
                    'question_answers.status as status',
                    'users.name as user_name',
                    'organizations.name as org_name',
                    'products.path as product_path',
                    'products.name as product_name'
                )
                ->join('users', 'question_answers.created_by', '=', 'users.id');
        } else {
            $model = QuestionAnswer::join(
                'organizations',
                'question_answers.question_answerable_id',
                '=',
                'organizations.id'
            )
                ->where('question_answers.question_answerable_type', '=', Organization::class)
                ->select(
                    'question_answers.content as content',
                    'question_answers.id as id',
                    'question_answers.created_by as created_by',
                    'question_answers.status as status',
                    'users.name as user_name',
                    'organizations.name as org_name',
                    'organizations.path as org_path'
                )
                ->join('users', 'question_answers.created_by', '=', 'users.id');
        }

        if (!Gate::allows('*', Organization::class)) {
            $model = $model->where(function ($query) use ($orgId) {
                $query->where('organizations.manage_organization_id', '=', -1);

                if (Gate::any(
                    ['*',
                        AbilityName::MANAGE_ORG_GOV,
                        AbilityName::MANAGE_ORG_GOV_HEALTH,
                        AbilityName::MANAGE_ORG_GOV_AGRICULTURE,
                        AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE],
                    Organization::class
                )) {
                    $query->orWhere(function ($query) use ($orgId) {
                        $query->where('organizations.manage_organization_id', '=', $orgId);

                        if (!Gate::allows(AbilityName::MANAGE_ORG_GOV, Organization::class)) {
                            $query->where(function ($query) {
                                if (Gate::allows(AbilityName::MANAGE_ORG_GOV_HEALTH, Organization::class)) {
                                    $query->orWhere('organizations.department_manage_id', '=', 1);
                                }
                                if (Gate::allows(AbilityName::MANAGE_ORG_GOV_AGRICULTURE, Organization::class)) {
                                    $query->orWhere('organizations.department_manage_id', '=', 2);
                                }
                                if (Gate::allows(AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE, Organization::class)) {
                                    $query->orWhere('organizations.department_manage_id', '=', 3);
                                }
                            });
                        }
                    });
                }
            });
        }

        $keyword = $request->keyword ?? "";
        if (strlen($keyword) > 0) {
            $model = $model->where(function ($query) use ($keyword) {
                $query->where('question_answers.content', 'like', '%' . $keyword . '%');
            });
        }

        $active = $request->active_search ?? -1;
        if ($active != -1) {
            $model = $model->where('question_answers.status', '=', $active);
        }

        return $model;
    }

    public function indexData(Request $request)
    {
        $data = self::getPreData($request);
        return DataTables::eloquent($data)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\QuestionAnswer  $questionAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(QuestionAnswer $questionAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\QuestionAnswer  $questionAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit(QuestionAnswer $questionAnswer)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\QuestionAnswer  $questionAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, QuestionAnswer $questionAnswer)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\QuestionAnswer  $questionAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy(QuestionAnswer $questionAnswer)
    {
        //Check quyền
        $this->checkPermission($questionAnswer);

        $questionAnswer->delete();
    }

    public function updateChecked(Request $request)
    {
        if (isset($request->id)) {
            $Check = $request->id;
            $Checks = explode(",", $Check);
            foreach ($Checks as $ck) {
                {
                    $questionAnswer = QuestionAnswer::find($ck);
                    $questionAnswer->status = 1;
                }
                $this->checkPermission($questionAnswer);
                $questionAnswer->updated_by = getUserId();
                $questionAnswer->save();
            }
        }
        if (isset($request->id_nosave)) {
            $noCheck = $request->id_nosave;
            $noChecks = explode(",", $noCheck);
            foreach ($noChecks as $nck) {
                {
                    $questionAnswer = QuestionAnswer::find($nck);
                    $questionAnswer->updated_by = getUserId();
                    $questionAnswer->status = 2;
                }
                $this->checkPermission($questionAnswer);
                $questionAnswer->save();
            }
        }
        return response()->json(['status' => 'success', 'msg' => 'Cập nhật thành công']);
    }
}
