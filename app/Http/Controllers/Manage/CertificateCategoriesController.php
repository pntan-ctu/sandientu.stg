<?php

namespace App\Http\Controllers\manage;

use App\Business\CertBusiness;
use App\Http\Requests\StoreCertCateRequest;
use App\Http\Requests\UpdateCertCateRequest;
use App\Models\Region;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\CertificateCategory;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Manage;
use Image;
use App\Utils\UploadFileUtil;
use App\Utils\ResizeImageUtil;
use App\Authors\CertificateCategoryAuthor;

class CertificateCategoriesController extends Controller
{
    protected $certificateCategoryAuthor;

    public function __construct()
    {
        $this->certificateCategoryAuthor = new CertificateCategoryAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->certificateCategoryAuthor->canDo();

        return view('manage.certificate_categories.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->certificateCategoryAuthor->canDo();

        return CertBusiness::getAllCate($request);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->certificateCategoryAuthor->canDo();

        $tree = CertificateCategory::all();
        return view('manage.certificate_categories.create', ['tree' => $tree, 'cert_cate' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCertCateRequest $request)
    {
        //Check quyền
        $this->certificateCategoryAuthor->canDo();

        $cert = new CertificateCategory();
        if (isset($request->icon)) {
            $file = UploadFileUtil::UploadImage($request, 'icon');
            $cert->icon = $file->path;
        }
        $cert["name"] = $request->name;
        $cert["rank"] = $request->rank;
        $cert["type"] = $request->type;
        $cert["description"] = $request->description;
        $cert->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($iCertificateCategory)
    {
        //Check quyền
        $this->certificateCategoryAuthor->canDo();

        $tree = CertificateCategory::all();
        $certificateCategory = CertificateCategory::find($iCertificateCategory);
        return view('manage.certificate_categories.create', ['tree' => $tree, 'cert_cate' => $certificateCategory]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCertCateRequest $request, $iCertificateCategory)
    {
        //Check quyền
        $this->certificateCategoryAuthor->canDo();

        $certificateCategory = CertificateCategory::find($iCertificateCategory);
        $certificateCategory->fill($request->all());
        $file = UploadFileUtil::UploadImage($request, 'icon');
        if (isset($file)) {
            $certificateCategory->icon = $file->path;
        }
        $certificateCategory->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($iCertificateCategory)
    {
        //Check quyền
        $this->certificateCategoryAuthor->canDo();
        
        $certificateCategory = CertificateCategory::find($iCertificateCategory);
        $certificateCategory->delete();
    }
}
