<?php

namespace App\Http\Controllers\Manage;

use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use App\Models\Organization;
use Illuminate\Support\Facades\Validator;
use App\Utils\CommonUtil;
use App\Models\Region;
use App\Http\Requests\StoreInfomationRequest;
use App\Authors\InformationGovAuthor;

class InformationController extends Controller
{
    protected $informationGovAuthor;

    public function __construct()
    {
        $this->informationGovAuthor = new InformationGovAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getInfo()
    {
        //Check quyền
        $this->informationGovAuthor->canDo();

        $information = getOrgModel()->toArray();
        $i = 0;
        $id = $information["region_id"];
                $oGroup = Region::find($id);
                $sUrl = $oGroup["name"];
                $iParentId = $oGroup["parent_id"];
        while ($iParentId > 0) {
            $oGroup = Region::find($iParentId);
            $sUrl =  $sUrl ." - " . $oGroup["name"];
            $iParentId = $oGroup["parent_id"];
        }
        $information["map"] = $information["map_lat"] . ", " . $information["map_long"];
        return view('manage.information.index', ['information' => $information,'sUrl'=>$sUrl]);
    }
    public function postInfo(StoreInfomationRequest $request)
    {
        //Check quyền
        $this->informationGovAuthor->canDo();

        $information = Organization::find($request->id);
        $information->address   = $request->address;
        $information->tel       = $request->tel;
        $information->email     = $request->email;
        $validator = CommonUtil::validateMap($request, $information);
        if ($validator->fails()) {
            return redirect('manage/information')
                ->withErrors($validator)
                ->withInput();
        }
        $information->save();
        return redirect('manage/information');
    }
}
