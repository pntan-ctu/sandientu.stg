<?php

namespace App\Http\Controllers\Manage;

use App\Models\SlideShow;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use App\Utils\ResizeImageUtil;
use App\Http\Requests\StoreSlideShowRequest;
use App\Http\Requests\UpdateSlideShowRequest;
use App\Models\FileMetadata;
use Illuminate\Support\Facades\Auth;
use Image;
use App\Authors\SlideShowAuthor;

class SlideShowController extends Controller
{
    protected $slideShowAuthor;

    public function __construct()
    {
        $this->slideShowAuthor = new SlideShowAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->slideShowAuthor->canDo();

        return view('manage.slideshow.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->slideShowAuthor->canDo();

        $model = SlideShow::select(['*']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            $query->select(
                'slide_shows.title as title',
                'slide_shows.link as link',
                'slide_shows.image_path as image_path',
                'slide_shows.no as no',
                'slide_shows.active as active',
                'slide_shows.id as id',
                'users.name as user_name'
            )
                    ->join('users', 'slide_shows.created_by', '=', 'users.id');
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('slide_shows.image_path', 'like', '%' . $keyword . '%')
                                    ->orWhere('slide_shows.title', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('slide_shows.active', '=', request('active'));
            }
        })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->slideShowAuthor->canDo();

        return view('manage.slideshow.create', ['slideshow' => null,'none_create'=>1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    
    public function store(StoreSlideShowRequest $request)
    {
        //Check quyền
        $this->slideShowAuthor->canDo();

        $slideshow = new SlideShow();
        if (isset($request->image_path)) {
            $file = UploadFileUtil::UploadImage($request, 'image_path');
            $slideshow->image_path = $file->path;
        }
        $slideshow->title = $request->title;
        $slideshow->link = $request->link;
        $slideshow->created_by = Auth::user()->id;
        if ((int)$request->no == 0) {
            $iMaxNo = SlideShow::max('no');
            $slideshow->no = $iMaxNo + 1;
        } else {
            $slideshow->no = $request->no;
        }
        $slideshow->active = $request->active == true ? 1 : 0;
        $slideshow->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\SlideShow  $slideShow
     * @return \Illuminate\Http\Response
     */

    public function show($id)
    {
        //Check quyền
        $this->slideShowAuthor->canDo();

        $slideshow = SlideShow::find($id);
        $slideshow -> active = !($slideshow -> active);
        $slideshow->save();
        return redirect('manage/slideshow');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\SlideShow  $slideShow
     * @return \Illuminate\Http\Response
     */
    public function edit(SlideShow $slideshow)
    {
        //Check quyền
        $this->slideShowAuthor->canDo();

        return view('manage.slideshow.create', ['none_create'=>2,'slideshow' => $slideshow]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\SlideShow  $slideShow
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSlideShowRequest $request, SlideShow $slideshow)
    {
        //Check quyền
        $this->slideShowAuthor->canDo();

        $file = UploadFileUtil::UploadImage($request, 'image_path');
        if (isset($file)) {
            $slideshow->image_path = $file->path;
        }
        $slideshow->created_by = $slideshow->created_by;
        $slideshow->title = $request->title;
        $slideshow->link = $request->link;
        $slideshow->no = $request->no;
        $slideshow->active = $request->active == true ? 1 : 0;
        
        $slideshow->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\SlideShow  $slideShow
     * @return \Illuminate\Http\Response
     */
    public function destroy(SlideShow $slideshow)
    {
        //Check quyền
        $this->slideShowAuthor->canDo();

        $slideshow->delete();
    }
}
