<?php

namespace App\Http\Controllers\Manage;

use App\Models\Contact;
use App\Models\ContactFile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use App\OrgType;
use Illuminate\Support\Facades\Validator;
use App\Business\OrganizationBusiness;
use App\Utils\CommonUtil;
use App\Models\Organization;
use App\Http\Requests\StoreContactManageRequest;
use App\Authors\ContactAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class ContactController extends Controller
{
    protected $contactAuthor;

    public function __construct()
    {
        $this->contactAuthor = new ContactAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->contactAuthor->canDo();

        return view('manage.contact.index');
    }
    public function indexData(Request $request)
    {
        //Check quyền
        $this->contactAuthor->canDo();

        $model = Contact::join('organizations', 'contacts.organization_id', '=', 'organizations.id')
            ->select(
                'contacts.organization_id as organization_id',
                'contacts.title as title',
                'contacts.content as content',
                'contacts.name as name',
                'contacts.email as email',
                'contacts.phone as phone',
                'contacts.status as status',
                'contacts.forward_log as forward_log',
                'contacts.created_at as created_at',
                'contacts.id as id',
                'organizations.name as org_name'
            );

        if (!Gate::allows('*', Contact::class)) {
            $model->where(function ($query) {
                $query->where('organization_id', '=', -1);

                if (Gate::allows(AbilityName::MANAGE_VERIFY_CONTACT, Contact::class)) {
                    $query->orWhere('organization_id', '=', getOrgId());
                }
            });
        }

        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('contacts.title', 'like', '%' . $keyword . '%')
                        ->orWhere('contacts.name', 'like', '%' . $keyword . '%')
                        ->orWhere('organizations.name', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('contacts.status', '=', request('active'));
            }
        })->make();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->contactAuthor->canDo();

        return view('manage.contact.create', ['contact' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function edit(Contact $contact)
    {
        //Check quyền
        $this->contactAuthor->canManage($contact);

        $orgTypeTree = OrganizationBusiness::getTreeOrgGov();
        $contactFiles = $contact->contactFiles;
        $files = array();
        foreach ($contactFiles as $item) {
            $files[] = UploadFileUtil::getFileMetadata($item->filename);
        }
        return view('manage.contact.create', ['contact' => $contact,'files' => $files,'orgTypeTree'=>$orgTypeTree]);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function update(StoreContactManageRequest $request, Contact $contact)
    {
        //Check quyền
        $this->contactAuthor->canManage($contact);

        //$org_name_new = Organization::find($request->org_id);
        $contact->forward_log .= ($contact->forward_log ? ' -> ' : '') . $contact->organization->name;
        $contact->organization_id = $request->org_id;
        //$contact->forward_log = $contact->forward_log . ' -> '. $org_name_new->name;
        $contact->status = 0;
        $contact->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Contact  $contact
     * @return \Illuminate\Http\Response
     */
    public function destroy(Contact $contact)
    {
        //Check quyền
        $this->contactAuthor->canManage($contact);

        $contact->delete();
    }
    public function convert($id)
    {
        $contact = Contact::find($id);
        
        //Check quyền
        $this->contactAuthor->canManage($contact);

        $contact->status = !($contact -> status);
        $contact->save();
        return redirect('manage/contact');
    }
    public function showcontact($id)
    {
        $contact = Contact::find($id);

        //Check quyền
        $this->contactAuthor->canManage($contact);

        $contactFiles = $contact->contactFiles;
        $files = array();
        foreach ($contactFiles as $item) {
            $files[] = UploadFileUtil::getFileMetadata($item->filename);
        }
        return view('manage.contact.show', ['contact' => $contact,'files' => $files]);
    }
}
