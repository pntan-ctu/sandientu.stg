<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Organization;

class MapController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Lấy dữ liệu danh mục cơ sở sản xuất và cơ sở kinh doanh
        //$model = Organization::select(['id', 'name', 'address', 'website', 'fanpage','rank','tel','email']);
        $organization100 = Organization::where('organization_type_id', '=', '100')->get(); // san xuat
        $organization101 = Organization::where('organization_type_id', '=', '101')->get(); // kinh doanh
        // xử lý dữ liệu map

        //------
        return view(
            'manage.map.index',
            ['organization100' => $organization100, 'organization101' => $organization101]
        );
    }

    // TODO Kiểm tra lại xem có dùng đoạn này không
    /*public function indexData(Request $request)
    {
        $model = User::select(['id', 'name', 'email', 'active', 'created_at']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%')
                        ->orWhere('email', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('active', '=', request('active'));
            }
        })->make();
    }
    */

    // TODO Kiểm tra lại xem có dùng đoạn này không, nếu không xóa hàm, xóa cả view
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    /*public function create()
    {
        return view('manage.map.create', ['user' => null]);
    }
    */

    // TODO check action này xem có dùng không
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    //    public function store(StoreUserRequest $request)
    //    {
    //        // User::create($request->all()); // có thể dùng kiểu tắt này nếu không cần xử lý gì thêm
    //        $user = new User();
    //        $user->fill($request->all());
    //        $user->password = bcrypt($request->input('password'));
    //        $user->save();
    //        return redirect(route('map.index'));
    //    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    //    public function show(User $user)
    //    {
    //        // Với chức năng phức tạp cần xây dựng action này
    //        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    //    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    //    public function edit(User $user)
    //    {
    //        return view('manage.map.create', ['user' => $user]);
    //    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateUserRequest $request
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    //    public function update(UpdateUserRequest $request, User $user)
    //    {
    //        $user->fill($request->all());
    //        if ($request->filled('password')) {
    //            $user->password = bcrypt($request->input('password'));
    //        }
    //        $user->save();
    //        return redirect(route('map.index'));
    //    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    //    public function destroy(User $user)
    //    {
    //        $user->delete();
    //    }
}
