<?php

namespace App\Http\Controllers\Manage;

use App\Authors\DocumentTypeAuthor;
use App\Models\DocumentType;
use Illuminate\Http\Request;
use App\Http\Requests\StoreDocumentTypeRequest;
use App\Http\Requests\UpdateDocumentTypeRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;

class DocumentTypesController extends Controller
{
    protected $documentTypeAuthor;

    public function __construct()
    {
        $this->documentTypeAuthor = new DocumentTypeAuthor();
    }

    public function index()
    {
        //Check quyền
        $this->documentTypeAuthor->canDo();

        return view('manage.doctype.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->documentTypeAuthor->canDo();
        
        $model = DocumentType::select(['id', 'name']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                });
            }
        })->make();
    }

    public function store(StoreDocumentTypeRequest $request)
    {
        //Check quyền
        $this->documentTypeAuthor->canDo();

        $docType = new DocumentType();
        $docType->fill($request->all());
        $docType->save();
    }

    public function create()
    {
        //Check quyền
        $this->documentTypeAuthor->canDo();

        return view('manage.doctype.create', ['docType' => null]);
    }

    public function edit($id)
    {
        //Check quyền
        $this->documentTypeAuthor->canDo();

        $docType = DocumentType::find($id);
        return view('manage.doctype.edit', ['docType' => $docType]);
    }

    public function update(UpdateDocumentTypeRequest $request, $id)
    {
        //Check quyền
        $this->documentTypeAuthor->canDo();

        $docType = DocumentType::find($id);

        if (!$docType) {
            return response()
                            ->json(['error' => 'Loại văn bản không tồn tại']);
        }

        $docType->update([
            'name' => $request->name,
        ]);

        $docType->save();
    }

    public function destroy($id)
    {
        //Check quyền
        $this->documentTypeAuthor->canDo();

        $docType = DocumentType::find($id);

        if (!$docType) {
            return response()->json(['error' => 'Loại văn bản không tồn tại!']);
        }

        $docType->delete();
        
        return response()->json(['message' => 'Xóa thành công!']);
    }
}
