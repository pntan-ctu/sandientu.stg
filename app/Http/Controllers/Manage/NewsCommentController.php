<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Models\NewsComment;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UpdateNewsCommentRequest;
use App\Authors\NewsArticleAuthor;

class NewsCommentController extends Controller
{
    protected $newsArticleAuthor;

    public function __construct()
    {
        $this->newsArticleAuthor = new NewsArticleAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        return view('manage.news_comments.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $model = NewsComment::join('news_articles', 'news_articles.id', '=', 'news_comments.article_id')
            ->leftjoin('users', 'users.id', '=', 'news_comments.publish_by')
            ->select(['news_comments.id', 'news_articles.title', 'news_comments.comment',
                DB::raw('case when length(news_comments.comment) <= 100 then news_comments.comment
                            else concat(SUBSTR(news_comments.comment,1, 100), "...") end as sub_comment'),
                DB::raw("date_format(news_comments.created_at, '%d/%m/%Y %H:%i') as ngay_dang"),
                'news_comments.name as nguoi_binh_luan', 'news_comments.email', 'news_comments.active',
                DB::raw('case news_comments.active when 1 then users.name else "" end as nguoi_duyet')
            ]);

        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('news_comments.comment', 'like', '%' . $keyword . '%')
                        ->orWhere('news_comments.name', 'like', '%' . $keyword . '%')
                        ->orWhere('news_comments.email', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('news_comments.active', '=', request('active'));
            }
        })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\NewsComment $newsComment
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // Với chức năng phức tạp cần xây dựng action này
        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\NewsComment $newsComment
     * @return \Illuminate\Http\Response
     */
    public function edit(NewsComment $newsComment)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        return view('manage.news_comments.create', ['newsComment' => $newsComment]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateNewsCommentRequest $request
     * @param  \App\Models\NewsComment $newsComment
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNewsCommentRequest $request, NewsComment $newsComment)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $newsComment->fill($request->all());
        $newsComment->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\NewsComment $newsComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(NewsComment $newsComment)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $newsComment->delete();
    }

    public function setActive($commentId)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $comment = NewsComment::find($commentId);
        $comment->active = 1;
        $comment->publish_by = auth()->user()->id;
        $comment->publish_at = date('Y-m-d H:i:s');
        $comment->save();
        echo "success";
    }

    public function setInactive($commentId)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();
        
        $comment = NewsComment::find($commentId);
        $comment->active = 0;
        $comment->save();
        echo "success";
    }
}
