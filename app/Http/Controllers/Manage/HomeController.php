<?php

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\SlideShow;
use App\Models\NewsArticle;
use App\Models\Organization;
use App\Models\Product;
use App\Models\User;
use App\Models\Branch;
use App\Models\CommercialCenter;
use App\Models\Advertising;
use App\Models\QuestionAnswer;
use App\Models\Rating;
use App\Models\Infringement;
use App\Models\Order;
use App\Models\Contact;
use App\OrgType;
use Illuminate\Support\Facades\Cache;
use App\Business\MapsBussiness;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function manage()
    {
        $arOrg = array();
        $arOrg = MapsBussiness::getMapsLocation("-1", "");
        $curOrganizationId = getOrgId();
        $curOrganizationTypeId = getOrgTypeId();

        //Get number organization by organization_level_id
        //1 - Cá thể, hộ gia đình
        //2 - Trang trại, xưởng
        //3 - Hợp tác xã
        //4 - Doanh nghiệp siêu nhỏ
        //5 - Doanh nghiệp nhỏ
        //6 - Doanh nghiệp vừa
        //7 - Doanh nghiệp lớn
        $organization1 = Organization::where('organization_type_id', '>=', '100')
                ->where('organization_level_id', '=', '1')->get();
        $organization2 = Organization::where('organization_type_id', '>=', '100')
                ->where('organization_level_id', '=', '2')->get();
        $organization3 = Organization::where('organization_type_id', '>=', '100')
                ->where('organization_level_id', '=', '3')->get();
        $organization4 = Organization::where('organization_type_id', '>=', '100')
                ->where('organization_level_id', '=', '4')->get();
        $organization5 = Organization::where('organization_type_id', '>=', '100')
                ->where('organization_level_id', '=', '5')->get();
        $organization6 = Organization::where('organization_type_id', '>=', '100')
                ->where('organization_level_id', '=', '6')->get();
        $organization7 = Organization::where('organization_type_id', '>=', '100')
                ->where('organization_level_id', '=', '7')->get();
        //Get number status order
        //1: Chưa xử lý
        //2: Đã xử lý
        //3: Đã hủy
        //4: Đã xác nhận
        $order1 = Order::where('status', '=', '1')->get();
        $order2 = Order::where('status', '=', '2')->get();
        $order3 = Order::where('status', '=', '3')->get();
        $order4 = Order::where('status', '=', '4')->get();

        //Cơ sở SX và KD chưa duyệt
        $organization0 = Organization::where('status', '=', '0');
        if ($curOrganizationTypeId != OrgType::VPDP) {
            $organization0->where(function ($qr) use ($curOrganizationId) {
                $qr->where('manage_organization_id', '=', $curOrganizationId)
                    ->orWhere('move_organization_id', '=', $curOrganizationId);
            });
        }

        //Tin cung cầu chưa duyệt
        $advertisings = Advertising::where('status', '=', '0');
        if ($curOrganizationTypeId != OrgType::VPDP) {
            $advertisings->where('region_id', '=', getRegionId());
        }

        //Thông tin phản hồi chưa xử lý
        $contact = Contact::where('status', '=', '0');
        if ($curOrganizationTypeId != OrgType::VPDP) {
            $contact->where('organization_id', '=', $curOrganizationId);
        }

        //Đánh cơ sở, sản phẩm giá chưa duyệt
        $rating1 = Rating::join('organizations', 'ratings.ratingable_id', '=', 'organizations.id')
            ->where('ratings.ratingable_type', '=', Organization::class)
            ->where('ratings.status', '=', '0');
        $rating2 = Rating::join('products', 'ratings.ratingable_id', '=', 'products.id')
            ->join('organizations', 'products.organization_id', '=', 'organizations.id')
            ->where('ratings.ratingable_type', '=', Product::class)
            ->where('ratings.status', '=', '0');
        if ($curOrganizationTypeId != OrgType::VPDP) {
            $rating1->where('organizations.manage_organization_id', '=', $curOrganizationId);
            $rating2->where('organizations.manage_organization_id', '=', $curOrganizationId);
        }
        $rating = $rating1->count() + $rating1->count();

        //Hỏi đáp cơ sở, sản phẩm chưa duyệt
        $faq1 = QuestionAnswer::join(
            'organizations',
            'question_answers.question_answerable_id',
            '=',
            'organizations.id'
        )
            ->where('question_answers.question_answerable_type', '=', Organization::class)
            ->where('question_answers.status', '=', '0');
        $faq2 = QuestionAnswer::join('products', 'question_answers.question_answerable_id', '=', 'products.id')
            ->join('organizations', 'products.organization_id', '=', 'organizations.id')
            ->where('question_answers.question_answerable_type', '=', Product::class)
            ->where('question_answers.status', '=', '0');
        if ($curOrganizationTypeId != OrgType::VPDP) {
            $faq1->where('organizations.manage_organization_id', '=', $curOrganizationId);
            $faq2->where('organizations.manage_organization_id', '=', $curOrganizationId);
        }
        $faq = $faq1->count() + $faq2->count();
        
        //Phản ánh vi phạm cơ sở, sản phẩm, người dùng chưa xử lý
        $infringement1 = Infringement::join('organizations as c', 'c.id', '=', 'infringements.infringementable_id')
            ->where('infringements.infringementable_type', '=', Organization::class)
            ->where('infringements.status', '=', '0');
        $infringement2 = Infringement::join('products as c', 'c.id', '=', 'infringements.infringementable_id')
            ->join('organizations as e', 'e.id', '=', 'c.organization_id')
            ->where('infringements.infringementable_type', '=', Product::class)
            ->where('infringements.status', '=', '0');
        $infringement3 = Infringement::where('infringements.infringementable_type', '=', User::class)
            ->where('infringements.status', '=', '0');
        if ($curOrganizationTypeId != OrgType::VPDP) {
            $infringement1->where('c.manage_organization_id', '=', $curOrganizationId);
            $infringement2->where('e.manage_organization_id', '=', $curOrganizationId);
        }
        $infringement = $infringement1->count() + $infringement2->count() + $infringement3->count();
        
        return view('manage/home', ["organization100"=>json_encode($arOrg),
            "organization0"=>$organization0->count(), "advertisings"=> $advertisings->count(),
            "contact"=> $contact->count(), "rating"=> $rating,
            "questionanswer"=>$faq, "infringement"=>$infringement,
            "organization1"=> count($organization1), "organization2"=> count($organization2),
            "organization3"=> count($organization3), "organization4"=> count($organization4),
            "organization5"=> count($organization5), "organization6"=> count($organization6),
            "organization7"=> count($organization7), "order1"=> count($order1),
            "order2"=> count($order2), "order3"=> count($order3), "order4"=> count($order4)]);
    }
}
