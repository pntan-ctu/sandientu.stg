<?php

namespace App\Http\Controllers\Manage;

use App\Business\RegionBusiness;
use App\Http\Requests\StoreMenuRequest;
use App\Http\Requests\UpdateMenuRequest;
use App\Models\Menu;
use App\Models\Module;
use App\Models\OrganizationType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\AuthorizationException;

class MenuController extends Controller
{
    public function index()
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $organizationTypes = OrganizationType::where('id', '>', 0)->select(['id', 'name'])->get();
        $treeMenuData = MenuController::getTreeMenuData(1);
        return view(
            'manage.menu.index',
            ['treeMenuData' => $treeMenuData, 'organizationTypes' => $organizationTypes]
        );
    }

    public function indexData(Request $request)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $model = Menu::leftjoin('menus as a', 'a.id', '=', 'menus.parent_id')
        ->select(['menus.id', 'menus.parent_id', 'menus.name', 'a.name as parent_name']);
        $data = DataTables::eloquent($model)->make();
        return $data;
    }

    public function create(Request $request)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $orgTypeId = $request->org_type_id;
        $maxNo = Menu::where('organization_type_id', $orgTypeId)
            ->whereNull('parent_id')->max('no');
        $nextNo = $maxNo + 1;
        $menu_arr = Menu::where('organization_type_id', '=', $orgTypeId)->get();
        $cboMenuData = RegionBusiness::getTree($menu_arr, null);
        $modules = Module::select(['id', DB::raw('concat(group_name, " / ", name) as name')])
            ->where('organization_type_id', '=', $orgTypeId)->orderBy('group_name')->get();
        $cboModuleData = RegionBusiness::getTree($modules, null);

        $menuId = $request->menu_id;
        if ($menuId > 0) {
            $menu = Menu::find($menuId);
        } else {
            $menu = null;
        }

        return view(
            'manage.menu.create',
            ['orgTypeId' => $orgTypeId, 'cboMenuData' => $cboMenuData,
                'cboModuleData'=>$cboModuleData, 'nextNo' => $nextNo, 'menu' => $menu]
        );
    }

    public function store(StoreMenuRequest $request)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $menuId = (int)$request->id;
        $menu = $menuId == 0 ? new Menu() : Menu::find($menuId);
        $menu->fill($request->all());
        $parentId = (int)$request->parent_id;
        if ($parentId == 0) {
            $menu->parent_id = null;
            $menu->icon = "fa-folder-open-o";
        }

        $moduleId = (int)$request->module_id;
        if ($moduleId == 0) {
            $menu->module_id = null;
        }

        $menu->name = removeSpecialCharacters($request->name, true);
        if ((int)$request->no == 0) {
            if ($parentId > 0) {
                $maxNo = Menu::where('parent_id', $parentId)->max('no');
            } else {
                $maxNo = Menu::whereNull('parent_id')->max('no');
            }
            $menu->no = $maxNo + 1;
        }
        $menu->save();
    }

    public function show(Menu $Menu)
    {
        // Với chức năng phức tạp cần xây dựng action này
        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    }

    public function edit(Menu $Menu)
    {
    }

    public function update(Request $request, Menu $Menu)
    {
    }

    public function destroy($menuId)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $Menu = Menu::find($menuId);
        $Menu->delete();
    }

    /************ get data for tree menu *************/
    public static function getTreeMenuData($org_type_id, $parent_id = 0, $url = "none")
    {
        $fullUrl = $url != "none" ? "concat('" . $url . " / ', menus.name) as url" : "menus.name as url";
        $where = $parent_id == 0 ? "menus.parent_id is null" : "menus.parent_id = $parent_id";
        $returnVal = DB::table("menus")
            ->select(
                'menus.id',
                'menus.name as text',
                'menus.parent_id',
                'menus.module_id',
                'menus.no',
                DB::raw($fullUrl),
                DB::raw('(select ifnull(MAX(c.no),0) + 1 from menus c where c.parent_id=menus.id) as next_no')
            )
            ->whereRaw($where)
            ->where('menus.organization_type_id', '=', $org_type_id)
            ->orderBy("no")->orderBy("name")->get();
        foreach ($returnVal as $key => $value) {
            $children = MenuController::getTreeMenuData($org_type_id, $value->id, $value->url);
            if (count($children) > 0) {
                $returnVal[$key]->children = $children;
            }
        }
        return $returnVal;
    }

    public function getNextNo($parentId)
    {
        if ($parentId > 0) {
            $maxNo = Menu::where('parent_id', $parentId)->max('no');
        } else {
            $maxNo = Menu::whereNull('parent_id')->max('no');
        }
        return $maxNo + 1;
    }
}
