<?php

namespace App\Http\Controllers\Manage;

use App\Models\VideoGroup;
use App\Models\Video;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StoreVideoGroupRequest;
use App\Http\Requests\UpdateVideoGroupRequest;
use App\Authors\VideoAuthor;

class VideoGroupController extends Controller
{
    protected $videoAuthor;

    public function __construct()
    {
        $this->videoAuthor = new VideoAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->videoAuthor->canDo();

        return view('manage.video_group.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $model = VideoGroup::select(['*']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%')
                                    ->orWhere('path', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('active', '=', request('active'));
            }
        })->make();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->videoAuthor->canDo();

        return view('manage.video_group.create', ['video_group' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideoGroupRequest $request)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $video_group = new VideoGroup();
        $video_group->name = $request->name;
        $video_group->description = $request->description;
        $video_group->created_by = Auth::user()->id;
        if ((int)$request->no == 0) {
            $iMaxNo = VideoGroup::max('no');
            $video_group->no = $iMaxNo + 1;
        } else {
            $video_group->no = $request->no;
        }
        $video_group->active = $request->active == true ? 1 : 0;
        $video_group->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\VideoGroup  $videoGroup
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
//        $video_group = VideoGroup::find($id);
//        $video_group -> active = !($video_group -> active);
//        $video_group ->save();
//        return redirect('manage/video-group');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\VideoGroup  $videoGroup
     * @return \Illuminate\Http\Response
     */
    public function edit(VideoGroup $video_group)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        return view('manage.video_group.create', ['video_group' => $video_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\VideoGroup  $videoGroup
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVideoGroupRequest $request, VideoGroup $video_group)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $video_group->name = $request->name;
        $video_group->description = $request->description;
        $video_group->no = $request->no;
        $video_group->active = $request->active == true ? 1 : 0;
        
        $video_group->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\VideoGroup  $videoGroup
     * @return \Illuminate\Http\Response
     */
    public function destroy(VideoGroup $video_group)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $result = Video::where('group_id', $video_group->id)->first();
        if (!$result) {
            $video_group->delete();
        } else {
            return response()->json(['status' => 'warning', 'msg' => 'Chuyên mục có chứa video, không được xóa']);
        }
        
        $video_group->delete();
    }
}
