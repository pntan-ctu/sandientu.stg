<?php

namespace App\Http\Controllers\Manage;

use App\Models\DocumentOrgan;
use Illuminate\Http\Request;
use App\Http\Requests\StoreDocumentOrganRequest;
use App\Http\Requests\UpdateDocumentOrganRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Authors\DocumentOrganAuthor;

class DocumentOrgansController extends Controller
{
    protected $documentOrganAuthor;

    public function __construct()
    {
        $this->documentOrganAuthor = new DocumentOrganAuthor();
    }

    public function index()
    {
        //Check quyền
        $this->documentOrganAuthor->canDo();

        return view('manage.docorgan.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->documentOrganAuthor->canDo();
        
        $model = DocumentOrgan::select(['id', 'name', 'no', 'active']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                });
            }
        })->make();
    }

    public function store(StoreDocumentOrganRequest $request)
    {
        //Check quyền
        $this->documentOrganAuthor->canDo();

        $docOrgan = new DocumentOrgan();
        $docOrgan->fill($request->all());
        $docOrgan["active"] = $request->active == true ? 1 : 0;
        $docOrgan->save();
    }

    public function create()
    {
        //Check quyền
        $this->documentOrganAuthor->canDo();

        return view('manage.docorgan.create', ['docOrgan' => null]);
    }

    public function edit($id)
    {
        //Check quyền
        $this->documentOrganAuthor->canDo();

        $docOrgan = DocumentOrgan::find($id);
        return view('manage.docorgan.edit', ['docOrgan' => $docOrgan]);
    }

    public function update(UpdateDocumentOrganRequest $request, $id)
    {
        //Check quyền
        $this->documentOrganAuthor->canDo();

        $docOrgan = DocumentOrgan::find($id);

        if (!$docOrgan) {
            return response()->json(['error' => 'Error: Cơ quan ban hành không tồn tại']);
        }
        if ($request["active"] == null) {
            $request["active"] = 0;
        }
        $docOrgan->update([
            'name' => $request->name,
            'no' => $request->no,
            'active' => $request->active == true ? 1 : 0,
        ]);
        $docOrgan->save();
    }

    public function destroy($id)
    {
        //Check quyền
        $this->documentOrganAuthor->canDo();

        $docOrgan = DocumentOrgan::find($id);

        if (!$docOrgan) {
            return response()
                            ->json(['error' => 'Cơ quan ban hành không tồn tại!']);
        }
        $docOrgan->delete();
        
        return response()->json(['message' => 'Xóa thành công!']);
    }
}
