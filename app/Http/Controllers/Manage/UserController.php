<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Yajra\DataTables\Facades\DataTables;
use App\Models\User;
use App\Models\Organization;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Authors\ManageUserAuthor;
use Silber\Bouncer\BouncerFacade as Bouncer;
use App\Business\OrganizationBusiness;
use App\OrgType;

class UserController extends Controller
{
    protected $manageUserAuthor;

    public function __construct(ManageUserAuthor $manageUserAuthor)
    {
        $this->manageUserAuthor = $manageUserAuthor;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->manageUserAuthor->canDo();

        $memberTypes = ['-1' => 'Tất cả'];
        if (Gate::allows(AbilityName::MANAGE_USER_INTERNAL_GOV, User::class)) {
            $memberTypes['1'] = 'Cán bộ nội bộ của CQQL';
        }
        if (Gate::allows(AbilityName::MANAGE_USER_CHILD_GOV, User::class)) {
            $memberTypes['2'] = 'Cán bộ của các CQQL cấp dưới';
        }
        if (Gate::allows(AbilityName::MANAGE_USER_MEMBER, User::class)) {
            $memberTypes['3'] = 'Thành viên đăng ký';
        }
        return view('manage.users.index', compact('memberTypes'));
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->manageUserAuthor->canDo();

        $orgId = getOrgId();
        $userId = getUserId();
        $model = User::select(['users.id',
            'users.name',
            'users.email',
            'users.active',
            'users.created_at',
            'users.password_reset',
            'organizations.name as org_name',
            DB::raw("if(users.id=$userId, 1, 0) as is_me")])
        ->join('organizations', 'users.organization_id', '=', 'organizations.id')
        ->where('organizations.organization_type_id', '<', 100);

        if (getOrgTypeId() != OrgType::ADMIN) {
            $model->where('organizations.organization_type_id', '<>', OrgType::ADMIN);
        }

        if (!Gate::allows('*', User::class)) {
            $model->where(function ($query) use ($orgId) {
                $query->where('users.organization_id', '=', -1);

                if (Gate::allows(AbilityName::MANAGE_USER_INTERNAL_GOV, User::class)) {
                    $query->orWhere('users.organization_id', '=', $orgId);
                }

                if (Gate::allows(AbilityName::MANAGE_USER_CHILD_GOV, User::class)) {
                    $query->orWhere('organizations.path_primary_key', 'like', getOrgModel()->path_primary_key . '/%');
                }

                if (Gate::allows(AbilityName::MANAGE_USER_MEMBER, User::class)) {
                    $query->orWhere('organizations.organization_type_id', '=', 0);
                }
            });
        }
        return DataTables::eloquent($model)->filter(
            function ($query) use ($orgId, $request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('users.name', 'like', '%' . $keyword . '%')
                            ->orWhere('users.tel', 'like', '%' . $keyword . '%')
                            ->orWhere('users.email', 'like', '%' . $keyword . '%')
                            ->orWhere('organizations.name', 'like', '%' . $keyword . '%');
                    });
                }
                if ($request->filled('active_search') && $request->input('active_search') != -1) {
                    $query->where('active', '=', request('active_search'));
                }
                if ($request->filled('member_type') && request('member_type') != -1) {
                    $memberType = request('member_type');
                    if ($memberType == 1) {
                        $query->where('users.organization_id', '=', $orgId);
                    } elseif ($memberType == 2) {
                        $query->where('users.organization_id', '<>', $orgId);
                        if (getOrgTypeId() == OrgType::ADMIN) {
                            $query->where('organizations.organization_type_id', '>', 1)
                                ->where('organizations.organization_type_id', '<', 100);
                        } else {
                            $query->where(
                                'organizations.path_primary_key',
                                'like',
                                getOrgModel()->path_primary_key . '/%'
                            );
                        }
                    } elseif ($memberType == 3) {
                        $query->where('organizations.organization_type_id', '=', 0);
                    }
                }
            }
        )->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->manageUserAuthor->canDo();

        $currentOrg = getOrgModel();
        $parentId = ($currentOrg->organization_type_id > 1) ? $currentOrg->id : 0;
        $orgTree = OrganizationBusiness::getTreeOrgGov($parentId);

        if ($currentOrg->organization_type_id > 1) {
            $currentOrg->children = $orgTree;
            $orgTypeTree[] = $currentOrg;
        } else {
            $orgTypeTree[] = $currentOrg;
            if (count($orgTree)>0) {
                $orgTypeTree[] = $orgTree[0];
            }
        }

        $orgTypeId = getOrgTypeId();
        $roles = array();//Bouncer::role()->where('scope', '=', $orgTypeId)->select(['id', 'name', 'title'])->get();
        return view('manage.users.create', ['user' => null,
            'roles' => $roles,'orgTypeTree' => $orgTypeTree]);
    }

    public function store(StoreUserRequest $request)
    {
        $user = new User($request->all());
        $pass = str_random(8);
        $user->password_reset = $pass;
        $user->password = Hash::make($pass);
        $user->organization_id = $request->organization_id;

        // TODO cho phép lựa chọn orgid từ giao diện, check quyền để đảm bảo không thể gán bừa org
        $this->manageUserAuthor->canManage($user);

        $user->save();

        // assign roles
        Bouncer::scope()->to($user->organization->organization_type_id);
        if ($request->filled('role')) {
            $roles = $request->role;
            foreach ($roles as $r) {
                $user->assign($r);
            }
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //Check quyền
        $this->manageUserAuthor->canManage($user);

        //Không cho sửa user hiện tại
        $currentLoginUserId = getUserId();
        if ($currentLoginUserId == $user->id) {
            throw new AuthorizationException();
        }

        $orgType = $user->organization->organization_type_id; //getOrgTypeId();
        $userId = $user->id;

        $currentOrg = getOrgModel();
        $parentId = ($currentOrg->organization_type_id > 1) ? $currentOrg->id : 0;
        $orgTree = OrganizationBusiness::getTreeOrgGov($parentId);
        
        if ($currentOrg->organization_type_id > 1) {
            $currentOrg->children = $orgTree;
            $orgTypeTree[] = $currentOrg;
        } else {
            $orgTypeTree[] = $currentOrg;
            if (count($orgTree)>0) {
                $orgTypeTree[] = $orgTree[0];
            }
        }
        
        Bouncer::scope()->to(null);
        $roles = Bouncer::role()::leftjoin("bouncer_assigned_roles as b", function ($query) use ($userId) {
            $query->on('b.role_id', '=', 'bouncer_roles.id')
                ->where('b.entity_id', '=', $userId)
                ->where('b.entity_type', '=', 'App\Models\User');
        })
            ->where('bouncer_roles.scope', '=', $orgType)
            ->select(['bouncer_roles.id', 'bouncer_roles.title', 'bouncer_roles.name',
                DB::raw('if(b.role_id>0, bouncer_roles.name, "") as role_id')
            ])->get();
        
        return view('manage.users.create', ['user' => $user, 'roles' => $roles,
            'orgTypeTree' => $orgTypeTree]);
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        //Check quyền
        $this->manageUserAuthor->canManage($user);

        //Không cho sửa user hiện tại
        $currentLoginUserId = getUserId();
        if ($currentLoginUserId == $user->id) {
            throw new AuthorizationException();
        }

        unset($request['email']);
        $user->fill($request->all());
        if (!$request->filled('active')) {
            $user->active = 0;
        }
        unset($user->email);
        //$user->organization_id = $request->organization_id; //Không cho sửa đơn vị

        $user->save();

        Bouncer::scope()->to($user->organization->organization_type_id);

        // remove roles old
        $allRoles = Bouncer::role()->get();
        foreach ($allRoles as $cRole) {
            $user->retract($cRole->name);
        }

        // assign roles new
        if ($request->filled('role')) {
            $roles = $request->role;
            foreach ($roles as $r) {
                $user->assign($r);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //Check quyền
        $this->manageUserAuthor->canManage($user);

        //Không cho sửa user hiện tại
        $currentLoginUserId = getUserId();
        if ($currentLoginUserId == $user->id) {
            throw new AuthorizationException();
        }

        $user->delete();
    }

    /**
     * Đặt lại mật khẩu
     *
     * @param User $user
     * @throws AuthorizationException
     */
    public function resetPassword(User $user)
    {
        //Check quyền
        $this->manageUserAuthor->canManage($user);

        //Không cho sửa user hiện tại
        $currentLoginUserId = getUserId();
        if ($currentLoginUserId == $user->id) {
            throw new AuthorizationException();
        }

        $pass = str_random(8);
        $user->password_reset = $pass;
        $user->password = Hash::make($pass);
        $user->save();
    }
    
    public function getRoles($orgId)
    {
        //Check quyền
        $organization = Organization::find($orgId);

        if ($organization == null) {
            return '';
        }
        
        $orgTypeId = $organization->organization_type_id;

        $roles = array();
        if ($orgTypeId == OrgType::ADMIN) {
            Bouncer::scope()->to($orgTypeId);
            $roles = Bouncer::role()->get();
        } else {
            Bouncer::scope()->to(null);
            $roles = Bouncer::role()->where('scope', $orgTypeId)->get();
        }

        $sDraw = "echo Former::checkboxes('role[]', '')->checkboxes([";
        foreach ($roles as $r) {
            $sDraw .= "'". $r['title'] . "'" . "=> ['name' => 'role[" .
                    $r['name'] . "]', 'value' => '" . $r['name'] . "'],";
        }
        $sDraw .= "])";
        foreach ($roles as $r) {
            $sDraw .= "->check('role[" . $r['role_id'] . "]')";
        }
        $sDraw .= ";";
        eval($sDraw);
    }
}
