<?php

namespace App\Http\Controllers\Manage;

use App\Models\Order;
use App\Models\Organization;
use App\Models\Region;
use App\OrgType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\Authors\ReportAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use Illuminate\Auth\Access\AuthorizationException;

class ReportOrgController extends Controller
{
    public function indexStatus()
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        $districts = [];
        if (getOrgId() == OrgType::VPDP) {
            $districts = Region::select('organizations.id', 'regions.name')
                ->join("organizations", 'organizations.region_id', '=', 'regions.id')
                ->where('regions.level', '=', 2)
                ->where('regions.parent_id', '=', 26)
                ->where('organizations.organization_type_id', '<', 100)
                ->orderBy('regions.name')->get();
        }
        return view('manage.reports.rank_org_status', ['Districts' => $districts]);
    }

    public function statisticStatus(Request $request)
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        $orgId = getOrgId();
        $districtId = $request->huyen_id ?? 0;
        $data = [];

        if (Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*') && $districtId == 0) {
            $data = DB::select("select r.name, tb.so_co_so, tb.chua_duyet, tb.da_duyet,
                  tb.ngung_hoat_dong, tb.vi_pham
                from organizations r
                left join
                    (select o.id, count(s.id) as so_co_so,
                    sum(if(s.status=0,1,0)) as chua_duyet, sum(if(s.status=1,1,0)) as da_duyet,
                    sum(if(s.status=2,1,0)) as ngung_hoat_dong, sum(if(s.status=3,1,0)) as vi_pham
                    from organizations s
                    inner join organizations m on m.id=s.manage_organization_id
                    inner join organizations o on o.path_primary_key=left(m.path_primary_key, 5)
                    where s.organization_type_id >= 100 and (m.path_primary_key like '2/%' or m.path_primary_key = 2)
                    and m.organization_type_id < 100
                    and s.deleted_at is null and m.deleted_at is null
                    group by o.id)
                tb on tb.id=r.id where r.manage_organization_id<=2 and r.organization_type_id BETWEEN 2 and 99
                order by r.organization_type_id, r.name");
        } elseif ((!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')
                && Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) || $districtId != 0) {
            if ($orgId == OrgType::VPDP) {
                $orgId = $districtId;
            }
            $data = DB::select("select r.name, tb.so_co_so, tb.chua_duyet, tb.da_duyet, tb.ngung_hoat_dong, tb.vi_pham,
                    if(r.id=$orgId, 1, 0) as main
                    from organizations r
                    left join
                        (select m.id, count(s.id) as so_co_so,
                        sum(if(s.status=0,1,0)) as chua_duyet, sum(if(s.status=1,1,0)) as da_duyet,
                        sum(if(s.status=2,1,0)) as ngung_hoat_dong, sum(if(s.status=3,1,0)) as vi_pham,
                        m.path_primary_key
                        from organizations s
                        inner join organizations m on m.id=s.manage_organization_id
                        where s.organization_type_id >= 100 and m.path_primary_key like '2/$orgId%'
                        and m.organization_type_id in (6,7)
                        and m.organization_type_id < 100 and s.deleted_at is null and m.deleted_at is null
                        group by m.id)
                    tb on tb.path_primary_key=r.path_primary_key
                    where r.path_primary_key like '2/$orgId%' and r.organization_type_id in (6,7)
                    order by main desc, r.name");
        }

        $fileName = "tk_so_luong_co_so.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);

        $i = 1;
        $row = 4;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d->name);
            $sheet->setCellValue("C$row", $d->so_co_so);
            $sheet->setCellValue("D$row", $d->chua_duyet);
            $sheet->setCellValue("E$row", $d->da_duyet);
            $sheet->setCellValue("F$row", $d->ngung_hoat_dong);
            $sheet->setCellValue("G$row", $d->vi_pham);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A5:G$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }
}
