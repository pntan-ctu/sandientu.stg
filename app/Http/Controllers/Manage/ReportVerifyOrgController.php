<?php

namespace App\Http\Controllers\Manage;

use App\Models\Organization;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Manage;
use Yajra\DataTables\Facades\DataTables;
use App\Business\OrganizationBusiness;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use Illuminate\Auth\Access\AuthorizationException;

class ReportVerifyOrgController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        $list = OrganizationBusiness::thongKeTinhHinhXL();
        return view('manage.report_verify_org.index', ['list'=>$list]);
    }

    public function indexData(Request $request)
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        return OrganizationBusiness::thongKeTinhHinhXL();
        
        /*$model = Organization::where('organizations.organization_type_id','>',0)
                ->where('organizations.organization_type_id','<',100)
                ->leftJoin('organizations as cs', function($join) {
                    $join->on('cs.manage_organization_id','=','organizations.id')
                         ->where('cs.organization_type_id','>=','100')
                         ->where('cs.status','=', 0);
                })
                ->leftJoin('contacts', function($join) {
                    $join->on('contacts.organization_id','=','organizations.id')
                         ->where('contacts.status','=', 0);
                })
                ->groupBy('organizations.id')
                ->selectRaw('organizations.id, organizations.name, '
                        . 'count(ifnull(cs.id, 0)) as cs_chua_duyet, '
                        . 'count(ifnull(contacts.id, 0)) as ph_chua_xl');
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('gov.name', 'like', '%' . $keyword . '%');
                });
            }
        })->make();*/
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Weblink  $weblink
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Weblink  $weblink
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Weblink  $weblink
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Weblink  $weblink
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
    }
}
