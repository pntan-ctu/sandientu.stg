<?php

namespace App\Http\Controllers\Manage;

use App\Models\DocumentType;
use App\Models\DocumentField;
use App\Models\DocumentOrgan;
use App\Models\Document;
use Illuminate\Http\Request;
use App\Http\Requests\StoreDocumentRequest;
use App\Http\Requests\UpdateDocumentRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\DB;
use App\Authors\DocumentAuthor;

class DocumentController extends Controller
{
    protected $documentAuthor;

    public function __construct()
    {
        $this->documentAuthor = new DocumentAuthor();
    }

    public function index()
    {
        //Check quyền
        $this->documentAuthor->canDo();

        $docType = DocumentType::all();
        $docOrgan = DocumentOrgan::all();
        $docField = DocumentField::all();
        return view('manage.document.index', ['docType' => $docType, 'docOrgan' => $docOrgan, 'docField' => $docField]);
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->documentAuthor->canDo();
        
        $model = Document::select(['id', 'number_symbol', 'quote',
                    DB::raw('date_format(sign_date, "%d/%m/%Y") as sign_date1')]);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('number_symbol', 'like', '%' . $keyword . '%')
                                    ->orWhere('quote', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('idDocType') && $request->input('idDocType') != 0) {
                $query->where('document_type_id', '=', request('idDocType'));
            }
            if ($request->filled('idDocField') && $request->input('idDocField') != 0) {
                $query->where('document_field_id', '=', request('idDocField'));
            }
            if ($request->filled('idDocOrgan') && $request->input('idDocOrgan') != 0) {
                $query->where('document_organ_id', '=', request('idDocOrgan'));
            }
        })->make();
    }

    public function store(StoreDocumentRequest $request)
    {
        //Check quyền
        $this->documentAuthor->canDo();

        $doc = new Document();
        $fileAttachment = UploadFileUtil::uploadFile($request, 'fileAttachment');

        Document::create([
            'number_symbol' => $request->number_symbol,
            'description' => $request->description,
            'sign_by' => $request->sign_by,
            'document_field_id' => $request->document_field_id,
            'document_type_id' => $request->document_type_id,
            'document_organ_id' => $request->document_organ_id,
            'quote' => $request->quote,
            //'duty' => '',
            'status' => 1,
            'sign_date' => $request->sign_date,
            'document_path' => isset($fileAttachment) ? $fileAttachment->path : null,
        ]);
    }

    public function create()
    {
        //Check quyền
        $this->documentAuthor->canDo();

        $docType = DocumentType::all();
        $docOrgan = DocumentOrgan::all();
        $docField = DocumentField::all();
        return view(
            'manage.document.create',
            ['doc' => null,
            'docType' => $docType,
            'docOrgan' => $docOrgan,
            'docField' => $docField]
        );
    }

    public function show($id)
    {
        //Check quyền
        $this->documentAuthor->canDo();
        
        $doc = Document::find($id);

        return view('manage.document.show', ['doc' => $doc]);
    }

    public function edit($id)
    {
        //Check quyền
        $this->documentAuthor->canDo();

        $doc = Document::find($id);
        $file = UploadFileUtil::getFileMetadata($doc->document_path);
        $fileName = isset($file) ? $file->file_name : '';
        $docType = DocumentType::all();
        $docOrgan = DocumentOrgan::all();
        $docField = DocumentField::all();
        return view(
            'manage.document.edit',
            ['fileName' => $fileName,
            'doc' => $doc,
            'docType' => $docType,
            'docOrgan' => $docOrgan,
            'docField' => $docField]
        );
    }

    public function update(UpdateDocumentRequest $request, $id)
    {
        //Check quyền
        $this->documentAuthor->canDo();

        $doc = Document::find($id);

        if (!$doc) {
            return response()
                            ->json(['error' => 'Văn bản không tồn tại']);
        }
        $fileAttachment = UploadFileUtil::uploadFile($request, 'fileAttachment');
        $doc->update([
            'number_symbol' => $request->number_symbol,
            'description' => $request->description,
            'sign_by' => $request->sign_by,
            'document_field_id' => $request->document_field_id,
            'document_type_id' => $request->document_type_id,
            'document_organ_id' => $request->document_organ_id,
            'quote' => $request->quote,
            //'duty' => '',
            'status' => 1,
            'sign_date' => $request->sign_date,
            'document_path' => isset($fileAttachment) ? $fileAttachment->path : $doc->document_path,
        ]);
        $doc->save();
    }

    public function destroy($id)
    {
        //Check quyền
        $this->documentAuthor->canDo();

        $doc = Document::find($id);

        if (!$doc) {
            return response()->json(['error' => 'Văn bản không tồn tại!']);
        }
        
        $doc->delete();

        return response()->json(['message' => 'Xóa thành công!']);
    }
}
