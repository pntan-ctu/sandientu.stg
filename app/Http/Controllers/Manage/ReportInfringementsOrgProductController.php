<?php

namespace App\Http\Controllers\Manage;

use App\Models\Organization;
use App\Models\Product;
use App\Models\Infringement;
use App\Models\InfringementCategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Cell\Coordinate;
use PhpOffice\PhpSpreadsheet\Style\Style;
use \PhpOffice\PhpSpreadsheet\Style\Alignment;
use App\Authors\ReportAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use Illuminate\Auth\Access\AuthorizationException;

class ReportInfringementsOrgProductController extends Controller
{
    public function index()
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }

        return view('manage.reports.rank_infringements_org_product');
    }

    public function reportType(Request $request)
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        $type = $request->report_type;
        switch ($type) {
            case "org":
                return $this->statisticByOrg($request);
                break;
            case "product":
                $this->statisticByProduct($request);
                break;
        }
    }

    public function statisticByProduct(Request $request)
    {
      
        $proClass = Product::class;
        $query = Infringement::join(
            'infringement_categories',
            'infringement_categories.id',
            '=',
            'infringements.category_id'
        )
            ->join('products', 'products.id', '=', 'infringements.infringementable_id')
            ->join(
                'organizations as b',
                'b.id',
                '=',
                'products.organization_id'
            )
            ->join('organizations as d', 'd.id', '=', 'b.manage_organization_id');

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $query = $query->where("d.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $query = $query->whereRaw("d.path_primary_key like '$path%'");
            }
        }

        $query = $query->where('infringements.infringementable_type', '=', $proClass);
        if ($request->filled('from_date')) {
            $fromDate = parseDate($request->from_date);
            $query = $query->where('infringements.created_at', '>=', $fromDate);
            $time = "Từ ngày ".$request->from_date;
        }
        if ($request->filled('to_date')) {
            $toDate = parseDate_end($request->to_date);
            $query = $query->where('infringements.created_at', '<=', $toDate);
            $time .= ($request->filled('from_date') ? " đ" : " Đ")."ến ngày ".$request->to_date;
        }
            $query=$query->groupBy('infringements.infringementable_id')
            ->selectRaw("products.id, products.name as pro_name, count(infringements.id) as count_total ");
            //->get();
        $cates = InfringementCategory::where('type', '=', 1)->get();
        foreach ($cates as $ct) {
            $query->selectRaw("sum(if(infringements.category_id = $ct->id, 1, 0)) as count_$ct->code");
        }
        $data = $query->get();
        $fileName = "tk_san_pham_bi_phan_anh_vi_pham.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);
        $col = 4;
        foreach ($cates as $ct) {
            $sheet->setCellValueByColumnAndRow($col, 6, $ct->code);
            $col++;
        }
        $i = 1;
        $row = 6;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['pro_name']);
            $sheet->setCellValue("C$row", $d['count_total']);
            $col = 4;
            foreach ($cates as $ct) {
                $sheet->setCellValueByColumnAndRow($col, $row, $d["count_$ct->code"]);
                $col++;
            }
            $i++;
        }
        $style = [
        "borders" => [
            "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
        ]
        ];
        $col = 3 + count($cates);
        $row = 6 + count($data);
        
        $col_Letter = Coordinate::stringFromColumnIndex($col);

        $sheet->mergeCells("A3:{$col_Letter}3");

        $sheet->getStyle("A5:{$col_Letter}{$row}")->applyFromArray($style);
        $sheet->mergeCells("C5:{$col_Letter}5");
        $row_note = $row+2;
        $row_detail = $row+3;
        $sheet->mergeCells("A{$row_note}:B{$row_note}")
        ->getStyle("A$row_detail")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->setCellValue("A$row_note", 'Chú thích:');
        foreach ($cates as $ct) {
            $sheet->setCellValue("A$row_detail", $ct->code.': ')
                    ->getStyle("A$row_detail")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
            $sheet->setCellValue("B$row_detail", $ct->name);
            $row_detail++;
        }
        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }
        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }

    public function statisticByOrg(Request $request)
    {
        $orgClass = Organization::class;
        $query = Infringement::join(
            'infringement_categories',
            'infringement_categories.id',
            '=',
            'infringements.category_id'
        )
            ->join('organizations', 'organizations.id', '=', 'infringements.infringementable_id')
            ->join('organizations as d', 'd.id', '=', 'organizations.manage_organization_id');

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $query = $query->where("d.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $query = $query->whereRaw("d.path_primary_key like '$path%'");
            }
        }

        $query = $query->where('infringements.infringementable_type', '=', $orgClass);

        if ($request->filled('from_date')) {
            $fromDate = parseDate($request->from_date);
            $query = $query->where('infringements.created_at', '>=', $fromDate);
            $time = "Từ ngày ".$request->from_date;
        }
        if ($request->filled('to_date')) {
            $toDate = parseDate_end($request->to_date);
            $query = $query->where('infringements.created_at', '<=', $toDate);
            $time .= ($request->filled('from_date') ? " đ" : " Đ")."ến ngày ".$request->to_date;
        }
            $query=$query->groupBy('infringements.infringementable_id')
            ->selectRaw("organizations.id, organizations.name, count(infringements.id) as count_total ");
            //->get();
        $cates = InfringementCategory::where('type', '=', 0)->get();
        foreach ($cates as $ct) {
            $query->selectRaw("sum(if(infringements.category_id = $ct->id, 1, 0)) as count_$ct->code");
        }
        $data = $query->get();
        $fileName = "tk_co_so_bi_phan_anh_vi_pham.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);
        $col = 4;
        foreach ($cates as $ct) {
            $sheet->setCellValueByColumnAndRow($col, 6, $ct->code);
            $col++;
        }
        $i = 1;
        $row = 6;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['name']);
            $sheet->setCellValue("C$row", $d['count_total']);
            $col = 4;
            foreach ($cates as $ct) {
                $sheet->setCellValueByColumnAndRow($col, $row, $d["count_$ct->code"]);
                $col++;
            }
            $i++;
        }
        $style = [
        "borders" => [
            "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
        ]
        ];
        $col = 3 + count($cates);
        $row = 6 + count($data);
        
        $colLetter = Coordinate::stringFromColumnIndex($col);

        $sheet->mergeCells("A3:{$colLetter}3");

        $sheet->getStyle("A5:{$colLetter}{$row}")->applyFromArray($style);
        $sheet->mergeCells("C5:{$colLetter}5");
        $rownote = $row+2;
        $rowdetail = $row+3;
        $sheet->mergeCells("A{$rownote}:B{$rownote}")
        ->getStyle("A$rowdetail")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_LEFT);
        $sheet->setCellValue("A$rownote", 'Chú thích :');
        foreach ($cates as $ct) {
            $sheet->setCellValue("A$rowdetail", $ct->code.' : ')
                    ->getStyle("A$rowdetail")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_RIGHT);
            $sheet->setCellValue("B$rowdetail", $ct->name);
            $rowdetail++;
        }
        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }
        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }
}
