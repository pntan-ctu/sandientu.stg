<?php

namespace App\Http\Controllers\Manage;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\NewsArticle;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use App\Http\Requests\StoreNewsRequest;
use App\Http\Requests\UpdateNewsRequest;
use Illuminate\Support\Facades\DB;
use App\Constants;
use App\Authors\NewsArticleAuthor;

class NewsController extends Controller
{
    protected $newsArticleAuthor;

    public function __construct()
    {
        $this->newsArticleAuthor = new NewsArticleAuthor();
    }

    public function index()
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $treeGroupData = NewsController::getTreeNewsGroups(0);
        $treeRelateGroupData = NewsController::getTreeRelateNewsGroups(0, -1, 'none');
        return view(
            'manage.news.index',
            ['treeGroupData' => $treeGroupData, 'treeRelateGroupData' => $treeRelateGroupData]
        );
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $id = request('sGroupId');
        //if ($id != "1") {
        $listIds = NewsGroupController::getIdsOfAllChild($id);
        $where = 'news_articles.group_id in (' . $listIds . ')';
        //} else {
        //    $where = $id . '=1';
        //}

        $model = NewsArticle::join('news_groups', 'news_groups.id', '=', 'news_articles.group_id')
            ->join('users', 'users.id', '=', 'news_articles.created_by')
            ->whereRaw($where)
            ->select(['news_articles.id', 'news_groups.name', 'news_articles.title', 'news_articles.summary',
                'news_articles.img_path', 'news_articles.path', 'news_articles.status', 'news_articles.group_id',
                'news_articles.relate_news', 'users.name as nguoi_dang', 'news_articles.created_at',
                DB::raw('date_format(news_articles.publish_at, "%d/%m/%Y %H:%i") as ngay_dang')]);
        //->orderBy('news_articles.created_at', 'desc');

        $data = DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('news_articles.title', 'like', '%' . $keyword . '%')
                        ->orWhere('news_articles.summary', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('news_articles.status', '=', request('active'));
            }
            if ($request->filled('newsId')) {
                $query->where('news_articles.id', '!=', request('newsId'));
            }
        })->make();

        $mainData = $data->original['data'];

        $start = request('start');
        foreach ($mainData as $i => $j) {
            $mainData[$i]['url'] = NewsGroupController::getFullUrl($mainData[$i]['group_id']);
            $mainData[$i]['stt'] = $start + $i + 1;
        }
        $data = json_decode(json_encode($data), true);
        $data['original']['data'] = $mainData;
        return $data['original'];
    }

    public function create()
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $treeGroupData = $this->getTreeNewsGroups(0);
        $treeRelateGroupData = $this->getTreeRelateNewsGroups(0, -1);
        return view(
            'manage.news.create',
            ['treeGroupData' => $treeGroupData, 'treeRelateGroupData' => $treeRelateGroupData]
        );
    }

    public function store(StoreNewsRequest $request)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $userId = auth()->user()->id;
        $id = (int)$request->id;
        if ($id == 0) {
            $NewsArticle = new NewsArticle();
            $NewsArticle->created_by = $userId;
        } else {
            $NewsArticle = NewsArticle::find($id);
        }

        $NewsArticle->group_id = $request->group_id;
        $NewsArticle->title = $request->title;
        $NewsArticle->summary = $request->summary;
        $NewsArticle->content = request('content'); //$NewsArticle->content = request->content;

        if (isset($request->img_path)) {
            $file = UploadFileUtil::UploadImage($request, 'img_path');
            $NewsArticle->img_path = $file->path ?? null;
        } elseif (!isset($request->image_url)) {
            $NewsArticle->img_path = null;
        }

        $NewsArticle->hotnews = (int)$request->hotnews;
        $NewsArticle->comment = (int)$request->comment;
        $NewsArticle->shared = (int)$request->shared;
        $NewsArticle->tags = $request->tags;

        $NewsArticle->updated_by = $userId;

        $PublishBy = (int)$NewsArticle->publish_by;
        $status = (int)$request->status;
        $NewsArticle->status = $status;
        if ($PublishBy == 0 && $status == 1) {
            $NewsArticle->publish_by = $userId;
            $NewsArticle->publish_at = date('Y-m-d H:i:s');
        }

        $NewsArticle->save();
        return redirect('manage/news');
    }

    public function show(NewsArticle $NewsArticle)
    {
        // Với chức năng phức tạp cần xây dựng action này
        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    }

    public function update(UpdateNewsRequest $request, NewsArticle $NewsArticle)
    {
    }

    public function destroy($newsId)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $news = NewsArticle::find($newsId);
        $userId = auth()->user()->id;
        $news->deleted_by = $userId;
        $news->deleted_at = date('Y-m-d H:i:s');
        $news->save();
    }

    public function edit($newsId)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $treeGroupData = $this->getTreeNewsGroups(0);
        $news = NewsArticle::join(
            'news_groups',
            'news_groups.id',
            '=',
            'news_articles.group_id'
        )
            ->select(
                'news_articles.id',
                'news_articles.title',
                'news_articles.summary',
                'news_articles.content',
                'news_articles.img_path',
                'news_articles.hotnews',
                'news_articles.comment',
                'news_articles.shared',
                'news_articles.tags',
                'news_articles.relate_news',
                'news_articles.status',
                'news_articles.group_id',
                'news_groups.name'
            )
            ->where('news_articles.id', '=', $newsId)->get();
        $groupId = $news[0]->group_id;
        $news[0]['url'] = NewsGroupController::getFullUrl($groupId);
        $treeRelateGroupData = $this->getTreeRelateNewsGroups(0, $groupId);
        return view(
            'manage.news.create',
            ['news' => $news[0], 'treeGroupData' => $treeGroupData, 'treeRelateGroupData' => $treeRelateGroupData]
        );
    }

    /************ get data for drop-down-list (combobox) with tree *************/
    public static function getTreeNewsGroups($parent_id, $url = "none", $nonePrefix = 0)
    {
        $fullUrl = $url != "none" ? "concat('" . $url . " / ', name) as url" : "name as url";
        $returnVal = DB::table("news_groups")
            ->select(
                'id as value',
                'name as title',
                'parent_id',
                DB::raw($fullUrl)
            )
            ->where('parent_id', '=', $parent_id);
        if ($nonePrefix == 1) {
            $returnVal = $returnVal->where('id', '!=', Constants::PREFIX_NEWS_GROUP_ID);
        }
        $returnVal = $returnVal->whereNull('deleted_at')->orderBy("no")->orderBy("name")->get();
        foreach ($returnVal as $key => $v) {
            $children = NewsController::getTreeNewsGroups($v->value, $v->url);
            if (count($children) > 0) {
                $returnVal[$key]->children = $children;
            }
        }
        return $returnVal;
    }

    /************ get data for tree with checkbox *************/
    public static function getTreeRelateNewsGroups($parent_id, $current_id, $url = "none")
    {
        $fullUrl = $url != "none" ? "concat('" . $url . " / ', news_groups.name) as url" : "news_groups.name as url";
        $returnVal = DB::table("news_groups")
            ->select(
                'news_groups.id',
                'news_groups.name as text',
                'news_groups.parent_id',
                'news_groups.description',
                'news_groups.no',
                'news_groups.active',
                DB::raw('(select count(b.id) from news_articles b
                            where b.group_id=news_groups.id and deleted_at is null) as news_count'),
                DB::raw($fullUrl),
                DB::raw('(select ifnull(MAX(c.no),0) + 1
                            from news_groups c where c.parent_id=news_groups.id and deleted_at is null) as next_no')
            )
            ->where('parent_id', '=', $parent_id)
            //->where('id', '!=', Constants::PREFIX_NEWS_GROUP_ID)
            ->whereNull('deleted_at')->orderBy("no")->orderBy("name")->get();
        foreach ($returnVal as $key => $value) {
            $children = NewsController::getTreeRelateNewsGroups($value->id, $current_id, $value->url);
            if ($value->id == $current_id) {
                $returnVal[$key]->checked = true;
            }
            if (count($children) > 0) {
                //if($value->id > 0) $returnVal[$key]->state = "closed";
                $returnVal[$key]->children = $children;
            }
        }
        return $returnVal;
    }

    //$newsId, $listIdRelates
    public function updateRelateNews(Request $request)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();

        $newsId = request('iNewsId');
        $listIdRelates = request('sIdRelate');
        $news = NewsArticle::find($newsId);
        $news->relate_news = $listIdRelates;
        $news->save();
        return json_encode(array("success" => true));
    }

    public function getRelateNews(Request $request)
    {
        //Check quyền
        $this->newsArticleAuthor->canDo();
        
        $where = 'news_articles.group_id != '.Constants::PREFIX_NEWS_GROUP_ID;
        $id = request('sGroupRelateId');
        if ($id != "0") {
            $listIds = NewsGroupController::getIdsOfAllChild($id);
            $where .= ' and news_articles.group_id in (' . $listIds . ')';
        }

        $listRelateIds = request('sRelateId') != null ? request('sRelateId') : "0";

        $model = NewsArticle::join(
            'news_groups',
            'news_groups.id',
            '=',
            'news_articles.group_id'
        )
            ->whereRaw($where)
            ->where('news_articles.id', '!=', request('newsId'))
            ->select(['news_articles.id', 'news_groups.name', 'news_articles.title', 'news_articles.summary',
                'news_articles.img_path', 'news_articles.path', 'news_articles.status', 'news_articles.group_id',
                'news_articles.relate_news',
                DB::raw('case when news_articles.id in (' . $listRelateIds . ') then 1 else 0 end as checked')]);
        //->orderBy('news_articles.created_at', 'desc');

        $data = DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('news_articles.title', 'like', '%' . $keyword . '%')
                        ->orWhere('news_articles.summary', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('news_articles.status', '=', request('active'));
            }
        })->make();
        
        return $data;
    }
}
