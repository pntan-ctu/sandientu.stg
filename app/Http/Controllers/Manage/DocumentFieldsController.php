<?php

namespace App\Http\Controllers\Manage;

use App\Models\DocumentField;
use Illuminate\Http\Request;
use App\Http\Requests\StoreDocumentFieldRequest;
use App\Http\Requests\UpdateDocumentFieldRequest;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Facades\DataTables;
use App\Authors\DocumentFieldAuthor;

class DocumentFieldsController extends Controller
{
    protected $documentFieldAuthor;

    public function __construct()
    {
        $this->documentFieldAuthor = new DocumentFieldAuthor();
    }
    
    public function index()
    {
        //Check quyền
        $this->documentFieldAuthor->canDo();

        return view('manage.docfield.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->documentFieldAuthor->canDo();
        
        $model = DocumentField::select(['id', 'name', 'no', 'active']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%');
                });
            }
        })->make();
    }

    public function store(StoreDocumentFieldRequest $request)
    {
        //Check quyền
        $this->documentFieldAuthor->canDo();

        $docField = new DocumentField();
        $docField->fill($request->all());
        $docField["active"] = $request->active == true ? 1 : 0;
        $docField->save();
    }

    public function create()
    {
        //Check quyền
        $this->documentFieldAuthor->canDo();

        return view('manage.docfield.create', ['docField' => null]);
    }

    public function edit($id)
    {
        //Check quyền
        $this->documentFieldAuthor->canDo();

        $docField = DocumentField::find($id);
        return view('manage.docfield.edit', ['docField' => $docField]);
    }

    public function update(UpdateDocumentFieldRequest $request, $id)
    {
        //Check quyền
        $this->documentFieldAuthor->canDo();

        $docField = DocumentField::find($id);

        if (!$docField) {
            return response()->json(['error' => 'Lĩnh vực văn bản không tồn tại']);
        }
        if ($request["active"] == null) {
            $request["active"] = 0;
        }
        $docField->update([
            'name' => $request->name,
            'no' => $request->no,
            'active' => $request->active == true ? 1 : 0,
        ]);
        $docField->save();
    }

    public function destroy($id)
    {
        //Check quyền
        $this->documentFieldAuthor->canDo();

        $docField = DocumentField::find($id);
        if (!$docField) {
            return response()
                            ->json(['error' => 'Lĩnh vực văn bản không tồn tại!']);
        }
        $docField->delete();
        return response()->json(['message' => 'Xóa thành công!']);
    }
}
