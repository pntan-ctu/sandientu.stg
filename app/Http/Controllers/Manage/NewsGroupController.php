<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\StoreNewsGroupRequest;
use App\Http\Requests\UpdateNewsGroupRequest;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use Yajra\DataTables\Facades\DataTables;
use App\Models\NewsGroup;
use App\Http\Controllers\Manage\NewsController;
use App\Constants;
use App\Authors\NewsGroupAuthor;

class NewsGroupController extends Controller
{
    protected $newsGroupAuthor;

    public function __construct()
    {
        $this->newsGroupAuthor = new NewsGroupAuthor();
    }
    
    public function index()
    {
        //Check quyền
        $this->newsGroupAuthor->canDo();

        $treeNewsGroupData = NewsController::getTreeRelateNewsGroups(0, 0);
        $cboTreeGroupData = NewsController::getTreeNewsGroups(0, 'none', 1);
        return view(
            'manage.news_group.index',
            ['treeNewsGroupData' => $treeNewsGroupData, 'cboTreeGroupData' => $cboTreeGroupData,
                'PREFIX_NEWS_GROUP_ID' => Constants::PREFIX_NEWS_GROUP_ID]
        );
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->newsGroupAuthor->canDo();

        $model = NewsGroup::leftjoin('news_groups as a', 'a.id', '=', 'news_groups.parent_id')
        ->select(['news_groups.id', 'news_groups.parent_id', 'news_groups.name', 'news_groups.description',
            'news_groups.active', 'a.name as parent_name']);
        $data = DataTables::eloquent($model)->make();
        return $data;
    }

    public function create()
    {
        //return view('manage.news_group.create');
    }

    public function store(StoreNewsGroupRequest $request)
    {
        //Check quyền
        $this->newsGroupAuthor->canDo();

        $id = (int)$request->id;
        if ($id == 0) {
            $newsGroup = new NewsGroup();
        } else {
            $newsGroup = NewsGroup::find($id);
        }

        $newsGroup->fill($request->all());

        if ($id == 0) {
            unset($newsGroup->id);
        }

        $newsGroup->name = removeSpecialCharacters($request->name, true);

        if ((int)$request->no == 0) {
            $maxNo = NewsGroup::where('parent_id', $request->parent_id)->max('no');
            $newsGroup->no = $maxNo + 1;
        }
        $newsGroup->save();

        return redirect("manage/news-group");
    }

    public function show(NewsGroup $newsGroup)
    {
        // Với chức năng phức tạp cần xây dựng action này
        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    }

    public function edit(NewsGroup $newsGroup)
    {
        //Check quyền
        $this->newsGroupAuthor->canDo();

        $tree = NewsGroup::with('children')->where('parent_id', '=', 1)->get();
        return view('manage.news_group.create', ['newsGroup' => $newsGroup, 'tree' => $tree]);
    }

    public function update(UpdateNewsGroupRequest $request, NewsGroup $newsGroup)
    {
        /*$newsGroup->fill($request->all());
        if(!isset($request->active)) $newsGroup->active = 0;
        $newsGroup->save();*/
    }

    public function destroy($iIdNewsGroup)
    {
        //Check quyền
        $this->newsGroupAuthor->canDo();
        
        $newsGroup = NewsGroup::find($iIdNewsGroup);
        $newsGroup->delete();
    }

    public static function getIdsOfAllChild($id)
    {
        $s = $id;
        $Results = NewsGroup::whereRaw('parent_id in ('.$id.')')->get()->toArray();
        foreach ($Results as $r) {
            if (isset($r['id'])) {
                $s .= ",".(NewsGroupController::getIdsOfAllChild($r['id']));
            }
        }
        return $s;
    }

    public static function getFullUrl($id)
    {
        $group = NewsGroup::find($id);
        $url = $group->name;
        $parentId = $group->parent_id;
        while ($parentId > 0) {
            $group = NewsGroup::find($parentId);
            $url = $group->name . " / " . $url;
            $parentId = $group->parent_id;
        }
        return $url;
    }

    public function getNextNo($parentId)
    {
        $maxNo = NewsGroup::where('parent_id', $parentId)->max('no');
        return $maxNo + 1;
    }
}
