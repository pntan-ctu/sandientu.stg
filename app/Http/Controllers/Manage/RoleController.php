<?php

namespace App\Http\Controllers\Manage;

use App\Models\Module;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Silber\Bouncer\BouncerFacade as Bouncer;
use Yajra\DataTables\Facades\DataTables;
use Silber\Bouncer\Database\Role;
use App\Models\ModuleRole;
use App\Models\OrganizationType;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\AuthorizationException;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $organizationTypes = OrganizationType::where('id', '>', 1)->select(['id', 'name'])->get();
        return view('manage.roles.index', ['organizationTypes' => $organizationTypes]);
    }

    public function indexData(Request $request)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $scope = request('organization_type_id');
        $model = Bouncer::role()->where('scope', '=', $scope);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('name', 'like', '%' . $keyword . '%')
                        ->orWhere('title', 'like', '%' . $keyword . '%');
                });
            }
        })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $organizationTypes = OrganizationType::where('id', '>', 0)->select(['id', 'name'])->get();
        return view('manage.roles.create', ['role' => null, 'organizationTypes' => $organizationTypes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreRoleRequest $request)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        Bouncer::scope()->to($request->scope);
        Bouncer::role()->firstOrCreate([
            'name' => $request->name,
            'title' => $request->title
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function show(Role $role)
    {
        // Với chức năng phức tạp cần xây dựng action này
        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $organizationTypes = OrganizationType::where('id', '>', 0)->select(['id', 'name'])->get();
        return view('manage.roles.create', ['role' => $role, 'organizationTypes' => $organizationTypes]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateRoleRequest $request
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        Bouncer::scope()->to($request->scope);
        $role->fill($request->all());
        $role->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $role->delete();
    }

    public function getModules(Request $request)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $roleId = $request->filled('role_id') ? request('role_id') : 0;
        $scope = 0;
        if ($roleId > 0) {
            $role = Bouncer::role()->where('id', '=', $roleId)->firstOrFail();
            $scope = $role->scope ?? 0;
        }

        Bouncer::scope()->to($scope);

        $model = Bouncer::ability()
            ->leftjoin('bouncer_permissions as p', function ($join) use ($scope, $roleId) {
                $join->on('p.ability_id', '=', 'bouncer_abilities.id');
                $join->where('p.scope', '=', $scope);
                $join->where('p.entity_id', '=', $roleId);
            });
        if ($scope >= 100) {
            $model = $model->where('bouncer_abilities.scope', '=', $scope);
        }
        $model = $model->orderBy('id', 'asc')
            ->select(['bouncer_abilities.id', 'bouncer_abilities.group as group_name',
            'bouncer_abilities.title as name', DB::raw('ifnull(p.entity_id, 0) as checked')]);

        $data = DataTables::eloquent($model)->make();
        return $data;
    }

    public function updateModuleRoles(Request $request)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $scope = request('scope');
        $roleId = request('role_id');
        $listModuleIds = request('module_ids');
        $permissions = [];
        if (strlen($listModuleIds) > 0) {
            $ModuleIds = explode(",", $listModuleIds);
            foreach ($ModuleIds as $mId) {
                $perItem = [
                    'ability_id' => $mId,
                    'entity_id' => $roleId,
                    'entity_type' => 'roles',
                    'scope' => $scope
                ];
                $permissions[] = $perItem;
            }
        }

        DB::table('bouncer_permissions')
            ->where('entity_id', '=', $roleId)
            ->where('scope', '=', $scope)
            ->delete();
        if (count($permissions) > 0) {
            DB::table('bouncer_permissions')->insert($permissions);
        }

        return json_encode(array("success" => true));
    }
}
