<?php

namespace App\Http\Controllers\Manage;

use App\Models\Album;
use App\Models\AlbumDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Image;
use Illuminate\Support\Facades\Storage;
use App\Authors\AlbumAuthor;

//use App\Http\Controllers\Manage;



class AlbumDetailController extends Controller
{
    protected $albumAuthor;

    public function __construct()
    {
        $this->albumAuthor = new AlbumAuthor();
    }

    public function index()
    {
    }

    public function store()
    {
    }

    public function create()
    {
    }

    public function show()
    {
    }

    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->albumAuthor->canDo();

        $alb = AlbumDetail::find($id);
        if (!$alb) {
            return response()
                            ->json(['error' => 'error: Ảnh không tồn tại!']);
        }
        $alb->delete();
    }
}
