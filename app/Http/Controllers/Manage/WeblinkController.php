<?php

namespace App\Http\Controllers\Manage;

use App\Models\Weblink;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use App\Http\Controllers\Manage;
use Image;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use App\Utils\ResizeImageUtil;
use App\Http\Requests\StoreWeblinkRequest;
use App\Models\FileMetadata;
use App\Http\Requests\UpdateWeblinkRequest;
use App\Constants;
use App\Authors\WeblinkAuthor;

class WeblinkController extends Controller
{
    protected $weblinkAuthor;

    public function __construct()
    {
        $this->weblinkAuthor = new WeblinkAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->weblinkAuthor->canDo();

        return view('manage.weblink.index', ['group_id' => Constants::WEBLINK_GROUPS]);
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->weblinkAuthor->canDo();
        
        $model = Weblink::select(['*']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            $query->select(
                'weblinks.title as title',
                'weblinks.url as url',
                'weblinks.img_path as img_path',
                'weblinks.no as no',
                'weblinks.group_id as group_id',
                'weblinks.new_window as new_window',
                'weblinks.active as active',
                'weblinks.id as id',
                'users.name as user_name'
            )
                    ->join('users', 'weblinks.created_by', '=', 'users.id');
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('weblinks.img_path', 'like', '%' . $keyword . '%')
                                    ->orWhere('weblinks.title', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('weblinks.group_id', '=', request('active'));
            }
        })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->weblinkAuthor->canDo();

        return view('manage.weblink.create', ['weblink' => null,'group_id' => Constants::WEBLINK_GROUPS]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreWeblinkRequest $request)
    {
        //Check quyền
        $this->weblinkAuthor->canDo();

        $weblink = new Weblink();
        if (isset($request->avatar)) {
            $file = UploadFileUtil::UploadImage($request, 'avatar');
            $weblink->img_path = $file->path;
        }
        $weblink->title = $request->title;
        $weblink->url = $request->url;
        $weblink->created_by = Auth::user()->id;
        if ((int)$request->no == 0) {
            $iMaxNo = Weblink::max('no');
            $weblink->no = $iMaxNo + 1;
        } else {
            $weblink->no = $request->no;
        }
        $weblink->group_id = $request->group_id;
        $weblink->active = $request->active == true ? 1 : 0;
        $weblink->new_window = $request->new_window == true ? 1 : 0;
        $weblink->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Weblink  $weblink
     * @return \Illuminate\Http\Response
     */
    public function show(Weblink $weblink)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Weblink  $weblink
     * @return \Illuminate\Http\Response
     */
    public function edit(Weblink $weblink)
    {
        //Check quyền
        $this->weblinkAuthor->canDo();

        return view('manage.weblink.create', ['weblink' => $weblink,'group_id' => Constants::WEBLINK_GROUPS]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Weblink  $weblink
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWeblinkRequest $request, Weblink $weblink)
    {
        //Check quyền
        $this->weblinkAuthor->canDo();
        
        $file = UploadFileUtil::UploadImage($request, 'avatar');
        if (isset($file)) {
            $weblink->img_path = $file->path;
        }
        $weblink->title = $request->title;
        $weblink->url = $request->url;
        $weblink->no = $request->no;
        $weblink->group_id = $request->group_id;
        $weblink->active = $request->active == true ? 1 : 0;
        $weblink->new_window = $request->new_window == true ? 1 : 0;
        
        $weblink->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Weblink  $weblink
     * @return \Illuminate\Http\Response
     */
    public function destroy(Weblink $weblink)
    {
        //Check quyền
        $this->weblinkAuthor->canDo();
        
        $weblink->delete();
    }
}
