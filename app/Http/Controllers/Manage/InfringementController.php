<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\UpdateInfringementRequest;
use App\Http\Controllers\Controller;
use App\Models\Infringement;
use App\Models\Organization;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Yajra\DataTables\Facades\DataTables;
use App\OrgType;
use App\Authors\ManageOrganizationAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class InfringementController extends Controller
{
    protected $orgAuthor;

    public function __construct()
    {
        $this->orgAuthor = new ManageOrganizationAuthor();
    }

    public function checkPermission($infringement)
    {
        $org = null;
        if ($infringement->infringementable_type == Organization::class) {
            $org = $infringement->infringementable;
        } else {
            $org = $infringement->infringementable->organization;
        }

        $this->orgAuthor->canManage($org);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->orgAuthor->canDo();
        
        return view('manage.infringements.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->orgAuthor->canDo();
        
        $model = Infringement::select(['id', 'comment']);
        return DataTables::eloquent($model)->make();
    }

    /*
     * $type: Organization, Product, User
     * $search: text input to search
     */
    public function preGetData($type, $search = "", $status = -1, $infringementId = 0)
    {
        $orgId = getOrgId();
        if ($type == 'Organization') {
            $model = Infringement::join('infringement_categories as b', 'b.id', '=', 'infringements.category_id')
                ->join('organizations as c', 'c.id', '=', 'infringements.infringementable_id')
                ->join('users as d', 'd.id', '=', 'infringements.created_by')
                ->select(['infringements.id', 'infringements.comment', 'infringements.status',
                    DB::raw("date_format(infringements.created_at, '%H:%i %d/%m/%Y') as ngay_dang"),
                    'b.name as cate_name', 'c.name as obj_name', 'd.name as nguoi_dang',
                    'b.type', DB::raw("concat('/shop/', c.path) as path, ifnull(c.status, 3) as active"),
                    DB::raw("case c.status when 0 then 'Chưa duyệt' when 1 then 'Đang hoạt động'
                    when 2 then 'Ngừng hoạt động' else 'Khóa do vi phạm' end as active_txt")])
                ->where('infringements.infringementable_type', '=', Organization::class)
                ->whereNull('b.deleted_at')
                ->whereNull('c.deleted_at')
                ->whereNull('d.deleted_at');

            if (!Gate::allows('*', Organization::class)) {
                $model->where(function ($query) use ($orgId) {
                    $query->where('c.manage_organization_id', '=', -1);

                    if (Gate::any(
                        ['*',
                            AbilityName::MANAGE_ORG_GOV,
                            AbilityName::MANAGE_ORG_GOV_HEALTH,
                            AbilityName::MANAGE_ORG_GOV_AGRICULTURE,
                            AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE],
                        Organization::class
                    )) {
                        $query->orWhere(function ($query) use ($orgId) {
                            $query->where('c.manage_organization_id', '=', $orgId);

                            if (!Gate::allows(AbilityName::MANAGE_ORG_GOV, Organization::class)) {
                                $query->where(function ($query) {
                                    if (Gate::allows(AbilityName::MANAGE_ORG_GOV_HEALTH, Organization::class)) {
                                        $query->orWhere('c.department_manage_id', '=', 1);
                                    }
                                    if (Gate::allows(AbilityName::MANAGE_ORG_GOV_AGRICULTURE, Organization::class)) {
                                        $query->orWhere('c.department_manage_id', '=', 2);
                                    }
                                    if (Gate::allows(
                                        AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE,
                                        Organization::class
                                    )) {
                                        $query->orWhere('c.department_manage_id', '=', 3);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }

        if ($type == 'Product') {
            $model = Infringement::join('infringement_categories as b', 'b.id', '=', 'infringements.category_id')
                ->join('products as c', 'c.id', '=', 'infringements.infringementable_id')
                ->join('users as d', 'd.id', '=', 'infringements.created_by')
                ->join('organizations as e', 'e.id', '=', 'c.organization_id')
                ->select(['infringements.id', 'infringements.comment', 'infringements.status',
                    DB::raw("date_format(infringements.created_at, '%H:%i %d/%m/%Y') as ngay_dang"),
                    'b.name as cate_name', 'c.name as obj_name', 'd.name as nguoi_dang',
                    'b.type', DB::raw("concat('/san-pham/', c.path) as path, ifnull(c.active, 2) as active"),
                    DB::raw("case c.active when 0 then 'Chưa duyệt' when 1 then 'Đã duyệt'
                    else 'Khóa do vi phạm' end as active_txt")])
                ->where('infringements.infringementable_type', '=', Product::class)
                ->whereNull('b.deleted_at')
                ->whereNull('c.deleted_at')
                ->whereNull('e.deleted_at')
                ->whereNull('d.deleted_at');

            if (!Gate::allows('*', Organization::class)) {
                $model->where(function ($query) use ($orgId) {
                    $query->where('e.manage_organization_id', '=', -1);

                    if (Gate::any(
                        ['*',
                            AbilityName::MANAGE_ORG_GOV,
                            AbilityName::MANAGE_ORG_GOV_HEALTH,
                            AbilityName::MANAGE_ORG_GOV_AGRICULTURE,
                            AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE],
                        Organization::class
                    )) {
                        $query->orWhere(function ($query) use ($orgId) {
                            $query->where('e.manage_organization_id', '=', $orgId);

                            if (!Gate::allows(AbilityName::MANAGE_ORG_GOV, Organization::class)) {
                                $query->where(function ($query) {
                                    if (Gate::allows(AbilityName::MANAGE_ORG_GOV_HEALTH, Organization::class)) {
                                        $query->orWhere('e.department_manage_id', '=', 1);
                                    }
                                    if (Gate::allows(AbilityName::MANAGE_ORG_GOV_AGRICULTURE, Organization::class)) {
                                        $query->orWhere('e.department_manage_id', '=', 2);
                                    }
                                    if (Gate::allows(
                                        AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE,
                                        Organization::class
                                    )) {
                                        $query->orWhere('e.department_manage_id', '=', 3);
                                    }
                                });
                            }
                        });
                    }
                });
            }
        }

        if ($type == 'User') {
            $this->orgAuthor->canDo();
            $model = Infringement::join('infringement_categories as b', 'b.id', '=', 'infringements.category_id')
                ->join('users as c', 'c.id', '=', 'infringements.infringementable_id')
                ->join('users as d', 'd.id', '=', 'infringements.created_by')
                ->select(['infringements.id', 'infringements.comment', 'infringements.status',
                    DB::raw("date_format(infringements.created_at, '%H:%i %d/%m/%Y') as ngay_dang"),
                    'b.name as cate_name', 'c.name as obj_name', 'd.name as nguoi_dang',
                    'b.type', DB::raw("concat('/user-activity/', c.email) as path, ifnull(c.active, 0) as active"),
                    DB::raw("case c.active when 0 then 'Khóa do vi phạm' else 'Đang hoạt động' end as active_txt")])
                ->where('infringements.infringementable_type', '=', User::class)
                ->whereNull('b.deleted_at')
                ->whereNull('c.deleted_at')
                ->whereNull('d.deleted_at');
        }

        if (strlen($search) > 0) {
            $model = $model->where('infringements.comment', 'like', '%'.$search.'%');
        }

        if ($status > -1) {
            $model = $model->where('infringements.status', '=', $status);
        }

        if ($infringementId > 0) {
            $model = $model->where('infringements.id', '=', $infringementId);
        }

        return $model;
    }

    /*
     * $type: Organization, Product, User
     */
    public function getData($type)
    {
        //Check quyền
        $this->orgAuthor->canDo();
        
        $model = self::preGetData($type);
        return DataTables::eloquent($model)->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Infringement $infringement
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // Với chức năng phức tạp cần xây dựng action này
        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Infringement $infringement
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateInfringementRequest $request
     * @param  \App\Models\Infringement $infringement
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Infringement $infringement
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
    }

    /*
     * dùng cho cơ quan quản lý
     * $request->status chia làm 3 trường hợp
     * 1. Org: 1 - Đang hoạt động, 2 - Ngưng hoạt động, 3 - Khóa do vi phạm
     * 2. Product: 1 - Mở khóa, 2 - Khóa
     * 3. User: 1 - Mở khóa, 0 - Khóa
     */
    public function perform($id, $status)
    {
        $infringement = Infringement::find($id);
        $this->checkPermission($infringement);

        if ($infringement->status == 0) {
            $infringement->status = 1;
            $infringement->save();
        }

        $type = $infringement->infringementCategory->type;
        $objId = $infringement->infringementable_id;

        // $type == 0: Organization
        if ($type == 0) {
            $organization = Organization::find($objId);
            $organization->status = $status;
            $organization->save();
        }

        // $type == 1: Product
        if ($type == 1) {
            $product = Product::find($objId);
            $product->active = $status;
            $product->save();
        }

        // $type == 2: User
        if ($type == 2) {
            $user = User::find($objId);
            $user->active = $status;
            $user->save();
        }
    }
}
