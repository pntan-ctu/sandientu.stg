<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\Sms;
use App\Models\Organization;
use App\Models\User;
use App\Models\SmsHistory;
use App\Http\Requests\StoreSmsRequest;
use App\Authors\SmsHistoryAuthor;

/**
 * Description of newPHPClass
 *
 * @author Admin
 */
class SmsHistoryController extends Controller
{
    protected $smsHistoryAuthor;

    public function __construct()
    {
        $this->smsHistoryAuthor = new SmsHistoryAuthor();
    }

    public function index()
    {
        //Check quyền
        $this->smsHistoryAuthor->canDoHis();
        
        return view('manage.sms_history.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->smsHistoryAuthor->canDoHis();
        
        $model = SmsHistory::select(['*']);
        $data = DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('fromdate')) {
                $fromdate = $request->input('fromdate');
                $query->where(function ($query) use ($fromdate) {
                    $query->where('created_at', '>=', $fromdate);
                });
            }
            if ($request->filled('todate')) {
                $todate = $request->input('todate');
                $query->where(function ($query) use ($todate) {
                    $query->where('created_at', '<=', $todate);
                });
            }
        })->make();
        
        $mainData = $data->original['data'];

        $iStart = request('start');
        foreach ($mainData as $i => $j) {
            $mainData[$i]['stt'] = $iStart + $i + 1;
        }
        $data = json_decode(json_encode($data), true);
        $data['original']['data'] = $mainData;
        return $data['original'];
    }
    
    
    public function search(Request $request)
    {
        //Check quyền
        $this->smsHistoryAuthor->canDoHis();
        
        $fromdate= $request->fromdate;
        $todate= $request->todate;
        $sms = SmsHistory::where('crated_at', '>=', $fromdate, 'and', 'crated_at', '<=', $todate)
        ->take(1000000)->paginate(25);
        /*$sms_all = SmsHistory::join('users','sms_histories.user_send_id','=','users.id')
        ->select('sms_histories.id','sms_histories.user_send_id','sms_histories.content',
            'sms_histories.tel_receiver','sms_histories.sms_count','sms_histories.created_by',
            'users.name as user_name')->get();*/
        return view('manage.sms_history.index', ['sms'=>$sms,'fromdate'=>$fromdate,'todate'=>$todate]);
    }
    
    public function show()
    {
    }
}
