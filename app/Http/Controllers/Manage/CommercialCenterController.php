<?php

namespace App\Http\Controllers\Manage;

use App\Models\CommercialCenter;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Models\Region;
use Yajra\DataTables\Facades\DataTables;
use App\Business\CommercialCenterBusiness;
use App\Business\RegionBusiness;
use App\Business\AreaBusiness;
use Illuminate\Support\Facades\Validator;
use App\Utils\CommonUtil;
use App\Http\Requests\StoreCommercialCenterRequest;
use App\Http\Requests\UpdateCommercialCenterRequest;
use App\Authors\CommercialCenterAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class CommercialCenterController extends Controller
{
    protected $commercialCenterAuthor;

    public function __construct()
    {
        $this->commercialCenterAuthor = new CommercialCenterAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->commercialCenterAuthor->canDo();

        return view('manage.commercial_center.index');
    }
    
    public function indexData(Request $request)
    {
        //Check quyền
        $this->commercialCenterAuthor->canDo();

        $model = CommercialCenter::select(['commercial_centers.id as id', 'commercial_centers.name as name',
            'commercial_centers.region_id as region_id',
            'commercial_centers.address as address',
            'commercial_centers.introduction as introduction',
            'commercial_centers.created_at as created_at',
            'regions.name as region_name','organizations.name as org_name',
            'regions.full_name as url'])
        ->join('regions', 'commercial_centers.region_id', '=', 'regions.id')
        ->join('organizations', 'commercial_centers.manage_organization_id', '=', 'organizations.id');
        
        if (!Gate::allows('*', CommercialCenter::class)) {
            $model->where(function ($query) {
                $query->where('commercial_centers.manage_organization_id', '=', -1);

                if (Gate::allows(AbilityName::MANAGE_COMMERCIAL_CENTER, CommercialCenter::class)) {
                    $query->orWhere('commercial_centers.manage_organization_id', '=', getOrgId());
                }
            });
        }

        return DataTables::eloquent($model)->filter(
            function ($query) use ($request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('commercial_centers.name', 'like', '%' . $keyword . '%')
                            ->orWhere('commercial_centers.introduction', 'like', '%' . $keyword . '%');
                    });
                }
            }
        )->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->commercialCenterAuthor->canDo();

        $organization_id = auth()->user()->organization_id;
        $region_id = auth()->user()->Organization->region_id;
        $region_obj = Region::find($region_id);
        $path_code = $region_obj->path_primary_key;
        $region_arr = Region::where('path_primary_key', 'like', '%' . $path_code . '%')->get();
        $tree = RegionBusiness::getTree($region_arr, $region_obj->parent_id);
        return view('manage.commercial_center.create', ['none_create'=>1,
            'tree' => $tree, 'commercialCenter' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCommercialCenterRequest $request)
    {
        //Check quyền
        $this->commercialCenterAuthor->canDo();

        $commercialCenter = new CommercialCenter($request->all());
        $commercialCenter->manage_organization_id = auth()->user()->organization_id;
//        $commercialCenter["region_id"] = $request->region_id;
//        $commercialCenter["name"] = $request->name;
//        ;
//        $commercialCenter["address"] = $request->address;
//        $commercialCenter["introduction"] = $request->introduction;
//        $commercialCenter["manage_organization_id"] = auth()->user()->organization_id;
//        $commercialCenter["map_lat"] = $request->map_lat;
//        $commercialCenter["map_long"] = $request->map_long;
        $commercialCenter->save();
        return redirect('manage/commercial-center');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\CommercialCenter  $commercialCenter
     * @return \Illuminate\Http\Response
     */
    public function show(CommercialCenter $commercialCenter)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\CommercialCenter  $commercialCenter
     * @return \Illuminate\Http\Response
     */
    public function edit(CommercialCenter $commercialCenter)
    {
        //Check quyen
        $this->commercialCenterAuthor->canManage($commercialCenter);

        $organization_id = auth()->user()->organization_id;
        $region_id = auth()->user()->Organization->region_id;
        $region_obj = Region::find($region_id);
        $path_code = $region_obj->path_primary_key;
        $region_arr = Region::where('path_primary_key', 'like', '%' . $path_code . '%')->get();
        $tree = RegionBusiness::getTree($region_arr, $region_obj->parent_id);
        return view('manage.commercial_center.create', ['none_create'=>2,
            'tree' => $tree, 'commercialCenter' => $commercialCenter]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\CommercialCenter  $commercialCenter
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCommercialCenterRequest $request, CommercialCenter $commercialCenter)
    {
        //Check quyen
        $this->commercialCenterAuthor->canManage($commercialCenter);

        $commercialCenter->fill($request->all());
//        $commercialCenter["region_id"] = $request->region_id;
//        $commercialCenter["name"] = $request->name;
//        $commercialCenter["address"] = $request->address;
//        ;
//        $commercialCenter["introduction"] = $request->introduction;
//        $commercialCenter["map_lat"] = $request->map_lat;
//
//        $commercialCenter["map_long"] = $request->map_long;
        $commercialCenter->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\CommercialCenter  $commercialCenter
     * @return \Illuminate\Http\Response
     */
    public function destroy(CommercialCenter $commercialCenter)
    {
        //Check quyen
        $this->commercialCenterAuthor->canManage($commercialCenter);

        $commercialCenter->delete();
    }
}
