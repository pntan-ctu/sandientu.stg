<?php

namespace App\Http\Controllers\Manage;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\OrgType;
use App\Authors\ReportAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use Illuminate\Auth\Access\AuthorizationException;

class ReportRankProductController extends Controller
{
    public function index(Request $request)
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        return view('manage.reports.rank_product');
    }

    public function statistic(Request $request)
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        $type = $request->report_type;
        switch ($type) {
            case "orders":
                $this->statisticProductByOrders($request);
                break;
            case "views":
                $this->statisticProductByViews($request);
                break;
            case "ratings":
                $this->statisticProductByRatings($request);
                break;
            case "ranks":
                $this->statisticProductByRanks($request);
                break;
        }
    }

    public function statisticProductByOrders(Request $request)
    {
        $time = "";

        $data = Order::join(
            'order_items as a',
            'a.order_id',
            '=',
            'orders.id'
        )
        ->join('products as b', 'b.id', '=', 'a.product_id')
        ->join('organizations as c', 'c.id', '=', 'b.organization_id')
        ->join('organizations as d', 'd.id', '=', 'c.manage_organization_id');

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $data = $data->where("d.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $data = $data->whereRaw("d.path_primary_key like '$path%'");
            }
        }

        $data = $data->whereNull('b.deleted_at')
        ->whereNull('c.deleted_at')
        ->where('orders.status', '=', 4);
        if ($request->filled('from_date')) {
            $fromDate = parseDate($request->from_date);
            $data = $data->where('orders.order_date', '>=', $fromDate);
            $time = "Từ ngày ".$request->from_date;
        }
        if ($request->filled('to_date')) {
            $toDate = parseDate_end($request->to_date);
            $data = $data->where('orders.order_date', '<=', $toDate);
            $time .= ($request->filled('from_date') ? " đ" : " Đ")."ến ngày ".$request->to_date;
        }
        $data = $data->groupBy('b.id')
            ->select(['b.name', 'c.name as ten_co_so', 'c.address',
                DB::raw('count(orders.id) as so_don_hang, sum(orders.total_money) as tong_tien')])
            ->orderBy("so_don_hang", "desc")->get();

        $fileName = "tk_xep_hang_san_pham_theo_so_don_hang.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);
        $sheet->setCellValue("A4", $time);

        $i = 1;
        $row = 5;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['name']);
            $sheet->setCellValue("C$row", $d['ten_co_so']);
            $sheet->setCellValue("D$row", $d['address']);
            $sheet->setCellValue("E$row", $d['so_don_hang']);
            $sheet->setCellValue("F$row", $d['tong_tien']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A6:F$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }

    public function statisticProductByViews(Request $request)
    {
        $products = Product::join(
            'organizations as b',
            'b.id',
            '=',
            'products.organization_id'
        )
            ->join('organizations as d', 'd.id', '=', 'b.manage_organization_id');

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $products = $products->where("d.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $products = $products->whereRaw("d.path_primary_key like '$path%'");
            }
        }

        $products = $products->whereNull('b.deleted_at')
        ->select(['products.name', 'b.name as ten_co_so', 'b.address', 'products.count_view'])
        ->orderBy("products.count_view", "desc")->get();

        $fileName = "tk_xep_hang_san_pham_theo_so_luot_xem.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);

        $i = 1;
        $row = 5;
        foreach ($products as $pr) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $pr['name']);
            $sheet->setCellValue("C$row", $pr['ten_co_so']);
            $sheet->setCellValue("D$row", $pr['address']);
            $sheet->setCellValue("E$row", $pr['count_view']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A6:E$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }

    public function statisticProductByRatings(Request $request)
    {
        $data = Product::join(
            'organizations as b',
            'b.id',
            '=',
            'products.organization_id'
        )
            ->join('organizations as d', 'd.id', '=', 'b.manage_organization_id');

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $data = $data->where("d.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $data = $data->whereRaw("d.path_primary_key like '$path%'");
            }
        }

        $data = $data->whereNull('b.deleted_at')
        ->select(['products.name', 'b.name as ten_co_so', 'b.address', 'products.count_rate', 'products.avg_rate'])
        ->orderBy("products.count_rate", "desc")->get();

        $fileName = "tk_xep_hang_san_pham_theo_so_luot_danh_gia_binh_luan.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);

        $i = 1;
        $row = 5;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['name']);
            $sheet->setCellValue("C$row", $d['ten_co_so']);
            $sheet->setCellValue("D$row", $d['address']);
            $sheet->setCellValue("E$row", $d['count_rate']);
            $sheet->setCellValue("F$row", $d['avg_rate']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A6:F$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }

    public function statisticProductByRanks(Request $request)
    {
        $data = Product::join(
            'organizations as b',
            'b.id',
            '=',
            'products.organization_id'
        )
            ->join('organizations as d', 'd.id', '=', 'b.manage_organization_id');

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $data = $data->where("d.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $data = $data->whereRaw("d.path_primary_key like '$path%'");
            }
        }

        $data = $data->whereNull('b.deleted_at')
        ->select(['products.name', 'b.name as ten_co_so', 'b.address',
            'products.count_order', 'products.avg_rate', 'products.count_view'])
        ->orderBy("products.rank", "desc")->get();

        $fileName = "tk_xep_hang_san_pham_theo_diem_rank.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);

        $i = 1;
        $row = 5;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['name']);
            $sheet->setCellValue("C$row", $d['ten_co_so']);
            $sheet->setCellValue("D$row", $d['address']);
            $sheet->setCellValue("E$row", $d['count_order']);
            $sheet->setCellValue("F$row", $d['avg_rate']);
            $sheet->setCellValue("G$row", $d['count_view']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A6:G$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }
}
