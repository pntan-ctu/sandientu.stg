<?php

namespace App\Http\Controllers\Manage;

use App\Models\AlbumGroup;
use App\Models\Album;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\StoreAlbumGroupRequest;
use App\Http\Requests\UpdateAlbumGroupRequest;
use App\Authors\AlbumAuthor;

class AlbumGroupsController extends Controller
{
    protected $albumAuthor;

    public function __construct()
    {
        $this->albumAuthor = new AlbumAuthor();
    }

    public function index()
    {
        //Check quyền
        $this->albumAuthor->canDo();

        return view('manage.album_group.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $model = AlbumGroup::select(['*']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            $query->select(
                'album_groups.name as name',
                'album_groups.description as description',
                'album_groups.created_by as created_by',
                'album_groups.no as no',
                'album_groups.active as active',
                'album_groups.id as id',
                'users.name as user_name'
            )
                    ->join('users', 'album_groups.created_by', '=', 'users.id');
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('album_groups.id', 'like', '%' . $keyword . '%')
                                    ->orWhere('album_groups.name', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('album_groups.active', '=', request('active'));
            }
        })->make();
    }

    public function create()
    {
        //Check quyền
        $this->albumAuthor->canDo();

        return view('manage.album_group.create', ['album_group' => null]);
    }
    
    public function store(StoreAlbumGroupRequest $request)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $album_group = new AlbumGroup();
        $album_group->name = $request->name;
        $album_group->description = $request->description;
        $album_group->no = $request->no;
        $album_group->created_by = Auth::user()->id;
        ;
        $album_group->active = $request->active == true ? 1 : 0;
        $album_group->save();
    }

    public function show($id)
    {
    }

    public function edit(AlbumGroup $album_group)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        return view('manage.album_group.create', ['album_group' => $album_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAlbumGroupRequest $request, AlbumGroup $album_group)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $album_group->name = $request->name;
        $album_group->description = $request->description;
        $album_group->no = $request->no;
        $album_group->created_by = $album_group->created_by;
        $album_group->active = $request->active == true ? 1 : 0;
        $album_group->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AlbumGroup $album_group)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $result = Album::where('group_id', $album_group->id)->first();
        if (!$result) {
            $album_group->delete();
        } else {
            return response()->json(['status' => 'warning', 'msg' => 'Chuyên mục có chứa Album ảnh, không được xóa']);
        }
        
        $album_group->delete();
    }
}
