<?php

namespace App\Http\Controllers\Manage;

use App\Models\Poll;
use App\Models\PollAnswer;
use App\Models\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StorePollRequest;
use App\Http\Requests\UpdatePollRequest;
use App\Authors\PollAuthor;

class PollController extends Controller
{
    protected $pollAuthor;

    public function __construct()
    {
        $this->pollAuthor = new PollAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->pollAuthor->canDo();

        return view('manage.poll.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->pollAuthor->canDo();
        
        $poll = Poll::select(['*']);
        return DataTables::eloquent($poll)->filter(function ($query) use ($request) {
            $query->select(
                'polls.name as name',
                'polls.no as no',
                'polls.public as public',
                'polls.votes as votes',
                'users.name as user_name',
                'polls.id as id'
            )
                ->join('users', 'polls.created_by', '=', 'users.id');
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('polls.name', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('polls.public', '=', request('active'));
            }
        })->make();
    }
    public function getPollAnswer($pollId)
    {
        //Check quyền
        $this->pollAuthor->canDo();
        
        $model = PollAnswer::select(['id','name','no','public','votes','created_by']);
        return DataTables::eloquent($model)->filter(function ($query) use ($pollId) {
            $query->where(function ($query) use ($pollId) {
                $query->where('poll_id', '=', $pollId);
            });
        })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->pollAuthor->canDo();

        return view('manage.poll.create', ['poll' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePollRequest $request)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        $poll = new Poll;
        $poll -> name = $request->name;
        $poll -> votes = 0;
        $poll -> no = $request->no;
        $poll -> created_by = Auth::user()->id;
        $poll -> public = $request->public == true ? 1 : 0;
        $poll->save();
        if (isset($request->mytext)) {
            $pollId = Poll::max('id');
            $count=1;
            foreach ($request->mytext as $multiltext) {
                $poll = new PollAnswer();
                $pollno = Poll::find($pollId);
                $poll -> name = $multiltext;
                $poll -> poll_id = $pollId;
                $poll -> votes = 0;
                $poll -> no = $count;
                $poll -> created_by = Auth::user()->id;
                $poll -> public = $request->public == true ? 1 : 0;
                $poll->save();
                $count++;
            }
        }
        
        return redirect('manage/poll');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        $poll = Poll::find($id);
        $poll -> public = !($poll -> public);
        $poll->save();
        return redirect('manage/poll');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function edit(Poll $poll)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        return view('manage.poll.create', ['poll' => $poll]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Poll  $poll
     * @return \Illuminate\Http\Response
     */

    public function update(UpdatePollRequest $request, Poll $poll)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        $poll -> name = $request->name;
        $poll -> votes = $poll -> votes;
        $poll -> no = $request->no;
        $poll -> created_by = $poll -> created_by;
        $poll -> public = $request->public == true ? 1 : 0;
        $poll->save();
        return redirect('manage/poll');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Poll  $poll
     * @return \Illuminate\Http\Response
     */
    public function destroy(Poll $poll)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        $result = PollAnswer::where('poll_id', $poll->id)->first();
        if (!$result) {
            $poll->delete();
        } else {
            $poll->delete();
            $result->delete();
        }
    }
    
    public function search(Request $request)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        $keyword= $request->keyword;
        $poll = Poll::join('users', 'polls.created_by', '=', 'users.id')
                ->select(
                    'polls.id',
                    'polls.poll_id',
                    'polls.name',
                    'polls.no',
                    'polls.public',
                    'polls.votes',
                    'polls.created_by',
                    'users.name as user_name'
                )
                ->where('polls.name', 'like', '%'.$keyword.'%')->take(32)->paginate(15);
        $poll_answer = PollAnswer::join('users', 'poll_answers.created_by', '=', 'users.id')
                ->select(
                    'poll_answers.id',
                    'poll_answers.poll_id',
                    'poll_answers.name',
                    'poll_answers.no',
                    'poll_answers.public',
                    'poll_answers.votes',
                    'poll_answers.created_by',
                    'users.name as user_name'
                )
                ->where('poll_answers.name', 'like', '%'.$keyword.'%')->take(32)->paginate(15);

        return view('manage.poll.index', ['poll'=>$poll,'keyword'=>$keyword
                ,'poll_answer'=>$poll_answer]);
    }
}
