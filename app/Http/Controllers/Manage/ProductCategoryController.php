<?php

namespace App\Http\Controllers\Manage;

use App\Business\DepartmentManageBussiness;
use App\Http\Requests\StoreProductCategoryRequest;
use App\Http\Requests\UpdateProductCategoryRequest;
use App\Models\DepartmentManage;
use App\Models\ProductCategory;
use App\Utils\UploadFileUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Authors\ProductCategoryAuthor;

class ProductCategoryController extends Controller
{
    protected $productCategoryAuthor;

    public function __construct()
    {
        $this->productCategoryAuthor = new ProductCategoryAuthor();
    }

    public function index(Request $request)
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        return view('manage.product_category.index');
    }

    public function create()
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        $arr = ProductCategory::get(['id', 'parent_id', 'name']);
        $tree = $this->getTreeCategory($arr);
        $departments = DepartmentManageBussiness::renderRadio(0);
        return response()->json(['view' => view(
            'manage.product_category.create',
            ['tree' => $tree, 'productCategory' => null, 'departments' => $departments]
        )->render()]);
    }

    public function store(StoreProductCategoryRequest $request)
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        $productCategory = new ProductCategory();
        $productCategory->fill($request->all());
        if ($productCategory->parent_id == 0) {
            $productCategory->parent_id = null;
        }
        $productCategory->name = str_replace('/', '', $productCategory->name);
        if (isset($request->icon)) {
            $file = UploadFileUtil::UploadImage($request, 'icon');
            $productCategory->icon = $file->path;
        }
        $productCategory->save();
        $this->savePath($productCategory);
        return response()->json(['status' => 'success', 'msg' => 'Thêm danh mục thành công']);
    }

    public function update(UpdateProductCategoryRequest $request, ProductCategory $productCategory)
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        $productCategory->fill($request->all());
        if ($productCategory->parent_id == 0) {
            $productCategory->parent_id = null;
        }
        $productCategory->name = str_replace('/', '', $productCategory->name);
        if (isset($request->icon)) {
            $file = UploadFileUtil::UploadImage($request, 'icon');
            $productCategory->icon = $file->path;
        }
        $this->savePath($productCategory, true);
        return response()->json(['status' => 'success', 'msg' => 'Cập nhật danh mục thành công']);
    }

    public function show(ProductCategory $productCategory)
    {
    }

    public function edit(ProductCategory $productCategory)
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        $departments = DepartmentManageBussiness::renderRadio();
        $arr = ProductCategory::where('id', '<>', $productCategory->id)->get(['id', 'parent_id', 'name']);
        $tree = $this->getTreeCategory($arr);
        return response()->json(['view' => view(
            'manage.product_category.create',
            ['tree' => $tree, 'productCategory' => $productCategory, 'departments' => $departments]
        )->render()]);
    }

    public function destroy(ProductCategory $productCategory)
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        $result = ProductCategory::where('parent_id', $productCategory->id)->first();
        if (!$result) {
            $productCategory->delete();
        } else {
            return response()->json(['status' => 'warning', 'msg' => 'Bạn phải xóa hết danh mục con']);
        }
        return response()->json(['status' => 'success', 'msg' => 'Xóa danh mục thành công']);
    }

    public function indexData()
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        $model = ProductCategory::select(['id', 'name', 'description', 'parent_id']);
        return DataTables::eloquent($model)->filter(function ($query) {
            if (request()->filled('keyword')) {
                $keyword = request('keyword');
                $query->where('name', 'like', '%' . $keyword . '%')
                    ->orWhere('description', 'like', '%' . $keyword . '%');
            }
        })->make();
    }

    public function getTreeCategory($objs)
    {
        $arrObj = [];
        foreach ($objs as $item) {
            $arrObj[$item->parent_id][] = $item;
        }
        $tree = $this->insertToParent($arrObj, $arrObj[null]);
        return $tree;
    }

    public function insertToParent(&$arrObj, $parents)
    {
        $tree = [];
        foreach ($parents as $key => $item) {
            if (isset($arrObj[$item->id])) {
                $item->children = $this->insertToParent($arrObj, $arrObj[$item->id]);
                unset($arrObj[$item->id]);
            }
            $tree[] = $item;
        }
        return $tree;
    }

    public function dataGrid()
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        if (isset(request()->keywork)) {
            $arr = ProductCategory::where('name', 'like', '%' . request()->keywork . '%')
                ->get(['id', 'parent_id', 'name', 'description', 'path_primary_key']);
            $ids = "";
            foreach ($arr as $item) {
                if ($ids == "") {
                    $ids = $item->path_primary_key;
                } else {
                    $ids .= "/" . $item->path_primary_key;
                }
            }
            $ids = explode("/", $ids);
            $ids = array_unique($ids);
            $arr = ProductCategory::whereIn('id', $ids)
                ->get(['id', 'parent_id', 'name', 'description', 'path_primary_key']);
        } else {
            $arr = ProductCategory::get(['id', 'parent_id', 'name', 'description']);
        }

        $data = $this->getTreeCategory($arr);
        return json_encode($data);
    }

    public function savePath($productCategory, $isUpdate = false)
    {
        //Check quyền
        $this->productCategoryAuthor->canDo();

        $path_primary_key = $productCategory->path_primary_key;
        $path_name = $productCategory->path_name;
        if (isset($productCategory->parent_id)) {
            $parent = ProductCategory::find($productCategory->parent_id);
            $productCategory->path_primary_key = $parent->path_primary_key . "/" . $productCategory->id;
            $productCategory->path_name = $parent->path_name . "/" . $productCategory->name;
        } else {
            $productCategory->path_primary_key = $productCategory->id;
            $productCategory->path_name = $productCategory->name;
        }
        $productCategory->save();

        if ($isUpdate) {
            $childrens = ProductCategory::where('path_primary_key', 'like', $path_primary_key . '/%')
                ->get(['id', 'parent_id', 'name', 'description', 'path_primary_key', 'path_name']);
            foreach ($childrens as $item) {
                $item->path_primary_key = $productCategory->path_primary_key . "/" .
                    str_replace($path_primary_key . "/", "", $item->path_primary_key);
                $item->path_name = $productCategory->path_name . "/" .
                    str_replace($path_name . "/", "", $item->path_name);
                $item->save();
            }
        }
    }
}
