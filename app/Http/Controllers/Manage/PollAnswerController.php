<?php

namespace App\Http\Controllers\Manage;

use App\Models\PollAnswer;
use App\Models\Poll;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\StorePollRequest;
use App\Http\Requests\UpdatePollRequest;
use App\Authors\PollAuthor;

class PollAnswerController extends Controller
{
    protected $pollAuthor;

    public function __construct()
    {
        $this->pollAuthor = new PollAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        if (isset($request->mytext)) {
            $pollno = PollAnswer::where('poll_id', '=', $request->pollId)->max('no');
            $pollpl = Poll::where('id', '=', $request->pollId)->max('public');
            foreach ($request->mytext as $multiltext) {
                $pollAnswer = new PollAnswer();
                $pollAnswer -> name = $multiltext;
                $pollAnswer -> poll_id = $request->pollId;
                $pollAnswer -> votes = 0;
                $pollAnswer -> no = $pollno+1;
                $pollAnswer -> created_by = Auth::user()->id;
                $pollAnswer -> public = $pollpl;
                $pollAnswer->save();
                $pollno++;
            }
        }
        return redirect('manage/poll');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\PollAnswer  $pollAnswer
     * @return \Illuminate\Http\Response
     */
    public function show(PollAnswer $pollAnswer)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\PollAnswer  $pollAnswer
     * @return \Illuminate\Http\Response
     */
    public function edit(PollAnswer $pollAnswer)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        return view('manage.poll_answer.create', ['pollAnswer' => $pollAnswer]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\PollAnswer  $pollAnswer
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePollRequest $request, PollAnswer $pollAnswer)
    {
        //Check quyền
        $this->pollAuthor->canDo();

        $pollAnswer->name = $request->name;
        $pollAnswer->no = $request->no;
        $pollAnswer->poll_id= $pollAnswer->poll_id;
        $pollAnswer->votes=$pollAnswer->votes;
        $pollAnswer->created_by = $pollAnswer -> created_by;
        $pollAnswer -> public = $request->public == true ? 1 : 0;
        $pollAnswer->save();
        return redirect('manage/poll');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\PollAnswer  $pollAnswer
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Check quyền
        $this->pollAuthor->canDo();
        
        $alb = PollAnswer::find($id);
        $alb->delete();
    }
}
