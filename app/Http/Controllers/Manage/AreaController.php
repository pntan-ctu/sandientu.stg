<?php

namespace App\Http\Controllers\Manage;

use App\Http\Requests\StoreAreaRequest;
use App\Http\Requests\UpdateAreaRequest;
use App\Models\Region;
use App\Utils\UploadFileUtil;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Area;
use App\Business\AreaBusiness;
use App\Business\RegionBusiness;
use App\Authors\AreaAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class AreaController extends Controller
{
    protected $areaAuthor;

    public function __construct()
    {
        $this->areaAuthor = new AreaAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->areaAuthor->canDo();

        return view('manage.area.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->areaAuthor->canDo();
        
        $model = Area::select(['areas.id as id', 'areas.name as name',
            'areas.map as map','areas.region_id as region_id',
            'areas.instroduction as instroduction', 'areas.created_at as created_at',
            'regions.name as region_name','organizations.name as org_name',
            'regions.full_name as url'])
        ->join('regions', 'areas.region_id', '=', 'regions.id')
        ->join('organizations', 'areas.manage_organization_id', '=', 'organizations.id');

        if (!Gate::allows('*', Area::class)) {
            $model->where(function ($query) {
                $query->where('areas.manage_organization_id', '=', -1);

                if (Gate::allows(AbilityName::MANAGE_AREA, Area::class)) {
                    $query->orWhere('areas.manage_organization_id', '=', getOrgId());
                }
            });
        }

        return DataTables::eloquent($model)->filter(
            function ($query) use ($request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('areas.name', 'like', '%' . $keyword . '%')
                            ->orWhere('areas.instroduction', 'like', '%' . $keyword . '%')
                            ->orWhere('organizations.name', 'like', '%' . $keyword . '%');
                    });
                }
                if ($request->filled('active') && $request->input('active') != -1) {
                    $query->where('active', '=', request('active'));
                }
            }
        )->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->areaAuthor->canDo();

        $organization_id = auth()->user()->organization_id;
        $region_id = auth()->user()->Organization->region_id;
        $region_obj = Region::find($region_id);
        $path_code = $region_obj->path_primary_key;
        $region_arr = Region::where('path_primary_key', 'like', '%' . $path_code . '%')->get();
        $tree = RegionBusiness::getTree($region_arr, $region_obj->parent_id);
        return view('manage.area.create', ['none_create'=>1,'tree' => $tree, 'area' => null]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAreaRequest $request)
    {
        //Check quyền
        $this->areaAuthor->canDo();

        $area = new Area();
        if (isset($request->map)) {
            $file = UploadFileUtil::UploadImage($request, 'map');
            $area["map"] = $file->path;
        }
        $area["region_id"] = $request->region_id;
        //$area["path"] = str_slug($request->name, '-');
        $area["name"] = $request->name;
        $area["instroduction"] = $request->instroduction;
        $area["manage_organization_id"] = auth()->user()->organization_id;
        $area->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Area $area)
    {
        //Check quyen
        $this->areaAuthor->canManage($area);

        $organization_id = auth()->user()->organization_id;
        $region_id = auth()->user()->Organization->region_id;
        $region_obj = Region::find($region_id);
        $path_code = $region_obj->path_primary_key;
        $region_arr = Region::where('path_primary_key', 'like', '%' . $path_code . '%')->get();
        $tree = RegionBusiness::getTree($region_arr, $region_obj->parent_id);
        return view('manage.area.create', ['none_create'=>2,'tree' => $tree, 'area' => $area]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAreaRequest $request, Area $area)
    {
        //Check quyen
        $this->areaAuthor->canManage($area);

        //$area["path"] = str_slug($request->name, '-');
        $area["name"] = $request->name;
        $area["instroduction"] = $request->instroduction;
        //$area["manage_organization_id"] = auth()->user()->organization_id;
        $area["region_id"] = $request->region_id;
        if (isset($request->map) && $request->map) {
            $file = UploadFileUtil::UploadImage($request, 'map');
            $area["map"] = $file->path;
        }
        $area->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Area $area)
    {
        //Check quyen
        $this->areaAuthor->canManage($area);

        $area->delete();
    }
}
