<?php

namespace App\Http\Controllers\Manage;

use App\Models\HorMenu;
use App\Models\ProductCategory;
use App\Models\NewsGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Controllers\Manage\ProductCategoryController;
use App\Http\Requests\StoreHorMenuRequest;
use App\Http\Requests\UpdateHorMenuRequest;
use Illuminate\Support\Facades\Gate;
use Illuminate\Auth\Access\AuthorizationException;

class HorMenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $horMenu = HorMenu::select(
            'hor_menus.id',
            'hor_menus.parent_id',
            'hor_menus.title',
            'hor_menus.mtype',
            'hor_menus.relate_id',
            'hor_menus.link',
            'hor_menus.no',
            'hor_menus.active',
            'hor_menus.new_tab'
        )->orderBy('no', 'asc')->get()->toArray();

        $i = 0;
        foreach ($horMenu as $item) {
            if ($item["mtype"]==1) {
                $id = $item["relate_id"];
                $oGroup = NewsGroup::find($id);
                $sUrl = $oGroup["name"];
                $iParentId = $oGroup["parent_id"];
                while ($iParentId > 0) {
                    $oGroup = NewsGroup::find($iParentId);
                    $sUrl =  $oGroup["name"]. " - " .$sUrl ;
                    $iParentId = $oGroup["parent_id"];
                }
            } elseif ($item["mtype"]==2) {
                $id = $item["relate_id"];
                $oGroup = ProductCategory::find($id);
                $sUrl = $oGroup["name"];
                $iParentId = $oGroup["parent_id"];
                while ($iParentId > 0) {
                    $oGroup = ProductCategory::find($iParentId);
                    $sUrl =  $oGroup["name"]. " - " .$sUrl;
                    $iParentId = $oGroup["parent_id"];
                }
            } elseif ($item["mtype"]==3) {
                $sUrl=$item["link"];
            }
            $horMenu[$i]['url'] = $sUrl;
            $i++;
        }

        return view('manage.hor_menu.index', ['horMenu'=>$horMenu]);
    }
    public function create()
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $arr = HorMenu::get(['id','parent_id','title as name']);
        $tree = $this->getTreeMenu($arr);
        
        $arr_product = ProductCategory::get(['id','parent_id','name']);
        $treeproduct = \App\Business\RegionBusiness::getTree($arr_product, null);
        
        $arr_new = NewsGroup::get(['id','parent_id','name']);
        $treenew = \App\Business\RegionBusiness::getTree($arr_new, 0);
        return view(
            'manage.hor_menu.create',
            ['treeproduct' => $treeproduct,
            'tree' => $tree,'horMenu'=>null,
            'productCategory' => null,
            'newsGroup'=>null,'treenew'=>$treenew]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHorMenuRequest $request)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $horMenu = new HorMenu();
        if ($request->id ==0) {
            $find_id = HorMenu::max('id');
            $find_no = HorMenu::max('no');
            $horMenu->id = $find_id +1;
            $horMenu->parent_id = null;
            $horMenu->title = $request->title;
            $horMenu->no = $find_no+1;
        }
        if ($request->id !=0) {
            $find = HorMenu::where('parent_id', '=', $request->id)->count();
            $horMenu->no = $find+1;
            $horMenu->parent_id = $request->id;
            $horMenu->title = $request->title;
        }
        $replace = $this->replace($request, $horMenu);
        $horMenu->save();
        return redirect('manage/hor-menu');
    }
    /**
     * Display the specified resource.
     *
     * @param  \App\Models\HorMenu  $horMenu
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $horMenu = HorMenu::find($id);
        $horMenu -> active = !($horMenu -> active);
        $horMenu->save();
        return redirect('manage/horMenu');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\HorMenu  $horMenu
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        $horMenu = HorMenu::find($id);
        $arr = HorMenu::get(['id','parent_id','title as name']);
        $tree = $this->getTreeMenu($arr);
        
        $arr_product = ProductCategory::get(['id','parent_id','name']);
        $treeproduct = \App\Business\RegionBusiness::getTree($arr_product, null);
        
        $arr_new = NewsGroup::get(['id','parent_id','name']);
        $treenew = \App\Business\RegionBusiness::getTree($arr_new, 0);
        return view(
            'manage.hor_menu.create',
            ['none_create'=>1,
            'treeproduct' => $treeproduct,'tree' => $tree,'horMenu'=>$horMenu,
            'treenew'=>$treenew]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\HorMenu  $horMenu
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHorMenuRequest $request, HorMenu $horMenu)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }

        if ($request->id ==0) {
            $horMenu->parent_id = null;
            $horMenu->title = $request->title;
        }
        if ($request->id !=0) {
            $horMenu->parent_id = $request->id;
            $horMenu->title = $request->title;
        }
        $replace = $this->replace($request, $horMenu);
        $horMenu->no = $request->no;
        $horMenu->save();
        return redirect('manage/hor-menu');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\HorMenu  $horMenu
     * @return \Illuminate\Http\Response
     */
    public function destroy(HorMenu $horMenu)
    {
        //Check quyền
        if (!Gate::any('*', '*')) {
            throw new AuthorizationException();
        }
        
        $result = HorMenu::where('parent_id', $horMenu->id)->first();
        if (!$result) {
            $horMenu->delete();
        } else {
            $horMenu->delete();
            $result->delete();
        }

        return redirect('manage/hor-menu');
    }
    
    public function getTreeMenu($objs)
    {
        $arrObj = [];
        foreach ($objs as $item) {
            $arrObj[$item->parent_id][] = $item;
        }
        $tree = $this->insertToParent($arrObj, $arrObj[null]);
        return $tree;
    }

    public function insertToParent(&$arrObj, $parents)
    {
        $tree = [];
        foreach ($parents as $key => $item) {
            if (isset($arrObj[$item->id])) {
                $item->children = $this->insertToParent($arrObj, $arrObj[$item->id]);
            }
            $tree[] = $item;
        }
        return $tree;
    }
    public function replace($request, $horMenu)
    {
        if ($request->mtype == 3) {
            $horMenu->mtype = $request->mtype ;
            $horMenu->relate_id = 0;
            $horMenu->link = $request->link;
        } elseif ($request->mtype == 2) {
            $path_product = ProductCategory::find($request->product_id);
            $horMenu->mtype = $request->mtype ;
            $horMenu->relate_id = $request->product_id;
            $horMenu->link = $path_product->path;
        } elseif ($request->mtype == 1) {
            $path_new = NewsGroup::find($request->new_id);
            $horMenu->mtype = $request->mtype ;
            $horMenu->relate_id = $request->new_id;
            $horMenu->link = $path_new->path;
        }
        $horMenu->active = $request->active == true ? 1 : 0;
        $horMenu->new_tab = $request->new_tab == true ? 1 : 0;
    }
}
