<?php

namespace App\Http\Controllers\Manage;

use App\Business\AreaBusiness;
use App\Business\CommercialCenterBusiness;
use App\Business\DepartmentManageBussiness;
use App\Business\OrganizationBusiness;
use App\Business\RegionBusiness;
use App\Constants;
use App\Http\Requests\StoreOrganizationRequest;
use App\Http\Requests\UpdateOrganizationRequest;
use App\Models\Organization;
use App\Models\OrganizationLevel;
use App\Models\OrganizationType;
use App\Models\DepartmentManage;
use App\OrgType;
use App\Utils\CommonUtil;
use App\Utils\UploadFileUtil;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;
use App\Authors\ManageOrganizationAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\Area;
use App\Models\CommercialCenter;

class OrganizationController extends Controller
{
    protected $manageOrganizationAuthor;

    public function __construct()
    {
        $this->manageOrganizationAuthor = new ManageOrganizationAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $organizationTypeId)
    {
        //Check quyen
        $this->manageOrganizationAuthor->canDo();

        $orgTypeModel = OrganizationType::find($organizationTypeId);
        if (!isset($orgTypeModel) || $organizationTypeId < 100) {
            return null;
        }
        $treeOrgGov = OrganizationBusiness::getTreeOrgGov();
        return view('manage.organization.index', ['orgTypeModel' => $orgTypeModel, 'TreeOrgGov' => $treeOrgGov]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */ 
    public function create(int $organizationTypeId)
    {
        //Check quyen
        $this->manageOrganizationAuthor->canDo();

        $orgTypeModel = OrganizationType::find($organizationTypeId);
        if (!isset($orgTypeModel) || $organizationTypeId < 100) {
            return null;
        }

        $regions = RegionBusiness::getTreeWithParent(Constants::region);  
        $areas = null;
        $commercialCenters = null;
        switch ($organizationTypeId) {
            case OrgType::CSKD: //101
                $commercialCenters = CommercialCenterBusiness::lists();
                break;
            case OrgType::CSSX: //100
                $areas = AreaBusiness::lists();
                break;
        }
        $departments = DepartmentManageBussiness::renderRadioCheckAthor($this->getDepamentByAuthor(), 0);
        $organizationLevel = OrganizationLevel::get();
        return view(
            'manage.organization.create',
            [
                'organization' => null,
                'orgTypeModel' => $orgTypeModel,
                'regions' => $regions, 'areas' => $areas,
                'commercialCenters' => $commercialCenters,
                'departments' => $departments,
                'organizationLevel' => $organizationLevel,
                'founding_type' => Constants::FOUNDING_TYPE,
                'status' => Constants::ORG_STATUS,
                'areaAll' => Area::all(),
                'commerAll' => CommercialCenter::all()
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(int $organizationTypeId, StoreOrganizationRequest $request)
    {
        //Check quyen
        $this->manageOrganizationAuthor->canDo();

        $organization = new Organization();
        $organization->fill($request->all());
        $validator = CommonUtil::validateMap($request, $organization);
        if ($validator->fails()) {
            return redirect(route('organization.create', [$organizationTypeId]))
                ->withErrors($validator)
                ->withInput();
        }
        if ($request->hasFile('logo_image')) {
            $upload = UploadFileUtil::uploadImage($request, 'logo_image');
            $organization->logo_image = $upload->path;
        }
        $organization->organization_type_id = $organizationTypeId;// se thay bang param
        $organization->manage_organization_id = getOrgId();
        $organization->created_by = getUserId();
        $organization->save();
        return redirect(route('organization.index', ['organizationTypeId' => $organizationTypeId]));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function show(int $organizationTypeId, Organization $organization)
    {
        //Check quyen
        $this->manageOrganizationAuthor->canManage($organization);

        $treeOrgGov = OrganizationBusiness::getTreeOrgGov();
        return view('manage.organization.move', ['Organization' => $organization, 'TreeOrgGov' => $treeOrgGov]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function edit(int $organizationTypeId, Organization $organization)
    {
        //Check quyen
        $this->manageOrganizationAuthor->canManage($organization);

        $orgTypeModel = OrganizationType::find($organizationTypeId);
        if (!isset($orgTypeModel) || $organizationTypeId < 100) {
            return null;
        }


        $regions = RegionBusiness::getTreeWithParent(61);
        $areas = null;
        $commercialCenters = null;
        switch ($organizationTypeId) {
            case OrgType::CSKD: //101
                $commercialCenters = CommercialCenterBusiness::lists();
                break;
            case OrgType::CSSX: //100
                $areas = AreaBusiness::lists();
                break;
        }

        $departments = DepartmentManageBussiness::renderRadioCheckAthor(
            $this->getDepamentByAuthor(),
            $organization->department_manage_id
        );

        if (isset($organization->map_lat)) {
            $organization->map = $organization->map_lat . ", " . $organization->map_long;
        }
        $organizationLevel = OrganizationLevel::get();
        return view(
            'manage.organization.create',
            [
                'organization' => $organization,
                'orgTypeModel' => $orgTypeModel,
                'regions' => $regions, 'areas' => $areas,
                'commercialCenters' => $commercialCenters,
                'departments' => $departments,
                'organizationLevel' => $organizationLevel,
                'founding_type' => Constants::FOUNDING_TYPE,
                'status' => Constants::ORG_STATUS,
                'areaAll' => Area::all(),
                'commerAll' => CommercialCenter::all()
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\Models\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function update(int $organizationTypeId, UpdateOrganizationRequest $request, Organization $organization)
    {
        //Check quyen
        $this->manageOrganizationAuthor->canManage($organization);

        $organization->fill($request->all());
        $validator = CommonUtil::validateMap($request, $organization);
        if ($validator->fails()) {
            return redirect(route('organization.edit', [$organizationTypeId, $organization->id]))
                ->withErrors($validator)
                ->withInput();
        }
        if ($request->hasFile('logo_image')) {
            $upload = UploadFileUtil::uploadImage($request, 'logo_image');
            $organization->logo_image = $upload->path;
        }
        $organization->updated_by = getUserId();

        $organization->save();
        return redirect(route('organization.index', ['organizationTypeId' => $organizationTypeId]));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Organization $organization
     * @return \Illuminate\Http\Response
     */
    public function destroy(int $organizationTypeId, Organization $organization)
    {
        //Check quyen
        $this->manageOrganizationAuthor->canManage($organization);
        $organization->deleted_by = getUserId();
        $organization->save();

        $organization->delete();
        return response()->json(['status' => 'success', 'msg' => 'Xóa cơ sở thành công']);
    }

    public function indexData(int $organizationTypeId)
    {
        //Check quyen
        $this->manageOrganizationAuthor->canDo();

        $orgId = getOrgId();
        $model = Organization::where('organizations.organization_type_id', '=', $organizationTypeId)
                ->join('organizations as gov', 'organizations.manage_organization_id', '=', 'gov.id');

        if (!Gate::allows('*', Organization::class)) {
            $model->where(function ($query) use ($orgId) {
                $query->where('organizations.manage_organization_id', '=', -1);

                if (Gate::any(
                    ['*',
                        AbilityName::MANAGE_ORG_GOV,
                        AbilityName::MANAGE_ORG_GOV_HEALTH,
                        AbilityName::MANAGE_ORG_GOV_AGRICULTURE,
                        AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE],
                    Organization::class
                )) {
                    $query->orWhere(function ($query) use ($orgId) {
                        $query->where('organizations.manage_organization_id', '=', $orgId);
                        $query->orWhere('organizations.move_organization_id', '=', $orgId);
                    });

                    if (!Gate::allows(AbilityName::MANAGE_ORG_GOV, Organization::class)) {
                        $query->where(function ($query) {
                            if (Gate::allows(AbilityName::MANAGE_ORG_GOV_HEALTH, Organization::class)) {
                                $query->orWhere('organizations.department_manage_id', '=', 1);
                            }
                            if (Gate::allows(AbilityName::MANAGE_ORG_GOV_AGRICULTURE, Organization::class)) {
                                $query->orWhere('organizations.department_manage_id', '=', 2);
                            }
                            if (Gate::allows(AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE, Organization::class)) {
                                $query->orWhere('organizations.department_manage_id', '=', 3);
                            }
                        });
                    }
                }
            });
        }

        $model->select(['organizations.id', 'organizations.name', 'organizations.address',
            'organizations.tel', 'organizations.status','gov.name as gov_name',
            DB::raw('if(organizations.move_organization_id != '.$orgId.', 1, 0) as forward'),
            DB::raw('if(organizations.move_organization_id = '.$orgId.', 1, 0) as toward')
        ]);
        
        return DataTables::eloquent($model)->filter(function ($query) {
            if (request()->filled('keyword')) {
                $keyword = request('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('organizations.name', 'like', '%' . $keyword . '%')
                        ->orWhere('organizations.address', 'like', '%' . $keyword . '%')
                        ->orWhere('organizations.tel', 'like', '%' . $keyword . '%')
                        ->orWhere('gov.name ', 'like', '%' . $keyword . '%');
                });
            }
            
            if (request()->filled('status')) {
                $status = request('status');
                if ($status >= 0) {
                    $query->where('organizations.status', $status);
                }
            }
        })->make();
    }

    public function move(Request $request)
    {
        $currentOrgId = $request->currentOrgId;
        $toOrgId = $request->toOrgId;

        if (!($toOrgId > 0)) {
            return false;
        }

        $org = Organization::find($currentOrgId);

        if ($org == null) {
            return false;
        }

        //Check quyen
        $this->manageOrganizationAuthor->canManage($org);

        $org->updated_by = getUserId();
        $org->move_organization_id = $toOrgId;
        $org->save();
    }

    public function cancelMove(Request $request)
    {
        $currentOrgId = $request->orgId;
        $org = Organization::find($currentOrgId);

        if ($org == null) {
            return false;
        }
        
        //Check quyen
        $this->manageOrganizationAuthor->canManage($org);

        $org->updated_by = getUserId();
        $org->move_organization_id = null;
        $org->save();
    }

    public function getOrgInfo(Request $request)
    {
        $currentOrgId = $request->orgId;
        $org = Organization::find($currentOrgId);

        if ($org == null) {
            return false;
        }
        
        //Check quyen
        $this->manageOrganizationAuthor->canManage($org);

        $manageOrg =  Organization::find($org->manage_organization_id);
        return response()->json([
            'ten_cs' => $org->name,
            'dia_chi' => $org->address,
            'ten_co_quan' => $manageOrg->name
        ]);
    }

    public function resolveMove(Request $request)
    {
        $currentOrgId = $request->orgId;
        $choice = $request->choice;

        $org = Organization::find($currentOrgId);

        //Check quyen
        $this->manageOrganizationAuthor->canManage($org);

        if ($org->move_organization_id != getOrgId()) {
            return false;
        }

        $org->updated_by = getUserId();
        if ($choice == 1) {
            $org->move_logs .= ($org->move_logs ? ";" : "")
            . ($org->manage_organization_id . '>' . $org->move_organization_id);
            $org->manage_organization_id = $org->move_organization_id;
        }
        $org->move_organization_id = null;
        $org->save();
    }

    private function getDepamentByAuthor()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_ORG_GOV],
            Organization::class
        )) {
            if (Gate::allows(AbilityName::MANAGE_ORG_GOV_HEALTH, Organization::class)) {
                return DepartmentManage::where('id', '=', 1)->get();
            }

            if (Gate::allows(AbilityName::MANAGE_ORG_GOV_AGRICULTURE, Organization::class)) {
                return DepartmentManage::where('id', '=', 2)->get();
            }

            if (Gate::allows(AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE, Organization::class)) {
                return DepartmentManage::where('id', '=', 3)->get();
            }
        }

        return DepartmentManage::get();
    }
}
