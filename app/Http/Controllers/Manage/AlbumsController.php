<?php

namespace App\Http\Controllers\Manage;

use App\Models\Album;
use App\Models\AlbumGroup;
use App\Models\AlbumDetail;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use App\Utils\ResizeImageUtil;
use App\Http\Requests\StoreAlbumRequest;
use App\Http\Requests\UpdateAlbumRequest;
use App\Authors\AlbumAuthor;

class AlbumsController extends Controller
{
    protected $albumAuthor;

    public function __construct()
    {
        $this->albumAuthor = new AlbumAuthor();
    }

    public function index()
    {
        //Check quyền
        $this->albumAuthor->canDo();

        //$Albums = Album::orderBy('created_at', 'desc')->paginate(10);
        //return view('manage.albums.index', ['Albums' => $Albums]);
        return view('manage.albums.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->albumAuthor->canDo();
        
        $album = Album::select(['*']);
        return DataTables::eloquent($album)->filter(function ($query) use ($request) {
            $query->select(
                'albums.title as title',
                'albums.path as path',
                'albums.description as description',
                'albums.img_path as img_path',
                'albums.created_by as create_by',
                'albums.active as active',
                'albums.id as id',
                'albums.no as no',
                'album_groups.name as group_name',
                'users.name as user_name'
            )
                    ->join('album_groups', 'albums.group_id', '=', 'album_groups.id')
                    ->join('users', 'albums.created_by', '=', 'users.id');
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('group_id', 'like', '%' . $keyword . '%')
                                    ->orwhere('albums.title', 'like', '%' . $keyword . '%')
                                    ->orWhere('album_groups.name', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('albums.active', '=', request('active'));
            }
        })->make();
    }

    public function create()
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $album_group = AlbumGroup::all();
        return view('manage.albums.create', ['album_group' => $album_group,'album'=>null]);
    }

    public function store(StoreAlbumRequest $request)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $album = new Album;
        if (isset($request->avatar)) {
            $file = UploadFileUtil::UploadImage($request, 'avatar');
            $album->img_path = $file->path;
        }
        $album->title = $request->title;
        $album->description = $request->description;
        $album->group_id = $request->albumGroup;
        $album->created_by = Auth::user()->id;
        $album->active = $request->active == true ? 1 : 0;
        $album->no = $request->no;
        $album->home = 0;
        $album ->save();
        return redirect('manage/albums');
    }

    
    public function show($id)
    {
//        $alb = Album::find($id);
//
//        return view('manage.albums.show', ['alb' => $alb]);
    }

    public function edit($id)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $album = Album::find($id);
        $album_group = AlbumGroup::all();
        return view('manage.albums.edit', ['album' => $album, 'album_group' => $album_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAlbumRequest $request, $id)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $album = Album::find($id);
        if (isset($request->avatar)) {
            $file = UploadFileUtil::UploadImage($request, 'avatar');
            $album->img_path = $file->path;
        }
        $album->title = $request->title;
        $album->description = $request->description;
        $album->group_id = $album->group_id;
        $album->created_by = Auth::user()->id;
        $album->active = $request->active == true ? 1 : 0;
        $album->no = $request->no;
        $album->home = 0;
        $album ->save();
        return redirect('manage/albums');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        // return $id;
        $album = Album::find($id);
        print_r($album);
        $result = AlbumDetail::where('album_id', $id)->first();
        if (!$result) {
            $album->delete();
        } else {
            $album->delete();
            $result->delete();
        }
    }

    public function uploadSubmit(Request $request)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $name_album = Album::find($request->albumId);
        $images = UploadFileUtil::MultiUploadImage($request, 'photos');
        if (isset($images)) {
            foreach ($images as $image) {
                AlbumDetail::create([
                    'album_id' => $request->albumId,
                    'title' => $name_album->title,
                    'created_by' => Auth::user()->id,
                    'img_path' => $image->path,
                ]);
            }
        }

        return response()->json(['message' => 'success: Đã thêm thành công!']);
    }

    public function getImagesAlbum($albumId)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $model = AlbumDetail::select(['id', 'img_path',]);
        return DataTables::eloquent($model)->filter(function ($query) use ($albumId) {
            $query->where(function ($query) use ($albumId) {
                $query->where('album_id', '=', $albumId);
            });
        })->make();
    }

    public function destroyAlbumDetail($albumDetailId)
    {
        //Check quyền
        $this->albumAuthor->canDo();

        $alb = AlbumDetail::find($albumDetailId);
        if (!$alb) {
            return response()->json(['error' => 'error: Ảnh không tồn tại!']);
        }
        $alb->delete();
        //return response()->json(['message' => 'success: Đã xóa thành công!']);
    }
}
