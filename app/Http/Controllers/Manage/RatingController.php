<?php

namespace App\Http\Controllers\Manage;

use App\Models\Rating;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Product;
use App\Models\Organization;
use App\OrgType;
use App\Authors\ManageOrganizationAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class RatingController extends Controller
{
    protected $orgAuthor;

    public function __construct()
    {
        $this->orgAuthor = new ManageOrganizationAuthor();
    }

    public function checkPermission($rating)
    {
        $org = null;
        if ($rating->ratingable_type == Organization::class) {
            $org = $rating->ratingable;
        }

        if ($rating->ratingable_type == Product::class) {
            $org = $rating->ratingable->organization;
        }

        $this->orgAuthor->canManage($org);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->orgAuthor->canDo();

        return view('manage.rating.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->orgAuthor->canDo();
        
        $orgId = getOrgId();
        $model = '';
        if (request('type_search') == 'products') {
            $model = Rating::join('products', 'ratings.ratingable_id', '=', 'products.id')
                ->join('organizations', 'products.organization_id', '=', 'organizations.id')
                ->where('ratings.ratingable_type', '=', Product::class)
                ->select(
                    'ratings.comment as comment',
                    'ratings.id as id',
                    'ratings.rate as rate',
                    'ratings.status as status',
                    'users.name as user_name',
                    'organizations.name as org_name',
                    'products.path as pdt_path',
                    'products.name as product_name'
                )
                ->join('users', 'ratings.created_by', '=', 'users.id');
        } elseif (request('type_search') == 'organizations') {
            $model = Rating::join('organizations', 'ratings.ratingable_id', '=', 'organizations.id')
                ->where('ratings.ratingable_type', '=', Organization::class)
                ->select(
                    'ratings.comment as comment',
                    'ratings.id as id',
                    'ratings.rate as rate',
                    'ratings.status as status',
                    'users.name as user_name',
                    'organizations.name as org_name',
                    'organizations.path as org_path'
                )
                ->join('users', 'ratings.created_by', '=', 'users.id');
        }

        $model->whereNull('organizations.deleted_at')->where('organizations.status', '<', 3);

        if (!Gate::allows('*', Organization::class)) {
            $model->where(function ($sqlQuery) use ($orgId) {
                $sqlQuery->where('organizations.manage_organization_id', '=', -1);

                if (Gate::any(
                    ['*',
                        AbilityName::MANAGE_ORG_GOV,
                        AbilityName::MANAGE_ORG_GOV_HEALTH,
                        AbilityName::MANAGE_ORG_GOV_AGRICULTURE,
                        AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE],
                    Organization::class
                )) {
                    $sqlQuery->orWhere(function ($sqlQuery) use ($orgId) {
                        $sqlQuery->where('organizations.manage_organization_id', '=', $orgId);

                        if (!Gate::allows(AbilityName::MANAGE_ORG_GOV, Organization::class)) {
                            $sqlQuery->where(function ($sqlQuery) {
                                if (Gate::allows(AbilityName::MANAGE_ORG_GOV_HEALTH, Organization::class)) {
                                    $sqlQuery->orWhere('organizations.department_manage_id', '=', 1);
                                }
                                if (Gate::allows(AbilityName::MANAGE_ORG_GOV_AGRICULTURE, Organization::class)) {
                                    $sqlQuery->orWhere('organizations.department_manage_id', '=', 2);
                                }
                                if (Gate::allows(AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE, Organization::class)) {
                                    $sqlQuery->orWhere('organizations.department_manage_id', '=', 3);
                                }
                            });
                        }
                    });
                }
            });
        }

        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('ratings.comment', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active_search') && $request->input('active_search') != -1) {
                $query->where('ratings.status', '=', request('active_search'));
            }
        })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function show(Rating $rating)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function edit(Rating $rating)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Rating $rating)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Rating  $rating
     * @return \Illuminate\Http\Response
     */
    public function destroy(Rating $rating)
    {
        $this->checkPermission($rating);
        $rating->delete();
    }

    public function updateChecked(Request $request)
    {
        if (isset($request->id)) {
            $Check = $request->id;
            $Checks = explode(",", $Check);
            foreach ($Checks as $ck) {
                {
                $rating = Rating::find($ck);
                   $rating->status = 1;
                }
                $this->checkPermission($rating);
                $rating->save();
            }
        }

        if (isset($request->id_nosave)) {
            $noCheck = $request->id_nosave;
            $noChecks = explode(",", $noCheck);
            foreach ($noChecks as $nck) {
                {
                $rating = Rating::find($nck);
                   $rating->status = 2;
                }
                $this->checkPermission($rating);
                $rating->save();
            }
        }
        
        return response()->json(['status' => 'success', 'msg' => 'Cập nhật thành công']);
    }
}
