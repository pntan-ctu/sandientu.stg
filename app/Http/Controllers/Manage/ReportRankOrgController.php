<?php

namespace App\Http\Controllers\Manage;

use App\Models\Order;
use App\Models\Organization;
use App\OrgType;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\DB;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use App\Authors\ReportAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use Illuminate\Auth\Access\AuthorizationException;

class ReportRankOrgController extends Controller
{
    public function index()
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        return view('manage.reports.rank_organization');
    }

    public function statistic(Request $request)
    {
        //Check quyền
        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL, AbilityName::MANAGE_REPORT_SCOPE], '*')) {
            throw new AuthorizationException();
        }
        
        $type = $request->report_type;
        switch ($type) {
            case "orders":
                $this->statisticOrganizationByOrders($request);
                break;
            case "views":
                $this->statisticOrganizationByViews($request);
                break;
            case "ratings":
                $this->statisticOrganizationByRatings($request);
                break;
            case "ranks":
                $this->statisticOrganizationByRanks($request);
                break;
        }
    }

    public function statisticOrganizationByOrders(Request $request)
    {
        $orgTypeCSSX = OrgType::CSSX;
        $orgTypeCSKD = OrgType::CSKD;
        $time = "";

        $data = Order::join(
            'organizations as b',
            'b.id',
            '=',
            'orders.provider_organization_id'
        )
        ->join(
            'organizations as c',
            'c.id',
            '=',
            'b.manage_organization_id'
        )
        ->whereRaw("(b.organization_type_id = $orgTypeCSSX or b.organization_type_id = $orgTypeCSKD)");

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $data = $data->where("c.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $data = $data->whereRaw("c.path_primary_key like '$path%'");
            }
        }

        $data = $data->whereNull('b.deleted_at')
        ->where('orders.status', '=', 4);
        if ($request->filled('from_date')) {
            $fromDate = parseDate($request->from_date);
            $data = $data->where('orders.order_date', '>=', $fromDate);
            $time = "Từ ngày ".$request->from_date;
        }
        if ($request->filled('to_date')) {
            $toDate = parseDate_end($request->to_date);
            $data = $data->where('orders.order_date', '<=', $toDate);
            $time .= ($request->filled('from_date') ? " đ" : " Đ")."ến ngày ".$request->to_date;
        }
        $data = $data->groupBy('b.id')
        ->select(['b.name as ten_co_so', 'b.address',
            DB::raw('count(orders.id) as so_don_hang, sum(orders.total_money) as tong_tien')])
        ->orderBy("so_don_hang", "desc")->get();

        $fileName = "tk_xep_hang_co_so_theo_so_don_hang.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);
        $sheet->setCellValue("A4", $time);

        $i = 1;
        $row = 5;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['ten_co_so']);
            $sheet->setCellValue("C$row", $d['address']);
            $sheet->setCellValue("D$row", $d['so_don_hang']);
            $sheet->setCellValue("E$row", $d['tong_tien']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A6:E$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }

    public function statisticOrganizationByViews(Request $request)
    {
        $orgTypeCSSX = OrgType::CSSX;
        $orgTypeCSKD = OrgType::CSKD;

        $orgs = Organization::join(
            'organizations as c',
            'c.id',
            '=',
            'organizations.manage_organization_id'
        )
            ->whereRaw("(organizations.organization_type_id = $orgTypeCSKD
            or organizations.organization_type_id = $orgTypeCSSX)");

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $orgs = $orgs->where("c.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $orgs = $orgs->whereRaw("c.path_primary_key like '$path%'");
            }
        }

        $orgs = $orgs->select(['organizations.name', 'organizations.address', 'organizations.count_view'])
            ->orderBy("organizations.count_view", "desc")->get();

        $fileName = "tk_xep_hang_co_so_theo_so_luot_xem.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);

        $i = 1;
        $row = 5;
        foreach ($orgs as $or) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $or['name']);
            $sheet->setCellValue("C$row", $or['address']);
            $sheet->setCellValue("D$row", $or['count_view']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A6:D$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }

    public function statisticOrganizationByRatings(Request $request)
    {
        $orgTypeCSSX = OrgType::CSSX;
        $orgTypeCSKD = OrgType::CSKD;

        $data = Organization::join(
            'organizations as c',
            'c.id',
            '=',
            'organizations.manage_organization_id'
        )
            ->whereRaw("(organizations.organization_type_id=$orgTypeCSSX or
                organizations.organization_type_id=$orgTypeCSKD)");

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $data = $data->where("c.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $data = $data->whereRaw("c.path_primary_key like '$path%'");
            }
        }

        $data = $data->select(['organizations.name', 'organizations.address',
                'organizations.count_rate', 'organizations.avg_rate'])
            ->orderBy("organizations.count_rate", "desc")->get();

        $fileName = "tk_xep_hang_co_so_theo_so_luot_danh_gia_binh_luan.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);

        $i = 1;
        $row = 5;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['name']);
            $sheet->setCellValue("C$row", $d['address']);
            $sheet->setCellValue("D$row", $d['count_rate']);
            $sheet->setCellValue("E$row", $d['avg_rate']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A6:E$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }

    public function statisticOrganizationByRanks(Request $request)
    {
        $orgTypeCSSX = OrgType::CSSX;
        $orgTypeCSKD = OrgType::CSKD;

        $data = Organization::join(
            'organizations as b',
            'b.id',
            '=',
            'organizations.manage_organization_id'
        )
            ->whereRaw("(organizations.organization_type_id=$orgTypeCSSX
            or organizations.organization_type_id=$orgTypeCSKD)");

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $data = $data->where("b.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $data = $data->whereRaw("b.path_primary_key like '$path%'");
            }
        }

        $data = $data->select(['organizations.name', 'organizations.address', 'organizations.count_order',
                'organizations.avg_rate', 'organizations.count_view'])
            ->orderBy("organizations.rank", "desc")->get();

        $fileName = "tk_xep_hang_co_so_theo_diem_rank.xlsx";
        $objPHPExcel = IOFactory::load(
            resource_path("views/manage/reports/templates/$fileName")
        );
        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);

        $i = 1;
        $row = 5;
        foreach ($data as $d) {
            $row++;
            $sheet->setCellValue("A$row", $i);
            $sheet->setCellValue("B$row", $d['name']);
            $sheet->setCellValue("C$row", $d['address']);
            $sheet->setCellValue("D$row", $d['count_order']);
            $sheet->setCellValue("E$row", $d['avg_rate']);
            $sheet->setCellValue("F$row", $d['count_view']);
            $i++;
        }

        $style = [
            "borders" => [
                "allBorders" => [ "borderStyle"=>Border::BORDER_THIN ]
            ]
        ];

        $sheet->getStyle("A6:F$row")->applyFromArray($style);

        $fileType="Html";
        $docType = $request->doc_type;
        if ($docType=="excel") {
            $fileType="Xlsx";
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header('Content-Disposition: attachment;filename='.$fileName);
            header('Cache-Control: max-age=0');
        }

        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
        exit; // không có lệnh này thì sẽ lỗi khi xuất excel 2007
    }
}
