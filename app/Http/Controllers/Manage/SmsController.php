<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Http\Controllers\Manage;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreSmsRequest;
use App\Models\Organization;
use App\Models\SmsHistory;
use App\Models\User;
use App\Utils\Sms;
use App\Authors\SmsHistoryAuthor;

/**
 * Description of newPHPClass
 *
 * @author Admin
 */
class SmsController extends Controller
{
    protected $smsHistoryAuthor;

    public function __construct()
    {
        $this->smsHistoryAuthor = new SmsHistoryAuthor();
    }

    public function index()
    {
        //Check quyền
        $this->smsHistoryAuthor->canDo();

        $o_id = auth()->user()->organization_id;
        $cate1 = Organization::find($o_id);
        $Categorys = Organization::where('manage_organization_id', '=', $o_id)->orderBy('organization_type_id')->get();
        $tree = 'id: 0, text: "' . $cate1->name . '", expanded: true, spriteCssClass: "rootfolder", items: [';
        
        //---------------------------
        //if(count($Categorys)){
        foreach ($Categorys as $Category) {
            $tree .= '{';
            $tree .= 'id: 0, text: "' . $Category->name . '", expanded: false, spriteCssClass: "folder", items: [';

            $childs = $Category->childs()->orderBy('organization_type_id')->get();
            if (count($childs)) {
                $tree .= $this->childView1($Category);
            }

            //Get users
            $users = User::where('organization_id', '=', $Category->id, 'and', 'active', '=', 1)->get();
            foreach ($users as $u1) {
                $tel = '0';
                if (isset($u1->tel)) {
                    $tel = $u1->tel;
                }
                $tree .= '{ id: ' . $tel . ', text: "' . $u1->name . '-' . $tel . '", spriteCssClass: "html" },';
            }
            $tree .= ']},';
//                    else {
//                        $tree .= ']},';
//
//                    }
        }

        $users = User::where('organization_id', $o_id)->where('active', 1)->get();
        foreach ($users as $u1) {
            $tel = '0';
            if (isset($u1->tel)) {
                $tel = $u1->tel;
            }
            $tree .= '{ id: ' . $tel . ', text: "' . $u1->name . '-' . $tel . '", spriteCssClass: "html" },';
        }

        //}
        //$tree .= ']}';
        $tree .= ']';

        return view('manage.sms.index', compact('tree'));
    }

    public function store(StoreSmsRequest $request)
    {
        //Check quyền
        $this->smsHistoryAuthor->canDo();

        $user_send_id = auth()->user()->id;
        $content = $request->content;
        $content = trim($content);
        $content = Sms::chuanHoa($content);
        $tel = $request->tel;
        $sNumberList = explode(',', $tel);
        //print_r($sNumberList);exit();
        foreach ($sNumberList as $number) {
            //Bo ky tu la, khoang cach cua SDT
            $number = str_replace("&nbsp;", "", $number);
            $number = str_replace(" ", "", $number);
            $number = str_replace("/'", "", $number);
            $number = str_replace("'", "", $number);
            $number = str_replace(".", "", $number);
            $number = Sms::getPhone($number);
            $request_id = rand(1, 1000000000);
            $aDataToSend = array(
                "requestId" => $request_id,
                "params" => array($content),
                "mobilelist" => array($number)
            );
            $result = Sms::sendByVinaWS($aDataToSend);
            //insert to DB
            $sms = new SmsHistory();
            $sms->user_send_id = $user_send_id;
            $sms->content = $content;
            $sms->tel_receiver = $number;
            $sms->tel_type = Sms::getMang($number);
            $sms->sms_count = Sms::getMessLength($content);
            $sms->month = date('m');
            $sms->year = date('Y');
            $sms->save();
        }
        //return redirect('manage/sms');
        echo json_encode(array("success" => true, "msg" => "success"));
    }

    public function getOrganization()
    {
        //Check quyền
        $this->smsHistoryAuthor->canDo();
        
        $o_id = auth()->user()->organization_id;
//            $categories = Organization::where('id', '=', $o_id)->get();
//            $allCategories = Organization::where('manage_organization_id','=',$o_id)->get();
//            return view('manage.sms.index',['categories'=>$categories,'allCategories'=>$allCategories]);
        $cate1 = Organization::find($o_id);
        $Categorys = Organization::where('manage_organization_id', '=', $o_id)->orderBy('organization_type_id')->get();
        $tree = '<ul id="treeview-checkbox-demo" class="filetree"><li class="tree-view closed">' . $cate1>name;

        $users = User::where('organization_id', $o_id)->where('active', 1)->get();
        $tree .= '<ul class="tree-name">';
        foreach ($users as $u1) {
            $tree .= '<li class="tree-view closed"><a class="tree-name">' . $u1->name . '</a></li>';
        }
        $tree .= '</ul>';

        $tree .= '<ul class="tree-name">';
        foreach ($Categorys as $Category) {
            $tree .= '<li class="tree-view closed"><a class="tree-name">' . $Category->name . '</a>';
            //Get users
            $users = User::where('organization_id', '=', $Category->id, 'and', 'active', '=', 1)->get();
            $tree .= '<ul class="tree-name">';
            foreach ($users as $u1) {
                $tree .= '<li class="tree-view closed"><a class="tree-name">' . $u1->name . '</a></li>';
            }
            $tree .= '</ul>';
            //end get users
            //$tree .='<ul class="tree-name">';

            if (count($Category->childs)) {
                $tree .= $this->childView($Category);
            }
        }
        $tree .= '</ul>';
        $tree .= '</li>';
        $tree .= '</ul>';
        // return $tree;
        return view('manage.sms.index', compact('tree'));
    }

    public function childView($Category)
    {
        $childs = $Category->childs()->orderBy('organization_type_id')->get();
        
        if (!count($childs)) {
            return '';
        }

        $html = '<ul>';
        foreach ($childs as $arr) {
            $o_id = $arr->id;
            $users = User::where('organization_id', '=', $o_id, 'and', 'active', '=', 1)->get();

            if (count($arr->childs) || count($users)) {
                $html .= '<li class="tree-view closed"><a class="tree-name">' . $arr->name . '</a>';
                $html .= $this->childView($arr);
                $html .= '<ul class="tree-name">';
                foreach ($users as $u1) {
                    $html .= '<li class="tree-view closed"><a class="tree-name">' . $u1->name . '</a></li>';
                }
                $html .= '</ul>';
            } else {
                $html .= '<li class="tree-view"><a class="tree-name">' . $arr->name . '</a>';
                $html .= "</li>";
            }
        }

        $html .= "</ul>";
        return $html;
    }

    public function getOrganization1()
    {
    }

    public function childView1($Category)
    {
        $childs = $Category->childs()->orderBy('organization_type_id')->get();
        ;
        if (!count($childs)) {
            return '';
        }

        $html = '';
        foreach ($childs as $arr) {
                $o_id = $arr->id;
                $users = User::where('organization_id', '=', $o_id, 'and', 'active', '=', 1)->get();

            if (count($arr->childs) || count($users)) {
                $html .= '{';
                $html .= 'id: 0, text: "' . $arr->name . '", expanded: false, spriteCssClass: "folder", items: [';

                $html .= $this->childView1($arr);

                foreach ($users as $u1) {
                    $tel = '0';
                    if (isset($u1->tel)) {
                        $tel = $u1->tel;
                    }
                    $html .= '{ id: ' . $tel . ', text: "' . $u1->name . '-' . $tel . '", spriteCssClass: "html" },';
                }
                $html .= ']},';
                //$html .= ']},';
            } else {
                $html .= '{ id: 0, text: "' . $arr->name . '", spriteCssClass: "html" },';
            }
        }

        return $html;
    }

    public function show()
    {
    }
}
