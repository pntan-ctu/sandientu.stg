<?php

namespace App\Http\Controllers\Manage;

use App\Models\Video;
use App\Models\VideoGroup;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Utils\UploadFileUtil;
use App\Utils\ResizeImageUtil;
use Image;
use App\Http\Requests\StoreVideoRequest;
use App\Http\Requests\UpdateVideoRequest;
use App\Authors\VideoAuthor;

class VideoController extends Controller
{
    protected $videoAuthor;

    public function __construct()
    {
        $this->videoAuthor = new VideoAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->videoAuthor->canDo();

        return view('manage.video.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->videoAuthor->canDo();
        
        $video = Video::select(['*']);
        return DataTables::eloquent($video)->filter(function ($query) use ($request) {
            $query->select(
                'videos.title as title',
                'videos.path as path',
                'videos.description as description',
                'videos.img_path as img_path',
                'videos.width as width',
                'videos.height as height',
                'videos.youtube_url as youtube_url',
                'videos.created_by as create_by',
                'videos.active as active',
                'videos.id as id',
                'video_groups.name as group_name',
                'users.name as user_name'
            )
                    ->join('video_groups', 'videos.group_id', '=', 'video_groups.id')
                    ->join('users', 'videos.created_by', '=', 'users.id');
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('group_id', 'like', '%' . $keyword . '%')
                                    ->orwhere('videos.title', 'like', '%' . $keyword . '%')
                                    ->orWhere('video_groups.name', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('videos.active', '=', request('active'));
            }
        })->make();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $video_group = VideoGroup::all();
        return view('manage.video.create', ['video' => null,'video_group'=>$video_group]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreVideoRequest $request)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $video = new Video();
        if (isset($request->avatar)) {
            $file = UploadFileUtil::UploadImage($request, 'avatar');
            $video->img_path = $file->path;
        }
        $video->group_id = 1;
        $video->title = $request->title;
        $video->description = $request->description;
        $video->width = $request->width;
        $video->height = $request->height;
        $video->youtube_url = $request->youtube_url;
        $video->created_by = Auth::user()->id;
        $video->active = $request->active == true ? 1 : 0;
        $video->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $video = Video::find($id);
        $video -> active = !($video -> active);
        $video->save();
        return redirect('manage/video');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function edit(Video $video)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $video_group = VideoGroup::all();
        return view('manage.video.create', ['none_create'=>1,'video' => $video,
            'video_group'=>$video_group]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateVideoRequest $request, Video $video)
    {
        //Check quyền
        $this->videoAuthor->canDo();
        $file = UploadFileUtil::UploadImage($request, 'avatar');
        if (isset($file)) {
            if($file != null) 
            $video->img_path = $file->path;
        }
        $video->group_id = $request->group_id;
        $video->title = $request->title;
        $video->description = $request->description;
        $video->width = $request->width;
        $video->height = $request->height;
        $video->youtube_url = $request->youtube_url;
        $video->created_by = $video->created_by;
        $video->active = $request->active == true ? 1 : 0;
        $video->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Video  $video
     * @return \Illuminate\Http\Response
     */
    public function destroy(Video $video)
    {
        //Check quyền
        $this->videoAuthor->canDo();

        $video->delete();
    }
}
