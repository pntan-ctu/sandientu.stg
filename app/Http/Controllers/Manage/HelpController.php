<?php

namespace App\Http\Controllers\Manage;

use App\Models\Help;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\DataTables\Facades\DataTables;
use App\Http\Requests\StoreHelpRequest;
use App\Http\Requests\UpdateHelpRequest;
use App\Authors\HelpAuthor;

class HelpController extends Controller
{
    protected $helpAuthor;

    public function __construct()
    {
        $this->helpAuthor = new HelpAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->helpAuthor->canDo();

        $help = null; //Help::all();
        return view('manage.help.index', ['help'=>$help]);
    }
    public function indexData(Request $request)
    {
        //Check quyền
        $this->helpAuthor->canDo();

        $model = Help::select(['*']);
        return DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('question', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('active') && $request->input('active') != -1) {
                $query->where('status', '=', request('active'));
            }
        })->make();
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Check quyền
        $this->helpAuthor->canDo();

        return view('manage.help.create', ['help' => null,'none_create'=>1]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreHelpRequest $request)
    {
        //Check quyền
        $this->helpAuthor->canDo();

        $help = new Help();
        $help->question = $request->question;
        $help->answers = $request->answers;
        $help->common = isset($request->common_status) ? $request->common : 0;
        $help->name = 'Ban Quản Trị';
        $help->email = $request->email;
        $help->phone = $request->phone;
        $help->address = $request->address;
        $help->status = 0;
        $help->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //Check quyền
        $this->helpAuthor->canDo();

        $help = Help::find($id);
        $help->status = !$help->status;
        $help->save();
        return redirect('manage/help');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Help $help)
    {
        //Check quyền
        $this->helpAuthor->canDo();

        return view('manage.help.create', ['help' => $help,'none_create'=>2]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateHelpRequest $request, Help $help)
    {
        //Check quyền
        $this->helpAuthor->canDo();

        //return $request->common_status;
        $help->question = $request->question;
        $help->email = $request->email;
        $help->phone = $request->phone;
        $help->address = $request->address;
        $help->answers = $request->answers;
        //$help->status = $request->status == true ? 1 : 0;
        $help->common = isset($request->common_status) ? $request->common : 0;
        $help->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Help $help)
    {
        //Check quyền
        $this->helpAuthor->canDo();

        $help->delete();
    }
}
