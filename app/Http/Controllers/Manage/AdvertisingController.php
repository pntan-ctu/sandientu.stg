<?php

namespace App\Http\Controllers\Manage;

use App\Business\AdvertisingCategoryBussiness;
use App\Http\Controllers\Controller;
use App\Models\Advertising;
use App\Models\AdvertisingCategory;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\UpdateAdvertisingRequest;
use App\OrgType;
use App\Authors\AdvertisingAuthor;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class AdvertisingController extends Controller
{
    protected $advertisingAuthor;

    public function __construct()
    {
        $this->advertisingAuthor = new AdvertisingAuthor();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //Check quyền
        $this->advertisingAuthor->canDo();

        return view('manage.advertisings.index');
    }

    public function indexData(Request $request)
    {
        //Check quyền
        $this->advertisingAuthor->canDo();
        
        $model = Advertising::join('advertising_categories as b', 'b.id', '=', 'advertisings.category_id')
            ->join('product_categories as c', 'c.id', '=', 'advertisings.product_category_id')
            ->join('regions as r', 'r.id', '=', 'advertisings.region_id')
            ->select(['b.name as loai_quang_cao', 'c.name as loai_san_pham', 'advertisings.id',
                'advertisings.advertisingable_type', 'advertisings.advertisingable_id', 'advertisings.category_id',
                'advertisings.product_category_id', 'advertisings.title', 'advertisings.content',
                'advertisings.address', 'advertisings.tel', 'advertisings.fanpage',
                DB::raw("date_format(advertisings.from_date, '%d/%m/%Y') as ngay_bd"),
                DB::raw("date_format(advertisings.to_date, '%d/%m/%Y') as ngay_kt"),
                'advertisings.status', 'advertisings.created_at', 'advertisings.status']);

        if (!Gate::allows('*', Advertising::class)) {
            $model->where(function ($query) {
                $query->where('advertisings.region_id', '=', -1);

                if (Gate::allows(AbilityName::MANAGE_CENSORSHIP_ADVERTISING, Advertising::class)) {
                    //$query->orWhere('advertisings.region_id', '=', getRegionId());
                    $query->orWhere('r.path_primary_key', 'like', getOrgModel()->region->path_primary_key . '%');
                }
            });
        }

        $data = DataTables::eloquent($model)->filter(function ($query) use ($request) {
            if ($request->filled('keyword')) {
                $keyword = $request->input('keyword');
                $query->where(function ($query) use ($keyword) {
                    $query->where('advertisings.title', 'like', '%' . $keyword . '%')
                        ->orWhere('advertisings.content', 'like', '%' . $keyword . '%');
                });
            }
            if ($request->filled('status') && $request->input('status') != -1) {
                $query->where('advertisings.status', '=', request('status'));
            }
        })->make();

        $mainData = $data->original['data'];
        foreach ($mainData as $i => $j) {
            //$mainData[$i]['nguoi_dang'] = Advertising::find($mainData[$i]['id'])->advertisingable->name;
            $nguoi_dang = Advertising::find($mainData[$i]['id'])->advertisingable;
            $mainData[$i]['nguoi_dang'] = isset($nguoi_dang->id) ? $nguoi_dang->name : "";
        }
        $data = json_decode(json_encode($data), true);
        $data['original']['data'] = $mainData;
        return $data['original'];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store()
    {
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Advertising $advertising
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        // Với chức năng phức tạp cần xây dựng action này
        // Trên màn hình view cung cấp khả năng edit riêng từng trường dữ liệu (inline edit)
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Advertising $advertising
     * @return \Illuminate\Http\Response
     */
    public function edit(Advertising $advertising)
    {
        //Check quyen
        $this->advertisingAuthor->canManage($advertising);

        $arr = ProductCategory::get(['id', 'parent_id', 'name']);
        $tree = $this->getTreeCategory($arr);
        //$categories = AdvertisingCategory::select(['id', 'name'])->orderBy('name')->get();
        $categories = AdvertisingCategoryBussiness::renderRadio();
        $productCategory = ProductCategory::find($advertising->product_category_id);
        return view(
            'manage.advertisings.create',
            ['adv' => $advertising, 'categories' => $categories, 'productCategory' => $productCategory, 'tree' => $tree]
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  UpdateAdvertisingRequest $request
     * @param  \App\Models\Advertising $advertising
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAdvertisingRequest $request, Advertising $advertising)
    {
        //Check quyen
        $this->advertisingAuthor->canManage($advertising);

        $advertising->fill($request->all());
        $advertising->product_category_id = $request->parent_id;
        $advertising->updated_by = getUserId();
        $advertising->save();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Advertising $advertising
     * @return \Illuminate\Http\Response
     */
    public function destroy(Advertising $advertising)
    {
        //Check quyen
        $this->advertisingAuthor->canManage($advertising);

        $advertising->delete();
    }

    public function setActive($advertisingId)
    {
        $advertising = Advertising::find($advertisingId);

        //Check quyen
        $this->advertisingAuthor->canManage($advertising);

        $advertising->status = 1;
        $advertising->save();
        echo "success";
    }

    public function setInactive($advertisingId)
    {
        $advertising = Advertising::find($advertisingId);

        //Check quyen
        $this->advertisingAuthor->canManage($advertising);

        $advertising->status = 2;
        $advertising->save();
        echo "success";
    }

    public function getTreeCategory($objs)
    {
        $arrObj = [];
        foreach ($objs as $item) {
            $arrObj[$item->parent_id][] = $item;
        }
        $tree = $this->insertToParent($arrObj, $arrObj[null]);
        return $tree;
    }

    public function insertToParent(&$arrObj, $parents)
    {
        $tree = [];
        foreach ($parents as $key => $item) {
            if (isset($arrObj[$item->id])) {
                $item->children = $this->insertToParent($arrObj, $arrObj[$item->id]);
                unset($arrObj[$item->id]);
            }
            $tree[] = $item;
        }
        return $tree;
    }
}
