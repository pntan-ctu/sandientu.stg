<?php

namespace App\Http\Controllers;

use App\Constants;
use App\Models\Order;
use App\Models\Payment;
use Illuminate\Http\Request;
//use Illuminate\Http\Response;
use App\Utils\UploadFileUtil;
use App\Utils\ThumbnailImageUtil;
use App\OrgType;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cache;
use App\Models\Organization;
use App\Models\User;
use App\Models\Message;
use Illuminate\Support\Facades\Gate;
use Silber\Bouncer\BouncerFacade as Bouncer;

class TestController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function index()
    {
        return view('test');
    }

    public function test()
    {
        //Phải đăng nhập
        return Auth::user();

        //Bouncer::scope()->to(6);
        \Silber\Bouncer\BouncerFacade::scope()->to(2);
        return Bouncer::role()->get();
        
        return Gate::allows(\App\Authors\AbilityName::MANAGE_ORG_GOV, Organization::class) ? 'OK' : 'NOT OK';

        exit;

        Bouncer::scope()->to(2);
        //Bouncer::scope()->to(null);
        //return Bouncer::ability()->get();
        return Bouncer::allows('*') ? 'OK' : 'NOT OK';
        return Bouncer::ability()->get();
        //Gate::allows();
        exit;
        $org = Organization::find(38);
        $user = User::find(2);
        echo getRoleAccess();
        echo $user->getUrlActivity();
        return User::findForUrlActivity('longdv.tha@vnpt.vn');
        echo '<br>10-' . Organization::find(10)->visits()->count();
        echo '<br>11-' . Organization::find(11)->visits()->count();
        echo '<br>38-' . Organization::find(38)->visits()->count();
        echo Organization::visitTypes()->fresh()->top(2); //$org->visits()->count();

        //Cache::put('xxxxx', $org, 1);
        //Cache::get('xxxxx');

        //$org->addMesage('subject111', 'content111', $user);
        //return $user->sendMesage('subject222', 'content222', $org);
        
        //return $user->sentMesages;
        /*$messge = new Message();
        $messge->subject = 'Xin chào';
        $messge->content = 'Nọi dung';
        //$messge->sendable_type = User::class;
        //$messge->sendable_id = $user->id;
        //$messge->sendable() = $user;
        $messge->sendable()->associate($user);

        $org->messages()->save($messge);*/
        exit;

        /*$ord = new \App\Models\Order();
        $ord->fill([
                    "provider_organization_id" =>  10,
                    "order_date" => now(),
                    "customer_name" => 'Khách hàng cá nhân',
                    "address" => "Thanh Hóa",
                    "region_id" => 26,
                    "total_money" => 1,
                    "status" => 1,
                    ]) ;
        $user = \App\Models\User::find(getUserId());
        $user->orders()->save($ord);

        $org = \App\Models\Organization::find(getOrgId());
        $org->orders()->create([
                    "provider_organization_id" =>  10,
                    "order_date" => now(),
                    "customer_name" => 'Khách hàng doanh nghiệp',
                    "address" => "Thanh Hóa",
                    "region_id" => 26,
                    "total_money" => 1,
                    "status" => 1,
                    ]);
        $orders = \App\Models\User::find(2)->orders;
        foreach ($orders as $key => $value) {
            echo $value->orderable;
        }*/
        echo getOrgTypeId();
        exit;
        //echo
        //dd();
        //echo \App\Models\User::class;
        echo "</br>===========</br>";
        //App::getUserId();
        echo auth()->user()->id;
        echo "</br>";
        echo auth()->user()->organization_id;
        echo "</br>";
        echo auth()->user()->Organization->organization_type_id;
        echo "</br>===================</br>";
        echo getOrgTypeId() . "===" . OrgType::CSSX;
    }

    public function one(Request $request)
    {
        $file = UploadFileUtil::UploadImage($request, 'photo');
        if ($file==null) {
            echo "Is null";
        } else {
            print_r($file);
        }
    }

    public function multi(Request $request)
    {
        $arrFile = UploadFileUtil::MultiUploadImage($request, 'photos', 10, 10);
        print_r($arrFile);
    }

    public function download(Request $request)
    {
        //VD: Lấy file bởi Id file metadata
        //$file = UploadFileUtil::GetFileMetadataById($request->file_id);

        //VD: Lấy file bởi tên lưu trữ
        //$file = UploadFileUtil::GetFileMetadata($request->path);

        //Từ đây có thể lấy: $file->url (trả về đường dẫn đầy đủ)

        //VD: Download file bởi tên lưu trữ
        //$url = UploadFileUtil::GetFullPath($request->path);
        //return response()->download($url); //$url hoặc $file->full_path lấy từ trên

        //print_r($file);

        //echo UploadFileUtil::GetUrl($request->path);
        //echo UploadFileUtil::GetUrlById($request->file_id);
        //return UploadFileUtil::Download($request->path);
        return UploadFileUtil::DownloadById($request->file_id);

        //echo ThumbnailImageUtil::ThumbnailById($request->file_id, 100, 100);
        //echo ThumbnailImageUtil::Thumbnail($file->path, 100, 100);
    }
    
    public function export(Request $request)
    {
        $type = $request->type;
        \App\Business\ReportBussiness::exportExample($type);
    }

    public function testPayment($name, $amount)
    {
        switch ($name) {
            case 'payments':
                $model = Payment::class;
                break;
            case 'orders':
                $model = Order::class;
                break;
        }

        $data = $model::orderBy('created_at', 'desc')->limit($amount)->get();
        return $data;
        exit;

        $url_base = "http://sandbox.vnptpay.vn/rest/payment/v1.0.5/init";
        $api_key = "acf392daf78c3cbeeb9705dab9a63212";
        $input = [
            "ACTION"=> "CONFIRM",
            "RESPONSE_CODE"=> "00",
            "MERCHANT_SERVICE_ID"=> 622,
            "MERCHANT_ORDER_ID"=> "366",
            "AMOUNT"=> "160000",
            "CURRENCY_CODE"=> "VND",
            "VNPTPAY_TRANSACTION_ID"=> "190130050295",
            "PAYMENT_METHOD"=> "NCB",
            "PAY_DATE"=> "20190219102730",
            "ADDITIONAL_INFO"=> "Thanh toán đơn hàng 366"
        ];
        $rs = "";
        foreach ($input as $k => $v) {
            if ($k != "SECURE_CODE") {
                $rs .= $v . "|";
            }
        }
        $rs .= "36e3c5b5a6d53fce2a2cc23d46ee59d7";
        $secureCode = hash("sha256", $rs);
        return $secureCode;
        $input['SECURE_CODE'] = $secureCode;

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, json_encode($input));
        curl_setopt($curl, CURLOPT_URL, $url_base);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Authorization: Bearer '.$api_key
        ));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

        // EXECUTE:
        $result = curl_exec($curl);
        if (!$result) {
            return ["success" => false, "msg" => "not connect"];
        }
        curl_close($curl);
        $rs = json_decode($result, true);
        return $rs['RESPONSE_CODE'];
    }
}
