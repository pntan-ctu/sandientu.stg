<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Utils\UploadFileUtil;
use App\Utils\ThumbnailImageUtil;

class UtilController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    public function viewImage($width, $height, $path)
    {
        $img = ThumbnailImageUtil::thumbnail($path, $width, $height);
        return ($img != null) ? $img->response() : null;
    }

    public function viewImageId($width, $height, $id)
    {
        $img = ThumbnailImageUtil::thumbnailById($id, $width, $height);
        return ($img != null) ? $img->response() : null;
    }

    public function uploadImage(Request $request)
    {
        $file = UploadFileUtil::uploadImage($request, 'file');
        return json_encode(['success' => ($file != null), 'data' => $file]);
    }

    public function multiUploadImage(Request $request)
    {
        $files = UploadFileUtil::multiUploadImage($request, 'files');
        return json_encode(['success' => (count($files) > 0), 'data' => $files]);
    }

    public function uploadImageCKEditor(Request $request)
    {
        //FckEditor upload ảnh bởi đối tượng file có tên 'upload'
        $img = UploadFileUtil::uploadImage($request, 'upload');
        
        $url = ($img != null) ? "/image/0/0/{$img->path}" : "";
        $message = ($img == null) ? "Lỗi không upload được ảnh !" : "";

        return view('common/uploadCKEditor', [
                        'CKEditorFuncNum' => $request->CKEditorFuncNum,
                        'data' => [
                            'url' => $url,
                            'message' => $message,
                        ],
                    ]);
    }

    public function uploadFile(Request $request)
    {
        $file = UploadFileUtil::uploadFile($request, 'file');
        return json_encode(['success' => ($file != null), 'data' => $file]);
    }

    public function multiUploadFile(Request $request)
    {
        $files = UploadFileUtil::multiUploadFile($request, 'files');
        return json_encode(['success' => (count($files) > 0), 'data' => $files]);
    }

    public function uploadFileCKEditor(Request $request)
    {
        //FckEditor upload file bởi đối tượng file có tên 'upload'
        $file = UploadFileUtil::uploadFile($request, 'upload');
        
        $url = ($file != null) ? "/download/{$file->path}" : "";
        $message = ($file == null) ? "Lỗi không upload được ảnh !" : "";

        return view('common/uploadCKEditor', [
                        'CKEditorFuncNum' => $request->CKEditorFuncNum,
                        'data' => [
                            'url' => $url,
                            'message' => $message,
                        ],
                    ]);
    }

    public function download($path)
    {
        return UploadFileUtil::download($path);
    }

    public function downloadById($id)
    {
        return UploadFileUtil::downloadById($id);
    }
}
