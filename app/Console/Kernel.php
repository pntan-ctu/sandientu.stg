<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;
use App\Business\ScheduleBussiness;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->call(function () {
            ScheduleBussiness::calcRatingOrganizations();
        })->everyMinute()->name("update_rating_organizaions")->withoutOverlapping(30)->onOneServer();
        
        $schedule->call(function () {
            ScheduleBussiness::calcRatingProducts();
        })->everyMinute()->name("update_rating_products")->withoutOverlapping(30)->onOneServer();
        
        $schedule->call(function () {
            ScheduleBussiness::calcViewProducts();
        })->everyMinute()->name("update_view_products")->withoutOverlapping(30)->onOneServer();
        
        $schedule->call(function () {
            ScheduleBussiness::calcOrderProducts();
        })->everyMinute()->name("update_order_products")->withoutOverlapping(30)->onOneServer();
        
        $schedule->call(function () {
            ScheduleBussiness::calcOrderOrganizations();
        })->everyMinute()->name("update_order_organizaions")->withoutOverlapping(30)->onOneServer();

        $schedule->call(function () {
            ScheduleBussiness::calcViewOrganizations();
        })->everyMinute()->name("update_view_organizaions")->withoutOverlapping(30)->onOneServer();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
