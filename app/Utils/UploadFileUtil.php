<?php

namespace App\Utils;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use App\Models\FileMetadata;
use Illuminate\Support\Facades\Validator;
use Intervention\Image\Facades\Image;

class UploadFileUtil
{
    /**
     * Kiểm tra và trả về kiểu file
     */
    private static function getFileType($extension)
    {
        $extension_image = ['bmp', 'dib', 'rle', 'jpg', 'jpeg', 'jpe', 'jfif', 'gif', 'tif', 'tiff', 'png'];
        $extension_doc = ['doc', 'docx'];
        $extension_excel = ['xls', 'xlsx'];
        $extension_pdf = ['pdf'];
        //$extension_media = ['wave', 'mp3', 'mp4'];
        $extension_other = ['ppt', 'pptx', 'rar', 'zip', 'swf', 'txt'];

        $extension = strtolower($extension);
        if (in_array($extension, $extension_image)) {
            return "image";
        } elseif (in_array($extension, $extension_doc)) {
            return "doc";
        } elseif (in_array($extension, $extension_excel)) {
            return "excel";
        } elseif (in_array($extension, $extension_pdf)) {
            return "pdf";
        /*} elseif (in_array($extension, $extension_media)) {
            return "media";*/
        } elseif (in_array($extension, $extension_other)) {
            return "other";
        } else {
            return false;
        }
    }

    /**
     * Resize picture
     */
    private static function resizeImage(string $path, int $width, int $height)
    {
        if (Storage::exists($path)) {
            $file = Storage::get($path);
            $igmOld = Image::make($file);
            if ($igmOld->getWidth() > $width || $igmOld->getHeight() > $height) {
                $imageNew = $igmOld->resize($width, $height, function ($constraint) {
                    $constraint->aspectRatio();
                })->encode('jpg', 80);
                Storage::put($path, (string)$imageNew);
            }
        }
    }

    /**
     * Upload one image. Return FileMetadata stored
     */
    public static function uploadImage(Request $request, $key, int $width = 0, int $height = 0)
    {
        if ($request->hasFile($key)) {
            $file = $request->file($key);
            return  self::uploadImageFile($file, $width, $height);
        }
        return array();
    }

    //Quan them cho API
    public static function uploadImageFile($file, int $width = 0, int $height = 0)
    {
        $user = Auth::user();
        $user_id = ($user != null) ? $user->id : 0;

        $max_size = config('filesystems.max_size_file_image');
        $max_width = config('filesystems.max_width_image');
        $max_height = config('filesystems.max_height_image');

        $extension = $file->getClientOriginalExtension();
        $file_name = $file->getClientOriginalName();
        $file_size = $file->getClientSize();
        $typefile = self::getFileType($extension);

        if ($typefile != 'image' || $file_size > $max_size) {
            return null;
        } else {
            //Lưu file
            $path = $file->store(''); //$typefile

            //resize ảnh theo yêu cầu hoặc có kích thước quá giới hạn
            if ($width <= 0 || $width > $max_width) {
                $width = $max_width;
            }
            if ($height <= 0 || $height > $max_height) {
                $height = $max_height;
            }
            self::resizeImage($path, $width, $height);

            $fileMeta = FileMetadata::create([
                'path' => $path,
                'file_name' => $file_name,
                'type' => $typefile,
                'size' => $file_size,
                'upload_by' => $user_id
            ]);

            return $fileMeta;
        }
    }

    /**
     * Upload multi file. Return array FileMetadata stored
     */
    public static function multiUploadImage(Request $request, string $key, int $width = 0, int $height = 0)
    {
        // kiểm tra có files sẽ xử lý
        if ($request->hasFile($key)) {
            $files = $request->file($key);
            return  self::multiUploadImageFile($files, $width, $height);
        }
        return array();
    }

    //Vuong them cho API
    public static function multiUploadImageFile($files, int $width = 0, int $height = 0)
    {
        $user = Auth::user();
        $user_id = ($user != null) ? $user->id : 0;

        $max_size = config('filesystems.max_size_file_image');
        $max_width = config('filesystems.max_width_image');
        $max_height = config('filesystems.max_height_image');


        $arrFileMetas = array();
        // kiểm tra tất cả các files xem có đuôi mở rộng đúng không
        foreach ($files as $file) {
            $extension = $file->getClientOriginalExtension();
            $file_name = $file->getClientOriginalName();
            $file_size = $file->getClientSize();

            $typefile = self::getFileType($extension);

            if ($typefile != 'image' || $file_size > $max_size) {
                continue;
            } else {
                //Lưu file
                $path = $file->store(''); //$typefile

                //resize ảnh theo yêu cầu hoặc có kích thước quá giới hạn
                if ($width <= 0 || $width > $max_width) {
                    $width = $max_width;
                }
                if ($height <= 0 || $height > $max_height) {
                    $height = $max_height;
                }
                self::resizeImage($path, $width, $height);

                $fileMeta = FileMetadata::create([
                    'path' => $path,
                    'file_name' => $file_name,
                    'type' => $typefile,
                    'size' => $file_size,
                    'upload_by' => $user_id
                ]);

                $arrFileMetas[] = $fileMeta;
            }
        }
        //Return array FileMetadata
        return $arrFileMetas;
    }
    /**
     * Upload one file. Return FileMetadata stored
     */
    public static function uploadFile(Request $request, $key)
    {
        if ($request->hasFile($key)) {
            $file = $request->file($key);

            $user = Auth::user();
            $user_id = ($user != null) ? $user->id : 0;
            $max_size = config('filesystems.max_size_file');

            $extension = $file->getClientOriginalExtension();
            $file_name = $file->getClientOriginalName();
            $file_size = $file->getClientSize();
            $typefile = self::getFileType($extension);

            if ($typefile === false || $file_size > $max_size) {
                return null;
            } else {
                $path = $file->store(''); //$typefile

                $fileMeta = FileMetadata::create([
                    'path' => $path,
                    'file_name' => $file_name,
                    'type' => $typefile,
                    'size' => $file_size,
                    'upload_by' => $user_id
                ]);

                return $fileMeta;
            }
        }
        return null;
    }

    /**
     * Upload multi file. Return array FileMetadata stored
     */
    public static function multiUploadFile(Request $request, string $key)
    {
        // kiểm tra có files sẽ xử lý
        if ($request->hasFile($key)) {
            $files = $request->file($key);

            $user = Auth::user();
            $user_id = ($user != null) ? $user->id : 0;
            $max_size = config('filesystems.max_size_file');

            $arrFileMetas = array();
            // kiểm tra tất cả các files xem có đuôi mở rộng đúng không
            foreach ($files as $file) {
                $extension = $file->getClientOriginalExtension();
                $file_name = $file->getClientOriginalName();
                $file_size = $file->getClientSize();

                $typefile = self::getFileType($extension);

                if ($typefile === false || $file_size > $max_size) {
                    continue;
                } else {
                    $path = $file->store(''); //$typefile

                    $fileMeta = FileMetadata::create([
                        'path' => $path,
                        'file_name' => $file_name,
                        'type' => $typefile,
                        'size' => $file_size,
                        'upload_by' => $user_id
                    ]);

                    $arrFileMetas[] = $fileMeta;
                }
            }
            //Return array FileMetadata
            return $arrFileMetas;
        }

        return array();
    }

    /**
     * Trả về file_metadata
     * Param: $path : tên file được lưu trữ
     */
    public static function getFileMetadata($path)
    {
        $file = FileMetadata::where('path', $path)->first();
        if ($file == null) {
            return $file;
        }

        $file['url'] = UploadFileUtil::GetUrl($file->path);
        return $file;
    }

    /**
     * Trả về file_metadata
     * Param: $id : id file_metadata
     */
    public static function getFileMetadataById($id)
    {
        $file = FileMetadata::find($id);
        if ($file == null) {
            return $file;
        }

        $file['url'] = UploadFileUtil::GetUrl($file->path);
        return $file;
    }

    /**
     * Lấy Url bởi tên file
     * Param: $path : tên file được lưu trữ
     */
    public static function getUrl($path)
    {
        return Storage::url($path);
    }

    /**
     * Lấy Url bởi tên file
     * Param: $id : id FileMetadata được lưu trữ
     */
    public static function getUrlById($id)
    {
        $file = FileMetadata::find($id);
        if ($file == null) {
            return null;
        }
        return Storage::url($file->path);
    }

    /**
     * Download bởi tên file
     * Param: $path : tên file được lưu trữ
     */
    public static function download($path)
    {
        $file = FileMetadata::where('path', $path)->first();
        if ($file == null) {
            return $file;
        }

        return Storage::download($file->path, $file->file_name);
    }

    /**
     * Download bởi tên file
     * Param: $id : id FileMetadata được lưu trữ
     */
    public static function downloadById($id)
    {
        $file = FileMetadata::find($id);
        if ($file == null) {
            return null;
        }
        return Storage::download($file->path, $file->file_name);
    }

    /* Sử dụng hàm chung phía trên, sau sẽ sửa lại hợp lý hơn
    public static function uploadImageValidator(Request $request, $key, int $width = 0, int $height = 0)
    {
        $max_width = config('filesystems.max_width_image');
        $max_height = config('filesystems.max_height_image');

        $user = Auth::user();

        $user_id = ($user != null) ? $user->id : 0;
        $file = $request->file($key);
        $extension = $file->getClientOriginalExtension();
        $file_name = $file->getClientOriginalName();
        $file_size = $file->getClientSize();
        $typefile = self::getFileType($extension);

        //Lưu file
        $path = $file->store(''); //$typefile

        //resize ảnh theo yêu cầu hoặc có kích thước quá giới hạn
        if ($width <= 0 || $width > $max_width) {
            $width = $max_width;
        }
        if ($height <= 0 || $height > $max_height) {
            $height = $max_height;
        }
        self::resizeImage($path, $width, $height);

        $fileMeta = FileMetadata::create([
            'path' => $path,
            'file_name' => $file_name,
            'type' => $typefile,
            'size' => $file_size,
            'upload_by' => $user_id
        ]);

        return $fileMeta;
    }*/
}
