<?php

namespace App\Utils;

use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Storage;
use App\Models\FileMetadata;

class ThumbnailImageUtil
{
    public static function getSize($size)
    {
        $arrSizes = config('filesystems.size_thumb_images');
        $min_size = 999999;
        $max_size = 0;
        foreach ($arrSizes as $value) {
            //Tìm số nhỏ nhất trong mảng >= $size
            if ($value >= $size && $value < $min_size) {
                $min_size = $value;
            }
            //Tìm số nhỏ nhất trong mảng
            if ($value > $max_size) {
                $max_size = $value;
            }
        }
        return ($min_size > $max_size) ? $max_size : $min_size;
    }

    public static function thumbnail(string $path, int $width, int $height)
    {
        $width = self::getSize($width);
        $height = self::getSize($height);

        if ($width == 0 && $height == 0) {
            return Storage::exists($path) ? Image::make(Storage::get($path)) : null;
        }

        $path_thumbnail = "thumbs/{$width}x{$height}/{$path}";
        $disk_thumb = Storage::disk(config('filesystems.disk_thumb', 'public'));

        if (!$disk_thumb->exists($path_thumbnail)) {
            if (!Storage::exists($path)) {
                return null;
            }

            $igmOld = Image::make(Storage::get($path));

            //Giữ nguyên kích thức nếu width, height = 0
            $width = ($width <= 0) ? $igmOld->getWidth() : $width;
            $height = ($height <= 0) ? $igmOld->getHeight() : $height;

            $imageNew = $igmOld->resize($width, $height, function ($constraint) {
                $constraint->aspectRatio();
            })->encode('jpg', 80);

            $disk_thumb->put($path_thumbnail, (string)$imageNew);
        }

        //$url = $disk_thumb->url($path_thumbnail);
        return Image::make($disk_thumb->get($path_thumbnail));
    }

    public static function thumbnailById(int $fileId, int $width, int $height)
    {
        $file = FileMetadata::find($fileId);
        if ($file == null) {
            return null;
        }

        $path = $file->path;

        return self::thumbnail($path, $width, $height);
    }
}
