<?php
/**
 * Created by PhpStorm.
 * User: letro
 * Date: 10/1/2018
 * Time: 1:53 PM
 */

namespace App\Utils;

use App\Models\Organization;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class CommonUtil
{
    public static function validateMap($request, $organization)
    {
        $validator = Validator::make($request->all(), []);
        $validator->after(function ($validator) use ($request, $organization) {
            if (isset($request->map)) {
                $arrMap = explode(",", $request->map);
                if (count($arrMap) != 2) {
                    $organization->map_lat = null;
                    $organization->map_long = null;
                    $validator->errors()
                        ->add('map', 'Định dạng trường Bản đồ không đúng. Định dạng đúng: 19.808241, 105.777592');
                } elseif (trim($arrMap[0]) != "" && trim($arrMap[1]) != "") {
                    $organization->map_lat = floatval(trim($arrMap[0]));
                    $organization->map_long = floatval(trim($arrMap[1]));
                    if ($organization->map_lat < -90.0 || $organization->map_lat > 90.0) {
                        $validator->errors()
                            ->add('map', 'Trường ' . $organization->map_lat .
                                ' phải có giá trị <= 90.00 và >= -90.00');
                    } elseif ($organization->map_long < -180.0 || $organization->map_long > 180.0) {
                        $validator->errors()
                            ->add('map', 'Trường ' . $organization->map_long .
                                ' phải có độ dài <= 9, có giá trị <= 180.00 và >= -180.00');
                    }
                }
            } else {
                $organization->map_lat = null;
                $organization->map_long = null;
            }
        });
        return $validator;
    }

    public static function validateMap2($request, $organization)
    {
        $validator = Validator::make($request->all(), []);
        $validator->after(function ($validator) use ($request, $organization) {
            if (isset($request->map_lat)) {
                $organization->map_lat = floatval(trim($request->map_lat));
                if ($organization->map_lat < -90.0 || $organization->map_lat > 90.0) {
                    $validator->errors()
                        ->add('map_lat', 'Trường map_lat phải có giá trị <= 90.00 và >= -90.00');
                }
            } else {
                $organization->map_lat = null;
            }

            if (isset($request->map_long)) {
                $organization->map_long = floatval(trim($request->map_long));
                if ($organization->map_long < -180.0 || $organization->map_long > 180.0) {
                    $validator->errors()
                        ->add('map_long', 'Trường map_long phải có giá trị <= 180.00 và >= -180.00');
                }
            } else {
                $organization->map_long = null;
            }
        });
        return $validator;
    }

    public static function validateFoundingNumber($request, $organizationId)
    {
        $validator = Validator::make($request->all(), []);
        $validator->after(function ($validator) use ($request, $organizationId) {
            $organization = Organization::where('founding_type', $request->founding_type)
                ->where('founding_number', $request->founding_number)
                ->where('id', '!=', $organizationId)
                ->first(['id']);
            if (isset($organization)) {
                $validator->errors()
                    ->add('founding_number', 'Số giấy phép đăng ký này đã được sử dụng');
            }
        });
        return $validator;
    }

    /**
     * @author Nguyen Quan
     * get number unread messages of current login user or organization
     */
    public static function countUnreadMessages()
    {
        $objAccess = getRoleAccess() == \App\RoleAccess::BUSINESS ? getOrgModel() : Auth::user();
        $unreadMessages = $objAccess->unreadMessages ?? [];
        return count($unreadMessages);
    }
}
