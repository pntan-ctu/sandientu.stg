<?php

namespace App\Utils;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\FileMetadata;

class Youtube
{
    public function __construct()
    {
    }
    
    public function parseVideoEntry($entry)
    {
        //$obj = new stdClass;
        $obj = (object) array(); //Thử xem có lỗi không
      
        // get author name and feed URL
        $obj->author = $entry->author->name;
        $obj->authorURL = $entry->author->uri;
      
        // get nodes in media: namespace for media information
        $media = $entry->children('https://search.yahoo.com/mrss/');
        $obj->title = $media->group->title;
        $obj->description = $media->group->description;
      
        // get video player URL
        $attrs = $media->group->player->attributes();
        $obj->watchURL = $attrs['url'];
      
        // get video thumbnail
        $attrs = $media->group->thumbnail[0]->attributes();
        $obj->thumbnailURL = $attrs['url'];
        $attrs = $media->group->thumbnail[1]->attributes();
        $obj->thumbnailURL1 = $attrs['url'];
            
        // get <yt:duration> node for video length
        $yt = $media->children('https://gdata.youtube.com/schemas/2007');
        $attrs = $yt->duration->attributes();
        $obj->length = $attrs['seconds'];
      
        // get <yt:stats> node for viewer statistics
        $yt = $entry->children('https://gdata.youtube.com/schemas/2007');
        $attrs = $yt->statistics->attributes();
        $obj->viewCount = $attrs['viewCount'];
      
        // get <gd:rating> node for video ratings
        $gd = $entry->children('https://schemas.google.com/g/2005');
        if ($gd->rating) {
            $attrs = $gd->rating->attributes();
            $obj->rating = $attrs['average'];
        } else {
            $obj->rating = 0;
        }
        
        // get <gd:comments> node for video comments
        $gd = $entry->children('https://schemas.google.com/g/2005');
        if ($gd->comments->feedLink) {
            $attrs = $gd->comments->feedLink->attributes();
            $obj->commentsURL = $attrs['href'];
            $obj->commentsCount = $attrs['countHint'];
        }
      
        // get feed URL for video responses
        $entry->registerXPathNamespace('feed', 'https://www.w3.org/2005/Atom');
        $nodeset = $entry->xpath("feed:link[@rel='https://gdata.youtube.com/
      schemas/2007#video.responses']");
        if (count($nodeset) > 0) {
            $obj->responsesURL = $nodeset[0]['href'];
        }
         
        // get feed URL for related videos
        $entry->registerXPathNamespace('feed', 'https://www.w3.org/2005/Atom');
        $nodeset = $entry->xpath("feed:link[@rel='https://gdata.youtube.com/
      schemas/2007#video.related']");
        if (count($nodeset) > 0) {
            $obj->relatedURL = $nodeset[0]['href'];
        }
    
        // return object to caller
        return $obj;
    }
    
    public function &getFeedContent($sUrl)
    {
        try {
            preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v/)[^&\n]+(?=\?)"
                    . "|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $sUrl, $matches);

            if (!empty($matches)) {
                $vid = $matches[0];
                $feedURL = 'https://gdata.youtube.com/feeds/api/videos/' . $vid;

                $entry = @simplexml_load_file($feedURL);
                if (!$entry) {
                    return "";
                }
                
                $video = $this->parseVideoEntry($entry);

                $sContent = "<div id='youtube-player'><table>";
                $sContent .= "<tr>";
                $sContent .= "<td valign='top' id='player'>"
                        . "<div vid='$vid' onclick='return youtube_player(this);"
                        . "' style='cursor: pointer' class='feed-play'></div>"
                        . "<img style='cursor: pointer' border='0' vid='$vid' "
                        . "onclick='return youtube_player(this);"
                        . "' src=\"$video->thumbnailURL1\"/></td>";
                $sContent .= "<td valign='top'>"
                        . "<a target='_blank' href=\"{$video->watchURL}\">{$video->title}</a>"
                        . "<div style='margin-top:5px;'>";
                $sContent .= sprintf("%0.2f", $video->length/60) . " minutes | {$video->viewCount} views</div>";
                $sContent .= $this->getmore($video->description) . "</td>";
                $sContent .= "</tr>";
                $sContent .= "</table></div>";
            } else {
                return "";
            }

            return $sContent;
        } catch (\Exception $e) {
            return "";
        }
    }
    
    public function &getContent($sUrl, $sTitle = false)
    {
        try {
            preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v/)[^&\n]+(?=\?)"
                    . "|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $sUrl, $matches);

            if (!empty($matches)) {
                $vid = $matches[0];
                $feedURL = 'https://gdata.youtube.com/feeds/api/videos/' . $vid;

                $entry = @simplexml_load_file($feedURL);
                if (!$entry) {
                    return "";
                }
                
                $video = $this->parseVideoEntry($entry);
                $sTitle = $sTitle === false ? $video->title : $sTitle;
                $sContent = "<div id='youtube-player'><table>";
                $sContent .= "<tr>";
                $sContent .= "<td valign='top'><div vid='$vid' onclick='return youtube_player2(this);"
                        . "' style='cursor: pointer' class='feed-play'></div><div id='player'>"
                        . "<img style='cursor: pointer' border='0' vid='$vid' onclick='return youtube_player2(this);"
                        . "' src=\"$video->thumbnailURL1\"/><div></td>";
                $sContent .= "<td valign='top'><a vid='$vid' onclick='return youtube_player2(this);"
                        . "' href='#'><b>{$sTitle}</b></a><div style='margin-top:5px;'>";
                $sContent .= sprintf("%0.2f", $video->length/60) . " minutes | {$video->viewCount} views</div>";
                $sContent .= $this->getmore($video->description) . "</td>";
                $sContent .= "</tr>";
                $sContent .= "</table></div>";
            } else {
                return "";
            }

            return $sContent;
        } catch (\Exception $e) {
            return "";
        }
    }
    
    public function &getVideoContent($sUrl, $width = 0, $height = 0, $image_id = 0)
    {
        try {
            preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v/)[^&\n]+(?=\?)"
                    . "|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $sUrl, $matches);
            if (!empty($matches)) {
                $vid = $matches[0];
                $tmp = "<embed width='{$width}' height='{$height}' flashvars='width={$width}&amp;"
                . "height={$height}' wmode='opaque' salign='tl' allowscriptaccess='never' "
                . "allowfullscreen='true' scale='scale' quality='high' bgcolor='#FFFFFF' "
                . "name='swf_ul352q_1' id='swf_ul352q_1' style='width: 100%;' "
                . "src='https://www.youtube.com/v/{$vid}?version=3&amp;autohide=1&amp;"
                . "autoplay=1' type='application/x-shockwave-flash'>";
                
                $sImg = "<img alt='Xem video'  onclick=\"javascript: var embed_ = $(this).attr('vid');"
                        . " $(this).parents('#youtube-player').html(embed_);"
                        . "\" style='cursor: pointer; ".($width > 0 && $height > 0? "width:{$width}px;"
                        . " height:{$height}px;":"")."' ".($image_id > 0 ? " "
                        . "class = 'img_linkvideo' ":"")." border='0' vid=\"$tmp\" src=";
                if ($image_id) {
                    /*$width = $width - 15;
                    $height = $height - 15;*/
                    $sImg .= "\"file/thumb/{$width}/{$height}/{$image_id}.jpg\"/>";
                } else {
                    //$VideoURL = 'http://gdata.youtube.com/feeds/api/videos/' . $vid;
                    //$entry = @simplexml_load_file($VideoURL);
                    //if (!$entry) return "";
                    //$video = $this->parseVideoEntry($entry);
                    //$sImg .= "\"$video->thumbnailURL1\"/>";
                    $sImg .= "\"https://img.youtube.com/vi/".$vid."/0.jpg\"". " />";
                }
                $sContent = "<div id='youtube-player'>";
                $sContent .= "<div style = 'height:{$height}px;"
                . " ' onclick=\"javascript: var embed_ = $(this).attr('vid');"
                . " $(this).parents('#youtube-player').html(embed_);"
                . "\" style='cursor: pointer' class='youtobe-play' vid=\"$tmp\" ></div>";
                $sContent .= $sImg;
                $sContent .= "</div>";
            } else {
                return "";
            }

            return $sContent;
        } catch (\Exception $e) {
            return "";
        }
    }
    
    public function &playVideo($sUrl, $width = 0, $height = 0)
    {
        try {
            preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v/)[^&\n]+(?=\?)"
                    . "|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $sUrl, $matches);

            if (!empty($matches)) {
                $vid = $matches[0];
                $VideoURL = 'https://gdata.youtube.com/feeds/api/videos/' . $vid;

                //$entry = @simplexml_load_file($VideoURL);
                //if (!$entry) return "";
                
                $embed =  "<embed width='{$width}' height='{$height}' flashvars='width={$width}&amp;"
                . "height={$height}' wmode='opaque' salign='tl' allowscriptaccess='never' "
                . "allowfullscreen='true' scale='scale' quality='high' "
                . "bgcolor='#FFFFFF' name='swf_ul352q_1' id='swf_ul352q_1' style='' "
                . "src='https://www.youtube.com/v/{$vid}?version=3&amp;autohide=1&amp;autoplay=0' "
                . "type='application/x-shockwave-flash'>";
             
                return $embed;
            } else {
                return "";
            }
        } catch (\Exception $e) {
            return "";
        }
    }
    
    public function &playVideoServer($sUrl, $width = 0, $height = 0, $image_id = 0)
    {
        try {
            $url = $sUrl.".flv";
            $eData = "static/player.swf";
            $iUrl = "file/thumb/{$width}/{$height}/{$image_id}.jpg";
            $embed = "<object type='application/x-shockwave-flash' src='{$url}' "
            . "data='{$eData}' width='{$width}' height='{$height}' play='false' loop='true'>
                        <param name='wmode' value='opaque'>
                        <param name='allowfullscreen' value='true'>
                        <param name='allowscriptaccess' value='always'>
                        <param name='flashvars' value='controlbar=bottom&amp;
                        backcolor=000000&amp;frontcolor=999999&amp;lightcolor=CC0000&amp;
                        screencolor=000000&amp;viral.oncomplete=false&amp;
                        viral.onpause=false&amp;provider=http&amp;
                        width={$width}&amp;height={$height}&amp;repeat=true&amp;"
                        . "autostart=false&amp;file={$url}&amp;image={$iUrl}'>
                 </object>";
            return $embed;
        } catch (\Exception $e) {
            return "";
        }
    }
    
    public function getmore($s)
    {
        $count = 200;
        if (strlen($s) <= $count + 50) {
            return $s;
        } else {
            $count = strpos($s, " ", $count);
            $s1 = substr($s, 0, $count);
            $s2 = substr($s, $count);
            $s = "$s1 <span onclick='return show_more(this);'>...&nbsp;"
                    . "<a href='#' onclick='return false;'>(Show more)</a>"
                    . "</span>"
                    . "<span style='display:none;'>$s2 "
                    . "<span onclick='return show_less(this);'>&nbsp;&nbsp;"
                    . "<a href='#' onclick='return false;'>(Show less)</a></span></span>";
        }
        
        return $s;
    }


    public function &getSameVideo($sUrl)
    {
        preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v/)[^&\n]+(?=\?)"
                . "|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $sUrl, $matches);
        if (!empty($matches)) {
            $vid = $matches[0];
            $sImg = "<img src=\"https://img.youtube.com/vi/".$vid."/0.jpg\"". " />";
            $sContent = "<div id='youtube-player'>";
            $sContent .= $sImg;
            $sContent .= "</div>";
        } else {
            return "";
        }
        return $sContent;
    }
}
