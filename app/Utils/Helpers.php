<?php

//Chạy cmd : composer dumpautoload

if (! function_exists('getUserId')) {
    function getUserId()
    {
        if (!auth()->check()) {
            return -1;
        }

        return auth()->user()->id;
    }
}

if (! function_exists('isUserMember')) {
    function isUserMember()
    {
        if (! auth()->check()) {
            return false;
        }

        $orgModel = auth()->user()->Organization;
        if (! isset($orgModel)) {
            return true;
        }

        return ($orgModel->organization_type_id == App\OrgType::MEMBER);
    }
}

if (! function_exists('isUserBusiness')) {
    function isUserBusiness()
    {
        if (! auth()->check()) {
            return false;
        }

        $orgModel = auth()->user()->Organization;
        if (! isset($orgModel)) {
            return false;
        }

        return ($orgModel->organization_type_id >= 100);
    }
}

if (! function_exists('isUserGov')) {
    function isUserGov()
    {
        if (! auth()->check()) {
            return false;
        }

        $orgModel = auth()->user()->Organization;
        if (! isset($orgModel)) {
            return false;
        }

        return ($orgModel->organization_type_id > 0 && $orgModel->organization_type_id < 100);
    }
}

if (! function_exists('getOrgModel')) {
    function getOrgModel()
    {
        if (!auth()->check()) {
            return null;
        }

        return auth()->user()->Organization;
    }
}

if (! function_exists('getOrgId')) {
    /**
     * @return int -1 nếu chưa đăng nhập, 0 nếu không thuộc org nào
     */
    function getOrgId()
    {
        if (!auth()->check()) {
            return -1;
        }

        $orgModel = auth()->user()->Organization;
        if (! isset($orgModel)) {
            return 0;
        }

        return $orgModel->id;
    }
}

if (! function_exists('getOrgTypeId')) {
    function getOrgTypeId()
    {
        if (! auth()->check()) {
            return -1;
        }

        $orgModel = auth()->user()->Organization;
        if (! isset($orgModel)) {
            return 0;
        }

        return $orgModel->organization_type_id;
    }
}

if (! function_exists('getRegionId')) {
    function getRegionId()
    {
        if (!auth()->check()) {
            return -1;
        }

        $orgModel = auth()->user()->Organization;
        if (! isset($orgModel)) {
            return -1;
        }

        return $orgModel->region_id;
    }
}

if (! function_exists('setRoleAccess')) {
    function setRoleAccess($role)
    {
        $userId = getUserId();
        cache()->forever(
            "RoleAccess_$userId",
            ($role != App\RoleAccess::BUSINESS) ? App\RoleAccess::PERSONAL : App\RoleAccess::BUSINESS
        );
    }
}

if (! function_exists('getRoleAccess')) {
    function getRoleAccess()
    {
        if (!auth()->check()) {
            return App\RoleAccess::GUEST;
        }
        
        if (isUserBusiness()) {
            $userId = auth()->user()->id;
            return cache()->rememberForever("RoleAccess_$userId", function () {
                return isUserBusiness() ? App\RoleAccess::BUSINESS : App\RoleAccess::PERSONAL;
            });
        } else {
            return App\RoleAccess::PERSONAL;
        }
    }
}

if (! function_exists('getAccessingObject')) {
    /**
     * @return \App\Models\User|\App\Models\Organization|null
     */
    function getAccessingObject()
    {
        if (getRoleAccess() == App\RoleAccess::BUSINESS) {
            return getOrgModel();
        } else {
            return auth()->user();
        }
    }
}

if (! function_exists('getVisitSite')) {
    function getVisitSite()
    {
        $object = \App\Models\OrganizationType::find(\App\OrgType::VPDP);
        return visits($object);
    }
}

if (! function_exists('incVisitSite')) {
    function incVisitSite()
    {
        $object = \App\Models\OrganizationType::find(\App\OrgType::VPDP);
        visits($object)->increment(); //->seconds(5*60)
    }
}

if (! function_exists('escapeJsonEncode')) {
    function escapeJsonEncode($value)
    {
        return json_encode($value, JSON_HEX_QUOT|JSON_HEX_TAG|JSON_HEX_AMP|JSON_HEX_APOS);
    }
}

if (! function_exists('viewImage')) {
    function viewImage($path, $width = 0, $height = 0)
    {
        if (strlen($path) > 0) {
            return url("/image/$width/$height/$path");
        } else {
            return url('img/no-image.png');
        }
    }
}

if (! function_exists('viewImageId')) {
    function viewImageId($id, $width = 0, $height = 0)
    {
        if ($id > 0) {
            return url("/image-id/$width/$height/$id");
        } else {
            return url('img/no-image.png');
        }
    }
}

//if (! function_exists('changeTitle')) {
//    //MB_CASE_UPPER / MB_CASE_TITLE / MB_CASE_LOWER
//    function changeTitle($str, $strSymbol = '-', $case = MB_CASE_LOWER)
//    {
//        $str=trim($str);
//        if ($str == "") {
//            return "";
//        }
//        $str = str_replace('"', '', $str);
//        $str = str_replace("'", '', $str);
//        $str = stripUnicode($str);
//        $str = mb_convert_case($str, $case, 'utf-8');
//        $str = preg_replace('/[\W|_]+/', $strSymbol, $str);
//        return $str;
//    }
//}

//if (! function_exists('stripUnicode')) {
//    function stripUnicode($str)
//    {
//        if (!$str) {
//            return '';
//        }
//        //$str = str_replace($a, $b, $str);
//        $unicode = array(
//            'a'=>'á|à|ả|ã|ạ|ă|ắ|ằ|ẳ|ẵ|ặ|â|ấ|ầ|ẩ|ẫ|ậ|å|ä|æ|ā|ą|ǻ|ǎ',
//            'A'=>'Á|À|Ả|Ã|Ạ|Ă|Ắ|Ằ|Ẳ|Ẵ|Ặ|Â|Ấ|Ầ|Ẩ|Ẫ|Ậ|Å|Ä|Æ|Ā|Ą|Ǻ|Ǎ',
//            'ae'=>'ǽ',
//            'AE'=>'Ǽ',
//            'c'=>'ć|ç|ĉ|ċ|č',
//            'C'=>'Ć|Ĉ|Ĉ|Ċ|Č',
//            'd'=>'đ|ď',
//            'D'=>'Đ|Ď',
//            'e'=>'é|è|ẻ|ẽ|ẹ|ê|ế|ề|ể|ễ|ệ|ë|ē|ĕ|ę|ė',
//            'E'=>'É|È|Ẻ|Ẽ|Ẹ|Ê|Ế|Ề|Ể|Ễ|Ệ|Ë|Ē|Ĕ|Ę|Ė',
//            'f'=>'ƒ',
//            'F'=>'',
//            'g'=>'ĝ|ğ|ġ|ģ',
//            'G'=>'Ĝ|Ğ|Ġ|Ģ',
//            'h'=>'ĥ|ħ',
//            'H'=>'Ĥ|Ħ',
//            'i'=>'í|ì|ỉ|ĩ|ị|î|ï|ī|ĭ|ǐ|į|ı',
//            'I'=>'Í|Ì|Ỉ|Ĩ|Ị|Î|Ï|Ī|Ĭ|Ǐ|Į|İ',
//            'ij'=>'ĳ',
//            'IJ'=>'Ĳ',
//            'j'=>'ĵ',
//            'J'=>'Ĵ',
//            'k'=>'ķ',
//            'K'=>'Ķ',
//            'l'=>'ĺ|ļ|ľ|ŀ|ł',
//            'L'=>'Ĺ|Ļ|Ľ|Ŀ|Ł',
//            'o'=>'ó|ò|ỏ|õ|ọ|ô|ố|ồ|ổ|ỗ|ộ|ơ|ớ|ờ|ở|ỡ|ợ|ö|ø|ǿ|ǒ|ō|ŏ|ő',
//            'O'=>'Ó|Ò|Ỏ|Õ|Ọ|Ô|Ố|Ồ|Ổ|Ỗ|Ộ|Ơ|Ớ|Ờ|Ở|Ỡ|Ợ|Ö|Ø|Ǿ|Ǒ|Ō|Ŏ|Ő',
//            'Oe'=>'œ',
//            'OE'=>'Œ',
//            'n'=>'ñ|ń|ņ|ň|ŉ',
//            'N'=>'Ñ|Ń|Ņ|Ň',
//            'u'=>'ú|ù|ủ|ũ|ụ|ư|ứ|ừ|ử|ữ|ự|û|ū|ŭ|ü|ů|ű|ų|ǔ|ǖ|ǘ|ǚ|ǜ',
//            'U'=>'Ú|Ù|Ủ|Ũ|Ụ|Ư|Ứ|Ừ|Ử|Ữ|Ự|Û|Ū|Ŭ|Ü|Ů|Ű|Ų|Ǔ|Ǖ|Ǘ|Ǚ|Ǜ',
//            'R'=>'Ŕ|Ŗ|Ř',
//            's'=>'ß|ſ|ś|ŝ|ş|š|ŕ|ŗ|ř',
//            'S'=>'Ś|Ŝ|Ş|Š',
//            't'=>'ţ|ť|ŧ',
//            'T'=>'Ţ|Ť|Ŧ',
//            'w'=>'ŵ',
//            'W'=>'Ŵ',
//            'y'=>'ý|ỳ|ỷ|ỹ|ỵ|ÿ|ŷ',
//            'Y'=>'Ý|Ỳ|Ỷ|Ỹ|Ỵ|Ÿ|Ŷ',
//            'z'=>'ź|ż|ž',
//            'Z'=>'Ź|Ż|Ž'
//        );
//        foreach ($unicode as $khongdau => $codau) {
//            $arr = explode("|", $codau);
//            $str = str_replace($arr, $khongdau, $str);
//        }
//        return $str;
//    }
//}

//if (! function_exists('dateTimeFormat')) {
//    function dateTimeFormat($str)
//    {
//        $arr = explode(" ", $str);
//        $date_format = date('d/m/Y', strtotime($arr[0]));
//        $time = 'Ngày '.$date_format.' vào lúc '.$arr[1];
//        return $time;
//    }
//}
//
//if (! function_exists('getWeekDay')) {
//    function getWeekDay()
//    {
//        $weekday = strtolower(date('l'));
//        switch ($weekday) {
//            case 'monday':
//                return 'Thứ Hai';
//                break;
//            case 'tuesday':
//                return 'Thứ Ba';
//                break;
//            case 'wednesday':
//                return 'Thứ Tư';
//                break;
//            case 'thursday':
//                return 'Thứ Năm';
//                break;
//            case 'friday':
//                return 'Thứ Sáu';
//                break;
//
//            case 'saturday':
//                return 'Thứ Bảy';
//                break;
//
//            default:
//                return 'Chủ Nhật';
//                break;
//        }
//    }
//}

//if (! function_exists('keywordHighlight')) {
//    function keywordHighlight($keyword, $str)
//    {
//        return str_replace($keyword, '<mark style="background:#ff0;">'.$keyword.'</mark>', $str);
//    }
//}

/* 15/11/2018 to 2018-11-15 00:00:00 */
if (! function_exists('parseDate')) {
    function parseDate($date)
    {
        return Carbon\Carbon::createFromFormat('d/m/Y', $date)->startOfDay();
    }
}

/* 15/11/2018 to 2018-11-15 23:59:59 */
if (! function_exists('parseDate_end')) {
    function parseDate_end($date)
    {
        return Carbon\Carbon::createFromFormat('d/m/Y', $date)->endOfDay();
    }
}

if (! function_exists('parseDateTime')) {
    function parseDateTime($dateTime)
    {
        return Carbon\Carbon::createFromFormat('d/m/Y H:i', $dateTime);
    }
}

/* @author: Nguyễn Quân */
if (! function_exists('removeSpecialCharacters')) {
    function removeSpecialCharacters($strInput, $isUTF8)
    {
        if ($isUTF8) {
            return preg_replace('/[^\p{L}\p{N}\s\-\_]/u', '', $strInput);
        } else {
            return preg_replace('/[^A-Za-z0-9\s\-\_]/', '', $strInput);
        }
    }
}
/* @author: Đậu Hoàng Phương */
if (! function_exists('playVideoYoutuber')) {
    function playVideoYoutuber($sUrl, $with, $height)
    {
        $youtuber = new \App\Utils\Youtube();
        $sYoutuber = $youtuber->getVideoContent($sUrl, $with, $height);
        return $sYoutuber;
    }
}
if (! function_exists('playVideo')) {
    function playVideo($sUrl, $with, $height)
    {
        $youtuber = new \App\Utils\Youtube();
        $sYoutuber = $youtuber->playVideo($sUrl, $with, $height);
        return $sYoutuber;
    }
}
if (! function_exists('getSameVideo')) {
    function getSameVideo($sUrl)
    {
        $youtuber1 = new \App\Utils\Youtube();
        $sYoutuber1 = $youtuber1->getSameVideo($sUrl);
        return $sYoutuber1;
    }
}
