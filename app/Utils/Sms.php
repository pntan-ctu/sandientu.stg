<?php

namespace App\Utils;

class Sms
{
    /*private $VINA_ERROR_CODE = array(
        "-1" => "Exception",
        "0" => "Success",
        "1" => "Username, password, IP, status các API không hợp lệ",
        "2" => "Thời gian đặt lịch sai định dạng",
        "3" => "ID method không hợp lệ",
        "7" => "Template không hợp lệ hoặc không tồn tại với nhãn và đại lý",
        "8" => "Sai thời gian quy định đối với tin nhắn QC",
        "9" => "Contract_type_id không hợp lệ",
        "10" => "User_name không hợp lệ (user đăng nhập của Agent không đúng)",
        "11" => "Độ dài tin nhắn không hợp lệ (quá 640 kí tự)",
        "12" => "Thời gian không hợp lệ với chính sách của Vinaphone",
        "13" => "Hợp đồng không đúng",
        "14" => "Label không hợp lệ"
    );*/

    public function __construct()
    {
    }

    /**
     * Hàm gửi SMS theo services của VINAPHONE
     *
     * @param array $aDataToSend array("params"=>array("string_content"),"mobilelist"=>array("phonenumber"))
     *               "params" phải được chuẩn hóa ($util->formatSMS()) trước khi truyền vào
     *               "mobilelist"  phải được chuẩn hóa ($util->formatPhoneNumber()) trước khi truyền vào
     * @return string $result {"RPLY": {"name": "send_sms_list","ERROR": "[error_code]","ERROR_DESC": "[error_desc]"}}
     */
    public static function sendByVinaWS($aDataToSend)
    {
        $data = json_encode(self::buildContentForVinaWS($aDataToSend));

        $curl_handle = curl_init("http://113.185.0.35:8888/smsmarketing/api");
        curl_setopt($curl_handle, CURLOPT_HTTPHEADER, array(
            'Accept: application/json;charset=utf-8',
            'Content-Type: application/json;charset=utf-8',
            'Expect: 100-continue',
            'Connection: Keep-Alive'));
        curl_setopt($curl_handle, CURLOPT_HEADER, false);
        curl_setopt($curl_handle, CURLOPT_POST, true);
        curl_setopt($curl_handle, CURLOPT_POSTFIELDS, $data);
        curl_setopt($curl_handle, CURLOPT_FOLLOWLOCATION, false);
        curl_setopt($curl_handle, CURLOPT_RETURNTRANSFER, true);
        $result = curl_exec($curl_handle);
        curl_close($curl_handle);

        return json_decode($result, true);
    }

    private static function buildContentForVinaWS($aDataToSend)
    {
        //$util = phpviet::getLib("util");

        $requestId = $aDataToSend["requestId"];

        $returnVal1 = array(
            "RQST" => array(
                "name" => "send_sms_list",
                "REQID" => "{$requestId}",
                "LABELID" => "67950",//Được lấy từ hệ thống SMSBRN
                "CONTRACTTYPEID" => "1",//1: CSKH (giá rẻ hơn); 2: QC (giá đắt hơn)
                "CONTRACTID" => "3089",//Được lấy từ hệ thống SMSBRN
                "TEMPLATEID" => "264630",//Được lấy từ hệ thống SMSBRN
                "SCHEDULETIME" => "",//Để trống => Gửi ngay, Nếu đặt: dd/MM/yyyy hh24:mi, ví dụ : 08/05/2012 16:30
                "ISTELCOSUB" => "0",//Mặc định là 0
                "AGENTID" => "116",//Được lấy từ hệ thống SMSBRN
                "APIUSER" => "VNPT-TTKD",//Được lấy từ hệ thống SMSBRN (UBTHUONGXUAN)
                "APIPASS" => "TTKD!@#321",//Được lấy từ hệ thống SMSBRN
                "USERNAME" => "TH_CS"//(TH_CS)
            )
        );

        $returnVal = array(
            "RQST" => array(
                "name" => "send_sms_list",
                "REQID" => "{$requestId}",
                "LABELID" => "67313",//Được lấy từ hệ thống SMSBRN
                "CONTRACTTYPEID" => "1",//1: CSKH (giá rẻ hơn); 2: QC (giá đắt hơn)
                "CONTRACTID" => "6250",//Được lấy từ hệ thống SMSBRN
                "TEMPLATEID" => "262408",//Được lấy từ hệ thống SMSBRN
                "SCHEDULETIME" => "",//Để trống => Gửi ngay, Nếu đặt: dd/MM/yyyy hh24:mi, ví dụ : 08/05/2012 16:30
                "ISTELCOSUB" => "0",//Mặc định là 0
                "AGENTID" => "116",//Được lấy từ hệ thống SMSBRN
                "APIUSER" => "VPDP-ATVSTPTHA",//Được lấy từ hệ thống SMSBRN (UBTHUONGXUAN)
                "APIPASS" => "VPDP-ATVSTPTHA!@#777",//Được lấy từ hệ thống SMSBRN
                "USERNAME" => "TH_CS"//(TH_CS)
            )
        );

        $aParams = array();
        $num = 0;
        foreach ($aDataToSend["params"] as $param) {
            $num++;
            /*$sms = $util->formatSMS(trim($param));
            array_push($aParams, array("NUM"=>"$num","CONTENT"=>"$sms"));*/
            //$param = self::chuan_hoa($param);
            array_push($aParams, array("NUM" => "$num", "CONTENT" => "$param"));
        }

        $sMobileList = "";
        foreach ($aDataToSend["mobilelist"] as $mobileNumber) {
            /*$phoneNumber=$util->formatPhoneNumber(trim($mobileNumber));
            if($util->getProvider($phoneNumber)!="Other"){
                $sMobileList = empty($sMobileList)?"{$phoneNumber}":$sMobileList.",{$phoneNumber}";
            }*/
            $sMobileList = empty($sMobileList) ? "{$mobileNumber}" : $sMobileList . ",{$mobileNumber}";
        }
        $returnVal["RQST"]["MOBILELIST"] = "{$sMobileList}";
        $returnVal["RQST"]["PARAMS"] = $aParams;

        return $returnVal;
    }

    public static function isVina($s)
    {
        //if (substr($s, 0, 4) == "0378") return 1;
        //if (substr($s, 0, 4) == "0373") return 1;
        if (substr($s, 0, 3) == "091") {
            return 1;
        }
        if (substr($s, 0, 3) == "094") {
            return 1;
        }
        if (substr($s, 0, 4) == "0123") {
            return 1;
        }
        if (substr($s, 0, 4) == "0124") {
            return 1;
        }
        if (substr($s, 0, 4) == "0125") {
            return 1;
        }
        if (substr($s, 0, 4) == "0127") {
            return 1;
        }
        if (substr($s, 0, 4) == "0129") {
            return 1;
        }
        if (substr($s, 0, 3) == "088") {
            return 1;
        }

        if (substr($s, 0, 4) == "8491") {
            return 1;
        }
        if (substr($s, 0, 4) == "8494") {
            return 1;
        }
        if (substr($s, 0, 5) == "84123") {
            return 1;
        }
        if (substr($s, 0, 5) == "84124") {
            return 1;
        }
        if (substr($s, 0, 5) == "84125") {
            return 1;
        }
        if (substr($s, 0, 5) == "84127") {
            return 1;
        }
        if (substr($s, 0, 5) == "84129") {
            return 1;
        }
        if (substr($s, 0, 4) == "8488") {
            return 1;
        }

        if (substr($s, 0, 2) == "91") {
            return 1;
        }
        if (substr($s, 0, 2) == "94") {
            return 1;
        }
        if (substr($s, 0, 3) == "123") {
            return 1;
        }
        if (substr($s, 0, 3) == "124") {
            return 1;
        }
        if (substr($s, 0, 3) == "125") {
            return 1;
        }
        if (substr($s, 0, 3) == "127") {
            return 1;
        }
        if (substr($s, 0, 3) == "129") {
            return 1;
        }
        if (substr($s, 0, 2) == "88") {
            return 1;
        }

        if (substr($s, 0, 3) == "083") {
            return 1;
        }
        if (substr($s, 0, 3) == "084") {
            return 1;
        }
        if (substr($s, 0, 3) == "085") {
            return 1;
        }
        if (substr($s, 0, 3) == "081") {
            return 1;
        }
        if (substr($s, 0, 3) == "082") {
            return 1;
        }

        if (substr($s, 0, 2) == "83") {
            return 1;
        }
        if (substr($s, 0, 2) == "84") {
            return 1;
        }
        if (substr($s, 0, 2) == "85") {
            return 1;
        }
        if (substr($s, 0, 2) == "81") {
            return 1;
        }
        if (substr($s, 0, 2) == "82") {
            return 1;
        }

        if (substr($s, 0, 4) == "8483") {
            return 1;
        }
        if (substr($s, 0, 4) == "8484") {
            return 1;
        }
        if (substr($s, 0, 4) == "8485") {
            return 1;
        }
        if (substr($s, 0, 4) == "8481") {
            return 1;
        }
        if (substr($s, 0, 4) == "8482") {
            return 1;
        }

        return 0;
    }

    public static function isMobi($s)
    {
        if (substr($s, 0, 3) == "090") {
            return 2;
        }
        if (substr($s, 0, 3) == "093") {
            return 2;
        }
        if (substr($s, 0, 4) == "0120") {
            return 2;
        }
        if (substr($s, 0, 4) == "0121") {
            return 2;
        }
        if (substr($s, 0, 4) == "0122") {
            return 2;
        }
        if (substr($s, 0, 4) == "0126") {
            return 2;
        }
        if (substr($s, 0, 4) == "0128") {
            return 2;
        }
        if (substr($s, 0, 3) == "089") {
            return 2;
        }

        if (substr($s, 0, 4) == "8490") {
            return 2;
        }
        if (substr($s, 0, 4) == "8493") {
            return 2;
        }
        if (substr($s, 0, 5) == "84120") {
            return 2;
        }
        if (substr($s, 0, 5) == "84121") {
            return 2;
        }
        if (substr($s, 0, 5) == "84122") {
            return 2;
        }
        if (substr($s, 0, 5) == "84126") {
            return 2;
        }
        if (substr($s, 0, 5) == "84128") {
            return 2;
        }
        if (substr($s, 0, 4) == "8489") {
            return 2;
        }

        if (substr($s, 0, 2) == "90") {
            return 2;
        }
        if (substr($s, 0, 2) == "93") {
            return 2;
        }
        if (substr($s, 0, 3) == "120") {
            return 2;
        }
        if (substr($s, 0, 3) == "121") {
            return 2;
        }
        if (substr($s, 0, 3) == "122") {
            return 2;
        }
        if (substr($s, 0, 3) == "126") {
            return 2;
        }
        if (substr($s, 0, 3) == "128") {
            return 2;
        }
        if (substr($s, 0, 2) == "89") {
            return 2;
        }

        if (substr($s, 0, 3) == "070") {
            return 2;
        }
        if (substr($s, 0, 3) == "076") {
            return 2;
        }
        if (substr($s, 0, 3) == "077") {
            return 2;
        }
        if (substr($s, 0, 3) == "078") {
            return 2;
        }
        if (substr($s, 0, 3) == "079") {
            return 2;
        }

        if (substr($s, 0, 4) == "8470") {
            return 2;
        }
        if (substr($s, 0, 4) == "8476") {
            return 2;
        }
        if (substr($s, 0, 4) == "8477") {
            return 2;
        }
        if (substr($s, 0, 4) == "8478") {
            return 2;
        }
        if (substr($s, 0, 4) == "8479") {
            return 2;
        }

        if (substr($s, 0, 2) == "70") {
            return 2;
        }
        if (substr($s, 0, 2) == "76") {
            return 2;
        }
        if (substr($s, 0, 2) == "77") {
            return 2;
        }
        if (substr($s, 0, 2) == "78") {
            return 2;
        }
        if (substr($s, 0, 2) == "79") {
            return 2;
        }
        return 0;
    }

    public static function isViettel($s)
    {
        if (substr($s, 0, 3) == "096") {
            return -1;
        }
        if (substr($s, 0, 3) == "097") {
            return -1;
        }
        if (substr($s, 0, 3) == "098") {
            return -1;
        }
        if (substr($s, 0, 3) == "016") {
            return -1;
        }
        if (substr($s, 0, 3) == "086") {
            return -1;
        }

        if (substr($s, 0, 4) == "8496") {
            return -1;
        }
        if (substr($s, 0, 4) == "8497") {
            return -1;
        }
        if (substr($s, 0, 4) == "8498") {
            return -1;
        }
        if (substr($s, 0, 4) == "8416") {
            return -1;
        }
        if (substr($s, 0, 4) == "8486") {
            return -1;
        }

        if (substr($s, 0, 2) == "96") {
            return -1;
        }
        if (substr($s, 0, 2) == "97") {
            return -1;
        }
        if (substr($s, 0, 2) == "98") {
            return -1;
        }
        if (substr($s, 0, 2) == "16") {
            return -1;
        }
        if (substr($s, 0, 2) == "86") {
            return -1;
        }

        if (substr($s, 0, 4) == "8432") {
            return -1;
        }
        if (substr($s, 0, 4) == "8433") {
            return -1;
        }
        if (substr($s, 0, 4) == "8434") {
            return -1;
        }
        if (substr($s, 0, 4) == "8435") {
            return -1;
        }
        if (substr($s, 0, 4) == "8436") {
            return -1;
        }
        if (substr($s, 0, 4) == "8437") {
            return -1;
        }
        if (substr($s, 0, 4) == "8438") {
            return -1;
        }
        if (substr($s, 0, 4) == "8439") {
            return -1;
        }

        if (substr($s, 0, 3) == "032") {
            return -1;
        }
        if (substr($s, 0, 3) == "033") {
            return -1;
        }
        if (substr($s, 0, 3) == "034") {
            return -1;
        }
        if (substr($s, 0, 3) == "035") {
            return -1;
        }
        if (substr($s, 0, 3) == "036") {
            return -1;
        }
        if (substr($s, 0, 3) == "037") {
            return -1;
        }
        if (substr($s, 0, 3) == "038") {
            return -1;
        }
        if (substr($s, 0, 3) == "039") {
            return -1;
        }

        if (substr($s, 0, 2) == "32") {
            return -1;
        }
        if (substr($s, 0, 2) == "33") {
            return -1;
        }
        if (substr($s, 0, 2) == "34") {
            return -1;
        }
        if (substr($s, 0, 2) == "35") {
            return -1;
        }
        if (substr($s, 0, 2) == "36") {
            return -1;
        }
        if (substr($s, 0, 2) == "37") {
            return -1;
        }
        if (substr($s, 0, 2) == "38") {
            return -1;
        }
        if (substr($s, 0, 2) == "39") {
            return -1;
        }

        return 0;
    }

    public static function isVinaOrMobi($s)
    {
        if (self::isVina($s)) {
            return 1;
        } elseif (self::isMobi($s)) {
            return 2;
        } else {
            return 0;
        }
    }

    public static function getMang($s)
    {
        if (self::isVina($s)) {
            return 1;
        } elseif (self::isMobi($s)) {
            return 2;
        } elseif (self::isViettel($s)) {
            return -1;
        } else {
            return 0;
        }
    }

    public static function unicodeChuan($str)
    {
        if (!$str) {
            return $str;
        }
        $a = array(
            'á' => 'á',
            'à' => 'à',
            'ạ' => 'ạ',
            'ả' => 'ả',
            'ã' => 'ã',
            'ấ' => 'ấ',
            'ầ' => 'ầ',
            'ậ' => 'ậ',
            'ẩ' => 'ẩ',
            'ẫ' => 'ẫ',
            'ắ' => 'ắ',
            'ằ' => 'ằ',
            'ặ' => 'ặ',
            'ẳ' => 'ẳ',
            'ẵ' => 'ẵ',
            'Á' => 'Á',
            'À' => 'À',
            'Ạ' => 'Ạ',
            'Ả' => 'Ả',
            'Ã' => 'Ã',
            'Ấ' => 'Ấ',
            'Ầ' => 'Ầ',
            'Ậ' => 'Ậ',
            'Ẩ' => 'Ẩ',
            'Ẫ' => 'Ẫ',
            'Ắ' => 'Ắ',
            'Ằ' => 'Ằ',
            'Ặ' => 'Ặ',
            'Ẳ' => 'Ẳ',
            'Ẵ' => 'Ẵ',
            'é' => 'é',
            'è' => 'è',
            'ẹ' => 'ẹ',
            'ẻ' => 'ẻ',
            'ẽ' => 'ẽ',
            'ế' => 'ế',
            'ề' => 'ề',
            'ệ' => 'ệ',
            'ể' => 'ể',
            'ễ' => 'ễ',
            'É' => 'É',
            'È' => 'È',
            'Ẹ' => 'Ẹ',
            'Ẻ' => 'Ẻ',
            'Ẽ' => 'Ẽ',
            'Ế' => 'Ế',
            'Ề' => 'Ề',
            'Ệ' => 'Ệ',
            'Ể' => 'Ể',
            'Ễ' => 'Ễ',
            'ó' => 'ó',
            'ò' => 'ò',
            'ọ' => 'ọ',
            'ỏ' => 'ỏ',
            'õ' => 'õ',
            'ố' => 'ố',
            'ồ' => 'ồ',
            'ộ' => 'ộ',
            'ổ' => 'ổ',
            'ỗ' => 'ỗ',
            'ớ' => 'ớ',
            'ờ' => 'ờ',
            'ợ' => 'ợ',
            'ở' => 'ở',
            'ỡ' => 'ỡ',
            'Ó' => 'Ó',
            'Ò' => 'Ò',
            'Ọ' => 'Ọ',
            'Ỏ' => 'Ỏ',
            'Õ' => 'Õ',
            'Ố' => 'Ố',
            'Ồ' => 'Ồ',
            'Ộ' => 'Ộ',
            'Ổ' => 'Ổ',
            'Ỗ' => 'Ỗ',
            'Ớ' => 'Ớ',
            'Ờ' => 'Ờ',
            'Ợ' => 'Ợ',
            'Ở' => 'Ở',
            'Ỡ' => 'Ỡ',
            'ú' => 'ú',
            'ù' => 'ù',
            'ụ' => 'ụ',
            'ủ' => 'ủ',
            'ũ' => 'ũ',
            'ứ' => 'ứ',
            'ừ' => 'ừ',
            'ự' => 'ự',
            'ử' => 'ử',
            'ữ' => 'ữ',
            'Ú' => 'Ú',
            'Ù' => 'Ù',
            'Ụ' => 'Ụ',
            'Ủ' => 'Ủ',
            'Ũ' => 'Ũ',
            'Ứ' => 'Ứ',
            'Ừ' => 'Ừ',
            'Ự' => 'Ự',
            'Ử' => 'Ử',
            'Ữ' => 'Ữ',
            'í' => 'í',
            'ì' => 'ì',
            'ị' => 'ị',
            'ỉ' => 'ỉ',
            'ĩ' => 'ĩ',
            'Í' => 'Í',
            'Ì' => 'Ì',
            'Ị' => 'Ị',
            'Ỉ' => 'Ỉ',
            'Ĩ' => 'Ĩ',
            'ý' => 'ý',
            'ỳ' => 'ỳ',
            'ỵ' => 'ỵ',
            'ỷ' => 'ỷ',
            'ỹ' => 'ỹ',
            'Ý' => 'Ý',
            'Ỳ' => 'Ỳ',
            'Ỵ' => 'Ỵ',
            'Ỷ' => 'Ỷ',
            'Ỹ' => 'Ỹ');
        return strtr($str, $a);
    }

    public static function unsign($str, $style = 0)
    {
        $str = self::unicodeChuan($str);
        $VietNamChar = array(
            "aAeEoOuUiIdDyY",
            "á-à-ạ-ả-ã-â-ấ-ầ-ậ-ẩ-ẫ-ă-ắ-ằ-ặ-ẳ-ẵ",
            "Á-À-Ạ-Ả-Ã-Â-Ấ-Ầ-Ậ-Ẩ-Ẫ-Ă-Ắ-Ằ-Ặ-Ẳ-Ẵ",
            "é-è-ẹ-ẻ-ẽ-ê-ế-ề-ệ-ể-ễ",
            "É-È-Ẹ-Ẻ-Ẽ-Ê-Ế-Ề-Ệ-Ể-Ễ",
            "ó-ò-ọ-ỏ-õ-ô-ố-ồ-ộ-ổ-ỗ-ơ-ớ-ờ-ợ-ở-ỡ",
            "Ó-Ò-Ọ-Ỏ-Õ-Ô-Ố-Ồ-Ộ-Ổ-Ỗ-Ơ-Ớ-Ờ-Ợ-Ở-Ỡ",
            "ú-ù-ụ-ủ-ũ-ư-ứ-ừ-ự-ử-ữ",
            "Ú-Ù-Ụ-Ủ-Ũ-Ư-Ứ-Ừ-Ự-Ử-Ữ",
            "í-ì-ị-ỉ-ĩ",
            "Í-Ì-Ị-Ỉ-Ĩ",
            "đ",
            "Đ",
            "ý-ỳ-ỵ-ỷ-ỹ",
            "Ý-Ỳ-Ỵ-Ỷ-Ỹ"
        );

        for ($i = 1; $i < count($VietNamChar); $i++) {
            $tmp = explode("-", $VietNamChar[$i]);

            for ($j = 0; $j < count($tmp); $j++) {
                $str = str_replace($tmp[$j], $VietNamChar[0][$i - 1], $str);
            }
        };

        $aChar = array(
            'Š' => 'S', 'š' => 's', 'Ð' => 'Dj', 'Ž' => 'Z', 'ž' => 'z',
            'À' => 'A', 'Á' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
            'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E',
            'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', 'Í' => 'I', 'Î' => 'I',
            'Ï' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O',
            'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
            'Û' => 'U', 'Ü' => 'U', 'Ý' => 'Y', 'Þ' => 'B', 'ß' => 'Ss',
            'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
            'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e',
            'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
            'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o',
            'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
            'ú' => 'u', 'û' => 'u', /*'ý' => 'y', */
            'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f'
        );

        $str = strtr($str, $aChar);

        if ($style == 1) {
            $str = str_replace('#', '', $str);
            $str = str_replace('?', '', $str);
            $str = str_replace('%', '', $str);
            $str = str_replace('~', '', $str);
            $str = str_replace('&', '', $str);
            $str = trim(preg_replace('/[^\w\d_ -]/si', '', $str));//remove all illegal chars
            $str = str_replace(' ', '-', $str);

            while (strpos($str, "--") !== false) {
                $str = str_replace('--', '-', $str);
            }

            $str = trim($str, "-");
        }

        $str = str_replace("́", "", $str);
        $str = str_replace("̀", "", $str);
        $str = str_replace("̣", "", $str);
        $str = str_replace("̃", "", $str);
        $str = str_replace("̉", "", $str);

        return $str;
    }

    public static function chuanHoa($sMess)
    {
        $sAllow = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ()[].,'\";:-_+=/\\!~`#\$%&*@|{}? \n";
        $sMess = self::unsign($sMess);
        while (strpos($sMess, "  ") !== false) {
            $sMess = str_replace("  ", " ", $sMess);
        }
        $sMess = trim($sMess);

//        $tmp ="";
//
//        for ($i=0; $i<=strlen($sMess); ++$i)
//        {
//            if(!empty(substr($sMess, $i, 1)) && (strpos($sAllow, substr($sMess, $i, 1)) !== false)){
//                $tmp .= substr($sMess, $i, 1);
//            }
//        }
//        $sMess = trim($tmp);
        return $sMess;
    }

    public static function getMessLength($mess, $num = 160)
    {
        if ($num <= 0) {
            $num = 1;
        }

        $mess = trim($mess);

        $lenMess = mb_strlen($mess);
        if ($lenMess <= 0) {
            return 0;
        }
        if ($lenMess <= $num) {
            return 1;
        }
        if ($lenMess <= 306 && $lenMess > 160) {
            return 2;
        }
        if ($lenMess <= 459 && $lenMess > 306) {
            return 3;
        }
        return 4;
    }

    public static function getPhone($phone)
    {
        $s = trim($phone);
        if (strlen($s) > 10 && substr($s, 0, 2) == "84") {
            $s = "0" . substr($s, 2, strlen($s) - 2);
        }
        if (substr($s, 0, 1) != "0") {
            $s = "0" . $s;
        }
        if (strlen($s) > 13 || strlen($s) < 10) {
            return '';
        }
        return "84" . substr($s, 1, strlen($s) - 1);
    }

    /*public function sendByViettelWS($aDataToSend){
        try {
            $requestId = $aDataToSend["requestId"];//Trả về một chuỗi 32 ký tự
            $proxy = new SoapClient(PHPVIET_VIETTEL_SMS_WS, array('cache_wsdl' => WSDL_CACHE_NONE));
            $aParams = array(
                "User"=>"abc",//Được lấy từ hệ thống SMSBRN
                "Password"=>"123456a@1",//Được lấy từ hệ thống SMSBRN
                "CPCode"=>"ABC",//Được lấy từ hệ thống SMSBRN
                "RequestID"=>"{$requestId}",//Cái này là gì?Truyền bằng bao nhiêu?
                Tại sao trong Phần mô tả ghi mặc định là MO, từ MT 2 trở lên = 4, trong VD lại là 1?
                "UserID"=>"{$aDataToSend["mobilelist"][0]}",//Số gửi là số nào?
                 Tại sao trong VD, số gửi và nhận lại giống nhau
                "ReceiverID"=>"{$aDataToSend["mobilelist"][0]}",
                "ServiceID"=>"ABC",//Được lấy từ hệ thống SMSBRN
                "CommandCode"=>"bulksms",
                "Content"=>"{$aDataToSend["params"][0]}",
                "ContentType"=>"0"
            );

            $result = json_decode(json_encode($proxy->wsCpMt($aParams)),true);
            return $result;
        }
        catch (Exception $ex) {
            return array();
        }
    }*/
}
