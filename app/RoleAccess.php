<?php

namespace App;

final class RoleAccess
{
    const PERSONAL = "PERSONAL"; //Cá nhân
    const BUSINESS = 'BUSINESS'; //Cơ sở SXKD
    const GUEST = 'GUEST'; //Chưa đăng nhập
}
