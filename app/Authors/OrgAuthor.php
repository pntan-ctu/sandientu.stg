<?php
namespace App\Authors;

use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\Organization;
use App\Authors\ManageOrganizationAuthor;

class OrgAuthor
{
    protected $manageOrganizationAuthor;

    public function __construct()
    {
        $this->manageOrganizationAuthor = new ManageOrganizationAuthor();
    }

    //Check quyền trả vể true/false
    public function canDo(Organization $organization, string $abilityName)
    {
        if (isUserBusiness()
            && $organization->id == getOrgId()
            && Gate::any(['*', AbilityName::ORG_OWNE, $abilityName], '*')) {
            return true;
        }
        if (isUserGov()
            && $this->manageOrganizationAuthor->canManage($organization)
            && ($abilityName == AbilityName::ORG_INFO)) {
            return true;
        }
        return false;
    }

    //Check quyền thực hiện các chức năng quản lý: Toàn quyền hoặc thêm nhóm quyền $abilityName
    public function canManageObj(Organization $organization, string $abilityName)
    {
        if ($this->canDo($organization, $abilityName)) {
            return true;
        }
        throw new AuthorizationException();
    }

    //Check quyền như trên nhưng theo id
    public function canManageId(int $organizationId, string $abilityName)
    {
        $organization = Organization::find($organizationId);

        return $this->canManageObj($organization, $abilityName);
    }

    //Check quyền vào trang chủ cơ sở (dashboard)
    public function canDashboard(Organization $organization)
    {
        if (isUserBusiness()
            && ($organization->id == getOrgId())
            && Gate::any(['*', AbilityName::ORG_OWNE,
                AbilityName::ORG_EXCHANGE,
                AbilityName::ORG_MANAGE,
                AbilityName::ORG_INFO], '*')) {
            return true;
        }
        if (isUserGov() && $this->manageOrganizationAuthor->canManage($organization)) {
            return true;
        }
        throw new AuthorizationException();
    }
}
