<?php
namespace App\Authors;

use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\Advertising;

class AdvertisingAuthor
{
    /**
     * Authorize action view for the current Advertising.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_CENSORSHIP_ADVERTISING],
            Advertising::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }

    /**
     * Authorize action manage for the current Advertising against argument $Advertising.
     *
     * @param Advertising $Advertising
     * @return bool
     * @throws AuthorizationException
     */
    public function canManage(Advertising $advertising)
    {
        // $regionId = getRegionId();
        if (!Gate::allows('*', Advertising::class)) {
            // if (!(Gate::allows(AbilityName::MANAGE_CENSORSHIP_ADVERTISING, Advertising::class)
            //     && $advertising->region_id == $regionId
            // ))
            if (!Gate::allows(AbilityName::MANAGE_CENSORSHIP_ADVERTISING, Advertising::class)
                || strrpos($advertising->region->path_primary_key, getOrgModel()->region->path_primary_key) === false
            ) {
                throw new AuthorizationException();
            }
        }
        return true;
    }
}
