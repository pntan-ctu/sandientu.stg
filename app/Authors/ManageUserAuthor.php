<?php
namespace App\Authors;

use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\User;

class ManageUserAuthor
{
    /**
     * Authorize action view for the current user.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (Gate::any(
            [
                '*',
                AbilityName::MANAGE_USER_INTERNAL_GOV,
                AbilityName::MANAGE_USER_CHILD_GOV,
                AbilityName::MANAGE_USER_MEMBER],
            User::class
        )) {
            return true;
        }

        throw new AuthorizationException();
    }

    /**
     * Authorize action manage for the current user against argument $user.
     *
     * @param User $user
     * @return bool
     * @throws AuthorizationException
     */
    public function canManage(User $user)
    {
        if (Gate::allows('*', User::class)) {
            return true;
        }

        if (Gate::allows(AbilityName::MANAGE_USER_INTERNAL_GOV, User::class)
            && $user->organization_id === getOrgId()
            || Gate::allows(AbilityName::MANAGE_USER_CHILD_GOV, User::class)
            && strpos($user->organization->path_primary_key, getOrgModel()->path_primary_key . '/') !== false
            || Gate::allows(AbilityName::MANAGE_USER_MEMBER, User::class)
            && $user->organization->organization_type_id === OrgType::MEMBER) {
            return true;
        }
        
        throw new AuthorizationException();
    }
}
