<?php
namespace App\Authors;

use App\Models\DocumentOrgan;
use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\DocumentType;

class DocumentOrganAuthor
{
    /**
     * Authorize action view for the current user.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_LIST_DOCUMENT_ORGAN],
            DocumentOrgan::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }
}
