<?php
namespace App\Authors;

use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\Area;

class AreaAuthor
{
    /**
     * Authorize action view for the current Area.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_AREA],
            Area::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }

    /**
     * Authorize action manage for the current Area against argument $Area.
     *
     * @param Area $Area
     * @return bool
     * @throws AuthorizationException
     */
    public function canManage(Area $area)
    {
        $currentOrgId = getOrgId();
        if (!Gate::allows('*', Area::class)) {
            if (!(
                Gate::allows(AbilityName::MANAGE_AREA, Area::class)
                && $area->manage_organization_id == $currentOrgId
            )) {
                throw new AuthorizationException();
            }
        }
        return true;
    }
}
