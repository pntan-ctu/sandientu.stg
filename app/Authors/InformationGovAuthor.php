<?php
namespace App\Authors;

use App\Models\Album;
use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class InformationGovAuthor
{
    /**
     * Authorize action view for the current user.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(['*', AbilityName::MANAGE_GOV_INFO], '*')) {
            throw new AuthorizationException();
        }
        return true;
    }
}
