<?php
namespace App\Authors;

use App\Models\Document;
use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class DocumentAuthor
{
    /**
     * Authorize action view for the current user.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_DOCUMENT],
            Document::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }
}
