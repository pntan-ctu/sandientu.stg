<?php
namespace App\Authors;

class AbilityName
{
    /**
     * Toàn quyền
     * Check quyền ability *,*, dùng cho site Quản trị hệ thống
     */

    /**
     * Quản lý vùng sản xuất, địa điểm kinh doanh
     */
    const MANAGE_AREA = 'manage_area'; //Quản lý vùng sản xuất
    const MANAGE_COMMERCIAL_CENTER = 'manage_commercial_center'; //Quản lý vùng sản xuất

    /**
     * Quyền quản lý cơ sở:
     * Có quyền này kèm với quyền kiểm duyệt đánh giá, hỏi đáp, xử lý cơ sở và
     * sản phẩm của cơ sở đó vi phạm
     */
    const MANAGE_ORG_GOV = 'manage_org_gov'; //QL tất cả cơ sở thuộc cấp CQQL được phân công
    const MANAGE_ORG_GOV_HEALTH = 'manage_org_gov_health'; //Như trên nhưng thuộc lĩnh vực YT
    const MANAGE_ORG_GOV_AGRICULTURE = 'manage_org_gov_agriculture'; //Như trên nhưng thuộc lĩnh vực NN
    const MANAGE_ORG_GOV_INDUSTRY_AND_TRADE = 'manage_org_gov_industry_and_trade'; //Như trên nhưng thuộc lĩnh vực CT

    /**
     * Kiểm duyệt tin cung cầu
     */
    const MANAGE_CENSORSHIP_ADVERTISING = 'manage_censorship_advertising'; //Duyệt tin cung cầu

    /**
     * Xử lý thông tin phản ánh thành viên vi phạm và thông tin phản hồi
     */
    const MANAGE_VERIFY_MEMBER_INFRINGMENT = 'manage_verify_member_infringement'; //Xử lý phản ánh thành viên vi phạm
    const MANAGE_VERIFY_CONTACT = 'manage_verify_contact'; //Xử lý thông tin phản hồi

    /**
     * Thống kê báo cáo
     */
    const MANAGE_REPORT_ALL = 'manage_report_all'; //Trên dữ liệu toàn hệ thống
    const MANAGE_REPORT_SCOPE = 'manage_report_scope'; //Trên dữ liệu thuộc phạm vi quản lý

    /**
     * Quản lý thông tin hữu ích
     */
    const MANAGE_NEWS_ARTICLE = 'manage_news_article'; //Tin tức ATTP
    const MANAGE_DOCUMENT = 'manage_document'; //Văn bản về ATTP
    const MANAGE_ALBUM = 'manage_album'; //Thư viện ảnh
    const MANAGE_VIDEO = 'manage_video'; //Thư viện video

    /**
     * Tiện ích web
     */
    const MANAGE_SLIDE = 'manage_slide'; //Quản lí slide ảnh
    const MANAGE_WEBLINK = 'manage_weblink'; //Quản lí liên kết web
    const MANAGE_POLL = 'manage_poll'; //Quản lí thăm dò ý kiến
    const MANAGE_HELP = 'manage_help'; //Quản lí thăm dò ý kiến

    /**
     * Nhắn tin SMS
     */
    const MANAGE_SMS_SEND = 'manage_sms_send'; //Gửi tin nhắn
    const MANAGE_SMS_HISTORY = 'manage_sms_history'; //Xem lịch sử nhắn tin

    /**
     * Quản lý danh mục dùng chung
     */
    const MANAGE_LIST_PRODUCT_CATEGORY = 'manage_list_product_category'; //Loại sản phảm
    const MANAGE_LIST_CERTIFICATE_CATEGORY = 'manage_list_certificate_category'; //Loại chứng chỉ
    const MANAGE_LIST_NEWS_GROUP = 'manage_list_news_group'; //Chuyên mục tin tức
    const MANAGE_LIST_DOCUMENT_FIELD = 'manage_list_document_field'; //Lĩnh vực văn bản
    const MANAGE_LIST_DOCUMENT_TYPE = 'manage_list_document_type'; //Loại văn bản
    const MANAGE_LIST_DOCUMENT_ORGAN = 'manage_list_document_organ'; //Cơ quan ban hành VB

    /**
     * Quản lý người dùng
     */
    const MANAGE_USER_INTERNAL_GOV = 'manage_user_internal_gov'; //Cán bộ nội bộ của CQQL
    const MANAGE_USER_CHILD_GOV = 'manage_user_child_gov'; //Cán bộ của các cơ quan cấp dưới
    const MANAGE_USER_MEMBER = 'manage_user_member'; //Thành viên đăng ký cá nhân

    /**
     * Quản lý danh mục dùng chung
     */
    const MANAGE_GOV_INFO = 'manage_gov_info'; //Cập nhật thông tin tổ chức

    /**
     * ========================== CƠ SỞ ================
     * Quản lý thông tin cơ sở
     */
    const ORG_OWNE = 'org_owne'; //Chủ cơ sở
    const ORG_EXCHANGE = 'org_exchange'; //Giao dịch
    const ORG_MANAGE = 'org_manage'; //Quản lý
    const ORG_INFO = 'org_info'; //Thông tin cơ sở
}
