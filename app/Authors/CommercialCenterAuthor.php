<?php
namespace App\Authors;

use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\CommercialCenter;

class CommercialCenterAuthor
{
    /**
     * Authorize action view for the current CommercialCenter.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_COMMERCIAL_CENTER],
            CommercialCenter::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }

    /**
     * Authorize action manage for the current CommercialCenter against argument $CommercialCenter.
     *
     * @param CommercialCenter $CommercialCenter
     * @return bool
     * @throws AuthorizationException
     */
    public function canManage(CommercialCenter $commercialCenter)
    {
        $currentOrgId = getOrgId();
        if (!Gate::allows('*', CommercialCenter::class)) {
            if (!(
                Gate::allows(AbilityName::MANAGE_COMMERCIAL_CENTER, CommercialCenter::class)
                && $commercialCenter->manage_organization_id == $currentOrgId
            )) {
                throw new AuthorizationException();
            }
        }
        return true;
    }
}
