<?php
namespace App\Authors;

use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\Organization;

class ManageOrganizationAuthor
{
    /**
     * Authorize action view for the current Organization.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (Gate::any(
            [
                '*',
                AbilityName::MANAGE_ORG_GOV,
                AbilityName::MANAGE_ORG_GOV_HEALTH,
                AbilityName::MANAGE_ORG_GOV_AGRICULTURE,
                AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE],
            Organization::class
        )) {
            return true;
        }

        throw new AuthorizationException();
    }

    /**
     * Authorize action manage for the current Organization against argument $Organization.
     *
     * @param Organization $Organization
     * @return bool
     * @throws AuthorizationException
     */
    public function canManage(Organization $organization)
    {
        $currentOrgId = getOrgId();
        
        if (Gate::allows('*', Organization::class)) {
            return true;
        }
        
        if ((Gate::allows(AbilityName::MANAGE_ORG_GOV, Organization::class)
                || Gate::allows(AbilityName::MANAGE_ORG_GOV_HEALTH, Organization::class)
                && $organization->department_manage_id == 1
                || Gate::allows(AbilityName::MANAGE_ORG_GOV_AGRICULTURE, Organization::class)
                && $organization->department_manage_id == 2
                || Gate::allows(AbilityName::MANAGE_ORG_GOV_INDUSTRY_AND_TRADE, Organization::class)
                && $organization->department_manage_id == 3)
            && ($organization->manage_organization_id == $currentOrgId
                || $organization->move_organization_id == $currentOrgId)) {
            return true;
        }

        throw new AuthorizationException();
    }
}
