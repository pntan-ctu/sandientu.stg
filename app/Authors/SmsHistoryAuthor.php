<?php
namespace App\Authors;

use App\Models\SmsHistory;
use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class SmsHistoryAuthor
{
    /**
     * Authorize action view for the current user.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_SMS_SEND],
            SmsHistory::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }

    public function canDoHis()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_SMS_HISTORY],
            SmsHistory::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }
}
