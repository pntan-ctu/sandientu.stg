<?php
namespace App\Authors;

use App\Models\CertificateCategory;
use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;

class CertificateCategoryAuthor
{
    /**
     * Authorize action view for the current user.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_LIST_CERTIFICATE_CATEGORY],
            CertificateCategory::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }
}
