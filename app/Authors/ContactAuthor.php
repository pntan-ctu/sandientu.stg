<?php
namespace App\Authors;

use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\Contact;

class ContactAuthor
{
    /**
     * Authorize action view for the current Contact.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_VERIFY_CONTACT],
            Contact::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }

    /**
     * Authorize action manage for the current Contact against argument $Contact.
     *
     * @param Contact $Contact
     * @return bool
     * @throws AuthorizationException
     */
    public function canManage(Contact $contact)
    {
        $currentOrgId = getOrgId();
        if (!Gate::allows('*', Contact::class)) {
            if (!(Gate::allows(AbilityName::MANAGE_VERIFY_CONTACT, Contact::class)
                && $contact->organization_id == $currentOrgId
            )) {
                throw new AuthorizationException();
            }
        }
        return true;
    }
}
