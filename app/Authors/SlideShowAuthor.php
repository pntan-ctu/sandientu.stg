<?php
namespace App\Authors;

use App\Models\SlideShow;
use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Models\NewsGroup;

class SlideShowAuthor
{
    /**
     * Authorize action view for the current user.
     *
     * @return bool
     *
     * @throws \Illuminate\Auth\Access\AuthorizationException
     */
    public function canDo()
    {
        if (!Gate::any(
            ['*', AbilityName::MANAGE_SLIDE],
            SlideShow::class
        )) {
            throw new AuthorizationException();
        }
        return true;
    }
}
