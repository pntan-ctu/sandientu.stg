<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 11/1/18
 * Time: 21:46
 */

namespace App;

final class Constants
{
    //Cho phép public thông tin cơ sở, SP khi chưa duyệt
    const ALLOW_PUBLIC_ORG_PRODUCT = false;

    //Cho phép public đánh giá, hỏi đáp khi chưa duyệt
    const ALLOW_PUBLIC_RATING_FQA = true;

    //Nhóm tin bài cố định cho phần footer (Chỉ được sửa)
    const PREFIX_NEWS_GROUP_ID = 1;

    //Quyền chủ cơ sở sản xuất, dùng gán khi đăng ký cơ sở sản xuất
    const ROLE_CHU_CSSX = 'chu-cssx';

    //Quyền chủ cơ sở kinh doanh, dùng gán khi đăng ký cơ sở kinh doanh
    const ROLE_CHU_CSKD = 'chu-cskd';

    //Loại giấy tờ chứng thực đăng ký cơ sở
    const FOUNDING_TYPE = [
        1 => 'Giấy cam kết sản xuất TPAT',
        2 => 'Giấy chứng nhận cơ sở đủ ĐK ATTP',
        3 => 'Giấy xác nhận nguồn gốc xuất xứ',
        4 => 'Giấy phép đăng ký kinh doanh'
    ];

    //Trạng thái cơ sở
    const ORG_STATUS = [
        0 => 'Chưa kiểm duyệt',
        1 => 'Đã kiểm duyệt',
        2 => 'Ngừng hoạt động',
        3 => 'Khóa do vi phạm',
    ];

    //Trạng thái thông tin sản phẩm
    const PRODUCT_STATUS = [
        0 => 'Chưa kinh doanh',
        1 => 'Còn hàng',
        2 => 'Theo mùa vụ',
        3 => 'Hết hàng',
        4 => 'Ngừng kinh doanh',
    ];

    //Trạng thái kiểm duyệt sản phẩm
    const PRODUCT_ACTIVE = [
        '0' => 'Chưa kiểm duyệt',
        '1' => 'Đã kiểm duyệt',
        '2' => 'Khóa do vi phạm',
    ];

    //Trạng thái đơn hàng
    const ORDER_STATUS = [
        1 => 'Chưa xử lý',
        2 => 'Đã xử lý',
        3 => 'Đã hủy đơn',
        4 => 'Đã nhận hàng',
    ];

    //Nhóm weblink
    const WEBLINK_GROUPS = [
        '1' => 'Banner quảng cáo',
        '2' => 'Thương hiệu đồng hành',
    ];

    // id của phương thức thanh toán VNPTPAY trong bảng pay_types
    const PAY_TYPE_VNPTPAY = 3;
    const VNPTPAY_API_VERSION = "1.0.5";
    const VNPTPAY_URL_BASE = "http://sandbox.vnptpay.vn/rest/payment/v1.0.5/init";
    const VNPTPAY_MERCHANT_SERVICE_ID = 622;

    // Vận chuyển
    const VNPOST_BASE_API_URL = "https://hcconline.vnpost.vn/testv2live/serviceApi/v1/";
    const VNPOST_NORMAL_PETROL_FEE = 0.1; // phụ phí xăng dầu chuyển phát thường hoặc Logistics Eco = 10% cước chính
    const VNPOST_FAST_PETROL_FEE = 0.15; // phụ phí xăng dầu chuyển phát nhanh = 15% cước chính
    /*
     * Phụ phí vùng xa được áp dụng trong 2 trường hợp:
     * - Đầu đến của chuyển phát thường hoặc Logistics Eco là vùng xa
     * - Một trong 2 đầu của chuyển phát nhanh là vùng xa
     */
    const VNPOST_FAR_AREA_FEE = 0.2; // phụ phí vùng xa = 20% cước chính


    const PAY_TYPE_MOMO = 4;
    const MOMO_PARTNER_CODE = "MOMO5DXL20200217";
    const MOMO_ACCESS_KEY = "j10IipKLkZArcYgr";
    const MOMO_SECRET_KEY = "Q6DNfTyPT71Y19BV8z9gndlQ4TejAeIL";
    const MOMO_URL = "https://test-payment.momo.vn/gw_payment/transactionProcessor";
    const returnUrl = "http://bds.vnpttiengiang.vn/get-momo-notify";
    const notifyUrl ="http://bds.vnpttiengiang.vn/get-momo-notify";

    const region = 60;
}
