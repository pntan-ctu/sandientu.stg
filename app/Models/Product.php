<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Visits;
use App\Models\Traits\Statistic;

/**
 * Class Product
 *
 * @property int $id
 * @property string $path
 * @property int $organization_id
 * @property int $product_category_id
 * @property string $code
 * @property string $name
 * @property string $unit
 * @property float $price
 * @property float $price_sale
 * @property string $avatar_image
 * @property int $status
 * @property string $duration
 * @property string $barcode
 * @property bool $has_check
 * @property string check_temp_image
 * @property string $capacity
 * @property string $pack_style
 * @property string $preservation_method
 * @property int $count_view
 * @property float $avg_rate
 * @property int $count_rate
 * @property int $count_order
 * @property float $rank
 * @property int $active
 * @property int $weight_per_unit
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property ProductCategory $productCategory
 * @property Organization $organization
 * @property Collection|OrderItem[] $orderItems
 * @property Collection|ProductImage[] $productImages
 * @property Collection|ProductInfoDetail[] $productInfoDetails
 * @property Collection|ProductLink[] $productLinks
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Product extends Model
{
    use SoftDeletes;
    use Sluggable;
    use Visits;
    use Statistic;
    
    protected $casts = [
        'organization_id' => 'int',
        'product_category_id' => 'int',
        'price' => 'float',
        'price_sale' => 'float',
        'status' => 'int',
        'has_check' => 'bool',
        'count_view' => 'int',
        'avg_rate' => 'float',
        'count_rate' => 'int',
        'count_order' => 'int',
        'rank' => 'float',
        'active' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int',
        'deleted_by' => 'int',
        'weight_per_unit' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        //'path',
        'organization_id',
        'product_category_id',
        'code',
        'name',
        'unit',
        'price',
        'price_sale',
        'avatar_image',
        'status',
        'duration',
        'barcode',
        'has_check',
        'check_temp_image',
        'capacity',
        'pack_style',
        'preservation_method',
        //'count_view',
        //'avg_rate',
        //'count_rate',
        //'count_order',
        //'rank',
        'active',
        'created_by',
        'updated_by',
        'deleted_by',
        'weight_per_unit',
        'it5_vitri',
        'it5_giaytophaply',
        'it5_dientichsudung',
        'it5_ngangdai',
        'it5_donvidientich',
        'it5_donvitien',
        'it5_sdtlienhe',
        'it5_soto',
        'it5_sothua',
        'it5_maxa',
        'it5_loaitour',
        'it5_thoigiankhoihanh',
        'it5_noikhoihanh',
        'it5_noiden',
        'it5_thoigiandidukien',
        'it5_phuongtiendi',
        'it5_soluongtuyendung',
        'it5_trinhdotoithieu',
        'it5_tuoitoida',
        'it5_tuoitoithieu',
        'it5_loaicongviec',
        'id_parent'
    ];

    protected $appends = ['status_text','active_text'];
    
    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }
    
    //Vuongth: Phuc vu cho trait Statistic
    public function orders()
    {
        $returnVal = $this->orderItems()
                ->join("orders as o", "order_items.order_id", "=", "o.id")
                ->whereIn("o.status", [2,4])
                ->selectRaw("order_items.*,o.status")->get();
        return $returnVal;
    }

    public function productImages()
    {
        return $this->hasMany(ProductImage::class);
    }

    public function productInfoDetails()
    {
        return $this->hasMany(ProductInfoDetail::class);
    }

    //Liên kết nguồn gốc sản phẩm
    public function productLinks()
    {
        return $this->hasMany(ProductLink::class);
    }

    //Danh sách chứng nhận, xác nhận của sản phẩm
    public function certificates()
    {
        return $this->morphMany('App\Models\Certificate', 'certificateable');
    }

    //DS đánh giá đối với sản phẩm này
    public function ratings()
    {
        return $this->morphMany('App\Models\Rating', 'ratingable');
    }

    //DS người yêu thích đối với sản phẩm này
    public function favorites()
    {
        return $this->morphMany('App\Models\Favorite', 'favoriteable');
    }

    //DS câu hỏi đối với sản phẩm này
    public function questionAnswers()
    {
        return $this->morphMany('App\Models\QuestionAnswer', 'question_answerable');
    }

    //DS phản ánh vi phạm
    public function infringements()
    {
        return $this->morphMany('App\Models\Infringement', 'infringementable');
    }

    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
    
    public function getStatusTextAttribute()
    {
        //Trạng thái SP: 0: Chưa kinh doanh, 1-Còn hàng, 2-Theo mùa vụ, 3-Hết hàng, 4-Ngừng kinh doanh
        $returnVal="";
        switch ($this->status) {
            case 0:
                $returnVal="Chưa kinh doanh";
                break;
            case 1:
                $returnVal="Còn hàng";
                break;
            case 2:
                $returnVal="Theo mùa vụ";
                break;
            case 3:
                $returnVal="Hết hàng";
                break;
            default:
                $returnVal="Ngừng kinh doanh";
                break;
        }
        return $returnVal;
    }
    
    public function getActiveTextAttribute()
    {
        $returnVal="";
        switch ($this->active) {
            case 0:
                $returnVal="Chưa duyệt";
                break;
            case 1:
                $returnVal="Đã duyệt";
                break;
            default:
                $returnVal="Khóa";
                break;
        }
        return $returnVal;
    }
}
