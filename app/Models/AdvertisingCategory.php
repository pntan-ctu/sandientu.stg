<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AdvertisingCategory
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Collection|Advertising[] $advertisings
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class AdvertisingCategory extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'name',
        'path'
    ];

    public function advertisings()
    {
        return $this->hasMany(Advertising::class, 'category_id');
    }
}
