<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Contact
 *
 * @property int $id
 * @property int $organization_id
 * @property string $title
 * @property string $content
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $forward_log
 * @property int $status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Organization $organization
 * @property Collection|ContactFile[] $contactFiles
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Contact extends Model
{
    use SoftDeletes;

    protected $casts = [
        'organization_id' => 'int',
        'status' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'organization_id',
        'title',
        'content',
        'name',
        'email',
        'phone',
        'forward_log',
        'status'
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function contactFiles()
    {
        return $this->hasMany(ContactFile::class);
    }
}
