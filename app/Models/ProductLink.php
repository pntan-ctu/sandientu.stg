<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductLink
 *
 * @property int $id
 * @property int $product_id
 * @property int $link_product_id
 * @property string $descript
 *
 * @property Product $product
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class ProductLink extends Model
{
    public $timestamps = false;

    protected $casts = [
        'product_id' => 'int',
        'link_product_id' => 'int'
    ];

    protected $fillable = [
        'product_id',
        'link_product_id',
        'descript'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function linkProduct()
    {
        return $this->belongsTo(Product::class, 'link_product_id');
    }
}
