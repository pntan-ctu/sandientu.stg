<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentType
 *
 * @property int $id
 * @property string $name
 *
 * @property Collection|Document[] $documents
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class DocumentType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
