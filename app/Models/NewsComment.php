<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NewsComment
 *
 * @property int $id
 * @property int $article_id
 * @property string $name
 * @property string $email
 * @property string $comment
 * @property int $active
 * @property int $publish_by
 * @property Carbon $publish_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property NewsArticle $newsArticle
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class NewsComment extends Model
{
    use SoftDeletes;

    protected $casts = [
        'article_id' => 'int',
        'active' => 'int',
        'publish_by' => 'int'
    ];

    protected $dates = [
        'publish_at',
        'deleted_at'
    ];

    protected $fillable = [
        'article_id',
        'name',
        'email',
        'comment',
        'active',
        //'publish_by',
        //'publish_at'
    ];

    public function newsArticle()
    {
        return $this->belongsTo(NewsArticle::class, 'article_id');
    }
}
