<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Certificate
 *
 * @property int $id
 * @property string $certificateable_type
 * @property int $certificateable_id
 * @property int $certificate_category_id
 * @property string $title
 * @property string $image
 * @property string $description
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property CertificateCategory $certificateCategory
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Certificate extends Model
{
    protected $casts = [
        'certificateable_id' => 'int',
        'certificate_category_id' => 'int'
    ];

    protected $fillable = [
        'certificateable_type',
        'certificateable_id',
        'certificate_category_id',
        'title',
        'image',
        'description'
    ];

    public function certificateCategory()
    {
        return $this->belongsTo(CertificateCategory::class);
    }

    /**
     * Get all of the owning commentable models.
     */
    public function certificateable()
    {
        return $this->morphTo();
    }
}
