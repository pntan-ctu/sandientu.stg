<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductCategory
 *
 * @property int $id
 * @property string $path
 * @property int $parent_id
 * @property int $department_manage_id
 * @property string $name
 * @property string $short_name
 * @property string $description
 * @property string $path_primary_key
 * @property string $path_name
 * @property string $path_short_name
 * @property string $icon
 * @property int $no
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property DepartmentManage $departmentManage
 * @property ProductCategory $productCategory
 * @property Collection|Advertising[] $advertisings
 * @property Collection|ProductCategory[] $productCategories
 * @property Collection|ProductInfoCfg[] $productInfoCfgs
 * @property Collection|Product[] $products
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class ProductCategory extends Model
{
    use SoftDeletes;
    use Sluggable;

    protected $casts = [
        'parent_id' => 'int',
        'department_manage_id' => 'int',
        'no' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        //'path',
        'parent_id',
        'department_manage_id',
        'name',
        'short_name',
        'description',
        'path_primary_key',
        'path_name',
        'path_short_name',
        'icon',
        'no',
        'id'
    ];

    public function departmentManage()
    {
        return $this->belongsTo(DepartmentManage::class);
    }

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class, 'parent_id');
    }

    public function advertisings()
    {
        return $this->hasMany(Advertising::class);
    }

    public function productCategories()
    {
        return $this->hasMany(ProductCategory::class, 'parent_id');
    }

    public function productInfoCfgs()
    {
        return $this->hasMany(ProductInfoCfg::class);
    }

    public function products()
    {
        return $this->hasMany(Product::class);
    }

    // TODO comment ý nghĩa vì không theo quy luật thông thường
    public function products2()
    {
        return $this->hasManyThrough(
            'App\Models\Product',
            'App\Models\ProductCategory',
            'parent_id',
            'product_category_id',
            'id'
        );
    }

    public function sluggable(): array
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
}
