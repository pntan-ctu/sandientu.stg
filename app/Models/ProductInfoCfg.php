<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ProductInfoCfg
 *
 * @property int $id
 * @property string $key
 * @property int $product_category_id
 * @property string $name
 *
 * @property ProductCategory $productCategory
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class ProductInfoCfg extends Model
{
    public $timestamps = false;

    protected $casts = [
        'product_category_id' => 'int'
    ];

    protected $fillable = [
        'key',
        'product_category_id',
        'name'
    ];

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }
}
