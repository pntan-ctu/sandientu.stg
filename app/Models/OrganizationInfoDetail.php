<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationInfoDetail
 *
 * @property int $id
 * @property int $organization_id
 * @property string $key
 * @property string $content
 *
 * @property Organization $organization
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class OrganizationInfoDetail extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_id' => 'int'
    ];

    protected $fillable = [
        'organization_id',
        'key',
        'content'
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
