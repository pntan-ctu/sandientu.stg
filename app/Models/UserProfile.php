<?php

namespace App\Models;

use App\Models\Traits\LocalizableDateTime;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UserProfile
 *
 * @property int $user_id
 * @property Carbon $date_of_birth
 * @property int $sex
 * @property string $address
 * @property int $region_id
 * @property string $avatar
 * @property string $identification
 * @property Carbon $identification_date
 * @property string $identification_by
 *
 * @property Region $region
 * @property User $user
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class UserProfile extends Model
{
    use LocalizableDateTime;

    protected $primaryKey = 'user_id';
    public $incrementing = false;
    public $timestamps = false;

    protected $casts = [
        'user_id' => 'int',
        'sex' => 'int',
        'region_id' => 'int'
    ];

    protected $dates = [
        'date_of_birth',
        'identification_date'
    ];

    protected $fillable = [
        'date_of_birth',
        'sex',
        'address',
        'region_id',
        'avatar',
        'identification',
        'identification_date',
        'identification_by',
        'fanpage'
    ];
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
