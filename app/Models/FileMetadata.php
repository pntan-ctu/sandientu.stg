<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class FileMetadata
 *
 * @property int $id
 * @property string $path
 * @property string $file_name
 * @property string $type
 * @property int $size
 * @property string $hash_data
 * @property int $upload_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class FileMetadata extends Model
{
    protected $casts = [
        'size' => 'int',
        'upload_by' => 'int'
    ];

    protected $fillable = [
        'path',
        'file_name',
        'type',
        'size',
        'hash_data',
        'upload_by'
    ];
}
