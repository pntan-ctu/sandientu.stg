<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Infringement
 *
 * @property int $id
 * @property string $infringementable_type
 * @property int $infringementable_id
 * @property int $category_id
 * @property string $comment
 * @property int $status
 * @property int $created_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property InfringementCategory $infringementCategory
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Infringement extends Model
{
    use SoftDeletes;

    protected $casts = [
        'infringementable_id' => 'int',
        'category_id' => 'int',
        'status' => 'int',
        'created_by' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'infringementable_type',
        'infringementable_id',
        'category_id',
        'comment',
        'status',
        'created_by'
    ];

    public function infringementable()
    {
        return $this->morphTo();
    }
    
    public function infringementCategory()
    {
        return $this->belongsTo(InfringementCategory::class, 'category_id');
    }
    
    public function createdByUser()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
