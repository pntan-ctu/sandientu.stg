<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SocialAccount
 *
 * @property int $user_id
 * @property string $provider_user_id
 * @property string $provider
 * @property string $nick_name
 * @property string $avatar
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property User $user
 *
 * @package App\Models
 */
class SocialAccount extends Model
{
    public $incrementing = false;

    protected $casts = [
        'user_id' => 'int'
    ];

    protected $fillable = [
        'user_id',
        'provider_user_id',
        'provider',
        'nick_name',
        'avatar'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
