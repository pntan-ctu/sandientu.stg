<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Country
 *
 * @property int $id
 * @property string $name
 * @property string $vn_name
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Country extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'vn_name'
    ];
}
