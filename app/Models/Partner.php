<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Partner
 *
 * @property int $id
 * @property string $path
 * @property int $organization_id
 * @property string $type
 * @property int $link_organization_id
 * @property string $name
 * @property string $address
 * @property string $tel
 * @property string $email
 * @property string $website
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Organization $organization
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Partner extends Model
{
    use Sluggable;
    protected $casts = [
        'organization_id' => 'int',
        'link_organization_id' => 'int'
    ];

    protected $fillable = [
        'path',
        'organization_id',
        'type',
        'link_organization_id',
        'name',
        'address',
        'tel',
        'email',
        'website'
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class, 'link_organization_id');
    }
    
    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
}
