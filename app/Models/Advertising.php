<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Traits\LocalizableDateTime;
use App\Models\Traits\Visits;

/**
 * Class Advertising
 *
 * @property int $id
 * @property string $advertisingable_type
 * @property int $advertisingable_id
 * @property int $category_id
 * @property int $product_category_id
 * @property int $region_id
 * @property string $title
 * @property string $path
 * @property string $content
 * @property int $link_product_id
 * @property string $address
 * @property string $tel
 * @property string $fanpage
 * @property Carbon $from_date
 * @property Carbon $to_date
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Region $region
 * @property ProductCategory $productCategory
 * @property AdvertisingCategory $advertisingCategory
 * @property Collection|AdvertisingImage[] $advertisingImages
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Advertising extends Model
{
    use SoftDeletes;
    use Sluggable;
    use LocalizableDateTime;
    use Visits;

    protected $casts = [
        'advertisingable_id' => 'int',
        'category_id' => 'int',
        'product_category_id' => 'int',
        'region_id' => 'int',
        'link_product_id' => 'int',
        'status' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int'
    ];

    protected $dates = [
        'from_date',
        'to_date',
        'deleted_at'
    ];

    protected $fillable = [
        'advertisingable_type',
        'advertisingable_id',
        'category_id',
        'product_category_id',
        'region_id',
        'title',
//        'path',
        'content',
        'link_product_id',
        'address',
        'tel',
        'it5_mucluong',
        'it5_quymoduan',
        'it5_cuocphithamquan',
        'fanpage',
        'from_date',
        'to_date',
        'status',
        'created_by',
        'updated_by'
    ];

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function productCategory()
    {
        return $this->belongsTo(ProductCategory::class);
    }

    public function advertisingCategory()
    {
        return $this->belongsTo(AdvertisingCategory::class, 'category_id');
    }

    public function advertisingCategoryName()
    {
        $names =  $this->advertisingCategory()
                        ->selectRaw('id, name');
        return $names;
    }

    public function advertisingImages()
    {
        return $this->hasMany(AdvertisingImage::class);
    }

    public function createdByUser()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
    
    public function advertisingable()
    {
        return $this->morphTo();
    }

    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'title'
            ]
        ];
    }
}
