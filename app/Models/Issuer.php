<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Issuer
 *
 * @property int $id
 * @property string $name
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Issuer extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];
}
