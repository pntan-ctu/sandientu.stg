<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PollAnswer
 *
 * @property int $id
 * @property int $poll_id
 * @property string $name
 * @property int $no
 * @property int $public
 * @property int $votes
 * @property int $created_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Poll $poll
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class PollAnswer extends Model
{
    protected $casts = [
        'poll_id' => 'int',
        'no' => 'int',
        'public' => 'int',
        'votes' => 'int',
        'created_by' => 'int'
    ];

    protected $fillable = [
        'poll_id',
        'name',
        'no',
        'public',
        'votes',
        'created_by'
    ];

    public function poll()
    {
        return $this->belongsTo(Poll::class);
    }
}
