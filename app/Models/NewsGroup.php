<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class NewsGroup
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property string $path
 * @property string $description
 * @property int $no
 * @property int $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Collection|NewsArticle[] $newsArticles
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class NewsGroup extends Model
{
    use SoftDeletes;
    use Sluggable;

    protected $casts = [
        'parent_id' => 'int',
        'no' => 'int',
        'active' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'parent_id',
        'name',
        'path',
        'description',
        'no',
        'active'
    ];

    public function newsArticles()
    {
        return $this->hasMany(NewsArticle::class, 'group_id');
    }

    // TODO quan hệ trên DB?
    public function parent()
    {
        return $this->belongsTo(NewsGroup::class, 'parent_id');
    }

    public function children()
    {
        return $this->hasMany(NewsGroup::class, 'parent_id');
    }

    /**
     * Return the sluggable configuration array for this model.
     * name: is origin. path: retrieve after slug name
     * @return array
     */
    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
}
