<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Traits\Visits;
use Cmgmyr\Messenger\Models\Thread as MessengerThread;

/**
 * Class Thread
 *
 * @property int $id
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Thread extends MessengerThread
{
    protected $fillable = ['subject', 'organization_id', 'user_id'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
