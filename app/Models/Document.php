<?php

namespace App\Models;

use App\Models\Traits\LocalizableDateTime;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Document
 *
 * @property int $id
 * @property int $document_field_id
 * @property int $document_type_id
 * @property int $document_organ_id
 * @property string $number_symbol
 * @property string $path
 * @property string $document_path
 * @property Carbon $illegal_date
 * @property string $quote
 * @property string $sign_by
 * @property string $duty
 * @property string $description
 * @property Carbon $sign_date
 * @property Carbon $enable_date
 * @property int $status
 * @property int $view_count
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property DocumentField $documentField
 * @property DocumentType $documentType
 * @property DocumentOrgan $documentOrgan
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Document extends Model
{
    use SoftDeletes;
    use Sluggable;
    use LocalizableDateTime;

    protected $casts = [
        'document_field_id' => 'int',
        'document_type_id' => 'int',
        'document_organ_id' => 'int',
        'status' => 'int',
        'view_count' => 'int'
    ];

    protected $dates = [
        'illegal_date',
        'sign_date',
        'enable_date',
        'deleted_at'
    ];

    protected $fillable = [
        'document_field_id',
        'document_type_id',
        'document_organ_id',
        'number_symbol',
        //'path',
        'document_path',
        'illegal_date',
        'quote',
        'sign_by',
        'duty',
        'description',
        'sign_date',
        'enable_date',
        'status',
        'view_count'
    ];

    public function documentField()
    {
        return $this->belongsTo(DocumentField::class);
    }

    public function documentType()
    {
        return $this->belongsTo(DocumentType::class);
    }

    public function documentOrgan()
    {
        return $this->belongsTo(DocumentOrgan::class);
    }

    public function sluggable()
    {
        return [
            'path' => [
                'source' => ['number_symbol', 'quote'],
                'separator' => '-'
            ],
        ];
    }
}
