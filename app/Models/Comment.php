<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Comment
 *
 * @property int $id
 * @property string $commentable_type
 * @property int $commentable_id
 * @property int $parent_id
 * @property string $title
 * @property string $content
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Comment extends Model
{
    use SoftDeletes;

    protected $casts = [
        'commentable_id' => 'int',
        'parent_id' => 'int',// TODO quan hệ cha con à?
        'status' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int',
        'deleted_by' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'commentable_type',
        'commentable_id',
        'parent_id',
        'title',
        'content',
        'status',
        //'created_by',
        //'updated_by',
        //'deleted_by'
    ];

    /**
     * Get all of the owning commentable models.
     */
    public function commentable()
    {
        return $this->morphTo();
    }
}
