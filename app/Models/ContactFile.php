<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ContactFile
 *
 * @property int $id
 * @property int $contact_id
 * @property string $filename
 *
 * @property Contact $contact
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class ContactFile extends Model
{
    public $timestamps = false;

    protected $casts = [
        'contact_id' => 'int'
    ];

    protected $fillable = [
        'contact_id',
        'filename'
    ];

    public function contact()
    {
        return $this->belongsTo(Contact::class);
    }
}
