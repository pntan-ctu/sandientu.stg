<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationLevel
 *
 * @property int $id
 * @property string $name
 *
 * @property Collection|Organization[] $organizations
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class OrganizationLevel extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name'
    ];

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }
}
