<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class InfringementCategory
 *
 * @property int $id
 * @property string $code
 * @property string $name
 * @property int $type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Collection|Infringement[] $infringements
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class InfringementCategory extends Model
{
    use SoftDeletes;

    protected $casts = [
        'type' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'code',
        'name',
        'type'
    ];

    public function infringements()
    {
        return $this->hasMany(Infringement::class, 'category_id');
    }
}
