<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Rating
 *
 * @property int $id
 * @property string $ratingable_type
 * @property int $ratingable_id
 * @property int $rate
 * @property string $comment
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Rating extends Model
{
    use SoftDeletes;

    protected $casts = [
        'ratingable_id' => 'int',
        'rate' => 'int',
        'status' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int',
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'ratingable_type',
        'ratingable_id',
        'rate',
        'comment',
        'status',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    /**
     * Get all of the owning commentable models.
     */
    public function ratingable()
    {
        return $this->morphTo();
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
