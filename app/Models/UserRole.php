<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class UserRole
 *
 * @property int $id
 * @property int $role_id
 * @property int $user_id
 *
 * @property Role $role
 * @property User $user
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class UserRole extends Model
{
    public $timestamps = false;
    protected $casts = [
        'role_id' => 'int',
        'user_id' => 'int'
    ];

    protected $fillable = [
        'role_id',
        'user_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
