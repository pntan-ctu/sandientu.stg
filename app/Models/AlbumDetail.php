<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AlbumDetail
 *
 * @property int $id
 * @property int $album_id
 * @property string $title
 * @property int $created_by
 * @property string $img_path
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Album $album
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class AlbumDetail extends Model
{
    use SoftDeletes;

    protected $casts = [
        'album_id' => 'int',
        'created_by' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'album_id',
        'title',
        'created_by',
        'img_path'
    ];

    public function album()
    {
        return $this->belongsTo(Album::class);
    }
}
