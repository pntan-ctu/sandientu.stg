<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class SlideShow
 *
 * @property int $id
 * @property string $title
 * @property string $image_path
 * @property string $link
 * @property int $created_by
 * @property int $no
 * @property int $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class SlideShow extends Model
{
    use SoftDeletes;
    protected $casts = [
        'created_by' => 'int',
        'no' => 'int',
        'active' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'title',
        'image_path',
        'link',
        'created_by',
        'no',
        'active'
    ];
}
