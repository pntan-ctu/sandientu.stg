<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationShipType
 *
 * @property int $id
 * @property int $organization_id
 * @property int $ship_type_id
 * @property string $content
 * @property int $is_default
 *
 * @property ShipType $shipType
 * @property Organization $organization
 *
 * @package App\Models
 */
class OrganizationShipType extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_id' => 'int',
        'ship_type_id' => 'int',
        'is_default' => 'int'
    ];

    protected $fillable = [
        'ship_type_id',
        'content',
        'is_default'
    ];

    public function shipType()
    {
        return $this->belongsTo(ShipType::class);
    }
    
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
