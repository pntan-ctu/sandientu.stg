<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Favorite
 *
 * @property int $id
 * @property string $favoriteable_type
 * @property int $favoriteable_id
 * @property int $user_id
 *
 * @property User $user
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Favorite extends Model
{
    public $timestamps = false;

    protected $casts = [
        'favoriteable_id' => 'int',
        'user_id' => 'int'
    ];

    protected $fillable = [
        'favoriteable_type',
        'favoriteable_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Get all of the owning commentable models.
     */
    public function favoriteable()
    {
        return $this->morphTo();
    }
}
