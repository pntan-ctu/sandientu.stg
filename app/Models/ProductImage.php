<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductImage
 *
 * @property int $id
 * @property int $product_id
 * @property string $image
 * @property string $decription
 *
 * @property Product $product
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class ProductImage extends Model
{
    public $timestamps = false;

    protected $casts = [
        'product_id' => 'int'
    ];

    protected $fillable = [
        'product_id',
        'image',
        'decription'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
