<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ShipType
 *
 * @property int $id
 * @property string $name
 * @property int $require_register
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Collection|Organization[] $organizations
 *
 * @package App\Models
 */
class ShipType extends Model
{
    use SoftDeletes;

    protected $casts = [
        'require_register' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'name',
        'require_register'
    ];

    public function orders()
    {
        return $this->hasMany(Order::class);
    }

    public function organizations()
    {
        return $this->belongsToMany(Organization::class, 'organization_ship_types')
                    ->withPivot('id', 'content', 'is_default');
    }
}
