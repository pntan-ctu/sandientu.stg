<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Message
 *
 * @property int $id
 * @property string $messageable_type
 * @property int $messageable_id
 * @property string $subject
 * @property string $content
 * @property string $sendable_type
 * @property int $sendable_id
 * @property Carbon $read_at
 * @property int $created_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Message extends Model
{
    use SoftDeletes;

    protected $casts = [
        'messageable_id' => 'int',
        'sendable_id' => 'int',
        'created_by' => 'int'
    ];

    protected $dates = [
        'read_at',
        'deleted_at'
    ];

    protected $fillable = [
        'messageable_type',
        'messageable_id',
        'subject',
        'content',
        'sendable_type',
        'sendable_id',
        'read_at',
        'created_by'
    ];

    /**
     * Lấy đối tượng nhận message
     */
    public function messageable()
    {
        return $this->morphTo();
    }

    /**
     * Mark the notification as read.
     *
     * @return void
     */
    public function markAsRead()
    {
        if (is_null($this->read_at)) {
            $this->forceFill(['read_at' => $this->freshTimestamp()])->save();
        }
    }

    /**
     * Mark the message as unread.
     *
     * @return void
     */
    public function markAsUnread()
    {
        if (! is_null($this->read_at)) {
            $this->forceFill(['read_at' => null])->save();
        }
    }

    /**
     * Determine if a message has been read.
     *
     * @return bool
     */
    public function read()
    {
        return $this->read_at !== null;
    }

    /**
     * Determine if a message has not been read.
     *
     * @return bool
     */
    public function unread()
    {
        return $this->read_at === null;
    }

    /**
     * Lấy đối tượng đã gửi message
     */
    public function sendable()
    {
        return $this->morphTo();
    }
}
