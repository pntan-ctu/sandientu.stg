<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class VideoGroup
 *
 * @property int $id
 * @property string $name
 * @property string $path
 * @property string $description
 * @property int $created_by
 * @property int $no
 * @property int $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection|Video[] $videos
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class VideoGroup extends Model
{
    //use SoftDeletes;
    use Sluggable;

    protected $casts = [
        'created_by' => 'int',
        'no' => 'int',
        'active' => 'int'
    ];

    protected $fillable = [
        'name',
        'path',
        'description',
        'created_by',
        'no',
        'active'
    ];

    public function videos()
    {
        return $this->hasMany(Video::class, 'group_id');
    }

    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
}
