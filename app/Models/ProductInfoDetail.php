<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class ProductInfoDetail
 *
 * @property int $id
 * @property int $product_id
 * @property string $key
 * @property string $content
 *
 * @property Product $product
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class ProductInfoDetail extends Model
{
    public $timestamps = false;
    protected $casts = [
        'product_id' => 'int'
    ];

    protected $fillable = [
        'product_id',
        'key',
        'content'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }
}
