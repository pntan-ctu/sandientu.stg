<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class CommercialCenter
 *
 * @property int $id
 * @property string $path
 * @property int $manage_organization_id
 * @property int $region_id
 * @property string $name
 * @property string $address
 * @property string $introduction
 * @property float $map_lat
 * @property float $map_long
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Organization $organization
 * @property Region $region
 * @property Collection|Organization[] $organizations
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class CommercialCenter extends Model
{
    use SoftDeletes;
    use Sluggable;

    protected $casts = [
        'manage_organization_id' => 'int',
        'region_id' => 'int',
        'map_lat' => 'float',
        'map_long' => 'float'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        //'path',
        'manage_organization_id',
        'region_id',
        'name',
        'address',
        'introduction',
        'map_lat',
        'map_long'
    ];

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function manageOrganization()
    {
        return $this->belongsTo(Organization::class, 'manage_organization_id');
    }
    
    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
}
