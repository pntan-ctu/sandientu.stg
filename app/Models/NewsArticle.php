<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Visits;

/**
 * Class NewsArticle
 *
 * @property int $id
 * @property int $group_id
 * @property string $title
 * @property string $path
 * @property string $summary
 * @property string $content
 * @property string $img_path
 * @property int $view_count
 * @property int $hotnews
 * @property int $comment
 * @property int $shared
 * @property string $tags
 * @property string $relate_news
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property int $publish_by
 * @property Carbon $publish_at
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property NewsGroup $newsGroup
 * @property Collection|NewsComment[] $newsComments
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class NewsArticle extends Model
{
    use SoftDeletes;
    use Sluggable;
    use Visits;


    protected $casts = [
        'group_id' => 'int',
        'view_count' => 'int',
        'hotnews' => 'int',
        'comment' => 'int',
        'shared' => 'int',
        'status' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int',
        'deleted_by' => 'int',
        'publish_by' => 'int'
    ];

    protected $dates = [
        'publish_at',
        'deleted_at'
    ];

    protected $fillable = [
        'group_id',
        'title',
        //'path',
        'summary',
        'content',
        'img_path',
        'view_count',
        'hotnews',
        'comment',
        'shared',
        'tags',
        'relate_news',
        'status',
        'created_by',
        'updated_by',
        'deleted_by',
        'publish_by',
        'publish_at'
    ];

    public function newsGroup()
    {
        return $this->belongsTo(NewsGroup::class, 'group_id');
    }

    public function newsComments()
    {
        return $this->hasMany(NewsComment::class, 'article_id');
    }

    //DS người yêu thích đối với bài viết này
    public function favorites()
    {
        return $this->morphMany('App\Models\Favorite', 'favoriteable');
    }

    /**
     * Return the sluggable configuration array for this model.
     * name: is origin. path: retrieve after slug name
     * @return array
     */
    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'title'
            ]
        ];
    }
}
