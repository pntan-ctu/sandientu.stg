<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class AlbumGroup
 *
 * @property int $id
 * @property string $name
 * @property string $description
 * @property int $created_by
 * @property int $no
 * @property int $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Collection|Album[] $albums
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class AlbumGroup extends Model
{
    use SoftDeletes;

    protected $casts = [
        'created_by' => 'int',
        'no' => 'int',
        'active' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'name',
        'description',
        //'created_by',
        'no',
        'active'
    ];

    public function albums()
    {
        return $this->hasMany(Album::class, 'group_id');
    }
}
