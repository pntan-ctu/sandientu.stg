<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class PasswordReset
 *
 * @property int $id
 * @property string $email
 * @property string $token
 * @property Carbon $created_at
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class PasswordReset extends Model
{
    public $timestamps = false;

    protected $hidden = [
        'token'
    ];

    protected $fillable = [
        'email',
        'token'
    ];
}
