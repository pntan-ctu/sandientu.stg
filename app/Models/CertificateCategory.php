<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class CertificateCategory
 *
 * @property int $id
 * @property string $name
 * @property int $rank
 * @property int $type
 * @property string $description
 * @property string $icon
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Collection|Certificate[] $certificates
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class CertificateCategory extends Model
{
    use SoftDeletes;

    protected $casts = [
        'rank' => 'int',
        'type' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'name',
        'rank',
        'type',
        'description',
        'icon'
    ];

    public function certificates()
    {
        return $this->hasMany(Certificate::class);
    }
}
