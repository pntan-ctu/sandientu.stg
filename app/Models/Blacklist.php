<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Blacklist
 *
 * @property int $id
 * @property int $organization_id
 * @property int $user_id
 * @property string $reason
 * @property int $created_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Organization $organization
 * @property User $user
 *
 * @package App\Models
 */
class Blacklist extends Model
{
    protected $casts = [
        'id' => 'int',
        'organization_id' => 'int',
        'user_id' => 'int',
        'created_by' => 'int'
    ];

    protected $fillable = [
        'organization_id',
        'user_id',
        'reason',
        'created_by'
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
