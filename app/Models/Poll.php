<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Poll
 *
 * @property int $id
 * @property string $name
 * @property int $no
 * @property int $public
 * @property int $votes
 * @property int $created_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection|PollAnswer[] $pollAnswers
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Poll extends Model
{
    // TOTO trường votes ở bảng này và bảng anwer có khác nhau gì
    protected $casts = [
        'no' => 'int',
        'public' => 'int',
        'votes' => 'int',
        'created_by' => 'int'
    ];

    protected $fillable = [
        'name',
        'no',
        'public',
        'votes',
        //'created_by'
    ];

    public function pollAnswers()
    {
        return $this->hasMany(PollAnswer::class);
    }
}
