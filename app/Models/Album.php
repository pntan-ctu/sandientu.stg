<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Traits\Visits;

/**
 * Class Album
 *
 * @property int $id
 * @property int $group_id
 * @property string $title
 * @property string $path
 * @property string $description
 * @property string $img_path
 * @property int $created_by
 * @property int $no
 * @property int $active
 * @property int $home
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property AlbumGroup $albumGroup
 * @property Collection|AlbumDetail[] $albumDetails
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Album extends Model
{
    use SoftDeletes;
    use Sluggable;
    use Visits;

    protected $casts = [
        'group_id' => 'int',
        'created_by' => 'int',
        'no' => 'int',
        'active' => 'int',
        'home' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'group_id',
        'title',
        //'path',
        'description',
        'img_path',
        //'created_by',
        'no',
        'active',
        'home'
    ];

    public function albumGroup()
    {
        return $this->belongsTo(AlbumGroup::class, 'group_id');
    }

    public function albumDetails()
    {
        return $this->hasMany(AlbumDetail::class);
    }

    //DS người yêu thích đối với album này
    public function favorites()
    {
        return $this->morphMany('App\Models\Favorite', 'favoriteable');
    }

    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'title'
            ]
        ];
    }
}
