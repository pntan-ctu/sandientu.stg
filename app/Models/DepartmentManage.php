<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DepartmentManage
 *
 * @property int $id
 * @property string $name
 *
 * @property Collection|Organization[] $organizations
 * @property Collection|ProductCategory[] $productCategories
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class DepartmentManage extends Model
{
    protected $table = 'department_manages';
    public $timestamps = false;
    public static $snakeAttributes = false;

    protected $fillable = [
        'name'
    ];

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }

    public function productCategories()
    {
        return $this->hasMany(ProductCategory::class);
    }
}
