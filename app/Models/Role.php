<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Role
 *
 * @property int $id
 * @property int $organization_type_id
 * @property string $name
 * @property int $allow_delete
 *
 * @property OrganizationType $organizationType
 * @property Collection|Module[] $modules
 * @property Collection|User[] $users
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Role extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_type_id' => 'int',
        'allow_delete' => 'int'
    ];

    protected $fillable = [
        'organization_type_id',
        'name',
        'allow_delete'
    ];

    public function organizationType()
    {
        return $this->belongsTo(OrganizationType::class);
    }

    public function modules()
    {
        return $this->belongsToMany(Module::class, 'module_roles')
            ->withPivot('id');
    }

    public function users()
    {
        return $this->belongsToMany(User::class, 'user_roles')
            ->withPivot('id');
    }
}
