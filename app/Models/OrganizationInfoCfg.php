<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationInfoCfg
 *
 * @property int $id
 * @property string $key
 * @property int $organization_type_id
 * @property string $name
 *
 * @property OrganizationType $organizationType
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class OrganizationInfoCfg extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_type_id' => 'int'
    ];

    protected $fillable = [
        'key',
        'organization_type_id',
        'name'
    ];

    public function organizationType()
    {
        return $this->belongsTo(OrganizationType::class);
    }
    public function organizationInfoDetail()
    {
        return $this->belongsTo(OrganizationInfoDetail::class, 'key');
    }
    public function organizationInfoDetailContent()
    {
        $content =  $this->advertisingCategory()
                        ->selectRaw('id, content');
        return $content;
    }
}
