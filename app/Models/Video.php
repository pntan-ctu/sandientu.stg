<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use App\Models\Traits\Visits;

/**
 * Class Video
 *
 * @property int $id
 * @property int $group_id
 * @property string $title
 * @property string $path
 * @property string $description
 * @property string $img_path
 * @property int $width
 * @property int $height
 * @property string $youtube_url
 * @property int $created_by
 * @property int $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property VideoGroup $videoGroup
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Video extends Model
{
    use Sluggable;
    use Visits;

    protected $casts = [
        'group_id' => 'int',
        'width' => 'int',
        'height' => 'int',
        'created_by' => 'int',
        'active' => 'int'
    ];

    protected $fillable = [
        'group_id',
        'title',
        'path',
        'description',
        'img_path',
        'width',
        'height',
        'youtube_url',
        //'created_by',
        'active'
    ];

    public function videoGroup()
    {
        return $this->belongsTo(VideoGroup::class, 'group_id');
    }
    
    //DS người yêu thích đối với video này
    public function favorites()
    {
        return $this->morphMany('App\Models\Favorite', 'favoriteable');
    }

    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'title'
            ]
        ];
    }
}
