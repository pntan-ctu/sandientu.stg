<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class ModuleRole
 *
 * @property int $id
 * @property int $role_id
 * @property int $module_id
 *
 * @property Role $role
 * @property Module $module
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class ModuleRole extends Model
{
    public $timestamps = false;

    protected $casts = [
        'role_id' => 'int',
        'module_id' => 'int'
    ];

    protected $fillable = [
        'role_id',
        'module_id'
    ];

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }
}
