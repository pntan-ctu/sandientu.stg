<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Help
 *
 * @property int $id
 * @property string $path
 * @property string $question
 * @property string $answers
 * @property int $common
 * @property int $status
 * @property string $name
 * @property string $email
 * @property string $phone
 * @property string $address
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @package App\Models
 */
class Help extends Model
{
    use SoftDeletes;
    use Sluggable;

    protected $casts = [
        'common' => 'int',
        'status' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'question',
        'answers',
        'common',
        'status',
        'name',
        'email',
        'phone',
        'address'
    ];

    /**
     * Return the sluggable configuration array for this model.
     * name: is origin. path: retrieve after slug name
     * @return array
     */
    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'question'
            ]
        ];
    }
}
