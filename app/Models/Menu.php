<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Menu
 *
 * @property int $id
 * @property int $organization_type_id
 * @property int $parent_id
 * @property string $name
 * @property int $module_id
 * @property string $icon
 * @property int $no
 *
 * @property Menu $menu
 * @property Module $module
 * @property OrganizationType $organizationType
 * @property Collection|Menu[] $menus
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Menu extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_type_id' => 'int',
        'parent_id' => 'int',
        'module_id' => 'int',
        'no' => 'int'
    ];

    protected $fillable = [
        'organization_type_id',
        'parent_id',
        'name',
        'module_id',
        'icon',
        'no'
    ];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'parent_id');
    }

    public function module()
    {
        return $this->belongsTo(Module::class);
    }

    public function organizationType()
    {
        return $this->belongsTo(OrganizationType::class);
    }

    public function menus()
    {
        return $this->hasMany(Menu::class, 'parent_id');
    }
}
