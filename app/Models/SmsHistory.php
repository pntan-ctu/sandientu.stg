<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class SmsHistory
 *
 * @property int $id
 * @property int $user_send_id
 * @property string $content
 * @property string $tel_receiver
 * @property int $tel_type
 * @property int $sms_count
 * @property string $month
 * @property string $year
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class SmsHistory extends Model
{
    protected $casts = [
        'user_send_id' => 'int',
        'tel_type' => 'int',
        'sms_count' => 'int'
    ];

    protected $fillable = [
        'user_send_id',
        'content',
        'tel_receiver',
        'tel_type',
        'sms_count',
        'month',
        'year'
    ];
}
