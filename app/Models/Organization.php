<?php

namespace App\Models;

use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\LocalizableDateTime;
use App\Models\Traits\Messageable;
use App\Models\Traits\Visits;
use App\Models\Traits\Statistic;

/**
 * Class Organization
 *
 * @property int $id
 * @property string $path
 * @property int $organization_type_id
 * @property int $region_id
 * @property int $manage_organization_id
 * @property int $department_manage_id
 * @property string $path_primary_key
 * @property int $area_id
 * @property int $commercial_center_id
 * @property string $name
 * @property string $address
 * @property string $tel
 * @property string $email
 * @property string $website
 * @property string $fanpage
 * @property string $logo_image
 * @property string $director
 * @property int $founding_type
 * @property string $founding_number
 * @property string $founding_by_gov
 * @property Carbon $founding_date
 * @property int $organization_level_id
 * @property float $map_lat
 * @property float $map_long
 * @property int $count_view
 * @property float $avg_rate
 * @property int $count_rate
 * @property int $count_order
 * @property float $rank
 * @property int $move_organization_id
 * @property string $move_logs
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property CommercialCenter $commercialCenter
 * @property Area $area
 * @property Organization $organization
 * @property Region $region
 * @property OrganizationType $organizationType
 * @property DepartmentManage $departmentManage
 * @property OrganizationLevel $organizationLevel
 * @property Collection|Area[] $areas
 * @property Collection|Blacklist[] $blacklists
 * @property Collection|Branch[] $branches
 * @property Collection|CommercialCenter[] $commercialCenters
 * @property Collection|Contact[] $contacts
 * @property Collection|Follow[] $follows
 * @property Collection|Order[] $orders
 * @property Collection|OrganizationImage[] $organizationImages
 * @property Collection|OrganizationInfoDetail[] $organizationInfoDetails
 * @property Collection|PayType[] $payTypes
 * @property Collection|ShipType[] $shipTypes
 * @property Collection|Organization[] $childs
 * @property Collection|Partner[] $partners
 * @property Collection|Product[] $products
 * @property Collection|User[] $users
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Organization extends Model
{
    use Sluggable;
    use SoftDeletes;
    use LocalizableDateTime;
    use Messageable;
    use Visits;
    use Statistic;
    
    protected $casts = [
        'organization_type_id' => 'int',
        'region_id' => 'int',
        'manage_organization_id' => 'int',
        'department_manage_id' => 'int',
        'area_id' => 'int',
        'commercial_center_id' => 'int',
        'founding_type' => 'int',
        'organization_level_id' => 'int',
        'map_lat' => 'float',
        'map_long' => 'float',
        'count_view' => 'int',
        'avg_rate' => 'float',
        'count_rate' => 'int',
        'count_order' => 'int',
        'rank' => 'float',
        'move_organization_id' => 'int',
        'status' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int',
        'deleted_by' => 'int'
    ];

    protected $dates = [
        'founding_date',
        'deleted_at'
    ];

    protected $fillable = [
        //'path',
        //'organization_type_id',
        'region_id',
        //'manage_organization_id',
        'department_manage_id',
        //'path_primary_key',
        'area_id',
        'commercial_center_id',
        'name',
        'address',
        'tel',
        'email',
        'website',
        'fanpage',
        'logo_image',
        'director',
        'founding_type',
        'founding_number',
        'founding_by_gov',
        'founding_date',
        'organization_level_id',
        'map_lat',
        'map_long',
        //'count_view',
        //'avg_rate',
        //'count_rate',
        //'count_order',
        //'rank',
        //'move_organization_id',
        //'move_logs',
        'status',
        //'created_by',
        //'updated_by',
        //'deleted_by'
    ];

    //Object Địa điểm kinh doanh của CSKD này (nếu khi tạo CS này chọn nó thuộc địa điểm nào)
    public function commercialCenter()
    {
        return $this->belongsTo(CommercialCenter::class);
    }

    //Object Vùng sản xuất của CSSX này (nếu khi tạo CS này chọn nó thuộc vùng sản xuất nào)
    public function area()
    {
        return $this->belongsTo(Area::class);
    }

    //Object CQQL của cơ sở này hoặc CQQL cấp cha của CQQL này
    public function manageOrganization()
    {
        return $this->belongsTo(Organization::class, 'manage_organization_id');
    }

    //Object Địa phương của CQQL, CSSXKD này
    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    //Object loại tổ chức của CQQL, CSSXKD này
    public function organizationType()
    {
        return $this->belongsTo(OrganizationType::class);
    }

    //Object Ngành quản lý của cơ sở này
    public function departmentManage()
    {
        return $this->belongsTo(DepartmentManage::class);
    }

    //Object Quy mô doanh nghiệp của cơ sở này
    public function organizationLevel()
    {
        return $this->belongsTo(OrganizationLevel::class);
    }

    //List Vùng sản xuất mà CQQL này quản lý (tạo ra)
    public function areas()
    {
        return $this->hasMany(Area::class, 'manage_organization_id');
    }

    //List các người dùng bị ngăn chặn của cơ sở này
    public function blacklists()
    {
        return $this->hasMany(Blacklist::class);
    }

    //List các chi nhánh của cơ sở này
    public function branches()
    {
        return $this->hasMany(Branch::class);
    }

    //List Địa điểm KD mà CQQL này quản lý (tạo ra)
    public function commercialCenters()
    {
        return $this->hasMany(CommercialCenter::class, 'manage_organization_id');
    }

    //List các tin phản hồi, liên hệ gửi đến CQQL này
    public function contacts()
    {
        return $this->hasMany(Contact::class);
    }

    //List các ảnh của cơ sở này
    public function organizationImages()
    {
        return $this->hasMany(OrganizationImage::class);
    }

    //List các tin giới thiệu mở rộng của cơ sở này
    public function organizationInfoDetails()
    {
        return $this->hasMany(OrganizationInfoDetail::class);
    }

    //List hình thức thanh toán của cơ sở này
    public function payTypes()
    {
        return $this->belongsToMany(PayType::class, 'organization_pay_types')
                    ->withPivot('id', 'content', 'is_default');
    }

    //List hình thức vận chuyển của cơ sở này
    public function shipTypes()
    {
        return $this->belongsToMany(ShipType::class, 'organization_ship_types')
                    ->withPivot('id', 'content', 'is_default');
    }

    //List các sản phẩm của cơ sở này
    public function products()
    {
        return $this->hasMany(Product::class);
    }

    //List đối tác(gồm nhà cung cấp và nhà phân phối) của cơ sở này
    public function partners()
    {
        return $this->hasMany(Partner::class, 'organization_id');
    }

    //List người dùng thuộc CS hoặc CQQL này
    public function users()
    {
        return $this->hasMany(User::class);
    }

    //List các theo dõi (người) đang theo dõi Cơ sở này
    public function follows()
    {
        return $this->hasMany(Follow::class);
    }

    //List Cơ sở SXKD của CQQL này quản lý và CQQL cấp con của nó
    public function childs()
    {
        return $this->hasMany(Organization::class, 'manage_organization_id');
    }

    //Danh sách chứng nhận, xác nhận của doanh nghiệp
    public function certificates()
    {
        return $this->morphMany('App\Models\Certificate', 'certificateable');
    }

    //Lịch sử đơn hàng của doanh nghiệp
    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    //DS đánh giá đối với doanh nghiệp này
    public function ratings()
    {
        return $this->morphMany('App\Models\Rating', 'ratingable');
    }

    //DS người yêu thích đối với doanh nghiệp này
    public function favorites()
    {
        return $this->morphMany('App\Models\Favorite', 'favoriteable');
    }

    //DS thông tin đăng nhu cầu kết nối mua bán
    public function advertisings()
    {
        return $this->morphMany('App\Models\Advertising', 'advertisingable');
    }

    //DS câu hỏi đối với cơ sở này
    public function questionAnswers()
    {
        return $this->morphMany('App\Models\QuestionAnswer', 'question_answerable');
    }

    //DS phản ánh vi phạm
    public function infringements()
    {
        return $this->morphMany('App\Models\Infringement', 'infringementable');
    }

    //Object CQQL mà Cơ sở SXKD này được đề nghị chuyển đến
    public function moveOrganization()
    {
        return $this->belongsTo(Organization::class, 'move_organization_id');
    }

    //Get URL ảnh logo
    public function getLogo(int $width = 0, int $height = 0)
    {
        if (isset($this->logo_image)) {
            //return url("/image/$width/$height/{$this->logo_image}");
            return viewImage($this->logo_image, $width, $height);
        } else {
            return url('img/no-logo.jpg');
        }
    }

    public function sluggable(): array
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
}
