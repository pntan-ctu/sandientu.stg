<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class DocumentField
 *
 * @property int $id
 * @property string $name
 * @property int $no
 * @property int $active
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection|Document[] $documents
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class DocumentField extends Model
{
    protected $casts = [
        'no' => 'int',
        'active' => 'int'
    ];

    protected $fillable = [
        'name',
        'no',
        'active'
    ];

    public function documents()
    {
        return $this->hasMany(Document::class);
    }
}
