<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrderItem
 *
 * @property int $id
 * @property int $order_id
 * @property int $product_id
 * @property string $product_name
 * @property string $unit
 * @property float $price
 * @property int $amount
 * @property float $money
 *
 * @property Product $product
 * @property Order $order
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class OrderItem extends Model
{
    public $timestamps = false;

    protected $casts = [
        'order_id' => 'int',
        'product_id' => 'int',
        'price' => 'float',
        'amount' => 'int',
        'money' => 'float'
    ];

    protected $fillable = [
        'order_id',
        'product_id',
        'product_name',
        'unit',
        'price',
        'amount',
        'money'
    ];

    public function product()
    {
        return $this->belongsTo(Product::class);
    }

    public function order()
    {
        return $this->belongsTo(Order::class);
    }
}
