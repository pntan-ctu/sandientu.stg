<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Transportation extends Model
{
    use SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'transportations';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ship_type_id',
        'order_id',
        'weight',
        'amount_estimate',
        'time_estimate',
        'amount_final',
        'status',
        'time_delivery'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'ship_type_id' => 'int',
        'order_id' => 'int',
        'weight' => 'int',
        'amount_estimate' => 'float',
        'amount_final' => 'float',
        'status' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['time_delivery', 'created_at', 'updated_at', 'deleted_at'];
}
