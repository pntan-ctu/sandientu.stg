<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class AdvertisingImage
 *
 * @property int $id
 * @property int $advertising_id
 * @property string $image
 *
 * @property Advertising $advertising
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class AdvertisingImage extends Model
{
    public $timestamps = false;
    
    protected $casts = [
        'advertising_id' => 'int'
    ];

    protected $fillable = [
        'advertising_id',
        'image'
    ];

    public function advertising()
    {
        return $this->belongsTo(Advertising::class);
    }
}
