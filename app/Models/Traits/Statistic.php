<?php

namespace App\Models\Traits;

use Illuminate\Support\Facades\Redis;

trait Statistic
{
    private $viewKey="views";
    private $taggingProduct = "products";
    private $taggingOrganization = "organizations";
    private $cacheStore="schedule";
    private $prefix = "";
    private $redis;
    
    public function calcAvgRate()
    {
        return $this->ratings()->avg("rate");
    }
    
    public function calcCountRate()
    {
        return $this->ratings()->count();
    }
    
    public function calcCountOrder()
    {
        return $this->orders()
                ->whereIn("status", [2,4])
                ->count();
    }
    
    public function calcRank()
    {
        return (1000000 * $this->count_order) + (100 * $this->count_rate) + (10 * $this->avg_rate) + $this->count_view;
    }

    public function bookmarkViewProduct()
    {
        $this->redis=Redis::connection($this->cacheStore);
        $this->prefix = config("database.redis.{$this->cacheStore}.prefix");

        $key = "{$this->prefix}:{$this->taggingProduct}:{$this->viewKey}:{$this->id}";
        $rateCache = $this->redis->get($key);
        if (!$rateCache) {
            $this->redis->set($key, 1);
        }
    }

    public function bookmarkViewOrganization()
    {
        $this->redis=Redis::connection($this->cacheStore);
        $this->prefix = config("database.redis.{$this->cacheStore}.prefix");

        $key = "{$this->prefix}:{$this->taggingOrganization}:{$this->viewKey}:{$this->id}";
        $rateCache = $this->redis->get($key);
        if (!$rateCache) {
            $this->redis->set($key, 1);
        }
    }
}
