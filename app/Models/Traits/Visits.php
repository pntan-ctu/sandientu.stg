<?php
/**
 * Created by PhpStorm.
 * User: Tran Ba
 * Date: 9/18/2018
 * Time: 8:00 AM
 */

namespace App\Models\Traits;

use App\Models\Message;

trait Visits
{
    /**
     * Lấy đối tượng visits cho đối tượng cần thống kê
     *
     * @return \awssat\Visits\Visits
     */
    public function visits()
    {
        return visits($this);
    }

    /**
     * Lấy đối tượng visits cho loại đối tượng cần thống kê
     *
     * @return \awssat\Visits\Visits
     */
    public static function visitTypes()
    {
        return visits(self::class);
    }
    
    /*public function saveRedisView(){
        $redis = Redis::connection("schedule");
        $prefix = config("database.redis.schedule.prefix");
        $type=self::class;
        $key = "{$prefix}:{$type}:view:{$this->id}";
        $viewCache = $redis->get($key);
        if(!$viewCache){
            $redis->set("$key", 1);
        }
    }*/
}
