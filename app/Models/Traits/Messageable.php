<?php
/**
 * Created by PhpStorm.
 * User: Tran Ba
 * Date: 9/18/2018
 * Time: 8:00 AM
 */

namespace App\Models\Traits;

use App\Models\Message;

trait Messageable
{
    /**
     * Lấy tất cả các message gửi đến đối tượng hiện tại
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function getMessages()
    {
        return $this->morphMany('App\Models\Message', 'messageable')->orderBy('created_at', 'desc');
    }

    /**
     * Lấy tất cả các message gửi đến đối tượng hiện tại đã được đọc
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function readMessages()
    {
        return $this->getMessages()->whereNotNull('read_at');
    }

    /**
     * Lấy tất cả các message gửi đến đối tượng hiện tại chưa đọc
     *
     * @return \Illuminate\Database\Query\Builder
     */
    public function unreadMessages()
    {
        return $this->getMessages()->whereNull('read_at');
    }

    /**
     * Lấy các message mà đối tượng đã gửi
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphMany
     */
    public function sentMesages()
    {
        return $this->morphMany('App\Models\Message', 'sendable')->orderBy('created_at', 'desc');
    }

    /**
     * Thêm (gửi) thông báo cho đối tượng hiện tại (từ đối tượng $sendByObject)
     *
     * @return Message
     */
    public function addMesage($subject, $content, $sendByObject)
    {
        $messge = new Message();
        $messge->subject = $subject;
        $messge->content = $content;
        $messge->sendable()->associate($sendByObject);
        $messge->created_by = getUserId();

        $this->getMessages()->save($messge);
        return $messge;
    }

    /**
     * Gửi thông báo tới đối tượng $sendToObject (từ đối tượng hiện tại)
     *
     * @return Message
     */
    public function sendMesage($subject, $content, $sendToObject)
    {
        $messge = new Message();
        $messge->subject = $subject;
        $messge->content = $content;
        $messge->sendable()->associate($this);
        $messge->created_by = getUserId();

        $sendToObject->messages()->save($messge);
        return $messge;
    }
}
