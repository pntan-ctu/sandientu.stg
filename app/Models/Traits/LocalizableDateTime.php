<?php
/**
 * Created by PhpStorm.
 * User: Tran Ba
 * Date: 9/18/2018
 * Time: 8:00 AM
 */

namespace App\Models\Traits;

trait LocalizableDateTime
{
    /**
     * Determine if the given value is a locale standard date format.
     *
     * @param  string $value
     * @return bool
     */
    protected function isLocaleDateFormat($value)
    {
        return preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d{4})$/', $value);
    }

    /**
     * Determine if the given value is a locale standard date time format.
     *
     * @param  string $value
     * @return bool
     */
    protected function isLocaleDateTimeFormat($value)
    {
        return preg_match('/^(\d{1,2})\/(\d{1,2})\/(\d{4}) (\d{1,2}):(\d{1,2})$/', $value);
    }

    /*
     * @inheritdoc
     */
    protected function asDateTime($value)
    {
        if (is_string($value)) {
            if ($this->isLocaleDateFormat($value)) {
                return parseDate($value);
            } elseif ($this->isLocaleDateTimeFormat($value)) {
                return parseDateTime($value);
            }
        }
        return parent::asDateTime($value);
    }
}
