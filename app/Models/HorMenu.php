<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class HorMenu
 *
 * @property int $id
 * @property int $parent_id
 * @property string $title
 * @property int $mtype
 * @property int $relate_id
 * @property string $link
 * @property int $no
 * @property int $active
 * @property int $new_tab
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property HorMenu $horMenu
 * @property Collection|HorMenu[] $horMenus
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class HorMenu extends Model
{
    protected $casts = [
        'parent_id' => 'int',
        'mtype' => 'int',
        'relate_id' => 'int',
        'no' => 'int',
        'active' => 'int',
        'new_tab' => 'int'
    ];

    protected $fillable = [
        'parent_id',
        'title',
        'mtype',
        'relate_id',
        'link',
        'no',
        'active',
        'new_tab'
    ];

    public function horMenu()
    {
        return $this->belongsTo(HorMenu::class, 'parent_id');
    }

    public function horMenus()
    {
        return $this->hasMany(HorMenu::class, 'parent_id');
    }
}
