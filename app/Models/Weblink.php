<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Weblink
 *
 * @property int $id
 * @property string $title
 * @property string $url
 * @property string $img_path
 * @property int $no
 * @property int $created_by
 * @property int $group_id
 * @property int $active
 * @property int $new_window
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Weblink extends Model
{
    protected $casts = [
        'no' => 'int',
        'created_by' => 'int',
        'group_id' => 'int',
        'active' => 'int',
        'new_window' => 'int'
    ];

    protected $fillable = [
        'title',
        'url',
        'img_path',
        'no',
        'created_by',
        'group_id',
        'active',
        'new_window'
    ];
}
