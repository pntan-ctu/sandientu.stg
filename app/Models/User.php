<?php

namespace App\Models;

use Laravel\Passport\HasApiTokens;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Models\Traits\LocalizableDateTime;
use App\Models\Traits\Messageable;
use Silber\Bouncer\Database\HasRolesAndAbilities;
use Cmgmyr\Messenger\Traits\Messagable as MessengerMessagable;

/**
 * Class User
 *
 * @property int $id
 * @property string $email
 * @property string $password
 * @property string $name
 * @property int $organization_id
 * @property string $tel
 * @property string $remember_token
 * @property int $active
 * @property Carbon $email_verified_at
 * @property string $password_reset
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Organization $organization
 * @property Collection|Blacklist[] $blacklists
 * @property Collection|Advertising[] $advertisings
 * @property Collection|Favorite[] $favorites
 * @property Collection|Follow[] $follows
 * @property SocialAccount $socialAccount
 * @property Collection|Rating[] $ratings
 * @property UserProfile $userProfile
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;
    use LocalizableDateTime;
    use Messageable;
    use HasRolesAndAbilities;
    use HasApiTokens;
    use MessengerMessagable;

    protected $casts = [
        'organization_id' => 'int',
        'active' => 'int'
    ];

    protected $dates = [
        'email_verified_at',
        'deleted_at'
    ];

    protected $hidden = [
        'password',
        'remember_token'
    ];

    protected $fillable = [
        'email',
        'password',
        'name',
        //'organization_id',
        'tel',
        'remember_token',
        'active',
        //'email_verified_at',
        //'password_reset'
    ];

    public function blacklists()
    {
        return $this->hasMany(Blacklist::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function favorites()
    {
        return $this->hasMany(Favorite::class);
        //return $this->morphMany('App\Models\Favorite', 'favoriteable');
    }

    public function ratings()
    {
        return $this->hasMany(Rating::class, 'create_by');
        //return $this->morphMany('App\Models\Rating', 'ratingable');
    }

    public function follows()
    {
        return $this->hasMany(Follow::class);
    }

    public function socialAccount()
    {
        return $this->hasOne(SocialAccount::class);
    }

    public function userProfile()
    {
        return $this->hasOne(UserProfile::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'user_roles')
            ->withPivot('id');
    }

    //Lịch sử đơn hàng của người dùng
    public function orders()
    {
        return $this->morphMany('App\Models\Order', 'orderable');
    }

    //DS thông tin đăng nhu cầu kết nối mua bán
    public function advertisings()
    {
        return $this->morphMany('App\Models\Advertising', 'advertisingable');
    }

    //DS phản ánh vi phạm
    public function infringements()
    {
        return $this->morphMany('App\Models\Infringement', 'infringementable');
    }

    //Get URL ảnh avartar
    public function getAvatar(int $width = 0, int $height = 0)
    {
        $userProfile = $this->userProfile;
        if (isset($userProfile->avatar)) {
            //return url("/image/$width/$height/{$userProfile->avatar}");
            return viewImage($userProfile->avatar, $width, $height);
        } else {
            $socialAccount = $this->socialAccount;
            if (isset($socialAccount->avatar)) {
                return  $socialAccount->avatar;
            } else {
                return url('img/no-avatar.jpg');
            }
        }
    }

    //Lấy url của trang hoạt động của cá nhân
    public function getUrlActivity()
    {
        return route('user-activity', ['username' => $this->email]);
    }

    //Tìm user by username
    public static function findForUrlActivity($username)
    {
        return self::where('email', '=', $username)->first();
    }
}
