<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class QuestionAnswer
 *
 * @property int $id
 * @property string $question_answerable_type
 * @property int $question_answerable_id
 * @property int $parent_id
 * @property string $content
 * @property int $status
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property QuestionAnswer $questionAnswer
 * @property Collection|QuestionAnswer[] $questionAnswers
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class QuestionAnswer extends Model
{
    use SoftDeletes;

    protected $casts = [
        'question_answerable_id' => 'int',
        'parent_id' => 'int',
        'status' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int',
        'deleted_by' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'question_answerable_type',
        'question_answerable_id',
        'parent_id',
        'content',
        'status',
        'created_by',
        'updated_by',
        'deleted_by'
    ];

    public function questionAnswerable()
    {
        return $this->morphTo();
    }
    
    public function questionAnswer()
    {
        return $this->belongsTo(QuestionAnswer::class, 'parent_id');
    }

    public function questionAnswers()
    {
        return $this->hasMany(QuestionAnswer::class, 'parent_id');
    }
    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
