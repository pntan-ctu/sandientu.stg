<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationType
 *
 * @property int $id
 * @property string $name
 * @property string $description
 *
 * @property Collection|Menu[] $menus
 * @property Collection|Module[] $modules
 * @property Collection|OrganizationInfoCfg[] $organizationInfoCfgs
 * @property Collection|Organization[] $organizations
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class OrganizationType extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name',
        'description'
    ];

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function modules()
    {
        return $this->hasMany(Module::class);
    }

    public function organizationInfoCfgs()
    {
        return $this->hasMany(OrganizationInfoCfg::class);
    }

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }

    public function roles()
    {
        return $this->hasMany(Role::class);
    }
}
