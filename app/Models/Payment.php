<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class Payment
 *
 * @property int $id
 * @property int $pay_type_id
 * @property string $currency
 * @property string $description - Thanh toán đơn hàng orderId1_orderId2_orderId3...
 * @property float $amount
 * @property string $bank
 * @property string $transaction_id
 * @property string $response_code
 * @property Carbon $pay_date
 * @property string $pay_code - md5(orderId1_orderId2_orderId3...)
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property PayType $payType
 *
 * @package App\Models
 */
class Payment extends Model
{
    use SoftDeletes;

    protected $casts = [
        'pay_type_id' => 'int',
        'amount' => 'float'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'pay_type_id',
        'currency',
        'description',
        'amount',
        'bank',
        'transaction_id',
        'response_code',
        'pay_date',
        'pay_code'
    ];

    public function payType()
    {
        return $this->belongsTo(PayType::class);
    }
}
