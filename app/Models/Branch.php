<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;
use Cviebrock\EloquentSluggable\Sluggable;

/**
 * Class Branch
 *
 * @property int $id
 * @property string $path
 * @property int $organization_id
 * @property int $region_id
 * @property string $name
 * @property string $address
 * @property string $tel
 * @property string $email
 * @property float $map_lat
 * @property float $map_long
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Organization $organization
 * @property Region $region
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Branch extends Model
{
    use Sluggable;
    protected $casts = [
        'organization_id' => 'int',
        'region_id' => 'int',
        'map_lat' => 'float',
        'map_long' => 'float'
    ];

    protected $fillable = [
        'path',
        'organization_id',
        'region_id',
        'name',
        'address',
        'tel',
        'email',
        'map_lat',
        'map_long'
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }
    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
}
