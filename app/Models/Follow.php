<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Follow
 *
 * @property int $id
 * @property int $organization_id
 * @property int $user_id
 *
 * @property User $user
 * @property Organization $organization
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Follow extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_id' => 'int',
        'user_id' => 'int'
    ];

    protected $fillable = [
        'organization_id',
        'user_id'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
