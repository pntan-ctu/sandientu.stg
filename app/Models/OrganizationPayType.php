<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class OrganizationPayType
 *
 * @property int $id
 * @property int $organization_id
 * @property int $pay_type_id
 * @property string $content
 * @property int $is_default
 *
 * @property PayType $payType
 * @property Organization $organization
 *
 * @package App\Models
 */
class OrganizationPayType extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_id' => 'int',
        'pay_type_id' => 'int',
        'is_default' => 'int'
    ];

    protected $fillable = [
        'organization_id',
        'pay_type_id',
        'content',
        'is_default'
    ];

    public function payType()
    {
        return $this->belongsTo(PayType::class);
    }
    
    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
