<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Module
 *
 * @property int $id
 * @property int $organization_type_id
 * @property string $name
 * @property string $group_name
 * @property string $url
 * @property int $no
 * @property string $ability_name
 * @property string $ability_type
 * @property bool $allow_gov
 *
 * @property OrganizationType $organizationType
 * @property Collection|Menu[] $menus
 * @property Collection|Role[] $roles
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Module extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_type_id' => 'int',
        'no' => 'int',
        'allow_gov' => 'bool'
    ];

    protected $fillable = [
        'organization_type_id',
        'name',
        'group_name',
        'url',
        'no',
        'ability_name',
        'ability_type',
        'allow_gov'
    ];

    public function organizationType()
    {
        return $this->belongsTo(OrganizationType::class);
    }

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class, 'module_roles')
            ->withPivot('id');
    }
}
