<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

//use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Class OrganizationImage
 *
 * @property int $id
 * @property int $organization_id
 * @property string $image
 *
 * @property Organization $organization
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class OrganizationImage extends Model
{
    public $timestamps = false;

    protected $casts = [
        'organization_id' => 'int'
    ];

    protected $fillable = [
        'organization_id',
        'image'
    ];

    public function organization()
    {
        return $this->belongsTo(Organization::class);
    }
}
