<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Traits\Messageable;

/**
 * Class Order
 *
 * @property int $id
 * @property string $orderable_type
 * @property int $orderable_id
 * @property int $provider_organization_id
 * @property string $code
 * @property Carbon $order_date
 * @property string $customer_name
 * @property string $address
 * @property int $region_id
 * @property string $email
 * @property string $tel
 * @property float $total_money
 * @property string $comment
 * @property int $status
 * @property int $ship_type_id
 * @property string $ship_type_content
 * @property int $pay_type_id
 * @property string $pay_type_content
 * @property int $payment_id
 * @property int $created_by
 * @property int $updated_by
 * @property int $deleted_by
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Organization $organization
 * @property ShipType $shipType
 * @property PayType $payType
 * @property Collection|OrderItem[] $orderItems
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Order extends Model
{
    use SoftDeletes;
    use Messageable;

    protected $casts = [
        'orderable_id' => 'int',
        'provider_organization_id' => 'int',
        'region_id' => 'int',
        'total_money' => 'float',
        'status' => 'int',
        'ship_type_id' => 'int',
        'pay_type_id' => 'int',
        'payment_id' => 'int',
        'created_by' => 'int',
        'updated_by' => 'int',
        'deleted_by' => 'int'
    ];

    protected $dates = [
        'order_date',
        'deleted_at'
    ];

    protected $fillable = [
        'orderable_type',
        'orderable_id',
        'provider_organization_id',
        'code',
        'order_date',
        'customer_name',
        'address',
        'region_id',
        'email',
        'tel',
        'total_money',
        'comment',
        'status',
        'ship_type_id',
        'ship_type_content',
        'pay_type_id',
        'pay_type_content',
        'payment_id',
        //'created_by',
        //'updated_by',
        //'deleted_by'
    ];

    protected $appends = ['status_text','order_date_text'];
    
    public function organization()
    {
        return $this->belongsTo(Organization::class, 'provider_organization_id');
    }

    public function shipType()
    {
        return $this->belongsTo(ShipType::class);
    }

    public function payType()
    {
        return $this->belongsTo(PayType::class);
    }

    public function orderItems()
    {
        return $this->hasMany(OrderItem::class);
    }

    /**
     * Get customer object: org or user
     */
    public function orderable()
    {
        return $this->morphTo();
    }
    
    public function getStatusTextAttribute()
    {
        //1: Chưa xử lý; 2: Đã xử lý; 3: Đã hủy; 4: Xác nhận giao dịch thành công(của khách hàng)
        $returnVal="";
        switch ($this->status) {
            case 1:
                $returnVal="Chưa xử lý";
                break;
            case 2:
                $returnVal="Đã xử lý";
                break;
            case 3:
                $returnVal="Đã hủy";
                break;
            default:
                $returnVal="Thành công";
                break;
        }
        return $returnVal;
    }
    
    public function getOrderDateTextAttribute()
    {
        return isset($this->order_date) ? $this->order_date->format("H:i:s d/m/Y") : null;
    }
}
