<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Region
 *
 * @property int $id
 * @property int $parent_id
 * @property string $name
 * @property int $level
 * @property string $type
 * @property string $code
 * @property string $parent_code
 * @property string $path_primary_key
 * @property string $path_code
 * @property string $path_name
 * @property string $full_name
 *
 * @property Region $region
 * @property Collection|Advertising[] $advertisings
 * @property Collection|Area[] $areas
 * @property Collection|Branch[] $branches
 * @property Collection|CommercialCenter[] $commercialCenters
 * @property Collection|Organization[] $organizations
 * @property Collection|Region[] $regions
 * @property Collection|UserProfile[] $userProfiles
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Region extends Model
{
    public $timestamps = false;

    protected $casts = [
        'parent_id' => 'int',
        'level' => 'int'
    ];

    protected $fillable = [
        'parent_id',
        'name',
        'level',
        'type',
        'code',
        'parent_code',
        'path_primary_key',
        'path_code',
        'path_name',
        'full_name'
    ];

    public function region()
    {
        return $this->belongsTo(Region::class, 'parent_id');
    }

    public function advertisings()
    {
        return $this->hasMany(Advertising::class);
    }
    
    public function areas()
    {
        return $this->hasMany(Area::class);
    }

    public function branches()
    {
        return $this->hasMany(Branch::class);
    }

    public function commercialCenters()
    {
        return $this->hasMany(CommercialCenter::class);
    }

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }

    public function regions()
    {
        return $this->hasMany(Region::class, 'parent_id');
    }

    public function userProfiles()
    {
        return $this->hasMany(UserProfile::class);
    }
}
