<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Cviebrock\EloquentSluggable\Sluggable;
use App\Models\Traits\Visits;

/**
 * Class Area
 *
 * @property int $id
 * @property string $path
 * @property int $manage_organization_id
 * @property int $region_id
 * @property string $name
 * @property string $map
 * @property string $instroduction
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $deleted_at
 *
 * @property Region $region
 * @property Collection|Organization[] $organizations
 *
 * @package App\Models
 * @mixin \Eloquent
 */
class Area extends Model
{
    use SoftDeletes;
    use Sluggable;
    use Visits;

    protected $casts = [
        'manage_organization_id' => 'int',
        'region_id' => 'int'
    ];

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        //'path',
        'manage_organization_id',
        'region_id',
        'name',
        'map',
        'instroduction'
    ];

    public function organizations()
    {
        return $this->hasMany(Organization::class);
    }

    public function region()
    {
        return $this->belongsTo(Region::class);
    }

    public function manageOrganization()
    {
        return $this->belongsTo(Organization::class, 'manage_organization_id');
    }

    public function sluggable()
    {
        return [
            'path' => [
                'source' => 'name'
            ]
        ];
    }
}
