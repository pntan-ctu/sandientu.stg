<?php
namespace App\Observers;

use App\Models\Rating;
use Illuminate\Support\Facades\Redis;

class RatingObserver
{
    private $ratingKey="ratings";
    private $taggingProduct = "products";
    private $taggingOrganization = "organizations";
    private $cacheStore="schedule";
    private $prefix = "";
    private $redis;
    
    public function __construct()
    {
        $this->redis=Redis::connection($this->cacheStore);
        $this->prefix = config("database.redis.{$this->cacheStore}.prefix");
    }
    
    public function created(Rating $rating)
    {
        if ($rating->ratingable_type=="App\Models\Product") {
            $key = "{$this->prefix}:{$this->taggingProduct}:{$this->ratingKey}:{$rating->ratingable_id}";
            $rateCache = $this->redis->get($key);
            if (!$rateCache) {
                $this->redis->set($key, 1);
            }
        } else {
            $key = "{$this->prefix}:{$this->taggingOrganization}:{$this->ratingKey}:{$rating->ratingable_id}";
            $rateCache = $this->redis->get($key);
            if (!$rateCache) {
                $this->redis->set($key, 1);
            }
        }
    }
}
