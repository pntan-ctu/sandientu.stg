<?php
namespace App\Observers;

use App\Models\Order;
use Illuminate\Support\Facades\Redis;

class OrderObserver
{
    private $orderKey="orders";
    private $taggingProduct = "products";
    private $taggingOrganization = "organizations";
    private $cacheStore="schedule";
    private $prefix = "";
    private $redis;
    
    public function __construct()
    {
        $this->redis=Redis::connection($this->cacheStore);
        $this->prefix = config("database.redis.{$this->cacheStore}.prefix");
    }
    
    public function updated(Order $order)
    {
        if ($order->status==2) {
            $key = "{$this->prefix}:{$this->taggingOrganization}:{$this->orderKey}:{$order->provider_organization_id}";
            $ordCache = $this->redis->get($key);
            if (!$ordCache) {
                $this->redis->set($key, 1);
                $this->pushToCacheOrderItem($order);
            }
        }
    }
    
    private function pushToCacheOrderItem(Order $order)
    {
        $orderItems = $order->orderItems();
       
        foreach ($orderItems as $item) {
            $key = "{$this->prefix}:{$this->taggingProduct}:{$this->orderKey}:{$item->product_id}";
            $orderItemCache=$this->redis->get($key);
            if (!$orderItemCache) {
                $this->redis->set($key, 1);
            }
        }
    }
}
