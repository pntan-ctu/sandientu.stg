<?php

namespace App\Events;

use App\Models\Thread;
use Cmgmyr\Messenger\Models\Message;
use App\Models\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class MessageSent implements ShouldBroadcast
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Message details
     *
     * @var Message
     */
    private $message;

    /**
     * Is new thread
     *
     * @var bool
     */
    private $isNewThread;

    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Message $message, $isNewThread)
    {
        $this->message = $message;
        $this->isNewThread = $isNewThread;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        $channels = [];

        foreach ($this->message->participants as $participant) {
            array_push($channels, new PrivateChannel('user.messages.' . $participant->user_id));
        }

        return $channels;
    }

    /**
     * Get the data to broadcast.
     *
     * @return array
     */
    public function broadcastWith()
    {
        $this->message->setVisible(['id', 'body', 'thread_id', 'created_at']);
        $user = $this->message->user;
        $user->load('userProfile:user_id,avatar');
        $user->setVisible(['id', 'name', 'userProfile']);
        $data = [
            'message' => $this->message,
            'user' => $user,
        ];
        if ($this->isNewThread) {
            $thread = $this->message->thread;
            $thread->load('organization:id,name');
            $thread->setVisible(['id', 'subject', 'organization']);
            $data['thread'] = $thread;
        }
        return $data;
    }
}
