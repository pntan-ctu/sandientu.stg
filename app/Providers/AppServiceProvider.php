<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Silber\Bouncer\BouncerFacade as Bouncer;
use App\Models\Order;
use App\Models\Rating;
use App\Observers\OrderObserver;
use App\Observers\RatingObserver;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Bouncer::tables([
            'roles' => 'bouncer_roles',
            'assigned_roles' => 'bouncer_assigned_roles',
            'abilities' => 'bouncer_abilities',
            'permissions' => 'bouncer_permissions',
        ]);
        //Bouncer::cache();
        Order::observe(OrderObserver::class);
        Rating::observe(RatingObserver::class);

        //Schema::defaultStringLength(255);

        // Relation::morphMap([
        //     'users' => 'App\Models\User',
        //     'organizations' => 'App\Models\Organization',
        // ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
    }
}
