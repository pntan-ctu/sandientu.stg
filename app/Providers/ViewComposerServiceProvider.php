<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewComposerServiceProvider extends ServiceProvider
{
    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('layouts.includes.menu-gov', 'App\Http\ViewComposers\MenuGovComposer');
        View::composer('layouts.includes.menu-org', 'App\Http\ViewComposers\MenuOrgComposer');
        View::composer('layouts.includes.menu-web-hor', 'App\Http\ViewComposers\MenuHorComposer');
        View::composer('layouts.includes.menu-web-product', 'App\Http\ViewComposers\MenuProductComposer');
        View::composer('web.products.productscate_left', 'App\Http\ViewComposers\MenuProductLeftComposer');
    }
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
