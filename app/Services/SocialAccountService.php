<?php

namespace App\Services;

use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Contracts\User as ProviderUser;
use App\Models\SocialAccount;
use App\Models\User;

class SocialAccountService
{
    public static function createOrGetUser(ProviderUser $providerUser, $social)
    {
        $account = SocialAccount::whereProvider($social)
            ->whereProviderUserId($providerUser->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {
            $email = $providerUser->getEmail(); //?? $providerUser->getNickname();

            if (strlen($email) <= 0) {
                return null;
            }

            $account = new SocialAccount([
                'provider_user_id' => $providerUser->getId(),
                'provider' => $social,
                'nick_name' => $providerUser->getNickname(),
                'avatar' => $providerUser->getAvatar()
            ]);

            $user = User::whereEmail($email)->first();

            if (!$user) {
                $pass = str_random(32);
                $user = User::create([
                    'email' => $email,
                    'name' => $providerUser->getName(),
                    'password' => Hash::make($pass),
                ]);
            }

            $account->user()->associate($user);
            $account->save();

            return $user;
        }
    }
}
