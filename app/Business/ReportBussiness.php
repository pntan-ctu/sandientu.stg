<?php
namespace App\Business;

use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Alignment;

class ReportBussiness
{
    public static function exportExample($type)
    {
        $objPHPExcel = IOFactory::load(resource_path("templates/report.xls"));

        $sheet = $objPHPExcel->getSheet(0);
        $sheet->setShowGridlines(false);
       
        //1. MergeCell theo range:
        $sheet->mergeCells("A7:H7")->setCellValue("A7", "Đây là mergeCells(range)");
        //2. MergeCell theo cac gia tri chi so column va row (start from 1)
        $sheet->mergeCellsByColumnAndRow(1, 8, 8, 10)
              ->setCellValue("A8", "Đây là mergeCellsByColumnAndRow(col1,row1,col2,row2)");

        //3. Cac kieu fill du lieu vao Cell:
        $sheet->setCellValue("A11", "1")                                 //Dien du lieu vao o A11
              ->setCellValueByColumnAndRow(2, 11, "Cơ sở SX A")         //Dien du lieu vao cot 1, dong 11
              ->setCellValueExplicitByColumnAndRow(
                  4,
                  11,
                  "0123456789",
                  DataType::TYPE_STRING
              );                           //Kieu gia tri cua o

        //4. Dinh dang cho Range
        $sheet->getStyle("A7:H11")->applyFromArray(
            array(
                    "borders"=>array(
                        "outline"=>array("borderStyle"=>Border::BORDER_THIN),
                        "horizontal"=> array("borderStyle"=>Border::BORDER_HAIR),
                        "vertical"=> array("borderStyle"=>Border::BORDER_THIN)
                    ),
                    "alignment"=>array(
                        "horizontal"=> Alignment::HORIZONTAL_CENTER,
                        "vertical"=> Alignment::VERTICAL_CENTER
                    ),
                    "font"=>array(
                        "bold"=>false,
                        "italic"=>true
                    )
                )
        );
        
        $fileType="Html";
        if ($type=="excel") {
            $fileName="report_download.xls";
            header('Content-Type: application/vnd.ms-excel');
            $fileType="Xls";
            header("Content-Disposition: attachment;filename=$fileName");
            header('Cache-Control: max-age=0');
        } elseif ($type=='pdf') {
            $fileName="report_download.pdf";
            header('Content-Type: application/pdf');
            $fileType="Mpdf";
            header("Content-Disposition: attachment;filename=$fileName");
            header('Cache-Control: max-age=0');
        }
        $objWriter = IOFactory::createWriter($objPHPExcel, $fileType);
        $objWriter->save('php://output');
    }
}
