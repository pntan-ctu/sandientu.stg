<?php

namespace App\Business;

use App\Models\Organization;
use App\Models\Branch;
use App\Models\CommercialCenter;

class MapsBussiness
{
    /**
     * Get location
     */
    /*
    public static function getMapsLocation1()
    {
        $arOrg = array();
        $organization101 = Organization::where('organization_type_id', '>=', '100')->get(); // san xuat & kinh doanh
        foreach ($organization101 as $key) {
            $org =  array();
            $org["path"] = $key["path"];
            $org["name"] = $key["name"];
            $org["address"] = $key["address"];
            $org["tel"] = $key["tel"];
            $org["email"] = $key["email"];
            $org["website"] = $key["website"];
            $org["map_lat"] = $key["map_lat"];
            $org["map_long"] = $key["map_long"];
            $org["organization_type_id"] = $key["organization_type_id"];
            $arOrg[] = $org;
        }
        $branch = Branch::get();
        foreach ($branch as $key) {
            $org =  array();
            $org["path"] = $key["path"];
            $org["name"] = $key["name"];
            $org["address"] = $key["address"];
            $org["tel"] = $key["tel"];
            $org["email"] = $key["email"];
            $org["website"] = $key["website"];
            $org["map_lat"] = $key["map_lat"];
            $org["map_long"] = $key["map_long"];
            $org["organization_type_id"] = 0;
            $arOrg[] = $org;
        }
        $commer = CommercialCenter::get();
        foreach ($commer as $key) {
            $org =  array();
            $org["path"] = $key["path"];
            $org["name"] = $key["name"];
            $org["address"] = $key["address"];
            $org["tel"] = "";
            $org["email"] = "";
            $org["website"] = "";
            $org["map_lat"] = $key["map_lat"];
            $org["map_long"] = $key["map_long"];
            $org["organization_type_id"] = 1;
            $arOrg[] = $org;
        }
        return $arOrg;
    }
    */
    public static function getMapsLocation($orgtype, $keyword)
    {
        $arOrg = array();
        $organization101 = array();
        if ($orgtype == "0") {
            if ($keyword <> "") {
                $organization101 = Organization::where('organization_type_id', '=', '101')
                    ->where('name', 'like', "%$keyword%")->get();
            } else {
                $organization101 = Organization::where('organization_type_id', '=', '101')->get();
            }
        } elseif ($orgtype == "1") {
            if ($keyword <> "") {
                $organization101 = Organization::where('organization_type_id', '=', '100')
                    ->where('name', 'like', "%$keyword%")->get();
            } else {
                $organization101 = Organization::where('organization_type_id', '=', '100')->get();
            }
        } elseif ($orgtype == "-1") {
            if ($keyword <> "") {
                $organization101 = Organization::where('organization_type_id', '>=', '100')
                    ->where('name', 'like', "%$keyword%")->get();
            } else {
                $organization101 = Organization::where('organization_type_id', '>=', '100')->get();
            }
        }
        foreach ($organization101 as $key) {
            $org =  array();
            $org["path"] = $key["path"];
            $org["name"] = htmlentities($key["name"]);
            $org["address"] = htmlentities($key["address"]);
            $org["tel"] = $key["tel"];
            $org["email"] = $key["email"];
            $org["website"] = $key["website"];
            $org["map_lat"] = $key["map_lat"];
            $org["map_long"] = $key["map_long"];
            $org["organization_type_id"] = $key["organization_type_id"];
            $arOrg[] = $org;
        }
        if ($orgtype == "-1" || $orgtype == "2") {
            if ($keyword <> "") {
                $branch = Branch::where('name', 'like', "%$keyword%")->get();
            } else {
                $branch = Branch::get();
            }
            foreach ($branch as $key) {
                $org =  array();
                $org["path"] = $key["path"];
                $org["name"] = htmlentities($key["name"]);
                $org["address"] =  htmlentities($key["address"]);
                $org["tel"] = $key["tel"];
                $org["email"] = $key["email"];
                $org["website"] = $key["website"];
                $org["map_lat"] = $key["map_lat"];
                $org["map_long"] = $key["map_long"];
                $org["organization_type_id"] = 0;
                $arOrg[] = $org;
            }
        }
        if ($orgtype == "-1" || $orgtype == "3") {
            if ($keyword <> "") {
                $commer = CommercialCenter::where('name', 'like', "%$keyword%")->get();
            } else {
                $commer = CommercialCenter::get();
            }
            foreach ($commer as $key) {
                $org =  array();
                $org["path"] = $key["path"];
                $org["name"] = htmlentities($key["name"]);
                $org["address"] = htmlentities($key["address"]);
                $org["tel"] = "";
                $org["email"] = "";
                $org["website"] = "";
                $org["map_lat"] = $key["map_lat"];
                $org["map_long"] = $key["map_long"];
                $org["organization_type_id"] = 1;
                $arOrg[] = $org;
            }
        }
        return $arOrg;
    }
}
