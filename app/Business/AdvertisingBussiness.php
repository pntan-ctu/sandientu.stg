<?php
namespace App\Business;

use App\Authors\AdvertisingAuthor;
use Illuminate\Support\Facades\DB;
use App\Models\Advertising;
use App\Models\AdvertisingImage;
use App\Business\OrganizationBusiness;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\Validator;

class AdvertisingBussiness
{
    protected $advertisingAuthor;

    public function __construct()
    {
        $this->advertisingAuthor = new AdvertisingAuthor();
    }

    /*
     *  type:
     *   0: Tất cả (lẫn lộn)
     *   1: Mua
     *   2: Bán
     *   3: Đối tác
     */
    public static function fullTextSearch($type, $strSearch, $pageNumber, $numberOfRowOnPage)
    {
        $daysToQuery = 90;
        $totalRow = DB::select(
            "SELECT COUNT(a.id) as row_count FROM advertisings a " .
                "WHERE MATCH(a.title,a.content) AGAINST(? IN NATURAL LANGUAGE MODE) ".
                "and a.status=1 and (a.category_id=? OR ?=0) and a.deleted_at is null and ".
                "(a.created_at BETWEEN DATE_ADD(sysdate(),INTERVAL -{$daysToQuery} DAY) AND sysdate())",
            [$strSearch,$type,$type]
        )[0]->row_count;

        if ($totalRow<$numberOfRowOnPage) {
            $pageNumber=1;
        }

        $start = ($pageNumber-1)*$numberOfRowOnPage;

        $numberOfRowNext = $totalRow - ($start + $numberOfRowOnPage);
        if ($numberOfRowNext<0) {
            $numberOfRowNext=0;
        }

        $totalPage = ceil($totalRow/$numberOfRowOnPage);

        /* Nếu có ảnh của tin "cung cầu" thì lấy ảnh đầu tiên
         * Nếu không có thì:
         *  - Nếu là "cá nhân" đăng tin => trả về Avatar của cá nhân
         *  - Nếu là "tổ chức" đăng tin => trả về logo_image của tổ chức, nếu logo null thì lại lấy ảnh cá nhân.
        */
        $imageField = "IFNULL((SELECT ai.image FROM advertising_images ai " .
                "WHERE ai.advertising_id=a.id ORDER BY ai.id LIMIT 0,1), " .
        "CASE WHEN a.advertisingable_type='App\Models\User' THEN " .
                        "(SELECT up.avatar " .
                        "FROM users u1 INNER JOIN user_profiles up on u1.id=up.user_id WHERE u1.id=u.id) " .
                    "ELSE IFNULL(" .
                            "(SELECT o.logo_image FROM organizations o WHERE o.id=a.advertisingable_id),".
                            "(SELECT up1.avatar FROM users u3 " .
                            "INNER JOIN user_profiles up1 on u3.id=up1.user_id WHERE u3.id=u.id)) " .
        "END) as image,";

        $data = DB::select("SELECT a.id,a.path,a.title,a.content,a.address,a.tel,a.category_id,a.created_at," .
                        "u.name,$imageField MATCH(a.title,a.content) AGAINST(? IN NATURAL LANGUAGE MODE) as score " .
                            "FROM advertisings a " .
                            "INNER JOIN users u ON a.created_by=u.id " .
                            "WHERE MATCH(a.title,a.content) AGAINST(? IN NATURAL LANGUAGE MODE) " .
                            "and a.status=1 and (a.category_id=? OR ?=0) and a.deleted_at is null and " .
                            "(a.created_at BETWEEN DATE_ADD(sysdate(),INTERVAL -{$daysToQuery} DAY) AND sysdate())" .
                            "ORDER BY score desc, a.created_at desc " .
                            "LIMIT ?, ?;", [$strSearch,$strSearch,$type,$type,$start,$numberOfRowOnPage]);

        return array(
            "total_page"=>$totalPage,
            "total_row"=>$totalRow,
            "number_of_row_next"=>$numberOfRowNext,
            "data"=>$data);
    }

    public static function search($search, $status, $pageSize = 10, $isOrganization = true)
    {
        //$advertisingable =  getAccessingObject();
        $advertisingable = null;
        if ($isOrganization) {
            $advertisingable = getOrgModel();
        } else {
            $advertisingable =  auth()->user();
        }
        $queryBuilder = Advertising::from("advertisings")
                ->join("product_categories as pc", "advertisings.product_category_id", "=", "pc.id")
                ->where("advertisings.advertisingable_type", get_class($advertisingable))
                ->where("advertisings.advertisingable_id", $advertisingable->id)
                ->where(function ($query) use ($search) {
                    $query->where("advertisings.title", "like", "%" . $search . "%")
                          ->orWhere("advertisings.content", "like", "%" . $search . "%");
                });

        if ($status>=0) {
            $queryBuilder->where("advertisings.status", $status);
        }

        return $queryBuilder->orderBy("advertisings.created_at", "advertisings.desc")
                ->select(DB::raw("advertisings.*,pc.name as product_category," .
                "(select image from advertising_images where advertising_id=advertisings.id limit 0,1) as image"))
                ->paginate($pageSize);
    }

    public static function save($input)
    {
        $validator = Validator::make($input, [
            "product_category_id"=>"required|numeric",
            "category_id"=>"required|numeric",
            "region_id"=>"required|numeric",
            "title"=>"required|max:511",
            "to_date"=>"nullable|date",
            "address"=>"required|max: 255",
            "tel"=>"required|max:32",
            "link_product_id"=>"nullable|numeric"
        ]);

        if ($validator->fails()) {
            //Neu du lieu khong validate
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        $id = $input["id"] ?? 0;

        if ($id == 0) {
            $advertising = self::insertAdvertising($input);
            return array("success"=>true,"advertising_id"=>$advertising->id);
        } else {
            $advertising = self::updateAdvertising($input);
            if ($advertising) {
                return array("success"=>true,"advertising_id"=>$advertising->id);
            } else {
                return array("success"=>false,"msg"=>"Tin này không tồn tại! Vui lòng kiểm tra lại!");
            }
        }
    }

    private static function insertAdvertising($input, $isOrganization = true)
    {
        //$advertisingable =  getAccessingObject();
        $advertisingable = null;
        if ($isOrganization) {
            $advertisingable = getOrgModel();
        } else {
            $advertisingable =  auth()->user();
        }

        $advertising = Advertising::create([
            "advertisingable_type"=>get_class($advertisingable),
            "advertisingable_id"=>$advertisingable->id,
            "category_id"=>$input["category_id"],
            "product_category_id"=>$input["product_category_id"],
            "region_id"=>$input["region_id"],
            "title"=>$input["title"],
            "content"=>isset($input["content"])?$input["content"]:"",
            "link_product_id"=>isset($input["link_product_id"])?(int)$input["link_product_id"]:null,
            "address"=>$input["address"],
            "tel"=>$input["tel"],
            "fanpage"=>isset($input["fanpage"])?$input["fanpage"]:null,
            "from_date"=>isset($input["from_date"])?$input["from_date"]:date('Y-m-d'),
            "to_date"=>isset($input["to_date"])?$input["to_date"]:null,
            "status"=>0,
            "created_by"=>auth()->user()->id
        ]);

        if (isset($input["images"]) && is_array($input["images"])) {
            $images = $input["images"];
            self::uploadAdvertisingImage($images, $advertising->id);
        }

        return $advertising;
    }

    private static function updateAdvertising($input)
    {
        $advertising = Advertising::find($input["id"]);

        if (isset($advertising)) {
            $advertising->category_id = $input["category_id"];
            $advertising->product_category_id=$input["product_category_id"];
            $advertising->region_id=$input["region_id"];
            $advertising->title=$input["title"];
            $advertising->content = $input["content"] ?? "";
            $advertising->link_product_id=isset($input["link_product_id"])?(int)$input["link_product_id"]:null;
            $advertising->address=$input["address"];
            $advertising->tel=$input["tel"];
            $advertising->fanpage=$input["fanpage"] ?? null;
            $advertising->to_date=$input["to_date"] ?? null;

            $advertising->save();

            if (isset($input["images"]) && is_array($input["images"])) {
                $images = $input["images"];
                self::uploadAdvertisingImage($images, $advertising->id);
            }

            if (isset($input["imagesDelete"]) && is_array($input["imagesDelete"])) {
                //Xử lý xóa ảnh của Advertising
                foreach ($input["imagesDelete"] as $id) {
                    AdvertisingImage::where("id", $id)->where("advertising_id", $input["id"])->delete();
                }
            }
            return $advertising;
        } else {
            return ['success' => false, 'message' => 'Lỗi: Không tìm thấy tin cung cầu'];
        }
    }

    private static function uploadAdvertisingImage($aFiles, $advertising_id)
    {
        $aArrFiles = UploadFileUtil::multiUploadImageFile($aFiles);
        foreach ($aArrFiles as $file) {
            AdvertisingImage::create([
                "advertising_id" => $advertising_id,
                "image" => $file->path
            ]);
        }
        return $aArrFiles;
    }

    public static function delete(Advertising $advertising)
    {
        $advertising->delete();
        return array("success"=>true,"msg"=>"Xóa thành công!");
    }

    /*
     * dùng cho cơ quan quản lý
     */
    public function searchManage($search, $status, $pageSize = 10)
    {
        //Check quyền
        $this->advertisingAuthor->canDo();

        $queryBuilder = Advertising::join('advertising_categories as b', 'b.id', '=', 'advertisings.category_id')
            ->join('product_categories as c', 'c.id', '=', 'advertisings.product_category_id')
            ->join('users', 'users.id', '=', 'advertisings.created_by')
            ->select([DB::raw('advertisings.*,
                (select advertising_images.image from advertising_images
                    where advertising_images.advertising_id = advertisings.id limit 1) as image'),
                'b.name as advertising_category_name', 'c.name as product_category_name', 'users.name as nguoi_dang'])
            ->where(function ($query) use ($search) {
                $query->where("title", "like", "%" . $search . "%")
                    ->orWhere("content", "like", "%" . $search . "%");
            });
        if ($status >= 0) {
            $queryBuilder->where("status", $status);
        }
        return $queryBuilder->orderBy("created_at", "desc")->paginate($pageSize);
    }

    /*
     * dùng cho cơ quan quản lý
     */
    public function deleteManage($advertising_id)
    {
        $advertising = Advertising::findOrFail($advertising_id);

        //Check quyen
        $this->advertisingAuthor->canManage($advertising);

        $advertising->delete();
        return ["success"=>true, "msg"=>"Xóa thành công!"];
    }

    /*
     * dùng cho cơ quan quản lý
     * $request->status: 0 - chờ duyệt, 1 - duyệt, 2 - không duyệt
     */
    public function setStatusManage($advertising_id, $status)
    {
        $advertising = Advertising::findOrFail($advertising_id);

        //Check quyen
        $this->advertisingAuthor->canManage($advertising);

        $advertising->status = $status;
        $advertising->save();
        return ["success"=>true, "msg"=>"Cập nhật thành công!"];
    }
}
