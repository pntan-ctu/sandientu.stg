<?php

namespace App\Business;

use App\Models\ProductCategory;

class ProductCategoryBusiness
{
    public static function getTree()
    {
        $arrInput = ProductCategory::selectRaw("id,ifnull(parent_id,0) as parent_id,name")->get();
        
        $returnVal = self::buildTree($arrInput, 0);
        
        return $returnVal;
    }
    
    private static function buildTree(&$arrInput, $parent_id)
    {
        $returnVal = array();
        foreach ($arrInput as $key => $element) {
            if ($element["parent_id"]==$parent_id) {
                $children = self::buildTree($arrInput, $element["id"]);
                if (count($children)) {
                    $element["children"]=$children;
                }
                array_push($returnVal, $element);
                unset($arrInput[$key]);
            }
        }
        return $returnVal;
    }
}
