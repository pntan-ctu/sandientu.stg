<?php

namespace App\Business;

use App\Models\Partner;
use App\Models\Organization;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class PartnerBusiness
{
    /**
     * Get list menu by current user
     */
    public static function getAll($organizationId, $type, $request)
    {
        $model = Partner::where('organization_id', '=', $organizationId)->where('type', $type)
            ->select(['id', 'name', 'address', 'tel', 'email', 'website', 'created_at','link_organization_id'])
            ->orderBy('name', 'asc');
        return DataTables::eloquent($model)->filter(
            function ($query) use ($request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%')
                            ->orWhere('address', 'like', '%' . $keyword . '%');
                    });
                }
            }
        )->make();
    }
    public static function getAllOrgLink($organizationId, $type, $request)
    {
        $model = Organization::leftJoin('partners', function ($join) use ($organizationId) {
                $join->on('organizations.id', '=', 'partners.link_organization_id')
                     ->where('partners.organization_id', '=', $organizationId);
        })
            ->where('organization_type_id', '>=', 100)
            ->whereNull('partners.id')
            ->where('organizations.id', '<>', $organizationId)
            ->select(['organizations.id', 'organizations.name', 'organizations.address',
                'organizations.tel', 'organizations.email', 'organizations.website', 'organizations.created_at']);
        return DataTables::eloquent($model)->filter(
            function ($query) use ($request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('organizations.name', 'like', '%' . $keyword . '%')
                            ->orWhere('organizations.address', 'like', '%' . $keyword . '%');
                    });
                }
            }
        )->make();
    }
}
