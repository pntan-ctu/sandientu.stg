<?php
namespace App\Business;

use App\Models\User;
use App\OrgType;
use Illuminate\Auth\Access\AuthorizationException;
use Illuminate\Support\Facades\Gate;
use App\Models\Organization;
use App\Business\OrganizationBusiness;
use Illuminate\Support\Facades\Validator;
use App\Models\UserRole;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Silber\Bouncer\BouncerFacade as Bouncer;

class UserBusiness
{
    public static function search($organization_id, $search, $active, $perPage = 10)
    {
        if (isUserBusiness()) {
            $organization_id = getOrgId();
        }
        
        $queryBuilder = User::where("users.organization_id", $organization_id)
            ->leftjoin('user_profiles', 'user_profiles.user_id', '=', 'users.id')
            ->selectRaw('users.*, user_profiles.avatar')
            ->where(function ($query) use ($search) {
                $query->where("users.name", "like", "%" . $search . "%")
                ->orWhere("users.email", "like", "%" . $search . "%");
            });
                
        if ($active>=0) {
            $queryBuilder->where("users.active", $active);
        }
        
        return $queryBuilder->orderBy("users.created_at", 'asc')->paginate($perPage);
    }
    
    public static function save($input)
    {
        $id = $input["id"] ?? 0;
        if ($id == 0) {
            $validator = Validator::make($input, [
                'name' => 'required',
                'email' => 'required|email|unique:users',
                'tel'=>'nullable|numeric',
                'active'=>'nullable|numeric'
            ]);
        } else {
            $validator = Validator::make($input, [
                'name' => 'required',
                'tel'=>'nullable|numeric',
                'active'=>'nullable|numeric'
            ]);
        }
        if ($validator->fails()) {
            //Neu du lieu khong validate
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }
        
        if ($id == 0) {
            $user = self::insert($input);
            return array("success"=>true,"user_id"=>$user->id);
        } else {
            $user = self::update($input);
            if ($user) {
                return array("success"=>true,"user_id"=>$user->id);
            } else {
                return array("success"=>false,"msg"=>"Người dùng không tồn tại. Vui lòng kiểm tra lại!");
            }
        }
    }
    
    private static function insert($input)
    {
        $sPass = str_random(8);
        
        $user = new User();
        $user->organization_id=$input["organization_id"];
        $user->name=$input["name"];
        $user->email=$input["email"];
        $user->password=Hash::make($sPass);
        $user->password_reset=$sPass;
        $user->tel= $input["tel"] ?? null;
        $user->active = $input["active"] ?? 0;
        
        $user->save();

        if (isset($input['roles']) && is_array($input['roles'])) {
            $roles = $input['roles'];
            foreach ($roles as $r) {
                $user->assign($r);
            }
        }
        return $user;
    }
    
    public static function update($input)
    {
        $user_id = (int)$input["id"];

        $user = User::find($user_id);

        if ($user) {
            $user->name = $input["name"];
            $user->active = $input["active"] ?? 0;
            $user->tel= $input["tel"] ?? null;
            $user->save();

            // remove roles
            $orgType = getOrgTypeId();
            $allRoles = Bouncer::role()->where('scope', '=', $orgType)->get();
            foreach ($allRoles as $cRole) {
                $user->retract($cRole->name);
            }

            // assign roles
            if (isset($input['roles']) && is_array($input['roles'])) {
                $roles = $input['roles'];
                foreach ($roles as $r) {
                    $user->assign($r);
                }
            }

            return $user;
        } else {
            return null;
        }
    }
    
    public static function delete(User $user)
    {
        $organization = $user->organization;
        $user->delete();
        return array("success" => true, "msg" => "Xóa người dùng thành công!");
    }
    
    public static function resetPass(User $user)
    {
        $organization = $user->organization;
        if (OrganizationBusiness::hasPermission($organization)) {
            $sPass = str_random(8);
            $user->password_reset=$sPass;
            $user->password=Hash::make($sPass);
            $user->save();
            return array("success" => true, "msg" => "Reset pass thành công!","password_reset"=>$sPass);
        } else {
            return array("success" => false, "msg" => "Bạn không có quyền thao tác trên cơ sở này!");
        }
    }
}
