<?php

namespace App\Business;

use App\Models\ProductInfoCfg;
use Illuminate\Support\Facades\DB;
use App\Models\Product;
use App\Models\Organization;
use App\Models\ProductImage;
use App\Models\ProductLink;
use App\Models\ProductInfoDetail;
use App\Models\Certificate;
use App\Constants;
use Illuminate\Support\Facades\Validator;
use App\Business\OrganizationBusiness;
use App\Utils\UploadFileUtil;

class ProductBussiness
{

    /**
     * Lấy danh sách Sản phẩm phục vụ view ngoài web
     * Đã check trạng thái được hiện thị hay không
     * Tra về query để có thể chèn thêm các ĐK khác
     */
    public static function getListForView()
    {
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }
    
    public static function getListBDS()
    {   
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.id_parent','=',40)
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }

    public static function getListNS()
    {   
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.id_parent','=',50)
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }

    public static function getListDVDL()
    {   
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.id_parent','=',53)
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }

    public static function getListDVVL()
    {   
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.id_parent','=',55)
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }
    public static function getListDADT()
    {   
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.id_parent','=',56)
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }
    /**
     * Lấy đối tượng Cơ sở bởi path phục vụ view chi tiết SP ngoài web
     * Đã check trạng thái được hiện thị hay không
     */
    public static function getModelForView($path)
    {
        $product = Product::where('path', $path)
            //->where('products.status', '>', 0) //Bỏ SP chưa KD
            //->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS thôi hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $product->first();
    }

    /* Params:
     *  - $strSearch: Chuỗi tìm kiếmT
     *  - $sortType: 0 - Theo độ chính xác; 1 - Theo Giá tăng dần; 2 - Theo giá giảm dần
     */

    public static function fullTextSearch($strSearch, $pageNumber, $numberOfRowOnPage, $sortType = 0)
    {
        $totalRow = DB::select(
            'SELECT count(p.id) as row_count FROM products p ' .
                'INNER JOIN organizations o ON p.organization_id=o.id ' .
                'WHERE MATCH(p.name) AGAINST(? IN NATURAL LANGUAGE MODE) ' .
                'and p.active<2 and o.status=1 and p.deleted_at is null and o.deleted_at is null',
            [$strSearch]
        )[0]->row_count;

        if ($totalRow < $numberOfRowOnPage) {
            $pageNumber = 1;
        }

        $start = ($pageNumber - 1) * $numberOfRowOnPage;

        $numberOfRowNext = $totalRow - ($start + $numberOfRowOnPage);
        if ($numberOfRowNext < 0) {
            $numberOfRowNext = 0;
        }

        $totalPage = ceil($totalRow / $numberOfRowOnPage);

        $sortClause = ($sortType == 0) ? "score desc" : (($sortType == 1) ? "p.price asc" : "p.price desc");

        $data = DB::select("SELECT p.id,p.path,p.code,p.name,p.unit,p.price,p.price_sale, o.id as org_id," .
            "p.avatar_image,p.count_view,p.avg_rate,p.count_rate,p.count_order," .
            "p.rank,p.created_at,MATCH(p.name) AGAINST(? IN NATURAL LANGUAGE MODE) as score, ".
            "o.name as org_name, o.logo_image as org_logo, p.organization_id, o.path as org_path " .
            "FROM products p " .
            'INNER JOIN organizations o ON p.organization_id=o.id ' .
            "WHERE MATCH(p.name) AGAINST(? IN NATURAL LANGUAGE MODE) and p.active<2 and o.status=1 " .
            "and p.deleted_at is null and o.deleted_at is null " .
            "ORDER BY $sortClause, p.created_at desc " .
            "LIMIT ?, ?", [$strSearch, $strSearch, $start, $numberOfRowOnPage]);

        return array(
            "total_page" => $totalPage,
            "total_row" => $totalRow,
            "number_of_row_next" => $numberOfRowNext,
            "sort_type" => $sortType,
            "data" => $data
        );
    }

    public static function fullTextSearchNearest($strSearch, $pageSize, $my_lat, $my_lon)
    {
        $dist = 300; //Giới hạn khoảng cách tìm kiếm trong phạm vi 300Km đổ lại
        $rEarth = 6371; //Bán kính trái đất tính theo Km
        $oneDegree = 110.57; //Một độ tính từ tâm trái đất đến phần vỏ (tính theo Km)

        $lon1 = $my_lon - $dist / abs(cos(deg2rad($my_lat)) * $oneDegree);
        $lon2 = $my_lon + $dist / abs(cos(deg2rad($my_lat)) * $oneDegree);

        $lat1 = $my_lat - ($dist / $oneDegree);
        $lat2 = $my_lat + ($dist / $oneDegree);
        $selectDist = "$rEarth * 2 * ASIN(SQRT(POWER(SIN(($my_lat - o.map_lat) * pi()/180 / 2), 2) + " .
            "COS($my_lat * pi()/180) * COS(o.map_lat * pi()/180)*" .
            "POWER(SIN(($my_lon - o.map_long) * pi()/180 / 2), 2)))";

        $queryBuilder = DB::table("products as p")
            ->join("organizations as o", "o.id", "=", "p.organization_id")
            ->join("regions as r", "r.id", "=", "o.region_id")
            ->where("o.status", 1)
            ->whereNull("p.deleted_at")
            ->whereNull("o.deleted_at")
            ->where("p.active", "<", 2)
            ->whereBetween("o.map_long", [$lon1, $lon2])
            ->whereBetween("o.map_lat", [$lat1, $lat2])
            ->whereRaw("MATCH(p.name) AGAINST('$strSearch' IN NATURAL LANGUAGE MODE)")
            ->whereRaw("$selectDist <= $dist")
            ->selectRaw("p.id,p.path,p.code,p.name,p.unit,p.price,p.price_sale, o.id as org_id," .
                "p.avatar_image,p.count_view,p.avg_rate,p.count_rate,p.count_order," .
                "p.rank,p.created_at,MATCH(p.name) AGAINST('$strSearch' IN NATURAL LANGUAGE MODE) as score," .
                "$selectDist as distance,o.map_lat,o.map_long,"
                . "o.organization_type_id ,o.name as organization_name," .
                "o.address,r.full_name,o.tel,o.email,o.website,o.path");
        return  $queryBuilder->orderByRaw("distance asc,score desc, p.created_at desc")->paginate($pageSize);
            //        return  $queryBuilder->orderByRaw("distance asc,score desc, p.created_at desc")->get();
        ;
    }

    /*
     * Lay danh sach cac san pham theo Organization
     */

    public static function getListByOrganization(
        $organization_id,
        $product_category_id,
        $product_name,
        $active,
        $pageSize = 10
    ) {
        if (isUserBusiness()) {
            $organization_id = getOrgId();
        }

        $returnVal = Product::where("organization_id", $organization_id);

        if ($product_name) {
            $returnVal = $returnVal->where(function ($query) use ($product_name) {
                $query->where("name", "like", '%' . $product_name . '%')
                    ->orWhere("code", "like", '%' . $product_name . '%');
            });
        }
        if ($product_category_id) {
            $returnVal = $returnVal->where("product_category_id", $product_category_id);
        }


        if ($active > -1) {
            $returnVal = $returnVal->where("status", $active);
        }


        return $returnVal->orderBy("created_at", 'asc')->paginate($pageSize);
    }

    public static function save($input)
    {
        $organization_id = isUserBusiness() ? getOrgId() :
            (isset($input["organization_id"]) ? (int)$input["organization_id"] : 0);

        $organization = Organization::findOrFail($organization_id);

        //Kiem tra quyen:
        if (OrganizationBusiness::hasPermission($organization)) {
            $validator = Validator::make($input, [
                'name' => 'required|max:255',
                'product_category_id' => 'required',
                'unit' => 'required|max:32',
                'price' => 'nullable|numeric',
                'price_sale' => 'nullable|numeric',
                'stt_avatar' => 'nullable|numeric',
                'id_avatar' => 'nullable|numeric',
                'weight_per_unit' => 'nullable|numeric'
            ]);

            if ($validator->fails()) {
                //Neu du lieu khong validate
                $arr = $validator->errors()->toArray();
                $errors = [];
                foreach ($arr as $key => $item) {
                    $errors[$key] = implode(' ; ', $item);
                }
                return response()->json(['error' => $errors], 422);
            }

            if (isset($input["images"])) {
                $check = self::filesUploadAreImages($input["images"]);
                if (!$check) {
                    return response()->json(['error' => array("images" => "Files upload phải là ảnh.")], 422);
                }
            }

            $productExists = Product::where('name', $input["name"])
                ->where('organization_id', $organization_id);

            $id = isset($input["id"]) ? (int)$input["id"] : 0;

            if (!$id) {
                //Xu ly khi $id=0 (Them moi)
                if ($productExists->first()) {
                    //Check trung san pham
                    return array("success" => false, "msg" => "Tên sản phẩm đã tồn tại!");
                }

                return self::insertProduct($input, $organization_id);
            } else {
                //Xu ly khi $id<>0 (Cap nhat)
                if ($productExists->where("id", "<>", $id)->first()) {
                    //Check trung san pham
                    return array("success" => false, "msg" => "Tên sản phẩm đã tồn tại!");
                }
                return self::updateProduct($input);
            }
        } else {
            return array("success" => false, "msg" => "Bạn không có quyền thao tác trên cơ sở này!");
        }
    }

    //Ham  insert Product
    private static function insertProduct($input, $organization_id)
    {
        $max = Product::max('id') + 1;

        //Insert Product
        $product = Product::create([
            "organization_id" => $organization_id,
            "product_category_id" => $input["product_category_id"],
            "code" => 'SP' . str_pad("$max", 8, "0", STR_PAD_LEFT),
            "name" => $input["name"],
            "unit" => $input["unit"] ?? "",
            "weight_per_unit" => $input["weight_per_unit"] ?? 1000,
            "price" => isset($input["price"]) ? (float)$input["price"] : null,
            "price_sale" => isset($input["price_sale"]) ? (float)$input["price_sale"] : null,
            "status" => isset($input["status"]) ? (int)$input["status"] : 1,
            "duration" => $input["duration"] ?? "",
            "barcode" => $input["barcode"] ?? "",
            "has_check" => $input["has_check"] === 'true' ? 1 : 0,
            "capacity" => $input["capacity"] ?? "",
            "pack_style" => $input["pack_style"] ?? "",
            "preservation_method" => $input["preservation_method"] ?? "",
            "created_by" => auth()->user()->id
        ]);

        //Insert Images for Product
        if (isset($input["images"])) {
            if (is_array($input["images"])) {
                $images = $input["images"];
                $arrImagesInfo = self::uploadProductImages($images, $product->id);

                if (isset($input["stt_avatar"]) && isset($arrImagesInfo[$input["stt_avatar"]])) {
                    $product->avatar_image = $arrImagesInfo[$input["stt_avatar"]]->path;
                } else {
                    $product->avatar_image = $arrImagesInfo[0]->path;
                }
                $product->save();
            }
        }

        //Insert Product Link
        if (isset($input["links"])) {
            if (is_array($input["links"])) {
                $links = $input["links"];
                foreach ($links as $lnk) {
                    ProductLink::create([
                        "product_id" => $product->id,
                        "link_product_id" => $lnk
                    ]);
                }
            }
        }
        return array("success" => true, "product_id" => $product->id);
    }

    //Ham  update Product
    private static function updateProduct($input)
    {
        $product_id = (int)$input["id"];

        $product = Product::find($product_id);

        if ($product) {
            //Update Product infomations:
            $product->product_category_id = $input["product_category_id"];
            $product->name = $input["name"];
            $product->unit = $input["unit"];
            $product->weight_per_unit = $input["weight_per_unit"] ?? 1000;
            $product->price = isset($input["price"]) ? (int)$input["price"] : $product->price;
            $product->price_sale = isset($input["price_sale"]) ? (int)$input["price_sale"] : $product->price_sale;
            $product->status = isset($input["status"]) ? (int)$input["status"] : $product->status;
            $product->duration = $input["duration"] ?? $product->duration;
            $product->barcode = $input["barcode"] ?? $product->barcode;
            $product->has_check = $input["has_check"] === 'true' ? 1 : 0;
            $product->capacity = $input["capacity"] ?? $product->capacity;
            $product->pack_style = $input["pack_style"] ?? $product->pack_style;
            $product->preservation_method = $input["preservation_method"] ?? $product->preservation_method;
            $product->updated_by = auth()->user()->id;

            //update:
            $product->save();

            if (isset($input["images"])) {
                //ProductImage::where("product_id", $product_id)->deleted();
                //Insert Images for Product
                if (is_array($input["images"])) {
                    $images = $input["images"];
                    $arrImagesInfo = self::uploadProductImages($images, $product->id);

                    //Xu ly luu Avatar neu do la anh moi
                    if (isset($input["stt_avatar"]) && isset($arrImagesInfo[$input["stt_avatar"]])) {
                        $product->avatar_image = $arrImagesInfo[$input["stt_avatar"]]->path;
                        $product->save();
                    }
                }
            }

            //Luu avatar neu do la anh dang co trong DB
            if (isset($input["id_avatar"])) {
                $productImage = ProductImage::where("id", $input["id_avatar"])
                    ->where("product_id", $product_id)->first();
                if ($productImage) {
                    $product->avatar_image = $productImage->image;
                    $product->save();
                }
            }

            if (isset($input["imagesDelete"]) && is_array($input["imagesDelete"])) {
                //Xử lý xóa ảnh của Product
                foreach ($input["imagesDelete"] as $id) {
                    ProductImage::where("id", $id)->where("product_id", $product_id)->delete();
                }
            }


            if (isset($input["links"])) {
                ProductLink::where("product_id", $product_id)->delete();
                //Insert Product Link
                if (is_array($input["links"])) {
                    $links = $input["links"];
                    foreach ($links as $lnk) {
                        ProductLink::create([
                            "product_id" => $product->id,
                            "link_product_id" => $lnk
                        ]);
                    }
                }
            }
            return array("success" => true, "product_id" => $product_id);
        } else {
            return array("success" => false, "msg" => "Sản phẩm không tồn tại. Vui lòng kiểm tra lại!");
        }
    }

    public static function delete(Product $product)
    {
        $organization = $product->organization;
        if (OrganizationBusiness::hasPermission($organization)) {
            $product->delete();
            return array("success" => true, "msg" => "Xóa sản phẩm thành công!");
        } else {
            return array("success" => false, "msg" => "Bạn không có quyền thao tác trên cơ sở này!");
        }
    }

    public static function getProductInfoConfig($product_id, $product_category_id)
    {
        $productInfoConfig = DB::table("product_info_cfgs")
            ->leftJoin('product_info_details', function ($join) use ($product_id) {
                $join->on('product_info_cfgs.key', '=', 'product_info_details.key')
                    ->where('product_info_details.product_id', '=', $product_id);
            })
            ->where('product_info_cfgs.product_category_id', '=', null)
            ->whereOr('product_info_cfgs.product_category_id', '=', $product_category_id)
            ->get([
                'product_info_details.id', 'product_info_details.product_id',
                'product_info_cfgs.key', 'product_info_details.content', 'product_info_cfgs.name'
            ]);

        return $productInfoConfig;
    }

    /*
     * input include fields: product_id, detail_info
     */
    public static function saveProductInfoDetails($input)
    {
        $product_id = $input["product_id"] ?? 0;
        $product = Product::find($product_id);
        if ($product) {
            DB::beginTransaction();
            ProductInfoDetail::where("product_id", $product_id)->delete();

            $detailInfo = $input['detail_info'];
            foreach ($detailInfo as $info) {
                ProductInfoDetail::create([
                    "product_id" => $product_id,
                    "key" => $info['key'],
                    "content" => $info['content'] ?? ''
                ]);
            }
            DB::commit();
            return ["success" => true, "msg" => "Cập nhật thành công."];
        } else {
            return ["success" => false, "msg" => "Sản phẩm không tồn tại. Vui lòng kiểm tra lại."];
        }
    }

    public static function getInfoDetails($organization_id, $product_id)
    {
        $product = Product::where("organization_id", $organization_id)
            ->where("id", $product_id)->first();

        $info = ProductInfoCfg::whereNull('product_info_cfgs.product_category_id')
            ->orWhere('product_info_cfgs.product_category_id', $product->product_category_id)
            ->leftjoin('product_info_details as details', function ($join) use ($product_id) {
                $join->on('product_info_cfgs.key', '=', 'details.key');
                $join->where('product_id', '=', $product_id);
            })
            ->select(['product_info_cfgs.key', 'product_info_cfgs.name', 'details.content'])->get();

        return $info;
    }

    private static function filesUploadAreImages($files)
    {
        $returnVal = true;
        foreach ($files as $file) {
            $check = getimagesize($file->getPathname());
            if (!$check) {
                $error = true;
                break;
            }
        }
        return $returnVal;
    }

    private static function uploadProductImages($aFiles, $product_id)
    {
        $aArrFiles = UploadFileUtil::multiUploadImageFile($aFiles);
        foreach ($aArrFiles as $file) {
            ProductImage::create([
                "product_id" => $product_id,
                "image" => $file->path
            ]);
        }
        return $aArrFiles;
    }

    public static function getProductsRelationForView($product_category_id, $id)
    {
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->where("product_category_id", '=', $product_category_id)
            ->where("id", '<>', $id)
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }
    public static function getProductsRelationShopForView($organization_id, $id)
    {
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->where("organization_id", '=', $organization_id)
            ->where("id", '<>', $id)
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }
    public static function getProductsShopForView($organization_id)
    {
        $products = Product::where('products.status', '>', 0) //Bỏ SP chưa KD
            ->where('products.status', '<', 4) //Bỏ SP ngừng kinh doanh
            ->where('products.active', '<', 2) //Bỏ SP bị khóa VP, không yc duyệt SP
            ->where("organization_id", '=', $organization_id)
            ->whereHas('organization', function ($query) {
                $query->where('organizations.status', '<', 2); //Bỏ CS ngưng hoạt động, vi phạm
                if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
                    $query->where('organizations.status', '>', 0); //Bỏ CS chưa đc kiểm duyệt
                }
            })->with('organization');

        return $products;
    }


    public static function getParentID($product_id)
    {
        $parent_id_first  = DB::table('product_categories')->select('parent_id')->where('id','=',$product_id) ->get();
                        if($parent_id_first[0]->parent_id == null)  $parent_id = $product->product_category_id; else $parent_id = $parent_id_first[0]->parent_id;

        return $parent_id_first;
    }
}