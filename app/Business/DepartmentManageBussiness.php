<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/11/18
 * Time: 10:41
 */

namespace App\Business;

use App\Models\DepartmentManage;

class DepartmentManageBussiness
{
    public static function renderRadio($checked = -1)
    {
        $arr = DepartmentManage::get();
        $radios = [];
        foreach ($arr as $key => $item) {
            $radios[$item->name] = ($key == $checked) ?
                ['name' => 'department_manage_id', 'value' => $item->id, 'checked' => true] :
                ['name' => 'department_manage_id', 'value' => $item->id];
        }
        return $radios;
    }

    public static function renderRadioCheckAthor($arrDep, $checked = -1)
    {
        $radios = [];
        foreach ($arrDep as $key => $item) {
            $radios[$item->name] = ($key == $checked) ?
                ['name' => 'department_manage_id', 'value' => $item->id, 'checked' => true] :
                ['name' => 'department_manage_id', 'value' => $item->id];
        }
        return $radios;
    }
}
