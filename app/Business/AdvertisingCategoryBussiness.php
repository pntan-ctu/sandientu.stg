<?php
/**
 * Created by PhpStorm.
 * User: mac
 * Date: 10/11/18
 * Time: 10:41
 */

namespace App\Business;

use App\Models\AdvertisingCategory;

class AdvertisingCategoryBussiness
{
    public static function renderRadio($checked = -1)
    {
        $arr = AdvertisingCategory::get();
        $radios = [];
        foreach ($arr as $key => $item) {
            $radios[$item->name] = $key == $checked ? [
                'name' => 'category_id', 'value' => $item->id, 'checked' => true] :
                ['name' => 'category_id', 'value' => $item->id];
        }
        return $radios;
    }
}
