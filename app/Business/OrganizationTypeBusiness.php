<?php

namespace App\Business;

use App\Models\OrganizationType;

class OrganizationTypeBusiness
{
    public static function lists()
    {
        $data = OrganizationType::all('id', 'name');
        return $data;
    }
    public static function getTypeBusiness()
    {
        $data = OrganizationType::where('id', '>', 99)->get();
        return $data;
    }
}
