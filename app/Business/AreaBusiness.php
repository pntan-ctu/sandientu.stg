<?php

namespace App\Business;

use Illuminate\Support\Facades\Auth;
use App\Models\Area;
use App\Models\Region;
use Yajra\DataTables\Facades\DataTables;
use App\Models\Organization;
use App\OrgType;

class AreaBusiness
{
    /**
     * Get list menu by current user
     */
    public static function getAll($request)
    {
        $model = Area::select(['areas.id as id', 'areas.name as name',
            'areas.map as map','areas.region_id as region_id',
            'areas.instroduction as instroduction', 'areas.created_at as created_at',
            'regions.name as region_name','organizations.name as org_name',
            'regions.full_name as url'])
        ->join('regions', 'areas.region_id', '=', 'regions.id')
        ->join('organizations', 'areas.manage_organization_id', '=', 'organizations.id');

        if (getOrgTypeId() != OrgType::VPDP) {
            $model->where('areas.manage_organization_id', '=', getOrgId());
        }

        $data = DataTables::eloquent($model)->filter(
            function ($query) use ($request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('areas.name', 'like', '%' . $keyword . '%')
                            ->orWhere('areas.instroduction', 'like', '%' . $keyword . '%')
                            ->orWhere('organizations.name', 'like', '%' . $keyword . '%');
                    });
                }
                if ($request->filled('active') && $request->input('active') != -1) {
                    $query->where('active', '=', request('active'));
                }
            }
        )->make();

        return $data;
        
        /*$mainData = $data->original['data'];

        foreach ($mainData as $i => $j) {
            $mainData[$i]['url'] = AreaBusiness::getFullUrl($mainData[$i]['region_id']);
        }
        $data = json_decode(json_encode($data), true);
        $data['original']['data'] = $mainData;
        return $data['original'];*/
    }

    //hungth
    public static function lists()
    {
        return Area::all('id', 'name');
    }
    public static function getFullUrl($id)
    {
        $oGroup = Region::find($id);
        $sUrl = $oGroup->name;
        $iParentId = $oGroup->parent_id;
        while ($iParentId > 0) {
            $oGroup = Region::find($iParentId);
            $sUrl =  $sUrl. " , " . $oGroup->name;
            $iParentId = $oGroup->parent_id;
        }
        return $sUrl;
    }
}
