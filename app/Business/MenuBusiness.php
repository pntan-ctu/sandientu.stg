<?php

namespace App\Business;

use App\Models\Menu;
use App\Models\Module;
use App\Models\Organization;
use Illuminate\Support\Facades\Gate;
use App\Authors\OrgAuthor;

class MenuBusiness
{
    /**
     * Get list menu by current user
     */
    public static function getByCurrentUser()
    {
        $aResult = array();
        if (auth()->check()) {
            $user_id = getUserId();
            $organizationTypeId = getOrgTypeId();

            //Lấy tạm, bổ sung check theo quyền user sau
            $aMenus = Menu::where('organization_type_id', $organizationTypeId)
                ->orderBy('no', 'asc')->get();

            //Xử lý lấy url và check quyền load menu
            foreach ($aMenus as $oMenu) {
                if (isset($oMenu->module)) {
                    $aAbility = explode(';', $oMenu->module->ability_name);
                    if (Gate::any($aAbility, $oMenu->module->ability_type)) {
                        $oMenu->url = $oMenu->module->url;
                        $aResult[] = $oMenu;
                    }
                } else {
                    $oMenu->url = null;
                    $aResult[] = $oMenu;
                }
            }
        }
        return $aResult;
    }

    //Lấy memu theo loại CSSX hoặc CSKD cho site Org
    public static function getByOrganization($organization)
    {
        /*$ogrTypeId = (int)getOrgTypeId(); //organizationTypeId của user đang login
        if ($ogrTypeId >= 100) {
            //Lấy tạm, bổ sung check theo quyền user cơ sở sau
            $aMenus = Menu::where('organization_type_id', $organization->organization_type_id)
            ->orderBy('no', 'asc')->get();
        } else {
            //Chỉ cho cơ quan quản lý vào những chức năng quản lý thông tin của cơ sở
            $aMenus = Menu::where('organization_type_id', $organization->organization_type_id)
                ->orderBy('no', 'asc')->get();
        }*/
        $aMenus = Menu::where('organization_type_id', $organization->organization_type_id)
            ->orderBy('no', 'asc')->get();

        //Xử lý lấy url và check quyền load menu
        $orgAuthor = new OrgAuthor();
        $isUserBusiness = isUserBusiness();

        $aResult = array();
        foreach ($aMenus as $oMenu) {
            if (isset($oMenu->module)) {
                //$aAbility = explode(';', $oMenu->module->ability_name);
                if (($oMenu->module->allow_gov || $isUserBusiness)
                    && $orgAuthor->canDo($organization, $oMenu->module->ability_name)) {
                    $oMenu->url = $oMenu->module->url;
                    $aResult[] = $oMenu;
                }
            } else {
                $oMenu->url = null;
                $aResult[] = $oMenu;
            }
        }
        //Kết quả
        return $aResult;
    }
}
