<?php

namespace App\Business;

use App\Models\Order;
use App\Models\Organization;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Validator;

class OrderBusiness
{
    /**
     * Get các đơn hàng của nhà cung cấp
     *
     * @param int $providerOrganizationId
     * @param int $status
     * @param string $keyword
     * @return JsonResponse
     * @throws \Exception
     */
    public static function getOrderOfProviders($providerOrganizationId, $status = -1, $keyword = '')
    {
        $orders = Order::join('organizations', 'orders.provider_organization_id', '=', 'organizations.id')
            ->where("provider_organization_id", "=", $providerOrganizationId)
            ->select(['orders.*',
                'organizations.name as provider_name',
                'organizations.address as provider_address',
                'organizations.tel as provider_tel'
            ]);
        return self::makeDataTableResponse($status, $keyword, $orders);
    }

    /**
     * Lấy lịch sử đặt hàng của 1 doanh nghiệp
     *
     * @param int $organizationId
     * @param int $status
     * @param string $keyword
     * @return JsonResponse
     * @throws \Exception
     */
    public static function getOrderOfOrganizations($organizationId, $status = -1, $keyword = '')
    {
        $organization = Organization::find($organizationId);
        if (isset($organization)) {
            $orders = $organization->orders()
            ->join('organizations', 'orders.provider_organization_id', '=', 'organizations.id')
            ->select(['orders.*',
                'organizations.name as provider_name',
                'organizations.address as provider_address',
                'organizations.tel as provider_tel'
            ]);

            return self::makeDataTableResponse($status, $keyword, $orders);
        } else {
            return new JsonResponse();
        }
    }

    /**
     * Lấy lịch sử đặt hàng của 1 người dùng
     *
     * @param int $userId
     * @param int $status
     * @param string $keyword
     * @return JsonResponse
     * @throws \Exception
     */
    public static function getOrderOfUsers($userId, $status = -1, $keyword = '')
    {
        $user = User::find($userId);
        if (isset($user)) {
            $orders = $user->orders()
            ->join('organizations', 'orders.provider_organization_id', '=', 'organizations.id')
            ->select(['orders.*',
                'organizations.name as provider_name',
                'organizations.address as provider_address',
                'organizations.tel as provider_tel'
            ]);

            return self::makeDataTableResponse($status, $keyword, $orders);
        } else {
            return new JsonResponse();
        }
    }

    /**
     * @param int $status
     * @param string $keyword
     * @param \Illuminate\Database\Eloquent\Collection|Order[] $orders
     * @return JsonResponse
     * @throws \Exception
     */
    protected static function makeDataTableResponse($status, $keyword, $orders): JsonResponse
    {
        return DataTables::eloquent($orders)->filter(
            function ($query) use ($status, $keyword) {
                if ($status != -1) {
                    $query->where('orders.status', '=', $status);
                } else {
                    $query->where('orders.status', '>', 0);
                }
                if (strlen($keyword) > 0) {
                    $query->where(function ($query) use ($keyword) {
                        $query->where('orders.customer_name', 'like', '%' . $keyword . '%')
                            ->orWhere('orders.address', 'like', '%' . $keyword . '%')
                            ->orWhere('orders.email', 'like', '%' . $keyword . '%')
                            ->orWhere('orders.comment', 'like', '%' . $keyword . '%')
                            ->orWhere('organizations.name', 'like', '%' . $keyword . '%')
                            ->orWhere('organizations.address', 'like', '%' . $keyword . '%');
                    });
                }
                $query->orderBy('order_date', 'desc');
            }
        )->make();
    }
    
    /*
     * Lay danh sach cac hoa don theo Organization
     */
    public static function getListByOrganization(
        $organization_id,
        $search,
        $status,
        $pageSize = 10
    ) {
        if (isUserBusiness()) {
            $organization_id= getOrgId();
        }
        
        $returnVal = Order::where("provider_organization_id", $organization_id);
        
        if ($search) {
                $returnVal = $returnVal->where(function ($query) use ($search) {
                    $query->where("code", "like", '%' . $search . '%')
                        ->orWhere("customer_name", "like", '%' . $search . '%');
                });
        }
        
        if ($status) {
            $returnVal = $returnVal->where("status", $status);
        }
        
        
        return $returnVal->orderBy("created_at", "DESC")->paginate($pageSize);
    }
    
    public static function getCountNewOrders($organization_id)
    {
        return Order::where("provider_organization_id", $organization_id)->where("status", 1)->count();
    }
    
    public static function save($input)
    {
        $organization_id=isUserBusiness()?getOrgId():
            (isset($input["organization_id"])?(int)$input["organization_id"]:0);
        
        $organization = Organization::findOrFail($organization_id);
        
        //Kiem tra quyen:
        if (OrganizationBusiness::hasPermission($organization)) {
            $validator=Validator::make($input, [
                'id'=>'required',
                "status"=>'required'
            ]);
            if ($validator->fails()) {
                //Neu du lieu khong validate
                return response()->json(['error'=>$validator->errors()], 422);
            }
            
            $order = Order::where("provider_organization_id", $organization_id)
                    ->where("id", (int)$input["id"])->first();
            if ($order) {
                $order->status=(int)$input["status"];
                $order->save();
                return array("success"=>true,"msg"=>"Cập nhật thành công");
            } else {
                return array("success"=>false,"msg"=>"Không tồn tại hóa đơn này trên hệ thống!");
            }
        } else {
            return array("success"=>false,"msg"=>"Bạn không có quyền thao tác trên cơ sở này!");
        }
    }
}
