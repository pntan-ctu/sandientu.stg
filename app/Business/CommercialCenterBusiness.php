<?php

namespace App\Business;

use App\Models\CommercialCenter;
use Illuminate\Support\Facades\Auth;
use App\Models\Region;
use Yajra\DataTables\Facades\DataTables;
use App\OrgType;

class CommercialCenterBusiness
{
    public static function getAll($request)
    {
        $model = CommercialCenter::select(['commercial_centers.id as id', 'commercial_centers.name as name',
            'commercial_centers.region_id as region_id',
            'commercial_centers.address as address',
            'commercial_centers.introduction as introduction',
            'commercial_centers.created_at as created_at',
            'regions.name as region_name','organizations.name as org_name',
            'regions.full_name as url'])
        ->join('regions', 'commercial_centers.region_id', '=', 'regions.id')
        ->join('organizations', 'commercial_centers.manage_organization_id', '=', 'organizations.id');
        
        if (getOrgTypeId() != OrgType::VPDP) {
            $model->where('commercial_centers.manage_organization_id', '=', getOrgId());
        }

        return DataTables::eloquent($model)->filter(
            function ($query) use ($request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('commercial_centers.name', 'like', '%' . $keyword . '%')
                            ->orWhere('commercial_centers.introduction', 'like', '%' . $keyword . '%');
                    });
                }
            }
        )->make();
        /*$mainData = $data->original['data'];
        foreach ($mainData as $i => $j) {
            $mainData[$i]['url'] = CommercialCenterBusiness::getFullUrl($mainData[$i]['region_id']);
        }
        $data = json_decode(json_encode($data), true);
        $data['original']['data'] = $mainData;
        return $data['original'];*/
    }
    public static function lists()
    {
        $data = CommercialCenter::all('id', 'name');
        return $data;
    }
    public static function getFullUrl($id)
    {
        $oGroup = Region::find($id);
        $sUrl = $oGroup->name;
        $iParentId = $oGroup->parent_id;
        while ($iParentId > 0) {
            $oGroup = Region::find($iParentId);
            $sUrl =  $sUrl. " , " . $oGroup->name;
            $iParentId = $oGroup->parent_id;
        }
        return $sUrl;
    }
}
