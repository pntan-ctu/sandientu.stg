<?php
namespace App\Business;

use Illuminate\Support\Facades\DB;

class NewsArticlesBussiness
{
    public static function fullTextSearch($strSearch, $pageNumber, $numberOfRowOnPage)
    {
        $daysToQuery = 90;
        $totalRow = DB::select(
            "SELECT COUNT(a.id) as row_count " .
                "FROM news_articles a " .
                "WHERE MATCH(a.title,a.summary,a.content) " .
                "AGAINST(? IN NATURAL LANGUAGE MODE) and a.status=1 and a.deleted_at is null ",
            //"and (a.created_at BETWEEN DATE_ADD(sysdate(),INTERVAL -{$daysToQuery} DAY) AND sysdate())",
            [$strSearch]
        )[0]->row_count;

        if ($totalRow<$numberOfRowOnPage) {
            $pageNumber=1;
        }

        $start = ($pageNumber-1)*$numberOfRowOnPage;

        $numberOfRowNext = $totalRow - ($start + $numberOfRowOnPage);
        if ($numberOfRowNext<0) {
            $numberOfRowNext=0;
        }

        $totalPage = ceil($totalRow/$numberOfRowOnPage);

        //$data = DB::select("SELECT a.id,a.path,a.title,a.summary,a.content,a.img_path," .
        $data = DB::select("SELECT a.id,a.path,a.title,a.summary,a.img_path," .
                            "a.view_count,a.hotnews,a.comment,a.shared,a.tags, " .
                            "a.relate_news,a.publish_at,a.created_at, u.name, " .
                            "MATCH(a.title,a.summary,a.content) AGAINST(? IN NATURAL LANGUAGE MODE) as score " .
                            "FROM news_articles a " .
                            "INNER JOIN users u ON a.created_by=u.id " .
                            "WHERE MATCH(a.title,a.summary,a.content) AGAINST(? IN NATURAL LANGUAGE MODE) " .
                            "and a.status=1 and a.deleted_at is null " .
        //"and (a.created_at BETWEEN DATE_ADD(sysdate(),INTERVAL -{$daysToQuery} DAY) AND sysdate())" .
                            "ORDER BY score desc, a.created_at desc " .
                            "LIMIT ?, ?", [$strSearch,$strSearch,$start,$numberOfRowOnPage]);

        return array(
            "total_page"=>$totalPage,
            "total_row"=>$totalRow,
            "number_of_row_next"=>$numberOfRowNext,
            "data"=>$data);
    }
}
