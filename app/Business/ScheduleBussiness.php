<?php

namespace App\Business;

use App\Models\Product;
use App\Models\Organization;
use Illuminate\Support\Facades\Redis;

class ScheduleBussiness
{
    private static $viewKey = "views";
    private static $ratingKey = "ratings";
    private static $orderKey = "orders";
    private static $taggingProduct = "products";
    private static $taggingOrganization = "organizations";
    private static $cacheStore = "schedule";

    public static function calcViewProducts()
    {
        $redis = Redis::connection(self::$cacheStore);
        $prefix = config('database.redis.' . self::$cacheStore . '.prefix');
        $namespace = $prefix . ":" . self::$taggingProduct . ":" . self::$viewKey . ":";
        $arrProducts = $redis->keys("$namespace*");
        
        foreach ($arrProducts as $sProductId) {
            $productId = str_replace($namespace, "", $sProductId);
            $product = Product::find($productId);
            if ($product) {
                $product->count_view = $product->visits()->count();
                $product->rank = $product->calcRank();
                
                $product->save();
            }
            $redis->del($sProductId);
        }
    }
    
    public static function calcRatingProducts()
    {
        $redis = Redis::connection(self::$cacheStore);
        $prefix = config('database.redis.' . self::$cacheStore . '.prefix');
        $namespace = $prefix . ":" . self::$taggingProduct . ":" . self::$ratingKey . ":";
        $arrProducts = $redis->keys("$namespace*");
        
        foreach ($arrProducts as $sProductId) {
            $productId = str_replace($namespace, "", $sProductId);
            $product = Product::find($productId);
            if ($product) {
                $product->avg_rate = $product->calcAvgRate();
                $product->count_rate = $product->calcCountRate();
                $product->rank = $product->calcRank();
                
                $product->save();
            }
            $redis->del($sProductId);
        }
    }
    
    public static function calcOrderProducts()
    {
        $redis = Redis::connection(self::$cacheStore);
        $prefix = config('database.redis.' . self::$cacheStore . '.prefix');
        $namespace = $prefix . ":" . self::$taggingProduct . ":" . self::$orderKey . ":";
        $arrProducts = $redis->keys("$namespace*");
        
        foreach ($arrProducts as $sProductId) {
            $productId = str_replace($namespace, "", $sProductId);
            $product = Product::find($productId);
            if ($product) {
                $product->count_order = $product->calcCountOrder();
                $product->rank = $product->calcRank();
                
                $product->save();
            }
            $redis->del($sProductId);
        }
    }
    
    public static function calcViewOrganizations()
    {
        $redis = Redis::connection(self::$cacheStore);
        $prefix = config('database.redis.' . self::$cacheStore . '.prefix');
        $namespace = $prefix . ":" . self::$taggingOrganization . ":" . self::$viewKey . ":";
        $arrOrganizations = $redis->keys("$namespace*");
        
        foreach ($arrOrganizations as $sOrganizationId) {
            $organizationId = str_replace($namespace, "", $sOrganizationId);
            $organization = Organization::find($organizationId);
            if ($organization) {
                $organization->count_view = $organization->visits()->count();
                $organization->rank = $organization->calcRank();
                
                $organization->save();
            }
            $redis->del($sOrganizationId);
        }
    }
    
    public static function calcRatingOrganizations()
    {
        $redis = Redis::connection(self::$cacheStore);
        $prefix = config('database.redis.' . self::$cacheStore . '.prefix');
        $namespace = $prefix . ":" . self::$taggingOrganization . ":" . self::$ratingKey . ":";
        
        $arrOrganizations = $redis->keys("$namespace*");
        
        foreach ($arrOrganizations as $sOrganizationId) {
            $organizationId = str_replace($namespace, "", $sOrganizationId);
            $organization = Organization::find($organizationId);
            if ($organization) {
                $organization->avg_rate = $organization->calcAvgRate();
                $organization->count_rate = $organization->calcCountRate();
                $organization->rank = $organization->calcRank();
                
                $organization->save();
            }
            $redis->del($sOrganizationId);
        }
    }
    
    public static function calcOrderOrganizations()
    {
        $redis = Redis::connection(self::$cacheStore);
        $prefix = config('database.redis.' . self::$cacheStore . '.prefix');
        $namespace = $prefix . ":" . self::$taggingOrganization . ":" . self::$orderKey . ":";
        $arrOrganizations = $redis->keys("$namespace*");
        
        foreach ($arrOrganizations as $sOrganizationId) {
            $organizationId = str_replace($namespace, "", $sOrganizationId);
            $organization = Organization::find($organizationId);
            if ($organization) {
                $organization->count_order = $organization->calcCountOrder();
                $organization->rank = $organization->calcRank();
                
                $organization->save();
            }
            $redis->del($sOrganizationId);
        }
    }
}
