<?php

namespace App\Business;

use App\Models\Certificate;
use App\Models\CertificateCategory;
use App\Models\Organization;
use Illuminate\Support\Facades\Auth;
use App\Models\Area;
use Yajra\DataTables\Facades\DataTables;

class CertBusiness
{
    /**
     * Get list menu by current user
     */
    public static function getCertOfOrganizations($model, $type_id, $active = -1, $keyword = '')
    {
        $type = $model::find($type_id);
        if (isset($type)) {
            $certificates = $type->certificates();
            return DataTables::eloquent($certificates)->filter(
                function ($query) use ($active, $keyword) {
                    if ($keyword) {
                        $query->where(function ($query) use ($keyword) {
                            $query->where('title', 'like', '%' . $keyword . '%')
                                ->orWhere('description', 'like', '%' . $keyword . '%');
                        });
                    }
                    if ($active && $active != -1) {
                        $query->where('active', '=', request('active'));
                    }
                }
            )->make();
        } else {
            return array();
        }
    }

    /**
     * Get list menu by current user
     */
    public static function getAllCate($request)
    {
        $model = CertificateCategory::select(['id', 'name', 'rank', 'type','icon', 'description', 'created_at']);
        return DataTables::eloquent($model)->filter(
            function ($query) use ($request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('name', 'like', '%' . $keyword . '%')
                            ->orWhere('description', 'like', '%' . $keyword . '%');
                    });
                }
                if ($request->filled('type') && $request->input('type') != -1) {
                    $query->where('type', '=', request('type'));
                }
            }
        )->make();
    }

    public static function lists($type)
    {
        return CertificateCategory::where('type', $type)->get(['id', 'name']);
    }
}
