<?php

namespace App\Business;

use App\Models\Branch;
use App\Models\Region;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Facades\DataTables;

class BranchBusiness
{
    /**
     * Get list menu by current user
     */
    public static function getAll($organizationId, $request)
    {
        $model = Branch::where('organization_id', '=', $organizationId)
        ->select(['branches.id as id', 'branches.name as name',
            'branches.address as address', 'branches.tel as tel',
            'branches.email as email', 'branches.map_lat as map_lat',
            'branches.created_at as created_at','branches.region_id as region_id'])
            ->join('regions', 'branches.region_id', '=', 'regions.id');
        
        $data= DataTables::eloquent($model)->filter(
            function ($query) use ($request) {
                if ($request->filled('keyword')) {
                    $keyword = $request->input('keyword');
                    $query->where(function ($query) use ($keyword) {
                        $query->where('branches.name', 'like', '%' . $keyword . '%')
                            ->orWhere('branches.address', 'like', '%' . $keyword . '%');
                    });
                }
            }
        )->make();
        $mainData = $data->original['data'];
        foreach ($mainData as $i => $j) {
            $mainData[$i]['url'] = BranchBusiness::getFullUrl($mainData[$i]['region_id']);
        }
        $data = json_decode(json_encode($data), true);
        $data['original']['data'] = $mainData;
        return $data['original'];
    }
    public static function getFullUrl($id)
    {
        $oGroup = Region::find($id);
        $sUrl = $oGroup->name;
        $iParentId = $oGroup->parent_id;
        while ($iParentId > 0) {
            $oGroup = Region::find($iParentId);
            $sUrl = $sUrl. " , " . $oGroup->name;
            $iParentId = $oGroup->parent_id;
        }
        return $sUrl;
    }
}
