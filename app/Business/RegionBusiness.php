<?php

namespace App\Business;

use Illuminate\Support\Facades\Auth;
use App\Models\Area;
use App\Models\Region;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Facades\Cache;

class RegionBusiness
{
    //Giới hạn Root cho tree Region là Tỉnh Sóc Trăng
    const REGIONIDROOT = 60;
    
    /**
     * Get list menu by current user
     */
    public static function getTree($region_arr, $parent_id)
    {
        $tree = array();
        foreach ($region_arr as $key => $value) {
            if ($value->parent_id == $parent_id) {
                //$tree_new = new \stdClass();
                $tree_new = (object) array();
                $tree_new->id = $value->id;
                $tree_new->name = $value->name;
                $tree_new->parent_id = $value->parent_id;
                $value->children = self::getTree($region_arr, $value->id);
                $tree_new->children = $value->children;
                $tree[] = $tree_new;
            }
        }

        return $tree;
    }

    //hungth
    public static function getTreeWithParent($region_id = self::REGIONIDROOT)
    {
        return Cache::rememberForever("RegionTreeWithoutParent_$region_id", function () use ($region_id) {
            $reg = Region::find($region_id);
            $region_arr = Region::where('path_primary_key', "like", "{$reg->path_primary_key}" . "/%")
                ->orWhere('id', $reg->id)->get(['id', 'name', 'parent_id']);
            $regions = RegionBusiness::tree($region_arr, $reg->parent_id);
            return $regions;
        });
    }

    //hungth
    public static function getTreeWithoutParent($region_id = self::REGIONIDROOT)
    {
        return Cache::rememberForever("RegionTreeWithoutParent_$region_id", function () use ($region_id) {
            $reg = Region::find($region_id);
            $region_arr = Region::where('path_primary_key', "like", "{$reg->path_primary_key}" . "/%")
                ->get(['id', 'name', 'parent_id']);
            $regions = RegionBusiness::tree($region_arr, $reg->id);
            return $regions;
        });
    }

    //hungth
    public static function tree($objs, $parent_id)
    {
        $arrObj = [];
        foreach ($objs as $item) {
            $arrObj[$item->parent_id][] = $item;
        }
        $tree = RegionBusiness::insertToParent($arrObj, $arrObj[$parent_id]);
        return $tree;
    }

    public static function insertToParent(&$arrObj, $parents)
    {
        $tree = [];
        foreach ($parents as $key => $item) {
            if (isset($arrObj[$item->id])) {
                $item->children = RegionBusiness::insertToParent($arrObj, $arrObj[$item->id]);
                unset($arrObj[$item->id]);
            }
            $tree[] = $item;
        }
        return $tree;
    }
}
