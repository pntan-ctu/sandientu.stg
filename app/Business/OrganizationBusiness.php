<?php

namespace App\Business;

use App\Http\Requests\CommonRules;
use App\Models\Branch;
use App\Models\Certificate;
use App\Models\DepartmentManage;
use App\Models\OrganizationImage;
use App\Models\OrganizationInfoCfg;
use App\Models\OrganizationInfoDetail;
use App\Models\OrganizationLevel;
use App\Models\OrganizationPayType;
use App\Models\OrganizationShipType;
use App\Models\Partner;
use App\Models\Product;
use App\Models\User;
use App\Utils\CommonUtil;
use App\Utils\UploadFileUtil;
use Illuminate\Support\Facades\DB;
use App\Models\Organization;
use App\OrgType;
use Illuminate\Validation\Rule;
use Yajra\DataTables\Facades\DataTables;
use App\Constants;
use Illuminate\Support\Facades\Gate;
use App\Authors\AbilityName;
use App\Authors\ManageOrganizationAuthor;
use App\Authors\OrgAuthor;
use Illuminate\Support\Facades\Validator;

class OrganizationBusiness
{
    protected $manageOrganizationAuthor;
    protected $orgAuthor;
    protected $abilityOrgManage = AbilityName::ORG_MANAGE;
    protected $abilityOrgInfo = AbilityName::ORG_INFO;

    public function __construct()
    {
        $this->manageOrganizationAuthor = new ManageOrganizationAuthor();
        $this->orgAuthor = new OrgAuthor();
    }

    /**
     * Lấy danh sách Cơ sở phục vụ view ngoài web
     * Đã check trạng thái được hiện thị hay không
     * Tra về query để có thể chèn thêm các ĐK khác
     */
    public static function getListForView()
    {
        $organizations = Organization::where('organization_type_id', '>=', 100)
            ->where('status', '<', 2);

        if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
            $organizations->where('status', '>', 0);
        }

        return $organizations;
    }

    /**
     * Lấy đối tượng Cơ sở bởi path phục vụ view chi tiết ngoài web
     * Đã check trạng thái được hiện thị hay không
     */
    public static function getModelForView($path)
    {
        $organization = Organization::where('path', '=', $path)
            ->where('organization_type_id', '>=', 100)
            ->where('status', '<', 3);

        if (Constants::ALLOW_PUBLIC_ORG_PRODUCT == false) {
            $organization->where('status', '>', 0);
        }

        return $organization->first();
    }

    /**
     * Kiểm tra 1 cơ sở có thuộc đơn vị hành chính quản lý
     */
    public static function checkPermissionManage(Organization $organization)
    {
        // //Check dựa theo region
        // $regionId = getRegionId();

        // $region = Region::find($regionId);
        // if ($region == null) return false;

        // $pathOrg = $organization->Region->path_primary_key;
        // $pathRegion = $region->path_primary_key;

        // return stripos("@{$pathOrg}", $pathRegion) > 0;
        $orgTypeId = getOrgTypeId();
        $orgId = getOrgId();
        $manageOrganizationId = $organization->manage_organization_id;
        return (($manageOrganizationId == $orgId) || ($orgTypeId == \App\OrgType::VPDP));
    }

    public static function search($organizationTypeId, $status, $keyword)
    {
        $organizationTypeId = (int)$organizationTypeId;
        $status = (int)$status;

        $model = Organization::where('organization_type_id', '=', $organizationTypeId)->select('*');
        return DataTables::eloquent($model)->filter(function ($query) use ($status, $keyword) {
            if ($status >= 0) {
                $query->where('status', '=', $status);
            }
            if (strlen($keyword)) {
                $query->where('name', 'like', '%' . $keyword . '%')
                    ->orWhere('address', 'like', '%' . $keyword . '%')
                    ->orWhere('tel', 'like', '%' . $keyword . '%');
            }
        })->make();
    }

    public static function getTreeOrgGov($parent_id = 0)
    {
        $org_type_arr = Organization::where('organization_type_id', '>', 1)
            ->where('organization_type_id', '<', 100)->get();
        $regions = OrganizationBusiness::typetree($org_type_arr, $parent_id);
        return $regions;
    }

    public static function typetree($objs, $parent_id)
    {
        $arrObj = [];
        foreach ($objs as $item) {
            $arrObj[$item->manage_organization_id][] = $item;
        }
        $tree = (isset($arrObj[$parent_id])) ?
            OrganizationBusiness::insertToParent($arrObj, $arrObj[$parent_id]) : array();
        return $tree;
    }

    public static function insertToParent(&$arrObj, $parents)
    {
        $tree = [];
        foreach ($parents as $key => $item) {
            if (isset($arrObj[$item->id])) {
                $item->children = OrganizationBusiness::insertToParent($arrObj, $arrObj[$item->id]);
                unset($arrObj[$item->id]);
            }
            $tree[] = $item;
        }
        return $tree;
    }

    public static function fullTextSearch($strSearch, $pageNumber, $numberOfRowOnPage)
    {
        $totalRow = DB::select(
            'SELECT COUNT(o.id) as row_count FROM organizations o ' .
            'WHERE MATCH(o.name) AGAINST(? IN NATURAL LANGUAGE MODE) and o.organization_type_id>=100 '
            . ' and o.status>0 and o.status<3 and o.deleted_at is null ',
            [$strSearch]
        )[0]->row_count;

        if ($totalRow < $numberOfRowOnPage) {
            $pageNumber = 1;
        }

        $start = ($pageNumber - 1) * $numberOfRowOnPage;

        $numberOfRowNext = $totalRow - ($start + $numberOfRowOnPage);
        if ($numberOfRowNext < 0) {
            $numberOfRowNext = 0;
        }

        $totalPage = ceil($totalRow / $numberOfRowOnPage);

        $data = DB::select("SELECT o.id,o.path,o.name,o.address,o.tel,o.email,o.website," .
            "o.fanpage,o.logo_image,o.director,o.founding_type, " .
            "o.founding_number,o.founding_by_gov,o.founding_date,o.map_lat," .
            "o.map_long,o.avg_rate,o.count_rate,o.rank," .
            "o.created_at,MATCH(o.name) AGAINST(? IN NATURAL LANGUAGE MODE) as score " .
            "FROM organizations o " .
            "WHERE MATCH(o.name) AGAINST(? IN NATURAL LANGUAGE MODE) " .
            "and o.organization_type_id>=100 and o.status>0 and o.status<3 and o.deleted_at is null " .
            "ORDER BY score desc, o.created_at desc " .
            "LIMIT ?, ?", [$strSearch, $strSearch, $start, $numberOfRowOnPage]);

        return array(
            "total_page" => $totalPage,
            "total_row" => $totalRow,
            "number_of_row_next" => $numberOfRowNext,
            "data" => $data);
    }

    public static function thongKeTinhHinhXL($key = null)
    {
        $region = getOrgModel()->region;
        $qry = Organization::join('regions', 'organizations.region_id', '=', 'regions.id')
            ->join('organizations as d', 'd.id', '=', 'organizations.manage_organization_id');

        if (!Gate::any(['*', AbilityName::MANAGE_REPORT_ALL], '*')) {
            if (!Gate::any([AbilityName::MANAGE_REPORT_SCOPE], '*')) {
                // không lấy dữ liệu
                $qry = $qry->where("d.id", "<", "0");
            } else {
                // giới hạn trong scope
                $org = getOrgModel();
                $path = $org->path_primary_key;
                $qry = $qry->whereRaw("d.path_primary_key like '$path%'");
            }
        }


        $qry = $qry->where('organizations.organization_type_id', '>', 1)
            ->where('organizations.organization_type_id', '<', 100)
            ->where('regions.path_primary_key', 'like', $region->path_primary_key . '%')
            ->select('organizations.*');
        if (request()->filled('keyword')) {
            $keyword = request()->keyword;
            $qry->where('organizations.name', 'like', "%$keyword%");
        }
        $list = $qry->paginate(10);
        foreach ($list as $gov) {
            $qry = $gov->childs()->where('organization_type_id', '>=', 100);
            $gov['cs_dang_ky'] = $qry->count();
            $gov['cs_chua_duyet'] = $qry->where('status', '=', 0)->count();

            $qry = $gov->contacts();
            $gov['ph_da_gui'] = $qry->count();
            $gov['ph_chua_xl'] = $qry->where('status', '=', 0)->count();
        }
        return $list;
    }

    /*
     * Ham kiem tra xem USER dang dang nhap co duoc QUYEN thao tac voi TO CHUC hay khong
     */
    public static function hasPermission(Organization $organization)
    {
        $user = auth()->user();
        $userOrgType = $user->Organization->organization_type_id;
        $returnVal = false;
        switch ($userOrgType) {
            case OrgType::VPDP || OrgType::ADMIN:
                $returnVal = true;
                break;
            case OrgType::CSKD || OrgType::CSSX:
                $returnVal = $user->organization_id == $organization->id;
                break;
            case OrgType::SCT || OrgType::SNN || OrgType::SYT:
                $returnVal = $userOrgType == $organization->organization_type_id;
                break;
            default:
                $returnVal = $user->organization->region_id == $organization->region_id;
                break;
        }
        return $returnVal;
    }

    public function saveGeneralInfo($input)
    {
        $organization_id = isUserBusiness() ? getOrgId() :
            (isset($input["organization_id"]) ? (int)$input["organization_id"] : 0);

        $organization = Organization::findOrFail($organization_id);

        // Check quyen
        $this->orgAuthor->canManageObj($organization, $this->abilityOrgInfo);

        $validate = [
            'name' => 'required|max:255',
            'region_id' => 'required',
            'address' => 'required',
            'tel' => 'required|max:32',
            'director' => 'required|max:64',
            'logo_image' => CommonRules::IMAGE,
            'founding_number' => 'required',
            'founding_by_gov' => 'required|max:127',
            'founding_date' => 'required',
            'organization_level_id' => 'required',
            'status' => 'required'
        ];

        if ($organization->status == 0) {
            $validate = array_merge($validate, [
                'department_manage_id' => 'required',
                'founding_type' => 'required'
            ]);
        }

        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        $organization->fill($input);

        // Kiểm tra nếu không phải là người dùng thuộc cơ quan quản lý
        if (!isUserGov()) {
            // Co so da duyet thi khong duoc thay doi ten va mot so thong tin
            if ($organization->status > 0) {
                unset($organization->name);
                unset($organization->department_manage_id);
                unset($organization->founding_type);
                unset($organization->founding_number);
                unset($organization->founding_by_gov);
                unset($organization->founding_date);
            }

            // Co so chua duyet hoac da bi khoa vi pham thi khong dc cap nhat trang thai
            if ($organization->status <= 0 || $organization->status > 2) {
                unset($organization->status);
            }
        }

        if (isset($input["logo_image"])) {
            $check = self::checkImage($input["logo_image"]);
            if (!$check) {
                return response()->json(['error' => array("image" => "File upload không phải là ảnh.")], 422);
            }

            $logo = self::uploadLogo($input["logo_image"], $organization_id);
            $organization->logo_image = $logo->path;
        } else {
            $deleteLogo = $input["delete_logo"] ?? 0;
            if ($deleteLogo == 1) {
                $organization->logo_image = null;
            }
        }

        $organization->updated_by = getUserId();
        $organization->save();
        return array("success" => true);
    }

    private static function checkImage($file)
    {
        $check = getimagesize($file->getPathname());
        return !$check ? false : true;
    }

    private static function uploadLogo($file, $organization_id)
    {
        $logo = UploadFileUtil::uploadImageFile($file);
        OrganizationImage::create([
            "organization_id" => $organization_id,
            "image" => $logo->path
        ]);
        return $logo;
    }

    public static function getGeneralInfo($organization_id)
    {
        $result = [];
        $org = Organization::find($organization_id);
        $result['general_info'] = $org;
        $regions = RegionBusiness::getTreeWithParent($org->manageOrganization->region_id);
        $result['regions'] = $regions;
        $departments = DepartmentManage::get();
        $result['departments'] = $departments;

        $aFOUNDING_TYPE = Constants::FOUNDING_TYPE;
        $rs = [];
        foreach ($aFOUNDING_TYPE as $k => $v) {
            $newItem['id'] = $k;
            $newItem['name'] = $v;
            $rs[] = $newItem;
        }
        $result['founding_type'] = $rs;

        $organizationLevel = OrganizationLevel::get();
        $result['organization_levels'] = $organizationLevel;

        $status = Constants::ORG_STATUS;
        $isUserGov = isUserGov();
        if (!$isUserGov) {
            if ($org->status == 0) {
                $status = [0 => $status[0]];
            } elseif ($org->status > 2) {
                $status = [3 => $status[3]];
            } else {
                unset($status[0]);
                unset($status[3]);
            }
        }

        $aStatus = [];
        foreach ($status as $k => $v) {
            $newItem['id'] = $k;
            $newItem['name'] = $v;
            $aStatus[] = $newItem;
        }
        $result['status'] = $aStatus;

        return $result;
    }

    public static function getIntroductionInfo($organization_id)
    {
        $org = Organization::findOrFail($organization_id);
        $orgTypeId = $org->organization_type_id;
        $info = OrganizationInfoCfg::where(
            'organization_info_cfgs.organization_type_id',
            '=',
            $orgTypeId
        )
            ->leftjoin('organization_info_details as details', function ($join) use ($organization_id) {
                    $join->on('organization_info_cfgs.key', '=', 'details.key');
                    $join->where('organization_id', '=', $organization_id);
            })
            ->select(['organization_info_cfgs.key', 'organization_info_cfgs.name', 'details.content'])->get();
        return $info;
    }

    public function saveIntroductionInfo($input)
    {
        $organization_id = isUserBusiness() ? getOrgId() :
            (isset($input["organization_id"]) ? (int)$input["organization_id"] : 0);

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        DB::beginTransaction();
        OrganizationInfoDetail::where('organization_id', $organization_id)->delete();

        $introductionInfo = $input['introduction_info'];
        foreach ($introductionInfo as $info) {
            $itemInfo = [
                'organization_id' => $organization_id,
                'content' => $info['content'] ?? '',
                'key' => $info['key']
            ];
            OrganizationInfoDetail::insert($itemInfo);
        }
        DB::commit();
        return ["success" => true];
    }

    public function getCertificates($organization_id, $pageSize = 10)
    {
        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $cers = Certificate::where("certificateable_id", $organization_id)
            ->where('certificateable_type', '=', Organization::class);
        return $cers->orderBy("created_at", 'asc')->paginate($pageSize);
    }

    public function saveCertificate($input)
    {
        $organization_id = isUserBusiness() ? getOrgId() :
            (isset($input["organization_id"]) ? (int)$input["organization_id"] : 0);

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $validate = [
            'title' => 'required|max:255',
            'certificate_category_id' => 'required',
            'description' => 'required|max:255',
            'image' => CommonRules::IMAGE
        ];

        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        $img_path = '';
        $deleteImage = 0;
        if (isset($input["image"])) {
            $check = self::checkImage($input["image"]);
            if (!$check) {
                return response()->json(['error' => array("image" => "File upload không phải là ảnh.")], 422);
            }

            $file = UploadFileUtil::uploadImageFile($input["image"]);
            $img_path = $file->path;
        } else {
            $deleteImage = $input["delete_image"] ?? 0;
        }

        $id = $input['id'] ?? 0;
        $cer = $id == 0 ? new Certificate() : Certificate::findOrFail($id);
        $cer->fill($input);
        $cer->certificateable_type = Organization::class;
        $cer->certificateable_id = $organization_id;
        if (strlen($img_path) > 0) {
            $cer->image = $img_path;
        }
        if ($deleteImage == 1) {
            $cer->image = null;
        }
        $cer->save();
        return array("success" => true);
    }

    public function deleteCertificate($certificate, $organization_id)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $certificate->delete();
        return ["success" => true, "msg" => "Xóa chứng nhận, xác nhận thành công"];
    }

    public function getBranches($organization_id, $pageSize = 10)
    {
        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $branches = Branch::where("organization_id", $organization_id)
            ->join('regions', 'regions.id', '=', 'branches.region_id')
            ->selectRaw('branches.*,
                concat(ifnull(branches.address, ""), "-", regions.full_name) as full_address');
        return $branches->orderBy("created_at", 'asc')->paginate($pageSize);
    }

    public function saveBranch($input)
    {
        $organization_id = isUserBusiness() ? getOrgId() :
            (isset($input["organization_id"]) ? (int)$input["organization_id"] : 0);

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $id = $input['id'] ?? 0;

        $validate = [
            'region_id' => 'required|numeric|min:1',
            'map_lat' => 'nullable|numeric|max:90|min:-90',
            'map_long' => 'nullable|numeric|max:180|min:-180'
        ];
        $validate['name'] = $id == 0 ? 'required|max:255|unique:branches,name' : 'required|max:255';
        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        if ($id > 0) {
            $count = Branch::where('id', '!=', $id)
                ->where('name', '=', $input['name'])->count();
            if ($count > 0) {
                return ["success" => true, "msg" => "Tên chi nhánh đã tồn tại. Vui lòng chọn tên khác"];
                exit;
            }
        }

        $branch = $id == 0 ? new Branch() : Branch::findOrFail($id);
        $branch->fill($input);
        $branch->organization_id = $organization_id;
        $branch->save();
        return array("success" => true);
    }

    public function deleteBranch($branch, $organization_id)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $branch->delete();
        return ["success" => true, "msg" => "Xóa chi nhánh thành công"];
    }

    /*
     *  $type: cung-cap or phan-phoi
     */
    public function getPartners($organization_id, $type, $keyword, $pageSize = 10)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $partners = Partner::where("organization_id", $organization_id)
            ->where("type", $type)
            ->whereRaw("((name like '%$keyword%') or (address like '%$keyword%'))");
        return $partners->orderBy("name", 'asc')->paginate($pageSize);
    }

    /*
     *  $type: cung-cap or phan-phoi
     */
    public function savePartner($input)
    {
        $organization_id = isUserBusiness() ? getOrgId() :
            (isset($input["organization_id"]) ? (int)$input["organization_id"] : 0);

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $id = $input['id'] ?? 0;

        $validate = [
            'address' => 'required',
            'name' => 'required|max:255'
        ];

        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        if ($id > 0) {
            $count = Partner::where('id', '!=', $id)
                ->where('name', '=', $input['name'])
                ->where('type', '=', $input['type'])->count();
            if ($count > 0) {
                return ["success" => true, "msg" => "Tên cơ sở "
                    . ($input['type'] == "cung-cap" ? "cung cấp" : "phân phối")
                    . " đã tồn tại. Vui lòng chọn tên khác"];
                exit;
            }
        }

        $partner = $id == 0 ? new partner() : partner::findOrFail($id);
        $partner->fill($input);
        $partner->organization_id = $organization_id;
        $partner->save();
        return array("success" => true);
    }

    /*
     *  $type: cung-cap or phan-phoi
     */
    public function deletePartner($partner, $organization_id)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $partner->delete();
        return ["success" => true, "msg" => "Xóa thành công"];
    }

    /*
     *  $type: pay or ship
     */
    public function getPayShipTypes($organization_id, $type, $pageSize = 10)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;
        // check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $model = $type == "pay" ? OrganizationPayType::class : OrganizationShipType::class;
        $tbl = $type == "pay" ? "organization_pay_types" : "organization_ship_types";
        $PayShip = $model::where("organization_id", $organization_id)
            ->join($type."_types as b", "b.id", "=", $tbl.".".$type."_type_id")
            ->selectRaw($tbl.".*, b.name as $type"."_type_name");
        return $PayShip->orderBy($type . "_type_id", 'asc')->paginate($pageSize);
    }

    /*
     *  $type: pay or ship
     */
    public function saveOrgShipPayType($input)
    {
        $organization_id = isUserBusiness() ? getOrgId() :
            (isset($input["organization_id"]) ? (int)$input["organization_id"] : 0);

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $type = $input['type'];
        $id = $input['id'] ?? 0;

        $validate = [
            $type."_type_id" => Rule::unique("organization_".$type."_types")
                    ->where(function ($query) use ($id, $organization_id) {
                        return $query->where('id', '!=', $id)->where('organization_id', $organization_id);
                    }),
            'content' => 'required|max:1023'
        ];

        $messages = [
            $type.'_type_id.unique' => 'Hình thức '. ($type == 'pay' ? 'thanh toán' : 'vận chuyển') .' này đã tồn tại'
        ];

        $validator = Validator::make($input, $validate, $messages);

        if ($validator->fails()) {
            $arr = $validator->errors()->toArray();
            $errors = [];
            foreach ($arr as $key => $item) {
                $errors[$key] = implode(' ; ', $item);
            }
            return response()->json(['error' => $errors], 422);
        }

        $model = $type == "pay" ? OrganizationPayType::class : OrganizationShipType::class;

        $objPayShip = $id == 0 ? new $model : $model::findOrFail($id);
        $objPayShip->fill($input);
        $objPayShip->organization_id = $organization_id;
        $objPayShip->save();
        return array("success" => true);
    }

    /*
     *  $type: pay or ship
     */
    public function deleteOrgShipPayType($organization_id, $orgPayShipTypeId, $type)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;
        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $model = $type == "pay" ? OrganizationPayType::class : OrganizationShipType::class;
        $objPayShip = $model::findOrFail($orgPayShipTypeId);
        $objPayShip->delete();
        return ["success" => true, "msg" => "Xóa thành công"];
    }

    public function getProductCertificates($organization_id, $product_id)
    {
        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $product = Product::where("organization_id", $organization_id)
            ->where("id", $product_id)->first();
        return $product ? $product->certificates : [];
    }

    public function saveProductCertificate($input)
    {
        $organization_id = $input['organization_id'];
        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $validator = Validator::make($input, [
            'title' => 'required|max:255',
            'certificate_category_id' => 'required',
            'description' => 'required|max:255',
            'image' => CommonRules::IMAGE
        ]);

        if ($validator->fails()) {
            //Neu du lieu khong validate
            return response()->json(['error' => $validator->errors()], 422);
        }

        $img_path = '';
        $deleteImage = 0;
        if (isset($input["image"])) {
            $check = self::checkImage($input["image"]);
            if (!$check) {
                return response()->json(['error' => array("image" => "File upload không phải là ảnh.")], 422);
            }

            $file = UploadFileUtil::uploadImageFile($input["image"]);
            $img_path = $file->path;
        } else {
            $deleteImage = $input["delete_image"] ?? 0;
        }

        $product_id = $input["product_id"] ?? 0;
        $product = Product::find($product_id);
        if ($product) {
            $id = $input["id"] ?? 0;
            $certificate = $id == 0 ? new Certificate() : Certificate::findOrFail($id);
            $certificate->certificateable_type = "App\Models\Product";
            $certificate->certificateable_id = $product_id;
            $certificate->certificate_category_id = $input["certificate_category_id"];
            $certificate->title = $input["title"];
            $certificate->description = $input["description"];
            if (strlen($img_path) > 0) {
                $certificate->image = $img_path;
            }
            if ($deleteImage == 1) {
                $certificate->image = null;
            }
            $certificate->save();
            return ["success" => true];
        } else {
            return ["success" => false, "msg" => "Sản phẩm không tồn tại. Vui lòng kiểm tra lại!"];
        }
    }

    public function deleteProductCertificate($cer, $organization_id)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $cer->delete();
        return ["success" => true, "msg" => "Xóa chứng nhận, xác nhận thành công"];
    }

    public function getAllOrgLink($organization_id, $type, $request)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $orgLinks = Organization::where('organization_type_id', '>=', 100)
            ->where('id', '<>', $organization_id)
            ->whereRaw("not exists (select partners.id from partners
                where partners.link_organization_id=organizations.id and partners.type='$type')");
        if ($request->filled('keyword')) {
            $keyword = $request->input('keyword');
            $orgLinks->where(function ($query) use ($keyword) {
                $query->where('name', 'like', '%' . $keyword . '%')
                    ->orWhere('address', 'like', '%' . $keyword . '%');
            });
        }

        $pageSize = $request->perPage ?? 10;

        $orgLinks = $orgLinks->select(['id', 'name', 'address', 'tel', 'email', 'website', 'created_at'])
            ->orderBy("name", 'asc')->paginate($pageSize);
        return $orgLinks;
    }

    public function addPartnerLinkOrg($organization_id, $type, $link_organization_id)
    {
        $organization_id = isUserBusiness() ? getOrgId() : $organization_id;

        // Check quyen
        $this->orgAuthor->canManageId($organization_id, $this->abilityOrgInfo);

        $linkOrg = Organization::findOrFail($link_organization_id);
        $partner = new Partner();
        $partner["organization_id"] = $organization_id;
        $partner["type"] = $type;
        $partner["link_organization_id"] = $link_organization_id;
        $partner["name"] = $linkOrg->name;
        $partner["address"] = $linkOrg->address;
        $partner["tel"] = $linkOrg->tel;
        $partner["email"] = $linkOrg->email;
        $partner["website"] = $linkOrg->website;
        $partner->save();
        return ["success" => true];
    }

    /*
     * @author Nguyen Quan
     * common function for multi-call from others
     */
    public function registerBusiness($request)
    {
        $isUserGov = isUserGov();

        $input = $request->all();
        $validate = [
            'organization_type_id' => 'required',
            'name' => 'required|max:255',
            'logo_image' => CommonRules::IMAGE,
            'founding_type' => 'required',
            'founding_number' => 'required|max:31',
            'founding_by_gov' => 'required|max:127',
            'founding_date' => 'required|date_format:d/m/Y',
            'director' => 'required|max:63'
        ];

        if (!$isUserGov) {
            $validate['manage_organization_id'] = 'required';
        }

        $validator = Validator::make($input, $validate);

        if ($validator->fails()) {
            //Neu du lieu khong validate
            return response()->json(['error' => $validator->errors()], 422);
        }

        $img_path = '';
        if (isset($input["logo_image"])) {
            $fileImg = $input["logo_image"];
            $check = getimagesize($fileImg->getPathname());
            if (!$check) {
                return response()->json(['error' => array("logo_image" => "File upload không phải là ảnh.")], 422);
            }

            $file = UploadFileUtil::uploadImageFile($input["logo_image"]);
            $img_path = $file->path;
        }

        $organization = new Organization();
        $organization->fill($input);

        $validator = CommonUtil::validateFoundingNumber($request, 0);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }

        $validator = CommonUtil::validateMap2($request, $organization);
        if ($validator->fails()) {
            return response()->json(['error' => $validator->errors()], 422);
        }


        if (strlen($img_path) > 0) {
            $organization->logo_image = $img_path;
        }

        if ($request->organization_type_id == OrgType::CSSX) {
            $organization["commercial_center_id"] = null;
        } elseif ($request->organization_type_id == OrgType::CSKD) {
            $organization["area_id"] = null;
        }

        $organization->organization_type_id = $input["organization_type_id"]; // do không để fillable trong model

        $manageOrgId = $isUserGov ? getOrgId() : $input["manage_organization_id"];
        $organization->manage_organization_id = $manageOrgId; // do không để fillable trong model

        $organization->status = $isUserGov ? $input["status"] : 0;
        $organization->created_by = getUserId();
        $organization->save();

        if (!$isUserGov) {
            $user = User::find(getUserId());
            $user->organization_id = $organization->id;
            $user->save();
        }

        return ['success' => true, 'organization_id' => $organization->id];
    }

    public function delete($organization_id)
    {
        $org = Organization::findOrFail($organization_id);
        // Check quyen
        $this->manageOrganizationAuthor->canManage($org);
        $org = Organization::findOrFail($organization_id);
        $org->deleted_by = getUserId();
        $org->save();
        $org->delete();
        return ['success' => true];
    }

    public function updateStatus($organization_id, $status)
    {
        $org = Organization::findOrFail($organization_id);
        // Check quyen
        $this->manageOrganizationAuthor->canManage($org);
        $org->status = $status;
        $org->updated_by = getUserId();
        $org->save();
        return ['success' => true];
    }
}
