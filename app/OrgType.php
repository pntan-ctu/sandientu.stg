<?php

namespace App;

final class OrgType
{
    const MEMBER = 0;
    const ADMIN = 1;
    const VPDP = 2;
    const SYT = 3;
    const SNN = 4;
    const SCT = 5;
    const HUYEN = 6;
    const XA = 7;
    const CSSX = 100;
    const CSKD = 101;
}
