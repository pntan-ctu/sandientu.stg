## About SDC

Phần mềm kết nối cung cầu sản phẩm nông sản thực phẩm an toàn trên địa bàn tỉnh Thanh Hóa

## Thư viện

- Giao diện phân hệ quản lý [AdminLTE](https://github.com/almasaeed2010/AdminLTE) | [doc](https://adminlte.io/docs/2.4/installation)
- Menu [laravel-menu](https://github.com/lavary/laravel-menu) | [doc](#)
- Form [anahkiasen/former](https://github.com/formers/former) | [doc](https://github.com/formers/former/wiki)
- DataTables [yajra/laravel-datatables-oracle](https://github.com/yajra/laravel-datatables) | [doc](http://yajrabox.com/docs/laravel-datatables/master)
- Validation [jquery-validation](https://github.com/jquery-validation/jquery-validation) | [doc](https://jqueryvalidation.org/documentation/)
- Laravel language [laravel-lang](https://github.com/caouecs/Laravel-lang) | [doc](https://github.com/caouecs/Laravel-lang/blob/master/README.md)
- CKEditor 4 [CKeditor 4](https://ckeditor.com/ckeditor-4/) | [doc](https://ckeditor.com/docs/ckeditor4/latest/)  
Khi dùng trên modal, cần gọi hàm js: `sdcApp.fixCKEditorModal()` để có thể focus được vào các điều khiển trên toolbox của CKEditor
- Input Mask [Inputmask](https://github.com/RobinHerbots/Inputmask) | [doc](https://github.com/RobinHerbots/Inputmask/blob/4.x/README.md)  
Example: 

    ```php
    $form.find('#email').inputmask('email');  
    $form.find('#tel').inputmask('tel');// 11 số  
    $form.find('.date').inputmask('date');// dd/mm/yyyy - khi submit về máy chủ có định dạng dd/mm/yyyy 
    $form.find('#price').inputmask('integer-group');// 123.456.789 - khi submit về máy chủ sẽ tự động được gỡ mask thành 123456789
    $form.find('#weight').inputmask('currency');// 123.456.789,09 - khi submit về máy chủ sẽ tự động được gỡ mask thành 123456789.09
    ```
Có thể xem và khai báo thêm các định dạng mới vào file public/js/inputmask/scd-alias.js
- Slug [cviebrock/eloquent-sluggable](https://github.com/cviebrock/eloquent-sluggable) | [doc](https://github.com/cviebrock/eloquent-sluggable/blob/master/README.md#updating-your-eloquent-models)  
- Intl (định dạng số, ngày) [Propaganistas/Laravel-Intl](https://github.com/Propaganistas/Laravel-Intl) | [doc](https://github.com/Propaganistas/Laravel-Intl/blob/master/README.md)  
    - Số:  
        - Format:  
        Dùng facade:  
        
            ```php
            use Propaganistas\LaravelIntl\Facades\Number;
            
            Number::format(1000); // '1.000'
            Number::percent('0.75'); // '75%'
            Currency::format(99999, 'VND'); // 99.999 ₫
            ```
            hoặc dùng helper:
            
            ```php
            number(1000); // '1.000'
            number()->percent('0.75'); // '75%'
            currency(99999, 'VND'); // 99.999 ₫
            ```
        
        - Parse: `Number::parse('1.893.938,32')` hoặc `number()->parse('1.893.938,32')`   
    - Ngày: Kiểu dữ liệu khuyến nghị dùng là class [Carbon\Carbon](https://carbon.nesbot.com) (thừa kế và mở rộng nhiều tiện ích từ class DateTime của PHP). Các property của mode khai
    báo dữ liệu kiểu date, datetime (trong mảng $casts) hoặc được khai báo trong mảng $dates của model sẽ có kiểu dữ liệu là Carbon.
        - Format:  

            ```php 
            use Carbon;
            
            $date = carbon('2018-9-29 22:44:55');
            
            $date->toShortDateString(); // 29/09/2018
            $date->toMediumDateString(); // 29 thg 9, 2018
            $date->toLongDateString(); // 29 tháng 9, 2018
            $date->toFullDateString(); // Thứ Bảy, 29 tháng 9, 2018
            
            $date->toShortTimeString(); // 22:44
            $date->toMediumTimeString(); // 22:44:55
            $date->toLongTimeString(); // 22:44:55 GMT+7
            $date->toFullTimeString(); // 22:44:55 Giờ Đông Dương
            
            $date->toShortDatetimeString(); // 22:44, 29/09/2018
            $date->toMediumDatetimeString(); // 22:44:55, 29 thg 9, 2018
            $date->toLongDatetimeString(); // 22:44:55 GMT+7 29 tháng 9, 2018
            $date->toFullDatetimeString(); // 22:44:55 Giờ Đông Dương Thứ Bảy, 29 tháng 9, 2018
            ```

        - Parse:
        
            ```php
            $date = parseDate('22/1/2018'); // 2018-1-22 00:00:00.0000
            $time = parseDateTime('22/1/2018 23:02:36'); // 2018-1-22 23:02:36.0000
            ```
- sdc Thư viện dùng chung:
    - Định dạng cột ngày trên datatables dùng hàm render
    `sdcApp.renderDataTableDate(toFormat)`, với toFormat là chuỗi định dạng theo
    chuẩn của thư viện [Moment](https://momentjs.com). Không truyền tham số (mặc định) sẽ có định dạng DD/MM/YYYY.
    Có thể truyền giá trị tắt dst (date-short-time định dạng HH:mm DD/MM/YYYY),
    dft (date-full-time định dạng HH:mm:ss DD/MM/YYYY). Ví dụ:
    
        ```javascript
        var tableConf = {
            ...
            "columns": [
                ...
                {
                    data: 'created_at',
                    class: 'text-center',
                    render: sdcApp.renderDataTableDate('dft') // 23:12:56 23/8/2018
                },
                {
                    data: 'updated_at',
                    class: 'text-center',
                    render: sdcApp.renderDataTableDate('dst') // 23:12 23/8/2018
                },
                {
                    data: 'deleted_at',
                    class: 'text-center',
                    render: sdcApp.renderDataTableDate() // 23/8/2018
                },
                ...
            ]
            ...
        };

        ```

    - Định dạng cột số trên datatable: dùng hàm render `sdcApp.renderDataTableNumber(precision, postfix, prefix,
    thousands, decimal)`. Ví dụ:  
    
        ```javascript
        var tableConf = {
            ...
            "columns": [
                ...
                {
                    data: 'price',
                    class: 'text-right',
                    render: sdcApp.renderDataTableNumber() // 123.456.789
                },
                {
                    data: 'price',
                    class: 'text-right',
                    render: sdcApp.renderDataTableNumber(2) // 123.456.789,12
                },
                {
                    data: 'price',
                    class: 'text-right',
                    render: sdcApp.renderDataTableNumber(2, ' đ') // 123.456.789,23 đ
                },
                {
                    data: 'price',
                    class: 'text-right',
                    render: sdcApp.renderDataTableNumber(2, ' đ', 'p ', "'", ',') // p 123'456'789,23 đ
                }                 
                ...
            ]
            ...
        };

        ```
- Gói mở rộng Responsive cho DataTables  
    Để áp dụng gói mở rộng này lên một datatables, cần thực hiện các bước  
    - Thêm class `responsive` vào thẻ table
    - Thêm thuộc tính `width` vào mã javascript khai báo các column
    - Bỏ phần `<colgroup> ... <colgroup>` trong thẻ table
    - Thêm các class điều khiển việc ẩn hiện cho các cột vào thẻ `th`, chi tiết tại https://datatables.net/extensions/responsive/classes.  
    Một số class thường dùng:  
        - all: luôn luôn hiển thị, thường áp dụng cho cột tên, cột nút lệnh
        - min-mobile-l: chỉ hiển thị khi kích thước ngang > 320
        - min-tablet-p: chỉ hiển thị khi kích thước ngang > 480
        - min-tablet-l: chỉ hiển thị khi kích thước ngang > 768  
    Ví dụ:  

    ```html
    <table class="table table-bordered table-striped table-hover responsive" id="datatable" width="100%">
        <thead>
        <tr role="row" class="heading">
            <th class="all">Họ tên</th>
            <th class="min-tablet-p">Email</th>
            <th class="min-tablet-p">Mật khẩu khởi tạo</th>
            <th class="min-tablet-l">Ngày tạo</th>
            <th class="all">Kích hoạt</th>
            <th>Reset mật khẩu</th>
            <th class="all"></th>
        </tr>
        </thead>
    </table>
    ```
    ```javascript
    "columns": [
            {"data": 'name', "width": "20%"},
            {"data": 'email', "width": "15%"},
    ```

## Best practices

- https://github.com/alexeymezenin/laravel-best-practices

## QA tools
- Check code style và fix: [squizlabs/PHP_CodeSniffer](https://github.com/squizlabs/PHP_CodeSniffer)
    - Cài: composer global require "squizlabs/php_codesniffer=*"
    - Check: đứng trong thư mục gốc mã nguồn:
        - Check tất cả file có đuôi .php thư mục app, trừ file app/Support/helpers.php  
            `phpcs --standard=PSR2 --extensions=php --ignore=app/Support/helpers.php app`  
        - Check tất cả thư mục app  
            `phpcs --standard=PSR2 app`  
        - Check cụ thể file app/Models/Abc.php  
            `phpcs --standard=PSR2 app/Models/Abc.php`  
    - Fix: đứng trong thư mục gốc mã nguồn:
        - Fix tất cả file trong thư mục app  
            `phpcbf --standard=PSR2 app`
        - Fix một file cụ thể
            `phpcbf --standard=PSR2 app/Models/Abc.php`
- Fix code style: [FriendsOfPHP/PHP-CS-Fixer](https://github.com/FriendsOfPHP/PHP-CS-Fixer)
    - Cài: composer global require friendsofphp/php-cs-fixer
    - Fix:  
        `php-cs-fixer fix /path/to/dir`  
        `php-cs-fixer fix /path/to/file`  
(Truy cập các link gốc để có các option nâng cao)

## Các lệnh thường dùng cho module messenger
- File sass và js (file tự code từ đầu) nên để trong thư mục resource và dùng các lệnh biên dịch sang thư mục public:
(nguồn và dích theo cấu hình trong file webpack.mix.js)
	- Biên dịch và tự cập nhật file đích khi có thay đổi ở file nguồn:  
	`npm run watch`  (trên linux)  
	`npm run watch-poll`  (trên windows)  
	- Biên dịch js, sass cho môi trường dev  
	`npm run dev`
	- Biên dịch js, sass cho môi trường product (js và css ở public sẽ ở dạng nén)  
	`npm run prod`  

Để tính năng chat realtime hoạt động được, cần chạy websockets server và queue:  
- Larave-websockets:
	`php artisan websockets:serve`
- Queue
	`php artisan queue:work`