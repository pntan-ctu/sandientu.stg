Inputmask.extendAliases({
    'date': {
        alias: "datetime",
        inputFormat: 'dd/mm/yyyy'
    }
});
Inputmask.extendAliases({
    'tel': {
        mask: '9',
        repeat: 11
    }
});
Inputmask.extendAliases({
    'integer-group': {
        alias: "numeric",
        groupSeparator: ".",
        autoGroup: true,
        digits: 0,
        autoUnmask: true,
        removeMaskOnSubmit: true
    }
});
Inputmask.extendAliases({
    'currency': {
        prefix: "",
        groupSeparator: ".",
        radixPoint: ',',
        autoUnmask: true,
        removeMaskOnSubmit: true,
        onUnMask: function (maskedValue, unmaskedValue, opts) {
            if (unmaskedValue === "" && opts.nullable === true) {
                return unmaskedValue;
            }
            var processValue = maskedValue.replace(opts.prefix, "");
            processValue = processValue.replace(opts.suffix, "");
            processValue = processValue.replace(new RegExp(Inputmask.escapeRegex(opts.groupSeparator), "g"), "");
            if (opts.radixPoint !== "" && processValue.indexOf(opts.radixPoint) !== -1)
                processValue = processValue.replace(Inputmask.escapeRegex.call(this, opts.radixPoint), ".");

            return processValue;
        }
    }
});