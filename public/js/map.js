var map; var markersArray = []; var urlStar = mappath + 'css/images/star_'; var prev_infowindow;
function toCavans(iType, markerId){
        if(markersArray[iType]){
            $.each(markersArray[iType], function(iType, vType) {
                if(vType.mid == markerId){
                    vType.setMap(map);
                    map.setCenter(vType.position);
                    //vType.setAnimation(google.maps.Animation.BOUNCE);
                    google.maps.event.trigger(vType, 'click');
                }
            });
        }
        return;
    };
$(function() {
    function findCavans(iType, markerId){
        if(markersArray[iType]){
            $.each(markersArray[iType], function(iType, vType) {
                if(vType.mid == markerId){
                    vType.setMap(map);
                    map.setCenter(vType.position);
                    //vType.setAnimation(google.maps.Animation.BOUNCE);
                    google.maps.event.trigger(vType, 'click');
                }
            });
        }
        return;
    };
    
    function createMapPanel(){
        var content = '';
        $.each(jmaptype, function(iType, vType) {content += '<label><input type="checkbox" value="' + vType.id + '"' + (vType.checked == "1"? 'checked = "checked"':'')+ '/> <img height="14" width="14" alt="" src="'+  mapurl + "14/14/" + vType.icon_id + ".jpg" + '"/> ' + vType.ten + '</label>';});
        $("#map_category").append(content);
        $("#map_category input[type='checkbox']").click(function() {showHideCanvas(this);});
    };
    
    function createCanvans(vDetail, imgId){
        var myLatlng = new google.maps.LatLng(vDetail.lat, vDetail.lng); var image = mapurl + "24/21/" + imgId + ".jpg"; 
        var marker = new google.maps.Marker({ position: myLatlng, title: vDetail.ten, icon: image, mid: vDetail.id });
        var contentString = '';
        contentString += '<div style = "text-align: left; padding-bottom: 10px;">';
            contentString += '<div><h1 style = "margin: 0; padding: 0; font-size: 17px;">'+ vDetail.ten +'';
            if(vDetail.link) contentString += '<a href = "'+ vDetail.link + '" target="_blank" style = "text-decoration: none; padding-left: 5px; font-family: tahoma;"> chi tiết...</a>';
            contentString += '</h1></div>';
            
            contentString += '<div style="line-height: 22px;"><table boder = "0" cellpading = "0" cellspace = "0" ><tr><td>'
                if(vDetail.address) contentString += '<span ><b>' + vDetail.address + '</b></span>';
                if(vDetail.email)contentString += '<span style = "display: block;"> Email: ' + vDetail.email + '</span>';
                if(vDetail.phone)contentString += '<span style = "display: block;"> Phone: ' + vDetail.phone + '</span>';
                if(vDetail.note)contentString += '<span style = "line-height: 1.3em; text-decoration: none; max-height: 6.4em; overflow: hidden; display: block; color: black; font-size: 12px; font-family: tahoma; text-align: justify;">' + vDetail.note + '</span>';
                contentString += '</td><td style = "padding:25px 0 0 10px; vertical-align: top;"><span><img width="90" height="54" alt="" src="'+  mapurl + "90/54/" + vDetail.pic_id + ".jpg" + '"/></span>';
                if(vDetail.rank)contentString += '<span style = "display: block; font-family: tahoma; font-size: 10px;">' + createRateStar(vDetail.rank) + ' </span>';
            contentString += '</td></tr></table></div>';
        contentString += '</div>';
        marker.info = new google.maps.InfoWindow({maxWidth: 350, content: contentString }); 
        google.maps.event.addListener(marker, 'click', function() { if(prev_infowindow) {prev_infowindow.close();} prev_infowindow = marker.info; marker.info.open(map, marker); });
        if(openmarker == vDetail.id){
            google.maps.event.trigger(marker, 'click');
        }
        return marker;
    };
    
    var initSearch=function(){ var map_search = $("#map_search"); var map_text_search = $("#map_text_search"); var map_search_display = $("#map_search_display"); var width = parseInt(map_search.width() - (map_search.width()/3)); if(width > 300) {width = 300;} map_text_search.width(width); map_search_display.width(width); var lastText = $('<input type="hidden" value="" id = "map_text_search_last"/>'); map_text_search.keyup(function() { var newVal = map_text_search.val(); var oldVal = lastText.val(); if( oldVal != newVal){ lastText.val(newVal); clearTimeout($.data(this, 'timer_mapsearch')); $(this).data('timer_mapsearch', setTimeout(getTextSearch, 200));} }); map_text_search.click(function(){ setSearchDisplay(1);});}; var setSearchDisplay=function(display){ display = ($.type(display.data) !== 'undefined')? display.data: display; var map_search_display = $("#map_search_display"); var crr_display = (map_search_display.css("display") !== "none")? 1:0; var listChild = map_search_display.children().length; if(display && !crr_display && listChild){ map_search_display.slideDown(300); $(document).bind('mousedown', function(){setTimeout(function(){setSearchDisplay(0)}, 200);});} if(!display && crr_display){ map_search_display.slideUp(200); $(document).unbind('mousedown'); } return false; };var getTextSearch=function(){ var map_text_search = $("#map_text_search"); var map_search_display = $("#map_search_display"); $.ajax({url : "?call=map.getTextSearch", type: "post",dataType : "json", data : {"tinh_id" : jmaplocal.tinh_id,"huyen_id" : jmaplocal.id,"textsearch":map_text_search.val()}, success : function(rs){map_search_display.html(""); if(rs && rs.length){ $.each(rs, function(i, v) {var item = "<label class = 'map_item_srearched' mType = '" + v.type_id + "' mId = '" + v.id + "'>" + v.ten + "</label>"; map_search_display.append(item); }); $("#map_search_display label[class='map_item_srearched']").click(function() { var iType = $(this).attr("mType"); var markerId = $(this).attr("mId"); findCavans(iType, markerId);}); setSearchDisplay(1); } else {setSearchDisplay(0);}return;} }); };var createRateStar=function(rate) { rate = parseInt(rate);if(rate == 0) {return '';}if(rate>10) {rate = 10;} var numStar = parseInt(rate/2); var endStar = rate - (numStar*2); var imgContent = ''; var imgTmp = '<img boder="0" style = "float:left; width:13px; height:13px;" src = '; for (var i=0;i<numStar && i<5;i++){imgContent += imgTmp + '"'+ urlStar + '5.png"/>';} if(numStar==0 && endStar==0)endStar = 1; if(endStar>0 && numStar<5){ imgContent += imgTmp + '"'+ urlStar + endStar.toString() + '.png"/>'; } imgContent += '  <span style = "display: block; font-family: tahoma; font-size: 10px; line-height: 14px;">('+ rate.toString() + '/10)</span>'; return imgContent; };var setInitCanvans=function(){$.each(jmaptype, function(iType, vType) {if(jmapdetai[vType.id]){var tmpDetail = jmapdetai[vType.id];$.each(tmpDetail, function(iDetail, vDetail) {var marker = createCanvans(vDetail, vType.icon_id);if(vType.checked == "1"){ marker.setMap(map); }if(markersArray[vType.id]){var tmpArr = markersArray[vType.id];tmpArr.push(marker);markersArray[vType.id] = tmpArr;}else { markersArray[vType.id] = new Array(marker); }});}});};var showHideCanvas=function(check){check = $(check);var value = check.val();var isChecked = check.attr('checked');if(markersArray[value]){$.each(markersArray[value], function(iType, vType) {if(isChecked){vType.setMap(map);}else{vType.setMap(null);}});}};
    var initialize=function(){
        var myOptions = { center: new google.maps.LatLng(jmaplocal.lat, jmaplocal.lng), zoom: parseInt(jmaplocal.zoom), mapTypeId: google.maps.MapTypeId.ROADMAP }; map = new google.maps.Map(document.getElementById("map_canvas"),myOptions);
        setInitCanvans(); createMapPanel(); initSearch();
    };
   //init map 
   initialize();
});
