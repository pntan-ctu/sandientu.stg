partner = function (baseUrl, type) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name'},
            {"data": 'address'},
            {"data": 'tel'},
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-right',
                "render": function (value, type, data) {
                    if(data.link_organization_id == null)
                    {
                        return '<a href="' + baseUrl + '/' + data.id + '/edit"' + ' class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="glyphicon glyphicon-pencil"></i></a>\n\
                            <a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>';
                    }
                    else{
                        return '<a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>';
                    }
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [4, 'desc']
    };
    var tableConfOrgLink = {
        "rowId": 'id',
        "deferLoading": 0,
        "pageLength": 5,
        "info": false,
        "columns": [
            {data: 'name', name: 'name', "orderable": false},
            {data: 'address', name: 'address', "orderable": false},
            {data: 'tel', name: 'tel', "orderable": false},
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function (id) {
                    return '<a class="btn fa fa-plus-square add-org" data-toggle="tooltip" title="Thêm" style="font-size: 20px;padding:0px;"' +
                            'onclick="addOrg(this)"></a>';
                }
            },
            {data: 'id', "visible": false}
        ],
        "order": [[3, 'desc'], [1, 'asc']]
    };
    return {
        init_index: function () {
            CommonCRUD2(
                    {
                        baseUrl: baseUrl,
                        tableId: '#datatable',
                        tableConf: tableConf,
                        searchFormId: '#search-form',
                        searchFieldNames: ['keyword', 'organizationId'],
                        btnAddId: '#btn-add',
                        titleCreate: type == 'cung-cap' ? 'Thêm nhà cung cấp' : 'Thêm nhà phân phối',
                        titleUpdate: type == 'cung-cap' ? 'Cập nhật nhà cung cấp' : 'Cập nhật nhà phân phối',
                        modalSize: 'large',
                        nameColIndex: 0,
                        formShowedHandler: function (form) {
                            $('.js-example-basic-single').select2({
                                width: '100%',
                                dropdownParent: form,
                                templateResult: function (data) {
                                    // We only really care if there is an element to pull classes from
                                    if (!data.element) {
                                        return data.text;
                                    }

                                    var $element = $(data.element);
                                    var level = $element.attr('level');
                                    var pad = level * 20;
                                    var icon_class = "icon_class";
                                    if (level == 0)
                                        icon_class = "icon_class_0";
                                    if (level == 1)
                                        icon_class = "icon_class_1";
                                    if (level == 2)
                                        icon_class = "icon_class_2";
                                    if (level == 3)
                                        icon_class = "icon_class_3";

                                    var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right ' + icon_class + ' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                                    var $wrapper = $(wraptext);
                                    return $wrapper;
                                },
                                templateSelection: function (state) {
                                    if (!state.id) {
                                        return state.text;
                                    }
                                    return $(state.element).attr('path');
                                }
                            });
                        },
                    }
            );
        },
        init_create: function (_type, _organizationId) {
            organizationId = _organizationId;
            type = _type;
            $("#org_links").click(function () {
                $("#modal-org-links").modal("show");
            });
            $("#btnSaveOrgLinks").click(function () {

                $("#modal-org-links").modal("hide");
                var strId = "";
                var box = $('#box-add').children(".org-add");
                if ($(box).length > 0) {
                    for (var i = 0; i < $(box).length; i++) {
                        if ($(box[i]).attr('id') != '0')
                            strId = strId + "," + $(box[i]).attr('id');
                    }
                }
                $.ajax({
                    url: 'business/' + organizationId + '/' + type + '/partner/postOrg',
                    data: {
                        strId: strId,
                        organizationId: organizationId,
                        type: type
                    },
                    method: 'POST',
                    dataType: 'json',
                    success: function () {
                        $('#datatable').DataTable().ajax.reload(null, false);
                        $('#datatable-org-link').DataTable().ajax.reload(null, false);
                        $('.org-add').remove();
                        toastr.success("Cập nhật thành công");
                    },
                    error: function () {
                        sdcApp.showLoading(false);
                        toastr.error("Bạn chưa chọn cơ sở !");
                    }
                });
            });
            $(".org-add i").click(function (e) {
                e.preventDefault();
                $(this).parent().parent().remove();
            })
        },
        init_org_link: function () {
            CommonCRUD2(
                    {
                        baseUrl: baseUrl,
                        tableId: '#datatable-org-link',
                        tableConf: tableConfOrgLink,
                        searchFormId: '#search-form-link',
                        searchFieldNames: ['keyword', 'organizationId'],
                        bindDefaultEditAction: false,
                        formShowedHandler: function (form) {
                        }
                    }
            );
        }
    };

};
function addOrg(t) {
    var table = $('#datatable-org-link').DataTable();
    var data_row = table.row($(t).closest('tr')).data();
    if ($('#box-add #' + data_row.id).length == 0) {
        console.log($('#box-add #' + data_row.id).length);
        $('#box-add').append("<div class='org-add' id='" + data_row.id + "'><span  class='col-lg-12'>" + data_row.name + "<i class='fa fa-close icon-close'></i></span></div>");
    }
    $(".org-add i").click(function (e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    })
}