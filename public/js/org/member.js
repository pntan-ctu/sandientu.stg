Member = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name'},
            {"data": 'email'},
            {"data": 'password_reset', "orderable": false},
            {
                "data": 'created_at',
                "class": "text-center",
                "render": sdcApp.renderDataTableDate('dft')
            },
            {
                "data": 'active',
                "class": "text-center",
                "render": function (value, type, data) {
                    return value == 1 ? '<a><i class="glyphicon glyphicon-ok-circle"></i></a>' : '';
                }
            },
            {
                "data": 'id',
                "class": "text-center",
                "render": function (value, type, data) {
                    return data.is_me == 0 ? '<button title="Reset mật khẩu" class="btn btn-link btn-xs" onclick="resetPassword(' + data.id + ', \'' + data.name + '\')"><i class="glyphicon glyphicon-retweet"></i></button>' : '';
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "render": function (value, type, data) {
                    return data.is_me == 0 ? sdcApp.commonAjaxTblActionCell : ''; //sdcApp.commonAjaxTblEdit;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [5, 'desc']
    };
    var userConf = {
        baseUrl: baseUrl,
        tableId: '#datatable',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['keyword', 'active_search'],
        btnAddId: '#btn-add',
        titleCreate: 'Thêm người dùng',
        titleUpdate: 'Cập nhật người dùng',
        modalSize: 'large',
        formShowedHandler: function ($form) {
            $form.find('#email').inputmask('email');
            $form.find('#tel').inputmask('tel');
            $form.find('#date_of_birth').inputmask('date');
            $form.find('#date_of_birth').datepicker({
                language: 'vi',
                autoclose: true
            });
        }
    };
    $(function () {
        CommonCRUD2(userConf);
    });
};
