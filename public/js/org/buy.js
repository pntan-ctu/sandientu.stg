Buy = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'provider_name'},
            {
                data: 'total_money',
                class: 'text-right',
                render: sdcApp.renderDataTableNumber(0) // 23:12:56 23/8/2018
            },
            {
                data: 'order_date',
                class: 'text-center',
                render: sdcApp.renderDataTableDate('dft') // 23:12:56 23/8/2018
            }, // TODO date format
            {
                data: 'status',
                class: 'text-center',
                render: function (value, type, data) {
                    switch(value) {
                        case '1':
                            return "Chưa xử lý";
                        case '2':
                            return "Đã xử lý";
                        case '3':
                            return "Đã hủy";
                        case '4':
                            return "Đã xác nhận";
                        default:
                            return "Nháp";
                    }
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function (value, type, data) {
                    return '<button type="button" class="btn btn-link btn-xs btn-show" onclick="show_list(' + data.id + ');" data-toggle="modal" title="Xem chi tiết"><i class="fa fa-th-large"></i></button>';
                    //return sdcApp.commonAjaxTblDel;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [3, 'desc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'status', 'organization_id'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm chứng nhận, xác nhận',
            titleUpdate: 'Cập nhật chứng nhận, xác nhận',
            modalSize: 'large',
            nameColIndex: 0,
            beforeSerialize: function($form){
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            },
            //requestSaveDoneHandler: function(data){console.log(data)},
            //requestSaveFailHandler: function(xhr){console.log(xhr)},
            //requestDeleteDoneHandler: function(data){console.log(data)},
            //requestDeleteFailHandler: function(xhr){console.log(xhr)},
            formShowedHandler: function($form){
                $('.bootbox').on('shown.bs.modal', function () {
                    $('#my-editor').trigger('focus')
                })
                CKEDITOR.replace( 'my-editor');

            },
            createRequestData: function () {
                var organization_id = jQuery("#organization_id").val();
                return {organization_id: organization_id}
            },
            //storeHandler: function(){console.log('storeHandler'); return false},
            //updateHandler: function(){console.log('updateHandler'); return false},
            // buttons: {
            //     cancel: {
            //         label: "Hủy"
            //     },
            //     ok: {
            //         label: "Lưu",
            //         callback: function () {
            //             okHandler(editItemId);
            //             return false;
            //         }
            //     }
            // },
            // bindRowActionHandlers: function () {
            //     sdcApp.bindTblDelEvent(conf.tableId, oTable, conf.baseUrl, conf.nameColIndex);
            //     sdcApp.bindTblEditEvent(conf.tableId, oTable, function (id) {
            //         var editUrl = conf.baseUrl + '/' + id + '/edit';
            //         editItemId = id;
            //         okHandler = update;
            //         showForm(conf.titleUpdate, editUrl);
            //     });
            // }
            // bindDefaultEditAction: true,
            // bindDefaultDeleteAction: true,
        };
        return CommonCRUD2(conf);
        /*CommonCRUD(
         baseUrl,
         '#datatable',
         tableConf,
         '#search-form',
         ['keyword', 'active'],
         '#btn-add',
         'Thêm vùng',
         'Cập nhật vùng',
         'large'
         );*/
    });
};
