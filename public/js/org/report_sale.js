ReportSale = function (baseUrl, organizationId) {
    var tableConf = {
        "columns": [
            {"data": 'stt', "orderable": false, "class": "text-center"},
            {"data": 'name', "orderable": false},
            {"data": 'amount', "class": "text-center"},
            {"data": 'money', "class": "text-right", render: sdcApp.renderDataTableNumber(0, ' đ')}
        ],
        "order": [3, 'desc']
    };
    return {
            init_index: function () {
                       $('.datepicker').inputmask('date');
                        $('.datepicker').datepicker({
                            language: 'vi',
                            autoclose: true
                        });
                        $('#to_date').datepicker('setDate', new Date());
                        var date = new Date();
                        date.setMonth(date.getMonth(), 1);
                        $('#from_date').datepicker('setDate', date);

                        $(".report_type").change(function () {
                            window.location.href = "{{url('manage/report-rank-org/')}}/" + this.value;
                        });
                        $("#btnExport").click(function () {
                        sdcApp.blockUI();
                        var from_date = $('#from_date').val();
                        var to_date = $('#to_date').val();
                        var urlReport = 'business/' + organizationId + '/report-sale/exportExcel?from_date='+from_date+'&to_date='+to_date;
                        window.location.href = urlReport;
                        sdcApp.unblockUI();
                        });
                CommonCRUD2(
                    {
                        baseUrl: baseUrl,
                        tableId: '#datatable',
                        tableConf: tableConf,
                        searchFormId: '#search-form',
                        searchFieldNames: ['from_date', 'to_date'],
                        btnAddId: '#btn-add',
                        titleCreate: 'Thêm mới cơ sở sản xuất / kinh doanh',
                        titleUpdate: 'Cập nhật cơ sở sản xuất / kinh doanh',
                        modalSize: 'large',
                        bindDefaultEditAction: false,
                    }
                );
            }
    }
};
