BlackList = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'user_name'},
            {"data": 'reason'},
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function (id) {
                    return '<a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>';
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [2, 'desc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'organizationId'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm chi nhánh ',
            titleUpdate: 'Cập nhật chi nhánh',
            modalSize: 'large',
            nameColIndex: 0,
            formShowedHandler: function (form) {
                    $('.js-example-basic-single').select2({
                        width:'100%',
                        dropdownParent: form,
                        templateResult: function (data) {
                            // We only really care if there is an element to pull classes from
                            if (!data.element) {
                                return data.text;
                            }

                            var $element = $(data.element);
                            var level = $element.attr('level');
                            var pad = level * 20;
                            var icon_class = "icon_class";
                            if(level==0)
                                icon_class="icon_class_0";
                            if(level==1)
                                icon_class="icon_class_1";
                            if(level==2)
                                icon_class="icon_class_2";
                            if(level==3)
                                icon_class="icon_class_3";

                            var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right '+icon_class+' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                            var $wrapper = $(wraptext);
                            return $wrapper;
                        },
                        templateSelection: function (state) {
                            if (!state.id) {
                                return state.text;
                            }
                            return $(state.element).attr('path');
                        }
                    });
                },
        };
        return CommonCRUD2(conf);
        /*CommonCRUD(
         baseUrl,
         '#datatable',
         tableConf,
         '#search-form',
         ['keyword', 'active'],
         '#btn-add',
         'Thêm vùng',
         'Cập nhật vùng',
         'large'
         );*/
    });
};
