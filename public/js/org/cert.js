Cert = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {
                "data": 'title',
                "class": "text-left"
            },
            {
                "data": 'description',
                "class": "text-left",
                orderable: false
            }, // TODO date format
            {
                "data": 'image', "class": "text-center", name: 'image', "orderable": false,
                render: function (value) {
                    if (value) {
                        return '<img style="width:100px;" src="' + '/image' + '/' + '100' + '/100/' + value + '" class="btn btn-link btn-xs"></a>';
                    } else {
                        return '<img style="width:100px;" src="' + '/img/' + 'no-image.png' + '" class="btn btn-link btn-xs"></a>';
                    }
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [0, 'asc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'active', 'organization_id'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm chứng nhận, xác nhận',
            titleUpdate: 'Cập nhật chứng nhận, xác nhận',
            modalSize: 'large',
            nameColIndex: 0,
            formShowedHandler: function (form) {
                $('#certificate_category_id').select2({
                    width: '100%',
                    dropdownParent: form,
                    placeholder: "Chọn loại chứng nhận, xác nhận..."
                });
            }
        };
        return CommonCRUD2(conf);
    });
};
