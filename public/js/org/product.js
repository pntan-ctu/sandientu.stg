Product = function (baseUrl, _product_id) {
    // var data = data:'price';
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {data: 'code', name: 'code', "orderable": false, "width": "15%"},
            {data: 'name', name: 'name', "width": "35%"},
           {data: 'price', name: 'price', render: sdcApp.renderDataTableNumber(0), "width": "20%"},
            {data: 'it5_dientichsudung', name: 'it5_dientichsudung', "orderable": false, "width": "20%"},
            {
                data: 'status', name: 'status', "orderable": false, "width": "15%",
                "render": function (data) {
                    switch (data) {
                        case "0":
                            return "Chưa kinh doanh"; 
                        case "1":
                            return "Đang bán";
                        case "2":
                            return "Theo mùa vụ";
                        case "3":
                            return "Đã hủy";
                        case "4":
                            return "Ngừng kinh doanh";
                    }
                    return "";
                }
            },
            // {data: 'duration', name: 'duration', "orderable": false, "visible": false},
            {
                "data": 'avatar_image', "width": "120",
                "name": 'avatar_image',
                "class": 'text-center',
                orderable: false,
                "render": function (data) {
                    return "<img width='100' height='60' src= '/image/100/100/" + data + "' />";
                }
            },
            {
                "data": 'id', "width": "10%",
                orderable: false,
                "class": 'text-center',
                "render": function (id) {
                    return '<a href="' + baseUrl + '/' + id + '/edit"' + ' class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="glyphicon glyphicon-pencil"></i></a><a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>';
                }
            },
            {data: 'updated_at', "visible": false}
        ],
        "order": [2, 'asc']
    };

    var tableConfCert = {
        "rowId": 'id',
        "columns": [
            {"data": 'title'},
            {
                data: 'created_at',
                "orderable": false,
                class: 'text-center',
                render: sdcApp.renderDataTableDate('dft') // 23:12:56 23/8/2018
            }, // TODO date format
            {
                "data": 'image', "class": "text-center", name: 'image',"orderable": false,
                render: function (value) {
                    if (value) {
                        return '<img style="width:200px; height:100px" src="' + '/image' + '/' + '100' + '/100/' + value + '" class="btn btn-link btn-xs"></a>';
                    } else {
                        return '<img style="width:200px; height:100px" src="' + '/img/' + 'no-image.png' + '" class="btn btn-link btn-xs"></a>';
                    }
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [1, 'asc']
    };

    var tableConfProductLink = {
        "rowId": 'id',
        "deferLoading": 0,
        "pageLength": 5,
        "info": false,
        "columns": [
            {data: 'code', name: 'code', "orderable": false},
            {data: 'name', name: 'name', "orderable": false},
            {data: 'cssx', name: 'cssx', "orderable": false},
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function (id) {
                    return '<a class="btn fa fa-plus-square add-product" data-toggle="tooltip" title="Thêm" style="font-size: 20px;padding:0px;"' +
                        'onclick="addProduct(this)"></a>';
                }
            },
            {data: 'updated_at', "visible": false}
        ],
        "order": [[4, 'desc'], [1, 'asc']]
    };

    var index = 1;
    var me;
    var organizationId;
    var product_id;

    return {
        addToTable: function (data) {
            console.log(data);
            for (var i = 0; i < data.length; i++) {
                index++;
                $(".grid_image").append("<div class='col-xs-4 '><i class='fa fa-close icon-close'></i>" +
                    "<img path='" + data[i].path + "' id='drag" + index + "' draggable='true' ondragstart='drag(event)' style='width: 100%;height: 50px;' src='" + 'image/0/0/' + data[i].path + "'></div>");
            }
            $(".grid_image i").click(function (e) {
                e.preventDefault();
                $(this).parent().remove();
            });

            if (data.length > 0 && $('.image_avatar>img').attr('path') == undefined) {
                $('.image_avatar>img').attr({'src': 'image/0/0/' + data[0].path, 'path': data[0].path});
            }
            if (data.length > 0) {
                $('#check_temp_image_path').val(data[0].check_temp_image);
            }
        },

// upload ajax
        upload: function (me) {
            var formData = sdcApp.getFormDataAndType($("#upload_form"));
            sdcApp.showLoading(true);
            $.ajax({
                url: 'upload/images',
                method: 'POST',
                data: formData.data,
                contentType: formData.contentType,
                dataType: 'json',
                processData: false
            }).done(function (rs) {
                if (rs.success) {
                    me.addToTable(rs.data);
                }
                sdcApp.showLoading(false);
            }).fail(function () {
                sdcApp.showLoading(false);
            });
        },
        savePrimaryInfo: function () {
            sdcApp.showLoading(true);
            var paths = [];
            var link_product_ids = [];
            var des = "";
            var form = $(".form-input");
            var path_avatar = $(".image_avatar>img").attr('path');
            $(".grid_image").children('div').each(function () {
                paths.push($(this).children('img').attr('path'));
            });
            $("#paths").val(paths);
            $("#path_avatar").val(path_avatar);

            var box = $('#box-add-create').children(".product-add");
            if ($(box).length > 0) {
                for (var i = 0; i < $(box).length; i++) {
                    if ($(box[i]).attr('id') != '0')
                        link_product_ids.push($(box[i]).attr('id'));
                    else
                        $("#descript").val($(box[i]).children('span').text());
                }
            }
            $("#link_product_id").val(link_product_ids);
            
            var dataForm = form.serialize();
            me.ajaxSave('business/' + organizationId + '/product' + (product_id == '' ? '' : '/' + product_id), dataForm);

        },
        ajaxSave: function (url, dataForm) {
            $.ajax({
                url: url,
                method: product_id == '' ? 'POST' : 'PUT',
                data: dataForm,
                dataType: 'json',
                processData: false
            }).done(function (data) {
                sdcApp.showLoading(false);
                if (!data.success) {
                    sdcApp.showToast({status: 'error', msg: data.msg});
                    return;
                }
                if (product_id == '') {
                    $("#step1").removeClass("active");
                    $("#step1").addClass("complete");
                    if (!$("#step2").hasClass('active')) {
                        $("#step2").removeClass("disabled");
                        $("#step2").addClass("complete");
                        $("#step3").removeClass("disabled");
                        $("#step3").addClass("active");
                    }
                }

                product_id = data.product_id;
                $(".id-primary").val(product_id);
                me.switch_step("2");

            }).fail(function (xhr) {
                sdcApp.showLoading(false);
                if (xhr.status === 422) {
                    var form = $(".form-input");
                    sdcApp.resetFormErrors(form);
                    var errors = xhr.responseJSON.errors;
                    sdcApp.renderFormErrors(errors, form);
                    sdcApp.showLoading(false);
                    sdcApp.showToast({status: 'error', msg: 'Vui lòng nhập đủ thông tin'});
                }
            });
        },
        saveMoreInfo: function () {
            for (instance in CKEDITOR.instances) {
                CKEDITOR.instances[instance].updateElement();
            }
            sdcApp.showLoading(true);
            var form = $("#more-info");
            var dataForm = form.serialize();
            $.ajax({
                url: 'business/' + organizationId + '/product/store-more-info',
                method: 'POST',
                data: dataForm,
                dataType: 'json',
                processData: false
            }).done(function (data) {
                sdcApp.showLoading(false);
                if (!data.success) {
                    sdcApp.showToast({status: 'error', msg: data.msg});
                    return;
                }
                me.switch_step("3");
            }).fail(function (xhr) {
                sdcApp.showLoading(false);
                sdcApp.showToast({status: 'error', msg: 'Việc lưu thông tin bị lỗi. Vui lòng thử lại!'});
            });
        },
        switch_step: function (step) {
            $(".bs-wizard-info").css('color', '#717171')
            $("#step" + step + " .bs-wizard-info").css('color', '#d91c5f')
            $(".form-info").css("display", "none");
            $(".step_" + step).css("display", "block");
            if (step == 2) {
                me.getViewMoreInfo();
            } else if (step == 3) {
                me.getViewCertificate();
            }
        },
        getViewMoreInfo: function () {
            if ($("#more_info_container").children().length != 0)
                return;
            sdcApp.showLoading(true);
            $.ajax({
                url: 'business/' + organizationId + '/product/get-view-more-info',
                method: 'GET',
                data: {
                    'product_id': product_id
                },
                dataType: 'json',
                // processData: false
            }).done(function (data) {
                sdcApp.showLoading(false);
                $("#more_info_container").html(data.moreInfo);
                for (name in CKEDITOR.instances) {
                    CKEDITOR.instances[name].destroy(true);
                }
                CKEDITOR.replaceAll('editor');
                $('#save_other_info').click(function (e) {
                    e.preventDefault();
                    me.saveMoreInfo();
                });
            }).fail(function (xhr) {
                sdcApp.showLoading(false);
                if (xhr.status === 422) {
                    sdcApp.showToast({status: 'error', msg: 'Việc lưu thông tin bị lỗi. Vui lòng thử lại!'});
                }
            });
        },
        getViewCertificate: function () {
            if ($("#certificate_info_container").children().length != 0)
                return;
            sdcApp.showLoading(true);
            $.ajax({
                url: 'business/' + organizationId + '/product/get-view-certificate',
                method: 'GET',
                data: {
                    'product_id': product_id
                },
                dataType: 'json'
            }).done(function (data) {
                sdcApp.showLoading(false);
                $("#certificate_info_container").html(data.viewCertificate);
                Cert('business/' + organizationId + '/' + product_id + '/certificates');
                $('#save_certificate').click(function (e) {
                    e.preventDefault();
                    me.saveMoreInfo();
                });
            }).fail(function (xhr) {
                sdcApp.showLoading(false);
                if (xhr.status === 422) {
                    sdcApp.showToast({status: 'error', msg: 'Việc lưu thông tin bị lỗi. Vui lòng thử lại!'});
                }
            });
        },
        init_index: function () {
            CommonCRUD2(
                {
                    baseUrl: baseUrl,
                    tableId: '#datatable',
                    tableConf: tableConf,
                    searchFormId: '#search-form',
                    searchFieldNames: ['keyword'],
                    btnAddId: '#btn-add',
                    titleCreate: 'Thêm mới sản phẩm',
                    titleUpdate: 'Cập nhật sản phẩm',
                    modalSize: 'large',
                    bindDefaultEditAction: false,
                }
            );
        },
        init_create: function (_me, _organizationId) {
            me = _me;
            organizationId = _organizationId;
            product_id = _product_id;

            $('#save_primary_info').click(function (e) {
                e.preventDefault();
                me.savePrimaryInfo();
            });

            $("#image_upload_input").on("change", function (event) {
                event.preventDefault();
                me.upload(me);
            });
            $(".bs-wizard-dot").click(function (e) {
                if (!$(this).parent().hasClass("disabled"))
                    me.switch_step($(this).attr('step'));
            });
            $("#status").on("change", function (e) {
                if (this.value != 2)
                    $("#duration").attr("disabled", "disabled");
                else
                    $("#duration").removeAttr("disabled");
            });

            $('#price_sale,#price,#dientichsudung').inputmask("numeric", {
                radixPoint: ".",
                groupSeparator: ",",
                digits: 2,
                min:0,
                autoGroup: true,
                // prefix: 'VNĐ: ', //No Space, this will truncate the first character
                rightAlign: false,
                autoUnmask: true,
                greedy: false,
                removeMaskOnSubmit: true
            });
            // $('#certificate_category_id').select2({
            //     width: '100%',
            //     placeholder: "Chọn loại chứng nhận, xác nhận..."
            // });
            $("#step" + 1 + " .bs-wizard-info").css('color', '#d91c5f');
            $("#product_links").click(function () {
                $("#box-add").empty();
                $("#more-detail").val('');
                var box = $('#box-add-create').children(".product-add");
                if ($(box).length > 0) {
                    for (var i = 0; i < $(box).length; i++) {
                        if ($(box[i]).attr('id') != '0')
                            $("#box-add").append($(box[i]).clone());
                        else
                            $("#more-detail").val($(box[i]).children('span').text());
                    }
                    ;
                }
                $("#modal-product-links").modal("show");
            });
            $("#btnSaveProductLinks").click(function () {
                var isAppend = false;
                if ($('#box-add').children(".product-add").length > 0) {
                    $("#box-add-create").html($('#box-add').children());
                    isAppend = true;
                }
                if ($("#more-detail").val() != "") {
                    if (isAppend)
                        $("#box-add-create").append("<div id='0' class='product-add'><span class='col-lg-12'>" + $("#more-detail").val() + "<i class='fa fa-close icon-close'></i></span></div>");
                    else
                        $("#box-add-create").html("<div id='0' class='product-add'><span  class='col-lg-12'>" + $("#more-detail").val() + "<i class='fa fa-close icon-close'></i></span></div>");
                }

                $(".product-add i").click(function (e) {
                    e.preventDefault();
                    $(this).parent().parent().remove();
                })

                $("#modal-product-links").modal("hide");
            });
            $(".product-add i").click(function (e) {
                e.preventDefault();
                $(this).parent().parent().remove();
            });
             $(".grid_image i").click(function (e) {
                e.preventDefault();
                $(this).parent().remove();
            });
        },
        init_product_link: function () {
            CommonCRUD2(
                {
                    baseUrl: baseUrl,
                    tableId: '#datatable-product-link',
                    tableConf: tableConfProductLink,
                    searchFormId: '#search-form',
                    searchFieldNames: ['keyword', 'product_category_id_modal'],
                    bindDefaultEditAction: false,
                    formShowedHandler: function (form) {
                    }
                }
            );
        }
    }


};

allowDrop = function (ev) {
    ev.preventDefault();
}

drag = function (ev) {
    ev.dataTransfer.setData("text", ev.target.id);
}

drop = function (ev) {
    ev.preventDefault();
    var data = ev.dataTransfer.getData("text");
    var nodeCopy = document.getElementById(data).cloneNode(true);
    var src = nodeCopy.getAttributeNode('src').value;
    var path = nodeCopy.getAttributeNode('path').value;
    // nodeCopy.id = "newId";
    //ev.target.appendChild(nodeCopy);
    $('.image_avatar>img').attr({'src': src, 'path': path});
}

function addProduct(t) {
    var table = $('#datatable-product-link').DataTable();
    var data_row = table.row($(t).closest('tr')).data();

    if ($('#box-add #' + data_row.id).length == 0) {
        console.log($('#box-add #' + data_row.id).length);
        $('#box-add').append("<div class='product-add' id='" + data_row.id + "'><span  class='col-lg-12'>" + data_row.name + " - " + data_row.cssx + "<i class='fa fa-close icon-close'></i></span></div>");
    }


    $(".product-add i").click(function (e) {
        e.preventDefault();
        $(this).parent().parent().remove();
    })
}

function uploadImage(files) {
    var valid_extensions = /(\.jpg|\.jpeg|\.gif|\.png)$/i;
    if (files.length > 0 && valid_extensions.test(files[0].name.toLowerCase())) {
        var formData = new FormData();
        formData.append('file', files[0]); 
        sdcApp.showLoading(true);
        $.ajax({
            url: 'upload/image',
            method: 'POST',
            data: formData,
            contentType: false,
            processData: false,
            dataType: 'json',
        }).done(function (rs) {
            if (rs.success) {
                $('#check_temp_image').val(rs.data.path);
            }
            sdcApp.showLoading(false);
        }).fail(function () {
            sdcApp.showLoading(false);
        });
        $(".upload-img-wrap").html('');
        $(".upload-img-wrap").html("<img width='128' height='128' src='" + (window.URL || window.webkitURL).createObjectURL(files[0]) + "'>");
        $('.upload-img-border').append('<a href="javascript:void(0)" class="upload-img-clear-btn" onclick="removeImage()">Remove</a>');
    } else {
        alert('Không đúng định dạng ảnh');
        $(".upload-img-wrap").html('');
    }
    //var formData = sdcApp.getFormDataAndType($("#upload_form"));
   
}
function removeImage() {
    $(".upload-img-wrap").html('');
    $('.upload-img-clear-btn').hide(true);
    $('#temp_image').val('');
    $('#check_temp_image').val('');
    // toastr.success("Xóa thành công !");
   
}
