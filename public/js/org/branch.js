branch = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name'},
            {"data": 'address',
                "render": function (value, type, data) {
                    if(value==null)
                        return data.url;
                    else
                        return value+' , '+data.url;     
                }
            },
            {"data": 'tel'},
            {"data": 'email'},
            {
                data: 'created_at',
                class: 'text-center',
                render: sdcApp.renderDataTableDate('dft') // 23:12:56 23/8/2018
            }, // TODO date format
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [6, 'desc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'organizationId'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm chi nhánh ',
            titleUpdate: 'Cập nhật chi nhánh',
            modalSize: 'large',
            nameColIndex: 0,
            formShowedHandler: function (form) {
                    $('.js-example-basic-single').select2({
                        width:'100%',
                        dropdownParent: form,
                        templateResult: function (data) {
                            // We only really care if there is an element to pull classes from
                            if (!data.element) {
                                return data.text;
                            }

                            var $element = $(data.element);
                            var level = $element.attr('level');
                            var pad = level * 20;
                            var icon_class = "icon_class";
                            if(level==0)
                                icon_class="icon_class_0";
                            if(level==1)
                                icon_class="icon_class_1";
                            if(level==2)
                                icon_class="icon_class_2";
                            if(level==3)
                                icon_class="icon_class_3";

                            var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right '+icon_class+' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                            var $wrapper = $(wraptext);
                            return $wrapper;
                        },
                        templateSelection: function (state) {
                            if (!state.id) {
                                return state.text;
                            }
                            return $(state.element).attr('path');
                        }
                    });
                },
        };
        return CommonCRUD2(conf);
        /*CommonCRUD(
         baseUrl,
         '#datatable',
         tableConf,
         '#search-form',
         ['keyword', 'active'],
         '#btn-add',
         'Thêm vùng',
         'Cập nhật vùng',
         'large'
         );*/
    });
};
