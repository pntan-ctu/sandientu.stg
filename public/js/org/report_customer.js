ReportCustomer = function (baseUrl, OrgId) {
    var tableConf = {
        "columns": [
            {"data": 'stt', "orderable": false, "class": "text-center"},
            {"data": 'customer_name'},
            {"data": 'customer_address', "class": "text-center"},
            {"data": 'customer_tel', "class": "text-center"},
            {"data": 'count_order',"class": "text-center"},
            {"data": 'total_money', "class": "text-right",render: sdcApp.renderDataTableNumber(0, ' đ')}
            
        ],
        "order": [5, 'desc']
    };
    return {
            init_index: function () {
                $('.datepicker').inputmask('date');
                $('.datepicker').datepicker({
                    language: 'vi',
                    autoclose: true
                });
                $('#to_date').datepicker('setDate', new Date());
                var date = new Date();
                date.setMonth(date.getMonth(), 1);
                $('#from_date').datepicker('setDate', date);

                $("#btnExport").click(function () {
                    sdcApp.blockUI();
                    var from_date = $('#from_date').val();
                    var to_date = $('#to_date').val();
                    var urlReport = 'business/' + OrgId + '/report-customer/exportExcel?from_date='+from_date+'&to_date='+to_date;
                    window.location.href = urlReport;
                    sdcApp.unblockUI();
                });

                CommonCRUD2({
                    baseUrl: baseUrl,
                    tableId: '#datatable',
                    tableConf: tableConf,
                    searchFormId: '#search-form',
                    searchFieldNames: ['from_date', 'to_date'],
                    modalSize: 'large',
                    bindDefaultEditAction: false,
                });
            }
    }
};
