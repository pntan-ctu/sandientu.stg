News = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'stt', "orderable": false, "class": "text-center"},
            {"data": 'title'},
            {"data": 'url', "orderable": false},
            {"data": 'nguoi_dang'},
            {"data": 'ngay_dang', "class": "text-center"},
            {
                "data": 'status',
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Xuất bản';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Soạn thảo';
                }
            },{
                "data": 'status',
                "class": "text-center",
                "orderable": false,
                "render": function (value, type, data) {
                    return '<img class="btn btn-link btn-xs relateNews" title="Chọn tin liên quan" style="cursor: pointer" src="css/images/grid.png" onclick="showFormRelateNews(\''+data.title+'\', '+data.id+', '+data.group_id+', \''+data.relate_news+'\')">';
                }
            },{
                "data": 'status',
                "orderable": false,
                "render": function (value, type, data) {
                    if(value == 0 && data.group_id > 1)
                        return '<a title="Sửa" class="btn btn-link btn-xs" href="manage/news/edit' + "/" + data.id + '"><i class="glyphicon glyphicon-pencil"></i></a>' +
                            '<a title="Xóa" class="btn btn-link btn-xs btn-del" href="#" id="'+data.id+'"><i class="glyphicon glyphicon-trash"></i></a>';
                    else
                        return '<a title="Sửa" class="btn btn-link btn-xs" href="manage/news/edit' + "/" + data.id + '"><i class="glyphicon glyphicon-pencil"></i></a>';
                }
            },
            {"data": 'id', "visible": false},
            {"data": 'group_id', "visible": false},
            {"data": 'relate_news', "visible": false},
            {"data": 'created_at', "visible": false}
        ],
        "order": [11, 'desc']
        /*"order": [[0, 'asc'],[11, 'desc']],
        "columnDefs": [{
            "targets": [0, 2],
            "orderable": false
        }]*/
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'active', 'sGroupId'],
            //btnAddId: '#btn-add',
            titleCreate: 'Thêm tin bài',
            titleUpdate: 'Cập nhật tin bài',
            modalSize: 'large',
            nameColIndex: 1
        };
        return CommonCRUD2(conf);
    });
};