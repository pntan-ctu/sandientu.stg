PhanQuyen = function (url) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {
                "data": 'checked',
                "render": function (value, type, data) {
                    var sChk = value > 0 ? "checked" : "";
                    return '<input '+sChk+' type="checkbox" name="module[]" value="'+data.id+'" onchange="chkModuleChange()" />';
                }
            },
            {"data": 'name', "orderable": false,},
            {"data": 'group_name', "visible": false},
            {"data": 'id', "visible": false}
        ],
        "order": [2, 'asc'],
        "pageLength": 500,
        "columnDefs": [{
            "targets": [0, 3],
            "orderable": false
        }],
        "drawCallback": function ( settings ) {
            var api = this.api();
            var rows = api.rows( {page:'current'} ).nodes();
            var last=null;
            var groupColumn = 2;
            api.column(groupColumn, {page:'current'} ).data().each( function ( group, i ) {
                if ( last !== group ) {
                    $(rows).eq( i ).before(
                        '<tr class="group"><td colspan="5">'+group+'</td></tr>'
                    );

                    last = group;
                }
            } );

            chkModuleChange();
        }
    };

    $(function () {
        CommonCRUD(
            url,
            '#dtModule',
            tableConf,
            '#frmPhanQuyen_search',
            ['organization_type_id', 'role_id'],
            'large'
        );
    });
};