DocumentType = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name'},
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            }
        ]
    };
      var conf = {
        baseUrl: baseUrl,
        tableId: '#datatable',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['keyword', 'active'],
        btnAddId: '#btn-add',
        titleCreate: 'Thêm mới',
        titleUpdate: 'Cập nhật',
        modalSize: 'large'
    };
    
     $(function () {
        CommonCRUD2(conf);
    });
};
