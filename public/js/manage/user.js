User = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name', "width": "20%"},
            {"data": 'email', "width": "20%"},
            {"data": 'org_name', "width": "20%"},
            {"data": 'password_reset', "orderable": false, "width": "12%"},
            {
                "data": 'active',
                "class": "text-center",
                "render": function (value, type, data) {
                    return value == 1 ? '<a><i class="glyphicon glyphicon-ok-circle"></i></a>' : '';
                },
                "width": "10%"
            },
            {
                "data": 'id',
                "class": "text-center",
                "render": function (value, type, data) {
                    return data.is_me == 0 ? '<button title="Reset mật khẩu" class="btn btn-link btn-xs" onclick="resetPassword(' + data.id + ', \'' + data.name + '\')"><i class="glyphicon glyphicon-retweet"></i></button>' : '';
                },
                "orderable": false,
                "width": "12%"
            },
            {
                "data": 'id',
                "orderable": false,
                "render": function (value, type, data) {
                    return data.is_me == 0 ? sdcApp.commonAjaxTblActionCell : ''; //sdcApp.commonAjaxTblEdit
                },
                "width": "6%"
            },
            {"data": 'id', "visible": false}
        ],
        "order": [7, 'desc']
    };
    var userConf = {
        baseUrl: baseUrl,
        tableId: '#datatable',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['keyword', 'active_search', 'member_type'],
        btnAddId: '#btn-add',
        titleCreate: 'Thêm người dùng',
        titleUpdate: 'Cập nhật người dùng',
        modalSize: 'large',
        formShowedHandler: function ($form) {
         
            $form.find('#email').inputmask('email');
            $form.find('#tel').inputmask('tel');
            $form.find('#date_of_birth').inputmask('date');
            $form.find('#date_of_birth').datepicker({
                language: 'vi',
                autoclose: true
            });
            $('.js-example-basic-single').select2({
            width: '100%',
            dropdownParent: $form,
            templateResult: function (data) {
                // We only really care if there is an element to pull classes from
                if (!data.element) {
                    return data.text;
                }

                var $element = $(data.element);
                var level = $element.attr('level');
                var pad = level * 20;
                var icon_class = "icon_class";
                if (level == 0)
                    icon_class = "icon_class_0";
                if (level == 1)
                    icon_class = "icon_class_1";
                if (level == 2)
                    icon_class = "icon_class_2";
                if (level == 3)
                    icon_class = "icon_class_3";

                var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right ' + icon_class + ' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                var $wrapper = $(wraptext);
                return $wrapper;
            },
            templateSelection: function (state) {
                if (!state.id) {
                    return state.text;
                }
                return $(state.element).attr('path');
            }
        });

        }
    };
    $(function () {
        CommonCRUD2(userConf);
    });
};
