Organization = function (baseUrl) {
    var tableConf = {
        responsive: true,
        "rowId": 'id',
        "columns": [
            {
                data: 'name',
                name: 'name',
                "width": "28%",
                render: function (value, type, data) {
                    var rs = data.name;
                    var preStr = "";
                    if(data.forward == 1){
                        preStr = '<button title="Hủy chuyển" class="btn btn-link btn-xs" onclick="cancelMove(' + data.id + ')"><i class="fa fa-times"></i></button>';
                    }
                    if(data.toward == 1){
                        preStr = '<button title="Xử lý tiếp nhận" class="btn btn-link btn-xs" onclick="showFormResolveMove(' + data.id + ')"><i class="fa fa-flickr"></i></button>';
                    }
                    return preStr + rs;
                }
            },
            {data: 'address', name: 'address', "orderable": false, "width": "30%"},
            {data: 'gov_name', name: 'gov_name', "orderable": false, "width": "18%"},
            {
                data: 'status', "width": "12%",
                name: 'status',
                "class": 'text-center',
                render: function (data) {
                    if (data == 0)
                        return "Chưa duyệt";
                    else if (data == 1)
                        return "Đang hoạt động";
                    else if (data == 2)
                        return "Ngừng hoạt động";
                    else if (data == 3)
                        return "Khóa do vi phạm";
                    else
                        return "Loại khác";
                }
            },
            {
                "data": 'id', "width": "12%",
                "orderable": false,
                "class": 'text-center',
                "render": function (id, type, data) {
                    var btnFrmMove = data.toward == 0 ?
                        '<button title="Chuyển cơ quan quản lý" class="btn btn-link btn-xs btn-show" onclick="showFormMove(' + id + ')"><i class="fa fa-share-square-o"></i></button>'
                        : '';
                    return '<a href="' + 'business/' + id + '" target="_blank" class="btn btn-link btn-xs btn-setting" data-toggle="tooltip" title="Quản lý"><i class="fa fa-th-large" style="font-size: 18px;"></i></a>' +
                        '<a href="' + baseUrl + '/' + id + '/edit"' + ' class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="glyphicon glyphicon-pencil"></i></a><a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>' +
                        btnFrmMove;
                }
            }
        ],
        "order": [0, 'asc']
    };

    return {
        init_index: function () {
            CommonCRUD2(
                {
                    baseUrl: baseUrl,
                    tableId: '#datatable',
                    tableConf: tableConf,
                    searchFormId: '#search-form',
                    searchFieldNames: ['keyword','status'],
                    btnAddId: '#btn-add',
                    titleCreate: 'Thêm mới cơ sở sản xuất / kinh doanh',
                    titleUpdate: 'Cập nhật cơ sở sản xuất / kinh doanh',
                    modalSize: 'large',
                    bindDefaultEditAction: false,
                }
            );
        },
        init_create: function (type) {
            $(".tel").inputmask({mask: "0|+9{10}", greedy: false, placeholder:"", clearMaskOnLostFocus: true, });
            $(".email").inputmask({
                mask: "*{1,20}[.*{1,20}][.*{1,20}][.*{1,20}]@*{1,20}[.*{2,6}][.*{1,2}]",
                greedy: false,
                onBeforePaste: function (pastedValue, opts) {
                    pastedValue = pastedValue.toLowerCase();
                    return pastedValue.replace("mailto:", "");
                },
                definitions: {
                    '*': {
                        validator: "[0-9A-Za-z!#$%&'*+/=?^_`{|}~\-]",
                        casing: "lower"
                    }
                }
            });
            $(".website").inputmask({
                mask: "http://*{1,100}", greedy: false,
                definitions: {
                    '*': {
                        validator: "[0-9A-Za-z!.#$%&'*+/=?^_`{|}~\-]"
                    }
                }
            });
            $('.datepicker').datepicker({
                language: 'vi',
                autoclose: true,
                format: 'dd/mm/yyyy'
            });

            $("#image_upload_input").on("change", function () {
                var files = $(this)[0].files;
                $('.image_avatar>img').attr('src', URL.createObjectURL(files[0]));
            });

            if ($('#area_id')) {
                $('#area_id').select2({
                    width: '100%',
                    placeholder: "--- Chọn vùng sản xuất ---"
                });
            }

            if ($('#commercial_center_id')) {
                $('#commercial_center_id').select2({
                    width: '100%',
                    placeholder: "--- Chọn địa điểm kinh doanh ---"
                });
            }

            /*$('.organization_type_id').select2({
                width: '100%',
                placeholder: "Hãy chọn loại tổ chức"
            });*/

        }
    }

};