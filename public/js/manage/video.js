Video = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'img_path',"orderable": false,
                "render": function (value) {
                    return '<img src=" '+"image"+"/"+"150"+"/"+"150"+"/"+value+' ">';
                }  },
            {"data": 'group_name'},
            {"data": 'title'},
            {"data": 'path',"orderable": false},
            {"data": 'description',"orderable": false},
            {"data": 'width',"orderable": false},
            {"data": 'height',"orderable": false},
            {"data": 'youtube_url',"orderable": false,
                "render":function(value){
                    return '<a target="_blank" href="'+value+'">'+value+'</a>';
                }
            },
            {"data": 'user_name',"orderable": false},
            {"data": 'active', "class": "text-center",
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [5, 'desc']
    };
   $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'active', 'organization_id'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm video',
            titleUpdate: 'Cập nhật video',
            modalSize: 'large',
            nameColIndex: 0,

            createRequestData: function () {
                var organization_id = jQuery("#organization_id").val();
                return {organization_id: organization_id}
            },
        };
        return CommonCRUD2(conf);
        /*CommonCRUD(
         baseUrl,
         '#datatable',
         tableConf,
         '#search-form',
         ['keyword', 'active'],
         '#btn-add',
         'Thêm vùng',
         'Cập nhật vùng',
         'large'
         );*/
    });
};
