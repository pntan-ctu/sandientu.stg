var product_category_id = 0;

function formatAction(val, row) {
    return '<a onclick="actionEdit(' + val + ')" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i\n' +
        '                            class="glyphicon glyphicon-pencil"></i></a>'
        + '<a onclick="actionDelete(' + val + ')" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i\n' +
        '                            class="glyphicon glyphicon-trash"></i></a>'
}

$("#btn-add").click(function () {
    actionEdit(0);
});

//load modal edit or add
function actionEdit(val) {
    product_category_id = val;
    sdcApp.showLoading(true);
    $.ajax({
        url: val == '' ? 'manage/product-category/create' : 'manage/product-category/' + val + '/edit',
        method: 'GET',
        dataType: 'json'
    }).done(function (data) {
        sdcApp.showLoading(false);
        $("#modal-product-category .modal-body").html(data.view);
        $("#modal-product-category").modal('show');
        formShowedHandler();
        sdcApp.showLoading(false);
    }).fail(function (xhr) {
        sdcApp.showLoading(false);
    });
}

//save
$("#btnSave").click(function () {
    $('#treeGrid').datagrid('loaded');
    sdcApp.showLoading(true);
    var form = $(".form-horizontal");
    var formData = sdcApp.getFormDataAndType(form);
    $.ajax({
        "url": 'manage/product-category' + (product_category_id == 0 ? '' : '/' + product_category_id),
        "method": 'POST',
        "data": formData.data,
        "dataType": 'json',
        "contentType": formData.contentType,
        "processData": formData.contentType !== false
    }).done(function (data) {
        $("#modal-product-category").modal('hide');
        $('#treeGrid').treegrid('reload').datagrid('loaded');
        sdcApp.showToast({status: 'success', msg: 'Lưu danh mục sản phẩm thành công'});
        sdcApp.showLoading(false);
    }).fail(function (xhr) {
        sdcApp.showLoading(false);
        if (xhr.status === 422) {
            sdcApp.resetFormErrors(form);
            var errors = xhr.responseJSON.errors;
            sdcApp.renderFormErrors(errors, form);
        }
    });
});

function actionDelete(val) {
    product_category_id = val;
    $("#modal-delete").modal('show');
}

function ajax_delete() {
    $('#treeGrid').datagrid('loaded');
    $("#modal-delete").modal('hide');
    sdcApp.showLoading(true);
    $.ajax({
        url: 'manage/product-category/' + product_category_id,
        method: 'DELETE',
        dataType: 'json',
        processData: false
    }).done(function (data) {
        $('#treeGrid').treegrid('reload').datagrid('loaded');
        sdcApp.showToast({status: 'success', msg: 'Xóa danh mục sản phẩm thành công'});
        sdcApp.showLoading(false);
    }).fail(function (xhr) {
        sdcApp.showLoading(false);
        if (xhr.status === 422) {
            sdcApp.showToast({status: 'error', msg: 'Việc Xoá thông tin bị lỗi. Vui lòng thử lại!'});
        }
    });
}

function formShowedHandler() {
    var form = $("#modal-product-category").find('form');
    $('.js-example-basic-single').select2({
        width: '100%',
        dropdownParent: form,
        templateResult: function (data) {
            // We only really care if there is an element to pull classes from
            if (!data.element) {
                return data.text;
            }

            var $element = $(data.element);
            var level = $element.attr('level');
            var pad = level * 20;
            var icon_class = "icon_class";
            if (level == 0)
                icon_class = "icon_class_0";
            if (level == 1)
                icon_class = "icon_class_1";
            if (level == 2)
                icon_class = "icon_class_2";
            if (level == 3)
                icon_class = "icon_class_3";

            var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right ' + icon_class + ' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

            var $wrapper = $(wraptext);
            return $wrapper;
        },
        templateSelection: function (state) {
            if (!state.id) {
                return state.text;
            }
            return $(state.element).attr('path');
        }
    });
}

$("#btn-search").click(function () {
    $('#treeGrid').treegrid('reload', {
        keywork: $("#keyword").val()
    }).datagrid('loaded');
});

