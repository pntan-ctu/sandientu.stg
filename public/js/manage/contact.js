Contact = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'title'},
            {"data": 'org_name',"orderable": false},
            {"data": 'created_at'}, // TODO date format
            {"data": 'status',"class": 'text-center',"orderable": false,
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã xử lý';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa xử lý';
                }
            },
            {"data": 'status',"class": 'text-center',"orderable": false,
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<a title="Bỏ xử lý phản hồi" style="color:red;" href="manage/contact/convert' + "/" + data.id + '"><i style="font-size:20px" class="fa fa-times"></i></a>';
                    else
                        return '<a title="Xử lý phản hồi" href="manage/contact/convert' + "/" + data.id + '"><i style="font-size:20px" class="fa fa-check"></i></a>';
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function (value, type, data) {
                    return '<a title="Chuyển tiếp" class="btn btn-link btn-xs btn-edit" href="' + baseUrl + '/' + value + '/edit"><i class="fa  fa-share-square-o"></i></a>' +
                            '<a title="Xem chi tiết" class="btn btn-link btn-xs" href="' + baseUrl + '/show/' + value + '"><i class="glyphicon glyphicon-list"></i></a>' +
                            '<a title="Xóa" class="btn btn-link btn-xs btn-del" href="#"><i class="glyphicon glyphicon-trash"></i></a>';
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [2, 'desc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'active'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm chi nhánh ',
            titleUpdate: 'Chuyển tiếp phản hồi',
            modalSize: 'large',
            nameColIndex: 0,
            formShowedHandler: function (form) {
                    $('.js-example-basic-single').select2({
                        width:'100%',
                        dropdownParent: form,
                        templateResult: function (data) {
                            // We only really care if there is an element to pull classes from
                            if (!data.element) {
                                return data.text;
                            }

                            var $element = $(data.element);
                            var level = $element.attr('level');
                            var pad = level * 20;
                            var icon_class = "icon_class";
                            if(level==0)
                                icon_class="icon_class_0";
                            if(level==1)
                                icon_class="icon_class_1";
                            if(level==2)
                                icon_class="icon_class_2";
                            if(level==3)
                                icon_class="icon_class_3";

                            var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right ' + icon_class + ' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                            var $wrapper = $(wraptext);
                            return $wrapper;
                        },
                        templateSelection: function (state) {
                            if (!state.id) {
                                return state.text;
                            }
                            return $(state.element).attr('path');
                        }
                    });
                },
        };
        return CommonCRUD2(conf);
        /*CommonCRUD(
         baseUrl,
         '#datatable',
         tableConf,
         '#search-form',
         ['keyword', 'active'],
         '#btn-add',
         'Thêm vùng',
         'Cập nhật vùng',
         'large'
         );*/
    });
};
