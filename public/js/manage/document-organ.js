DocumentOrgan = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name'},
            {"data": 'no'},
            {"data": 'active', class: "text-center",
                "render": function (value, type, data) {
                    if (value == 1)
                    {
                        return 'Kích hoạt';
                    } else
                    {
                        return 'Chưa kích hoạt';
                    }
                },
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            }
        ]
    };
     var conf = {
        baseUrl: baseUrl,
        tableId: '#datatable',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['keyword', 'active'],
        btnAddId: '#btn-add',
        titleCreate: 'Thêm mới',
        titleUpdate: 'Cập nhật',
        modalSize: 'large'
    };
    
     $(function () {
        CommonCRUD2(conf);
    });
};
