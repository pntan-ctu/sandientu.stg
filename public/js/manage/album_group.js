AlbumGroup = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name', "width": "20%"},
            {"data": 'description',"orderable": false, "width": "20%"},
            {"data": 'user_name',"orderable": false, "width": "20%"},
            {"data": 'no', "width": "20%"},
            {"data": 'active',
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                },
                "width": "10%"
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                },
                "width": "10%"
            },
            {"data": 'id', "visible": false}
        ],
        "order": [6, 'desc']
    };
    $(function () {
        CommonCRUD(
            baseUrl,
            '#datatable',
            tableConf,
            '#search-form',
            ['keyword', 'active'],
            '#btn-add',
            'Thêm nhóm Album',
            'Cập nhật nhóm Album',
            'large');
    });
};
