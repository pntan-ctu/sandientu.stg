Cert_cate = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'icon',"orderable": false,
                "render": function (value) {
                                   return '<img src=" '+"image"+"/"+"70"+"/"+"70"+"/"+value+' ">';
                }  },
            {"data": 'name'},
            {"data": 'rank',"class": 'text-center'},
            {
                data: 'created_at',
                class: 'text-center',
                render: sdcApp.renderDataTableDate('dft') // 23:12:56 23/8/2018
            }, // TODO date format
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [2, 'desc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'type'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm loại chứng nhận, xác nhận',
            titleUpdate: 'Cập nhật loại chứng nhận, xác nhận',
            modalSize: 'large',
            nameColIndex: 0,
            // Longdv bo ckeditor về text area
            // beforeSerialize: function($form){
            //     for (instance in CKEDITOR.instances) {
            //         CKEDITOR.instances[instance].updateElement();
            //     }
            // },
            // formShowedHandler: function($form){
            //     $('.bootbox').on('shown.bs.modal', function () {
            //         $('#my-editor').trigger('focus')
            //     })
            //     CKEDITOR.replace( 'my-editor');
            // },

            //requestSaveDoneHandler: function(data){console.log(data)},
            //requestSaveFailHandler: function(xhr){console.log(xhr)},
            //requestDeleteDoneHandler: function(data){console.log(data)},
            //requestDeleteFailHandler: function(xhr){console.log(xhr)},
            //storeHandler: function(){console.log('storeHandler'); return false},
            //updateHandler: function(){console.log('updateHandler'); return false},
            // buttons: {
            //     cancel: {
            //         label: "Hủy"
            //     },
            //     ok: {
            //         label: "Lưu",
            //         callback: function () {
            //             okHandler(editItemId);
            //             return false;
            //         }
            //     }
            // },
            // bindRowActionHandlers: function () {
            //     sdcApp.bindTblDelEvent(conf.tableId, oTable, conf.baseUrl, conf.nameColIndex);
            //     sdcApp.bindTblEditEvent(conf.tableId, oTable, function (id) {
            //         var editUrl = conf.baseUrl + '/' + id + '/edit';
            //         editItemId = id;
            //         okHandler = update;
            //         showForm(conf.titleUpdate, editUrl);
            //     });
            // }
            // bindDefaultEditAction: true,
            // bindDefaultDeleteAction: true,
        };
        return CommonCRUD2(conf);
        /*CommonCRUD(
         baseUrl,
         '#datatable',
         tableConf,
         '#search-form',
         ['keyword', 'active'],
         '#btn-add',
         'Thêm vùng',
         'Cập nhật vùng',
         'large'
         );*/
    });
};
