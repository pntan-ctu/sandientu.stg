Help = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'question',"orderable": false},
            {"data": 'name',"orderable": false},
            {"data": 'email',"orderable": false}, // TODO date format
            {"data": 'phone',"orderable": false},
            {"data":'address',"orderable": false},
            {"data": 'status',
                "render": function(value, type, data){
                    if(value==0)
                    {
                        return '<a href="manage/help'+"/"+data.id+'">Phê duyệt</a>';
                    }
                    else
                    {
                        return '<a href="manage/help'+"/"+data.id+'">Tắt phê duyệt</a>';
                    }
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function (value, type, data) {
                    return '<a href="manage/help/'+data.id+'/edit" class="btn btn-link btn-xs btn-edit" data-toggle="tooltip" title="Sửa"><i class="fa fa-list"></i></a>\n\
                        <a href="#" class="btn btn-link btn-xs btn-del" data-toggle="tooltip" title="Xóa"><i class="glyphicon glyphicon-trash"></i></a>';
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [5, 'asc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'active'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm Trợ giúp',
            titleUpdate: 'Cập nhật Trợ giúp',
            modalSize: 'large',
            nameColIndex: 0,
            beforeSerialize: function($form){
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            },
            //requestSaveDoneHandler: function(data){console.log(data)},
            //requestSaveFailHandler: function(xhr){console.log(xhr)},
            //requestDeleteDoneHandler: function(data){console.log(data)},
            //requestDeleteFailHandler: function(xhr){console.log(xhr)},
            formShowedHandler: function(form){
                if( $("#common_status").prop("checked") == false){
                    $("#common").val(0);
                    $("#div_common").hide();
                }
                $('.bootbox').on('shown.bs.modal', function () {
                    $('#my-editor').trigger('focus')
                })
                CKEDITOR.replace( 'my-editor');
                $('.js-example-basic-single').select2({
                    dropdownParent: form,
                    templateResult: function (data) {
                        // We only really care if there is an element to pull classes from
                        if (!data.element) {
                            return data.text;
                        }

                        var $element = $(data.element);
                        var pad = $element.attr('level') * 20;
                        var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right"></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                        var $wrapper = $(wraptext);
                        return $wrapper;
                    },
                    templateSelection: function (state) {
                        if (!state.id) {
                            return state.text;
                        }
                        return $(state.element).attr('path');
                    }
                });
            },
        };
        return CommonCRUD2(conf);
    });
};
