RelateNews = function (url) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {
                "data": 'checked',
                "render": function (value, type, data) {
                    var sChk = value == 1 ? "checked" : "";
                    return '<input '+sChk+' type="checkbox" name="relate-news[]" value="'+data.id+'" />';
                }
            },
            {"data": 'name'},
            {"data": 'title'},
            {"data": 'id', "visible": false}
        ],
        "order": [2, 'desc'],
        "columnDefs": [{
            "targets": [0, 3],
            "orderable": false
        }]
    };

    $(function () {
        CommonCRUD(
            url,
            '#datatable2',
            tableConf,
            '#frmRelateNews',
            ['keyword', 'newsId', 'sGroupRelateId', 'sRelateId'],
            'large'
        );
    });
};