SlideShow = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'image_path',"orderable": false
                ,
                "render": function (value) {
                    return '<img src=" '+"image/200/200/" +value+' ">';
                }  
            },
            {"data": 'title'},
            {"data": 'link',"orderable": false},
            {"data": 'user_name',"orderable": false}, // TODO date format
            {"data": 'no', "class": "text-center"},
            {"data":'active', 
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [5, 'desc']
    };
    $(function () {
        CommonCRUD(
            baseUrl,
            '#datatable',
            tableConf,
            '#search-form',
            ['keyword', 'active'],
            '#btn-add',
            'Thêm SlideShow',
            'Cập nhật SlideShow',
            'large');
    });
};
