Role = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name'},
            {"data": 'title'},
            {
                "data": 'id',
                "orderable": false,
                "class": "text-center",
                "render": function (value, type, data) {
                    return '<button title="Phân quyền" class="btn btn-link btn-xs" onclick="showFormPhanQuyen('+value+', '+data.organization_type_id+')"><i class="glyphicon glyphicon-cog"></i></button>';
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "render": function (value, type, data) {
                    return '<a title="Sửa" class="btn btn-link btn-xs btn-edit" href="' + baseUrl + '/' + value + '/edit"><i class="glyphicon glyphicon-edit"></i></a>' +
                        '<a title="Xóa" class="btn btn-link btn-xs btn-del" href="#"><i class="glyphicon glyphicon-trash"></i></a>';

                }
            },
            {"data": 'id', "visible": false},
            {"data": 'scope', "visible": false}
        ],
        "order": [0, 'asc']
    };
    var roleConf = {
        baseUrl: baseUrl,
        tableId: '#datatable',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['keyword', 'organization_type_id'],
        btnAddId: '#btn-add',
        titleCreate: 'Thêm nhóm người dùng',
        titleUpdate: 'Cập nhật nhóm người dùng',
        modalSize: 'medium'
    };
    $(function () {
        var frm = CommonCRUD2(roleConf);

        $("#organization_type_id").on("change", function (event) {
            frm.reloadTable();
        });
    });
};