QuestionAnswer = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'status',"orderable": false,
                 "render": function (value, type, data) {
                    return '<input  type="checkbox" name="module[]" value="'+data.id+'" " />';
                }
            },
            {
                "data": 'status',"class": 'text-center',
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã duyệt';
                    if(value == 0)
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chờ duyệt';
                    if(value == 2)
                        return '<img style="margin-top: -1.5px" src="css/images/red.png"> Không duyệt';
                }
            },
            {"data": 'content',"orderable": false},
            {"data": 'org_path',"orderable": false,
                "render": function (value, type, data) {
                    if (value !== undefined)
                        return '<a href="shop/' + data.org_path + '" target="_blank">'+data.org_name+'</a>';
                    else
                        return '<a href="san-pham/' + data.product_path + '" target="_blank">'+data.product_name + ' - ' + data.org_name+'</a>';
                }
            },
            {"data": 'user_name',"orderable": false},

            {"data": 'id', "visible": false}
        ],
        "order": [4, 'desc']
    };
   $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'active_search', 'type_search', 'organization_id'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm video',
            titleUpdate: 'Cập nhật video',
            modalSize: 'large',
            nameColIndex: 1,

            createRequestData: function () {
                var organization_id = jQuery("#organization_id").val();
                return {organization_id: organization_id}
            },
        };
        return CommonCRUD2(conf);
        /*CommonCRUD(
         baseUrl,
         '#datatable',
         tableConf,
         '#search-form',
         ['keyword', 'active'],
         '#btn-add',
         'Thêm vùng',
         'Cập nhật vùng',
         'large'
         );*/
    });
};
