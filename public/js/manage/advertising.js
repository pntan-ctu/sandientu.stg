Advertisings = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'loai_quang_cao', "width": "11%"},
            {"data": 'loai_san_pham', "width": "14%"},
            {"data": 'title', "width": "20%"},
            {"data": 'nguoi_dang', "width": "14%", "orderable": false},
            {"data": 'ngay_bd', "width": "10%"},
            {"data": 'ngay_kt', "width": "10%"},
            {
                "data": 'status',
                "class": "text-left",
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px;" src="css/images/blue.png"> <span style="color: blue">Đã duyệt</span>';
                    else if(value == 0)
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> <span">Chờ duyệt</span>';
                    else
                        return '<i class="glyphicon glyphicon-remove-circle"></i> <span style="color: red">Không duyệt</span>';
                },
                "width": "11%"
            },
            {
                "data": 'status',
                "orderable": false,
                "class": "text-right",
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<button title="Không duyệt" class="btn btn-link btn-xs" onclick="setInactive(' + data.id + ')"><i class="fa fa-times"></i></button>' +
                            '<a title="Sửa" class="btn btn-link btn-xs btn-edit" href="' + baseUrl + '/' + data.id + '/edit"><i class="glyphicon glyphicon-edit"></i></a>';
                            // + '<a title="Xóa" class="btn btn-link btn-xs btn-del" href="#" id="'+data.id+'"><i class="glyphicon glyphicon-trash"></i></a>';
                    else {
                        var btKhongDuyet = "";
                        if (value == 0) btKhongDuyet = '<button title="Không duyệt" class="btn btn-link btn-xs" onclick="setInactive(' + data.id + ')"><i class="fa fa-times"></i></button>';
                        return '<button title="Duyệt" class="btn btn-link btn-xs" onclick="setActive(' + data.id + ')"><i class="fa fa-check"></i></button>' +
                            btKhongDuyet +
                            '<a title="Sửa" class="btn btn-link btn-xs btn-edit" href="' + baseUrl + '/' + data.id + '/edit"><i class="glyphicon glyphicon-edit"></i></a>';
                            // + '<a title="Xóa" class="btn btn-link btn-xs btn-del" href="#" id="' + data.id + '"><i class="glyphicon glyphicon-trash"></i></a>';
                    }
                },
                "width": "10%"
            },
            {"data": 'created_at', "visible": false},
            {"data": 'id', "visible": false},
            {"data": 'content', "visible": false}
        ],
        "order": [8, 'desc']
    };

    var commentsConf = {
        baseUrl: baseUrl,
        tableId: '#dtAdvertisings',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['keyword', 'status'],
        nameColIndex: 2,
        //btnAddId: '#btn-add',
        //titleCreate: 'Thêm bình luận',
        titleUpdate: 'Sửa Tin kết nối cung cầu',
        modalSize: 'large',
        formShowedHandler: function (form) {
            $('.js-example-basic-single').select2({
                width:'100%',
                dropdownParent: form,
                templateResult: function (data) {
                    // We only really care if there is an element to pull classes from
                    if (!data.element) {
                        return data.text;
                    }

                    var $element = $(data.element);
                    var level = $element.attr('level');
                    var pad = level * 20;
                    var icon_class = "icon_class";
                    if(level==0)
                        icon_class="icon_class_0";
                    if(level==1)
                        icon_class="icon_class_1";
                    if(level==2)
                        icon_class="icon_class_2";
                    if(level==3)
                        icon_class="icon_class_3";

                    var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right '+icon_class+' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                    var $wrapper = $(wraptext);
                    return $wrapper;
                },
                templateSelection: function (state) {
                    if (!state.id) {
                        return state.text;
                    }
                    return $(state.element).attr('path');
                }
            });
            form.find('.datepicker').inputmask('date');
            form.find('.datepicker').datepicker({
                language: 'vi',
                autoclose: true
            });
        }
    };

    $(function () {
        CommonCRUD2(commentsConf);
    });
};