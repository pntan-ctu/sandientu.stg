VideoGroup = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name'},
            {"data": 'path',"orderable": false},
            {"data": 'description',"orderable": false},
            {"data": 'no', "class": "text-center"}, // TODO date format
            {"data": 'active', "class": "text-center",
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [5, 'desc']
    };
    $(function () {
        CommonCRUD(
            baseUrl,
            '#datatable',
            tableConf,
            '#search-form',
            ['keyword', 'active'],
            '#btn-add',
            'Thêm Nhóm Video',
            'Cập nhật nhóm video',
            'large');
    });
};