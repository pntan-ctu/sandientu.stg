Menu = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'id', "visible": false},
        ]
    };

    var commentsConf = {
        baseUrl: baseUrl,
        //tableId: '#dtNewsComments',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['organization_type_id'],
        btnAddId: '.btn-update',
        //titleCreate: 'Thêm menu',
        //titleUpdate: 'Sửa menu',
        modalSize: 'large',
        createRequestData: function(){
            return {
                menu_id: $("input[name='menu_id']").val(),
                org_type_id: $('#organization_type_id').val()
            };
        },
        requestSaveDoneHandler: function(){reloadTree();},
        formShowedHandler: function(form){
            $(".modal-title").html($("input[name='menu_id']").val() == 0 ? 'Thêm menu' : 'Sửa menu');

            $('.js-example-basic-single').select2({
                dropdownParent: form,
                templateResult: function (data) {
                    // We only really care if there is an element to pull classes from
                    if (!data.element) {
                        return data.text;
                    }

                    var $element = $(data.element);
                    var pad = $element.attr('level') * 20;
                    var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right"></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                    var $wrapper = $(wraptext);
                    return $wrapper;
                },
                templateSelection: function (state) {
                    if (!state.id) {
                        return state.text;
                    }
                    return $(state.element).attr('path');
                }
            });

            $("select[name='parent_id']").change(function(){
                $.ajax({
                    url: 'manage/menu/getNextNo/' + this.value,
                    success: function (data) {
                        $("input[name='no']").val(data);
                        //sdcApp.showLoading(false);
                    }
                });

            });
        },
    };

    $(function () {
        CommonCRUD2(commentsConf);
    });
};