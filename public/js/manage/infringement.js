 function Infringements (baseUrl, tableId) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'obj_name'},
            {
                "data": 'type',
                "render": function (value, type, data) {
                    if(value == 0) {
                        if(data.active == 0)
                            return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa duyệt';
                        if(data.active == 1)
                            return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đang hoạt động';
                        if(data.active == 2)
                            return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Ngừng hoạt động';
                        if(data.active == 3)
                            return '<a class="btn btn-link btn-xs btn-edit"><i class="glyphicon glyphicon-lock"></i></a> Khóa do vi phạm';
                    }

                    if(value == 1) {
                        if(data.active == 0)
                            return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa duyệt';
                        if(data.active == 1)
                            return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã duyệt';
                        if(data.active == 2)
                            return '<a class="btn btn-link btn-xs btn-edit"><i class="glyphicon glyphicon-lock"></i></a> Khóa do vi phạm';
                    }

                    if(value == 2) {
                        if(data.active == 0)
                            return '<a class="btn btn-link btn-xs btn-edit"><i class="glyphicon glyphicon-lock"></i></a> Khóa do vi phạm';
                        if(data.active == 1)
                            return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đang hoạt động';
                    }
                }
            },
            {"data": 'cate_name'},
            {"data": 'comment'},
            {"data": 'ngay_dang', "class": "text-center"},
            {"data": 'nguoi_dang'},
            {
                "data": 'status',
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã xử lý';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chờ xử lý';
                }
            },
            {
                "data": 'type',
                "orderable": false,
                "class": "text-right",
                "render": function (value, type, data) {
                    if(value < 2)
                        return '<a title="Xem chi tiết" class="btn btn-link btn-xs" href="' + data.path + '" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>' +
                            '<button title="Xử lý" class="btn btn-link btn-xs" onclick="perform(' + value + ', '+ data.id + ', \'' + data.obj_name + '\', ' + data.active + ')"><i class="glyphicon glyphicon-cog"></i></button>';
                            //+ '<a title="Sửa" class="btn btn-link btn-xs btn-edit" href="' + baseUrl + '/' + data.id + '/edit"><i class="glyphicon glyphicon-edit"></i></a>' +
                            //'<a title="Xóa" class="btn btn-link btn-xs btn-del" href="#" id="'+data.id+'"><i class="glyphicon glyphicon-trash"></i></a>';
                    else
                        return '<a title="Xem chi tiết" class="btn btn-link btn-xs" href="' + data.path + '" target="_blank"><i class="glyphicon glyphicon-eye-open"></i></a>' +
                            '<button title="Xử lý" class="btn btn-link btn-xs" onclick="perform(' + value + ', '+ data.id + ', \'' + data.obj_name + '\', ' + data.active + ')"><i class="glyphicon glyphicon-cog"></i></button>';
                }
            },
            {"data": 'id', "visible": false},
        ],
        "order": [4, 'desc']
    };

    var commentsConf = {
        baseUrl: baseUrl,
        tableId: tableId,
        tableConf: tableConf,
        titleUpdate: 'Sửa phản ánh vi phạm',
        modalSize: 'large'
    };

    $(function () {
        CommonCRUD2(commentsConf);
    });
};