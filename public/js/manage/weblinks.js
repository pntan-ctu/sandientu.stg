Weblink = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'img_path',"orderable": false,
                "render": function (value) {
                                   return '<img src=" '+"image"+"/"+"150"+"/"+"150"+"/"+value+' ">';
                }  },
            {"data": 'title'},
            {"data": 'url',"orderable": false},
            {"data": 'active',
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                }
            },
            {"data": 'new_window',
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã kích hoạt';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chưa kích hoạt';
                }
            },
            {"data": 'user_name',"orderable": false}, 
            {"data": 'no'},
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [5, 'desc']
    };
    $(function () {
        CommonCRUD(
            baseUrl,
            '#datatable',
            tableConf,
            '#search-form',
            ['keyword', 'active'],
            '#btn-add',
            'Thêm Liên kết Web',
            'Cập nhật Liên kết Web',
            'large');
    });
};
