Document = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'number_symbol', "orderable": false}, //Số/ký hiệu
            {"data": 'quote', "orderable": false, "class": ''}, // trích dẫn
            {"data": 'sign_date1', "orderable": true, "class": "text-center"}, // ngày ban hành
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            }
        ]
    };

    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'idDocType', 'idDocOrgan', 'idDocField'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm mới',
            titleUpdate: 'Cập nhật',
            modalSize: 'large',
            nameColIndex: 0,
            beforeSerialize: function ($form) {
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            },
            formShowedHandler: function ($form) {
                $form.find('#sign_date').inputmask('date');
                $form.find('#sign_date').datepicker({
                    language: 'vi',
                    autoclose: true
                });
                $form.find('.doc-select').select2({
                    dropdownParent: $form
                });
                $('.doc-selects').select2();
                CKEDITOR.replace('documentEditor');
                
            }
        };
        return CommonCRUD2(conf);
    });

};
function setTextFilename(filename) {
    // var filename = $('#images_up').val();
    if (filename.substring(3, 11) == 'fakepath') {
        filename = filename.substring(12);
    } // Remove c:\fake at beginning from localhost chrome
    $('#upload-file-info').html(filename);
}
$('.doc-selects').select2();