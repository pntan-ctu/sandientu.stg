Area = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'name'},
            {"data": 'url',"orderable": false},
            {   data: 'org_name',"orderable": false,
                class: 'text-center',
//                render: sdcApp.renderDataTableDate('dft') // 23:12:56 23/8/2018
            }, // TODO date format
            {"data": 'map', name: 'map',"orderable": false,
                render: function(value) {
                    if (value){
                        return '<img style="width:100%;" src="'+ './image'+ '/'+'100' + '/100/' + value + '" class="btn btn-link btn-xs"></a>';
                    }else {
                        return '<img style="width:100%;" src="'+ './img/' + 'no-image.png' + '" class="btn btn-link btn-xs"></a>';
                    }

                }
            },
            {
                "data": 'id',
                "orderable": false,
                "class": 'text-center',
                "render": function () {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            {"data": 'id', "visible": false}
        ],
        "order": [4, 'desc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'active'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm vùng sản xuất',
            titleUpdate: 'Cập nhật vùng sản xuất',
            modalSize: 'large',
            nameColIndex: 0,
            beforeSerialize: function($form){
                for (instance in CKEDITOR.instances) {
                    CKEDITOR.instances[instance].updateElement();
                }
            },
            //requestSaveDoneHandler: function(data){console.log(data)},
            //requestSaveFailHandler: function(xhr){console.log(xhr)},
            //requestDeleteDoneHandler: function(data){console.log(data)},
            //requestDeleteFailHandler: function(xhr){console.log(xhr)},
            formShowedHandler: function(form){
                $('.bootbox').on('shown.bs.modal', function () {
                    $('#my-editor').trigger('focus')
                })
                CKEDITOR.replace( 'my-editor');
                $('.js-example-basic-single').select2({
                    dropdownParent: form,
                    templateResult: function (data) {
                        // We only really care if there is an element to pull classes from
                        if (!data.element) {
                            return data.text;
                        }

                        var $element = $(data.element);
                        var pad = $element.attr('level') * 20;
                        var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right"></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                        var $wrapper = $(wraptext);
                        return $wrapper;
                    },
                    templateSelection: function (state) {
                        if (!state.id) {
                            return state.text;
                        }
                        return $(state.element).attr('path');
                    }
                });
            },
            //storeHandler: function(){console.log('storeHandler'); return false},
            //updateHandler: function(){console.log('updateHandler'); return false},
            // buttons: {
            //     cancel: {
            //         label: "Hủy"
            //     },
            //     ok: {
            //         label: "Lưu",
            //         callback: function () {
            //             okHandler(editItemId);
            //             return false;
            //         }
            //     }
            // },
            // bindRowActionHandlers: function () {
            //     sdcApp.bindTblDelEvent(conf.tableId, oTable, conf.baseUrl, conf.nameColIndex);
            //     sdcApp.bindTblEditEvent(conf.tableId, oTable, function (id) {
            //         var editUrl = conf.baseUrl + '/' + id + '/edit';
            //         editItemId = id;
            //         okHandler = update;
            //         showForm(conf.titleUpdate, editUrl);
            //     });
            // }
            // bindDefaultEditAction: true,
            // bindDefaultDeleteAction: true,
        };
        return CommonCRUD2(conf);
    });
};
