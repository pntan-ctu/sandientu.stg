SmsHistory = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'stt', "orderable": false, "class": "text-center"},
            {"data": 'content'},
            {"data": 'tel_receiver',"class": "text-center"},
            {"data": 'created_at',"class": "text-center"},
            {"data": 'sms_count',"class": "text-center"}
        ],
        "order": [3, 'desc']
    };

    var smsConf = {
        baseUrl: baseUrl,
        tableId: '#datatable',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['fromdate', 'todate'],
        modalSize: 'large'
    };

    $(function () {
        CommonCRUD2(smsConf);
    });
};