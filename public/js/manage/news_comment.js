NewsComments = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'title'},
            {"data": 'sub_comment'},
            {"data": 'nguoi_binh_luan'},
            {"data": 'email'},
            {"data": 'ngay_dang', "class": "text-center"},
            {
                "data": 'active',
                "render": function (value, type, data) {
                    if(value == 1)
                        return '<img style="margin-top: -1.5px" src="css/images/blue.png"> Đã duyệt';
                    else
                        return '<img style="margin-top: -1.5px" src="css/images/gray.png"> Chờ duyệt';
                }
            },
            //{"data": 'nguoi_duyet'},
            {
                "data": 'active',
                "orderable": false,
                "class": "text-right",
                "render": function (value, type, data) {
                    if(value == 0)
                        return '<button title="Duyệt" class="btn btn-link btn-xs" onclick="setActive(' + data.id + ')"><i class="fa fa-check"></i></button>' +
                            '<a title="Sửa" class="btn btn-link btn-xs btn-edit" href="' + baseUrl + '/' + data.id + '/edit"><i class="glyphicon glyphicon-edit"></i></a>' +
                            '<a title="Xóa" class="btn btn-link btn-xs btn-del" href="#" id="'+data.id+'"><i class="glyphicon glyphicon-trash"></i></a>';
                    else
                        return '<button title="Bỏ duyệt" class="btn btn-link btn-xs" onclick="setInactive(' + data.id + ')"><i class="fa fa-times"></i></button>' +
                            '<a title="Sửa" class="btn btn-link btn-xs btn-edit" href="' + baseUrl + '/' + data.id + '/edit"><i class="glyphicon glyphicon-edit"></i></a>' +
                            '<a title="Xóa" class="btn btn-link btn-xs btn-del" href="#" id="'+data.id+'"><i class="glyphicon glyphicon-trash"></i></a>';
                }
            },
            {"data": 'id', "visible": false},
            {"data": 'comment', "visible": false}
        ],
        "order": [4, 'desc']
    };

    var commentsConf = {
        baseUrl: baseUrl,
        tableId: '#dtNewsComments',
        tableConf: tableConf,
        searchFormId: '#search-form',
        searchFieldNames: ['keyword', 'active'],
        //btnAddId: '#btn-add',
        //titleCreate: 'Thêm bình luận',
        titleUpdate: 'Sửa bình luận',
        modalSize: 'large'
    };

    $(function () {
        CommonCRUD2(commentsConf);
    });
};