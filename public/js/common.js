jQuery(document).ready(function() {
    "use strict";

       
    jQuery(".subDropdown")[0] && jQuery(".subDropdown").on("click", function() {
            jQuery(this).toggleClass("plus"), jQuery(this).toggleClass("minus"), jQuery(this).parent().find("ul").slideToggle()
        })
    var width = $(window).width();
    if (width <= 480) {
        //$('.toggle-button-dmsp').
        $('.toggle-button-dmsp').attr('data-target','.menu-vertical-dmsp');
        $(".regular").slick({
            infinite: true,
            slidesToShow: 2,
            slidesToScroll: 2
        });
    } else if (width <= 768) {
        $('.toggle-button-dmsp').attr('data-target','.menu-vertical-dmsp');
        $(".regular").slick({
            infinite: true,
            slidesToShow: 3,
            slidesToScroll: 3
        });
    } else {
       // $('.menu-vertical-dmsp').attr('display','none');
        $(".regular").slick({
            infinite: true,
            slidesToShow: 5,
            slidesToScroll: 5
        });
        
    }

})
