function processOrder(arrOrgani) {
    var form_data = new FormData();
    form_data.append('ho_ten', $('#ho_ten').val());
    form_data.append('phone_number', $('#phone_number').val());
    var district_id = $('#district').val();
    var noVillage = district_id == 292 || district_id == 413 || district_id == 430 || district_id == 618 ? 1 : 0;
    var regionId = noVillage == 0 ? $('#village').val() : district_id;
    if(regionId == 0) {
        toastr.error('Bạn chưa chọn địa chỉ nhận hàng');
        return;
    }
    form_data.append('region_id', regionId);
    form_data.append('dia_chi', $('#dia_chi').val());
    $.each(arrOrgani, function (index, value) {
        if($('#shiptypeId'+value).val() !== undefined){
            form_data.append('ship_type_id'+value, $('#shiptypeId'+value).val());
        }
        form_data.append('pay_type_id'+value, $('#payTypeId'+value).val());
        form_data.append('transport_fee'+value, $('#transFee'+value).val());
        var shipTimeEstimate = $('#shipTimeEstimate'+value).val();
        form_data.append('ship_time_estimate'+value, shipTimeEstimate);
        form_data.append('weight'+value, $('#weight'+value).val());
    });
    $.ajax({
        url: url+'/dat-hang',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false
    }).done(function (result) {
        if (result.status === 'success') {
            /*if(result.do_payment == 1) {
                window.location.replace(result.redirect_url);
            }
            else {
                toastr.success("Đã đặt hàng thành công !");
                location.reload();
            }*/
            window.location.replace(result.redirect_url);
        }
        if (result.status === 'error') {
            toastr.error(result.message);
        }
    });
}

function addOrder(arrOrgani, amount_pay_online, amout_pay_online_txt) {
    if(amount_pay_online > 0) {
        swal("Bạn đồng ý thanh toán trực tuyến số tiền " + amout_pay_online_txt + '?', {
            buttons: {
                cancel: {
                    text: "Hủy",
                    value: false,
                    visible: true
                },
                confirm: {
                    text: "Chấp nhận",
                    value: true
                }
            },
        })
            .then((value) => {
                if(value){
                    processOrder(arrOrgani);
                }
            });
    }
    else processOrder(arrOrgani);
}

function processOrderByOrgani(organiId) {
    var form_data = new FormData();
    form_data.append('ho_ten', $('#ho_ten').val());
    form_data.append('phone_number', $('#phone_number').val());
    var district_id = $('#district').val();
    var noVillage = district_id == 292 || district_id == 413 || district_id == 430 || district_id == 618 ? 1 : 0;
    var regionId = noVillage == 0 ? $('#village').val() : district_id;
    if(regionId == 0) {
        toastr.error('Bạn chưa chọn địa chỉ nhận hàng');
        return;
    }
    form_data.append('region_id', regionId);
    form_data.append('dia_chi', $('#dia_chi').val());
    form_data.append('organiId', organiId);
    form_data.append('ship_type_id', $('#shiptypeId'+organiId).val());
    form_data.append('pay_type_id', $('#payTypeId'+organiId).val());
    form_data.append('transport_fee', $('#transFee'+organiId).val());
    var shipTimeEstimate = $('#shipTimeEstimate'+organiId).val();
    form_data.append('ship_time_estimate', shipTimeEstimate);
    form_data.append('weight', $('#weight'+organiId).val());
    $.ajax({
        url: url+'/add-order-organi',
        method: 'POST',
        data: form_data,
        contentType: false,
        processData: false
    }).done(function (result) {
        if (result.status === 'success') {
            /*if(result.do_payment == 1) {
                window.location.replace(result.redirect_url);
            }
            else {
                toastr.success("Đã đặt hàng thành công !");
                //location.reload();
            }*/
            window.location.replace(result.redirect_url);
        }
        if (result.status === 'error') {
            toastr.error(result.message);
        }
    });
}

function addOrderByOrgani(organiId, sys_pay_type_id, amount) {
    if(sys_pay_type_id == 3) {
        swal("Bạn đồng ý thanh toán trực tuyến số tiền " + amount + '?', {
            buttons: {
                cancel: {
                    text: "Hủy",
                    value: false,
                    visible: true
                },
                confirm: {
                    text: "Chấp nhận",
                    value: true
                }
            },
        })
        .then((value) => {
            if(value){
                processOrderByOrgani(organiId);
            }
        });
    }
    else processOrderByOrgani(organiId);
}

function showModelInfoOrder() {
    $('#modal-info-order').modal();
}

function showModelInfoShip(shipType, organiId) {
    $('#modal-info-ship').modal();
    $('#organiId').val(organiId);
    var shiptypeId = $('#shiptypeId'+organiId).val();
    var strHtml = '';
    $.each(shipType, function (index, value) {
        strHtml += ' <div class="form-check">';
        if(shiptypeId === '' && index === 0 )
            strHtml += '<input class="form-check-input" checked="true" type="radio" name="shipRadios" id="shipRadios_'+organiId+'_'+value.id+'" value="'+value.id+'">';
        else if(value.id  === parseInt(shiptypeId))
            strHtml += '<input class="form-check-input" checked="true" type="radio" name="shipRadios" id="shipRadios_'+organiId+'_'+value.id+'" value="'+value.id+'">';
        else
            strHtml += '<input class="form-check-input" type="radio" name="shipRadios" id="shipRadios_'+organiId+'_'+value.id+'" value="'+value.id+'">';
        strHtml += ' <label class="form-check-label" for="shipRadios_'+organiId+'_'+value.id+'">';
        if( value.hasOwnProperty("ship_type"))
            strHtml += value.ship_type['name'];
        else
           strHtml += "";
        strHtml += '</label>';
        strHtml += '</div>';
    });
    $('.check-type-ship').html('');
    $('.check-type-ship').html(strHtml);
}

function showModelInfoPay(payType, organiId, existPayOnline ) {
    $('#modal-info-pay').modal();
    $('#organiId').val(organiId);
    var paytypeId = $('#payTypeId'+organiId).val();
    var strHtml = '';
    $.each(payType, function (index, value) {
        strHtml += ' <div class="form-check">';
        if(paytypeId === '' && index === 0 )
            strHtml += '<input class="form-check-input" checked="true" type="radio" name="payRadios" id="payRadios_'+ organiId+'_'+value.id +'" value="'+value.id+'">';
        else if(value.id  === parseInt(paytypeId))
            strHtml += '<input class="form-check-input" checked="true" type="radio" name="payRadios" id="payRadios_'+ organiId+'_'+value.id +'" value="'+value.id+'">';
        else
            strHtml += '<input class="form-check-input" type="radio" name="payRadios" id="payRadios_'+ organiId+'_'+value.id +'" value="'+value.id+'">';
        strHtml += ' <label class="form-check-label" for="payRadios_'+ organiId+'_'+value.id +'">';
        strHtml += value.pay_type['name'];
       // strHtml += value.content;
        strHtml += '</label>';
        strHtml += '</div>';
    });
    $('.check-type-pay').html('');
    $('.check-type-pay').html(strHtml);
    if(existPayOnline == 1) $('#modal-info-pay #info-pay-msg').hide();
    else $('#modal-info-pay #info-pay-msg').show();
}

function updatewModelInfoOrder() {
    var fullname = $('#ho_ten').val();
    var phoneNumber = $('#phone_number').val();
    var address = $('#dia_chi').val();
    $('.name_info').html(fullname + " - " + phoneNumber);

    var province_id = $("#province").val();
    if(province_id == 0) {
        toastr.error("Bạn chưa chọn tỉnh/thành phố");
        return;
    }

    var district_id = $("#district").val();
    if(district_id == 0) {
        toastr.error("Bạn chưa chọn quận/huyện");
        return;
    }

    var noVillage = district_id == 292 || district_id == 413 || district_id == 430 || district_id == 618 ? 1 : 0;
    if(noVillage == 0) {
        var village_id = $("#village").val();
        if (village_id == 0) {
            toastr.error("Bạn chưa chọn phường/xã");
            return;
        }
    }

    var province_txt = $("#province option:selected").text();
    var district_txt = $("#district option:selected").text();
    var village_txt = $("#village option:selected").text();
    var address_full = address + ', ' + (noVillage == 0 ? village_txt + ', ' : '') + district_txt + ', ' + province_txt
    $('.address-full').html(address_full);
    $('#modal-info-order').modal('hide');
    var region_id = noVillage == 0 ? village_id : district_id;
    var url = window.location.href;
    var newUrl = "";
    if(url.indexOf("?") < 0) {
        newUrl = url + "?region=" + region_id;
    }
    else {
        if (url.indexOf("region") > -1) {
            var param = url.split("region");
            if (param[1].indexOf("&") < 0) {
                newUrl = url.substring(0, url.indexOf("region")) + "region=" + region_id;
            }
            else {
                newUrl = url.substring(0, url.indexOf("region")) + "region=" + region_id + param[1].substr(param[1].indexOf("&"));
            }
        }
        else {
            newUrl = url + "&region=" + region_id;
        }
    }

    url = newUrl;
    if (url.indexOf("name") > -1) {
        var param = url.split("name");
        if (param[1].indexOf("&") < 0) {
            newUrl = url.substring(0, url.indexOf("name")) + "name=" + fullname;
        }
        else {
            newUrl = url.substring(0, url.indexOf("name")) + "name=" + fullname + param[1].substr(param[1].indexOf("&"));
        }
    }
    else {
        newUrl = url + "&name=" + fullname;
    }

    url = newUrl;
    if (url.indexOf("tel") > -1) {
        var param = url.split("tel");
        if (param[1].indexOf("&") < 0) {
            newUrl = url.substring(0, url.indexOf("tel")) + "tel=" + phoneNumber;
        }
        else {
            newUrl = url.substring(0, url.indexOf("tel")) + "tel=" + phoneNumber + param[1].substr(param[1].indexOf("&"));
        }
    }
    else {
        newUrl = url + "&tel=" + phoneNumber;
    }

    url = newUrl;
    if (url.indexOf("address") > -1) {
        var param = url.split("address");
        if (param[1].indexOf("&") < 0) {
            newUrl = url.substring(0, url.indexOf("address")) + "address=" + address;
        }
        else {
            newUrl = url.substring(0, url.indexOf("address")) + "address=" + address + param[1].substr(param[1].indexOf("&"));
        }
    }
    else {
        newUrl = url + "&address=" + address;
    }

    window.location.href = newUrl;
}

function updateModelInfoShip() {
    var organiId = $('#organiId').val();
    var shiptype = $('input[name=shipRadios]:checked').val();
    var text = $("label[for='shipRadios_"+organiId+"_"+shiptype+"']").text();
    $('#shiptypeId'+organiId).val(shiptype);
    $('.ship-type-text'+organiId).html(text);
    $('#modal-info-ship').modal('hide');
    var url = window.location.href;
    var newUrl = "";
    if(url.indexOf("?") < 0) {
        newUrl = url + "?s" + organiId + "=" + shiptype;
    }
    else {
        if (url.indexOf("s" + organiId) > -1) {
            var param = url.split("s" + organiId);
            if (param[1].indexOf("&") < 0) {
                newUrl = url.substring(0, url.indexOf("s" + organiId)) + "s" + organiId + "=" + shiptype;
            }
            else {
                newUrl = url.substring(0, url.indexOf("s" + organiId)) + "s" + organiId + "=" + shiptype + param[1].substr(param[1].indexOf("&"));
            }
        }
        else {
            newUrl = url + "&s" + organiId + "=" + shiptype;
        }
    }

    if (newUrl.indexOf("p" + organiId) > -1) {
        var param = newUrl.split("p" + organiId);
        if (param[1].indexOf("&") < 0) {
            newUrl = newUrl.substring(0, newUrl.indexOf("p" + organiId)) + "p" + organiId + "=0";
        }
        else {
            newUrl = newUrl.substring(0, newUrl.indexOf("p" + organiId)) + "p" + organiId + "=0" + param[1].substr(param[1].indexOf("&"));
        }
    }

    window.location.href = newUrl;
}

function updateModelInfoPay() {
    var organiId = $('#organiId').val();
    var paytype = $('input[name=payRadios]:checked').val();
    var text = $("label[for='payRadios_"+organiId+"_"+paytype+"']").text();
    $('#payTypeId'+organiId).val(paytype);
    $('.pay-type-text'+organiId).html(text);
    $('#modal-info-pay').modal('hide');
    var url = window.location.href;
    var newUrl = "";
    if(url.indexOf("?") < 0) {
        newUrl = url + "?p" + organiId + "=" + paytype;
    }
    else {
        if (url.indexOf("p" + organiId) > -1) {
            var param = url.split("p" + organiId);
            if (param[1].indexOf("&") < 0) {
                newUrl = url.substring(0, url.indexOf("p" + organiId)) + "p" + organiId + "=" + paytype;
            }
            else {
                newUrl = url.substring(0, url.indexOf("p" + organiId)) + "p" + organiId + "=" + paytype + param[1].substr(param[1].indexOf("&"));
            }
        }
        else {
            newUrl = url + "&p" + organiId + "=" + paytype;
        }
    }
    window.location.href = newUrl;
}

$(document).on('ready', function () {
   $("#frm_InfoOrder").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "ho_ten": {
                required: true,
            },
            "phone_number": {
                required: true,
                number: true,
                maxlength: 13,
                minlength: 9,
            },
            "dia_chi": {
                required: true,
            }
        },
        messages: {
            "ho_ten": {
                required: "Vui lòng nhập họ tên",
            },
            "phone_number": {
                required: "Vui lòng nhập số điện thoại",
                number: "Vui lòng nhập định dạng số",
                maxlength: "Số điện thoại quá dài",
                minlength: "Số điện thoại quá ngắn",
            },
            "dia_chi": {
                required: "Vui lòng nhập địa chỉ cụ thể",
            }
        },
        submitHandler: function () {
            updatewModelInfoOrder();
        }
    });
});

$("#province").change(function(){
    $.blockUI({ message: 'Đang tải dữ liệu...' });
    $(".blockOverlay").css("zIndex", 1050);
    $(".blockMsg").css("zIndex", 1051);
    var provinceId = $("#province").val();
    $.ajax({
        type: "GET",
        url: "get-regions/" + provinceId,
        dataType: 'JSON',
        success: function(rs) {
            $("#district").empty();
            $('#district').append('<option value="0">-- Chọn quận/huyện --</option>');
            var len = rs.length;
            for(i = 0; i < len; i++) {
                $('#district').append('<option value="' + rs[i]['id'] + '">' + rs[i]['name'] + '</option>');
            }
            $("#village").empty();
            $('#village').append('<option value="0">-- Chọn phường/xã --</option>');
            $("#dia_chi").val('');
            $.unblockUI();
        }
    });
});

$("#district").change(function(){
    $.blockUI({ message: 'Đang tải dữ liệu...' });
    $(".blockOverlay").css("zIndex", 1050);
    $(".blockMsg").css("zIndex", 1051);
    var districtId = $("#district").val();
    $.ajax({
        type: "GET",
        url: "get-regions/" + districtId,
        dataType: 'JSON',
        success: function(rs) {
            $("#village").empty();
            $('#village').append('<option value="0">-- Chọn phường/xã --</option>');
            var len = rs.length;
            if(len == 0) {
                $('#village').hide();
            }
            else {
                $('#village').show();
                for (i = 0; i < len; i++) {
                    $('#village').append('<option value="' + rs[i]['id'] + '">' + rs[i]['name'] + '</option>');
                }
            }
            $("#dia_chi").val('');
            $.unblockUI();
        }
    });
});

$("#village").change(function(){
    $("#dia_chi").val('');
    $("#dia_chi").focus();
});