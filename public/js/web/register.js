$(document).on('ready', function () {
   $("#form_register").validate({
        onfocusout: false,
        onkeyup: false,
        onclick: false,
        rules: {
            "name": {
                required: true,
            },
            "tel": {
                required: true,
                number: true,
                maxlength: 13,
                minlength: 9,
            },
            "email": {
                required: true,
                email: true,
            },
            "password": {
                required: true,
                minlength: 6,
            },
            "password_confirmation": {
                required: true,
                equalTo: "#password",
            },
            "checkbox": {
                required: true,
            }
        },
        messages: {
            "name": {
                required: "Vui lòng nhập họ tên",
            },
            "tel": {
                required: "Vui lòng nhập số điện thoại",
                number: "Vui lòng nhập định dạng số",
                maxlength: "Số điện thoại quá dài",
                minlength: "Số điện thoại quá ngắn",
            },
            "email": {
                required: "Vui lòng nhập địa chỉ email",
                email: "Email của bạn phải có định dạng name@domain.com"
            },
            "password": {
                required: "Vui lòng nhập mật khẩu",
                minlength: "Mật khẩu phải 6 kí tự"
            },
            "password_confirmation": {
                required: "Vui lòng xác nhận mật khẩu",
                equalTo: "Xác nhận mật khẩu chưa đúng",
            },            
            "checkbox": {
                required: "Bạn cần phải đồng ý với điều khoản này",
            }
        },
        submitHandler: function(form) {
            form.submit();
        }
    });
});