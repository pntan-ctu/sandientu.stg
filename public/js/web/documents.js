Documents = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            {"data": 'number_symbol', "orderable": true,"class": 'text-center',
                "render": function (value, type, data) {
                    return '<a style="color: #337ab7" href="van-ban-qppl/'+data.path+'">'+value+'';
                }}, //Số/ký hiệu
            {"data": 'quote', "orderable": false}, // trích dẫn
            {"data": 'sign_date', "orderable": true,"class": 'text-center',
                "render": function (value, type, data) {
                    var dt = new Date(value);
                    return dt.toLocaleString('vi-VN',{year: 'numeric', month: 'numeric', day: 'numeric'}); 
                }
            }, // ngày ban hành
        ]
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable123',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'idType', 'idOrgan', 'idField'],
            btnAddId: '#btn-add',
            titleCreate: 'Thêm mới',
            titleUpdate: 'Cập nhật',
            modalSize: 'large',
            nameColIndex: 0,
        };
        return CommonCRUD2(conf);
    });

};

//function setTextFilename(filename) {
//    // var filename = $('#images_up').val();
//    if (filename.substring(3, 11) == 'fakepath') {
//        filename = filename.substring(12);
//    } // Remove c:\fake at beginning from localhost chrome
//    $('#upload-file-info').html(filename);
//}
//$('.doc-selects').select2();