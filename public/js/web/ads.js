Ads = function (baseUrl) {
    var tableConf = {
        "rowId": 'id',
        "columns": [
            { "data": 'title', "width": "40%" },
            {
                "data": 'advertising_category_name', "width": "20%",
                render: function (value) {
                    return value.name;
                },
            },
            {
                data: 'updated_at', "width": "15%",
                class: 'text-center',
                render: sdcApp.renderDataTableDate() // 23:12:56 23/8/2018
            }, // TODO date format
            {
                data: 'status', "width": "15%",
                class: 'text-center',
                render: function (value, type, data) {
                    switch (String(value)) {
                        case "0":
                            return "Chờ duyệt";
                        case "1":
                            return "Duyệt đăng";
                        case "2":
                            return "Hủy đăng";
                    }
                }
            },
            {
                "data": 'id', "width": "10%",
                "orderable": false,
                "class": 'text-center',
                "render": function (value, type, data) {
                    return sdcApp.commonAjaxTblActionCell;
                }
            },
            { "data": 'id', "visible": false }
        ],
        "order": [2, 'desc']
    };
    $(function () {
        var conf = {
            baseUrl: baseUrl,
            tableId: '#datatable',
            tableConf: tableConf,
            searchFormId: '#search-form',
            searchFieldNames: ['keyword', 'status'],
            btnAddId: '#btn-add',
            titleCreate: 'Đăng tin',
            titleUpdate: 'Cập nhật tin đăng',
            modalSize: 'large',
            nameColIndex: 0,
            formShowedHandler: function (form) {
                $('#link_product_id').select2({
                    dropdownParent: form,
                    width: '100%',
                    placeholder: "--- Chọn sản phẩm đã có trong cơ sở ---"
                }).on('select2:select', function (e) {
                    $('.grid_image').empty();
                    var data = e.params.data;
                    if ($('#id').val() == "") {
                        if (data.id == 0) {
                            $(".grid_image").empty();
                            $('.title').val('');
                            return;
                        }
                        $('.title').val(data.text);
                        sdcApp.showLoading(true);
                        $.ajax({
                            url: "{{url('user/advertising/product_link')}}",
                            method: 'GET',
                            data: {
                                product_id: data.id
                            },
                            dataType: 'json',
                            // processData: false
                        }).done(function (result) {
                            sdcApp.showLoading(false);
                            $("#product_category_id").val(result.product[0].product_category_id).trigger("change");
                            addImageToGrid(result.image);
                        }).fail(function (xhr) {
                            sdcApp.showLoading(false);
                            if (xhr.status === 422) {
                                sdcApp.showLoading(false);
                            }
                        });
                    }
                });
                $('.js-example-basic-single').select2({
                    width: '100%',
                    dropdownParent: form,
                    templateResult: function (data) {
                        // We only really care if there is an element to pull classes from
                        if (!data.element) {
                            return data.text;
                        }

                        var $element = $(data.element);
                        var level = $element.attr('level');
                        var pad = level * 20;
                        var icon_class = "icon_class";
                        if (level == 0)
                            icon_class = "icon_class_0";
                        if (level == 1)
                            icon_class = "icon_class_1";
                        if (level == 2)
                            icon_class = "icon_class_2";
                        if (level == 3)
                            icon_class = "icon_class_3";

                        var wraptext = '<i style="padding-left:' + pad + 'px" class="icon fa fa-angle-right ' + icon_class + ' "></i><span class="' + $element[0].className + '">' + data.text + '</span>';

                        var $wrapper = $(wraptext);
                        return $wrapper;
                    },
                    templateSelection: function (state) {
                        if (!state.id) {
                            return state.text;
                        }
                        return $(state.element).attr('path');
                    }
                });

                $(".category_id").on('change', function () {
                    if ($(this).val() == '1') {
                        $('.region-label').html('Khu vực giao hàng <sup>*</sup>');
                        if ($(".link_product").length > 0) {
                            $(".link_product").addClass('hidden-link-product');
                        }
                    }
                    else if ($(this).val() == '2') {
                        $('.region-label').html('Khu vực bán hàng <sup>*</sup>')
                        if ($(".link_product").length > 0) {
                            $(".link_product").removeClass('hidden-link-product');
                            $('#link_product_id').val(0).trigger('change');
                        }
                    }
                    else {
                        $('.region-label').html('Khu vực <sup>*</sup>')
                        if ($(".link_product").length > 0) {
                            $(".link_product").addClass('hidden-link-product');
                        }
                    }
                });
            },
            beforeSerialize: function (form) {
                var paths = [];
                var path_avatar = $(".image_avatar>img").attr('path');
                $(".grid_image").children('div').each(function () {
                    paths.push($(this).children('img').attr('path'));
                });
                $("#paths").val(paths);
                $("#path_avatar").val(path_avatar);
            }
        };
        return CommonCRUD2(conf);

    });
};
