/**
 * @license Copyright (c) 2003-2018, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see https://ckeditor.com/legal/ckeditor-oss-license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here.
	// For complete reference see:
	// http://docs.ckeditor.com/#!/api/CKEDITOR.config

	// The toolbar groups arrangement, optimized for two toolbar rows.
	config.toolbarGroups = [
		{ name: 'clipboard',   groups: [ 'clipboard', 'undo' ] },
		{ name: 'editing',     groups: [ 'find', 'selection', 'spellchecker' ] },
		{ name: 'links' },
		{ name: 'insert' },
		{ name: 'forms' },
		{ name: 'tools' },
		{ name: 'document',	   groups: [ 'mode', 'document', 'doctools' ] },
		{ name: 'others' },
		'/',
		{ name: 'basicstyles', groups: [ 'basicstyles', 'cleanup' ] },
		{ name: 'paragraph',   groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ] },
		{ name: 'styles' },
		{ name: 'colors' }//,
	//{ name: 'about' }
	];

	// Remove some buttons provided by the standard plugins, which are
	// not needed in the Standard(s) toolbar.
	config.removeButtons = 'Source,Preview';

	// Set the most common block elements.
	config.format_tags = 'p;h1;h2;h3;pre';

	// Simplify the dialog windows.
	config.removeDialogTabs = 'image:advanced;link:advanced';

    /********** NGUYEN QUAN start add **********/

    config.defaultLanguage = 'vi'; //Mặc định nếu sử dụng nếu máy NSD không đặt
    config.language = 'vi'; //Thiết lập chủ đích cho tất cả

    config.extraPlugins = 'image2, colorbutton, youtube, preview, justify';
	
    config.filebrowserUploadMethod = 'form';

    /* Filebrowser routes */
    // The location of an external file browser, that should be launched when "Browse Server" button is pressed.
    config.filebrowserBrowseUrl = "/storage/files";

    // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Flash dialog.
    config.filebrowserFlashBrowseUrl = "/storage/files";

    // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Link tab of Image dialog.
    config.filebrowserImageBrowseLinkUrl = "/storage/thumbs";

    // The location of an external file browser, that should be launched when "Browse Server" button is pressed in the Image dialog.
    config.filebrowserImageBrowseUrl = "/storage/thumbs";

    // Đường dẫn upload ảnh - Được cấu hình trong layout
    //config.filebrowserImageUploadUrl = "/ckeditor/pictures2";

    // Đường dẫn upload file liên kết (link) - Được cấu hình trong layout
    //config.filebrowserUploadUrl = "/ckeditor/attachment_files";

    // The location of a script that handles file uploads in the Flash dialog - Sẽ được cấu hình trong layout (chưa cầu hình)
    //config.filebrowserFlashUploadUrl = "/ckeditor/attachment_files";

    config.allowedContent = true;
    
    config.height = 400;

    /********** NGUYEN QUAN end add **********/
};
