const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/js')
    .scripts('resources/js/sdc.js', 'public/js/sdc.js')
    .scripts('resources/js/sdc-crud.js', 'public/js/sdc-crud.js')
    .scripts('resources/js/messenger.js', 'public/js/messenger.js')
   /*.sass('resources/sass/app.scss', 'public/css')*/;
